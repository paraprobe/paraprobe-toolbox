Future:
* Support of loading results and config files generated with older versions of the toolbox such as v0.4
- [ ] Add Python code for generating workflow and provenance graphs.

Roadmap v0.5
- [ ] Use conda-forge installation as the primary way how to get and install paraprobe-toolbox
- [ ] Added mode to pass feature identifier (e.g. cluster identifier) to paraprobe-surfacer to generate ensemble of wrapping polyhedra.
- [ ] Added export of Voronoi cell polyhedra geometry for visualization of tessellation
- [ ] Added mode to pass feature identifier (e.g. cluster identifier) to build volumetric representation of features via paraprobe-tessellator
- [ ] paraprobe-nanochem: store results of the delocalization for each iontype separately instead of only for the selected type, 
      will bring the key benefit that all possible combinations can be computed from simple additions of integrated intensities instead of
      always recomputing them
- [ ] paraprobe-nanochem: added multithreaded FFT-accelerated delocalization running computing by default the delocalization for all iontypes and stores this,
      a deeper inspection of the situation of delocalization how to revealed that a fast-Fourier transform might not really be necessary as there seems
      to be an imprecision in the communication within apt that which algorithm really runs in during the delocalization:
      paraprobe-nanochem runs the explicit analytical integration of 3D Gaussian kernels at the location of each reconstructed ion;
         this is the situation which D. Larson in his APT book from 2013 in the paragraph about delocalization reported to be the most accurate
         approach although back in the time this was computationally not a feasible option which has now changed and could become even faster with GPUs
      ivas/apsuite likely (according to the above-mentioned book) run a two-step process: firstly, narrow 3D Gaussian are placed and evaluated
          although it is unclear how exactly. secondly, a fast-Fourier transform algorithm is run to smoothen this initial field, however with the advent of
      apav by Jesse Smith a question comes up: apav also runs a two-step algorithm but the evaluation of the first-step narrow 3D Gaussian is performed
          using just the intensity but not a full integration, thereafter the fast-Fourier-transform part is effectively a Gaussian smoothing
          this is more akin to the classical moving window smoothing kernel approaches nowadays used in ML/AI than a fully convolution which only
          is done in Fourier for the reason to take advantage that the actual integration in real space becomes a multiplication in Fourier space.
      for these reasons it turned out that the approach originally taken in paraprobe-nanochem, although, slow is not necessarily in need for a replacement
      users should be aware that the three tools will deliver differently smoothened delocalization fields eventually even for the same nominal
      delocalization settings which reassure M. Kühbach's claim that these three approaches should be benchmarked against each other in the future to
      really compare the field values. This also became apparent when comparing the results from paraprobe-nanochem with those from IVAS/APSuite
      kindly performed by V. Rielli from S. Primig's group in Sydney. Individual delocalization fields are different. Therefore, iso-surfaces can be different,
      which permeates and can quantitatively effect each research study which makes use of such algorithms irrespective whether using IVAS/APSuite, paraprobe,
      or apav. Qualitatively and quantitatively the situation is expected that these effects are also relevant for the atomprobe Matlab toolbox from
      the Erlangen group. Respective developers are kindly invited to discuss the situation and team up for a one-on-one comparison of the resulting field
      quantities. This substantiates why the fields should be exportable and routinely exported with a study, specifically when the analysis cannot be
      reproduced bitwise numerically.

v0.4.1
- [x] Third-party dependency updates CGAL 5.6, HDF5 1.14.2, boost 1.83, rapids 23.08, readthedocs v2
- [x] Replaced deprecated OpenSSL calls to the SHA256 interface with modrn EVP interface calls
- [x] Proper submoduling to nexus-fairmat definitions
- [x] Refactoring under the hood to prepare that the Python tools of the toolbox are usable as modules
- [x] Renaming of the sub-directories and tools removing the name prefix paraprobe-, for example paraprobe-transcoder is now transcoder,
      indeed the removal of problematic hyphen in the tool names makes the tools now usable as modules as it should be and is needed
      for the integration of the tools with conda
- [x] Code cleanup obsolete metadata definitions from C/C++ header files
- [x] Bugfixed tools specifically the situation that the python and C/C++ tools wrote and processed different formatting conventions
      for scalar strings and string arrays. All tools now use variable-length, null-terminated, UTF8-encoded strings preferentially
      scalar strings or lists of strings. Respective calls to HDF5 are performed by convenience functions which perform relevant
      encoding and formatting conversions.
- [x] Clusterer (formerly paraprobe-clusterer) refactored into a production tool.
      Acknowledgements here to David Mayweg and his students from Chalmers University (Gothenburg) for testing and supporting this
      development.
- [x] Bugfixed tessellator specifically that the ion with highest z coordinate was not properly tessellated
- [x] Reporter (formerly paraprobe-autoreporter), added summary composition which ignores also the charge state of molecular ions.
- [x] Optional convenience speed-up of CDF computations for distancer and tessellator by probing predefined quantiles
      instead of visualizing all datapoints; realized via option quantile_based=True. When True sampling predefined quantiles,
      when False using the old default. which is slower.
- [x] Teaching, run all other examples to test the tools more thoroughly, H5Web widgets by default now commented out to
      enable running the examples with a single click (all cell runthrough), these run throughs were successful, the
      jupyter notebook for the rielli_primig and the mendez_martin example is not using the new syntax though, users
      interested in these examples should make the necessary modifications for which the usa_denton_smith example is a
      blueprint.
- [x] Bugfixed nanochem that when normalization is composition that the reported isovalue and respective scalarfields are now correctly
      multiplied by 100 and not as before a dimensionless quantity.
- [x] An experimental conda-build recipe (currently up-to-date with v0.4 but not with the commit detailed under v0.4.1 here)
      and conda-forge feedstock is available here https://anaconda.org/conda-forge/paraprobe


v0.4
* Summer 2023 version, users of older versions are advised to upgrade. Files generated with previous version may have to be recomputed.

2023/04/07
* Sphinx/readthedocs documentation added, experimental conda-forge channel with Sarath Menon,
* Bug-fixing of tutorials, tools, and documentation how NeXus data schemas match to config and results expected/reported by each tool of the toolbox

2023/01/22
* Functionality update, ion types are defined now based on each range, the ranged types are by default automatically checked against all hypothetically possible combinations of isotopes of the atoms from an element, molecular ion to identify, if possible the charge state of each ion. If no charge was uniquely identifiable even if one sacrifices isotopic uniqueness, the ion gets 0 as the charge(state). These cases can guide the researcher that there are eventually issues with these specific ranging definitions such as improper mass-to-charge calibration or likely misindexed ions. Functionalities in parmsetup, autoreporter, transcoder, and ranging tools were modified to reflect this change.
* All loading of file formats from technology partner files is now performed with an own library that can be reused by multiple projects. In the FAIRmat project this library is used to parse atom probe data for the NOMAD OASIS parsers and in the paraprobe-toolbox. This keeps the definitions better in sync and makes the development easier as we do not need to keep both low-level code portions for these tasks in sync individually.

2022/12/27 commit which represents a beta version of v0.4:
* The old C++ HDF5 wrapper was rewritten and supports now chunking and compression. This enabled, as desired and sought after a replacement of all output routines for all tools. The tools now use NeXus/HDF5 and compress the data by default (GZIP, compression_level 1), which reduces the storage cost for paraprobe tool runs by at least a factor of two for integer data.
* Paraprobe-selector was added as a new tool to define masks for custom regions of interest.
* The paraprobe-clusterer tool used in the 2022 arxiv preprint was deprecated. Its functionalities are partly replaced by a first development version which wraps all clustering now about the scikit-learn and (optionally) NVidias RapidsAI package. This is work in progress with David Mayweg at Chalmers University in Gothenburg and Fatim Mouhib from the IMM at RWTH Aachen University.
* This version of paraprobe-clusterer comes with a tutorial for performing DBScan cluster analyses as high-throughput tasks which can take advantage of NeXus/HDF5 to document the parameters, settings, and steps performed to begin with.
* The code behind paraprobe-intersector was revisited and cleaned to simplify its usage. A respective example usage has been added to an updated usa_portland_wang example.
* Within the process of walking through the code base deprecated and dead code was removed in many places.
* Compilation tests on WSL2 (Win10) using Ubuntu 22.04.1 LTS with the v11.3 gnu C/C++ compiler suite and newest thirdparty libraries were tested successfully.
* As of now the status of the conda build recipe for the paraprobe-toolbox is still under review.

v0.3.1 beta
This development version contains a number of feature additions and bug-fixes in preparation for v0.4 planned to be released around christmas
which is planned to offer a complete revision and change of the results files towards using NeXus/HDF5, fully documented and
using chunked and compression by default. The following features and bugfixes have already been implemented and can be tested:

* paraprobe-spatstat, the tool was reactivated to support now k-th nearest neighbors, and radial distribution functions, the tool can take into account various filters (spatial, iontype, sub-sampling) to compute statistics for specifically shaped and distant regions to meshed features in the dataset
* paraprobe-surfacer, can now use also the recently implemented 3D alpha wrappings functionalities of CGAL to offer users a further mechanisms for creating a watertight mesh to the edge of the dataset. The convex hull computation was changed so that it now by default computes a mesh refinement to improve the quality of the triangulated surface mesh of the polyhedron which represents the convex hull about the point cloud
* tutorials, were reorganized and updated, added a tutorial for search molecular ions using paraprobe-ranger
* paraprobe-intersector, added support for including proxies in the set intersection analyses in response to improving capabilities for automated parameter studies like exercised with V. V. Rielli and S. Primig in the 2022 computational geometry paper, added support for filtering the set of objects and proxies to be included via volume, added functionality to perform the analyses of coprecipitates for two sets directly. This is not a simple comparison between a current_set and a next_set. Instead, the members of the joint set current + next are compared against each other because coprecipitates are not only between objects that come from different sets. For the paper with Rielli et al. this comparison between current/current, current/next, and next/next was implemented via post-processing the results of three individual intersector tool runs using Python. With the new functionality this can be performed as a single intersector run. A respective convenience function was added to paraprobe-autoreporter-intersector to display the summary statistics of intersector runs.
* paraprobe-nanochem, added heuristics for handling objects whose surface mesh is not closed such as for objects at the edge of the dataset, an iterative hole-filling, mesh refinement, and fairing protocol is employed, added configurable edge_threshold to qualify objects in the interior and close to the edge of the dataset, fixed bug that aspect ratios were computed correctly but obb edge lengths were reported as distances squared
* paraprobe-autoreporter, added support for simple csv-based summary statistics for different type of reconstructed objects and proxies, and simple knn and rdf plotting for paraprobe-spatstat
* paraprobe-ranger, reactivated functionality for recursively constructing all possible molecular ions within a specified mass-to-charge-state ratio interval, given charge, maximum number of nuclids and target nuclid constraints.
* paraprobe-parmsetup, nanochem tool, tested a simplified formulation and syntax for scripting tasks
* paraprobe-parmsetup, ranger tool, tested a simplified formulation and syntax for scripting tasks
* paraprobe-parmsetup, minor changes, bugfixes and additions for various tools to reflect updated functionalities
* paraprobe-utils, updated the list of available isotope to include also those not observationally stable based on the ase and the radioactivedecay and ase pypi packages
* paraprobe-miscellaneous, implemented tests for replacing hdf5cxxwrapper for a new implementation supporting then chunking, gzip compression, auto-completion and generation of deeply nested groups, and attributes for main C/C++ data types, both reading and writing. Initial tests revealed that most datasets can thereby be compressed to cut the (disk) storage demands of paraprobe result files substantially to about a third of what is currently required. This applies mainly for the many places where integer numbers are stored as floating point numbers show in general lower compression ratios and thus less gains
* paraprobe-tools, implemented/fixed/reactivated support for filtering the analyzed reconstructed dataset volume via union_of_primitives, sub-sampling, and iontype filters, the union_of_primitives can be a collection of spheres, cylinders, and rotated cuboids, filtering can be instructed via the respective parmsetup tool for each paraprobe-tool. Filters can be combined to focus the analysis on specific spatial and compositional features. Sub-sampling should mainly be used only to easily restrict the analysis on e.g. the top or bottom of the dataset. Otherwise sub-sampling can be used to reduce the numerical costs of some analyses at the costs of accuracy. As a testimonial, I can report that I hardly use sub-sampling for reducing computational costs given the in most cases anyway fast analysis speed of the individual tools. This is why I rather favour accuracy over speed
* thirdparty, updated dependencies to use hdf5==1.13.2, boost==1.80.0, cgal==5.5.1, new dependency radioactivedecay==0.4.13
* nexus, added application definitions, i.e. schemes for the formatting of NeXus/HDF5 config files for the transcoder, ranger, surfacer, distancer, tessellator, nanochem, and intersector tool
* nexus, is now used on the input, i.e. configuration side
* licensing, thanks to efforts of Sarath Menon, Jan Janssens, Alaukik Saxena, Mariano Forti, Thomas Hammerschmidt, and Tilmann Hickel, there is now a conda-forge package available for the paraprobe-toolbox. Work on this required to deactivate the code sections which are specific for tetgen so users who wish to follow up on the work for which we used tetgen now have to download tetgen via its official repository. In what follows, paraprobe-toolbox will come with a GPLV3+ license. The individual dependencies will of course keep their own license which is also why these tools are stored individually in specific thirdparty/mandatory sub-directories

v0.3
* arxiv preprint version
