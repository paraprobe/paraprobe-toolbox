#!/bin/bash
# if using Windows/WSL2 make sure your computer qualifies for this and install WSL2 accordingly before you start
# check if HyperV is "yes" via system info, if HyperV is supported, check the Win10/Win11 build number via system info
# install WSL2, the Ubuntu kernel is recommended specifically v22.04.2 LTS.


# walk through these steps carefully

# install relevant tools to compile MPI/C/C++ software on Ubuntu
sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install m4 && sudo apt-get install file && sudo apt-get install build-essential
sudo apt-get install zip && sudo apt-get install unzip
gcc --version && g++ --version && make --version

# typically this installs older versions of the GNU compiler, if you really want to get the latest
# you can add the GNU compiler suite to your apt get repository as it is explained as method 2 here
# https://phoenixnap.com/kb/install-gcc-ubuntu

# continue with a few more essential packages
sudo apt-get install mpich && mpicxx --version && mpiexec --version
sudo apt-get install -y libgmp-dev && sudo apt-get install -y libmpfr-dev && sudo apt-get install -y git
sudo apt-get install apt-show-versions && sudo apt-get install libssl-dev
sudo apt-get install hwloc && lstopo YourMachineTopology.png

# in addition to these tools and libraries we need a recent version of the cmake build tool which takes care
# of all steps to resolve dependencies and paths to compile the C++ tools
# here is a recipe how you can get a sufficiently recent version of cmake
# cmake is developed by Kitware, the same company which is also developing paraview

# the easiest way to get the latest cmake version is via the Kitware apt repository
# follow the documentation of the Kitware team which guides you on this process
# https://apt.kitware.com/

# check if the installation was successful
cmake --version

# if this does not show a cmake vesion or fails it is likely that you have installed cmake in a custom path?
# in this case you should check and if necessary add the location of this path to the console PATH
# variable like so see here https://askubuntu.com/questions/720678/what-does-export-path-somethingpath-mean 
# i.e. export PATH="cmake-location-home/bin:$PATH" and then check that cmake is working
