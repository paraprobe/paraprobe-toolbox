#!/bin/bash

# DEFAULT/MANDATORY/CPU-PARALLELISM-ONLY INSTALLATION ROUTE to install and use every tool of the toolbox
# make sure to have conda activate i.e. execute within the conda base environment
conda create -n paraprobe-toolbox-py3.12 -c conda-forge python=3.12 jupyter jupyterlab jupyterlab-h5web pandas hdbscan gitpython nodejs ase radioactivedecay ifes-apt-tc-data-modeling odfpy sphinx

# activate/i.e. use this environment
conda activate paraprobe-toolbox-py3.12

# ALTERNATIVE INSTALLATION ROUTE via a preconfigure conda build recipe
conda create -n paraprobe-toolbox-py3.12 -c conda-forge python=3.12 paraprobe>=0.5.1

# ALTERNATIVE/OPTIONAL/CPU-AND-GPU-PARALLELISM INSTALLATION ROUTE
# USING THIS INSTALLATION ROUTE MAKES SENSE ONLY if you would like to use paraprobe-clusterer AND specifically use that
# tool's functionality to use NVidia's RAPIDSAI ML/AI library, THIS ROUTE MAKES SENSE ONLY when you have an NVidia GPU.
# ONLY IN THIS CASE, you should follow the alternative installation route below:

# make sure to have conda activate i.e. execute within the conda base environment
# inspect then the specific settings to use via the https://rapids.ai/ release selector
# the next line is an example that will certainly need modification for your particular system
# conda create -n paraprobe-toolbox-r23.12-cu12.0-py3.10 -c rapidsai -c conda-forge -c nvidia rapids=23.12 python=3.10 cuda-version=12.0

# activate/i.e. use this environment
# conda activate paraprobe-toolbox-r23.12-cu12.0-py3.10

# proceed with the installation of the other packages as it was shown in above-mentioned normal conda installation for paraprobe-toolbox
# NOW YOUR ENVIRONMENT IS READY

# make sure that your environment is active in your shell session.

# test if jupyter-lab works successfully, this is also how you can start the service
jupyter-lab --NotebookApp.token=''
# by default the notebook will be served on localhost:8888 so just type this address in your browser
# for WSL typing jupyter-lab will usually leave you with a link on the console stating that the
# jupyter server was started. You can then click the link (e.g. with holding Ctrl and clicking the link)
# this will redirect you to the browser and spin up the jupyter-lab for you then

# IF YOU ARE DONE and would like to do something else in say another environment
# consider to deactivate the conda paraprobe environment
conda deactivate
# this will bring your console back into the base environment
