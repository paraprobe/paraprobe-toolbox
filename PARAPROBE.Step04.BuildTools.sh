#!/bin/bash

# EXECUTE starting from the paraprobe-toolbox directory, only C/C++ tools need to compilation
Tools="utils ranger selector surfacer distancer tessellator spatstat nanochem intersector"

# the following tools are in different states of their development and have
# not been included in the automatic build as there might be issues with these
# tools
# crystalstructure

# GNU C/C++ compiler
MY_C_COMPILER=gcc-13
MY_CXX_COMPILER=g++-13
# Intel oneAPI C/C++ compiler
# make sure to call source setvars.sh within the with which you compile the paraprobe tools
# so that the Intel oneAPI compiler and library locations will be resolved properly
MY_C_COMPILER=icx
MY_CXX_COMPILER=icpx

# alternatively mpicxx

for toolname in $Tools; do
	cd code/$toolname
	rm -rf build/
	mkdir -p build
	cd build
	echo $PWD
	# if using cmake from a custom location point to the ../bin/cmake directory where that executable is located
	cmake -DLOCAL_INSTALL=ON -DGITSHA=`git rev-parse HEAD` -DCMAKE_BUILD_TYPE=Release -DCMAKE_POLICY_DEFAULT_CMP0144=NEW -DCMAKE_C_COMPILER=$MY_C_COMPILER -DCMAKE_CXX_COMPILER=$MY_CXX_COMPILER ..
	make -j16
	if [ $toolname != "utils" ]; then
		chmod +x paraprobe_$toolname
	fi
	cd ../../../
done
