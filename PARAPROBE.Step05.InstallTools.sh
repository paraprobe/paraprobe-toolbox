#!/bin/bash

# EXECUTE starting from the paraprobe-toolbox directory, only the compiled C/C++ executables need to be copied over
# the python apps (e.g. transcoder, clusterer, parmsetup, and reporter) are available as modules
# the jupyter notebook examples in teaching show how one can use these datasets
Tools="ranger selector surfacer distancer tessellator spatstat nanochem intersector"

# similar to the comment made in PARAPROBE.Step05.Build.Tools.sh the following tools
# are in development stage and should thus be compiled only by experienced developers
# crystalstructure
for toolname in $Tools; do
	cp code/$toolname/build/paraprobe_$toolname code/paraprobe_$toolname
	chmod +x code/paraprobe_$toolname
done
