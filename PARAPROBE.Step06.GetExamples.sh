#!/bin/bash

# EXECUTE starting from the paraprobe-toolbox directory

# unpack example data, which come shipped with the repository
cd teaching/example_data/usa_portland_wang
tar -xvf usa_portland_wang.tar.gz
cd ../../../

cd teaching/example_data/aut_leoben_leitner
tar -xvf aut_leoben_leitner.tar.gz
cd ../../../

cd teaching/example_data/usa_denton_smith
wget -c https://zenodo.org/record/7908429/files/usa_denton_smith_apav_si.zip?download=1 -O usa_denton_smith_apav_si.zip
unzip usa_denton_smith_apav_si.zip
cd ../../../
