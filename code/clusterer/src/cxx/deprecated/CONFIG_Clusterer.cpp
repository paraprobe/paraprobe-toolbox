//##MK::GPLV3

#include "CONFIG_Clusterer.h"


string ConfigClusterer::InputfileClusterDefs = "";
apt_uint ConfigClusterer::DatasetIDClusterDefs = 0;
string ConfigClusterer::InputfileRecon = "";
apt_uint ConfigClusterer::DatasetIDRecon = 0;
apt_uint ConfigClusterer::DatasetID = ConfigShared::SimID;
string ConfigClusterer::OutputfilePrefix = "PARAPROBE.Clusterer.Results.SimID." + to_string(ConfigClusterer::DatasetID);
bool ConfigClusterer::IOComputeConvexHull = true;
bool ConfigClusterer::IORecoverEvaporationSequenceID = false;

apt_real ConfigClusterer::DefaultEpsBallRadius = 0.01; //nm


bool ConfigClusterer::read_config_from_h5( const string h5fn )
{
	if ( ConfigShared::SimID == 1 ) {
		//Rielli and Primig
		return false;
	}
	else if ( ConfigShared::SimID == 2 ) {
		//Mendez Martin
		return false;
	}
	else if ( ConfigShared::SimID == 3 ) {
		//Jenkins, London
		InputfileRecon = "PARAPROBE.Transcoder.Results.SimID.3.h5";
		DatasetIDRecon = 3;
		InputfileClusterDefs = "PARAPROBE.Clusterer.Configuration.SimID.3.h5";
		DatasetIDClusterDefs = 3;
		IOComputeConvexHull = true;
		IORecoverEvaporationSequenceID = true;
		//##MK::not implemented for now because not essential for the CG paper
	}
	else {
		return false;
	}

	DatasetID = ConfigShared::SimID;
	OutputfilePrefix = "PARAPROBE.Clusterer.Results.SimID." + to_string(DatasetID);
	DefaultEpsBallRadius = 0.01; //nm

	return true;
}


bool ConfigClusterer::check_config()
{
	cout << "ConfigClusterer::" << "\n";
	cout << "InputfileClusterDefs " << InputfileClusterDefs << "\n";
	cout << "DatasetIDClusterDefs " << DatasetIDClusterDefs << "\n";
	cout << "InputfileReconstruction " << InputfileRecon << "\n";
	cout << "DatasetIDReconstruction " << DatasetIDRecon << "\n";
	cout << "OutputfilePrefix " <<  OutputfilePrefix << "\n";
	cout << "DatasetID " << DatasetID << "\n";
	cout << "IOConvexHull " << IOComputeConvexHull << "\n";
	cout << "IORecoverEvaporationSequenceID " << IORecoverEvaporationSequenceID << "\n";
	cout << "DefaultEpsBallRadius " << DefaultEpsBallRadius << " nm" << "\n";
	
	cout << "\n";
	return true;
}


void ConfigClusterer::report_config( vector<pparm> & res)
{
	/*
	res.push_back( pparm( "InputfilePointClouds", InputfilePointClouds, "", "on which inputfile was the analysis performed for reading the point clouds" ) );
	res.push_back( pparm( "DatasetIDPointClouds", val2str<apt_uint>(DatasetIDPointClouds), "", "which specific dataset inside the inputfile was loaded" ) );
	//##MK::
	*/
}
