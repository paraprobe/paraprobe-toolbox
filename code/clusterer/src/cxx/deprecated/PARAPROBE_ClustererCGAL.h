//##MK::GPLV3

#ifndef __PARAPROBE_CLUSTERER_CGAL_INTERFACE_H__
#define __PARAPROBE_CLUSTERER_CGAL_INTERFACE_H__

#include "CONFIG_Clusterer.h"

/*
//##MK::ASSURE THAT THIS ALSO WORK FOR THE FUNCTIONS USED HERE, UNTIL THEN, LEAVE THIS COMMENTED OUT
//If the macro CGAL_HAS_THREADS is not defined, then CGAL assumes it can use any thread-unsafe code (such as static variables).
//By default, this macro is not defined, unless BOOST_HAS_THREADS or _OPENMP is defined. It is possible to force its definition on the command line,
//and it is possible to prevent its default definition by setting CGAL_HAS_NO_THREADS from the command line.
#define CGAL_HAS_THREADS
//https://doc.cgal.org/latest/Manual/preliminaries.html#Preliminaries_thread_safety
#define I_ASSUME_CGAL_PMP_IS_THREADSAFE
*/


#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/convex_hull_3.h>
/*
#include <vector>
#include <fstream>
*/

typedef CGAL::Exact_predicates_inexact_constructions_kernel  K;
typedef CGAL::Polyhedron_3<K>                     Polyhedron_3;
typedef K::Point_3                                Point_3;
typedef CGAL::Surface_mesh<Point_3>               Surface_mesh;

#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/Polygon_mesh_processing/measure.h>
namespace PMP = CGAL::Polygon_mesh_processing;


#endif
