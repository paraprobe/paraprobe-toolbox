//##MK::GPLV3


#include "PARAPROBE_ClustererHDF5.h"
//https://www.learncpp.com/cpp-tutorial/114-constructors-and-initialization-of-derived-classes/


clusterer_h5::clusterer_h5()
{
}


clusterer_h5::~clusterer_h5()
{
}


int clusterer_h5::create_group_structure( const string h5fn, const apt_uint id )
{
	string fnm = h5fn;
	string grpnm = "";
	string fwslash = "/";
	int status = MYHDF5_SUCCESS;

	h5seqHdl h5w = h5seqHdl();
	status = h5w.create_file( fnm );

	string prefix = MYCLST + to_string(id);
	vector<string> grps = { 	prefix,
								prefix + MYCLST_DATA   };

	/* ##MK::these parts of the hierarchy already exist when loading results from the Python paraprobe-clusterer when loading AMETEK results
	prefix + MYCLST_META,
	prefix + MYCLST_META_PROC,
	prefix + MYCLST_META_TOOL,
	prefix + MYCLST_META_TOOL_COMP,
	*/

	for( auto it = grps.begin(); it != grps.end(); it++ ) {
		status = h5w.add_group( *it );
		if ( status == MYHDF5_SUCCESS ) {
			cout << "Created group " << *it << "\n";
		}
		else {
			cerr << "Creating group " << *it << " failed !" << "\n";
			return MYHDF5_FAILED;
		}
	}
	return true;
}


/*
int clusterer_h5::write_config( const string h5fn, const apt_uint id )
{
	string fnm = h5fn;
	string grpnm = "";
	string dsnm = "";
	string fwslash = "/";
	string str = "";
	int status = MYHDF5_SUCCESS;

	h5seqHdl h5w = h5seqHdl();
	status = h5w.use_file( fnm );

	string prefix = MYCHM + to_string(id);

	dsnm = prefix + MYCHM_META_PROC + fwslash + "Comment";
	str = "Applied successfully an analysis of the nanoscale architecture of the specimen";
	if( h5w.add_contiguous_string( dsnm, str ) != MYHDF5_SUCCESS ) {
		cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
	}
	dsnm = prefix + MYCHM_META_TOOL + fwslash + "Name";
	str = "paraprobe-nanochem";
	if( h5w.add_contiguous_string( dsnm, str ) != MYHDF5_SUCCESS ) {
		cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
	}

	//collect all configuration parameter with metadata
	//tool_config_dict myconfig;
	//ConfigNanochem::report_config( myconfig );
	//for( auto it = myconfig.begin(); it != myconfig.end(); it++ ) {
	//	dsnm = prefix + MYCHM_META_TOOL_COMP + fwslash + it->keyword;

	return MYHDF5_SUCCESS;
}
*/
