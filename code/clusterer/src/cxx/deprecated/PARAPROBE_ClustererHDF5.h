//##MK::GPLV3

#ifndef __PARAPROBE_CLUSTERER_HDF_HDL_H__
#define __PARAPROBE_CLUSTERER_HDF_HDL_H__


#include "PARAPROBE_ClustererMeshing.h"


class clusterer_h5 : public h5BaseHdl
{
	//tool-specific sub-class of a HDF5 inheriting all methods of the base class h5Hdl but adds tool specific read/write functions
	//coordinating instance handling all (sequential) writing to HDF5 file wrapping HDF5 low-level C library calls

public:
	clusterer_h5();
	~clusterer_h5();

	int create_group_structure( const string h5fn, const apt_uint id );
	/*
	int write_config( const string h5fn, const apt_uint id );
	*/
};

#endif
