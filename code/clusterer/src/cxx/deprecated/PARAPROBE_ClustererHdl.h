//##MK::GPLV3

#ifndef __PARAPROBE_CLUSTERER_HDL_H__
#define __PARAPROBE_CLUSTERER_HDL_H__

#include "PARAPROBE_ClustererXDMF.h"

class clustererHdl : public mpiHdl
{
	//process-level class which implements the worker instance

public:
	clustererHdl();
	~clustererHdl();
	

	bool read_relevant_input();
	/*
	bool broadcast_relevant_input();
	*/
	bool read_relevant_input_ametek();
	bool reconstruct_clusters_computed_ametek();
	bool recover_evaporation_sequence_id();

	/*
	bool read_relevant_input_reconstruction();
	*/

	void execute_cluster_analysis_workpackage();

	bool init_results_h5();
	bool write_config_cluster_analysis_task_h5();
	int write_results_cluster_analysis_task_h5();

	vector<pair<p3d,apt_real>> ametek_ion_in_cluster;
	map<apt_uint,cluster*> objs;

	clusterer_h5 debug_h5Hdl;
	clusterer_xdmf debug_xdmfHdl;
	
	profiler clusterer_tictoc;
};


#endif
