//##MK::GPLV3

#include "PARAPROBE_ClustererMeshing.h"


cluster::cluster()
{
	clusterid = UMX;
	volume = MYZERO;
	dt_convexhull = 0.;

	ions = vector<p3dm1>();
	vrts_cvh = vector<p3d>();
	fcts_cvh = vector<tri3u>();
};


cluster::~cluster()
{

}


int cluster::compute_quickhull3_convex_hull()
{
	double ctic = omp_get_wtime();
	double ctoc = 0.;

	vector<Point_3> points;
	for( auto it = ions.begin(); it != ions.end(); it++ ) {
		points.push_back( Point_3(it->x, it->y, it->z) );
	}

	/*
	// define polyhedron to hold convex hull
	Polyhedron_3 ply_mesh;
	// compute convex hull of non-collinear points
	CGAL::convex_hull_3(points.begin(), points.end(), ply_mesh);
	cout << "The convex hull contains " << ply_mesh.size_of_vertices() << " vertices" << "\n";
	*/

	Surface_mesh sm;
	CGAL::convex_hull_3(points.begin(), points.end(), sm);
	cout << "The convex hull of cluster " << clusterid << " contains " << num_vertices(sm) << " vertices" << "\n";

	bool triangulation_status = PMP::triangulate_faces(sm);
	if ( triangulation_status == true ) {
	//confirm that all faces are triangle
		for(boost::graph_traits<Surface_mesh>::face_descriptor fit : faces(sm)) {
			if (next(next(halfedge(fit, sm), sm), sm) != prev(halfedge(fit, sm), sm)) {
				cerr << "The triangulation of cluster " << clusterid << " failed, at least one non-triangular face is left in the mesh !" << "\n";
				return MYCGAL_CONVEX_HULL_TRIANGULATION_FAILED;
			}
		}

		volume = PMP::volume(sm);
		cout << "The volume of the convex hull to cluster " << clusterid << " is " << volume << " nm^3" << "\n";

		//export unique vertices and facets of the mesh, now sm is guaranteed triangle mesh
		//##MK::reduce number of vertices with vectorComparator
		Surface_mesh::Vertex_range vrng = sm.vertices();
		Surface_mesh::Vertex_range::iterator vb = vrng.begin();
		Surface_mesh::Vertex_range::iterator ve = vrng.end();
		for(   ; vb != ve; ++vb ) {
			//size_t vertexID = *vb;
			vrts_cvh.push_back( p3d( sm.point(*vb).x(), sm.point(*vb).y(), sm.point(*vb).z() ) );
		}

		Surface_mesh::Face_range frng = sm.faces();
		Surface_mesh::Face_range::iterator fb = frng.begin();
		Surface_mesh::Face_range::iterator fe = frng.end();
		for(   ; fb != fe; ++fb ) {

			vector<apt_uint> triangle; //https://doc.cgal.org/latest/Surface_mesh/index.html#circulators_example

			CGAL::Vertex_around_face_iterator<Surface_mesh> vbegin, vend;
			for(boost::tie(vbegin, vend) = vertices_around_face(sm.halfedge(*fb), sm); vbegin != vend; ++vbegin ) {
				triangle.push_back( (apt_uint) *vbegin ); //##MK::(size_t)
			}

			fcts_cvh.push_back( tri3u(triangle[0], triangle[1], triangle[2] ) );
		}

		ctoc = omp_get_wtime();
		cout << "The computation of the convex hull to cluster " << clusterid << " and extraction of unique vertices and facets took " << (ctoc-ctic) << "\n";
		dt_convexhull = (ctoc-ctic);
	}
	else {
		cerr << "The triangulation of cluster " << clusterid << " failed !" << "\n";
		return MYCGAL_CONVEX_HULL_TRIANGULATION_FAILED;
	}

	return MYCGAL_CONVEX_HULL_TRIANGULATION_SUCCESS;
}
