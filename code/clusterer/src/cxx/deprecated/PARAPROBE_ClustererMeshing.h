//##MK::GPLV3

#ifndef __PARAPROBE_CLUSTERER_MESHING_HDL_H__
#define __PARAPROBE_CLUSTERER_MESHING_HDL_H__


#include "PARAPROBE_ClustererStructs.h"

#define MYCGAL_CONVEX_HULL_TRIANGULATION_SUCCESS	0
#define MYCGAL_CONVEX_HULL_TRIANGULATION_FAILED		-1


class cluster
{
public:
	cluster();
	~cluster();

	int compute_quickhull3_convex_hull();

	apt_uint clusterid;
	apt_real volume;
	double dt_convexhull;

	vector<p3dm1> ions;			//position and evaporation ID, if it was recovered, otherwise UMX
	vector<p3d> vrts_cvh; 		//disjoint vertices of the quickhull3 convex hull
	vector<tri3u> fcts_cvh; 	//facet triangles of the quickhull3 convex hull
};


#endif
