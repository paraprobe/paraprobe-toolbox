//##MK::GPLV3

#ifndef __PARAPROBE_CLUSTERER_METADATA_H__
#define __PARAPROBE_CLUSTERER_METADATA_H__

#include "../../../paraprobe-utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"
//#include "../../../paraprobe-nanochem/src/cxx/PARAPROBE_NanochemMetadata.h"

#define MYCLST										"/ClusterGeometryAnalysisID"
	#define MYCLST_META								"/Metadata"
	#define MYCLST_META_PROC							"/Metadata/ProcessStatus"
	#define MYCLST_META_TOOL							"/Metadata/Tool"
	#define MYCLST_META_TOOL_COMP						"/Metadata/Tool/Component"
	//#define MYCLST_META_TOOL_COMP_FLT					"/Metadata/Tool/Component/Filter"
	//#define MYCLST_META_TOOL_COMP_FLT_MASK				"/Metadata/Tool/Component/Filter"

	#define MYCLST_DATA								"/Data"
	#define MYCLST_DATA_CVHL_TSKS						"/ConvexHull"
			//for each convex hull<id> about a cluster
			#define MYCLST_DATA_CVHL_TSKS_VXYZ				"VertexPosition"
			#define MYCLST_DATA_CVHL_TSKS_FCTS				"FacetIndices"
			#define MYCLST_DATA_CVHL_TSKS_TOPO				"Topology"
			#define MYCLST_DATA_CVHL_TSKS_EVAPID			"EvaporationID"

		#define MYCLST_DATA_CVHL_VOL					"ConvexHullVolume"
		#define MYCLST_DATA_CVHL_ID						"ConvexHullID"
		#define MYCLST_DATA_CVHL_VXYZ					"VertexPosition"
		//#define MYCLST_DATA_CVHL_FCTS					"FacetIndices"
		#define MYCLST_DATA_CVHL_TOPO					"Topology" //support array for visualizing in Paraview
		#define MYCLST_DATA_CVHL_CLID					"FacetClusterID"

#endif
