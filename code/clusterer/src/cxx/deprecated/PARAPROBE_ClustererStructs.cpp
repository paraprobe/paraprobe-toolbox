//##MK::GPLV3

#include "PARAPROBE_ClustererStructs.h"


kdtree::kdtree()
{
}


kdtree::~kdtree()
{
	if ( kauri.first != NULL ) {
		delete kauri.first; kauri.first = NULL;
	}
	if ( kauri.second != NULL ) {
		delete kauri.second; kauri.second = NULL;
	}
	kauri = pair<kd_tree*,vector<p3dm1>*>(NULL, NULL);
}


void kdtree::clear()
{
	if ( kauri.first != NULL ) {
		delete kauri.first; kauri.first = NULL;
	}
	if ( kauri.second != NULL ) {
		delete kauri.second; kauri.second = NULL;
	}
	kauri = pair<kd_tree*,vector<p3dm1>*>(NULL, NULL);
}


bool kdtree::build( ionCloud const & pp3 )
{
	kauri = pair<kd_tree*,vector<p3dm1>*>(NULL, NULL);

	vector<p3d> tmp_pos; //temp buffer for the kdtree to operate on
	vector<size_t> tmp_prm; //temp buffer for the permutations to operate on

	kd_tree* tree = NULL;
	try {
			tree = new kd_tree;
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation of kd_tree failed !" << "\n"; return false;
	}
	vector<p3dm1>* buf = NULL;
	try {
		buf = new vector<p3dm1>;
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation of vector<p3dm1> failed !" << "\n"; return false;
	}

	kauri.first = tree;
	kauri.second = buf;

	cout << "Constructing kd_tree for " << pp3.ionpp3.size() << " ions" << "\n";

	for( size_t ionidx = 0; ionidx < pp3.ionpp3.size(); ionidx++ ) {
		tmp_pos.push_back( pp3.ionpp3[ionidx] );
	}
	cout << "Identified occupancy of the kd_tree" << "\n";

	kauri.first->build( tmp_pos, tmp_prm, ConfigShared::KDTreeStackSize );

	kauri.first->pack_p3dm1( tmp_prm, tmp_pos, *(kauri.second) );

	if ( kauri.first->verify( *(kauri.second) ) == false ) {
		cerr << "Verification of kd_tree failed !" << "\n"; return false;
	}
	cout << "Constructed kd_tree successfully with " << kauri.second->size() << " ions" << "\n";

	//release temporaries
	tmp_pos = vector<p3d>();
	tmp_prm = vector<size_t>();
	return true;
}


void kdtree::query( p3d const & p, const apt_real sqrR, vector<p3dm1> & res )
{
	kauri.first->query_rball_noclear_nosort_external( p, *(kauri.second), sqrR, res );
}


aabb3d kdtree::get_aabb()
{
	aabb3d retval = aabb3d();
	for( auto it = kauri.second->begin(); it != kauri.second->end(); it++ ) {
		retval.possibly_enlarge_me( *it );
	}
	retval.add_epsilon_guard();
	retval.scale();

	return retval;
}
