//##MK::GPLV3

#ifndef __PARAPROBE_CLUSTERER_STRUCTS_H__
#define __PARAPROBE_CLUSTERER_STRUCTS_H__

#include "PARAPROBE_ClustererCGAL.h"


class kdtree
{
public:
	kdtree();
	~kdtree();

	void clear();
	bool build( ionCloud const & pp3 );
	void query( p3d const & p, const apt_real sqrR, vector<p3dm1> & res );
	aabb3d get_aabb();

private:
	pair<kd_tree*,vector<p3dm1>*> kauri; //first in [] tells how ions in second in [] are sorted, m1 gives evaporationID
};



#endif
