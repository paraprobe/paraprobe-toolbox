#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#

import numpy as np
import h5py
# from ifes_apt_tc_data_modeling.utils.nist_isotope_data import isotopes
from utils.src.python.hdf_five_string_io import read_strings_from_dataset


class AlgorithmDBScan():
    def __init__(self, eps=None, min_pts=None):
        self.config = {}
        self.config["eps"] = eps
        self.config["min_pts"] = min_pts


class AlgorithmHDBScan():
    def __init__(
            self, min_cluster_size=None, min_samples=None,
            cluster_selection_epsilon=None, alpha=None):
        self.config = {}
        self.config["min_cluster_size"] = min_cluster_size
        self.config["min_samples"] = min_samples
        self.config["cluster_selection_epsilon"] = cluster_selection_epsilon
        self.config["alpha"] = alpha


class ClusteringTask():
    """Representation of the settings for one clustering process task."""
    def __init__(self):
        self.dataset = {}
        self.iontypes = {}
        self.spatial_filter = {"windowing_method": "entire_dataset"}
        # self.ion_to_edge = {}
        self.ion_type_filter = "resolve_element"
        self.ion_query_isotope_vector = None
        self.dbscan = {}
        self.hdbscan = {}


class ConfigClusterer():
    """Configuration for the paraprobe-clusterer tool."""
    def __init__(self, jobid):
        assert 0 < jobid < np.iinfo(np.uint32).max, \
            "Argument jobid needs to be larger than zero but within the range of np.uint32 !"
        self.simid = jobid
        self.config_file_name = f"PARAPROBE.Clusterer.Config.SimID.{self.simid}.nxs"
        self.output_file_name = f"PARAPROBE.Clusterer.Results.SimID.{self.simid}.nxs"
        self.reconstruction = {}
        self.ranging = {}
        self.ion_to_edge = {}
        self.read_config_from_nexus(self.config_file_name)

    def read_config_from_nexus(self, nx5fn):
        """Read the configuration from the NeXus/HDF5 file."""
        with h5py.File(nx5fn, "r") as h5r:
            grpnm = "/entry1"  # currently only one entry
            n_processes = h5r[f"{grpnm}/number_of_tasks"][()]
            print(f"Writing results to {self.output_file_name}")
            self.clustering_tasks = {}
            for tskid in np.arange(1, n_processes + 1):
                self.clustering_tasks[tskid] = ClusteringTask()

                grpnm = f"/entry1/cluster_analysis{tskid}/reconstruction/"
                self.clustering_tasks[tskid].dataset["file_path"] \
                    = read_strings_from_dataset(h5r[f"{grpnm}path"][()]).rpartition("/")[-1]
                self.clustering_tasks[tskid].dataset["dset_mass_to_charge"] \
                    = read_strings_from_dataset(h5r[f"{grpnm}mass_to_charge"][()])
                self.clustering_tasks[tskid].dataset["dset_position"] \
                    = read_strings_from_dataset(h5r[f"{grpnm}position"][()])

                grpnm = f"/entry1/cluster_analysis{tskid}/ranging/"
                self.clustering_tasks[tskid].iontypes["file_path"] \
                    = read_strings_from_dataset(h5r[f"{grpnm}path"][()]).rpartition("/")[-1]
                self.clustering_tasks[tskid].iontypes["grpnm_iontypes"] \
                    = read_strings_from_dataset(h5r[f"{grpnm}ranging_definitions"][()])
                # spatial filter currently for the entire domain
                grpnm = f"/entry1/cluster_analysis{tskid}/"
                self.clustering_tasks[tskid].ion_type_filter \
                    = read_strings_from_dataset(h5r[f"{grpnm}ion_type_filter"][()])
                assert self.clustering_tasks[tskid].ion_type_filter == "resolve_element", \
                    "Currently only the resolve_element ion_type_filter is allowed!"
                self.clustering_tasks[tskid].ion_query_nuclide_vector \
                    = h5r[f"{grpnm}ion_query_nuclide_vector"][:, :]
                assert np.shape(self.clustering_tasks[tskid].ion_query_nuclide_vector)[0] == 1, \
                    "Currently n_ions has to be 1 for ion_query_nuclide_vector!"
                self.clustering_tasks[tskid].ion_query_nuclide_vector \
                    = self.clustering_tasks[tskid].ion_query_nuclide_vector[0, :]

                grpnm = f"/entry1/cluster_analysis{tskid}/dbscan/"
                if grpnm in h5r.keys():
                    method = read_strings_from_dataset(h5r[f"{grpnm}high_throughput_method"][()])
                    print(method)
                    assert method == "combinatorics", \
                        "Currently only combinatorics as method is supported !"
                    eps = h5r[f"{grpnm}eps"][...]
                    min_pts = h5r[f"{grpnm}min_pts"][...]
                    clst_tskid = 1
                    self.clustering_tasks[tskid].dbscan = {}
                    for i in eps:
                        for j in min_pts:
                            self.clustering_tasks[tskid].dbscan[clst_tskid] \
                                = AlgorithmDBScan(i, j)
                            clst_tskid += 1
                    print(f"TaskID {tskid} has {len(self.clustering_tasks[tskid].dbscan.keys())} DBScan tasks.")

                grpnm = f"/entry1/cluster_analysis{tskid}/hdbscan/"
                if grpnm in h5r.keys():
                    method = read_strings_from_dataset(h5r[f"{grpnm}high_throughput_method"][()])
                    print(method)
                    assert method == "combinatorics", "Currently only combinatorics as method is supported !"
                    min_cluster_size = h5r[f"{grpnm}min_cluster_size"][...]
                    min_samples = h5r[f"{grpnm}min_samples"][...]
                    cluster_selection_epsilon = h5r[f"{grpnm}cluster_selection_epsilon"][...]
                    alpha = h5r[f"{grpnm}alpha"][...]
                    clst_tskid = 1
                    self.clustering_tasks[tskid].hdbscan = {}
                    for i in min_cluster_size:
                        for j in min_samples:
                            for k in cluster_selection_epsilon:
                                for m in alpha:
                                    self.clustering_tasks[tskid].hdbscan[clst_tskid] \
                                        = AlgorithmHDBScan(i, j, k, m)
                                    clst_tskid += 1
                    print(f"TaskID {tskid} has {len(self.clustering_tasks[tskid].hdbscan.keys())} HDBScan tasks.")
        print(self.clustering_tasks[tskid])
