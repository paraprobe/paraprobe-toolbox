/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_CONFIG_DISTANCER_H__
#define __PARAPROBE_CONFIG_DISTANCER_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"

enum DISTANCING_METHOD {
	DISTANCING_NONE,
	DISTANCING_COMPLETE,
	DISTANCING_SKIN
};


struct trimesh_info
{
	string h5fn;
	string dsnm_verts;						//in case of piecewise linear complexes for which the vertex coordinate triplets were reduce to unique triplets ...
	string dsnm_facets;						//... and facet to vertex indices are provided
	string dsnm_vnormals;
	string dsnm_fnormals;
	string dsnm_patchid;					//to which cluster/surface patch is the triangle assigned
	match_filter<apt_uint> patch_identifier_filter;

	trimesh_info () : h5fn(""), dsnm_verts(""), dsnm_facets(""),
			dsnm_vnormals(""), dsnm_fnormals(""), dsnm_patchid(""),
			patch_identifier_filter(match_filter<apt_uint>()) {}

	trimesh_info( const string _h5fn, const string _dsnm_v, const string _dsnm_ft,
			const string _dsnm_vnrm, const string _dsnm_fnrm, const string _dsnm_patch ) :
		h5fn(_h5fn), dsnm_verts(_dsnm_v), dsnm_facets(_dsnm_ft),
			dsnm_vnormals(_dsnm_vnrm), dsnm_fnormals(_dsnm_fnrm),
			dsnm_patchid(_dsnm_patch),
			patch_identifier_filter(match_filter<apt_uint>()) {}
};

ostream& operator<<(ostream& in, trimesh_info const & val);


struct distancing_task
{
	apt_uint taskid;
	vector<trimesh_info>  trimeshes;
	distancing_task() :
		taskid(UMX), trimeshes(vector<trimesh_info>()) {}
};

ostream& operator<<(ostream& in, distancing_task const & val);


class ConfigDistancer
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static string InputfileDataset;
	static string ReconstructionDatasetName;
	static string MassToChargeDatasetName;
	static string InputfileIonTypes;
	static string IonTypesGroupName;

	static vector<trimesh_info> InputfileDatasetTriMeshes;
	//no connectivity information is required

	//add ROI filtering and sub-sampling functionalities
	static WINDOWING_METHOD WindowingMethod;
	static vector<roi_sphere> WindowingSpheres;
	static vector<roi_rotated_cylinder> WindowingCylinders;
	static vector<roi_rotated_cuboid> WindowingCuboids;
	static vector<roi_polyhedron> WindowingPolyhedra;
	//sub-sampling
	static lival<apt_uint> LinearSubSamplingRange;
	//match filters
	static match_filter<unsigned char> IontypeFilter;
	static match_filter<unsigned char> HitMultiplicityFilter;

	static DISTANCING_METHOD DistancingMethod;
	static apt_real DistancingRadiusMax; //skin depth

	static vector<distancing_task> DistancingTasks;

	//sensible defaults
	static apt_real ItrvQryDistIfoHdlBinWidthMin;
	static apt_real ItrvQryDistEpsilon;
	static lival<apt_real> AdvDistancingBinWidthRange;
	static bool IOStoreDistances;
};

#endif
