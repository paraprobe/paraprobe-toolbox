/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_DistancerHdl.h"


bool parse_configuration_from_nexus()
{
	tool_startup( "paraprobe-distancer" );
	if ( ConfigDistancer::read_config_from_nexus( ConfigShared::ConfigurationFile ) == true ) {
		cout << "Configuration from file " << ConfigShared::ConfigurationFile << " was accepted" << "\n";
		cout << "This analysis has SimulationID " << "SimID." <<  ConfigShared::SimID << "\n";
		cout << "Results of this analysis are written to " << ConfigShared::OutputfileName << "\n";
		cout << endl;
		return true;
	}
	cerr << "Parsing configuration failed !" << "\n";
	return false;
}


void execute_distancing_tasks( const int r, const int nr, char** pargv )
{
	//allocate process-level instance of a distancerHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	distancerHdl* dst = NULL;
	int localhealth = 1;
	try {
		dst = new distancerHdl;
		dst->set_myrank(r);
		dst->set_nranks(nr);
		//dst->commit_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " allocating distancerHdl class object failed !" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	if ( dst->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete dst; dst = NULL; return;
	}

	//we have all on board, now read the dataset that we want to process
	//MK::we could have all processes loading from the same file, or have only MASTER do so and broadcast, we opt for the latter strategy
	//##MK::should not be a problem because initialization is orders of magnitude cheaper than actual calculations...
	if ( ConfigDistancer::DistancingMethod != DISTANCING_NONE ) {
		//master reads first all relevant input, slaves wait
		if ( dst->get_myrank() == MASTER ) {

			if ( dst->read_relevant_input() == false ) {
				cout << "Rank " << dst->get_myrank() << " failed reading essential pieces of the relevant input !" << "\n";
				localhealth = 0;
			}
		}
		/*
		//MPI_Barrier( MPI_COMM_WORLD );
		if ( dst->all_healthy_still( localhealth ) == false ) {
			cerr << "Rank " << dst->get_myrank() << " has recognized that not all data have arrived at the master !" << "\n";
			delete dst; dst = NULL; return;
		}
		//##MK::master broadcasts relevant pieces of information to all slaves
		*/

		if ( dst->crop_reconstruction_to_analysis_window() == true ) {

			dst->execute_distancing_workpackage();

		}
		else {
			cout << "For the given filter settings no ion would remain in the ROI !" << "\n";
		}
	}

	//release resources
	delete dst; dst = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	string start_time = timestamp_now_iso8601();

	if ( argc == 1 || argc == 2 ) {
		string cmd_option = "--help";
		if ( argc == 2 ) { cmd_option = argv[1]; };
		command_line_help( cmd_option, "distancer" );
		return 0;
	}
	else if ( argc == 3 ) {
		ConfigShared::SimID = stoul( argv[SIMID] );
		ConfigShared::ConfigurationFile = argv[CONTROLFILE];
		ConfigShared::OutputfilePrefix = "PARAPROBE.Distancer.Results.SimID." + to_string(ConfigShared::SimID);
		ConfigShared::OutputfileName = ConfigShared::OutputfilePrefix + ".nxs";
		if ( init_results_nxs( ConfigShared::OutputfileName, "distancer", start_time, 1 ) == false ) {
			return 0;
		}
	}
	else {
		return 0;
	}

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( parse_configuration_from_nexus() == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks !" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	
	if ( nr == SINGLEPROCESS ) {

		cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

//EXECUTE SPECIFIC TASK
		execute_distancing_tasks( r, nr, argv );
	}
	else {
		cerr << "Rank " << r << " currently paraprobe-distancer is implemented for a single process only !" << "\n";
	}

	//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	if ( finish_results_nxs( ConfigShared::OutputfileName, start_time, tic, 1 ) == true ) {
		cout << "paraprobe-distancer success" << endl;
	}
	return 0;
}
