/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


#include "PARAPROBE_DistancerHdl.h"


distancerHdl::distancerHdl()
{
}


distancerHdl::~distancerHdl()
{
}


bool distancerHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigDistancer::InputfileDataset, ConfigDistancer::ReconstructionDatasetName ) == true &&
			read_ranging_from_h5( ConfigDistancer::InputfileIonTypes, ConfigDistancer::IonTypesGroupName ) == true &&
				read_ionlabels_from_h5( ConfigDistancer::InputfileIonTypes, ConfigDistancer::IonTypesGroupName ) == true ) {

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//build also here a default array with pieces of information for all ions
		if ( ions.ionpp3.size() > 0 && ions.ionifo.size() != ions.ionpp3.size() ) {
			try {
				ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(),
						p3dinfo( RMX, UNKNOWNTYPE, UNKNOWNTYPE, 0x01, WINDOW_ION_EXCLUDE ) );
				//assume all ions are infinitely large from the edge, of unknown (default ion type), and multiplicity one (i.e. 0x01)
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of information array for the ions failed !" << "\n";
				return false;
			}
		}

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		dist_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;
}


bool distancerHdl::crop_reconstruction_to_analysis_window()
{
	double tic = MPI_Wtime();

	bool roi_has_accepted_ions = false;
	//assume first we work with the entire reconstruction, hence the ions.ionifo.mask1 is ANALYZE_YES for all
	//successively remove points from the analysis window that meet or not certain properties, like position, associated iontype, multiplicity, etc.
	if ( ConfigDistancer::WindowingMethod == ENTIRE_DATASET ) {
		apply_all_filter();
	}
	else if ( ConfigDistancer::WindowingMethod == UNION_OF_PRIMITIVES ) { //##MK::debug implementation
		apply_none_filter();

		//define which ions to include as a union of different geometric primitives
		apply_roi_ensemble_accept_filter( CG_SPHERE, ConfigDistancer::WindowingSpheres );
		apply_roi_ensemble_accept_filter( CG_CYLINDER, ConfigDistancer::WindowingCylinders );
		apply_roi_ensemble_accept_filter( CG_CUBOID, ConfigDistancer::WindowingCuboids );
	}
	else if ( ConfigDistancer::WindowingMethod == BITMASKED_POINTS ) {
		cerr << "Region-of-interest filtering mode BITMASKED_POINTS is not implemented yet!" << "\n";
		return roi_has_accepted_ions;
	}
	else {
		cerr << "Facing a yet unimplemented region-of-interest filtering mode !" << "\n";
		return roi_has_accepted_ions;
	}

	if ( ConfigDistancer::LinearSubSamplingRange.min > 0 ||
			ConfigDistancer::LinearSubSamplingRange.incr > 1 ||
				ConfigDistancer::LinearSubSamplingRange.max < ions.ionpp3.size() ) {
		//execute only when we can really restrict something
		apply_linear_subsampling_filter( ConfigDistancer::LinearSubSamplingRange );
	}
	else {
		//ignore this filter as it will have no effect
	}

	if ( ConfigDistancer::IontypeFilter.is_whitelist == true ) {
		apply_iontype_remove_filter( ConfigDistancer::IontypeFilter.candidates, false );
	}
	else if ( ConfigDistancer::IontypeFilter.is_blacklist == true ) {
		apply_iontype_remove_filter( ConfigDistancer::IontypeFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	if ( ConfigDistancer::HitMultiplicityFilter.is_whitelist == true ) {
		apply_multiplicity_remove_filter( ConfigDistancer::HitMultiplicityFilter.candidates, false );
	}
	else if ( ConfigDistancer::HitMultiplicityFilter.is_blacklist == true ) {
		apply_multiplicity_remove_filter( ConfigDistancer::HitMultiplicityFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	check_filter();

	//define at least two window classes, ions are assigned a class,
	//window class 0x00 ions are discarded, ions of all other classes are considered
	window = vector<unsigned char>( ions.ionpp3.size(), WINDOW_ION_EXCLUDE );
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			window[i] = ANALYZE_YES; //WINDOW_ION_INCLUDE_DEFAULT_CLASS;
			roi_has_accepted_ions = true;
		}
		//else, nothing to do, window[i] already WINDOW_ION_EXCLUDE
	}

	double toc = MPI_Wtime();
	memsnapshot mm = dist_tictoc.get_memoryconsumption();
	dist_tictoc.prof_elpsdtime_and_mem( "CropReconstructionToAnalysisWindow", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return roi_has_accepted_ions;
}


bool distancerHdl::compute_query_distances( distancing_task const & info )
{
	double tic = omp_get_wtime();
	double toc = tic;
	memsnapshot mm = memsnapshot();

	//identify_ioncloud_dimensions() respecting the window
	ions.aabb = aabb3d();
	for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			ions.aabb.possibly_enlarge_me( ions.ionpp3[i] );
		}
	}
	ions.aabb.scale();

	//need a bounding box to both triangles and ion positions
	aabb3d tri_xyz_box = aabb3d();
	tri_xyz_box.possibly_enlarge_me( ions.aabb );
	tri_xyz_box.possibly_enlarge_me( bvh.window );
	tri_xyz_box.scale();
	cout << "tri_xyz_box" << "\n";
	cout << tri_xyz_box << "\n";

	//define a buffer for the querydistances
	//##MK::size can be optimized to account only for ions used instead of all
	try {
		querydist = vector<apt_real>( ions.ionpp3.size(), RMX );
	}
	catch (bad_alloc &croak)
	{
		cerr << "Allocation error for querydist !" << "\n"; return false;
	}

	//we have a 3D point cloud with Np members embedded in or on the facets of an arbitrarily sized and shaped AABB
	//we have in addition an ensemble of Ntri triangles floating in 3D space confined to the volume and/or facets of
	//an arbitrarily sized and shaped AABB
	//because in APT we know where these triangles come from we can make certain assumptions:
	//	namely triangles come e.g. from a set of 3D triangulated meshes for microstructural features or
	//  an alpha complex representing a model of the edge of the dataset
	//	alternatively triangles can come from an isosurface computation of a voxelization of the AABB
	//	on which triangles were extracted with a Marching cube-type algorithm, i.e. a triangle soup
	//we know that the triangles are not infinitely far away from the closest member of the point cloud
	//realizing this teaches us there are different use cases to consider when identifying useful heuristics that cut
	//costs for point-to-triangle analyses more efficiently than do other heuristics
	//1. most naive would be to test each member of the point cloud against each member of the triangle ensemble i.e. O(Np*Ntri)
	//		when it holds that Np >> Ntri this can be the most efficient strategy
	//		especially when the triangles sit in the (same) corner of the triangle AABB
	//			but this is a seldom case
	//2. better we could voxelize first the AABB culling the point cloud and compute distances for barycenters of cubic voxels
	//		then we can use these typically shorter distances plus a safety distance of e.g. half the length
	//		of the space diagonal of the cube voxel + eps that there needs to be the closest triangle for any point in that voxel
	//	one could use an iterating algorithm that reduces successively the voxel edge length and redo the computation
	//  possibly making use of already computed distances to narrow down better query estimates
	//	the smaller the voxel are the more voxels we have to compute for querying distances
	//	so query prep costs increase but once such query value is used, for fewer and fewer points in the voxel
    //  at which point this becomes again an almost every voxel barycenter against every triangle algorithm situation

	//when we want to compute the distance only within a skin of length dskin to the triangle set, it suffices to probe whether there is a
	//triangle in the radius skin-ball to the point, if so evaluate the shortest distance, if not set dskin as the minimum distance lower bound of the actual distance
	//	this is in fact an in-place filtering of the point-to-triangle distances

	//##MK::start for debugging purposes with half the AABB diagonal
	//MK::needs to use the AABB from the voxelization used for the Marching cubes, not the AABB to the point cloud because
	//there guard zones (paraprobe at least uses such) may allow that triangles extend (slightly) beyond the tightly fitting AABB of the point cloud

	apt_real Rmodel_naive = RMX;
	apt_real Rmodel_aabb = (sqrt(3.0) / 2.0) * sqrt(SQR(tri_xyz_box.xsz)+SQR(tri_xyz_box.ysz)+SQR(tri_xyz_box.zsz));
			//SQR(bvh.window.xmx-bvh.window.xmi) +
			//SQR(bvh.window.ymx-bvh.window.ymi) +
			//SQR(bvh.window.zmx-bvh.window.zmi) ); //half room diagonal of bounding box

	apt_real Rmodel_skin = Rmodel_aabb;
	if ( ConfigDistancer::DistancingMethod == DISTANCING_SKIN ) {
		Rmodel_skin = fmin( Rmodel_aabb, ConfigDistancer::DistancingRadiusMax );
	}

	//##MK::use either or

	//heuristic 1 use the same value for each point
	if ( ConfigDistancer::DistancingMethod == DISTANCING_SKIN ) {
		apt_real R = fmin( fmin( Rmodel_naive, Rmodel_aabb ), Rmodel_skin );
		apt_real RSQR = SQR(R);
		for ( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
			querydist[i] = RSQR + ConfigDistancer::ItrvQryDistEpsilon;
		}
		toc = omp_get_wtime();
		mm = memsnapshot();
		dist_tictoc.prof_elpsdtime_and_mem( "UseSkinDistIfoTaskID" + to_string(info.taskid), APT_XX, APT_IS_SEQ, mm, tic, toc);
	} //##MK::if skin becomes thick use better heuristic 2, details depend on triangle ensemble arrangement of points and values for Rmodel
	else {
		//heuristic 2 use an iteratively refined coarse distance for each point
		//bounding box to both the triangles and the ion positions

		if ( distifo_tree.previous != NULL ) {
			delete distifo_tree.previous; distifo_tree.previous = NULL;
		}
		if ( distifo_tree.current != NULL ) {
			delete distifo_tree.current; distifo_tree.current = NULL;
		}
		apt_real Rmodel_iter = Rmodel_aabb;
		apt_real Ledge_iter = ConfigDistancer::AdvDistancingBinWidthRange.max;

		toc = omp_get_wtime();
		mm = memsnapshot();
		dist_tictoc.prof_elpsdtime_and_mem( "InitItrvQryDistIfoTaskID" + to_string(info.taskid), APT_XX, APT_IS_SEQ, mm, tic, toc);
		tic = omp_get_wtime();
		apt_uint iterID = 0;
		do {
			cout << "Iterative querying of distances Rmodel_iter " << Rmodel_iter << " Ledge_iter " << Ledge_iter << "\n";
			distifo_tree.current = new itrvQryDistIfoHdl();
			distifo_tree.current->defaultifo = Rmodel_iter;
			distifo_tree.current->init_voxelization( tri_xyz_box, Ledge_iter );
			//MK::be careful with the above tri_xyz_box, if triangles are very far away from point cloud voxelized aabb3d will explode in size, different heuristics needed for this case
			distifo_tree.current->point_cloud_voxelize( ions.ionpp3, ions.ionifo ); //##MK::take into account windowing at some point

			distifo_tree.compute_distances( bvh );

			if ( distifo_tree.previous != NULL ) {
				delete distifo_tree.previous;
				distifo_tree.previous = NULL;
			}
			//hand pointer to tree with current distances over to use in the next iteration as previous
			distifo_tree.previous = distifo_tree.current;
			distifo_tree.current = NULL;

			Ledge_iter -= ConfigDistancer::AdvDistancingBinWidthRange.incr;
			cout << "About to start next iteration with Ledge_iter " << Ledge_iter << "\n";

			toc = omp_get_wtime();
			memsnapshot mm = memsnapshot();
			dist_tictoc.prof_elpsdtime_and_mem(
					"RunItrvQryDistIfoTaskID" + to_string(info.taskid) + "Iter" + to_string(iterID), APT_XX, APT_IS_PAR, mm, tic, toc);
			tic = omp_get_wtime();

			iterID++;
		} while ( Ledge_iter > ConfigDistancer::AdvDistancingBinWidthRange.min && Ledge_iter > ConfigDistancer::ItrvQryDistIfoHdlBinWidthMin );

/*
		distifo_tree.previous = NULL; //##MK::implement iterative refinement loop
		distifo_tree.current = new itrvQryDistIfoHdl();
		distifo_tree.current->defaultifo = Rmodel_aabb;

		apt_real current_binning = ConfigDistancer::AdvDistancingBinWidth.min; //nanometer
		distifo_tree.current->init_voxelization( bvh.window, current_binning );
		distifo_tree.current->point_cloud_voxelize( xyz ); //##MK::take into account windowing at some point
		distifo_tree.current->compute_distances( bvh );
*/
		//use results of the querying distance analysis to assign each ion a better, i.e. shorter query radius to work with and reduce point-triangle distancing costs
		/*
		apt_real Raddition = sqrt(3.0)/2.0 * distifo_tree.previous->vxlgrid.width;
		apt_real Raddition = sqrt(3.0)/2.0 * distifo_tree.current->vxlgrid.width;
		*/
		//##MK::edgelength of last iteration
		//MK::every point in the vxl is at most Raddition further away from the point of shortest distance between barycenter of vxl and triangle
		//think about taking the last voxel grid from the iterative coarse distancing computations and check
		//in which voxel each ion is such that we know how far the ion is at most from a close triangle, this is a lookup table approach
		for( size_t ionID = 0; ionID < ions.ionpp3.size(); ionID++ ) {
			if ( ions.ionifo[ionID].mask1 == ANALYZE_YES ) {
				//in which voxel, i.e. region of the dataset is the ion currently?
				size_t nxyz = distifo_tree.previous->gridinfo.where_xyz( ions.ionpp3[ionID] );
				//what is the corresponding query distance
				querydist[ionID] = distifo_tree.previous->get_query_distifo( nxyz ); // + Raddition + 10.0*EPSILON;
			}
		}

		//release no longer used coarse distance querying information
		if ( distifo_tree.previous != NULL ) {
			delete distifo_tree.previous; distifo_tree.previous = NULL;
		}
		if ( distifo_tree.current != NULL ) {
			delete distifo_tree.current; distifo_tree.current = NULL;
		}
		toc = omp_get_wtime();
		mm = memsnapshot();
		dist_tictoc.prof_elpsdtime_and_mem( "AcceptItrvQryDistIfoTaskID" + to_string(info.taskid), APT_XX, APT_IS_SEQ, mm, tic, toc);
	}
	return true;
}


bool distancerHdl::compute_exact_distances( distancing_task const & info )
{
	double tic = MPI_Wtime();

	if ( get_nranks() > SINGLEPROCESS ) {
		cerr << "Algorithm is currently not implemented for more than a single process !" << "\n";
		return false;
	}

	try {
		//distances are significant only at root
		distances = vector<apt_real>( ions.ionpp3.size(), RMX );
		//set all distances to a default value RMX, which allows to track cases of problems etc.
		triangle_id = vector<apt_uint>( ions.ionpp3.size(), UMX );
		//set all closest triangles to a default value UMX, which allows to track cases of problems etc.
	}
	catch (bad_alloc &croak) {
		cerr << "Rank " << get_myrank() << " allocation of distances and triangle_id arrays failed !" << "\n";
		return false;
	}

	cout << "Rank " << get_myrank() << " now executing analytical point-to-triangle distancing ..." << "\n";

	//cooperative computation of distances region by region
	#pragma omp parallel
	{
		/*
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		*/

		//threadlocal buffering of results to reduce time spent in pragma omp criticals

		//MK::the global IDs of the ions are stored interleaved within the ionpp3 object, this is the beauty
		//we will have no further cache-misses associated with finding what is the global ion while processing geometry

		double ctic = omp_get_wtime();

		vector<dinfo> myres;

		//MK::experiences shows that schedule(static,1) caused substantial load imbalance
		#pragma omp for schedule(dynamic,1) nowait
		for( size_t ionID = 0; ionID < ions.ionpp3.size(); ionID++ ) {

			if ( ions.ionifo[ionID].mask1 == ANALYZE_YES ) {
#ifdef MONITOR_TRIANGLE_COUNT
				double cctic = omp_get_wtime();
#endif

				p3d me = ions.ionpp3[ionID];
				apt_real RR = querydist[ionID];
				apt_real RRSQR = SQR(RR);

				//prune most triangles of the surface by utilizing bounded volume hierarchy (BVH) Rtree
				hedgesAABB e_aabb(
						trpl(me.x-RR-EPSILON, me.y-RR-EPSILON, me.z-RR-EPSILON),
						trpl(me.x+RR+EPSILON, me.y+RR+EPSILON, me.z+RR+EPSILON) );

				vector<apt_uint> mycand = bvh.kauri->query( e_aabb );

				//CPU-computation of analytical distances between point me and set of candidate triangles referred to my mycand
				if ( mycand.size() > 0 ) { //most likely case, unless we work with DISTANCING_SKIN and small DistancingRadiusMax
					//size_t ntests = 0;
					//size_t ntri = mycand.size();
					//size_t tr = 0;
					apt_real CurrentBestSqr = RRSQR;
					apt_uint CurrentBestTriID = UMX;
					apt_real CurrentValueSqr = RRSQR;
					for( auto cdt = mycand.begin(); cdt != mycand.end(); cdt++ ) {

						//analytical shortest (Euclidean distance) between point me and closest point on triangle with ID *cdt
						CurrentValueSqr = closestPointOnTriangle( triangles.at(*cdt), me );
						if ( CurrentValueSqr > CurrentBestSqr ) { //most likely not resetting the minimum
							continue;
						}
						else { //what we want as soon as possible, the shortest distance for all triangle candidates
							CurrentBestSqr = CurrentValueSqr;
							CurrentBestTriID = *cdt;
						}
					}
					//myres.push_back( pair<size_t,apt_real>( ionID, sqrt(CurrentBestSqr) ) ); //storage in thread-local memory, without immediate sync
					apt_uint ionid = ( ionID < UMX ) ? (apt_uint) ionID : UMX;
	#ifdef MONITOR_TRIANGLE_COUNT
					double cctoc = omp_get_wtime();
					apt_uint wkld = ( mycand.size() < UMX ) ? (apt_uint) mycand.size() : UMX;
					myres.push_back( dinfo( cctoc-cctic, wkld, ionid,
						CurrentBestTriID, sqrt(CurrentBestSqr) ) );
	#else
					myres.push_back( dinfo( ionid, CurrentBestTriID, sqrt(CurrentBestSqr) ) );
	#endif
					/*
					#pragma omp critical
					{
						cout << "Thread " << mt << " out of " << nt << " vxlID " << ionID << " x,y,z " << me.x << ";" << me.y << ";" << me.z
								<< " myres.back().ionID " << myres.back().ionID << " myres.back().d " << myres.back().d << " myres.back().wkld " << myres.back().wkld << "\n";
					}
					*/
				}
				else {
					//myres.push_back( pair<size_t,apt_real>( ionID, RR ) ); //if none set maximum, or
					apt_uint ionid = ( ionID < UMX ) ? (apt_uint) ionID : UMX;
	#ifdef MONITOR_TRIANGLE_COUNT
					double cctoc = omp_get_wtime();
					myres.push_back( dinfo( cctoc-cctic, 0, ionid, UMX, RR ) );
	#else
					myres.push_back( dinfo( ionid, UMX, RR ) );
	#endif
					//mywkl.push_back( MPI_WorkAndID( 0, IonID) );
				}

				/*
				//##MK::BEGIN DEBUG PROGRESS BAR
				if ( mt == MASTER ) {
					if ( ionID % 1000 != 0 ) {
						continue;
					}
					else {
						cout << "Master thread arrived at " << ionID << "\n";
					}
				}
				//##MK::END DEBUG PROGRESS BAR
				*/
			}
		} //MK::we use a nowait clause to avoid the implicit barrier of the pragma-omp-for construct

		//thereby completed threads can already fill in the values into MPI process level results buffers

		//done with all thread regions, compute square root only once in parallel and fuse results into process buffer
		#pragma omp critical
		{
			//threads of Master process store distances immediately in order of I/O within distances
			for( auto jt = myres.begin(); jt != myres.end(); jt++ ) {
				//distances[jt->first] = jt->second; //finally here we go from the more efficient handling of SQR(d) to d
				distances[jt->ionID] = jt->d;
				triangle_id[jt->ionID] = jt->triID;
			}
		}

		double ctoc = omp_get_wtime();
		#pragma omp master
		{
			cout << "Rank " << get_myrank() << " cooperative distancing for ions was successful took " << (ctoc-ctic) << " seconds" << "\n";
		} //no barrier at the end of omp master

	} //implicit barrier of pragma-omp-parallel end of parallel region

	//##MK::technically from now on the BVH is no longer required

	//##MK::normal projection, also parallelize at some point ##MK
	//and improve caching, e.g. bundle distances and triangle_id,
	//and triangle and triangles_signed_normal
	if ( distances.size() == triangle_id.size() ) {
		if ( ConfigDistancer::DistancingMethod == DISTANCING_COMPLETE &&
				triangles_signed_normals.size() == triangles.size() ) {
			for( size_t ionID = 0; ionID < ions.ionpp3.size(); ionID++ ) {
				if ( ions.ionifo[ionID].mask1 == ANALYZE_YES ) {
					p3d me = ions.ionpp3[ionID];
					apt_uint tri_id = triangle_id.at(ionID);
					tri3d tr = triangles.at(tri_id);
					p3d bc = tr.barycenter();
					p3d me_bc = p3d( me.x - bc.x, me.y - bc.y, me.z - bc.z );
					p3d tr_nrm = triangles_signed_normals.at(tri_id);

					apt_real projection = dot( me_bc, tr_nrm );
					if ( projection < MYZERO ) { //negative distance if on the other side of the normal
						distances.at(ionID) *= -MYONE;
					}
				}
			}
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = dist_tictoc.get_memoryconsumption();
	dist_tictoc.prof_elpsdtime_and_mem( "ExactDistancingTaskID" + to_string(info.taskid), APT_XX, APT_IS_PAR, mm, tic, toc);

	cout << "Rank " << get_myrank() << " computing parallelized distance to surface completed in " << (toc-tic) << " seconds" << "\n";
	return true;
}


void distancerHdl::execute_distancing_workpackage()
{
	double tic = MPI_Wtime();
	double toc = tic;

	apt_uint taskID = 0;
	for( auto dst_tskit = ConfigDistancer::DistancingTasks.begin(); dst_tskit != ConfigDistancer::DistancingTasks.end(); dst_tskit++ ) {
		//computing distances to geometric primitives for millions of points and get the closest
		//of eventually millions of candidates is a costly task
		//therefore MPI processes share the processing for a task, ions are distributed across processes,
		//further distributed internally by OpenMP multithreading

		//dst_tskit->taskid = taskID;

		if ( get_nranks() > 1 ) {
			cerr << "Rank " << get_myrank() << " current implementation does not support multi-process parallelism for distance computations, use a single process for now !" << "\n";
			return;
		}

		//master loads the geometry data which defines the mesh/triangle set/object against which to distance the ions
		triangles = vector<tri3d>();
		triangles_signed_normals = vector<p3d>();
		triangles_patchid = vector<apt_uint>();

		distances = vector<apt_real>();

		int localhealth = 1;
		int globalhealth = 0;
		if ( get_myrank() == MASTER ) {

			//##MK::I/O on the triangles

			for( auto it = dst_tskit->trimeshes.begin(); it != dst_tskit->trimeshes.end(); it++ ) {
				if ( it->h5fn != "" && it->dsnm_verts != "" && it->dsnm_facets != "" ) {

					HdfFiveSeqHdl h5r = HdfFiveSeqHdl( it->h5fn );
					ioAttributes anno = ioAttributes();
					string grpnm = "";
					string dsnm = "";

					/*
					apt_uint n_triangles = 0;
					*/
					vector<apt_real> real;
					if ( h5r.nexus_read( it->dsnm_verts, real ) != MYHDF5_SUCCESS ) {
						cerr << "Reading " << it->dsnm_verts << " failed !" << "\n"; return;
					}
					vector<apt_uint> uint;
					if ( h5r.nexus_read( it->dsnm_facets, uint ) != MYHDF5_SUCCESS ) {
						cerr << "Reading " << it->dsnm_facets << " failed !" << "\n"; return;
					}

					if ( (real.size() % ((size_t) 3)) != 0 || (uint.size() % ((size_t) 3)) != 0 ) {
						cerr << "Input arrays " << it->dsnm_verts << " and/or " << it->dsnm_facets << " need to have a 3-multiple number of entries !" << "¿n";
						return;
					}

					/*
					n_triangles = uint.size() / 3;
					*/

					vector<apt_uint> uint_ids;
					set<apt_uint> whitelist;
					set<apt_uint> blacklist;
					/*
					whitelist.insert( 47 );
					whitelist.insert( 598 );
					whitelist.insert( 1355 );
					*/

					if ( it->dsnm_patchid != "" ) {
						if ( h5r.nexus_read( it->dsnm_patchid, uint_ids ) != MYHDF5_SUCCESS ) {
							cerr << "Reading " << it->dsnm_patchid << " failed !" << "\n"; return;
						}

						if ( uint.size() / 3 != uint_ids.size() ) {
							cerr << "Input array " << it->dsnm_patchid << " is not the same length as the number of triangle faces !" << "\n"; return;
						}

						//instantiate patch filter
						cerr << "trimeshes patch_identifier filter currently works only for whitelists or no filter!" << "\n";
						if ( it->patch_identifier_filter.is_whitelist == true ) {
							for( auto kt = it->patch_identifier_filter.candidates.begin(); kt != it->patch_identifier_filter.candidates.end(); kt++ ) {
								whitelist.insert( *kt );
							}
						}
						cout << "patchid_whitelist will be used" << "\n";
						cout << "patchid_whitelist.size() " << whitelist.size() << "\n";
					}
					else {
						cout << "Neither a patchid whitelist nor blacklist will be used, all triangles from this trimeshes will be added to the complex!" << "\n";
						uint_ids = vector<apt_uint>( uint.size() / 3, UNDEFINED_PATCHID );
					}

					vector<apt_real> real_nrm;
					if ( it->dsnm_fnormals != "" ) {
						if ( h5r.nexus_read( it->dsnm_fnormals, real_nrm ) != MYHDF5_SUCCESS ) {
							cerr << "Reading " << it->dsnm_fnormals << " failed !" << "\n"; return;
						}

						if ( (real_nrm.size()/3 != uint.size()/3) || (real_nrm.size() % ((size_t) 3)) != 0 ) {
							cerr << "Input array " << it->dsnm_fnormals << " is not the same length as the number of triangle faces !" << "\n"; return;
						}
					}

					//handle match filter
					for( size_t fct = 0; fct < uint_ids.size(); fct++ ) { //works for multiple meshes without working with offsets because
						bool consider = false;
						if ( uint_ids[fct] == UNDEFINED_PATCHID ) { //most likely case, no filtering
							consider = true;
						}
						else {
							if ( whitelist.find( uint_ids[fct] ) != whitelist.end() ) {
								consider = true;
							}
						}

						if ( consider == true ) {
							triangles_patchid.push_back( uint_ids[fct] );

							//local vertex indices are used to translate into point coordinate specified triangles
							apt_uint v1 = uint[3*fct+0];
							apt_uint v2 = uint[3*fct+1];
							apt_uint v3 = uint[3*fct+2];
							triangles.push_back( tri3d(
									real[3*v1+0], real[3*v1+1], real[3*v1+2],
									real[3*v2+0], real[3*v2+1], real[3*v2+2],
									real[3*v3+0], real[3*v3+1], real[3*v3+2] ) );

							if ( real_nrm.size() > 0 ) { //if normal information is available
								triangles_signed_normals.push_back(
										p3d( real_nrm[3*fct+0], real_nrm[3*fct+1], real_nrm[3*fct+2] ) );
							}
						}
					}
					cout << "triangles.size() " << triangles.size() << "\n";
					cout << "triangles_signed_normals.size() " << triangles_signed_normals.size() << "\n";
					cout << "triangles_patchid.size() " << triangles_patchid.size() << "\n";
					//ones the list of triangles is filtering the resulting IDs will no longer be consecutive !

					//##MK::AABB to triangles might be different from AABB to point cloud
				}
				else {
					cerr << "Rank MASTER detected that an unsupported formatting of the source file with geometry data references was provided so we skip this task !" << "\n"; localhealth = 0;
					cerr << "Rank MASTER detected that for distancing task " << taskID << " the source file with the geometry data was not defined so we skip this task !" << "\n"; localhealth = 0;
				}
			} //load next collection of triangles to add to triangle ensemble against which to compute ion-to-triangle distances
		}

		//##MK::do we have all processes on board?
		//##MK::master broadcasts geometry information within triangles to slaves

		if ( triangles.size() > 0 ) {

			if ( bvh.build_triangle_tree_and_aabb3d( triangles ) == true ) {
				cout << "Rank " << get_myrank() << " built a triangle tree" << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " failed to build a triangle tree !" << "\n";
				taskID++; continue;
			}

			//compute individual query distances to reduce number of triangle analyses per point using different heuristics
			if ( compute_query_distances( *dst_tskit ) == true ) {
				cout << "Rank " << get_myrank() << " estimated useful query distances to cut numerical costs in subsequent point-triangle distancing" << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " failed in estimating useful query distances for cutting numerical costs in subsequent point-triangle distancing !" << "\n";
				taskID++; continue;
			}

			//compute distances for the ions using the querying information from the distance bins to sequentially cut point-triangle tests
			if ( compute_exact_distances( *dst_tskit ) == true ) {
				cout << "Rank " << get_myrank() << " computed analytical point to closest triangle distances" << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " failed in computing analytical point to closest triangle distances !" << "\n";
				taskID++; continue;
			}

			//clean the tree
			bvh.chop_triangle_tree();

			//wait until all processes have finished their workload
			MPI_Barrier(MPI_COMM_WORLD);
			globalhealth = 0;
			MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
			if ( globalhealth != get_nranks() ) {
				cerr << "Rank " << get_myrank() << " has recognized that not all processes were successful when computing their distance portions so we skip this task !" << "\n";
				taskID++; continue;
			}

			//consolidate results
			//##MK::implement multi-process results consolidation

			//master writes results to H5
			if ( get_myrank() == MASTER ) {

				if ( write_results_distancing_task_h5( *dst_tskit ) == MYHDF5_SUCCESS ) {
					//
				}
				else {
					cerr << "Writing distances for distancing task " << taskID << " failed !" << "\n"; localhealth = 0;
				}
			}

			//##MK::do we have all processes on board?
		}
		else {
			cerr << "Rank " << get_myrank() << " detected there are no triangles so we skip this task !" << "\n";
		}

		taskID++;

	} //next task

	toc = MPI_Wtime();
	memsnapshot mm = dist_tictoc.get_memoryconsumption();
	dist_tictoc.prof_elpsdtime_and_mem( "DistancingWorkpackage", APT_XX, APT_IS_PAR, mm, tic, toc);
}


bool distancerHdl::write_results_distancing_task_h5( distancing_task const & info )
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	xdmfBaseHdl xml;
	xml.add_grid_uniform( "exact distances" );

	apt_uint entry_id = 1;
	apt_uint proc_id = info.taskid + 1;  //##MK::better change task_id higher up in the calling hierarchy
	grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle";
	anno = ioAttributes();
	anno.add( "NX_class", string( "NXapm_paraprobe_tool_results" ) );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	//topology is for visualization support
	dsnm = grpnm + "/point_identifier";
	vector<apt_uint> uint = vector<apt_uint>( (1+1+1) * ions.ionpp3.size(), 1 ); //one triplet for each point
	//first entry is the XDMF primitive type key (here 1 polyvertex),
	//second entry is the number of primitives per point (here 1)
	//third entry is the ion_id
	for( size_t ionID = 0; ionID < ions.ionpp3.size(); ionID++ ) {
		uint[3*ionID+2] = ionID;
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	vector<size_t> dims = { ions.ionpp3.size()*(1+1+1), 1 }; //xdmf keyword, xdmf number of vertices, one vertex IDs for each ion position
	xml.add_topology_mixed( ions.ionpp3.size(), dims, H5_U32, strip_path_prefix(ConfigShared::OutputfileName) + ":" + dsnm );
	//##MK::H5_U64 in case that apt_uint is unsigned long !!

	dims = { ions.ionpp3.size(), 3 };
	xml.add_geometry_xyz( dims, H5_F32, strip_path_prefix(ConfigDistancer::InputfileDataset)
			+ ":" + ConfigDistancer::ReconstructionDatasetName );

	dsnm = grpnm + "/distance";
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
			dsnm,
			io_info({distances.size()}, {distances.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			distances,
			anno ) != MYHDF5_SUCCESS ) { return false; }

	dims = { ions.ionpp3.size(), 1 };
	xml.add_attribute_scalar( "ion-to-edge distance", "Node", dims, H5_F32, strip_path_prefix(ConfigShared::OutputfileName) + ":" + dsnm );

	dsnm = grpnm + "/triangle_identifier";
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({triangle_id.size()}, {triangle_id.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			triangle_id,
			anno ) != MYHDF5_SUCCESS ) { return false; }

	dims = { ions.ionpp3.size(), 1 };
	xml.add_attribute_scalar( "closest triangle id", "Node", dims, H5_U32, strip_path_prefix(ConfigShared::OutputfileName) + ":" + dsnm );

	const string xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id)
			+ ".TaskId." + to_string(proc_id) + ".DistancePntToSurf.xdmf";
	xml.write( xmlfn );

	//window
	grpnm = "/entry" + to_string(entry_id) + "/point_to_triangle/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_boolean_filter_mask") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) ions.ionifo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/bitdepth";
	apt_uint bit_depth = sizeof(unsigned char) * 8;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, bit_depth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/mask";
	/*
	vector<unsigned char> u08_wdw = vector<unsigned char>( ions.ionifo.size(), ANALYZE_YES );
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			u08_wdw[i] = ANALYZE_YES;
		}
	}
	*/
	dsnm = grpnm + "/mask";
	//use window in-place
	vector<unsigned char> u08_bitfield;
	BitPacker pack_bits;
	pack_bits.uint8_to_bitpacked_uint8( window, u08_bitfield );
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) { return false; }

	return true;
}
