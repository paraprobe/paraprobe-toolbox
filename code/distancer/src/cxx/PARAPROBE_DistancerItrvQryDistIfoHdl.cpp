/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_DistancerItrvQryDistIfoHdl.h"


itrvQryDistIfoHdl::itrvQryDistIfoHdl()
{
	owner = NULL;
	gridinfo = voxelgrid();
	querycnt = vector<apt_uint>();
	queryifo = vector<apt_real>();
	defaultifo = RMX;
}


itrvQryDistIfoHdl::~itrvQryDistIfoHdl()
{
	//MK::do not delete owner, only a pointer to reuse triangle data !
}


void itrvQryDistIfoHdl::init_voxelization( aabb3d const & box, const apt_real edgelength )
{
	gridinfo = voxelgrid( box, edgelength, 1 ); //1 voxel guard zone on either side

	try {
		querycnt = vector<apt_uint>( (size_t) gridinfo.nxyz, 0 );
		queryifo = vector<apt_real>( (size_t) gridinfo.nxyz, defaultifo );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error for querycnt and/or queryifo !" << "\n";
	}
}


void itrvQryDistIfoHdl::point_cloud_voxelize( vector<p3d> const & pts, vector<p3dinfo> const & ifo )
{
	for( auto it = pts.begin(); it != pts.end(); it++ ) {
		if ( ifo[it-pts.begin()].mask1 == ANALYZE_YES ) {
			size_t nxyz = gridinfo.where_xyz( *it );
			if ( nxyz != -1 ) {
				querycnt[nxyz]++;
			}
			else {
				cerr << "Point " << *it << " was unexpectedly not detected inside the to be voxelized box !" << "\n";
			}
		}
	}
	cout << "Point cloud voxelization performed" << "\n";
}


apt_real itrvQryDistIfoHdl::get_query_distifo( const size_t vxlID )
{
	size_t nxyz = vxlID;
	if ( nxyz < queryifo.size() ) {
		return queryifo[nxyz];
	}
	cerr << "vxlID " << vxlID << " using default distance unexpectedly !" << "\n";
	return defaultifo;
}


itrvQryTreeHdl::itrvQryTreeHdl()
{
	previous = NULL;
	current = NULL;
}


itrvQryTreeHdl::~itrvQryTreeHdl()
{
	if ( previous != NULL ) {
		delete previous;
		previous = NULL;
	}
	if ( current != NULL ) {
		delete current;
		current = NULL;
	}
}


void itrvQryTreeHdl::compute_distances( tritreeHdl const & tri3dbvh )
{
	double ctic = omp_get_wtime();

	//multi-threaded computation of query distances
	#pragma omp parallel
	{
		/*
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		*/

		//threadlocal buffering of results to reduce time spent in pragma omp criticals
		//vector<pair<size_t,apt_real>> myres;
		vector<dinfo> myres;

		//MK::experiences shows that schedule(static,1) caused substantial load imbalance
		apt_int nxyz = current->gridinfo.nxyz;
		apt_int nxy = current->gridinfo.nxy;
		apt_int nx = current->gridinfo.nx;
		apt_real width = current->gridinfo.dx; //needs to be cubic voxel !
		apt_real difo = current->defaultifo;
		apt_real xmi = current->gridinfo.aabb.xmi;
		apt_real ymi = current->gridinfo.aabb.ymi;
		apt_real zmi = current->gridinfo.aabb.zmi;
		apt_real Raddition = RMX;
		apt_real safety = ConfigDistancer::ItrvQryDistEpsilon;
		if ( previous != NULL ) {
			//a point can at most be further away than half the room-diagonal of an additional voxel
			Raddition = sqrt(3.0)/2.0 * previous->gridinfo.dx + safety;
		}
		else {
			Raddition = sqrt(3.0)/2.0 * current->gridinfo.dx + safety;
		}

		#pragma omp for schedule(dynamic,1) nowait
		for( apt_int i = 0; i < nxyz; i++ ) { //##MK::change the grid addressing to use rather unsigned int
#ifdef MONITOR_TRIANGLE_COUNT
			double cctic = omp_get_wtime();
#endif
			apt_int xyz = i;
			apt_int z = xyz / nxy;
			apt_int rem = xyz - z*nxy;
			apt_int y = rem / nx;
			apt_int x = rem - y*nx;

			p3d barycenter = p3d( 	xmi + (0.5+(apt_real) x)*width,
									ymi + (0.5+(apt_real) y)*width,
									zmi + (0.5+(apt_real) z)*width );

			//use information from previous distance computation if available
			apt_real Rprev = difo;
			if ( previous != NULL ) { //MK::is this not the first iteration, i.e. we have already a better than naive estimate of the coarse distance
				size_t xyz_prev = previous->gridinfo.where_xyz( barycenter );
				Rprev = previous->get_query_distifo( xyz_prev );
			}

			apt_real RR = Rprev + safety;
			apt_real RRSQR = SQR(RR);

			//prune most triangles by utilizing bounded volume hierarchy (BVH) Rtree
			hedgesAABB e_aabb( trpl(barycenter.x-RR-EPSILON, barycenter.y-RR-EPSILON, barycenter.z-RR-EPSILON),
								trpl(barycenter.x+RR+EPSILON, barycenter.y+RR+EPSILON, barycenter.z+RR+EPSILON) );

			vector<apt_uint> mycand = tri3dbvh.kauri->query( e_aabb );

			//CPU-computation of analytical distances between point me and set of candidate triangles referred to by my mycand
			if ( mycand.size() > 0 ) {

				//master thread uses GPU for distancing, slave threads use CPU for distancing
				/*
				apt_real CurrentBestSqr = cpu_computor.shortest_point_to_triangle_distance( tri3dbvh.tris, mycand );
				*/

				apt_real CurrentBestSqr = RRSQR;
				apt_real CurrentValueSqr = RRSQR;
				for( auto cdt = mycand.begin(); cdt != mycand.end(); cdt++ ) {
					//analytical shortest (Euclidean distance) between barycenter of voxel and closest point on triangle with ID *cdt
					CurrentValueSqr = closestPointOnTriangle( tri3dbvh.tris.at(*cdt), barycenter ); //##MK::replace at() by []
					if ( CurrentValueSqr > CurrentBestSqr ) { //most likely not resetting the minimum
						continue;
					}
					else { //what we want as soon as possible, the shortest distance for all triangle candidates
						CurrentBestSqr = CurrentValueSqr;
					}
				}
				apt_uint vxlid = ( i < UMX ) ? (apt_uint) i : UMX;
#ifdef MONITOR_TRIANGLE_COUNT
				double cctoc = omp_get_wtime();
				unsigned int wkld = ( mycand.size() < UMX ) ? (apt_uint) mycand.size() : UMX;
				//Raddition because all points in this vxl are at most sqrt(CurrentBestSqr) + ...
				// ... Raddition away from the closest point on the triangle for the barycenter
				myres.push_back( dinfo( cctoc-cctic, wkld, vxlid, sqrt(CurrentBestSqr) + Raddition ) );
#else
				myres.push_back( dinfo( vxlid, sqrt(CurrentBestSqr) + Raddition ) );
#endif
				//#pragma omp critical
				//{
				//	cout << "Thread " << mt << " out of " << nt << " vxlID " << vxlID << " x,y,z " << x << ";" << y << ";" << z
				//			<< " barycenter " << barycenter.x << ";" << barycenter.y << ";" << barycenter.z
				//				<< " Rprev " << Rprev << " wkld/mycand.size() " << myres.back().wkld << " ID " << myres.back().ionID
				//					<< " d " << myres.back().d << "\n";
				//}
			}
			else {
				apt_uint vxlid = ( i < UMX ) ? (apt_uint) i : UMX;
#ifdef MONITOR_TRIANGLE_COUNT
				double cctoc = omp_get_wtime();
				myres.push_back( dinfo( cctoc-cctic, 0, vxlid, RR + Raddition ) );
#else
				myres.push_back( dinfo( vxlid, RR + Raddition ) );
#endif
			}
		} //threads share processing of distances
		#pragma omp critical
		{
			for( auto jt = myres.begin(); jt != myres.end(); jt++ ) {
				//queryifo.at(jt->first) = jt->second;
				current->queryifo.at(jt->ionID) = jt->d;
			}
		}

	} //implicit barrier of pragma-omp-parallel end of parallel region

	double ctoc = omp_get_wtime();
	cout << "itrvQryDistIfoHdl computing_distances took " << (ctoc-ctic) << " seconds" << "\n";
}
