/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_DistancerTriTreeHdl.h"


tritreeHdl::tritreeHdl()
{
	kauri = NULL;
	tris = vector<tri3d>();
	window = aabb3d();
}
	

tritreeHdl::~tritreeHdl()
{
	if ( kauri != NULL ) {
		delete kauri; kauri = NULL;
	}
}


bool tritreeHdl::build_triangle_tree_and_aabb3d( vector<p3d> const & in_vrts, vector<tri3u> const in_fcts )
{
	//builds an AABBTree / RTree of triangles with respect to the global space the triangles occupy in $\mathcal{R}^3$
	if ( in_vrts.size() < 1 || in_fcts.size() < 1 ) {
		cerr << "tritreeHdl there are no triangles to build a tree from" << "\n";
		return false;
	}

	if ( in_fcts.size() >= static_cast<size_t>(UMX-1) ) {
		cerr << "tritreeHdl implementation currently supports at most UINT32MX-2 individual triangles !" << "\n";
		return false;
	}

	if ( kauri != NULL ) {
		cerr << "tritreeHdl a kauri BVH tree already exists !" << "\n";
		return false;
	}

	try {
		kauri = new class Tree( in_fcts.size() );
		tris.reserve( in_fcts.size() );
	}
	catch (bad_alloc &croak) {
		cerr << "tritreeHdl to allocate memory for the tree and triangle copy of the tritree BVH !" << "\n";
		return false;
	}

	//in-place building of tight axis-aligned bounding about all triangles
	window = aabb3d();
	apt_uint triID = 0;
	for ( auto it = in_fcts.begin(); it != in_fcts.end(); ++it ) {
		//##MK::fattening in lohedges/aabbcc code only for purpose of improving rebalancing. However, here the tree is static, so no fattening is required

		//window.possibly_enlarge_me( *it ); //avoid this here only because we would need to compute the aabb to the triangle two times
		apt_real x1 = in_vrts.at(it->v1).x;
		apt_real y1 = in_vrts[it->v1].y;
		apt_real z1 = in_vrts[it->v1].z;

		apt_real x2 = in_vrts.at(it->v2).x;
		apt_real y2 = in_vrts[it->v2].y;
		apt_real z2 = in_vrts[it->v2].z;

		apt_real x3 = in_vrts.at(it->v3).x;
		apt_real y3 = in_vrts[it->v3].y;
		apt_real z3 = in_vrts[it->v3].z;

		apt_real xmin = fmin( fmin(x1, x2), x3 );
		apt_real xmax = fmax( fmax(x1, x2), x3 );
		apt_real ymin = fmin( fmin(y1, y2), y3 );
		apt_real ymax = fmax( fmax(y1, y2), y3 );
		apt_real zmin = fmin( fmin(z1, z2), z3 );
		apt_real zmax = fmax( fmax(z1, z2), z3 );
		if ( xmin <= window.xmi )
			window.xmi = xmin;
		if ( xmax >= window.xmx )
			window.xmx = xmax;
		if ( ymin <= window.ymi )
			window.ymi = ymin;
		if ( ymax >= window.ymx )
			window.ymx = ymax;
		if ( zmin <= window.zmi )
			window.zmi = zmin;
		if ( zmax >= window.zmx )
			window.zmx = zmax;

		//https://github.com/lohedges/aabbcc
		//Here index is a key that is used to create a map between particles and nodes in the AABB tree.
		//The key should be unique to the particle and can take any value between 0 and std::numeric_limits<unsigned int>::max() - 1

		kauri->insertParticle( triID,
				trpl(xmin - EPSILON, ymin - EPSILON, zmin - EPSILON),
				trpl(xmax + EPSILON, ymax + EPSILON, zmax + EPSILON) );
		tris.push_back( tri3d( x1, y1, z1, x2, y2, z2, x3, y3, z3 ) );
		triID++;
	}

	window.scale();

	cout << "tritreeHdl kauri built successfully with " << tris.size() << " triangles and " << kauri->getNodeCount() << " AABB tree nodes" << "\n";
	return true;
}


bool tritreeHdl::build_triangle_tree_and_aabb3d( vector<tri3d> const & in )
{
	//builds an AABBTree / RTree of triangles with respect to the global space the triangles occupy in $\mathcal{R}^3$
	if ( in.size() < 1 ) {
		cerr << "tritreeHdl there are no triangles to build a tree from" << "\n";
		return false;
	}

	if ( in.size() >= static_cast<size_t>(UMX-1) ) {
		cerr << "tritreeHdl implementation currently supports at most UINT32MX-2 individual triangles !" << "\n";
		return false;
	}

	if ( kauri != NULL ) {
		cerr << "tritreeHdl a kauri BVH tree already exists !" << "\n";
		return false;
	}

	try {
		kauri = new class Tree( in.size() );
		tris.reserve( in.size() );
	}
	catch (bad_alloc &croak) {
		cerr << "tritreeHdl to allocate memory for the tree and triangle copy of the tritree BVH !" << "\n";
		return false;
	}

	//in-place building of tight axis-aligned bounding about all triangles
	window = aabb3d();
	apt_uint triID = 0;
	for ( auto it = in.begin(); it != in.end(); ++it ) {
		//##MK::fattening in lohedges/aabbcc code only for purpose of improving rebalancing. However, here the tree is static, so no fattening is required

		//window.possibly_enlarge_me( *it ); //avoid this here only because we would need to compute the aabb to the triangle two times

		apt_real xmin = fmin( fmin(it->x1, it->x2), it->x3 );
		apt_real xmax = fmax( fmax(it->x1, it->x2), it->x3 );
		apt_real ymin = fmin( fmin(it->y1, it->y2), it->y3 );
		apt_real ymax = fmax( fmax(it->y1, it->y2), it->y3 );
		apt_real zmin = fmin( fmin(it->z1, it->z2), it->z3 );
		apt_real zmax = fmax( fmax(it->z1, it->z2), it->z3 );
		if ( xmin <= window.xmi )
			window.xmi = xmin;
		if ( xmax >= window.xmx )
			window.xmx = xmax;
		if ( ymin <= window.ymi )
			window.ymi = ymin;
		if ( ymax >= window.ymx )
			window.ymx = ymax;
		if ( zmin <= window.zmi )
			window.zmi = zmin;
		if ( zmax >= window.zmx )
			window.zmx = zmax;

		//https://github.com/lohedges/aabbcc
		//Here index is a key that is used to create a map between particles and nodes in the AABB tree.
		//The key should be unique to the particle and can take any value between 0 and std::numeric_limits<unsigned int>::max() - 1

		kauri->insertParticle( triID,
				trpl(xmin - EPSILON, ymin - EPSILON, zmin - EPSILON),
				trpl(xmax + EPSILON, ymax + EPSILON, zmax + EPSILON) );
		tris.push_back( *it );
		triID++;
	}

	window.scale();

	cout << "tritreeHdl kauri built successfully with " << tris.size() << " triangles and " << kauri->getNodeCount() << " AABB tree nodes" << "\n";
	return true;
}


void tritreeHdl::chop_triangle_tree()
{
	if ( kauri != NULL ) {
		delete kauri;
		kauri = NULL;
	}
	tris = vector<tri3d>();
	window = aabb3d();
}
