/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_DISTANCER_TRITREEHDL_H__
#define __PARAPROBE_DISTANCER_TRITREEHDL_H__

#include "PARAPROBE_DistancerXDMF.h"

//wrapper to the aabb tree hdl for pack triangles into an AABB tree and make the code thereby easier to read

class tritreeHdl
{
public:
	tritreeHdl();
	~tritreeHdl();

	bool build_triangle_tree_and_aabb3d( vector<p3d> const & in_vrts, vector<tri3u> const in_fcts );
	bool build_triangle_tree_and_aabb3d( vector<tri3d> const & in );
	void chop_triangle_tree();

	Tree* kauri;
	aabb3d window;			//a tight axis-aligned bounding box to the triangles of the tree
	vector<tri3d> tris;		//the triangles handled by the bounded volume hierarchy
};

#endif
