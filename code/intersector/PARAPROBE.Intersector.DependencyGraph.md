v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-intersector source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigIntersector.h/.cpp
 2. PARAPROBE_IntersectorCGAL.h (--> rename used CGAL datatypes like in surfacer but this can be done for v0.4.1)
 3. PARAPROBE_IntersectorEigen.h
 4. PARAPROBE_IntersectorPQPInterface.h/.cpp
 5. PARAPROBE_IntersectorStructs.h/.cpp  -->remove what we do not need !
 6. PARAPROBE_IntersectorGPU.h/.cu (obsolete)
 7. PARAPROBE_IntersectorTetrahedralize.h/.cpp (obsolete)
 8. PARAPROBE_IntersectorTessellate.h/.cpp (obsolete, only meant as an example)
 9. PARAPROBE_IntersectorOverlapAnalysis.h/.cpp
10. PARAPROBE_IntersectorHDF5.h/.cpp (obsolete)
11. PARAPROBE_IntersectorXDMF.h/.cpp (obsolete)
12. PARAPROBE_IntersectorHdl.h/.cpp
13. PARAPROBE_Intersector.cpp
