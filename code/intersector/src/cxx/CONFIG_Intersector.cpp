/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


#include "CONFIG_Intersector.h"


ostream& operator << (ostream& in, v_feature_sub_set const & val)
{
	in << "v_feature_sub_set" << "\n";
	in << "type " << val.type << "\n";
	in << "filename " << val.filename << "\n";
	in << "grpnm_geo_prefix " << val.grpnm_geo_prefix << "\n";
	in << "identifier.size() " << val.identifier.size() << "\n";
	/*
	for( auto it = val.identifier.begin(); it != val.identifier.end(); it++ ) {
		in << *it << "\n";
	}
	*/
	return in;
}


ostream& operator << (ostream& in, v_feature_set const & val)
{
	in << "set_identifier " << val.set_identifier << "\n";
	in << "feature_types.size() " << val.feature_types.size() << "\n";
	for( auto it = val.feature_types.begin(); it != val.feature_types.end(); it++ ) {
		in << *it << "\n";
	}
	return in;
}


ostream& operator << (ostream& in, v_v_spatial_correlation_task const & val)
{
	in << "volume_volume_spatial_correlation task" << "\n";
	in << "taskid " << val.taskid << "\n";
	in << "method " << val.method << "\n";
	in << "proximity " << val.proximity << "\n";

	if ( val.IOAnalyzeOverlap == true )
		in << "analyzing overlap " << "yes" << "\n";
	else
		in << "analyzing overlap " << "no" << "\n";

	if ( val.IOAnalyzeProximity == true )
		in << "analyzing proximity " << "yes" << "\n";
	else
		in << "analyzing proximity " << "no" << "\n";

	if ( val.IOAnalyzeCoprecipitate == true )
		in << "analyzing coprecipitate " << "yes" << "\n";
	else
		in << "analyzing coprecipitate " << "no" << "\n";

	if ( val.IOStoreFwdLinks == true )
		in << "store forward (curr->next) links " << "yes" << "\n";
	else
		in << "store forward (curr->next) links " << "no" << "\n";

	if ( val.IOStoreBkwdLinks == true )
		in << "store backward (next->curr) links " << "yes" << "\n";
	else
		in << "store backward (next->curr) links " << "no" << "\n";

	in << "curr_set" << "\n";
	in << val.curr_set << "\n";
	in << "next_set" << "\n";
	in << val.next_set << "\n";
	return in;
}


vector<v_v_spatial_correlation_task> ConfigIntersector::SpatialCorrelationTasks =
		vector<v_v_spatial_correlation_task>();

//apt_int ConfigIntersector::DefaultPointsPerBlock = 5;
//string ConfigIntersector::TetGenFlags = "pqQ";
//quiet, default quality mesh //"pY", "pq", "pY"; //pYqQ //"pYv"; //"pq2.0a10.0v"


bool ConfigIntersector::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigIntersector::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string dsnm = "";

		apt_uint number_of_tasks = 0;
		apt_uint entry_id = 1;
		grpnm = "/entry" + to_string(entry_id);
		if ( h5r.nexus_read(grpnm + "/number_of_tasks", number_of_tasks ) != MYHDF5_SUCCESS ) {
			return false;
		}

		SpatialCorrelationTasks = vector<v_v_spatial_correlation_task>();
		for ( apt_uint tskid = 0; tskid < number_of_tasks; tskid++ ) {
			cout << "Reading in taskid " << tskid << "\n";
			apt_uint proc_id = tskid + 1;

			grpnm = "/entry" + to_string(entry_id) + "/v_v_spatial_correlation" + to_string(proc_id);
			if ( h5r.nexus_path_exists( grpnm ) == true ) {

				SpatialCorrelationTasks.push_back( v_v_spatial_correlation_task() );
				SpatialCorrelationTasks.back().taskid = tskid + 1;
				cout << "SpatialCorrelationTasks.back().taskid " << SpatialCorrelationTasks.back().taskid << "\n";

				SpatialCorrelationTasks.back().method = EVAPID_COMPARISON;
				/*
				string method = "shared_ion";
				if ( h5r.nexus_read( grpnm + "/intersection_detection_method", method ) != MYHDF5_SUCCESS ) { return false; }
				if ( method.compare("shared_ion") == 0 ) {
					SpatialCorrelationTasks.back().method = EVAPID_COMPARISON;
					cout << "Computing overlap by checking for overlapping ions" << "\n";
				}
				else if ( str.compare("tetrahedra_intersections") == 0 ) {
					SpatialCorrelationTasks.back().method = TETGEN_TETRAHEDRALIZE;
					cerr << "Computing overlap by checking tetrahedralization" << "\n";
					cerr << "In preliminary studies we used the TetGen library but found numerical issues in some cases." << "\n";
					cerr << "As it was discussed in the preprint to the 2022 computational geometry and composition analysis" << "\n";
					cerr << "paper we have decided to stop supporting this and use shared_ions for now as the method of choice" << "\n";
					cerr << "to detect overlaps. If needed in the future the code should instead use the gmsh library directly." << "\n";
					return false;
				}
				else {
					cerr << "Intersection detection method is unknown!" << "\n";
					return false;
				}
				*/

				unsigned char u08 = 0x00;
				if ( h5r.nexus_read( grpnm + "/analyze_intersection", u08 ) != MYHDF5_SUCCESS ) { return false; }
				SpatialCorrelationTasks.back().IOAnalyzeOverlap = ( u08 == 0x01 ) ? true : false;
				cout << "IOAnalyzeOverlap " << (int) SpatialCorrelationTasks.back().IOAnalyzeOverlap << "\n";

				u08 = 0x00;
				if ( h5r.nexus_read( grpnm + "/analyze_proximity", u08 ) != MYHDF5_SUCCESS ) { return false; }
				SpatialCorrelationTasks.back().IOAnalyzeProximity = ( u08 == 0x01 ) ? true : false;
				cout << "IOAnalyzeProximity " << (int) SpatialCorrelationTasks.back().IOAnalyzeProximity << "\n";

				if ( h5r.nexus_read( grpnm + "/threshold_proximity", SpatialCorrelationTasks.back().proximity ) != MYHDF5_SUCCESS ) { return false; }
				cout << "SpatialCorrelationTasks.back().proximity " << SpatialCorrelationTasks.back().proximity << "\n";

				u08 = 0x00;
				if ( h5r.nexus_read( grpnm + "/analyze_coprecipitation", u08 ) != MYHDF5_SUCCESS ) { return false; }
				SpatialCorrelationTasks.back().IOAnalyzeCoprecipitate = ( u08 == 0x01 ) ? true : false;
				cout << "IOAnalyzeCoprecipitation " << (int) SpatialCorrelationTasks.back().IOAnalyzeCoprecipitate << "\n";

				u08 = 0x00;
				if ( h5r.nexus_read( grpnm + "/has_current_to_next_links", u08 ) != MYHDF5_SUCCESS ) { return false; }
				SpatialCorrelationTasks.back().IOStoreFwdLinks = ( u08 == 0x01 ) ? true : false;
				cout << "IOStoreFwdLinks " << (int) SpatialCorrelationTasks.back().IOStoreFwdLinks << "\n";

				u08 = 0x00;
				if ( h5r.nexus_read( grpnm + "/has_next_to_current_links", u08 ) != MYHDF5_SUCCESS ) { return false; }
				SpatialCorrelationTasks.back().IOStoreBkwdLinks = ( u08 == 0x01 ) ? true : false;
				cout << "IOStoreBkwdLinks " << (int) SpatialCorrelationTasks.back().IOStoreBkwdLinks << "\n";

				string grpnm_curr = grpnm + "/current_set";
				if ( h5r.nexus_path_exists( grpnm_curr ) == true ) {
					if ( h5r.nexus_read( grpnm_curr + "/set_identifier",
							SpatialCorrelationTasks.back().curr_set.set_identifier) != MYHDF5_SUCCESS ) { return false; }
					cout << "SpatialCorrelationTasks.back().curr_set.set_identifier "
							<< SpatialCorrelationTasks.back().curr_set.set_identifier << "\n";

					apt_uint n_feature_types = 0;
					if ( h5r.nexus_read( grpnm_curr + "/number_of_feature_types", n_feature_types ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_feature_types > 0 ) {
						for( apt_uint typid = 1; typid <= n_feature_types; typid++ ) {
							cout << "Loading configuration for typid " << typid << "\n";
							SpatialCorrelationTasks.back().curr_set.feature_types.push_back( v_feature_sub_set() );

							string subgrpnm = grpnm_curr + "/feature" + to_string(typid);

							if ( h5r.nexus_read( subgrpnm + "/feature_type",
									SpatialCorrelationTasks.back().curr_set.feature_types.back().type ) != MYHDF5_SUCCESS ) { return false; }

							if ( h5r.nexus_read( subgrpnm + "/path",
									SpatialCorrelationTasks.back().curr_set.feature_types.back().filename ) != MYHDF5_SUCCESS ) { return false; }

							if ( h5r.nexus_read( subgrpnm + "/geometry",
									SpatialCorrelationTasks.back().curr_set.feature_types.back().grpnm_geo_prefix ) != MYHDF5_SUCCESS ) { return false; }

							if ( h5r.nexus_read( subgrpnm + "/feature_identifier",
									SpatialCorrelationTasks.back().curr_set.feature_types.back().identifier ) != MYHDF5_SUCCESS ) { return false; }

							cout << SpatialCorrelationTasks.back().curr_set.feature_types.back() << "\n";
						}
					}
					else {
						cerr << "current_set has no sub-sets !" << "\n";
					}
				}
				else {
					return false;
				}

				string grpnm_next = grpnm + "/next_set";
				if ( h5r.nexus_path_exists( grpnm_next ) == true ) {
					if ( h5r.nexus_read( grpnm_next + "/set_identifier",
							SpatialCorrelationTasks.back().next_set.set_identifier) != MYHDF5_SUCCESS ) { return false; }
					cout << "SpatialCorrelationTasks.back().next_set.set_identifier "
							<< SpatialCorrelationTasks.back().next_set.set_identifier << "\n";

					apt_uint n_feature_types = 0;
					if ( h5r.nexus_read( grpnm_next + "/number_of_feature_types", n_feature_types ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_feature_types > 0 ) {
						for( apt_uint typid = 1; typid <= n_feature_types; typid++ ) {
							cout << "Loading configuration for typid " << typid << "\n";
							SpatialCorrelationTasks.back().next_set.feature_types.push_back( v_feature_sub_set() );

							string subgrpnm = grpnm_next + "/feature" + to_string(typid);

							if ( h5r.nexus_read( subgrpnm + "/feature_type",
									SpatialCorrelationTasks.back().next_set.feature_types.back().type ) != MYHDF5_SUCCESS ) { return false; }

							if ( h5r.nexus_read( subgrpnm + "/path",
									SpatialCorrelationTasks.back().next_set.feature_types.back().filename ) != MYHDF5_SUCCESS ) { return false; }

							if ( h5r.nexus_read( subgrpnm + "/geometry",
									SpatialCorrelationTasks.back().next_set.feature_types.back().grpnm_geo_prefix ) != MYHDF5_SUCCESS ) { return false; }

							if ( h5r.nexus_read( subgrpnm + "/feature_identifier",
									SpatialCorrelationTasks.back().next_set.feature_types.back().identifier ) != MYHDF5_SUCCESS ) { return false; }

							cout << SpatialCorrelationTasks.back().next_set.feature_types.back() << "\n";
						}
					}
					else {
						cerr << "current_set has no sub-sets !" << "\n";
					}
				}
				else {
					return false;
				}

				cout << "Summary of taskid " << tskid << "\n";
				cout << SpatialCorrelationTasks.back() << "\n";

			} //next process
		}

		if ( SpatialCorrelationTasks.size() == 0 ) {
			cerr << "No tracking task was defined !" << "\n";
			return false;
		}

		return true;
	}

	//special developer case SimID == 0

	return false;
}
