/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_CONFIG_INTERSECTOR_H__
#define __PARAPROBE_CONFIG_INTERSECTOR_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"


enum OVERLAP_METHOD {
	TETGEN_TETRAHEDRALIZE,
	EVAPID_COMPARISON
};


struct v_feature_sub_set
{
	//metadata where to find geometry data and metadata to members of a set of volumetric features
	//which are described by triangulated surface meshes
	string type;
	string filename;
	string grpnm_geo_prefix;
	vector<apt_uint> identifier;

	v_feature_sub_set() :
		 type(""), filename(""), grpnm_geo_prefix(""),
		identifier(vector<apt_uint>()) {}
};

ostream& operator << (ostream& in, v_feature_sub_set const & val);


struct v_feature_set
{
	//metadata where to find geometry data and metadata to members of a set of volumetric features
	//which are described by triangulated surface meshes
	apt_uint set_identifier;
	vector<v_feature_sub_set> feature_types;		//e.g. objects far from edge, close to edge, proxies far from, close to edge, i.e. different sub-set of volumetric features

	v_feature_set() :
		set_identifier(UMX), feature_types(vector<v_feature_sub_set>()) {}
};

ostream& operator << (ostream& in, v_feature_set const & val);


struct v_v_spatial_correlation_task
{
	apt_uint taskid;							//do not mix up with the set_identifier or the feature_identifier !
	apt_uint method;							//whereby are volumetric intersections detected
	apt_real proximity;							//nm
	bool IOAnalyzeOverlap;						//compute if tetrahedralize objects overlap
	bool IOAnalyzeProximity;					//analyze which tetrahedralized objects are close within proximity to one another but not overlapping
	bool IOAnalyzeCoprecipitate;				//for each task before a clustering of the nodes which segments the graph into as many subgraphs as node groups exists
	bool IOStoreFwdLinks;						//current -> next
	bool IOStoreBkwdLinks;						//next -> current
	//padding

	v_feature_set curr_set;
	v_feature_set next_set;

	v_v_spatial_correlation_task() :
		taskid(UMX), method(EVAPID_COMPARISON),
		proximity(MYZERO),
		IOAnalyzeOverlap(true),
		IOAnalyzeProximity(true),
		IOAnalyzeCoprecipitate(false),
		IOStoreFwdLinks(true),
		IOStoreBkwdLinks(true),
		curr_set(v_feature_set()),
		next_set(v_feature_set()) {}
};

ostream& operator << (ostream& in, v_v_spatial_correlation_task const & val);


class ConfigIntersector
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static vector<v_v_spatial_correlation_task> SpatialCorrelationTasks;

	//sensible defaults
};

#endif
