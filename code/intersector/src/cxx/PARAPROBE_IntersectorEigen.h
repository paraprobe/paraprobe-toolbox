/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_INTERSECTOR_EIGEN_H__
#define __PARAPROBE_INTERSECTOR_EIGEN_H__

#include "PARAPROBE_IntersectorCGAL.h"

//add the Eigen header-only template library
#include "Eigen/Dense"
#include "Eigen/Geometry"


struct vectorComparator
{
	bool operator()( const Eigen::Vector3d & a, const Eigen::Vector3d & b) const
	{
		return a[0] < b[0] ?
				true :
				(a[0] > b[0] ?
						false :
						(a[1] < b[1] ?
								true :
								(a[1] > b[1] ?
										false : (a[2] < b[2] ? true : false))));
	}
};


#endif
