/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_INTERSECTOR_HDL_H__
#define __PARAPROBE_INTERSECTOR_HDL_H__

#include "PARAPROBE_IntersectorXDMF.h"

//individual analysis cases when checking overlap/intersections, proximity
#define YES_VOLUMETRIC_OVERLAP	0x00
#define NO_VOLUMETRIC_OVERLAP	0x01
#define WITHIN_PROXIMITY		0x02
#define FAR_AWAY				0x03
#define UNCLEAR_SITUATION		0xEE
#define UNKNOWN_SITUATION		0xFF


//target_set identifier
#define CURRENT_SET				0
#define NEXT_SET				1


class VolumeFeatureSet
{
public:
	VolumeFeatureSet();
	~VolumeFeatureSet();

	void clear_objs();
	void clear_bvh();

	bool read_relevant_v_input( v_feature_set & info );
	bool check_input();
	bool build_bvh();

	apt_uint identifier;
	vector<polyhedron_triangulated_surface_mesh> objs;
	Tree* bvh_objs;
};


class intersectorHdl : public mpiHdl
{
	//process-level class which implements the worker instance

public:
	intersectorHdl();
	~intersectorHdl();
	
	void clear_links();

	unsigned char v_v_core(
			polyhedron_triangulated_surface_mesh const & obj_a,
			polyhedron_triangulated_surface_mesh const & obj_b,
			const bool check_proximity,
			const apt_real threshold );
	void analyze_compare_sets_current_to_next(
			v_v_spatial_correlation_task const & info );
	void analyze_coprecipitates(
			v_v_spatial_correlation_task const & info );
	bool write_results_h5(
			v_v_spatial_correlation_task const & info );

	//##MK::think about extensions to check volumetric against linear and point features...
	//void analyze_v_l //volume to l
	//void analyze_v_p //solutes to interfaces functional overlap with paraprobe-nanochem, paraprobe-distancer
	//void analyze_l_l //dislocations, zone lines
	//void analyze_l_p //proximity points to lines solutes to dislocations
	//void analyze_p_p //proximity of points functional overlap with paraprobe-distancer, paraprobe-spatstat

	void execute_v_v_workpackage();

	VolumeFeatureSet current_set;
	VolumeFeatureSet next_set;

	vector<forward_link> c2n_links_i;
	vector<forward_link> n2c_links_i;
	vector<forward_link> c2n_links_p;
	vector<forward_link> n2c_links_p;

	map<size_t, pair<bool, apt_uint>> visited_clusterid;

	profiler intersector_tictoc;

	//deprecated functions
	/*
	map<apt_uint, vector<timedep_graph_node>*> objs_evolution;
	bool track_volume_evolution_single_object( const apt_uint taskid, const apt_uint target, vector<timedep_graph_node>* res );
	*/
};


#endif
