/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_IntersectorOverlapAnalysis.h"


bool positiveOrientation(const Tet & t)
{
	Vec3f e1 = t.vertices_[1] - t.vertices_[0];
	Vec3f e2 = t.vertices_[2] - t.vertices_[0];
	Vec3f e3 = t.vertices_[3] - t.vertices_[0];
	return (Vec3f::cross(e1,e2) | e3) > 0.0f;
}


bool edgeEdgeSeparationSAT(const Tet & t1, const Tet & t2)
{
	const int edgesIdx[6][2]   = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};
	//const int normalsIdx[6][2] = {{2,3},{1,3},{1,2},{0,3},{0,2},{0,1}};

	Vec3f edges1[6], edges2[6];
	for( int i = 0; i < 6; ++i ) {
		edges1[i] = t1.vertices_[edgesIdx[i][0]]-t1.vertices_[edgesIdx[i][1]];
		edges2[i] = t2.vertices_[edgesIdx[i][0]]-t2.vertices_[edgesIdx[i][1]];
	}
	for( int e1 = 0; e1 < 6; ++e1 ) {
		for( int e2 = 0; e2 < 6; ++e2 ) {
			Vec3f direction = Vec3f::cross(edges1[e1], edges2[e2]);
			if( direction.norm2() < EPSILON ) { //##MK::in the gitlab code this was commented out so the important case
				//that it t1 and t2 are the same tetrahedron, we except they are not disjoint so we need to vote false
				//with the norm2 however commented out the above cross product is the zero vector, likely polluted by precision errors 1e-6 )
				continue;
			}
			//++planeStatPerPair; //##MK::not required
			if( t2.maxAlongDirection(direction) <= t1.minAlongDirection(direction) )
				return true;
			if( t1.maxAlongDirection(direction) <= t2.minAlongDirection(direction) )
				return true;
		}
	}
	return false;
}


tetOverlap::tetOverlap()
{
}


tetOverlap::~tetOverlap()
{
}


bool tetOverlap::tet_disjoint_sat( roi_tetrahedron const & a, roi_tetrahedron const & b )
{
	//unexpected but test it out for yourself, if vertices in a and b represent exactly the same object (combinatorics) the test returns true although it should return false
	//##MK::for instance
	/*
	<?xml version="1.0" ?>
	<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>
	<Xdmf xmlns:xi="http://www.w3.org/2003/XInclude" Version="2.2">
	  <Domain>
	    <Grid Name="tetrsect" GridType="Uniform">
	      <Topology TopologyType="Tetrahedron" NumberOfElements="2">
	        <DataItem Dimensions="8" NumberType="UInt" Precision="4" Format="XML">
	          0 1 2 3
	          4 5 6 7
	        </DataItem>
	      </Topology>
	      <Geometry GeometryType="XYZ">
	        <DataItem Dimensions="8 3" NumberType="Double" Precision="8" Format="XML">
-2.9702889919281005859375 4.044038295745849609375 2.662694454193115234375
-3.0237410068511962890625 4.430759429931640625 2.4568531513214111328125
-3.0263881683349609375 3.988135814666748046875 2.59479427337646484375
-3.1131541728973388671875 4.424478054046630859375 2.7038514614105224609375
-2.9702889919281005859375 4.044038295745849609375 2.662694454193115234375
-3.0237410068511962890625 4.430759429931640625 2.4568531513214111328125
-3.0263881683349609375 3.988135814666748046875 2.59479427337646484375
-3.1131541728973388671875 4.424478054046630859375 2.7038514614105224609375
	        </DataItem>
	      </Geometry>
	      <Attribute AttributeType="Scalar" Center="Cell" Name="tetrahedron">
	        <DataItem Dimensions="2" DataType="UInt" Precision="4" Format="XML">
	           90 71
	       </DataItem>
	      </Attribute>
	    </Grid>
	  </Domain>
	</Xdmf>a
	*/
	//the above example was reported that tetrahedron 90 and tetrahedron 71 overlap
	//../../../thirdparty/mandatory/hornus2017/intersection-detection-master/tettest.cpp
	Tet t1;
	t1.vertices_[0] = Vec3f( a.v1.x, a.v1.y, a.v1.z );
	t1.vertices_[1] = Vec3f( a.v2.x, a.v2.y, a.v2.z );
	t1.vertices_[2] = Vec3f( a.v3.x, a.v3.y, a.v3.z );
	t1.vertices_[3] = Vec3f( a.v4.x, a.v4.y, a.v4.z );
	if( !positiveOrientation(t1) ) {
		swap(t1.vertices_[0], t1.vertices_[1]);
	}
	t1.computeNormalsAndCenter();
	/*
	if( ! t.containsZero() ) {
		goto recommence;
	}
	t.shift(shift);
	*/

	Tet t2;
	t2.vertices_[0] = Vec3f( b.v1.x, b.v1.y, b.v1.z );
	t2.vertices_[1] = Vec3f( b.v2.x, b.v2.y, b.v2.z );
	t2.vertices_[2] = Vec3f( b.v3.x, b.v3.y, b.v3.z );
	t2.vertices_[3] = Vec3f( b.v4.x, b.v4.y, b.v4.z );
	if( !positiveOrientation(t2) ) {
		swap(t2.vertices_[0], t2.vertices_[1]);
	}
	t2.computeNormalsAndCenter();

	/*
	bool faceSeparation(const Tet & t1, const Tet & t2) {
		return t1.faceSeparate(t2) || t2.faceSeparate(t1);
	}
	*/
	bool face_separation_t1_t2 = t1.faceSeparate(t2) || t2.faceSeparate(t1);

	//bool tetDisjointSAT(const Tet & t1, const Tet & t2) {
	bool retval = face_separation_t1_t2 || edgeEdgeSeparationSAT(t1, t2);

	return retval;
}



//bool tetOverlap::has_overlap_cgal( roi_tetrahedron const & a, roi_tetrahedron const & b )
//{
	//##MK::dysfunctional
	//##MK::for instance
	/*
	<?xml version="1.0" ?>
	<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>
	<Xdmf xmlns:xi="http://www.w3.org/2003/XInclude" Version="2.2">
	  <Domain>
	    <Grid Name="tetrsect" GridType="Uniform">
	      <Topology TopologyType="Tetrahedron" NumberOfElements="2">
	        <DataItem Dimensions="8" NumberType="UInt" Precision="4" Format="XML">
	          0 1 2 3
	          4 5 6 7
	        </DataItem>
	      </Topology>
	      <Geometry GeometryType="XYZ">
	        <DataItem Dimensions="8 3" NumberType="Double" Precision="8" Format="XML">
	9.42415142059326171875 -12.686931610107421875 25.8836688995361328125
	10.297855377197265625 -11.69927120208740234375 23.9938602447509765625
	10.10485553741455078125 -10.9297771453857421875 24.919673919677734375
	10.31072902679443359375 -10.95581912994384765625 24.7930927276611328125
	11.25255489349365234375 -11.01622772216796875 24.05571746826171875
	10.78653049468994140625 -11.08370685577392578125 23.4241485595703125
	10.299037933349609375 -10.30122280120849609375 23.2975330352783203125
	11.28629589080810546875 -10.4900665283203125 23.0770664215087890625
	        </DataItem>
	      </Geometry>
	      <Attribute AttributeType="Scalar" Center="Cell" Name="tetrahedron">
	        <DataItem Dimensions="2" DataType="UInt" Precision="4" Format="XML">
	           90 71
	       </DataItem>
	      </Attribute>
	    </Grid>
	  </Domain>
	</Xdmf>a
	*/
	//the above example was reported that tetrahedron 90 and tetrahedron 71 overlap
	//../../../thirdparty/mandatory/hornus2017/intersection-detection-master/tettest.cpp

	/*
	Point_3 p10(t1.vertices_[0].x(), t1.vertices_[0].y(), t1.vertices_[0].z());
	Point_3 p11(t1.vertices_[1].x(), t1.vertices_[1].y(), t1.vertices_[1].z());
	Point_3 p12(t1.vertices_[2].x(), t1.vertices_[2].y(), t1.vertices_[2].z());
	Point_3 p13(t1.vertices_[3].x(), t1.vertices_[3].y(), t1.vertices_[3].z());
	Point_3 p20(t2.vertices_[0].x(), t2.vertices_[0].y(), t2.vertices_[0].z());
	Point_3 p21(t2.vertices_[1].x(), t2.vertices_[1].y(), t2.vertices_[1].z());
	Point_3 p22(t2.vertices_[2].x(), t2.vertices_[2].y(), t2.vertices_[2].z());
	Point_3 p23(t2.vertices_[3].x(), t2.vertices_[3].y(), t2.vertices_[3].z());
	Point_3 p10(a.v1.x, a.v1.y, a.v1.z);
	Point_3 p11(a.v2.x, a.v2.y, a.v2.z);
	Point_3 p12(a.v3.x, a.v3.y, a.v3.z);
	Point_3 p13(a.v4.x, a.v4.y, a.v4.z);
	Point_3 p20(b.v1.x, b.v1.y, b.v1.z);
	Point_3 p21(b.v2.x, b.v2.y, b.v2.z);
	Point_3 p22(b.v3.x, b.v3.y, b.v3.z);
	Point_3 p23(b.v4.x, b.v4.y, b.v4.z);

	Triangle_3 tri1[4]={{p10,p11,p12}, {p10,p11,p13}, {p10,p12,p13}, {p11,p12,p13}};
	Triangle_3 tri2[4]={{p20,p21,p22}, {p20,p21,p23}, {p20,p22,p23}, {p21,p22,p23}};
	Tetrahedron_3 tet1(p10,p11,p12,p13);
	Tetrahedron_3 tet2(p20,p21,p22,p23);
	for( int i = 0; i < 4; ++i )
		if( CGAL::do_intersect(tri1[i], tet2) )
			return false;
	for( int i = 0; i < 4; ++i )
	if( CGAL::do_intersect(tri2[i], tet1) )
	return false;
	return true;
	}
}
*/


tetIntersectionVolume::tetIntersectionVolume()
{
}


tetIntersectionVolume::~tetIntersectionVolume()
{
}


pair<apt_real, int> tetIntersectionVolume::compute_intersection_volume( roi_tetrahedron const & a, roi_tetrahedron const & b )
{
	MyPoint p1(a.v1.x, a.v1.y, a.v1.z);
	MyPoint q1(a.v2.x, a.v2.y, a.v2.z);
	MyPoint s1(a.v3.x, a.v3.y, a.v3.z);
	MyPoint r1(a.v4.x, a.v4.y, a.v4.z);
	YourPolyhedron P1;
	P1.make_tetrahedron( p1, q1, r1, s1);
	/*
	if ( P1.is_closed() == false ) {
		cerr << "P1.is_closed() == false" << "\n";
		return MYZERO;
	}
	cout << "PMP::volume(P1) " << setprecision(32) << PMP::volume(P1) << "\n";
	*/
	MyPoint p2(b.v1.x, b.v1.y, b.v1.z);
	MyPoint q2(b.v2.x, b.v2.y, b.v2.z);
	MyPoint s2(b.v3.x, b.v3.y, b.v3.z);
	MyPoint r2(b.v4.x, b.v4.y, b.v4.z);
	YourPolyhedron P2;
	P2.make_tetrahedron( p2, q2, r2,  s2 );
	/*
	if ( P2.is_closed() == false ) {
		cerr << "P2.is_closed() == false" << "\n";
		return 0;
	}
	cout << "PMP::volume(P2) " << setprecision(32) << PMP::volume(P2) << "\n";
	*/

	Nef_polyhedron N1(P1);
	Nef_polyhedron N2(P2);

	//boolean intersection of two NEF polyhedra
	Nef_polyhedron N1isectN2 = N1 * N2;

	//N1isectN2.is_bounded(), only one volume ??
	if ( N1isectN2.is_empty() == false ) {
		Surface_mesh myout;
	    CGAL::convert_nef_polyhedron_to_polygon_mesh(N1isectN2, myout);
	    if ( CGAL::is_closed(myout) == true ) {
	    	return pair<apt_real,int>((apt_real) CGAL::to_double(PMP::volume(myout)), MYCGAL_NEF_SUCCESS); //reduce to floating point precision
	    }
	    else {
	    	cerr << "is_closed(CGAL::convert_nef_polyhedron_to_polygon_mesh(N1isectN2, myout)) unexpectedly returned false !" << "\n";
	    	return pair<apt_real,int>(MYZERO, MYCGAL_NEF_FAILED_NEF_TO_SRFMSH);
	    }
	}
	else {
		return pair<apt_real,int>(MYZERO, MYCGAL_NEF_SUCCESS);
	}

	return pair<apt_real,int>(MYZERO, MYCGAL_NEF_FAILED);
};



//additionally relevant code that can help to understand and test things
/*
#define OBJECT_IS_POLYHEDRON							0xFF
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <iostream>
typedef CGAL::Simple_cartesian<double>     MyKernel;
typedef MyKernel::Point_3                  MyPoint_3;
typedef CGAL::Polyhedron_3<MyKernel>       MyPolyhedron;
typedef MyPolyhedron::Vertex_iterator        Vertex_iterator;
*/

/*
#include <CGAL/Exact_integer.h>
#include <CGAL/Extended_homogeneous.h>
#include <CGAL/Polyhedron_3.h>
//#include <CGAL/Homogeneous.h>
#include <CGAL/Nef_polyhedron_3.h>
//#include <CGAL/IO/Nef_polyhedron_iostream_3.h>
//#include <iostream>
typedef CGAL::Extended_homogeneous<CGAL::Exact_integer>  YourKernel;
typedef YourKernel::Point_3                  YourPoint_3;
typedef CGAL::Polyhedron_3<YourKernel>  YourPolyhedron;
typedef CGAL::Nef_polyhedron_3<YourKernel> Nef_polyhedron;
typedef YourKernel::Vector_3  Vector_3;
typedef YourKernel::Aff_transformation_3  Aff_transformation_3;
*/


/*
int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	double toc = 0.;

	MyPoint p1( 1.0, 0.0, 0.0);
	MyPoint q1( 0.0, 1.0, 0.0);
	MyPoint s1( 1.0, 0.0, 2.0); //0.0, 0.0, 1.0
	MyPoint r1( -0.5, -0.5, -0.5); //0.0, 0.0, 0.0);
    YourPolyhedron P1;
    P1.make_tetrahedron( p1, q1, r1, s1);
    if ( P1.is_closed() == false ) {
    	cerr << "P1.is_closed() == false" << "\n";
    	return 0;
    }
    cout << "PMP::volume(P1) " << setprecision(32) << PMP::volume(P1) << "\n";
    MyPoint p2( 2.0, 0.0, 0.0); //-2, 0, 0
    MyPoint q2( 0.0, 2.0, 0.0); //0, -2, 0
    MyPoint r2( 0.0, 0.0, 2.0); //1.5, 0, 2 is a toucher
    MyPoint s2( 0.0, 0.0, 0.0);
    YourPolyhedron P2;
    P2.make_tetrahedron( p2, q2, r2,  s2 );
    cout << "PMP::volume(P2) " << setprecision(32) << PMP::volume(P2) << "\n";
    if ( P2.is_closed() == false ) {
    	cerr << "P2.is_closed() == false" << "\n";
    	return 0;
    }

    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";
    tic = omp_get_wtime();

    Nef_polyhedron N1(P1);
    Nef_polyhedron N2(P2);
    //boolean intersection of two NEF polyhedra
    Nef_polyhedron N1isectN2 = N1 * N2;
    //N1isectN2.is_bounded(), N1isectN2.is_empty()

    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";
    tic = omp_get_wtime();

    Surface_mesh myout;
    CGAL::convert_nef_polyhedron_to_polygon_mesh(N1isectN2, myout);
    if ( CGAL::is_closed(myout) == true ) {
    	cout << "PMP::volume(myout) " << setprecision(32) << PMP::volume(myout) << "\n";
    }
    else {
    	cerr << "CGAL::is_closed(myout) == false !" << "\n";
    	return 0;
    }
    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";

    cout << myout;
    cout << endl;

    //if ( N1isectN2.is_simple() == true ) {
    //	cout << "N1isectN2.is_simple() == true " << "\n";
    //	YourPolyhedron P1isectP2;
    //	N1isectN2.convert_to_polyhedron( P1isectP2 );
    //	if ( P1isectP2.is_closed() == true ) {
    //		cout << "P1isectP2.is_closed() == true" << "\n";
    //		cout << "P1isectP2.volume() " << PMP::volume(P1isectP2) << "\n";
    //	}
    //}

    return 0;
}

#else

//intersection volume and mesh of intersection region using nef polyhedra
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include <CGAL/Polygon_mesh_processing/measure.h>
#include <fstream>
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Exact_predicates_exact_constructions_kernel EK;
typedef CGAL::Surface_mesh<K::Point_3> Mesh;
typedef Mesh::Vertex_index vertex_descriptor;
typedef Mesh::Face_index face_descriptor;
typedef boost::graph_traits<Mesh>::vertex_descriptor vertex_descriptor;
typedef Mesh::Property_map<vertex_descriptor,EK::Point_3> Exact_point_map;
namespace PMP = CGAL::Polygon_mesh_processing;
namespace params = PMP::parameters;

#include <CGAL/Polygon_mesh_processing/self_intersections.h>
typedef boost::graph_traits<Mesh>::face_descriptor          bface_descriptor;
#include <CGAL/tags.h>

#include <CGAL/Polygon_mesh_processing/distance.h>
#include <CGAL/Polygon_mesh_processing/remesh.h>




#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/Polygon_mesh_processing/repair_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
typedef CGAL::Polyhedron_3<K, CGAL::Polyhedron_items_with_id_3>      Polyhedron;
typedef K::FT                                                   FT;
#include<array>
typedef std::array<FT, 3>                                       Custom_point;
#include <boost/property_map/property_map.hpp>

#include <CGAL/convex_hull_3.h>

struct Array_traits
{
	struct Equal_3
	{
		bool operator()(const Custom_point& p, const Custom_point& q) const {
			return (p == q);
		}
	};
	struct Less_xyz_3
	{
		bool operator()(const Custom_point& p, const Custom_point& q) const {
			return std::lexicographical_compare(p.begin(), p.end(), q.begin(), q.end());
		}
	};
	Equal_3 equal_3_object() const {
		return Equal_3();
	}
	Less_xyz_3 less_xyz_3_object() const {
		return Less_xyz_3();
	}
};


struct Exact_vertex_point_map
{
  // typedef for the property map
  typedef boost::property_traits<Exact_point_map>::value_type value_type;
  typedef boost::property_traits<Exact_point_map>::reference reference;
  typedef boost::property_traits<Exact_point_map>::category category;
  typedef boost::property_traits<Exact_point_map>::key_type key_type;
  // exterior references
  Exact_point_map exact_point_map;
  Mesh* tm_ptr;
  // Converters
  CGAL::Cartesian_converter<K, EK> to_exact;
  CGAL::Cartesian_converter<EK, K> to_input;
  Exact_vertex_point_map()
    : tm_ptr(nullptr)
  {}
  Exact_vertex_point_map(const Exact_point_map& ep, Mesh& tm)
    : exact_point_map(ep)
    , tm_ptr(&tm)
  {
    for (Mesh::Vertex_index v : vertices(tm))
      exact_point_map[v]=to_exact(tm.point(v));
  }
  friend
  reference get(const Exact_vertex_point_map& map, key_type k)
  {
    CGAL_precondition(map.tm_ptr!=nullptr);
    return map.exact_point_map[k];
  }
  friend
  void put(const Exact_vertex_point_map& map, key_type k, const EK::Point_3& p)
  {
    CGAL_precondition(map.tm_ptr!=nullptr);
    map.exact_point_map[k]=p;
    // create the input point from the exact one
    map.tm_ptr->point(k)=map.to_input(p);
  }
};


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	double toc = 0.;

//strategy 1 - approximate (##MK::need to compute symmetric) Hausdorff distance, works but very expensive and approximate ...
	//Mesh tm1, tm2;
	//CGAL::make_tetrahedron(	K::Point_3( 0., 0., 0. ),
	//						K::Point_3(2., 0., 0.),
	//						K::Point_3(1., 1., 1.),
	//						K::Point_3(1., 0., 2.),  tm1 );
	//tm2 = tm1;
	//CGAL::Polygon_mesh_processing::isotropic_remeshing(tm2.faces(),.05, tm2);
	//cout << "Approximated Hausdorff distance: " <<
	//		PMP::approximate_Hausdorff_distance<CGAL::Sequential_tag>(tm1, tm2,
	//				PMP::parameters::number_of_points_per_area_unit(stoul(argv[1]))) << "\n";

	//toc = omp_get_wtime();
	//cout << (toc-tic) << " s" << "\n";
	//tic = omp_get_wtime();
	//return 0;

//strategy 2 - polygon mesh processing clipping two tetrahedra, not as robust for edge touches, robustness ?

	Mesh m1;
	CGAL::make_tetrahedron(	K::Point_3(1.0, 0.0, 0.0),
							K::Point_3(0.0, 1.0, 0.0),
							K::Point_3(1.0, 0.0, 2.0),
							K::Point_3(-0.5, -0.5, -0.5), m1 );

	//vertex_descriptor p1 = m1.add_vertex(K::Point_3(1.0, 0.0, 0.0));
	//vertex_descriptor p2 = m1.add_vertex(K::Point_3(0.0, 1.0, 0.0));
	//vertex_descriptor p3 = m1.add_vertex(K::Point_3(1.0, 0.0, 2.0)); //0.0, 0.0, 1.0
	//vertex_descriptor p4 = m1.add_vertex(K::Point_3(-0.5, -0.5, -0.5)); //0.0, 0.0, 0.0);
	//m1.add_face(p1, p2, p3);
	//face_descriptor f243 = m1.add_face(p2, p4, p3);
	//if ( f243 == Mesh::null_face()) {
	//	f243 = m1.add_face( p2, p3, p4 ); assert( f243 != Mesh::null_face());
	//}
	//face_descriptor f413 = m1.add_face( p4, p1, p3 );
	//if ( f413 == Mesh::null_face()) {
	//	f413 = m1.add_face( p4, p3, p1 ); assert( f413 != Mesh::null_face());
	//}
	//face_descriptor f214 = m1.add_face( p2, p1, p4 );
	//if ( f214 == Mesh::null_face()) {
	//	f214 = m1.add_face( p2, p4, p1 ); assert( f214 != Mesh::null_face());
	//}
    cout << "PMP::volume(m1) " << setprecision(32) << PMP::volume(m1) << "\n";

    Mesh m2;
	CGAL::make_tetrahedron(	K::Point_3(2.0, 0.0, 0.0),
							K::Point_3(0.0, 2.0, 0.0),
							K::Point_3(0.0, 0.0, 2.0),
							K::Point_3(0.0, 0.0, 0.0), m2 );

	//vertex_descriptor q1 = m2.add_vertex(K::Point_3(2.0, 0.0, 0.0));
	//vertex_descriptor q2 = m2.add_vertex(K::Point_3(0.0, 2.0, 0.0));
	//vertex_descriptor q3 = m2.add_vertex(K::Point_3(0.0, 0.0, 2.0));
	//vertex_descriptor q4 = m2.add_vertex(K::Point_3(0.0, 0.0, 0.0));
	//m2.add_face(q1, q2, q3);
	//f243 = m1.add_face(q2, q4, q3);
	//if ( f243 == Mesh::null_face()) {
	//	f243 = m2.add_face( q2, q3, q4 ); assert( f243 != Mesh::null_face());
	//}
	//f413 = m1.add_face( q4, q1, q3 );
	//if ( f413 == Mesh::null_face()) {
	//	f413 = m2.add_face( q4, q3, q1 ); assert( f413 != Mesh::null_face());
	//}
	//f214 = m2.add_face( q2, q1, q4 );
	//if ( f214 == Mesh::null_face()) {
	//	f214 = m2.add_face( q2, q4, q1 ); assert( f214 != Mesh::null_face());
	//}
	cout << "PMP::volume(m2) " << setprecision(32) << PMP::volume(m2) << "\n";

	if ( CGAL::is_triangle_mesh(m1) == true )
		cout << "m1 is triangle mesh" << "\n";
	if ( CGAL::is_triangle_mesh(m2) == true )
		cout << "m2 is triangle mesh" << "\n";

	bool m1isect = PMP::does_self_intersect( m1, CGAL::parameters::vertex_point_map(get(CGAL::vertex_point, m1)));
	cout << (m1isect ? "m1 There are self-intersections." : "m1 There is no self-intersection.") << endl;

	bool m2isect = PMP::does_self_intersect( m2, CGAL::parameters::vertex_point_map(get(CGAL::vertex_point, m2)));
	cout << (m2isect ? "m2 There are self-intersections." : "m2 There is no self-intersection.") << endl;

	bool m1bound = PMP::does_bound_a_volume( m1 );
	cout << (m1bound ? "m1 Does bound a volume" : "m1 Does not bound a volume") << endl;

	bool m2bound = PMP::does_bound_a_volume( m2 );
	cout << (m2bound ? "m2 Does bound a volume" : "m2 Does not bound a volume") << endl;

	toc = omp_get_wtime();
	cout << (toc-tic) << " s" << "\n";
	tic = omp_get_wtime();

	Exact_point_map m1_exact_points = m1.add_property_map<vertex_descriptor,EK::Point_3>("v:exact_point").first;
	Exact_point_map m2_exact_points = m2.add_property_map<vertex_descriptor,EK::Point_3>("v:exact_point").first;
	Exact_vertex_point_map m1_vpm(m1_exact_points, m1);
	Exact_vertex_point_map m2_vpm(m2_exact_points, m2);

	toc = omp_get_wtime();
	cout << (toc-tic) << " s" << "\n";
	tic = omp_get_wtime();

	//now compute intersection region and store its mesh in-place inside m1 replacing m1 such with the result
	//Mesh isect;
	//bool status = PMP::corefine_and_compute_intersection( m1, m2, m1,
    //        params::vertex_point_map(m1_vpm),
    //        params::vertex_point_map(m2_vpm),
	//		params::vertex_point_map(m1_vpm) );
	//cout << ((status == true) ? "Intersection successfully computed" : "Intersection failed!") << "\n";

	Mesh m1m2union;
	bool valid_union = PMP::corefine_and_compute_union( m1, m2, m1m2union);
	cout << ((valid_union == true) ? "Unionn successfully computed" : "Union failed!") << "\n";

	toc = omp_get_wtime();
	cout << (toc-tic) << " s" << "\n";
	tic = omp_get_wtime();

	//cout << "PMP::volume(m1 (modified)) " << setprecision(32) << PMP::volume(m1) << "\n";
	//cout << m1 << endl;

	cout << "PMP::volume(m1m2union) " << setprecision(32) << PMP::volume(m1m2union) << "\n";
	cout << m1m2union << endl;

	double Vm1 = fabs(CGAL::to_double(PMP::volume(m1)));
	double Vm2 = fabs(CGAL::to_double(PMP::volume(m2)));
	double Vm1m2 = fabs(CGAL::to_double(PMP::volume(m1m2union)));
	double V = 0.5*((Vm1+Vm2) - Vm1m2);
	cout << "V " << setprecision(32) << V << "\n";

    toc = omp_get_wtime();
    cout << (toc-tic) << " s" << "\n";
    tic = omp_get_wtime();

	std::vector<std::array<FT, 3> > points;

	points.push_back(CGAL::make_array<FT>(0., 1., 0. ));
    points.push_back(CGAL::make_array<FT>(1., 0., 0.));
	points.push_back(CGAL::make_array<FT>(1., -0., 1.));
	points.push_back(CGAL::make_array<FT>(0.5, 0.5, 1.));
	points.push_back(CGAL::make_array<FT>(0.63636363636363646456572951137787, 0., 1.3636363636363637574788754136534));
	points.push_back(CGAL::make_array<FT>(0., 0., 0.25));
    points.push_back(CGAL::make_array<FT>(0., 0., 0.));

    cout << "Convex hull" << "\n";
    vector<K::Point_3> cpoints;
    cpoints.push_back(K::Point_3(0., 1., 0. ));
    cpoints.push_back(K::Point_3(1., 0., 0.));
    cpoints.push_back(K::Point_3(1., -0., 1.));
    cpoints.push_back(K::Point_3(0.5, 0.5, 1.));
    cpoints.push_back(K::Point_3(0.63636363636363646456572951137787, 0., 1.3636363636363637574788754136534));
    cpoints.push_back(K::Point_3(0., 0., 0.25));
    cpoints.push_back(K::Point_3(0., 0., 0.));
    Polyhedron chull;
    CGAL::convex_hull_3(cpoints.begin(), cpoints.end(), chull);
    cout << PMP::volume(chull) << "\n";

    vector<vector<size_t>> polygons;
    vector<size_t> p;
    p.clear();
    p.push_back(2); p.push_back(3); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(3); p.push_back(5); p.push_back(0);
    polygons.push_back(p);
    p.clear();
    p.push_back(3); p.push_back(0); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(4); p.push_back(5); p.push_back(3);
    polygons.push_back(p);
    p.clear();
    p.push_back(5); p.push_back(2); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(6); p.push_back(1); p.push_back(0);
    polygons.push_back(p);
    p.clear();
    p.push_back(6); p.push_back(5); p.push_back(1);
    polygons.push_back(p);
    p.clear();
    p.push_back(4); p.push_back(2); p.push_back(5);
    polygons.push_back(p);
    p.clear();
    p.push_back(0); p.push_back(5); p.push_back(6);
    polygons.push_back(p);
    p.clear();
    p.push_back(3); p.push_back(2); p.push_back(4);
    polygons.push_back(p);

    PMP::repair_polygon_soup(points, polygons, CGAL::parameters::geom_traits(Array_traits()));
    PMP::orient_polygon_soup(points, polygons);
    Polyhedron p1;
    PMP::polygon_soup_to_polygon_mesh(points, polygons, p1);
    // Number the faces because 'orient_to_bound_a_volume' needs a face <--> index map
    int index = 0;
	for( Polyhedron::Face_iterator ply_fb = p1.facets_begin(), ply_fe = p1.facets_end(); ply_fb != ply_fe; ++ply_fb ) {
		ply_fb->id() = index++;
	}
	bool p1closed = CGAL::is_closed(p1);
	cout << ((p1closed == true ) ? "p1 closed" : "p1 not closed!") << "\n";
	PMP::orient_to_bound_a_volume(p1);
    cout << "PMP volume(p1) " << CGAL::to_double(PMP::volume(p1)) << "\n";

    return 0;

//strategy 3 with nef polyhedra, works and is robust but expensive
    //MK::see the above example
}
*/
