/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_INTERSECTOR_OVERLAP_ANALYSIS_HDL_H__
#define __PARAPROBE_INTERSECTOR_OVERLAP_ANALYSIS_HDL_H__

#include "PARAPROBE_IntersectorTessellate.h"


#include "../../../thirdparty/mandatory/hornus2017/intersection-detection-master/vec.h"
#include "../../../thirdparty/mandatory/hornus2017/intersection-detection-master/convexes.h"


//relevant code snippets from tettest.cpp
bool positiveOrientation(const Tet & t);

bool edgeEdgeSeparationSAT(const Tet & t1, const Tet & t2);

/*
static const int edgesIdx[6][2]   = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};
static const int normalsIdx[6][2] = {{2,3},{1,3},{1,2},{0,3},{0,2},{0,1}};
*/

/*
void genTet(Tet & t) {
	recommence:
	float shift = drand48() * gSpread;
	RandomPointOnSphere rps;
	for( int i = 0; i < 4; ++i ) {
		t.vertices_[i] = rps.vec3f();
	}
	if( ! positiveOrientation(t) )
		swap(t.vertices_[0], t.vertices_[1]);
	t.computeNormalsAndCenter();
	if( ! t.containsZero() ) {
		goto recommence;
	}
	t.shift(shift);
#ifdef WITH_CGAL
		Point_3 p0(t.vertices_[0].x(), t.vertices_[0].y(), t.vertices_[0].z());
		Point_3 p1(t.vertices_[1].x(), t.vertices_[1].y(), t.vertices_[1].z());
		Point_3 p2(t.vertices_[2].x(), t.vertices_[2].y(), t.vertices_[2].z());
		Point_3 p3(t.vertices_[3].x(), t.vertices_[3].y(), t.vertices_[3].z());
	if( CGAL::orientation(p0, p1, p2, p3) != CGAL::POSITIVE )
		goto recommence;
#endif
}
*/


class tetOverlap
{
public:
	tetOverlap();
	~tetOverlap();

	bool tet_disjoint_sat( roi_tetrahedron const & a, roi_tetrahedron const & b );
	//bool has_overlap_cgal( roi_tetrahedron const & a, roi_tetrahedron const & b );
};


#define MYCGAL_NEF_SUCCESS 						0
#define MYCGAL_NEF_FAILED						-1
#define MYCGAL_NEF_FAILED_NEF_TO_SRFMSH			-2

class tetIntersectionVolume
{
public:
	tetIntersectionVolume();
	~tetIntersectionVolume();

	pair<apt_real,int> compute_intersection_volume( roi_tetrahedron const & a, roi_tetrahedron const & b );
};


#endif
