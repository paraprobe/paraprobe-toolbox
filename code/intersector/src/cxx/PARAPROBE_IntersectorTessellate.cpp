/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_IntersectorTessellate.h"

/*
ostream& operator << (ostream& in, wallstats const & val) {
	if ( val.xmi_touch == true )
		in << "xmi_touch " << "yes" << "\n";
	else
		in << "xmi_touch " << "no" << "\n";
	if ( val.xmx_touch == true )
		in << "xmx_touch " << "yes" << "\n";
	else
		in << "xmx_touch " << "no" << "\n";
	if ( val.ymi_touch == true )
		in << "ymi_touch " << "yes" << "\n";
	else
		in << "ymi_touch " << "no" << "\n";
	if ( val.ymx_touch == true )
		in << "ymx_touch " << "yes" << "\n";
	else
		in << "ymx_touch " << "no" << "\n";
	if ( val.zmi_touch == true )
		in << "zmi_touch " << "yes" << "\n";
	else
		in << "zmi_touch " << "no" << "\n";
	if (val.zmx_touch == true )
		in << "zmx_touch " << "yes" << "\n";
	else
		in << "zmx_touch " << "no" << "\n";
	if ( val.any_touch == true )
		in << "any_touch " << "yes" << "\n";
	else
		in << "any_touch " << "no" << "\n";
	if ( val.halo_insufficient == true )
		in << "halo_insufficient " << "yes" << "\n";
	else
		in << "halo_insufficient " << "no" << "\n";
	return in;
}


voronoiTessellator::voronoiTessellator()
{

}


voronoiTessellator::~voronoiTessellator()
{
	clear_heavy_geometry_data();
}


void voronoiTessellator::clear_heavy_geometry_data()
{
	for( size_t i = 0; i < cell_geometry.size(); i++ ) {
		if ( cell_geometry[i].cell_fvert != NULL ) {
			delete cell_geometry[i].cell_fvert; cell_geometry[i].cell_fvert = NULL;
		}
		if ( cell_geometry[i].cell_verts != NULL ) {
			delete cell_geometry[i].cell_verts; cell_geometry[i].cell_verts = NULL;
		}
	}
	cell_geometry = vector<cell_geom>();
}


p3i voronoiTessellator::identify_blockpartitioning( const size_t p_total, const apt_int p_perblock_target, aabb3d const & roi )
{
	p3i out = p3i(1, 1, 1);
	if ( roi.xsz > EPSILON ) {
		double yrel = (double) roi.ysz / (double) roi.xsz;
		double zrel = (double) roi.zsz / (double) roi.xsz;
		double ix = pow( ((double) p_total / (double) p_perblock_target) / (yrel * zrel), (1./3.) );
		double iy = yrel * ix;
		double iz = zrel * ix;
		out.x = ( (apt_int) floor(ix) > 0 ) ? (apt_int) floor(ix) : 1;
		out.y = ( (apt_int) floor(iy) > 0 ) ? (apt_int) floor(iy) : 1;
		out.z = ( (apt_int) floor(iz) > 0 ) ? (apt_int) floor(iz) : 1;
	}
	return out;
}


wallstats voronoiTessellator::identify_cell_wallcontact( vector<int> const & nbors )
{
	//does cell make boundary contact or not ? (indicated by negative neighbor
	//numeral info of neighbor details which domain wall cuts the cell into a potential incorrect geometry
	wallstats out = wallstats();
	for( auto it = nbors.begin(); it != nbors.end(); ++it ) {
		if ( *it >= 0 ) {
			continue;
		}
		else { //a cell which has boundary contact
			int thisone = *it; //abs(*it); //-1 to 1, -2 to 2, and so forth, Voro++ domain wall neighbors are -1,-2,-3,-4,-5,-6
			//Voro++ uses in such a case the following flags for a cuboidal box
			switch (thisone)
			{
				case -1:
				{
					out.xmi_touch = true;	out.any_touch = true;	out.halo_insufficient = true; break;
				}
				case -2:
				{
					out.xmx_touch = true;	out.any_touch = true;	out.halo_insufficient = true; break;
				}
				case -3:
				{
					out.ymi_touch = true;	out.any_touch = true;	out.halo_insufficient = true; break;
				}
				case -4:
				{
					out.ymx_touch = true;	out.any_touch = true;	out.halo_insufficient = true; break;
				}
				//##MK::we use a partitioning into threadlocal z domains of a global nonperiodic tess
				//##MK::hence check additionally whether haloregion was sufficient for
				//truncating the cell only through the halo regions and not the domain walls
				//##MK::this consistency check identifies whether cells close to the
				//threadlocal domain boundaries have correct geometry
				case -5:
				{
					out.zmi_touch = true;	out.any_touch = true;	out.halo_insufficient = true; break;
				}
				//if ( haloinfo.zmi_ishalo == true && out.halo_insufficient == false ) {
				//	out.halo_insufficient = true;
				//}
				case -6:
				{
					out.zmx_touch = true;	out.any_touch = true;	out.halo_insufficient = true; break;
				}
				//if ( haloinfo.zmx_ishalo == true && out.halo_insufficient == false ) {
				//	out.halo_insufficient = true;
				//}
				default:
					break;
			}
		}
	} //all neighbors have to be tested to know for sure with which boundaries we make contact
	return out;
}


bool voronoiTessellator::compute_voronoi_tessellation( vector<p3dm1> const & points_with_mark, aabb3d & box )
{
cout << "Compute vtess points_with_mark.size() " << points_with_mark.size() << "\n";
cout << "box " << box << "\n";

	//5.0 as target value based on empirical result by Rycroft, http://math.lbl.gov/voro++/examples/timing_test/
	if ( points_with_mark.size() >= (size_t) (I32MX-1) ) {
		cerr << "The point cloud passed to voronoiTessellator is has a size of "
				"larger than " << (size_t) I32MX-2 << ", this is currently not supported !" << "\n"; return false;
	}

	//blockpartitioning is the essential trick in Voro++ to reduce spatial querying costs
	//too few blocks too many points to test against
	//too many blocks too much memory overhead and cache trashing (L1,L2 and not to forget "page" TLB cache)
	p3i blocks = identify_blockpartitioning( points_with_mark.size(), ConfigIntersector::DefaultPointsPerBlock, box );
	bool periodicbox = false;

cout << "Blocks " << blocks << "\n";

	container con(	box.xmi - EPSILON, box.xmx + EPSILON,
					box.ymi - EPSILON, box.ymx + EPSILON,
					box.zmi - EPSILON, box.zmx + EPSILON,
					blocks.x, blocks.y, blocks.z,
					periodicbox, periodicbox, periodicbox, ConfigIntersector::DefaultPointsPerBlock );

	//buildstats stats = buildstats( blocks.x, blocks.y, blocks.z, ionportion, con.total_particles() );

	//MK::we demand that the entire set of points becomes tessellated
	//MK::then we can fill vectors in the order in which cells are processed instead of more costly dictionaries of cell to value mappings

	//MK::mark specifies for instance thermodynamic phase assigned to an ion because ion is part of a cluster
	//which should represent a precipitate of a thermodynamic phase
	cell_mark = map<int,apt_uint>();
	int i = 0; //temporary ion ID to map ion position in the container to the implicit order of the above linear walk through along ionpp1 and ionifo data arrays
	for( auto it = points_with_mark.begin(); it != points_with_mark.end(); it++ ) {
		//internally, Voro++ uses double precision, we could in the future also work with CGAL as an alternative

		if ( box.is_inside( *it ) == false ) { //assume that box is substantially smaller than volume of the entire point cloud, then this is the more efficient take than testing for == true
			continue;
		}
		else {
			con.put( i, (double) it->x, (double) it->y, (double) it->z );
			cell_mark.insert( pair<int,apt_uint>(i, it->m) );
			i++;
		}
	}

cout << "Initialized the container with " << i << " points" << "\n";

	//double cluster_vol[2] = {0., 0.};

	cell_clpid_volume = map<int,double>();
	cell_nbors_ival = map<int,pair<size_t,size_t>>();
	cell_nbors = vector<int>();
	//cell_label = vector<apt_uint>();
	//cell_vertices = vector<p3d>();
	//cell_vertices = vector<double>();
	//cell_topo = vector<unsigned int>();
	//stores the below processing order of clpids because Voro++ does not process cells like
	//clpid=0; clpid <nmax; clpid but based on the arrangement in the container
	cell_clpid = vector<int>(); //MK::this maps ids to clpids given the assumption that all cells have to be processed!

	size_t nbidx = 0;
	apt_uint vrtxidx = 0;
	c_loop_all cl(con);
	voronoicell_neighbor c;
	if (cl.start()) { //this is an incremental computing of the tessellation which holds at no point the entire tessellation
		do {
			//Voro++ is not order conserving inasmuch as it not guaranteed that the iteration over cl.start() to end() machines
			//off the cells to ions i in ascending order we should not assume an order
			int clpid = cl.pid();
			if ( con.compute_cell(c, cl) == true ) { //an ion for which the tessellation was successful

				//characterize volume of the cell
				//double vol = c.volume(); //MK::cell volume
				cell_clpid.push_back(clpid);

				cell_clpid_volume.insert( pair<int,double>(clpid, c.volume()) );

				//characterize nearest neighbors of the cell, cell ids !
				vector<int> neigh; //MK::number of first order neighbors including negative values in case of domain contact
				c.neighbors(neigh);
				cell_nbors.insert( cell_nbors.end(), neigh.begin(), neigh.end());
				cell_nbors_ival.insert( pair<int,pair<size_t,size_t>>(
						clpid, pair<size_t,size_t>(nbidx, nbidx + neigh.size())) );
				nbidx += neigh.size();

				//characterize eventual boundary contact of the cell
				wallstats health = identify_cell_wallcontact( neigh );

				//characterize center of the cell
				double x, y, z;
				cl.pos(x, y, z);

				//##MK::use vectorComparator

				//characterize convex polyhedron surface facet for visualizing the cell
				vector<int> fvert; //MK::vertex indices
				c.face_vertices(fvert);
				vector<double> verts; //MK::vertex coordinate values, not coordinate triplets!
				c.vertices(x, y, z, verts);

				vector<int>* fvert_buf = new vector<int>;
				vector<double>* verts_buf = new vector<double>;
				*(fvert_buf) = fvert;
				*(verts_buf) = verts;
				cell_geometry.push_back( cell_geom(clpid, fvert_buf, verts_buf) );

				////##MK::BEGIN DEBUG VISUALIZATION
				////get number of unique float triplets
				////http://math.lbl.gov/voro++/examples/polygons/
				//int j = 0;
				//for( size_t i = 0; i < neigh.size(); i++ ) { //O(N) processing
				//	cell_topo.push_back((unsigned int) 3); //XDMF keyword to visualize an n-polygon
				//	cell_topo.push_back((unsigned int) fvert[j]); //how many edges on the polygon? promotion uint32 to size_t no problem
				//	for( int k = 0; k < fvert[j]; k++) { //generating 3d points with implicit indices 0, 1, 2, ....
				//		//the structure of an fvert array is a contiguous set of blocks, neigh.size() blocks i.e. number of non-degenerate facets
				//		//each block has first the number of vertices per facet, followed by indices which double numbers to use in verts to compose a 3d coordinate triplet
				//		//the structure of an verts array is a collection of double triplets for vertex coordinates supporting the the facet polygons
				//		int l = 3*fvert[j+k+1];
				//		//cell_vertices.push_back( p3d((apt_real) verts[l], (apt_real) verts[l+1], (apt_real) verts[l+2]) );
				//		cell_vertices.push_back(verts[l+0]);
				//		cell_vertices.push_back(verts[l+1]);
				//		cell_vertices.push_back(verts[l+2]);
				//		//##MK::narrowing conversion from double to float suffices for visualization purposes but not necessarily for subsequent processing of the Voronoi cell polyhedra !
				//		cell_topo.push_back((unsigned int) vrtxidx);
				//		vrtxidx++;
				//	}
				//	cell_label.push_back((unsigned int) cell_mrk); //duplicating of labels for each polygon required for XDMF because for mixed xdmf topologies the cell is the polygon not the polyhedron !
				//	j += fvert[j] + 1;
				//}
				////##MK::END DEBUG VISUALIZATION

				//characterize cell thermodynamic phase
				apt_uint cell_mrk = cell_mark[clpid]; //at((size_t) clpid);
				//cell_label.push_back(cell_mrk);

				//if ( cell_mrk == 130 ) {
				//	cluster_vol[0] += vol;
				//}
				//if ( cell_mrk == 133 ) {
				//	cluster_vol[1] += vol;
				//}
				//cout << "Cell i " << clpid << " evapID " << points_with_mark.at(clpid).m <<
				//		" volume " << vol << " nbors " << first_neigh.size() << " halo_insufficient " << (int) health.halo_insufficient << "\n";

				if ( cell_mrk != UNKNOWNTYPE ) {
					if ( health.halo_insufficient == true ) {
						cerr << "Voro++::cl.pid() " << clpid << " we figured that a cell of an important feature is not properly represented !" << "\n";
						return false;
					}
				}
			}
			else {
				cerr << "Voro++::cl.pid() " << clpid << " was not computable !" << "\n";
			}
		} while (cl.inc());
	}

	//cout << "Tessellation performed" << "\n";
	//cout << "cell_clpid_volume.size() " << cell_clpid_volume.size() << "\n";
	//cout << "cell_nbors_ival.size() " << cell_nbors_ival.size() << "\n";
	//cout << "cell_nbors.size() " << cell_nbors.size() << "\n";
	//cout << "cell_label.size() " << cell_label.size() << "\n";
	//cout << "cell_vertices.size() " << cell_vertices.size() << "\n";
	//cout << "cell_topo.size() " << cell_topo.size() << "\n";

	//##MK::DEBUG visualization
	//h5seqHdl h5w = h5seqHdl();
	//h5w.create_file( "PARAPROBE.Intersector.Voronoi.Debug.h5");
	//string dsnm = "";
	//dsnm = "/VertexPosition";
	////vector<apt_real> r;
	////r.reserve( 3*cell_vertices.size() );
	////for( auto it = cell_vertices.begin(); it != cell_vertices.end(); it++ ) {
	////	r.push_back(it->x); r.push_back(it->y); r.push_back(it->z);
	////}
	////if ( h5w.add_contiguous_array2d( dsnm, H5_F32, r.size()/3, 3, r ) != MYHDF5_SUCCESS ) {
	//if ( h5w.add_contiguous_array2d( dsnm, H5_F64, cell_vertices.size()/3, 3, cell_vertices ) != MYHDF5_SUCCESS ) {
	//	cerr << "Writing " << dsnm << " failed !" << "\n";
	//}
	////r = vector<apt_real>();
	//dsnm = "/Topology";
	//if ( h5w.add_contiguous_array2d( dsnm, H5_U32, cell_topo.size()/1, 1, cell_topo ) != MYHDF5_SUCCESS ) {
	//	cerr << "Writing " << dsnm << " failed !" << "\n";
	//}
	//dsnm = "/CellLabel";
	//if ( h5w.add_contiguous_array2d( dsnm, H5_U32, cell_label.size()/1, 1, cell_label ) != MYHDF5_SUCCESS ) {
	//	cerr << "Writing " << dsnm << " failed !" << "\n";
	//}
	////##MK::END DEBUG

	//post-processing spatial filtering and microstructural object compositing from Voronoi cell based on adjacency relations
	//MK::report me all Voronoi cells with label A which have at least one kth=1 (nearest) neighbor of label 0 --> gives me the outer Voronoi "shell" of object A touching region 0
	//MK::report me all Voronoi cells with label A which have at least one kth=1 (nearest) neighbor of label not 0 but B or C --> gives me the outer Voronoi "shell" of object A touching objects B, C
	//MK::report me all Voronoi cells with label A which have only kth=1 neighbors of label A --> gives me the complement interior Voronoi region of object A
	//MK::the union of the three previous Voronoi cell sets is the Voronoi cell set representing all cells with label A
	vector<apt_uint> lbl_i = { 130 };
	unordered_set<int> cells_130;
	filter_cells_with_specific_labels( lbl_i, cells_130 ); //all cells building cluster 130
cout << "cells_130.size() " << cells_130.size() <<  "\n";
	lbl_i = { 133 };
	unordered_set<int> cells_133;
	filter_cells_with_specific_labels( lbl_i, cells_133 ); //all cells building cluster 133
cout << "cells_133.size() " << cells_133.size() <<  "\n";
	lbl_i = { UNKNOWNTYPE };
	unordered_set<int> cells_vac;
	filter_cells_with_specific_labels( lbl_i, cells_vac ); //all cells building the environment of Voronoi cells into which cluster 130 and 133 are embedded
cout << "cells_vac.size() " << cells_vac.size() <<  "\n";

	//cells of one label in label set lbl_i which have at least m_i neighbors inside the shells including up to the k_i-th nearest neighbor of one label in label set lbl_j
	lbl_i = { UNKNOWNTYPE, 130, 133 };
	vector<apt_uint> lbl_j = { 130, 133 };
	apt_uint m_i = 1;
	apt_uint k_i = 2;
	//all cells which have at least one of both cells in lbl_j in their second or first nearest neighbor shell
	unordered_set<int> bridge_130_to_133;
	//##MK::currently m_i = 1, k_i = 2,
	filter_cells_withcells_with_spec_labels_and_specific_higherorder_neighbors( lbl_i, lbl_j, bridge_130_to_133 );
cout << "bridge_130_to_133.size() " << bridge_130_to_133.size() << "\n";

	if ( export_cells_for_debugging_h5( bridge_130_to_133 ) == true ) {
		//
	}
	else {
		cerr << "Exporting cells for debugging failed !" << "\n"; return false;
	}

	//cout << "Volume cluster 130 " << cluster_vol[0] << " nm^3" << "\n";
	//cout << "Volume cluster 133 " << cluster_vol[1] << " nm^3" << "\n";

	return true;
}


bool voronoiTessellator::check_for_unphysical_truncation()
{
	//#########
	return true;
}


void voronoiTessellator::get_cell_nearest_neighbors()
{
	//#####
}


void voronoiTessellator::get_cell_geometry()
{
	//#####
}


void voronoiTessellator::filter_cells_with_specific_labels(
		vector<apt_uint> const & trgs, unordered_set<int> & out )
{
	out = unordered_set<int>();
	for( auto it = cell_mark.begin(); it != cell_mark.end(); it++ ) {
		apt_uint i_mark = it->second;
		for( auto jt = trgs.begin(); jt != trgs.end(); jt++ ) {
			if ( i_mark != *jt ) {
				continue;
			}
			else {
				out.insert(it->first); //returning a clpid !
				break;
			}
		}
	}
}


void voronoiTessellator::filter_cells_withcells_with_spec_labels_and_specific_higherorder_neighbors(
		vector<apt_uint> const & trgs, vector<apt_uint> const & admissible_nbors, unordered_set<int> & out )
//here exemplarily implemented for necessary_cnts = 1 and admissible_korder = 2 const apt_uint necessary_cnts, const apt_uint admissible_korder,
{
	out = unordered_set<int>();
	//query cells
	//if ( cell_mark.size() != cell_clpid.size() ) {
	//	cerr << "cell_mark.size() != cell_clpid.size() !" << "\n";
	//	return;
	//}

	int ni = (int) cell_clpid.size();
	for( int i = 0; i < ni; i++ ) {
		int clpid = cell_clpid[i];
		//##MK::one could build a BVH of cells and mark values to speed these things up further but here we want a proof-of-concept
		//##MK::also a maybe boost-based iterative graph processing approach would eventually be better and maybe faster, anyway
		apt_uint i_mark = cell_mark[clpid];
		for( auto it = trgs.begin(); it != trgs.end(); it++ ) {
			if ( i_mark != *it ) {
				continue;
			}
			else { //cell with clpid i has a mark we hunt after, but there are additional constraints before we accept the cell...
				//query now the cell_indices of all its neighbors up to order admissible_korder
				//and check if these cells have a mark in admissible_nbors
				set<int> nbors;
				map<int,pair<size_t,size_t>>::iterator kth_one = cell_nbors_ival.find(clpid); //first nearest neighbors of cell with id clpid
				if ( kth_one != cell_nbors_ival.end() ) {
					for( size_t idx_one = kth_one->second.first; idx_one < kth_one->second.second; idx_one++ ) {
						int clpid_one = cell_nbors[idx_one];
						if ( clpid_one >= 0 ) { //no wall contact additions!
							nbors.insert(clpid_one);
							map<int,pair<size_t,size_t>>::iterator kth_two = cell_nbors_ival.find(clpid_one);
							if ( kth_two != cell_nbors_ival.end() ) {
								for( size_t idx_two = kth_two->second.first; idx_two < kth_two->second.second; idx_two++ ) {
									int clpid_two = cell_nbors[idx_two];
									if ( clpid_two >= 0 ) {
										nbors.insert(clpid_two);
									}
								}
							}
						}
					}
				}
				//all neighbors found including myself
				//we can process myself as well another time because an insert into the unordered_set will not add it an nth time once it is in the set already!
				//avoid us to check for every neighbor if we process cell i

				map<apt_uint,apt_uint> admissible_nbors_cnts;
				for( auto mkit = admissible_nbors.begin(); mkit != admissible_nbors.end(); mkit++ ) {
					admissible_nbors_cnts.insert(pair<apt_uint,apt_uint>(*mkit,0));
				}

				//not all neighbors count because nbors need to be of a certain type
				//and we want to find at least a certain number of neighbors with a certain category
				for( auto nbclpit = nbors.begin(); nbclpit != nbors.end(); nbclpit++ ) {
					if ( *nbclpit != clpid ) {
						apt_uint nb_mark = cell_mark[*nbclpit];
						for( auto jt = admissible_nbors.begin(); jt != admissible_nbors.end(); jt++ ) {
							if ( nb_mark != *jt ) {
								continue;
							}
							else {
								//yes of admissible type
								admissible_nbors_cnts[nb_mark]++;
								break;
							}
						}
					}
				}

				//do we have a sufficient number of neighboring cells with marks as defined in admissible_nbors, ##MK::here hard-coded for debug purpose to have at least one of each !
				//if cell i qualifies to become included
				bool include = true;
				for( auto kt = admissible_nbors.begin(); kt != admissible_nbors.end(); kt++ ) {
					if ( admissible_nbors_cnts[*kt] < 1 ) {
						include = false;
						break;
					}
				}
				if ( include == true ) {
					out.insert(clpid);
				}
				break; //because no need to test if cell i has another mark a cell by definition can have only one mark !
			}
		} //next, is i of a target mark?
	} //next cell

	//MK::remember we do not return clpid here !! but indices on cell_mark
}


bool voronoiTessellator::export_cells_for_debugging_h5( unordered_set<int> const & trgs )
{
	double tic = MPI_Wtime();
	double toc = 0.;

	vector<unsigned int> u32_topo;
	vector<apt_real> r_verts;
	vector<int> i32_clpid;
	vector<unsigned int> u32_mark;

	//apt_uint vrtxidx = 0;

	map<Eigen::Vector3d,apt_uint,vectorComparator> unique_vertices;
	apt_uint cur_vertex_id = 0;

	for( auto clpit = trgs.begin(); clpit != trgs.end(); clpit++ ) {
		//int idx = *it;
		int clpid = *clpit; //cell_clpid[idx];
		unsigned int mrk = (unsigned int) cell_mark[clpid];

		//naive search for visualization data
		for( auto jt = cell_geometry.begin(); jt != cell_geometry.end(); jt++ ) {
			if ( clpid != jt->clpid ) {
				continue;
			}
			else {
				int j = 0;
				size_t nii = cell_nbors_ival[clpid].second - cell_nbors_ival[clpid].first;
				for( size_t ii = 0; ii < nii; ii++ ) {
					u32_topo.push_back((unsigned int) 3); //XDMF keyword to visualize an n-polygon
					u32_topo.push_back((unsigned int) jt->cell_fvert->at(j)); //how many edges on the polygon? promotion uint32 to size_t no problem
					for( int k = 0; k < jt->cell_fvert->at(j); k++) { //generating 3d points with implicit indices 0, 1, 2, ....
						//the structure of an fvert array is a contiguous set of blocks, neigh.size() blocks i.e. number of non-degenerate facets
						//each block has first the number of vertices per facet, followed by indices which double numbers to use in verts to compose a 3d coordinate triplet
						//the structure of an verts array is a collection of double triplets for vertex coordinates supporting the the facet polygons
						int l = 3*jt->cell_fvert->at(j+k+1);
						Eigen::Vector3d v(	(apt_real) jt->cell_verts->at(l+0),
											(apt_real) jt->cell_verts->at(l+1),
											(apt_real) jt->cell_verts->at(l+2) );
						map<Eigen::Vector3d,apt_uint>::iterator vit = unique_vertices.find(v);
						if ( vit != unique_vertices.end() ) {
							r_verts.push_back((apt_real) vit->first.x());
							r_verts.push_back((apt_real) vit->first.y());
							r_verts.push_back((apt_real) vit->first.z());
							u32_topo.push_back((unsigned int) vit->second);
						}
						else {
							r_verts.push_back((apt_real) v.x());
							r_verts.push_back((apt_real) v.y());
							r_verts.push_back((apt_real) v.z());
							unique_vertices.insert( pair<Eigen::Vector3d,apt_uint>(v, cur_vertex_id) );
							u32_topo.push_back(cur_vertex_id);
							cur_vertex_id++;
						}

						////cell_vertices.push_back( p3d((apt_real) verts[l], (apt_real) verts[l+1], (apt_real) verts[l+2]) );
						//r_verts.push_back((apt_real) jt->cell_verts->at(l+0));
						//r_verts.push_back((apt_real) jt->cell_verts->at(l+1));
						//r_verts.push_back((apt_real) jt->cell_verts->at(l+2));
						////##MK::narrowing conversion from double to float suffices for visualization purposes but not necessarily for subsequent processing of the Voronoi cell polyhedra !
						//u32_topo.push_back((unsigned int) vrtxidx);
						//vrtxidx++;

					}
					i32_clpid.push_back(clpid);
					u32_mark.push_back(mrk); //duplicating of labels for each polygon required for XDMF because for mixed xdmf topologies the cell is the polygon not the polyhedron !
					j += jt->cell_fvert->at(j) + 1;
				}
				break; //we found the cell
			}
		} //
	}

	h5seqHdl h5w = h5seqHdl();
	h5w.create_file( "PARAPROBE.Intersector.Results.SimID.3.Bridge130to133.h5");

	string dsnm = "";
	dsnm = "/VertexPosition";
	if ( h5w.add_contiguous_array2d( dsnm, H5_F32, r_verts.size()/3, 3, r_verts ) != MYHDF5_SUCCESS ) {
		cerr << "Writing " << dsnm << " failed !" << "\n";
	}
	r_verts = vector<apt_real>();
	dsnm = "/Topology";
	if ( h5w.add_contiguous_array2d( dsnm, H5_U32, u32_topo.size()/1, 1, u32_topo ) != MYHDF5_SUCCESS ) {
		cerr << "Writing " << dsnm << " failed !" << "\n";
	}
	dsnm = "/CellID";
	if ( h5w.add_contiguous_array2d( dsnm, H5_I32, i32_clpid.size()/1, 1, i32_clpid ) != MYHDF5_SUCCESS ) {
		cerr << "Writing " << dsnm << " failed !" << "\n";
	}
	dsnm = "/CellMark";
	if ( h5w.add_contiguous_array2d( dsnm, H5_I32, u32_mark.size()/1, 1, u32_mark ) != MYHDF5_SUCCESS ) {
		cerr << "Writing " << dsnm << " failed !" << "\n";
	}

	toc = MPI_Wtime();
	cout << "Voronoi composition debugging took " << (toc-tic) << " s" << "\n";

	return true;
}
*/

