/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_INTERSECTOR_TESSELLATE_HDL_H__
#define __PARAPROBE_INTERSECTOR_TESSELLATE_HDL_H__

#include "PARAPROBE_IntersectorTetrahedralize.h"

//##MK::this part of the code will no longer be maintained, instead such functionalities should be moved to paraprobe-tessellator
//the code given here is only an example how something like this can be achieved via computing Voronoi tessellations and performing topological analyses on these


//vector<p3dm1> recon_curr;

/*
struct wallstats
{
	//captures logical information which threadlocal domain walls cut the cell
	bool xmi_touch;
	bool xmx_touch;
	bool ymi_touch;
	bool ymx_touch;
	bool zmi_touch;
	bool zmx_touch;
	bool any_touch;
	bool halo_insufficient;		//if true indicates that halo was insufficient and at least
	//if halo_insufficient == true re-tessellation with larger halo guard width is required
	wallstats() : 	xmi_touch(false), xmx_touch(false),
					ymi_touch(false), ymx_touch(false),
					zmi_touch(false), zmx_touch(false),
					any_touch(false), halo_insufficient(false) {}
	wallstats(const bool _xmi, const bool _xmx, const bool _ymi, const bool _ymx,
			const bool _zmi, const bool _zmx, const bool _any, const bool _haloproblem ) :
				xmi_touch(_xmi), xmx_touch(_xmx),
				ymi_touch(_ymi), ymx_touch(_ymx),
				zmi_touch(_zmi), zmx_touch(_zmx),
				any_touch(_any), halo_insufficient(_haloproblem) {}
};

ostream& operator << (ostream& in, wallstats const & val);


struct cell_geom
{
	int clpid;
	vector<int>* cell_fvert;
	vector<double>* cell_verts;
	cell_geom() : clpid(I32MI), cell_fvert(NULL), cell_verts(NULL) {}
	cell_geom( const int _clpid, vector<int>* ptfvert, vector<double>* ptverts )
		: clpid(_clpid), cell_fvert(ptfvert), cell_verts(ptverts) {}
};


class voronoiTessellator
{
public:
	voronoiTessellator();
	~voronoiTessellator();

	void clear_heavy_geometry_data();
	p3i identify_blockpartitioning( const size_t p_total, const apt_int p_perblock_target, aabb3d const & roi );
	wallstats identify_cell_wallcontact( vector<int> const & nbors );

	bool compute_voronoi_tessellation( vector<p3dm1> const & points_with_mark, aabb3d & box );
	bool check_for_unphysical_truncation();
	void get_cell_nearest_neighbors();
	void get_cell_geometry();

	void filter_cells_with_specific_labels( vector<apt_uint> const & trgs, unordered_set<int> & out ); //all cells building cluster 130
	void filter_cells_withcells_with_spec_labels_and_specific_higherorder_neighbors(
			vector<apt_uint> const & trgs, vector<apt_uint> const & admissible_nbors, unordered_set<int> & out );
	//currently hard-coded for necessary_cnts = 1, admissible_korder = 2 const apt_uint necessary_cnts, const apt_uint admissible_korder,

	bool export_cells_for_debugging_h5( unordered_set<int> const & trgs );

	//neighbors are stored in [cell_nbors[first], cell_nbors[second]), so second is one past the end
	map<int,apt_uint> cell_mark; //either UNKNOWN or with IDs of target clusters, indices on this array match with the clpid of the cells starting from [0 with values up to cell_mark.size()-1]
	map<int,double> cell_clpid_volume;
	map<int,pair<size_t,size_t>> cell_nbors_ival;
	vector<int> cell_nbors;
	//vector<apt_uint> cell_label;
	//vector<p3d> cell_vertices;
	//vector<double> cell_vertices;
	//vector<unsigned int> cell_topo;

	vector<cell_geom> cell_geometry;

private:
	vector<int> cell_clpid;
};
*/


//OPTIONAL TESSELLATION ANALYSIS
		/*
		if ( ConfigIntersector::OverlapMethod == TETGEN_TETRAHEDRALIZE && ovrlp_tskit->IOAnalyzeTessellationCurr == true ) {
			double ctic = omp_get_wtime();

			//##MK::debug which objects should be analyzed?
			vector<apt_uint> curr_roiID_set = { 130, 133 }; //##MK::roiID, i.e. object "name", not curridx i.e. not array index on objs_curr !

			//load reconstructed ion positions, assign them UNKNOWN ID
			if ( read_reconstruction_input_current( *ovrlp_tskit ) == true ) {
				//
			}
			else {
				cerr << "Loading the ion position of the current reconstruction failed !" << "\n"; return;
			}

			//load evaporation IDs/ ion indices for current object
			if ( read_evaporation_ids_input_current( *ovrlp_tskit, curr_roiID_set ) == true ) {
				//
			}

			//for all ions with evaporationID in any object of curr_set replace label UNKNOWNID with the respective ROIID
			//build a helper dictionary to map from objs_curr array index to roiID
			//get abbb of this point cloud
			map<apt_uint,size_t> objidx2roiid;
			for( size_t i = 0; i < objs_curr.size(); i++ ) {
				apt_uint roiid = objs_curr[i].msh->roiID;
				objidx2roiid.insert( pair<apt_uint,size_t>(roiid, i) );
			}
			//now we dont need naive searches we can just check in the dictionary which id we need to query to query evaporation ids of target objects
			//filter all ions from reconstruction of the objects in curr_set with ID ROIID
			aabb3d box_about_targets = aabb3d();
			for( auto trgit = curr_roiID_set.begin(); trgit != curr_roiID_set.end(); trgit++ ) {
				map<apt_uint,size_t>::iterator thisone = objidx2roiid.find( *trgit );
				if ( thisone != objidx2roiid.end() ) {
					for( auto evapit = objs_curr[thisone->second].obj_support_pids.begin(); evapit != objs_curr[thisone->second].obj_support_pids.end(); evapit++ ) {
						//mark that this ion belongs not to the UNKNOWNTYPE but to a particular object with roiID *evapit
						if ( recon_curr.at(*evapit).m == UNKNOWNTYPE ) {
							recon_curr[*evapit].m = *trgit;
							//make use of the already likely stored position information of the ion in the cache
							box_about_targets.possibly_enlarge_me(recon_curr[*evapit]);
						}
						else {
							cerr << "There is an unexpected inconsistence in so far that an ion seems to be part "
									"of at least two objects of interest, currently this is not supported !" << "\n"; return;
						}
					}
				}
				else {
					cerr << "Unable to find an associated dataset for object " << *trgit << "\n"; return;
				}
			}
			//now the box_about_targets includes all points of the targets, add tesshalo halo region to this aabb on each side
			box_about_targets.scale();
			box_about_targets.add_guard( ovrlp_tskit->tesshalo );
			box_about_targets.add_epsilon_guard();
			box_about_targets.scale();

			//refilter to check which new ions we have in the now guarded aabb
			//compute Voronoi tessellation for each ion in the guarded aabb, assign each cell the corresponding ID (either ROIID or UNKNOWNID)

			voronoiTessellator vtess;
			if ( vtess.compute_voronoi_tessellation( recon_curr, box_about_targets ) == true ) {

				//check if all Voronoi cells to ions with ROI IDs did not intersect the boundary
				if ( vtess.check_for_unphysical_truncation() == true ) {

					//compute nearest neighbors of each Voronoi cell and get their disjoint vertices (to reduce storage size) and facets
					vtess.get_cell_nearest_neighbors();
					vtess.get_cell_geometry();

					//export the cells for rendering and verification purposes
					//##MK::#########debug rendering
					//if ( vtess.classify cells according to filter constraints discussed in
					//	CONFIG_Intersector.h i.e. cells with label <IDi>with
					//	at least m kth nearest neighbors of label <IDj>
					//##MK::########output result
				}
				else {
					cerr << "We found that the guard zone was not wide enough to prevent that relevant cells "
						"are bisected inside the box, i.e. some cells have relevant wall contact !" << "\n"; return;
				}
			}
			else {
				cerr << "We found that we were unable to compute the Voronoi tessellation !" << "\n"; return;
			}

			double ctoc = omp_get_wtime();
			cout << "Co-precipitation analysis took " << (ctoc-ctic) << " s" << "\n";
		}
		*/

#endif
