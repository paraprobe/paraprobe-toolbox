/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_IntersectorTetrahedralize.h"


tetrahedralizer::tetrahedralizer()
{
	is_tessellated = false;
	dt_tessellate = 0.;
}


tetrahedralizer::~tetrahedralizer()
{

}


//example code how an inputted triangulated surface mesh was tessellated using TetGen
/*
if( ConfigIntersector::OverlapMethod == TETGEN_TETRAHEDRALIZE ) {
	//compute tetrahedralization of the object
	cout << "About tessellating object " << obj_id << " of current set " << info.grpnm_obj_geom_curr << "\n";
	tetrahedralizer mytetgen;
	bool tetgen_status = mytetgen.tessellate( ply_mesh->vrts_surface, ply_mesh->fcts_surface, ply_mesh->vrts_volume, ply_mesh->cells_volume );
	if ( tetgen_status == false ) {
		cerr << "Tetrahedralizing object " << obj_id << " failed, WARNING object " << obj_id << " will not be included in curr ensemble !" << "\n";
		delete ply_mesh; ply_mesh = NULL; continue;
	}

	//compute axis-aligned bounding box (AABB) to the polyhedron
	aabb3d container = aabb3d();
	for( auto it = ply_mesh->vrts_volume.begin(); it != ply_mesh->vrts_volume.end(); it++ ) {
		container.possibly_enlarge_me( *it );
	}
	container.add_epsilon_guard();
	container.scale();
	ply_mesh->bvh_volume = container;

	//equip each polyhedron with a bounded volume hierarchy of its tetrahedra
	ply_bvh = new Tree( ply_mesh->cells_volume.size() );
	if ( ply_bvh != NULL ) {
		for( size_t tetID = 0; tetID < ply_mesh->cells_volume.size(); tetID++ ) {
			aabb3d cur_tet_in_a_box = aabb3d();
			cur_tet_in_a_box = ply_mesh->get_aabb( tetID );
			cur_tet_in_a_box.add_epsilon_guard();
			cur_tet_in_a_box.scale();

			ply_bvh->insertParticle( (apt_uint) tetID,
					trpl(cur_tet_in_a_box.xmi, cur_tet_in_a_box.ymi, cur_tet_in_a_box.zmi),
					trpl(cur_tet_in_a_box.xmx, cur_tet_in_a_box.ymx, cur_tet_in_a_box.zmx) );
		}
	}
	else {
		cerr << "BVH building of tetrahedralized object " << obj_id << " failed, WARNING object " << obj_id << " will not be included in curr ensemble !" << "\n";
		delete ply_mesh; ply_mesh = NULL; continue;
	}
}
else {
*/

//example code for checking the tetrahedron-tessellation of the each polyhedron against an aabb for overlaps
/*
if ( ConfigIntersector::OverlapMethod == TETGEN_TETRAHEDRALIZE ) {
	//next, more costly intersection test layer:
	//cout << "tetgen_tetrahedralize, objs_next_candidx.size() " << objs_next_candidx.size() << "\n";
	for( auto candidxt = objs_next_candidx.begin(); candidxt != objs_next_candidx.end(); candidxt++ ) {

		//check if at least one tetrahedron of the current obj intersects with tetrahedra of object next, use a coarse AABB test first
		bool curr_intersects_with_cand = false;

		for( size_t tet_curr_idx = 0; tet_curr_idx < objs_curr[curidx].msh->cells_volume.size(); tet_curr_idx++ ) {

			if ( curr_intersects_with_cand == false ) { //most likely we need to test at least some tetrahedra of cur for intersection with tets of cand next
				roi_tetrahedron tet_curr = objs_curr[curidx].msh->get_tetrahedron( tet_curr_idx );
				hedgesAABB cur_tet_bvh = objs_curr[curidx].bvh->getAABB( tet_curr_idx );

				vector<apt_uint> obj_next_cand_tet_cand = objs_next[*candidxt].bvh->query( cur_tet_bvh );

				////##MK::begin debug
				//cout << "testing " << *candidxt << " tet_curr_idx " << tet_curr_idx << "\n";
				//for( size_t kk = 0; kk < obj_next_cand_tet_cand.size(); kk++ ) {
				//	cout << obj_next_cand_tet_cand[kk] << " ";
				//}
				//cout << "\n";
				////##MK::end debug

				//most costly intersection test layer:
				//check which of the above tetrahedron candidate really overlap doing the costly tetrahedron-tetrahedron intersection test
				for( size_t kidx = 0; kidx < obj_next_cand_tet_cand.size(); kidx++ ) {

					apt_uint tet_next_idx = obj_next_cand_tet_cand[kidx];
					roi_tetrahedron tet_next = objs_next[*candidxt].msh->get_tetrahedron(tet_next_idx);

					tetOverlap tetrahedra_pair_intersection;
					sat_tests++;
					if ( tetrahedra_pair_intersection.tet_disjoint_sat( tet_curr, tet_next ) == true ) {
						////##MK::begin debug
						//if ( tet_next_idx == tet_curr_idx ) {
						//	cout << "Testing " << tet_next_idx << " vs " << tet_curr_idx << " disjoint " << "\n";
						//	cout << setprecision(32) << tet_curr.v1.x << " " << setprecision(32) << tet_curr.v1.y << " " << setprecision(32) << tet_curr.v1.z << "\n";
						//	cout << setprecision(32) << tet_curr.v2.x << " " << setprecision(32) << tet_curr.v2.y << " " << setprecision(32) << tet_curr.v2.z << "\n";
						//	cout << setprecision(32) << tet_curr.v3.x << " " << setprecision(32) << tet_curr.v3.y << " " << setprecision(32) << tet_curr.v3.z << "\n";
						//	cout << setprecision(32) << tet_curr.v4.x << " " << setprecision(32) << tet_curr.v4.y << " " << setprecision(32) << tet_curr.v4.z << "\n";

						//	cout << setprecision(32) << tet_next.v1.x << " " << setprecision(32) << tet_next.v1.y << " " << setprecision(32) << tet_next.v1.z << "\n";
						//	cout << setprecision(32) << tet_next.v2.x << " " << setprecision(32) << tet_next.v2.y << " " << setprecision(32) << tet_next.v2.z << "\n";
						//	cout << setprecision(32) << tet_next.v3.x << " " << setprecision(32) << tet_next.v3.y << " " << setprecision(32) << tet_next.v3.z << "\n";
						//	cout << setprecision(32) << tet_next.v4.x << " " << setprecision(32) << tet_next.v4.y << " " << setprecision(32) << tet_next.v4.z << "\n";
						//}
						////##MK::end debug
						continue;
					}
					else { //not disjoint, i.e. overlapping
						////##MK::begin debug
						//if ( tet_next_idx == tet_curr_idx ) {
						//	cout << "Testing " << tet_next_idx << " vs " << tet_curr_idx << " not disjoint" << "\n";
						//}
						////##MK::end debug

						//we can add a link between two nodes object to the next and vice versa
						apt_uint curr_obj_id = objs_curr[curidx].msh->roiID;
						apt_uint next_obj_id = objs_next[*candidxt].msh->roiID;

						//we also need to document which next objects the curr object overlaps with, for subsequent proximity analysis
						//objs_curr[curidx].obj_ovrlp_idx_vol[*candidxt] = MYZERO;
						//objs_next[*candidxt].obj_ovrlp_idx_vol[curidx] = MYZERO;

						//debugging verbose information

						//if ( curr_obj_id == 90 ) {
						//	cout << "SAT test voted that " << curr_obj_id << " overlaps with " << next_obj_id << " because at least the following tetrahedra intersect" << "\n";
						//	cout << "tet_curr" << "\n";
						//	cout << "tet_next" << "\n";
						//	cout << setprecision(32) << tet_curr.v1.x << " " << setprecision(32) << tet_curr.v1.y << " " << setprecision(32) << tet_curr.v1.z << "\n";
						//	cout << setprecision(32) << tet_curr.v2.x << " " << setprecision(32) << tet_curr.v2.y << " " << setprecision(32) << tet_curr.v2.z << "\n";
						//	cout << setprecision(32) << tet_curr.v3.x << " " << setprecision(32) << tet_curr.v3.y << " " << setprecision(32) << tet_curr.v3.z << "\n";
						//	cout << setprecision(32) << tet_curr.v4.x << " " << setprecision(32) << tet_curr.v4.y << " " << setprecision(32) << tet_curr.v4.z << "\n";

						//	cout << setprecision(32) << tet_next.v1.x << " " << setprecision(32) << tet_next.v1.y << " " << setprecision(32) << tet_next.v1.z << "\n";
						//	cout << setprecision(32) << tet_next.v2.x << " " << setprecision(32) << tet_next.v2.y << " " << setprecision(32) << tet_next.v2.z << "\n";
						//	cout << setprecision(32) << tet_next.v3.x << " " << setprecision(32) << tet_next.v3.y << " " << setprecision(32) << tet_next.v3.z << "\n";
						//	cout << setprecision(32) << tet_next.v4.x << " " << setprecision(32) << tet_next.v4.y << " " << setprecision(32) << tet_next.v4.z << "\n";
						//}

						//curr2next_links.push_back( forward_link(curr_obj_id, next_obj_id) );
						//next2curr_links.push_back( forward_link(next_obj_id, curr_obj_id) );

						//c2n_links_i[tskid]->push_back( forward_link(curr_obj_id, next_obj_id) );
						//n2c_links_i[tskid]->push_back( forward_link(next_obj_id, curr_obj_id) );
						my_curr2next_links_ovrlp.push_back( forward_link(curr_obj_id, next_obj_id) );
						my_next2curr_links_ovrlp.push_back( forward_link(next_obj_id, curr_obj_id) );

						//mark that there is an intersection for the next candidate and break candidate tests
						//##MK::objs_next_cand_status[*cand] = ANALYZE_YES;
						//where to jump out

						//for pure overlap computation it suffices to know that one tetrahedron of cand next intersects one tet of curr
						curr_intersects_with_cand = true;
						break;
					}
				} //test next tetrahedron of an obj_next candidate
			}
			else { //in the seldom case if it intersects were done, we know cand next intersects curr
				break;
			}
		} //test next obj_next candidate

		//define which candidates we have already tested, ##MK::for now we only test if there is at all one overlap
		//##MK::but not how large is the overlap volume between the objects !!!
		//because the latter requires an accumulation of computed intersection volume between pairs of arbitrary tetrahedra
		//these intersection computations can be computed very precisely using nef polyhedra
		//however this is very costly (in the order of 25ms per pair)

	} //test next tetrahedron of curr object
}
if ( ConfigIntersector::OverlapMethod == EVAPID_COMPARISON ) {
*/


/*
bool tetrahedralizer::tessellate( vector<p3d> const & vrts_in, vector<tri3u> const & fcts_in, vector<p3d> & vrts_out, vector<quad3u> & cells_out )
{
	double ctic = omp_get_wtime();

	is_tessellated = false;
	vrts_out = vector<p3d>();
	cells_out = vector<quad3u>();

	if ( vrts_in.size() < 4 || vrts_in.size() >= (size_t) I32MX ) {
		cerr << "Number of vertices should be at least four but not more than " << I32MX << " !" << "\n"; return false;
	}
	if ( fcts_in.size() < 4 || fcts_in.size() >= (size_t) I32MX ) {
		cerr << "Number of facets should be at least four but not more than " << I32MX << " !" << "\n"; return false;
	}

	//wias-berlin.de/software/tetgen/1.5/doc/manual/manual.pdf
	tetgenio in, out;
	tetgenio::facet* f;
	tetgenio::polygon* p;
	//initialize();

	tetgenbehavior* calls = NULL;
	try {
		calls = new tetgenbehavior;
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of tetgenbehavior call object failed !" << "\n"; return false;
	}

	string what = ConfigIntersector::TetGenFlags;
	char* cstr = NULL;
	try {
		cstr = new char[what.length() + 1];
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation of tetrahedralization parameter failed !" << "\n";
		delete calls; calls = NULL; delete [] cstr; cstr = NULL; return false;
	}
	strcpy(cstr, what.c_str());
	bool tetstatus = calls->parse_commandline( cstr ); //cstr //"pYv", "pYv" "pq2.0a10.0v"
	if ( tetstatus == false ) {
		cerr << "Parsing commandline arguments failed !" << "\n";
		delete calls; calls = NULL; delete [] cstr; cstr = NULL; return false;
	}
	delete [] cstr; cstr = NULL;

	in.firstnumber = 0;	 //facet indices start at 0
	int nvertices_in = (int) vrts_in.size(); //with above sanity check narrowing cast is now safe
	int nfaces_in = (int) fcts_in.size();

	in.numberofpoints = nvertices_in;
	in.pointlist = NULL;
	try {
		in.pointlist = new double[nvertices_in*3];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of in.pointlist failed !" << "\n";
		delete calls; calls = NULL; return false;
	}
	//cout << "Allocated pointlist to tetgen" << "\n";

	//pass vertices into tetgen data structure
	for ( int v = 0; v < nvertices_in; v++ ) {
		in.pointlist[v*3+0] = (double) vrts_in[v].x;
		in.pointlist[v*3+1] = (double) vrts_in[v].y;
		in.pointlist[v*3+2] = (double) vrts_in[v].z;
	}
	//cout << "Passed trimesh_v to tetgen" << "\n";

	in.numberoffacets = nfaces_in;
	in.facetlist = NULL;
	try {
		in.facetlist = new tetgenio::facet[in.numberoffacets];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of in.facetlist failed !" << "\n";
		delete calls; calls = NULL; return false;
	}
	//cout << "Allocated facet in tetgen" << "\n";

	in.facetmarkerlist = NULL; //##MK::we have no marks for this PLC

	for ( int ft = 0; ft < nfaces_in; ft++ ) { //add facets
		f = &in.facetlist[ft];
		f->numberofpolygons = 1;
		f->polygonlist = NULL;
		try {
			f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation of f.polygonlist failed!" << "\n";
			delete calls; calls = NULL; return false;
		}
		f->numberofholes = 0;
		f->holelist = NULL;

		p = &f->polygonlist[0];
		int nverts_polygon = 3;
		p->numberofvertices = nverts_polygon;
		p->vertexlist = NULL;
		try {
			p->vertexlist = new int[p->numberofvertices];
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation for p.vertexlist failed!" << "\n";
			delete calls; calls = NULL; return false;
		}
		//for( int vv = 0; vv < nverts_polygon; vv++ ) {
			p->vertexlist[0] = (int) fcts_in[ft].v1;
			p->vertexlist[1] = (int) fcts_in[ft].v2;
			p->vertexlist[2] = (int) fcts_in[ft].v3;
		//}
	}
	//cout << "Allocated facet vertices in tetgen" << "\n";

	// Set 'in.facetmarkerlist'
	//in.facetmarkerlist[0] = -1; in.facetmarkerlist[1] = -2; in.facetmarkerlist[2] = 0; in.facetmarkerlist[3] = 0;
	//in.facetmarkerlist[4] = 0; in.facetmarkerlist[5] = 0;

	// Output the PLC to files 'barin.node' and 'barin.poly'.
	//in.save_nodes("barin"); in.save_poly("barin");

	//use tetgen library to perform actual meshing
	try {
		tetrahedralize( calls, &in, &out);
	}
	catch (int tetgencroak) {
		cerr << "TetGen internal error " << tetgencroak << "\n";
		delete calls; calls = NULL; return false;
	}
	//cout << "Tetrahedralized complex" << "\n";

	//transfer results of the tetgen intermediate data structure
	int nvertices_out = out.numberofpoints;
	int ntetrahedra_out = out.numberoftetrahedra;

	if ( nvertices_out < 1 || ntetrahedra_out < 1 ) {
		cerr << "NumberOfPoints or number of tetraeder is <1 !" << "\n";
		delete calls; calls = NULL; return false;
	}
	try {
		vrts_out.reserve( nvertices_out );
		//tet_id.reserve( ntetrahedra_out );
		cells_out.reserve( ntetrahedra_out );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error for tet_v or tet_o !" << "\n";
		delete calls; calls = NULL; return false;
	}
	////cout << "Number of vertices out " << nvertices_out << "\n";
	////cout << "Number of tetraedra out " << ntetrahedra_out << "\n";
	for ( int v = 0; v < nvertices_out; v++ ) {
		vrts_out.push_back( p3d(	(apt_real) out.pointlist[v*3+0],
									(apt_real) out.pointlist[v*3+1],
									(apt_real) out.pointlist[v*3+2] ) );
		//cout << "xyz\t\t" << tet_v.back().x << "\t\t" << tet_v.back().y << "\t\t" << tet_v.back().z << "\n";
	}
	//cout << "Transferred " << nvertices_out << "/" << tet_v.size() << " tetrahedron vertices" << endl;

	for ( int cc = 0; cc < ntetrahedra_out; cc++ ) {
		////quad3u qd = quad3u(	out.tetrahedronlist[cc*4+0],
		////				out.tetrahedronlist[cc*4+1],
		////				out.tetrahedronlist[cc*4+2],
		////				out.tetrahedronlist[cc*4+3] );
		////tet_id.push_back( qd );
		//int q1 = out.tetrahedronlist[cc*4+0];
		//int q2 = out.tetrahedronlist[cc*4+1];
		//int q3 = out.tetrahedronlist[cc*4+2];
		//int q4 = out.tetrahedronlist[cc*4+3];
		////tet_id.push_back( quad3u( 	static_cast<unsigned int>(q1),
		////							static_cast<unsigned int>(q2),
		////							static_cast<unsigned int>(q3),
		////							static_cast<unsigned int>(q4)    ) );
		////tet_xyz.push_back( tet3d64( tet_v.at(q1), tet_v.at(q2), tet_v.at(q3), tet_v.at(q4) ) );
		////tet_v3d.push_back( tet3d( tet_v.at(qd.v1), tet_v.at(qd.v2), tet_v.at(qd.v3), tet_v.at(qd.v4) ) );
		////cout << "v1234\t\t" << tet_o.back().v1 << "\t\t" << tet_o.back().v2 << "\t\t" << tet_o.back().v3 << "\t\t" << tet_o.back().v4 << "\n";
		//ply_mesh_tets_quads.push_back( roi_tetrahedron() );
		//ply_mesh_tets_quads.back().v1 = p3d( out.pointlist[q1*3+0], out.pointlist[q1*3+1], out.pointlist[q1*3+2] );
		//ply_mesh_tets_quads.back().v2 = p3d( out.pointlist[q2*3+0], out.pointlist[q2*3+1], out.pointlist[q2*3+2] );
		//ply_mesh_tets_quads.back().v3 = p3d( out.pointlist[q3*3+0], out.pointlist[q3*3+1], out.pointlist[q3*3+2] );
		//ply_mesh_tets_quads.back().v4 = p3d( out.pointlist[q4*3+0], out.pointlist[q4*3+1], out.pointlist[q4*3+2] );
		//cells_out.push_back( quad3u((apt_uint) out.tetrahedronlist[cc*4+0],
		//							(apt_uint) out.tetrahedronlist[cc*4+1],
		//							(apt_uint) out.tetrahedronlist[cc*4+2],
		//							(apt_uint) out.tetrahedronlist[cc*4+3]) );
	}
	//cout << "Transferred " << ntetrahedra_out << "/" << tet_o.size() << " tetraeder" << "\n";

	//cout << "Tetraeder in polyhedron has ply_mesh_tets_quad.size() " << ply_mesh_tets_quads.size() << "\n";
	//Output mesh to files 'barout.node', 'barout.ele' and 'barout.face'.
	//out.save_nodes("barout");	out.save_elements("barout"); out.save_faces("barout");
	//deinitialize();
	delete calls;
	//cout << "TetGen completed" << "\n";

	is_tessellated = true;

	double ctoc = omp_get_wtime();
	dt_tessellate = (ctoc-ctic);
	return true;
}
*/
