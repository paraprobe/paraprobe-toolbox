#!/bin/bash

# execute within a local python virtual environment or within a conda environment

# run in paraprobe-toolbox/code
python -m pycodestyle --ignore=E501,E712 --exclude=thirdparty *.py
python -m pylint --rcfile=pylintrc.rc --ignore=thirdparty utils/src/python
python -m pylint --rcfile=pylintrc.rc --ignore=thirdparty parmsetup/src/python
python -m pylint --rcfile=pylintrc.rc --ignore=thirdparty transcoder/src/python
python -m pylint --rcfile=pylintrc.rc --ignore=thirdparty clusterer/src/python
python -m pylint --rcfile=pylintrc.rc --ignore=thirdparty reporter/src/python


# python -m mypy --ignore-missing-imports code
