cmake_minimum_required(VERSION 3.18.4)

set(MYTOOLNAME "nanochem")
set(MYPROJECTNAME "paraprobe_${MYTOOLNAME}")
message([STATUS] "Projectname is ${MYPROJECTNAME}")
project(${MYPROJECTNAME} LANGUAGES C CXX)
set(CMAKE_BUILD_DIR "build")
set(CMAKE_POLICY_DEFAULT_CMP0144 NEW)

if(LOCAL_INSTALL)
	include("../PARAPROBE.Dependencies.cmake")
else()
	message([STATUS] "CONDA_PATH: ${CONDA_PREFIX}")
endif()

set(MYTOOLPATH "${MYPROJECTPATH}/code/${MYTOOLNAME}/")

if(LOCAL_INSTALL)
	include_directories("${MYHDFPATH}/include")
	link_directories("${MYHDFPATH}/lib")

	set(BOOST_ROOT "${MYBOOSTPATH}")
	set(Boost_NO_SYSTEM_PATHS ON)
	link_directories(${Boost_LIBRARY_DIR})

	set(CGAL_DIR "${MYCGALPATH}")
	set(EIGEN3_INCLUDE_DIR "${MYEIGENPATH}")
	include_directories("${MYEIGENPATH}")
	find_package(CGAL QUIET)
	if(CGAL_FOUND)
		include( ${CGAL_USE_FILE} )
		find_package(Eigen3 3.3 REQUIRED)
		include(CGAL_Eigen3_support)
		if(NOT TARGET CGAL::Eigen3_support)
			message(STATUS "This project requires the Eigen library, and will not be compiled.")
		endif()
	else()
		message([FATAL_ERROR] "CGAL is required, employ it!")
	endif()

	include_directories("${MYVOROXXPATH}/src")

	find_package(MPI REQUIRED)
	include_directories(${MPI_INCLUDE_PATH})
else()
	include_directories("${CONDA_PREFIX}/include/voro++")

	find_package(HDF5 REQUIRED)
	include_directories(${HDF5_INCLUDE_DIRS})
	set(MYHDFLINKFLAGS "-L${CONDA_PREFIX}/lib/ ${CONDA_PREFIX}/lib/libhdf5_hl.so ${CONDA_PREFIX}/lib/libhdf5.so ${CONDA_PREFIX}/lib/libz.so.1 ${CONDA_PREFIX}/lib/libsz.so ${CONDA_PREFIX}/lib/libaec.so -ldl")

	find_package(Boost REQUIRED)
	include_directories(${Boost_INCLUDE_DIRS})

	find_package(CGAL REQUIRED)
	include(${CGAL_USE_FILE})
	include_directories(${CGAL_INCLUDE_DIRS})

	find_package(Eigen3 3.3 REQUIRED)
	include_directories("${CONDA_PREFIX}/include/eigen3")

	find_package(MPI REQUIRED)
	include_directories(${MPI_INCLUDE_PATH})
endif()

find_package(OpenSSL REQUIRED)
set(MYSSLLINKFLAGS "-lssl -lcrypto")

set(MYOPTLEVEL "-O3")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MYOPTLEVEL}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MYOPTLEVEL}")
add_definitions("-std=c++17")

# alternatively, assume Intel oneAPI C/C++ compiler icx and icpx respectively
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
	set(MYVERBOSE "-fopt-info-all=opt.all")
	set(MYOMP "-fopenmp")
else()
	set(MYVERBOSE "-Wall")
	set(MYOMP "-qopenmp")
endif()
add_definitions("${MYOPTLEVEL} ${MYVERBOSE} ${MYOMP}")

if(LOCAL_INSTALL)
	set(MYTOOLSRCPATH "${MYTOOLPATH}/src/cxx")
	set(COMPILEPATH "${MYUTILSPATH}")
else()
	set(MYTOOLSRCPATH "src/cxx")
	set(MYVOROSRCPATH "../../../voro++/src")
	set(COMPILEPATH "../compiled_code")
endif()

add_executable(${MYPROJECTNAME}
	${COMPILEPATH}/PARAPROBE_ArgsAndGitSha.cpp.o
	${COMPILEPATH}/PARAPROBE_OpenSSLInterface.cpp.o
	${COMPILEPATH}/PARAPROBE_TimeAndDateHandling.cpp.o
	${COMPILEPATH}/PARAPROBE_CiteMe.cpp.o
	${COMPILEPATH}/PARAPROBE_ConfigShared.cpp.o
	${COMPILEPATH}/PARAPROBE_FilePathHandling.cpp.o
	${COMPILEPATH}/PARAPROBE_BitMangling.cpp.o
	${COMPILEPATH}/PARAPROBE_HDF5Core.cpp.o
	${COMPILEPATH}/PARAPROBE_Profiling.cpp.o
	${COMPILEPATH}/PARAPROBE_Math.cpp.o
	${COMPILEPATH}/PARAPROBE_PrimsContinuum.cpp.o
	${COMPILEPATH}/PARAPROBE_PrimsDiscrete.cpp.o
	${COMPILEPATH}/PARAPROBE_Geometry.cpp.o
	${COMPILEPATH}/PARAPROBE_UserStructs.cpp.o
	${COMPILEPATH}/PARAPROBE_MPIStructs.cpp.o
	${COMPILEPATH}/PARAPROBE_PeriodicTable.cpp.o
	${COMPILEPATH}/PARAPROBE_Crystallography.cpp.o
	${COMPILEPATH}/PARAPROBE_OriMath.cpp.o
	${COMPILEPATH}/PARAPROBE_CGALInterface.cpp.o
	${COMPILEPATH}/PARAPROBE_Solutes.cpp.o
	${COMPILEPATH}/PARAPROBE_ThermodynamicPhase.cpp.o
	${COMPILEPATH}/PARAPROBE_Grains.cpp.o
	${COMPILEPATH}/PARAPROBE_Precipitates.cpp.o
	${COMPILEPATH}/PARAPROBE_Histogram2D.cpp.o
	${COMPILEPATH}/PARAPROBE_TwoPointStats3D.cpp.o
	${COMPILEPATH}/PARAPROBE_ROIs.cpp.o
	${COMPILEPATH}/PARAPROBE_VoxelTree.cpp.o
	${COMPILEPATH}/PARAPROBE_AABBTree.cpp.o
	${COMPILEPATH}/PARAPROBE_KDTree.cpp.o
	${COMPILEPATH}/PARAPROBE_IonCloudMemory.cpp.o
	${COMPILEPATH}/PARAPROBE_XDMFBaseHdl.cpp.o
	${COMPILEPATH}/PARAPROBE_TriangleSoupHdl.cpp.o
	${COMPILEPATH}/PARAPROBE_ToolsBaseHdl.cpp.o

	${MYTOOLSRCPATH}/PARAPROBE_ConfigNanochem.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemMCInterface.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemMCLewiner2002.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemPQPInterface.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemStructs.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemSpatialDecomposition.cpp
	# ${MYTOOLSRCPATH}/PARAPROBE_NanochemTetrahedralize.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemTriangleSoupClustering.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemManifoldHdl.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemIsosurfaceHdl.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemInterfaceHdl.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemCompositionHdl.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemHDF5.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemXDMF.cpp
	${MYTOOLSRCPATH}/PARAPROBE_NanochemHdl.cpp
	${MYTOOLSRCPATH}/PARAPROBE_Nanochem.cpp
)

message([STATUS] "MYTOOLNAME: ${MYTOOLNAME}")
message([STATUS] "MYPROJECT: ${MYPROJECTNAME}")
message([STATUS] "MYPROJECTPATH: ${MYPROJECTPATH}")
message([STATUS] "MYTOOLPATH: ${MYTOOLPATH}")
message([STATUS] "MYUTILSPATH: ${MYUTILSPATH}")
message([STATUS] "MYHDF5PATH: ${MYHDFPATH}")
message([STATUS] "MYBOOSTPATH: ${MYBOOSTPATH}")
message([STATUS] "MYCGALPATH: ${MYCGALPATH}")
message([STATUS] "MYEIGENPATH: ${MYEIGENPATH}")
message([STATUS] "MYVOROXXPATH: ${MYVOROXXPATH}")

message([STATUS] "MYCCC_COMPILER: __${CMAKE_C_COMPILER}__")
message([STATUS] "MYCXX_COMPILER: __${CMAKE_CXX_COMPILER}__")
message([STATUS] "We utilize optimization level ${MYOPTLEVEL}")
message([STATUS] "MPI_INCLUDE_PATH ${MPI_INCLUDE_PATH}")

target_link_libraries(${MYPROJECTNAME} ${MYOMP} ${MPI_LIBRARIES} ${MYHDFLINKFLAGS} ${MYSSLLINKFLAGS})

if(MPI_COMPILE_FLAGS)
	set_target_properties(${MYPROJECTNAME} PROPERTIES COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif()

if(MPI_LINK_FLAGS)
	set_target_properties(${MYPROJECTNAME} PROPERTIES LINK_FLAGS "${MPI_LINK_FLAGS}")
endif()
