/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigNanochem.h"


ostream& operator << (ostream& in, isosurf_tasklist const & val)
{
	in << "Iso-surfacing tasklist" << "\n";
	in << "tasklistid " << val.tasklistid << "\n";
	in << "deloc_method " << val.deloc_method << "\n";
	in << "nuclide_hash_whitelist" << "\n";
	//size_t nrows = val.nuclide_hash_whitelist.size() / MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION;
	size_t i = 1;
	for( auto it = val.nuclide_hash_whitelist.begin(); it != val.nuclide_hash_whitelist.end(); it++, i++ ) {
		in << *it << ", ";
		if ( i % MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION == 0 ) {
			in << "\n";
		}
	}
	in << "charge_state_whitelist" << "\n";
	for( auto it = val.charge_state_whitelist.begin(); it != val.charge_state_whitelist.end(); it++ ) {
		in << *it << ", ";
	}
	in << "\n";
	in << "Ions taken into account itypes" << "\n";
	for( auto it = val.itypes.begin(); it != val.itypes.end(); it++ ) {
		for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
			in << (int) it->ivec[i] << ", ";
		}
		in << "\n";
	}
	in << "Ions taken into account itypes_whitelist" << "\n";
	for( auto jt = val.itypes_whitelist.begin(); jt != val.itypes_whitelist.end(); jt++ ) {
		in << (int) *jt << "\n";
	}
	in << "Elements taken into account after molecular ion decomposition elemtypes_whitelist" << "\n";
	for( auto kt = val.elemtypes_whitelist.begin(); kt != val.elemtypes_whitelist.end(); kt++ ) {
		in << (int) *kt << "\n";
	}
	switch(val.delocalization)
	{
		case DELOCALIZATION_NONE:
		{
			in << "Delocalization " << "none" << "\n";
			break;
		}
		case DELOCALIZATION_GAUSSIAN_ION_DECOMPOSE:
		{
			in << "Delocalization " << "truncated anisotropic 3D Gaussian kernel, decomposing molecular ions" << "\n";
			in << "Kernel halfsize a " << val.deloc_halfsize_a << "\n";
			in << "Kernel sigma values" << "\n";
			for( auto it = val.deloc_sigma.begin(); it != val.deloc_sigma.end(); it++ ) {
				in << *it << "\n";
			}
			break;
		}
		case DELOCALIZATION_USE_A_SINGLE_PRECOMPUTED:
		{
			in << "Delocalization " << "using a precomputed scalar field" << "\n";
			in << "From filename " << val.deloc_precomputed_fnm << "\n";
			in << "From groupname " << val.deloc_precomputed_grpnm << "\n";
			break;
		}
		default:
		{
			break;
		}
	}
	switch(val.normalization)
	{
		case ISOSURF_NRMLZ_NONE:
		{
			in << "normalization " << "based on counts in voxel, unitless" << "\n"; break;
		}
		case ISOSURF_NRMLZ_COMPOSITION:
		{
			in << "normalization " << "based on composition ion/ion at.-% dimensionless" << "\n"; break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION:
		{
			in << "normalization " << "based on concentration ion/volume 1/nm^3" << "\n"; break;
		}
		default:
		{
			break;
		}
	}
	switch(val.edge_handling)
	{
		case ISOSURF_EDGE_TRIANGLES_CREATE:
		{
			in << "edge handling " << "creating triangles at the edge of the dataset" << "\n"; break;
		}
		case ISOSURF_EDGE_TRIANGLES_REMOVE:
		{
			in << "edge handling " << "instructing MC to not place triangles at the edge of the dataset" << "\n"; break;
		}
		default:
		{
			break;
		}
	}
	in << "edge threshold " << val.edge_threshold << " nm" << "\n";
	in << "gridresolutions" << "\n";
	for( auto it = val.gridresolutions.begin(); it != val.gridresolutions.end(); it++ ) {
		in << *it << " nm" << "\n";
	}
	in << "isovalues" << "\n";
	for( auto jt = val.isovalues.begin(); jt != val.isovalues.end(); jt++ ) {
		in << *jt << "\n";
	}
	if ( val.IOStoreIsoSrfScalarFields == true )
		in << "store isosurface scalar fields " << "yes" << "\n";
	else
		in << "store isosurface scalar fields " << "no" << "\n";
	if ( val.IOStoreIsoSrfTriSoup == true )
		in << "store isosurface triangle soup " << "yes" << "\n";
	else
		in << "store isosurface triangle soup " << "no" << "\n";
	if ( val.IOStoreIsoSrfObjects == true )
		in << "store isosurface objects " << "yes" << "\n";
	else
		in << "store isosurface objects " << "no" << "\n";

	if ( val.IOStoreIsoSrfClosedGeometry == true )
		in << "store closed objects geometry " << "yes" << "\n";
	else
		in << "store closed objects geometry " << "no" << "\n";
	if ( val.IOStoreIsoSrfClosedProps == true )
		in << "store closed objects props (volume, dimensions, orientation) " << "yes" << "\n";
	else
		in << "store closed objects props (volume, dimensions, orientation) " << "no" << "\n";
	if ( val.IOStoreIsoSrfClosedOBB == true )
		in << "store closed objects OBB " << "yes" << "\n";
	else
		in << "store closed objects OBB " << "no" << "\n";
	if ( val.IOStoreIsoSrfClosedIons == true )
		in << "store closed objects which ions included " << "yes" << "\n";
	else
		in << "store closed objects which ions included " << "no" << "\n";

	if ( val.IOStoreIsoSrfProxyGeometry == true )
		in << "store proxy objects geometry " << "yes" << "\n";
	else
		in << "store proxy objects geometry " << "no" << "\n";
	if ( val.IOStoreIsoSrfProxyProps == true )
		in << "store proxy objects props (volume, dimensions, orientation) " << "yes" << "\n";
	else
		in << "store proxy objects props (volume, dimensions, orientation) " << "no" << "\n";
	if ( val.IOStoreIsoSrfProxyOBB == true )
		in << "store proxy objects OBB " << "yes" << "\n";
	else
		in << "store proxy objects OBB " << "no" << "\n";
	if ( val.IOStoreIsoSrfProxyIons == true )
		in << "store proxy objects which ions included " << "yes" << "\n";
	else
		in << "store proxy objects which ions included " << "no" << "\n";

	if ( val.IOStoreIsoSrfOpenedAutoProxigram == true )
		in << "store free surface patches geometry auto-proxigram " << "yes" << "\n";
	else
		in << "store free surface patches geometry auto-proxigram " << "no" << "\n";
	if ( val.IOStoreIsoSrfClosedAutoProxigram == true )
		in << "store objects geometry auto-proxigrams " << "yes" << "\n";
	else
		in << "store objects geometry auto-proxigrams " << "no" << "\n";
	if ( val.DetectObjectEdgeTruncation == true )
		in << "detecting objects possibly truncated by the edge of the dataset " << "yes" << "\n";
	else
		in << "detecting objects possibly truncated by the edge of the dataset " << "no" << "\n";
	if ( val.DetectProxyEdgeTruncation == true )
		in << "detecting proxies possibly truncated by the edge of the dataset " << "yes" << "\n";
	else
		in << "detecting proxies possibly truncated by the edge of the dataset " << "no" << "\n";
	if ( val.DetectROIEdgeTruncation == true )
		in << "detecting auto ROIs possibly truncated by the edge of the dataset " << "yes" << "\n";
	else
		in << "detecting auto ROIs possibly truncated by the edge of the dataset " << "no" << "\n";
	return in;
}


void isosurf_task_info::set_itypes( vector<ion> const & ityps, vector<unsigned char> const & wlst, rangeTable const & ranging )
{
	//an iso-surface tasklist consists of multiple iso-surfacing tasks, for each task we so know only the molecular ion ivec that should be
	//considered as candidates, we need to map these to ityp unsigned char short keys though because ranging information for the ion point cloud
	//is only encoded in the ions.ionifo unsigned char array i_org and i_rnd
	itypes = vector<ion>();
	for( auto it = ityps.begin(); it != ityps.end(); it++ ) {
		itypes.push_back( ion() );
		itypes.back().id = it->id;
		itypes.back().charge_sgn = it->charge_sgn;
		itypes.back().charge_state = it->charge_state;
		for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
			itypes.back().ivec[i] = it->ivec[i];
		}
		itypes.back().ranges = vector<mqival>();
		for( auto jt = it->ranges.begin(); jt != it->ranges.end(); jt++ ) {
			itypes.back().ranges.push_back( *jt );
		}
	}
	//with the previous loop we made the specific iso-surfacing tasks familiar with the ivec of the molecular ion candidates to operate on
	//now it remains to build a whitelist with the associated unsigned char itypes that the iso-surfacing task handler can
	//interpret of which unsigned char iontype each ion is and whether this type matches any in the whitelist
	itypes_whitelist = vector<unsigned char>();
	for( auto jt = itypes.begin(); jt != itypes.end(); jt++ ) {
		//find respective unsigned char ityp key by querying the imported ranging
		for( auto kt = ranging.iontypes.begin(); kt != ranging.iontypes.end(); kt++ ) {
			if ( jt->is_the_same_ivec_as( kt->second ) == false ) {
				continue;
			}
			else {
				itypes_whitelist.push_back( kt->second.id );
				break;
			}
		}

	}
}


ostream& operator << (ostream& in, isosurf_task_info const & val)
{
	in << "Iso-surfacing tasklist" << "\n";
	in << "taskid " << val.taskid << "\n";
	in << "gridid " << val.gridid << "\n";
	in << "dlzid " << val.dlzid << "\n";
	in << "isrfid " << val.isrfid << "\n";
	in << "deloc_method " << val.deloc_method << "\n";
	in << "nuclide_hash_whitelist" << "\n";
	//size_t nrows = val.nuclide_hash_whitelist.size() / MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION;
	size_t i = 1;
	for( auto it = val.nuclide_hash_whitelist.begin(); it != val.nuclide_hash_whitelist.end(); it++, i++ ) {
		in << *it << ", ";
		if ( i % MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION == 0 ) {
			in << "\n";
		}
	}
	in << "charge_state_whitelist" << "\n";
	for( auto it = val.charge_state_whitelist.begin(); it != val.charge_state_whitelist.end(); it++ ) {
		in << *it << ", ";
	}
	in << "\n";
	in << "Ions taken into account" << "\n";
	for( auto it = val.itypes.begin(); it != val.itypes.end(); it++ ) {
		for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
			in << (int) it->ivec[i] << ", ";
		}
		in << "\n";
	}
	in << "itypes_whitelist" << "\n";
	for( auto jt = val.itypes_whitelist.begin(); jt != val.itypes_whitelist.end(); jt++ ) {
		in << (int) *jt << "\n";
	}
	in << "elemtypes_whitelist" << "\n";
	for( auto kt = val.elemtypes_whitelist.begin(); kt != val.elemtypes_whitelist.end(); kt++ ) {
		in << (int) *kt << "\n";
	}
	switch(val.delocalization)
	{
		case DELOCALIZATION_NONE:
		{
			in << "delocalization " << "none" << "\n"; break;
		}
		case DELOCALIZATION_GAUSSIAN_ION_DECOMPOSE:
		{
			in << "delocalization " << "truncated anisotropic 3D Gaussian kernel, decomposing molecular ions" << "\n";
			in << "kernel halfsize a " << val.deloc_halfsize_a << "\n";
			in << "kernel sigma " << val.deloc_sigma << "\n";
			break;
		}
		case DELOCALIZATION_USE_A_SINGLE_PRECOMPUTED:
		{
			in << "delocalization " << "using a precomputed scalar field" << "\n";
			in << "from filename " << val.deloc_precomputed_fnm << "\n";
			in << "from groupname " << val.deloc_precomputed_grpnm << "\n";
			break;
		}
		default:
		{
			break;
		}
	}
	switch(val.normalization)
	{
		case ISOSURF_NRMLZ_NONE:
		{
			in << "normalization " << "based on counts in voxel" << "\n"; break;
		}
		case ISOSURF_NRMLZ_COMPOSITION:
		{
			in << "normalization " << "based on composition" << "\n"; break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION:
		{
			in << "normalization " << "based on concentration" << "\n"; break;
		}
		default:
		{
			break;
		}
	}
	switch(val.edge_handling)
	{
		case ISOSURF_EDGE_TRIANGLES_CREATE:
		{
			in << "edge handling " << "letting MC place triangles also to the edge of the dataset" << "\n"; break;
		}
		case ISOSURF_EDGE_TRIANGLES_REMOVE:
		{
			in << "edge handling " << "letting MC not place triangles to the edge of the dataset" << "\n"; break;
		}
		default:
		{
			break;
		}
	}
	in << "edge threshold " << val.edge_threshold << " nm" << "\n";
	in << "resu " << val.resu << " nm" << "\n";
	in << "isoval " << val.isoval << "\n";
	if ( val.hasIsoSrfScalarField == true )
		in << "store isosurface scalar field " << "yes" << "\n";
	else
		in << "store isosurface scalar field " << "no" << "\n";
	if ( val.hasIsoSrfTriSoup == true )
		in << "store isosurface triangle soup " << "yes" << "\n";
	else
		in << "store isosurface triangle soup " << "no" << "\n";
	if ( val.hasObjects == true )
		in << "store isosurface objects " << "yes" << "\n";
	else
		in << "store isosurface objects " << "no" << "\n";

	if ( val.hasObjectsGeometry == true )
		in << "store objects geometry " << "yes" << "\n";
	else
		in << "store objects geometry " << "no" << "\n";
	if ( val.hasObjectsProps == true )
		in << "store objects properties " << "yes" << "\n";
	else
		in << "store objects properties " << "no" << "\n";
	if ( val.hasObjectsOBB == true )
		in << "store objects optimal bounding box " << "yes" << "\n";
	else
		in << "store objects optimal bounding box " << "no" << "\n";
	if ( val.hasObjectsIons == true )
		in << "store objects which ions included " << "yes" << "\n";
	else
		in << "store objects which ions included " << "no" << "\n";

	if ( val.hasProxiesGeometry == true )
		in << "store proxies geometry " << "yes" << "\n";
	else
		in << "store proxies geometry " << "no" << "\n";
	if ( val.hasProxiesProps == true )
		in << "store proxies properties " << "yes" << "\n";
	else
		in << "store proxies properties " << "no" << "\n";
	if ( val.hasProxiesOBB == true )
		in << "store proxies optimal bounding box " << "yes" << "\n";
	else
		in << "store proxies optimal bounding box " << "no" << "\n";
	if ( val.hasProxiesIons == true )
		in << "store proxies which ions included " << "yes" << "\n";
	else
		in << "store proxies which ions included " << "no" << "\n";
	return in;
}


ostream& operator << (ostream& in, interface_meshing_iter const & val)
{
	in << "target_edge_length " << val.target_edge_length << " nm" << "\n";
	in << "dcom_radius " << val.dcom_radius << " nm" << "\n";
	in << "smoothing_iter " << val.smoothing_iter << "\n";
	return in;
}


ostream& operator << (ostream& in, interface_task_info const & val)
{
	in << "tasklistid " << val.tasklistid << "\n";
	in << "isotope_matrix" << "\n";
	for( size_t row = 0; row < val.isotope_matrix.size()/MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; row++ ) {
		for( size_t col = 0; col < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; col++ ) {
			in << val.isotope_matrix[row*MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION+col] << " ";
		}
		in << "\n";
	}
	/*
	in << "iontypes_multiplicity" << "\n";
	for( auto it = val.itypes_multiplicity.begin(); it != val.itypes_multiplicity.end(); it++ ) {
		in << "ityp " << (int) it->first << "\t\t multiplicity " << (int) it->second << "\n";
	}
	*/
	/*
	in << "itypes_whitelist" << "\n";
	for( auto jt = val.itypes_whitelist.begin(); jt != val.itypes_whitelist.end(); jt++ ) {
		in << (int) *jt << "\n";
	}
	in << "elemtypes_whitelist" << "\n";
	for( auto kt = val.elemtypes_whitelist.begin(); kt != val.elemtypes_whitelist.end(); kt++ ) {
		in << (int) *kt << "\n";
	}
	*/
	in << "number of iterations " << val.number_of_iterations << "\n";
	in << "iteration parameter" << "\n";
	for( auto jt = val.iteration_parms.begin(); jt != val.iteration_parms.end(); jt++ ) {
		in << "Iteration " << jt - val.iteration_parms.begin() << "\n";
		in << *jt << "\n";
	}

	in << val.control_points.size() << " user-defined control points" << "\n";
	for ( auto kt = val.control_points.begin(); kt != val.control_points.end(); kt++ ) {
		in << *kt << "\n";
	}

	return in;
}


ostream& operator << (ostream& in, roi_info const & val)
{
	in << "height " << val.height << ", radius " << val.radius << "\n";
	if ( val.user_defined_rois == true ) {
		in << "user_defined_rois " << "yes" << "\n";
	}
	else {
		in << "user_defined_rois " << "no" << "\n";
	}
	in << "itypes" << "\n";
	for( auto it = val.itypes_whitelist.begin(); it != val.itypes_whitelist.end(); it++ ) {
		in << (int) *it << "\n";
	}
	in << "elemtypes" << "\n";
	for( auto jt = val.elemtypes_whitelist.begin(); jt != val.elemtypes_whitelist.end(); jt++ ) {
		in << (int) *jt << "\n";
	}
	return in;
}


ostream& operator << (ostream& in, composition_task_info const & val)
{
	in << "taskid " << val.taskid << "\n";
	in << "roi_info " << val.roiifo << "\n";
	if (val.cyl.size() > 0 ) {
		in << "cylinder" << "\n";
		for( auto it = val.cyl.begin(); it != val.cyl.end(); it++ ) {
			cout << "roi_id " << it->roiID << ", r " << it->r << "\n";
			cout << "p1 " << it->p1.x << ", " << it->p1.y << ", " << it->p1.z << "\n";
			cout << "p2 " << it->p2.x << ", " << it->p2.y << ", " << it->p2.z << "\n";
		}
	}
	return in;
}


ostream& operator << (ostream& in, input_triangle_soup const & val)
{
	in << "InputfilePrimitives " << val.InputfilePrimitives << "\n";
	in << "DatasetPrimitivesVertexPosition " << val.DatasetPrimitivesVertexPosition << "\n";
	in << "DatasetPrimitivesFacetIndices " << val.DatasetPrimitivesFacetIndices << "\n";
	in << "DatasetPrimitivesVertexNormal " << val.DatasetPrimitivesVertexNormal << "\n";
	in << "DatasetPrimitivesFacetNormal " << val.DatasetPrimitivesFacetNormal << "\n";
	in << "DatasetPrimitivesPatchIndices " << val.DatasetPrimitivesPatchIndices << "\n";
	in << "patch_identifier_filter " << val.patch_identifier_filter << "\n";
	in << val.patch_identifier_filter << "\n";
	return in;
}


string ConfigNanochem::InputfileDataset = "";
string ConfigNanochem::ReconstructionDatasetName = "";
string ConfigNanochem::MassToChargeDatasetName = "";
string ConfigNanochem::InputfileIonTypes = "";
string ConfigNanochem::IonTypesGroupName = "";
string ConfigNanochem::InputfileIonToEdgeDistances = "";
string ConfigNanochem::IonToEdgeDistancesDatasetName = "";
input_triangle_soup ConfigNanochem::InputEdgeMesh = input_triangle_soup();

ANALYSIS_METHOD ConfigNanochem::AnalysisMethod = NANOCHEM_NONE;
WINDOWING_METHOD ConfigNanochem::WindowingMethod = ENTIRE_DATASET;
vector<roi_sphere> ConfigNanochem::WindowingSpheres = vector<roi_sphere>();
vector<roi_rotated_cylinder> ConfigNanochem::WindowingCylinders = vector<roi_rotated_cylinder>();
vector<roi_rotated_cuboid> ConfigNanochem::WindowingCuboids = vector<roi_rotated_cuboid>();
vector<roi_polyhedron> ConfigNanochem::WindowingPolyhedra = vector<roi_polyhedron>();
lival<apt_uint> ConfigNanochem::LinearSubSamplingRange = lival<apt_uint>( 0, 1, UMX );
match_filter<unsigned char> ConfigNanochem::IontypeFilter = match_filter<unsigned char>();
match_filter<unsigned char> ConfigNanochem::HitMultiplicityFilter = match_filter<unsigned char>();

vector<isosurf_tasklist> ConfigNanochem::IsoSurfacingTasks = vector<isosurf_tasklist>();
vector<interface_task_info> ConfigNanochem::InterfaceModellingTasks = vector<interface_task_info>();

CHARACTERIZE_AUTOROIS ConfigNanochem::UserAutoRois = CHARACTERIZE_AUTOROIS_NONE;
string ConfigNanochem::IonToFeatureDistancesDatasetName = "";
string ConfigNanochem::InputfileIonToFeatureDistances = "";
input_triangle_soup ConfigNanochem::InputFeatureMesh = input_triangle_soup();
vector<composition_task_info> ConfigNanochem::CompositionAnalysisTasks = vector<composition_task_info>();


//sensible defaults for internal parameter controlling accuracy in computational geometry algorithms or other internals like tree queries
unsigned long ConfigNanochem::MaxMemoryPerNode = GIBIBYTE2BYTE(8);
size_t ConfigNanochem::TriSoupClusteringKDTreeMaxLeafSize = 256;
size_t ConfigNanochem::InterfaceModellingKDTreeMaxLeafSize = 32;
apt_real ConfigNanochem::LinearTimeVolVoxelEdgeLength = 1.; //nm
apt_real ConfigNanochem::MinScalarFieldGradientMagnitude = 0.01; //at.-%/nm
apt_real ConfigNanochem::TriSoupClusteringMaxDistance = 0.001; //nm
apt_real ConfigNanochem::OBBEpsilon = 0.001; //nm
apt_real ConfigNanochem::ClosedPolyhedraBVHQueryRadius = 0.1; //nm
apt_real ConfigNanochem::MinIonDistanceToEdge = 1.0; //nm
apt_real ConfigNanochem::VolFracQualifyingLargeObjects = 0.01;
apt_uint ConfigNanochem::Int2FloatOverflowThreshold = 100000;
string ConfigNanochem::TetGenFlags = "pqQ"; // "pY";
double ConfigNanochem::PLCtoTetsRelativeVolumeMismatch = 0.001; //one permille
size_t ConfigNanochem::MinIonSupportForClosedObjects = 1; //objects need to have at least one ion inside or on surface mesh
apt_uint ConfigNanochem::NGonalPrismNumberOfFacets = 360; //360; //360 facets to approximate a cylinder


bool ConfigNanochem::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigNanochem::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string sub_grpnm = "";
		string dsnm = "";

		//apt_uint number_of_processes = 1;
		apt_uint entry_id = 1;
		grpnm = "/entry" + to_string(entry_id);

		IsoSurfacingTasks = vector<isosurf_tasklist>();
		InterfaceModellingTasks = vector<interface_task_info>();
		CompositionAnalysisTasks = vector<composition_task_info>();

		apt_uint tskid = 0;
		cout << "Reading configuration for task " << tskid << "\n";
		//apt_uint proc_id = tskid + 1;

		bool some_analysis = false;
		vector<string> types = {"/delocalization", "/interface_meshing", "/oned_profile"};
		for ( auto it = types.begin(); it != types.end(); it++ ) {
			grpnm = "/entry" + to_string(entry_id) + *it;
			if ( h5r.nexus_path_exists( grpnm ) == true) {
				some_analysis = true; break;
			}
		}
		if ( some_analysis == false ) {
			cerr << "Unknown analysis method !" << "\n"; return false;
		}

		grpnm = "/entry" + to_string(entry_id) + "/delocalization";
		if ( h5r.nexus_path_exists( grpnm ) == true ) {
			sub_grpnm = grpnm + "/reconstruction";
			string path = "";
			if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileDataset = path_handling( path );
			cout << "InputfileDataset " << InputfileDataset << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";

			sub_grpnm = grpnm + "/ranging";
			path = "";
			if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileIonTypes = path_handling( path );
			cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "IonTypesGroupName " << IonTypesGroupName << "\n";

			sub_grpnm = grpnm + "/surface";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				path = "";
				string file_name = "";
				if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				file_name = path_handling( path );
				cout << "file_name " << file_name << "\n";
				string dsnm_vrts = "";
				if ( h5r.nexus_read( sub_grpnm + "/vertices", dsnm_vrts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_vrts " << dsnm_vrts << "\n";
				string dsnm_fcts = "";
				if ( h5r.nexus_read( sub_grpnm + "/indices", dsnm_fcts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_fcts " << dsnm_fcts << "\n";
				InputEdgeMesh = input_triangle_soup( file_name, dsnm_vrts, dsnm_fcts );
				cout << "InputEdgeMesh " << InputEdgeMesh << "\n";
			}

			sub_grpnm = grpnm + "/surface_distance";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				path = "";
				if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonToEdgeDistances = path_handling( path );
				cout << "InputfileIonToEdgeDistances " << InputfileIonToEdgeDistances << "\n";
				if ( h5r.nexus_read( sub_grpnm + "/distance", IonToEdgeDistancesDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IonToEdgeDistancesDatasetName " << IonToEdgeDistancesDatasetName << "\n";
			}

			//optional spatial filtering
			WindowingMethod = ENTIRE_DATASET;
			sub_grpnm = grpnm + "/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( sub_grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				//currently only UNION_OF_PRIMITIVES is implemented as a windowing method
				WindowingMethod = UNION_OF_PRIMITIVES;
				string sub_sub_grpnm = "";

				sub_sub_grpnm = sub_grpnm + "/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( sub_sub_grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								WindowingCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				sub_sub_grpnm = sub_grpnm + "/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( sub_sub_grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( sub_sub_grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( sub_sub_grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								WindowingCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				sub_sub_grpnm = sub_grpnm + "/ellipsoid_set";
				if ( h5r.nexus_path_exists( sub_sub_grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( sub_sub_grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( sub_sub_grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								WindowingSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << WindowingSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << WindowingCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << WindowingCuboids.size() << "\n";
			}
			//##MK::bitmask currently not implemented

			//optional sub-sampling filter for ion/evaporation ID
			sub_grpnm = grpnm + "/evaporation_id_filter";
			if( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			sub_grpnm = grpnm + "/iontype_filter";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( sub_grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			sub_grpnm = grpnm + "/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( sub_grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << HitMultiplicityFilter << "\n";

			AnalysisMethod = NANOCHEM_MODEL_ISOSURFACES_AND_CHARACTERIZE;
			cout << "Perform delocalization and analyze iso-surfaces on it" << "\n";
			IsoSurfacingTasks.push_back( isosurf_tasklist() );
			IsoSurfacingTasks.back().tasklistid = tskid;
			IsoSurfacingTasks.back().itypes = vector<ion>();
			IsoSurfacingTasks.back().itypes_whitelist = vector<unsigned char>();
			IsoSurfacingTasks.back().elemtypes_whitelist = vector<unsigned char>();

			//currently method does not have to be read because it is anyway "compute" only
			IsoSurfacingTasks.back().delocalization = DELOCALIZATION_GAUSSIAN_ION_DECOMPOSE;
			cout << "IsoSurfacingTasks.back().delocalization " << IsoSurfacingTasks.back().delocalization << "\n";

			//delocalization scalar field normalization method
			string method = "";
			if ( h5r.nexus_read( grpnm + "/normalization", method ) != MYHDF5_SUCCESS ) { return false; }
			if ( method.compare("none") == 0 ) {
				IsoSurfacingTasks.back().normalization = ISOSURF_NRMLZ_NONE;
			}
			else if ( method.compare( "composition") == 0 ) {
				IsoSurfacingTasks.back().normalization = ISOSURF_NRMLZ_COMPOSITION;
			}
			else if ( method.compare( "concentration") == 0 ) {
				IsoSurfacingTasks.back().normalization = ISOSURF_NRMLZ_CONCENTRATION;
			}
			else {
				cerr << "Unknown delocalization normalization method !" << "\n"; return false;
			}
			cout << "IsoSurfacingTasks.back().normalization " << IsoSurfacingTasks.back().normalization << "\n";

			//delocalization scalar field composition method
			if ( IsoSurfacingTasks.back().normalization != ISOSURF_NRMLZ_NONE ) {
				sub_grpnm = grpnm + "/decomposition";
				if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
					method = "";
					if ( h5r.nexus_read( sub_grpnm + "/method", method ) != MYHDF5_SUCCESS ) { return false; }
					IsoSurfacingTasks.back().deloc_method = method;
					cout << "IsoSurfacingTasks.back().deloc_method " << IsoSurfacingTasks.back().deloc_method << "\n";

					vector<unsigned short> nuclide_hash_wlst;
					if ( h5r.nexus_read( sub_grpnm + "/nuclide_whitelist", nuclide_hash_wlst ) != MYHDF5_SUCCESS ) { return false; }
					cout << "nuclide_whitelist.size() " << nuclide_hash_wlst.size() << "\n";
					IsoSurfacingTasks.back().nuclide_hash_whitelist = nuclide_hash_wlst;

					vector<int> charge_state_wlst;
					if ( h5r.nexus_read( sub_grpnm + "/charge_state_whitelist", charge_state_wlst ) != MYHDF5_SUCCESS ) { return false; }
					cout << "charge_state_whitelist.size() " << charge_state_wlst.size() << "\n";
					IsoSurfacingTasks.back().charge_state_whitelist = charge_state_wlst;
				}
			}

			IsoSurfacingTasks.back().gridresolutions = vector<apt_real>();
			if ( h5r.nexus_read( grpnm + "/grid_resolution", IsoSurfacingTasks.back().gridresolutions ) != MYHDF5_SUCCESS ) { return false; }
			cout << "IsoSurfacingTasks.back().gridresolutions.size() " << IsoSurfacingTasks.back().gridresolutions.size() << "\n";
			if ( h5r.nexus_read( grpnm + "/kernel_size", IsoSurfacingTasks.back().deloc_halfsize_a ) != MYHDF5_SUCCESS ) { return false; }
			cout << "IsoSurfacingTasks.back().deloc_halfsize_a " <<  IsoSurfacingTasks.back().deloc_halfsize_a << "\n";
			IsoSurfacingTasks.back().deloc_sigma = vector<double>();
			if ( h5r.nexus_read( grpnm + "/kernel_variance", IsoSurfacingTasks.back().deloc_sigma ) != MYHDF5_SUCCESS ) { return false; }
			cout << "IsoSurfacingTasks.back().deloc_sigma.size() " << IsoSurfacingTasks.back().deloc_sigma.size() << "\n";

			unsigned char option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_scalar_fields", option ) != MYHDF5_SUCCESS ) { return false; }
			IsoSurfacingTasks.back().IOStoreIsoSrfScalarFields = ( option == 0x01 ) ? true : false;
			cout << "IOStoreIsoSrfScalarFields " << (int) option << "\n";

			sub_grpnm = grpnm + "/isosurfacing";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				string method = "";
				if ( h5r.nexus_read( sub_grpnm + "/edge_method", method ) != MYHDF5_SUCCESS ) { return false; }
				if ( method.compare("default") == 0 ) {
					IsoSurfacingTasks.back().edge_handling = ISOSURF_EDGE_TRIANGLES_REMOVE;
				}
				else if ( method.compare("keep_edge_triangles") == 0 ) {
					IsoSurfacingTasks.back().edge_handling = ISOSURF_EDGE_TRIANGLES_CREATE;
				}
				else {
					cerr << "Unknown edge handling method !" << "\n"; return false;
				}
				cout << "IsoSurfacingTasks.back().edge_handling " << IsoSurfacingTasks.back().edge_handling << "\n";

				IsoSurfacingTasks.back().edge_threshold = 1.;
				if ( h5r.nexus_read( sub_grpnm + "/edge_threshold", IsoSurfacingTasks.back().edge_threshold ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IsoSurfacingTasks.back().edge_threshold " << IsoSurfacingTasks.back().edge_threshold << "\n";

				IsoSurfacingTasks.back().isovalues = vector<apt_real>();
				if ( h5r.nexus_read( sub_grpnm + "/phi", IsoSurfacingTasks.back().isovalues ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IsoSurfacingTasks.back().isovalues.size() " << IsoSurfacingTasks.back().isovalues.size() << "\n";
				if ( IsoSurfacingTasks.back().normalization == ISOSURF_NRMLZ_COMPOSITION ) {
					for( size_t k = 0; k < IsoSurfacingTasks.back().isovalues.size(); k++ ) {
						IsoSurfacingTasks.back().isovalues[k] /= 100.;
					}
				}

				unsigned char option = 0x00;
				path = sub_grpnm + "/has_triangle_soup";
				//if ( h5r.nexus_path_exists( path ) == true ) {
				if ( h5r.nexus_read( path, option ) != MYHDF5_SUCCESS ) { return false; }
				//}
				IsoSurfacingTasks.back().IOStoreIsoSrfTriSoup = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfTriSoup " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_object", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfObjects = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfObjects " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_object_geometry", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfClosedGeometry = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfClosedGeometry " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_object_properties", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfClosedProps = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfClosedProps " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_object_obb", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfClosedOBB = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfClosedOBB " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_object_ions", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfClosedIons = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfClosedIons " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_object_edge_contact", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().DetectObjectEdgeTruncation = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().DetectObjectEdgeTruncation " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_proxy", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfProxy = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfProxy " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_proxy_geometry", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfProxyGeometry = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfProxyGeometry " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_proxy_properties", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfProxyProps = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfProxyProps " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_proxy_obb", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfProxyOBB = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfProxyOBB " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_proxy_ions", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfProxyIons = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfProxyIons " << (int) option << "\n";

				option = 0x00;
				if ( h5r.nexus_read( sub_grpnm + "/has_proxy_edge_contact", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().DetectProxyEdgeTruncation = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().DetectProxyEdgeTruncation " << (int) option << "\n";

				option = 0x00;
				//if ( h5r.nexus_read( sub_grpnm + "/has_object_proxigram", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().IOStoreIsoSrfClosedAutoProxigram = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().IOStoreIsoSrfClosedAutoProxigram " << (int) option << "\n";

				option = 0x00;
				//if ( h5r.nexus_read( sub_grpnm + "/has_object_proxigram_edge_contact", option ) != MYHDF5_SUCCESS ) { return false; }
				IsoSurfacingTasks.back().DetectROIEdgeTruncation = ( option == 0x01 ) ? true : false;
				cout << "IsoSurfacingTasks.back().DetectROIEdgeTruncation " << (int) option << "\n";

				cout << IsoSurfacingTasks.back() << "\n";
			}
		}

		grpnm = "/entry" + to_string(entry_id) + "/interface_meshing";
		if ( h5r.nexus_path_exists( grpnm ) == true ) {
			sub_grpnm = grpnm + "/reconstruction";
			string path = "";
			if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileDataset = path_handling( path );
			cout << "InputfileDataset " << InputfileDataset << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";

			sub_grpnm = grpnm + "/ranging";
			path = "";
			if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileIonTypes = path_handling( path );
			cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "IonTypesGroupName " << IonTypesGroupName << "\n";

			sub_grpnm = grpnm + "/surface";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				path = "";
				string file_name = "";
				if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				file_name = path_handling( path );
				cout << "file_name " << file_name << "\n";
				string dsnm_vrts = "";
				if ( h5r.nexus_read( sub_grpnm + "/vertices", dsnm_vrts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_vrts " << dsnm_vrts << "\n";
				string dsnm_fcts = "";
				if ( h5r.nexus_read( sub_grpnm + "/indices", dsnm_fcts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_fcts " << dsnm_fcts << "\n";
				InputEdgeMesh = input_triangle_soup( file_name, dsnm_vrts, dsnm_fcts );
				cout << "InputEdgeMesh " << InputEdgeMesh << "\n";
			}

			sub_grpnm = grpnm + "/surface_distance";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				path = "";
				if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonToEdgeDistances = path_handling( path );
				cout << "InputfileIonToEdgeDistances " << InputfileIonToEdgeDistances << "\n";
				if ( h5r.nexus_read( sub_grpnm + "/distance", IonToEdgeDistancesDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IonToEdgeDistancesDatasetName " << IonToEdgeDistancesDatasetName << "\n";
			}

			//optional spatial filtering
			WindowingMethod = ENTIRE_DATASET;
			sub_grpnm = grpnm + "/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( sub_grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				//currently only UNION_OF_PRIMITIVES is implemented as a windowing method
				WindowingMethod = UNION_OF_PRIMITIVES;
				string sub_sub_grpnm = "";

				sub_sub_grpnm = sub_grpnm + "/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( sub_sub_grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								WindowingCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				sub_sub_grpnm = sub_grpnm + "/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( sub_sub_grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( sub_sub_grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( sub_sub_grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								WindowingCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				sub_sub_grpnm = sub_grpnm + "/ellipsoid_set";
				if ( h5r.nexus_path_exists( sub_sub_grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( sub_sub_grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( sub_sub_grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								WindowingSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << WindowingSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << WindowingCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << WindowingCuboids.size() << "\n";
			}
			//##MK::bitmask currently not implemented

			//optional sub-sampling filter for ion/evaporation ID
			sub_grpnm = grpnm + "/evaporation_id_filter";
			if( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			sub_grpnm = grpnm + "/iontype_filter";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( sub_grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			sub_grpnm = grpnm + "/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( sub_grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << HitMultiplicityFilter << "\n";

			AnalysisMethod = NANOCHEM_MODEL_SINGLE_INTERFACE_PCA_AND_DCOM;
			cout << "Analyzing a single interface" << "\n";
			InterfaceModellingTasks.push_back( interface_task_info() );
			InterfaceModellingTasks.back().tasklistid = tskid;

			string method = "";
			if ( h5r.nexus_read( grpnm + "/initialization", method ) != MYHDF5_SUCCESS ) { return false; }
			if ( method.compare("default") == 0 ) {
				cout << "Computing an interface based on decoration_filter settings" << "\n";
			}
			else if ( method.compare("control_point_file") == 0 ) {
				cout << "Importing control points..." << "\n";
				sub_grpnm = grpnm + "/control_point";
				vector<apt_real> real;
				if ( h5r.nexus_read( sub_grpnm + "/control_points", real ) != MYHDF5_SUCCESS ) { return false; }
				if ( real.size() == 0 || real.size() % 3 != 0 ) {
					cerr << real.size() << " control points were successfully loaded but this array has not an integer multiple of three entries !" << "\n";
					return false;
				}
				InterfaceModellingTasks.back().control_points = vector<p3d>();
				for( size_t row = 0; row < real.size() / 3; row++ ) {
					InterfaceModellingTasks.back().control_points.push_back(
							p3d( real[3*row+0], real[3*row+1], real[3*row+2] ) );
					cout << InterfaceModellingTasks.back().control_points.back() << "\n";
				}

				//see commit a8b4fca0f8fc85bbf4b126ac17753ebeb5efa117 for some example manually-collected control points
				//using Leitner's dataset and A. Reichmann's APTyzer
			}
			else {
				cerr << "Detected unknown initialization!" << "\n"; return false;
			}

			InterfaceModellingTasks.back().isotope_matrix = vector<unsigned short>();
			sub_grpnm = grpnm + "/decoration_filter";
			// "/method" is whitelist
			if ( h5r.nexus_read( sub_grpnm + "/match", InterfaceModellingTasks.back().isotope_matrix ) != MYHDF5_SUCCESS ) { return false; }
			cout << "InterfaceModellingTasks.back().isotope_matrix " << InterfaceModellingTasks.back().isotope_matrix.size() << "\n";
			for ( auto kt = InterfaceModellingTasks.back().isotope_matrix.begin();
					kt != InterfaceModellingTasks.back().isotope_matrix.end(); kt++ ) {
				cout << "\t\t" << *kt << "\n";
			}

			InterfaceModellingTasks.back().number_of_iterations = 0;
			if ( h5r.nexus_read( grpnm + "/number_of_iterations", InterfaceModellingTasks.back().number_of_iterations ) != MYHDF5_SUCCESS ) { return false; }
			cout << "InterfaceModellingTasks.back().number_of_iterations " << InterfaceModellingTasks.back().number_of_iterations << "\n";

			if ( InterfaceModellingTasks.back().number_of_iterations > 0 ) {
				vector<apt_real> edge_length;
				if ( h5r.nexus_read( grpnm + "/target_edge_length", edge_length ) != MYHDF5_SUCCESS ) { return false; }
				cout << "edge_length.size() " << edge_length.size() << "\n";
				vector<apt_real> dcom_radii;
				if ( h5r.nexus_read( grpnm + "/target_dcom_radius", dcom_radii ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dcom_radii.size() " << dcom_radii.size() << "\n";
				vector<apt_uint> smoothing_steps;
				if ( h5r.nexus_read( grpnm + "/target_smoothing_step", smoothing_steps ) != MYHDF5_SUCCESS ) { return false; }
				cout << "smoothing_steps.size() " << smoothing_steps.size() << "\n";
				if ( InterfaceModellingTasks.back().number_of_iterations == edge_length.size()
						&& edge_length.size() == dcom_radii.size()
						&& edge_length.size() == smoothing_steps.size() ) {

					InterfaceModellingTasks.back().iteration_parms = vector<interface_meshing_iter>();
					for( apt_uint iter = 0; iter < InterfaceModellingTasks.back().number_of_iterations; iter++ ) {
						InterfaceModellingTasks.back().iteration_parms.push_back(
							interface_meshing_iter( edge_length[iter], dcom_radii[iter], smoothing_steps[iter]) );
					}
				}
				else {
					cerr << "There is a size mismatch between the target_edge_length, dcom_radius, and smoothing_steps arrays !" << "\n"; return false;
				}
			}
			else {
				cerr << "An interface modelling task without any mesh refinement iteration is nonstop nonsense !" << "\n"; return false;
			}
			cout << InterfaceModellingTasks.back() << "\n";
		}

		grpnm = "/entry" + to_string(entry_id) + "/oned_profile";
		if ( h5r.nexus_path_exists( grpnm ) == true ) {
			AnalysisMethod = NANOCHEM_CHARACTERIZE_USER_INTERFACE_MODEL;
			cout << "Analyzing automated composition profiles for user-defined meshes of features" << "\n";
			cout << "enabling 1D profiling for interfacial excess mapping" << "\n";

			CompositionAnalysisTasks.push_back( composition_task_info() );
			CompositionAnalysisTasks.back().taskid = tskid + 1;
			CompositionAnalysisTasks.back().roiifo = roi_info();
			CompositionAnalysisTasks.back().roiifo.roi_type = CG_CYLINDER;

			string user_defined_grpnm = grpnm + "/user_defined_roi";
			CompositionAnalysisTasks.back().roiifo.user_defined_rois = (
				h5r.nexus_path_exists( user_defined_grpnm ) == false ) ? false : true;
			cout << "CompositionAnalysisTasks.back().roiifo.user_defined_rois "
				<< (int) CompositionAnalysisTasks.back().roiifo.user_defined_rois << "\n";

			sub_grpnm = grpnm + "/reconstruction";
			string path = "";
			if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileDataset = path_handling( path );
			cout << "InputfileDataset " << InputfileDataset << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";

			sub_grpnm = grpnm + "/ranging";
			path = "";
			if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileIonTypes = path_handling( path );
			cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
			if ( h5r.nexus_read( sub_grpnm + "/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "IonTypesGroupName " << IonTypesGroupName << "\n";

			sub_grpnm = grpnm + "/surface";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				path = "";
				string file_name = "";
				if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				file_name = path_handling( path );
				cout << "file_name " << file_name << "\n";
				string dsnm_vrts = "";
				if ( h5r.nexus_read( sub_grpnm + "/vertices", dsnm_vrts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_vrts " << dsnm_vrts << "\n";
				string dsnm_fcts = "";
				if ( h5r.nexus_read( sub_grpnm + "/indices", dsnm_fcts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_fcts " << dsnm_fcts << "\n";
				InputEdgeMesh = input_triangle_soup( file_name, dsnm_vrts, dsnm_fcts );
				cout << "InputEdgeMesh " << InputEdgeMesh << "\n";
			}

			sub_grpnm = grpnm + "/surface_distance";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				path = "";
				if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonToEdgeDistances = path_handling( path );
				cout << "InputfileIonToEdgeDistances " << InputfileIonToEdgeDistances << "\n";
				if ( h5r.nexus_read( sub_grpnm + "/distance", IonToEdgeDistancesDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IonToEdgeDistancesDatasetName " << IonToEdgeDistancesDatasetName << "\n";
			}

			InputFeatureMesh = input_triangle_soup();
			if ( CompositionAnalysisTasks.back().roiifo.user_defined_rois == false ) {
				sub_grpnm = grpnm + "/feature";
				path = "";
				if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				string file_name = path_handling( path );
				cout << "file_name " << file_name << "\n";
				string dsnm_vrts = "";
				if ( h5r.nexus_read( sub_grpnm + "/vertices", dsnm_vrts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_vrts " << dsnm_vrts << "\n";
				string dsnm_fcts = "";
				if ( h5r.nexus_read( sub_grpnm + "/indices", dsnm_fcts ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_fcts " << dsnm_fcts << "\n";
				string dsnm_fcts_nrms = "";
				if ( h5r.nexus_read( sub_grpnm + "/facet_normals", dsnm_fcts_nrms ) != MYHDF5_SUCCESS ) { return false; }
				cout << "dsnm_fcts_nrms " << dsnm_fcts_nrms << "\n";
				string dsnm_vrts_nrms = "";
				cout << "dsnm_vrts_nrms " << dsnm_vrts_nrms << "\n";

				InputFeatureMesh = input_triangle_soup( file_name, dsnm_vrts, dsnm_fcts, dsnm_vrts_nrms, dsnm_fcts_nrms, "" );

				sub_grpnm = grpnm + "/feature_distance";
				if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
					path = "";
					if ( h5r.nexus_read( sub_grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
					InputfileIonToFeatureDistances = path_handling( path );
					cout << "InputfileIonToFeatureDistances " << InputfileIonToFeatureDistances << "\n";
					if ( h5r.nexus_read( sub_grpnm + "/distance", IonToFeatureDistancesDatasetName ) != MYHDF5_SUCCESS ) { return false; }
					cout << "IonToFeatureDistancesDatasetName " << IonToFeatureDistancesDatasetName << "\n";
				}

				if ( h5r.nexus_read( grpnm + "/roi_cylinder_height",
					CompositionAnalysisTasks.back().roiifo.height ) != MYHDF5_SUCCESS ) { return false; }
				cout << "CompositionAnalysisTasks.back().roiifo.height "
				<< CompositionAnalysisTasks.back().roiifo.height << "\n";
				if ( h5r.nexus_read( grpnm + "/roi_cylinder_radius",
					CompositionAnalysisTasks.back().roiifo.radius ) != MYHDF5_SUCCESS ) { return false; }
				cout << "CompositionAnalysisTasks.back().roiifo.radius "
				<< CompositionAnalysisTasks.back().roiifo.radius << "\n";
			}

			//optional spatial filtering
			WindowingMethod = ENTIRE_DATASET;
			sub_grpnm = grpnm + "/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( sub_grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				//currently only UNION_OF_PRIMITIVES is implemented as a windowing method
				WindowingMethod = UNION_OF_PRIMITIVES;
				string sub_sub_grpnm = "";

				sub_sub_grpnm = sub_grpnm + "/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( sub_sub_grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								WindowingCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				sub_sub_grpnm = sub_grpnm + "/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( sub_sub_grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( sub_sub_grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( sub_sub_grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								WindowingCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				sub_sub_grpnm = sub_grpnm + "/ellipsoid_set";
				if ( h5r.nexus_path_exists( sub_sub_grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( sub_sub_grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( sub_sub_grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								WindowingSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << WindowingSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << WindowingCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << WindowingCuboids.size() << "\n";
			}
			//##MK::bitmask currently not implemented

			//optional sub-sampling filter for ion/evaporation ID
			sub_grpnm = grpnm + "/evaporation_id_filter";
			if( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			sub_grpnm = grpnm + "/iontype_filter";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( sub_grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			sub_grpnm = grpnm + "/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( sub_grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( sub_grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( sub_grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << HitMultiplicityFilter << "\n";

			UserAutoRois = CHARACTERIZE_AUTOROIS_ONED_PROFILES;
			switch(UserAutoRois)
			{
				case CHARACTERIZE_AUTOROIS_ONED_PROFILES:
				{
					cout << "Characterizing one-dimensional ion projections in the direction of the positive facet normal" << "\n";
					if ( CompositionAnalysisTasks.back().roiifo.user_defined_rois == true ) {
						string sub_sub_grpnm = grpnm + "/user_defined_roi/cylinder_set";
						if ( h5r.nexus_path_exists( sub_sub_grpnm ) == true ) {
							apt_uint n_cylinders = 0;
							if ( h5r.nexus_read( sub_sub_grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
							if ( n_cylinders > 0 ) {
								vector<apt_real> center;
								if ( h5r.nexus_read( sub_sub_grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
								vector<apt_real> height;
								if ( h5r.nexus_read( sub_sub_grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
								vector<apt_real> radii;
								if ( h5r.nexus_read( sub_sub_grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
								if ( center.size() / 3 == n_cylinders &&
										height.size() / 3 == n_cylinders &&
											radii.size() == n_cylinders ) {
									for ( apt_uint i = 0; i < n_cylinders; i++ ) {
										CompositionAnalysisTasks.back().cyl.push_back(
											roi_rotated_cylinder(
												p3d(center[3*i+0] - 0.5*height[3*i+0],
													center[3*i+1] - 0.5*height[3*i+1],
													center[3*i+2] - 0.5*height[3*i+2]),
												p3d(center[3*i+0] + 0.5*height[3*i+0],
													center[3*i+1] + 0.5*height[3*i+1],
													center[3*i+2] + 0.5*height[3*i+2]),
												radii[i] ) );
									}
								}
								center = vector<apt_real>();
								height = vector<apt_real>();
								radii = vector<apt_real>();
							}
						}
					}
					break;
				}
				case CHARACTERIZE_AUTOROIS_THREED_PROFILES:
				{
					cerr << "Characterize three-dimensional ion projections along 40962 directions of a geodesic sphere currently not implemented" << "\n";
					return false;
				}
				default:
				{
					cout << "Unknown characterization" << "\n";
					return false;
				}
			}

			cout << CompositionAnalysisTasks.back() << "\n";
		}

		//sensible defaults
		cout << "MaxMemoryPerComputingNode " << MaxMemoryPerNode << "\n";
		cout << "MinScalarFieldGradientMagnitude " << MinScalarFieldGradientMagnitude << "\n";
		cout << "TriSoupClusteringKDTreeMaxLeafSize " << TriSoupClusteringKDTreeMaxLeafSize << "\n";
		cout << "InterfaceModellingKDTreeMaxLeafSize " << InterfaceModellingKDTreeMaxLeafSize << "\n";
		cout << "LinearTimeVolVoxelEdgeLength " << LinearTimeVolVoxelEdgeLength << "\n";
		cout << "TriSoupClusteringMaxDistance " << TriSoupClusteringMaxDistance << "\n";
		cout << "OBBEpsilon " << OBBEpsilon << "\n";
		cout << "ClosedPolyhedraBVHQueryRadius " << ClosedPolyhedraBVHQueryRadius << "\n";
		cout << "MinIonDistanceToEdge " << MinIonDistanceToEdge << "\n";
		cout << "VolFracQualifyingLargeObjects " << VolFracQualifyingLargeObjects << "\n";
		cout << "Int2FloatOverflowThreshold " << Int2FloatOverflowThreshold << "\n";
		cout << "TetGenFlags " << TetGenFlags << "\n";
		cout << "PLCtoTetsRelativeVolumeMismatch " << PLCtoTetsRelativeVolumeMismatch << "\n";
		cout << "MinIonSupportForClosedObjects " << MinIonSupportForClosedObjects << "\n";
		cout << "NGonalPrismNumberOfFacets " << NGonalPrismNumberOfFacets << "\n";
		//cout << "Kuehbach40962.size() " << GeodesicSpheres::Kuehbach40962.size() << "\n";

		return true;
	}

	//special developer case SimID == 0

	return false;
}
