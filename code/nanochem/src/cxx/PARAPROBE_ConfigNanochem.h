/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_CONFIG_NANOCHEM_H__
#define __PARAPROBE_CONFIG_NANOCHEM_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"


enum ANALYSIS_METHOD {
	NANOCHEM_NONE,
	NANOCHEM_MODEL_ISOSURFACES_AND_CHARACTERIZE,
	NANOCHEM_MODEL_SINGLE_INTERFACE_PCA_AND_DCOM,
	NANOCHEM_CHARACTERIZE_USER_INTERFACE_MODEL
};


enum DELOCALIZATION_METHOD {
	DELOCALIZATION_NONE,
	DELOCALIZATION_GAUSSIAN_ION_DECOMPOSE,
	DELOCALIZATION_USE_A_SINGLE_PRECOMPUTED
};


enum ISOSURFACING_EDGE_EFFECT_HANDLING {
	ISOSURF_EDGE_TRIANGLES_CREATE,
	ISOSURF_EDGE_TRIANGLES_REMOVE
};


#define VOXEL_EVALUATE_YES			0xFF
#define VOXEL_EVALUATE_NO			0x00


enum ISOSURFACING_NORMALIZATION {
	ISOSURF_NRMLZ_NONE,
	ISOSURF_NRMLZ_COMPOSITION,
	ISOSURF_NRMLZ_CONCENTRATION
};


enum ISOSURFACING_METHOD {
	ISOSURF_NONE,
	ISOSURF_COMPUTE
};


enum ISOSURFACE_AUTOPROFILING {
	ISOSURF_AUTOPROFILING_NONE,
	ISOSURF_AUTOPROFILING_ONED_PROXIGRAMS,
	ISOSURF_AUTOPROFILING_THREED_PROXIGRAM
};


enum CONCENTRATION_PROFILING {
	CONCENTRATION_PROFILING_NONE,
	CONCENTRATION_PROFILING_USER,
	CONCENTRATION_PROFILING_ISOSURF
};


enum CHARACTERIZE_AUTOROIS {
	CHARACTERIZE_AUTOROIS_NONE,
	CHARACTERIZE_AUTOROIS_ONED_PROFILES,
	CHARACTERIZE_AUTOROIS_THREED_PROFILES
};


struct isosurf_tasklist
{
	//an isosurface task list contains a scan across eventually multiple iso-surfaces for different delocalizations each
	//string targets;							//key codes which ions to build the iso-surface from
	apt_uint tasklistid;						//tasklistID
	apt_uint delocalization;					//key code which delocalization approach
	apt_int deloc_halfsize_a;					//half-size of the kernel
	vector<double> deloc_sigma;					//sigma_x = sigma_y = 2sigmaz
	string deloc_precomputed_fnm;				//filename from which to load a single precomputed delocalization
	string deloc_precomputed_grpnm;				//grpnm under which to find the pieces of information that specify which grid and scalarfield to load
	apt_uint normalization;						//key code which normalization approach, see ConfigNanochem::IsoSurfacingNormalization enum which approaches possible
	apt_uint edge_handling;						//key code which strategy to use to handle iso-surface object at the edge of the dataset
	apt_real edge_threshold;					//what is the distance below which each object with an ion which has this distance to the edge is consider an object close to the edge of the dataset
	string deloc_method;
	vector<unsigned short> nuclide_hash_whitelist;
	vector<int> charge_state_whitelist;
	vector<ion> itypes;							//user-input level representation which iontypes to use
	vector<unsigned char> itypes_whitelist;		//low-level representation which iontypes actually used to consider to build the iso-surface from
	vector<unsigned char> elemtypes_whitelist; 	//low-level which element types to consider when doing a ion decomposition
	vector<apt_real> gridresolutions;			//which grid resolutions to compute during high-throughput screening
	vector<apt_real> isovalues;					//which isovalues (different normalization models possible) to compute during high-throughput screening
	bool IOStoreIsoSrfScalarFields;
	bool IOStoreIsoSrfTriSoup;

	bool IOStoreIsoSrfObjects;
	bool IOStoreIsoSrfClosedGeometry;
	bool IOStoreIsoSrfClosedProps;
	bool IOStoreIsoSrfClosedOBB;
	bool IOStoreIsoSrfClosedIons;

	bool IOStoreIsoSrfProxy;
	bool IOStoreIsoSrfProxyGeometry;
	bool IOStoreIsoSrfProxyProps;
	bool IOStoreIsoSrfProxyOBB;
	bool IOStoreIsoSrfProxyIons;

	bool IOStoreIsoSrfOpenedAutoProxigram;
	bool IOStoreIsoSrfClosedAutoProxigram;

	bool DetectObjectEdgeTruncation;
	bool DetectProxyEdgeTruncation;
	bool DetectROIEdgeTruncation;
	isosurf_tasklist() : tasklistid(0), delocalization(DELOCALIZATION_NONE),
			deloc_halfsize_a(0), deloc_sigma(vector<double>()),
			deloc_precomputed_fnm(""), deloc_precomputed_grpnm(""),
			normalization(ISOSURF_NRMLZ_NONE), edge_handling(ISOSURF_EDGE_TRIANGLES_CREATE), edge_threshold(EPSILON),
			deloc_method("resolve_none"), nuclide_hash_whitelist(vector<unsigned short>()), charge_state_whitelist(vector<int>()),
			itypes(vector<ion>()), itypes_whitelist(vector<unsigned char>()), elemtypes_whitelist(vector<unsigned char>()),
				gridresolutions(vector<apt_real>()), isovalues(vector<apt_real>()),
				IOStoreIsoSrfScalarFields(false), IOStoreIsoSrfTriSoup(false),
				IOStoreIsoSrfObjects(false),
				IOStoreIsoSrfClosedGeometry(false), IOStoreIsoSrfClosedProps(false),
				IOStoreIsoSrfClosedOBB(false), IOStoreIsoSrfClosedIons(false),
				IOStoreIsoSrfProxy(false),
				IOStoreIsoSrfProxyGeometry(false), IOStoreIsoSrfProxyProps(false),
				IOStoreIsoSrfProxyOBB(false), IOStoreIsoSrfProxyIons(false),
				IOStoreIsoSrfOpenedAutoProxigram(false),
				IOStoreIsoSrfClosedAutoProxigram(false),
				DetectObjectEdgeTruncation(false),
				DetectProxyEdgeTruncation(false),
				DetectROIEdgeTruncation(false) {}
	isosurf_tasklist( const apt_uint _tsklstid, const apt_uint _deloc,
			const apt_int _deloc_a, vector<double> const & _deloc_sigma,
			const string _deloc_pre_fnm, const string _deloc_pre_dsnm,
			const apt_uint _nrmlz, const apt_uint _edgehdl, const apt_real _edgethrs,
			vector<apt_real> const & _gridresolutions,
			vector<apt_real> const & _isovalues,
				const bool _io_isrf_fields, const bool _io_isrf_trisoup,
				const bool _io_objs,
				const bool _io_objs_geom, const bool _io_objs_props, const bool _io_objs_obb, const bool _io_objs_ions,
				const bool _io_prxy,
				const bool _io_prxy_geom, const bool _io_prxy_props, const bool _io_prxy_obb, const bool _io_prxy_ions,
				const bool _io_open_autoproxi, const bool _io_objs_autoproxi,
				const bool _detect_objs_edge,
				const bool _detect_prxy_edge,
				const bool _detect_roi_edge ) :
					tasklistid(_tsklstid), delocalization(_deloc),
					deloc_halfsize_a(_deloc_a), deloc_sigma(_deloc_sigma),
					deloc_precomputed_fnm(_deloc_pre_fnm), deloc_precomputed_grpnm(_deloc_pre_dsnm),
							normalization(_nrmlz), edge_handling(_edgehdl), edge_threshold(_edgethrs),
							deloc_method("resolve_none"), nuclide_hash_whitelist(vector<unsigned short>()), charge_state_whitelist(vector<int>()),
							itypes(vector<ion>()), itypes_whitelist(vector<unsigned char>()),
							elemtypes_whitelist(vector<unsigned char>()),
						gridresolutions(_gridresolutions),
							isovalues(_isovalues),
							IOStoreIsoSrfScalarFields(_io_isrf_fields), IOStoreIsoSrfTriSoup(_io_isrf_trisoup),
							IOStoreIsoSrfObjects(_io_objs),
							IOStoreIsoSrfClosedGeometry(_io_objs_geom), IOStoreIsoSrfClosedProps(_io_objs_props),
							IOStoreIsoSrfClosedOBB(_io_objs_obb), IOStoreIsoSrfClosedIons(_io_objs_ions),
							IOStoreIsoSrfProxy(_io_prxy),
							IOStoreIsoSrfProxyGeometry(_io_prxy_geom), IOStoreIsoSrfProxyProps(_io_prxy_props),
							IOStoreIsoSrfProxyOBB(_io_prxy_obb), IOStoreIsoSrfProxyIons(_io_prxy_ions),
							IOStoreIsoSrfOpenedAutoProxigram(_io_open_autoproxi),
							IOStoreIsoSrfClosedAutoProxigram(_io_objs_autoproxi),
							DetectObjectEdgeTruncation(_detect_objs_edge),
							DetectProxyEdgeTruncation(_detect_prxy_edge),
							DetectROIEdgeTruncation(_detect_roi_edge) {}
};

ostream& operator << (ostream& in, isosurf_tasklist const & val);


struct isosurf_task_info
{
	//a specific isosurface task on a specific delocalized grid
	apt_uint taskid;						//coordinating task id
	apt_uint gridid;						//gridID
	apt_uint dlzid;							//delocalizationID
	apt_uint isrfid;						//isrfID, taskID
	apt_uint delocalization;				//which delocalization approach
	apt_int deloc_halfsize_a;
	double deloc_sigma;
	string deloc_precomputed_fnm;			//filename from which to load a single precomputed delocalization
	string deloc_precomputed_grpnm;			//grpnm under which to find the pieces of information that specify which grid and scalarfield to load
	apt_uint normalization;					//which normalization approach
	apt_uint edge_handling;					//which approach to treat iso-surface triangle at the edge of the dataset
	apt_real edge_threshold;				//what is the distance below which each object with an ion which has this distance to the edge is consider an object close to the edge of the dataset
	string deloc_method;
	vector<unsigned short> nuclide_hash_whitelist;
	vector<int> charge_state_whitelist;
	vector<ion> itypes;						//user-input level representation which iontypes
	vector<unsigned char> itypes_whitelist;	//low-level representation which iontypes (ion not decomposed into disjoint elements!)
	vector<unsigned char> elemtypes_whitelist; //low-level representation which disjoint elements to hunt after, in IVAS/AP Suite a atomic decomposition of a molecular ion maps on elements!
	//the unsigned char encoded the proton count because for element we do not need to encode isotope information
	apt_real resu;							//which grid resolution
	apt_real isoval;						//which isovalue
	bool hasIsoSrfScalarField;
	bool hasIsoSrfTriSoup;
	bool hasObjects;
	bool hasObjectsGeometry;
	bool hasObjectsProps;
	bool hasObjectsOBB;
	bool hasObjectsIons;
	bool hasProxies;
	bool hasProxiesGeometry;
	bool hasProxiesProps;
	bool hasProxiesOBB;
	bool hasProxiesIons;
	double dt_discretization;
	double dt_marching_cubes;
	double dt_triangle_cluster;
	double dt_objects_find;
	double dt_closed_and_proxies_find;
	double dt_closed_quantify;
	double dt_proxies_quantify;
	double dt_h5io;
	isosurf_task_info() : taskid(UMX), gridid(UMX), dlzid(UMX), isrfid(UMX), delocalization(DELOCALIZATION_NONE),
			deloc_halfsize_a(0), deloc_sigma(0.), deloc_precomputed_fnm(""), deloc_precomputed_grpnm(""),
			normalization(ISOSURF_NRMLZ_NONE), edge_handling(ISOSURF_EDGE_TRIANGLES_CREATE), edge_threshold(EPSILON),
			deloc_method("resolve_none"), nuclide_hash_whitelist(vector<unsigned short>()), charge_state_whitelist(vector<int>()),
			itypes(vector<ion>()), itypes_whitelist(vector<unsigned char>()), elemtypes_whitelist(vector<unsigned char>()),
				resu(0.), isoval(0.), hasIsoSrfScalarField(false), hasIsoSrfTriSoup(false),
				hasObjects(false),
				hasObjectsGeometry(false), hasObjectsProps(false), hasObjectsOBB(false), hasObjectsIons(false),
				hasProxies(false),
				hasProxiesGeometry(false), hasProxiesProps(false), hasProxiesOBB(false), hasProxiesIons(false),
				dt_discretization(0.), dt_marching_cubes(0.), dt_triangle_cluster(0.), dt_objects_find(0.),
				dt_closed_and_proxies_find(0.), dt_closed_quantify(0.), dt_proxies_quantify(0.), dt_h5io(0.) {}
	isosurf_task_info( const apt_uint _tskid, const apt_uint _grdid, const apt_uint _dlzid, const apt_uint _srfid,
			const apt_uint _deloc, const apt_int _deloc_a, const double _deloc_sigma,
			const string _deloc_pre_fnm, const string _deloc_pre_dsnm,
			const apt_uint _nrmlz, const apt_uint _edgehdl, const apt_real _edgethrs, const apt_real _resu,
			const apt_real _isoval, const bool _io_isrf_field, const bool _io_isrf_trisoup,
			const bool _io_isrf_objs,
			const bool _io_objs_geom, const bool _io_objs_props, const bool _io_objs_obb, const bool _io_objs_ions,
			const bool _io_prxy,
			const bool _io_prxy_geom, const bool _io_prxy_props, const bool _io_prxy_obb, const bool _io_prxy_ions ) :
				taskid(_tskid), gridid(_grdid), dlzid(_dlzid), isrfid(_srfid),
					delocalization(_deloc), deloc_halfsize_a(_deloc_a), deloc_sigma(_deloc_sigma),
					deloc_precomputed_fnm(_deloc_pre_fnm), deloc_precomputed_grpnm(_deloc_pre_dsnm),
					normalization(_nrmlz), edge_handling(_edgehdl), edge_threshold(_edgethrs),
					deloc_method("resolve_none"), nuclide_hash_whitelist(vector<unsigned short>()), charge_state_whitelist(vector<int>()),
					itypes(vector<ion>()), itypes_whitelist(vector<unsigned char>()), elemtypes_whitelist(vector<unsigned char>()),
						resu(_resu), isoval(_isoval), hasIsoSrfScalarField(_io_isrf_field), hasIsoSrfTriSoup(_io_isrf_trisoup),
							hasObjects(_io_isrf_objs),
							hasObjectsGeometry(_io_objs_geom), hasObjectsProps(_io_objs_props), hasObjectsOBB(_io_objs_obb), hasObjectsIons(_io_objs_ions),
							hasProxies(_io_prxy),
							hasProxiesGeometry(_io_prxy_geom), hasProxiesProps(_io_prxy_props), hasProxiesOBB(_io_prxy_obb), hasProxiesIons(_io_prxy_ions),
							dt_discretization(0.), dt_marching_cubes(0.), dt_triangle_cluster(0.), dt_objects_find(0.),
							dt_closed_and_proxies_find(0.), dt_closed_quantify(0.), dt_proxies_quantify(0.), dt_h5io(0.) {}
	void set_itypes( vector<ion> const & ityps, vector<unsigned char> const & wlst, rangeTable const & ranging );
};

ostream& operator << (ostream& in, isosurf_task_info const & val);


struct interface_meshing_iter
{
	apt_real target_edge_length;
	apt_real dcom_radius;
	apt_uint smoothing_iter;
	interface_meshing_iter() : target_edge_length(MYONE),
			dcom_radius(MYONE), smoothing_iter(1) {}
	interface_meshing_iter( const apt_real _edgelen, const apt_real _dcomr, const apt_uint _smooth) :
		target_edge_length(_edgelen), dcom_radius(_dcomr), smoothing_iter(_smooth) {}
};

ostream& operator << (ostream& in, interface_meshing_iter const & val);


struct interface_task_info
{
	apt_uint tasklistid;							//tasklistID
	vector<unsigned short> isotope_matrix;			//a matrix whose rows are isotope_vectors, matrix has MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION columns
	/*
	vector<pair<unsigned char, unsigned char>> itypes_multiplicity; //array of input iontype selections mapped to iontype (first) and multiplicity (second)
	vector<unsigned char> itypes_whitelist;			//low-level representation which iontypes (ion not decomposed into disjoint elements!)
	vector<unsigned char> elemtypes_whitelist; 		//low-level representation which disjoint elements to hunt after
	*/
	vector<interface_meshing_iter> iteration_parms;
	apt_uint number_of_iterations;
	vector<p3d> control_points;						//a list of 3D control points from the user to guide the initial plane approximation method

	interface_task_info() : tasklistid(0),
			isotope_matrix(vector<unsigned short>()),
			iteration_parms(vector<interface_meshing_iter>()),
			number_of_iterations(0),
			control_points(vector<p3d>()) {}
	//itypes_multiplicity(vector<pair<unsigned char, unsigned char>>()),
	//itypes_whitelist(vector<unsigned char>()), elemtypes_whitelist(vector<unsigned char>()),
};

ostream& operator << (ostream& in, interface_task_info const & val);


struct roi_info
{
	vector<unsigned char> itypes_whitelist;
	vector<unsigned char> elemtypes_whitelist;
	apt_uint roi_type;
	apt_real height;
	apt_real radius;
	bool user_defined_rois;
	bool pad1;
	bool pad2;
	bool pad3;
	roi_info() : itypes_whitelist(vector<unsigned char>()), elemtypes_whitelist(vector<unsigned char>()),
			roi_type(CG_CYLINDER), height(0.), radius(0.), user_defined_rois(false),
			pad1(false), pad2(false), pad3(false) {}
};

ostream& operator << (ostream& in, roi_info const & val);


struct composition_task_info
{
	apt_uint taskid;							//tasklistID
	roi_info roiifo;							//which type, shaped and sized ROIs should be placed?
	vector<roi_rotated_cylinder> cyl;
	composition_task_info() : taskid(0), roiifo(roi_info()),
		cyl(vector<roi_rotated_cylinder>()) {}
};

ostream& operator << (ostream& in, composition_task_info const & val);


struct input_triangle_soup
{
	string InputfilePrimitives;
	string DatasetPrimitivesVertexPosition;
	string DatasetPrimitivesFacetIndices;
	string DatasetPrimitivesVertexNormal;
	string DatasetPrimitivesFacetNormal;
	string DatasetPrimitivesPatchIndices;
	match_filter<apt_uint> patch_identifier_filter;
	input_triangle_soup() : InputfilePrimitives(""), DatasetPrimitivesVertexPosition(""), DatasetPrimitivesFacetIndices(""),
			DatasetPrimitivesVertexNormal(""), DatasetPrimitivesFacetNormal(""), DatasetPrimitivesPatchIndices(""),
			patch_identifier_filter(match_filter<apt_uint>()) {}
	input_triangle_soup( const string _fnm, const string _dsnm_vrts, const string _dsnm_fcts ) :
		InputfilePrimitives(_fnm), DatasetPrimitivesVertexPosition(_dsnm_vrts), DatasetPrimitivesFacetIndices(_dsnm_fcts),
		DatasetPrimitivesVertexNormal(""), DatasetPrimitivesFacetNormal(""), DatasetPrimitivesPatchIndices(""),
		patch_identifier_filter(match_filter<apt_uint>()) {}
	input_triangle_soup( const string _fnm, const string _dsnm_vrts, const string _dsnm_fcts,
			const string _dsnm_vnrms, const string _dsnm_fnrms, const string _dsnm_ids ) :
			InputfilePrimitives(_fnm), DatasetPrimitivesVertexPosition(_dsnm_vrts), DatasetPrimitivesFacetIndices(_dsnm_fcts),
			DatasetPrimitivesVertexNormal(_dsnm_vnrms), DatasetPrimitivesFacetNormal(_dsnm_fnrms), DatasetPrimitivesPatchIndices(_dsnm_ids),
			patch_identifier_filter(match_filter<apt_uint>()) {}
};

ostream& operator << (ostream& in, input_triangle_soup const & val);



class ConfigNanochem
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static string InputfileDataset;
	static string ReconstructionDatasetName;
	static string MassToChargeDatasetName;
	static string InputfileIonTypes;
	static string IonTypesGroupName;

	//##MK::fuse into a datastructure edge model
	static string InputfileIonToEdgeDistances;
	static string IonToEdgeDistancesDatasetName;
	static input_triangle_soup InputEdgeMesh;

	static ANALYSIS_METHOD AnalysisMethod;

	//add ROI filtering and sub-sampling functionalities
	static WINDOWING_METHOD WindowingMethod;
	static vector<roi_sphere> WindowingSpheres;
	static vector<roi_rotated_cylinder> WindowingCylinders;
	static vector<roi_rotated_cuboid> WindowingCuboids;
	static vector<roi_polyhedron> WindowingPolyhedra;
	//sub-sampling
	static lival<apt_uint> LinearSubSamplingRange;
	//match filters
	static match_filter<unsigned char> IontypeFilter;
	static match_filter<unsigned char> HitMultiplicityFilter;

	//interface modelling
	static vector<isosurf_tasklist> IsoSurfacingTasks;
	static vector<interface_task_info> InterfaceModellingTasks;

	//composition analyes
	static string InputfileIonToFeatureDistances;
	static string IonToFeatureDistancesDatasetName;
	static input_triangle_soup InputFeatureMesh;
	static CHARACTERIZE_AUTOROIS UserAutoRois;
	static vector<roi_rotated_cylinder> UserRoisCylinders;
	static vector<composition_task_info> CompositionAnalysisTasks;

	//allows to study high-throughput many iso-surfaces, for different atom types, and resolutions or iso-values

	//sensible defaults
	static unsigned long MaxMemoryPerNode;
	static size_t TriSoupClusteringKDTreeMaxLeafSize;
	static size_t InterfaceModellingKDTreeMaxLeafSize;
	static apt_real LinearTimeVolVoxelEdgeLength;
	static apt_real MinScalarFieldGradientMagnitude;
	static apt_real TriSoupClusteringMaxDistance;
	static apt_real OBBEpsilon;
	static apt_real ClosedPolyhedraBVHQueryRadius;
	static apt_real MinIonDistanceToEdge;
	static apt_real VolFracQualifyingLargeObjects;
	static apt_uint Int2FloatOverflowThreshold;
	static string TetGenFlags;
	static double PLCtoTetsRelativeVolumeMismatch;
	static size_t MinIonSupportForClosedObjects;
	static apt_uint NGonalPrismNumberOfFacets;
};

#endif
