/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemHdl.h"


bool parse_configuration_from_nexus()
{
	tool_startup( "paraprobe-nanochem" );
	if ( ConfigNanochem::read_config_from_nexus( ConfigShared::ConfigurationFile ) == true ) {
		cout << "Configuration from file " << ConfigShared::ConfigurationFile << " was accepted" << "\n";
		cout << "This analysis has SimulationID " << "SimID." <<  ConfigShared::SimID << "\n";
		cout << "Results of this analysis are written to " << ConfigShared::OutputfileName << "\n";
		cout << endl;
		return true;
	}
	cerr << "Parsing configuration failed !" << "\n";
	return false;
}


void characterize_local_chemistry( const int r, const int nr, char** pargv )
{
	double tic = MPI_Wtime();

	//allocate process-level instance of a nanochemHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	nanochemHdl* chm = NULL;
	int localhealth = 1;
	try {
		chm = new nanochemHdl;
		chm->set_myrank(r);
		chm->set_nranks(nr);
		chm->commit_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " allocating nanochemHdl class object failed !" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	if ( chm->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete chm; chm = NULL; return;
	}

	//let master read input and broadcast to all processes
	if ( chm->get_myrank() == MASTER ) {

		if ( chm->read_relevant_input() == false ) {
			cout << "Rank " << chm->get_myrank() << " failed reading essential pieces of the relevant input !" << "\n";
			localhealth = 0;
		}
	}
	
	//##MK::strictly speaking not necessary
	MPI_Barrier(MPI_COMM_WORLD);
	if ( chm->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << chm->get_myrank() << " has recognized that not all processes have all relevant data !" << "\n";
		delete chm; chm = NULL; return;
	}

	//##MK::add window definition and threaded function that identifies which ion are in which region
	if ( chm->crop_reconstruction_to_analysis_window() == true ) {

		if ( ConfigNanochem::AnalysisMethod == NANOCHEM_MODEL_ISOSURFACES_AND_CHARACTERIZE ) {

			chm->execute_isosurface_workpackage();

		}
		else if ( ConfigNanochem::AnalysisMethod == NANOCHEM_MODEL_SINGLE_INTERFACE_PCA_AND_DCOM ) {

			chm->execute_interfacial_excess_workpackage();

		}
		else if ( ConfigNanochem::AnalysisMethod == NANOCHEM_CHARACTERIZE_USER_INTERFACE_MODEL ) {

			chm->execute_characterize_composition_workpackage();

		}
		else {
			cout << "Bored, no analyses to do..." << "\n";
		}

	}
	else {
		cout << "For the given filter settings no ion would remain in the ROI !" << "\n";
	}

	//chm->nanochem_tictoc.spit_profiling( "Nanochem", ConfigShared::SimID, chm->get_myrank() );

	//release resources
	delete chm; chm = NULL;

	double toc = MPI_Wtime();
	cout << "characterize_local_chemistry took on rank " << r << " took " << (toc-tic) << "\n";

}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	string start_time = timestamp_now_iso8601();

	if ( argc == 1 || argc == 2 ) {
		string cmd_option = "--help";
		if ( argc == 2 ) { cmd_option = argv[1]; };
		command_line_help( cmd_option, "nanochem" );
		return 0;
	}
	else if ( argc == 3 ) {
		ConfigShared::SimID = stoul( argv[SIMID] );
		ConfigShared::ConfigurationFile = argv[CONTROLFILE];
		ConfigShared::OutputfilePrefix = "PARAPROBE.Nanochem.Results.SimID." + to_string(ConfigShared::SimID);
		ConfigShared::OutputfileName = ConfigShared::OutputfilePrefix + ".nxs";
		if ( init_results_nxs( ConfigShared::OutputfileName, "nanochem", start_time, 1 ) == false ) {
			return 0;
		}
	}
	else {
		return 0;
	}

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( parse_configuration_from_nexus() == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	
	if ( nr == SINGLEPROCESS ) {

		cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

//EXECUTE SPECIFIC TASK
		characterize_local_chemistry( r, nr, argv );
	}
	else {
		cerr << "Rank " << r << " currently paraprobe-nanochem is implemented for a single process only !" << "\n";
	}

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	if ( finish_results_nxs( ConfigShared::OutputfileName, start_time, tic, 1 ) == true ) {
		cout << "paraprobe-nanochem success" << endl;
	}
	return 0;
}
