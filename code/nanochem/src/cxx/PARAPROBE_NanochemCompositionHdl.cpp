/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemCompositionHdl.h"


compositionHdl::compositionHdl()
{
	rois_cyl = vector<roi_rotated_cylinder*>();
	//rois_obb = vector<roi_rotated_cuboid*>();
}


compositionHdl::~compositionHdl()
{
	//make sure to release memory pointed to by pointers in rois_cyl
	for ( size_t i = 0; i < rois_cyl.size(); i++ ) {
		if ( rois_cyl[i] != NULL ) {
			delete rois_cyl[i];
			rois_cyl[i] = NULL;
		}
	}
	rois_cyl = vector<roi_rotated_cylinder*>();
	/*
	for ( size_t j = 0; j < rois_obb.size(); j++ ) {
		if ( rois_obb[j] != NULL ) {
			delete rois_obb[j];
			rois_obb[j] = NULL;
		}
	}
	rois_cyl = vector<roi_rotated_cylinder*>();
	*/
}


bool compositionHdl::multithreaded_composition_analysis_with_autorois(
		composition_task_info const & tskifo,
		rangeTable & rng,
		LinearTimeVolQuerier<p3dmidx> & ionbvh,
		vector<tri3d> const & edge_model_triangles,
		vector<tri3d> const & feature_model_triangles,
		vector<p3d> const & feature_model_facet_normals,
		vector<apt_uint> const & feature_model_facet_patchids )
{
	double tic = MPI_Wtime();

	#pragma omp parallel
	{
		//thread-local copies of triangles and thread-local BVH querier for the edge_mesh aka
		//triangle tree, triangle infos and their facet normals
		vector<tri3d> my_edge_tris = edge_model_triangles;
		vector<tri3d> my_feature_tris = feature_model_triangles;
		vector<p3d> my_feature_tris_fnrm = feature_model_facet_normals;
		vector<apt_uint> my_feature_tris_patchids = feature_model_facet_patchids;

		Tree* my_edge_tris_in_a_bvh = NULL;
		if ( my_edge_tris.size() > 0 ) { //only when there is a mesh
			my_edge_tris_in_a_bvh = new class Tree();

			for( size_t k = 0; k < my_edge_tris.size(); k++ ) {
				my_edge_tris_in_a_bvh->insertParticle( k,
					trpl( 	fmin(fmin(my_edge_tris[k].x1, my_edge_tris[k].x2), my_edge_tris[k].x3) - EPSILON,
							fmin(fmin(my_edge_tris[k].y1, my_edge_tris[k].y2), my_edge_tris[k].y3) - EPSILON,
							fmin(fmin(my_edge_tris[k].z1, my_edge_tris[k].z2), my_edge_tris[k].z3) - EPSILON  ),
					trpl(  	fmax(fmax(my_edge_tris[k].x1, my_edge_tris[k].x2), my_edge_tris[k].x3) + EPSILON,
							fmax(fmax(my_edge_tris[k].y1, my_edge_tris[k].y2), my_edge_tris[k].y3) + EPSILON,
							fmax(fmax(my_edge_tris[k].z1, my_edge_tris[k].z2), my_edge_tris[k].z3) + EPSILON  ) );
				//we can use k as the particle ID because hedgesAABB uses a map<particle, node>
			}
		}

		set<unsigned char> my_disjoint_elements = rng.disjoint_elements;
		map<unsigned char, vector<apt_uint>> my_ityp2mltply;
		for( auto ionit = rng.ityp2mltply.begin(); ionit != rng.ityp2mltply.end(); ionit++ ) {
			my_ityp2mltply[ionit->first] = ionit->second;
		}

		//##MK::BEGIN DEBUGGING
		#pragma omp master
		{
			cout << "Composition analyses, autoROIs" << "\n";
			cout << "These are the disjoint elements:" << "\n";
			for ( auto kt = my_disjoint_elements.begin(); kt != my_disjoint_elements.end(); kt++ ) {
				cout << (int) *kt << "\n";
			}
			cout << "These are the ityp2mltply" << "\n";
			for( auto ionit = rng.ityp2mltply.begin(); ionit != rng.ityp2mltply.end(); ionit++ ) {
				cout << "ionit->first " << (int) ionit->first << "\n";
				for ( auto kt = ionit->second.begin(); kt != ionit->second.end(); kt++ ) {
					cout << " " << *kt;
				}
				cout << "\n";
			}

			cout << "Performing multi-threaded automated ROI placing and analyses ..." << "\n";
		}
		//##MK::END DEBUGGING

		//thread-local cache for storing pointers to computed ROIs with metadata and numerical results
		//vector<roi_rotated_cuboid*> my_rois_obb;
		vector<roi_rotated_cylinder*> my_rois_cyl;

		//start processing, get the triangle facets, place and orient a ROI at each,
		//check that the ROI does not intersect with the edge of the dataset, and compute 1D proxigram


		//###MK::define match filter for triangles to be analyzed based on patchids
		set<apt_uint> whitelist;
		/*
		//IUC09 NFDI-MatWerk Aparna Saksena dataset presented at the MSE2022
		whitelist.insert( 47 );
		whitelist.insert( 598 );
		whitelist.insert( 1355 );
		*/
		if ( ConfigNanochem::InputFeatureMesh.patch_identifier_filter.is_whitelist == true ) {
			for ( auto kt = ConfigNanochem::InputFeatureMesh.patch_identifier_filter.candidates.begin();
					kt != ConfigNanochem::InputFeatureMesh.patch_identifier_filter.candidates.end(); kt++ ) {
				whitelist.insert( *kt );
			}
		}
		cout << "whitelist.size() " << whitelist.size() << "\n";

		//multi-threaded processing of independent triangle facets
		//##MK::currently all cylindrical ROIs have the same shape and size
		#pragma omp for schedule(dynamic, 1)
		for( size_t facetID = 0; facetID < my_feature_tris.size(); facetID++ ) {
			//place a ROI automatically (barycenter of ROI in barycenter of triangle)
			//for 1D ROI align the longest axis of the ROI parallel to the (positive) direction of the triangle unit normal
			//for 3D ROI align the ROI with the positive direction of each direction of a set of directions (here sampled from geodesic sphere)

			bool consider = false;
			apt_uint patchid = my_feature_tris_patchids[facetID];
			if ( patchid == UNDEFINED_PATCHID ) {
				consider = true;
			}
			else {
				if ( whitelist.find( patchid ) != whitelist.end() ) {
					consider = true;
				}
			}

			if ( consider == true ) {

				//compute where we want to place and how to orient the primitive relative to the triangle facet
				tri3d fct = my_feature_tris[facetID];

				//center the barycenter of the roi in the barycenter of the triangle
				p3d bary = fct.barycenter();

				//orient the roi signed perpendicular to the triangle plane pointing from barycenter outwards of the closed polyhedron
				p3d direction = my_feature_tris_fnrm[facetID];
				apt_real SQRlen = SQR(direction.x)+SQR(direction.y)+SQR(direction.z);
				if ( SQRlen < EPSILON ) {
					continue;
				}
				//##MK::switch to use GeodesicSpheres::Kuehbach40962

				//apt_uint roi_type = CG_CYLINDER;
				//##MK::switch between cuboid and cylinder

				roi_rotated_cylinder* roi_cyl = NULL;
				//##MK::roi_rotated_cuboid* roi_obb = NULL;

				roi_cyl = new roi_rotated_cylinder(
					bary, direction, tskifo.roiifo.height, tskifo.roiifo.radius );

				if ( roi_cyl != NULL ) {
					roi_cyl->roiID = facetID; //may be that there is a facetID 0

					//if ( facetID == 10000 ) {
					//	cout << "Reporting ROI 10000 " << *roi_cyl << "\n";

					roi_cyl->ownerID = 0;
					roi_cyl->nions = 0;
					roi_cyl->natoms = 0;
					//##MK::ownerID currently not used, could be used when different features are distinguished
					//##MK::for the similar iso-surface hdl auto roi functionalities this is used to store the object ID

					//##MK::BEGIN DEBUG
					//roi_cyl->itypes_whitelist = tskifo.roiifo.itypes_whitelist;
					//roi_cyl->elemtypes_whitelist = tskifo.roiifo.elemtypes_whitelist;
					//create a proxigram for each atom type
					roi_cyl->itypes_whitelist = vector<unsigned char>();
					roi_cyl->elemtypes_whitelist = vector<unsigned char>();
					for( auto kt = rng.disjoint_elements.begin(); kt != rng.disjoint_elements.end(); kt++ ) {
						roi_cyl->elemtypes_whitelist.push_back( *kt );
					}
					//##MK::END DEBUG

					roi_cyl->init_proxigrams();

					bool cylinder_edge_intersection = false; //assume cylinder does not intersect with the edge of the dataset
					aabb3d roi_in_a_box = roi_cyl->get_aabb();
					roi_in_a_box.scale();

					if ( my_edge_tris_in_a_bvh != NULL ) {
						apt_cylinder test_primitive = apt_cylinder( roi_cyl->p1, roi_cyl->p2, roi_cyl->r );
						//check if roi intersects with the edge of the dataset
						//inspect the neighboring triangles of the facet
						hedgesAABB nb_aabb( trpl( roi_in_a_box.xmi - EPSILON, roi_in_a_box.ymi - EPSILON, roi_in_a_box.zmi - EPSILON),
											trpl( roi_in_a_box.xmx + EPSILON, roi_in_a_box.ymx + EPSILON, roi_in_a_box.zmx + EPSILON) );

						vector<apt_uint> cand = my_edge_tris_in_a_bvh->query( nb_aabb );

						for( size_t j = 0; j < cand.size(); j++ ) { //strategy prove these candidate triangles for not intersecting
							tri3d T2 = my_edge_tris[cand[j]];

							if ( intersectTriangleCylinder( T2, test_primitive ) == true ) {
								//eventually the triangle intersects with the ROI cylinder...
								cylinder_edge_intersection = true;
								break; //...in this case there is no need to test further triangle candidates
							}
						} //...otherwise every eventually candidate has to be tested to be sure
					} //done checking for possible intersection of roi with edge triangles if such were loaded

					if ( cylinder_edge_intersection == false ) {
						//analyze a ROI cylinder only if it is completely embedded in the dataset, otherwise there are edge effects

						//find ions inside AABB about the roi
						vector<p3dmidx> cand_ions;
						ionbvh.query_noclear_nosort( roi_in_a_box, cand_ions );

						for( size_t k = 0; k < cand_ions.size(); k++ ) {
							//for those ions find those which are really inside or on the surface of the roi
							p3d p = p3d( cand_ions[k].x, cand_ions[k].y, cand_ions[k].z );
							if ( roi_cyl->is_inside( p ) == true ) {
								roi_cyl->nions++;
								//cout << "p is_inside " << p.x << ";" << p.y << ";" << p.z << ";" << (int) cand_ions[k].m1 << "\n";
								//for those ions in the cylinder find if they contribute atoms of elements of type in the elemtypes_whitelist
								map<unsigned char, vector<apt_uint>>::iterator thisone = my_ityp2mltply.find( cand_ions[k].m1 );
								if ( thisone != my_ityp2mltply.end() ) { //we found a multiplicator vector
									for( auto kt = roi_cyl->elemtypes_whitelist.begin(); kt != roi_cyl->elemtypes_whitelist.end(); kt++ ) {
										//for each elemtype-specific ROI
										size_t ii = 0;
										//get the multiplicity of this guy
										for( auto lt = my_disjoint_elements.begin(); lt != my_disjoint_elements.end(); lt++, ii++ ) {
											//current element we search is *kt
											if ( *kt != *lt ) {
												continue;
											}
											else { //found
												unsigned char element = *kt;
												apt_uint multiplicity = thisone->second.at(ii);
												if ( multiplicity > 0 ) {
													//#pragma omp critical
													//{
													//	cout << "facetID " << facetID << " p is_inside " << p.x << ";" << p.y << ";" << p.z << ";" << (int) cand_ions[k].m1 << " element " << (int) element << " mult " << multiplicity << "\n";
													//}

													//project only in these cases the ion signed onto the facet plane
													roi_cyl->proxigram_add( p, element, multiplicity, SIGNED_DISTANCE_BASED_ON_BARYCENTER ); //atomically decomposed
													roi_cyl->natoms += multiplicity;

													//project with a specific precomputed distance value
													//##MK::roi_cyl->proxigram_add( sgn_distance, element, multiplicity );

													break; //no need to test further disjoint element idx found
												}
											}
										}
									} //done with checking contributions of ion at position p for the elemtypes_whitelist
								}
								else {
									//##MK::should not happen but if it does its a sign that is relevant
									#pragma omp critical
									{
										cerr << "Thread " << omp_get_thread_num() << " found an ion for which there is no matching multiplicator vector" << "\n";
									}
								}
							} //done processing an ion inside roi_cyl
						} //next candidate ion

						roi_cyl->update_proxigrams(); //sorting etc.

						roi_cyl->compute_joint_proxigrams(); //fusing the individual ion/element-specific proxigrams
						//to a single one and store the position of each ion/element in the sorted sequence of signed distances

						roi_cyl->close_to_the_edge = false;
					} //done with a roi which should be processed
					else {
						roi_cyl->close_to_the_edge = true;
					}

					//register the result for roi_cyl
					my_rois_cyl.push_back( roi_cyl ); //so that the pointed to memory regions get not forgotten

					/*
					//##MK::BEGIN DEBUG
					#pragma omp critical
					{
						cout << "roi_cyl " << roi_cyl << " was registered" << "\n";
					}
					//##MK::END DEBUG
					*/

				} //done with the case for an instantiated cylindrical roi

			} //done with the case of a facet that should be analyzed

			if ( (my_rois_cyl.size() > 0) && (my_rois_cyl.size() % 1000 == 0) ) {
				#pragma omp critical
				{
					cout << "Thread " << omp_get_thread_num() << " has processed my_rois_cyl.size() " << my_rois_cyl.size() << "\n";
				}
			}

		} //next triangle facet from the feature mesh

		//register my results with the object
		#pragma omp critical
		{
			for( size_t jj = 0; jj < my_rois_cyl.size(); jj++ ) {
				rois_cyl.push_back( my_rois_cyl[jj] ); //hand over the pointer but do not delete it here by creating thread as
				//otherwise the results would no longer be accessible once out of the parallel region, causing memory flaw
			}
		}

		//release the thread-local triangle tree
		delete my_edge_tris_in_a_bvh;
		my_edge_tris_in_a_bvh = NULL;

	} //end of parallel region

	/*
	//##MK::BEGIN DEBUG
	cout << "Debugging results from composition analysis task " << taskifo.taskid << "\n";
	cout << "rois_cyl.size() " << rois_cyl.size() << "\n";
	for( size_t jj = 0; jj < rois_cyl.size(); jj++ ) {
		if ( rois_cyl.at(jj) != NULL ) {
			//cout << "rois_cyl[jj]->elemtyp_proxigrams.size() " << rois_cyl[jj]->elemtyp_proxigrams.size() << "\n";
			for( auto kkt = rois_cyl[jj]->elemtyp_proxigrams.begin(); kkt != rois_cyl[jj]->elemtyp_proxigrams.end(); kkt++ ) {
				if ( kkt->second != NULL ) {
					if ( kkt->second->size() > 0 ) {
						cout << "elemtyp_proxigrams " << (int) kkt->first << " size " << kkt->second->size() << " roiID " << rois_cyl[jj]->roiID << "\n";
						//for( size_t ll = 0; ll < kkt->second->size(); ll++ ) {
						//	cout << kkt->second->at(ll) << "\n";
						//}
					}
				}
				//cout << "\n";
			}
		}
	}
	//##MK::END DEBUG
	*/

	double toc = MPI_Wtime();
	cout << "Multithreaded placing and analyzing of automated ROIs for composition analysis taskid " << tskifo.taskid << " took " << (toc-tic) << " seconds" << "\n";

	return true;
}


bool compositionHdl::multithreaded_composition_analysis_with_autorois(
		composition_task_info & tskifo,
		rangeTable & rng,
		LinearTimeVolQuerier<p3dmidx> & ionbvh,
		vector<tri3d> const & edge_model_triangles)
{
	double tic = MPI_Wtime();

	#pragma omp parallel
	{
		//thread-local copies of triangles and thread-local BVH querier for the edge_mesh aka
		//triangle tree, triangle infos and their facet normals
		vector<tri3d> my_edge_tris = edge_model_triangles;
		Tree* my_edge_tris_in_a_bvh = NULL;
		if ( my_edge_tris.size() > 0 ) { //only when there is a mesh
			my_edge_tris_in_a_bvh = new class Tree();

			for( size_t k = 0; k < my_edge_tris.size(); k++ ) {
				my_edge_tris_in_a_bvh->insertParticle( k,
					trpl( 	fmin(fmin(my_edge_tris[k].x1, my_edge_tris[k].x2), my_edge_tris[k].x3) - EPSILON,
							fmin(fmin(my_edge_tris[k].y1, my_edge_tris[k].y2), my_edge_tris[k].y3) - EPSILON,
							fmin(fmin(my_edge_tris[k].z1, my_edge_tris[k].z2), my_edge_tris[k].z3) - EPSILON  ),
					trpl(  	fmax(fmax(my_edge_tris[k].x1, my_edge_tris[k].x2), my_edge_tris[k].x3) + EPSILON,
							fmax(fmax(my_edge_tris[k].y1, my_edge_tris[k].y2), my_edge_tris[k].y3) + EPSILON,
							fmax(fmax(my_edge_tris[k].z1, my_edge_tris[k].z2), my_edge_tris[k].z3) + EPSILON  ) );
				//we can use k as the particle ID because hedgesAABB uses a map<particle, node>
			}
		}

		set<unsigned char> my_disjoint_elements = rng.disjoint_elements;
		map<unsigned char, vector<apt_uint>> my_ityp2mltply;
		for( auto ionit = rng.ityp2mltply.begin(); ionit != rng.ityp2mltply.end(); ionit++ ) {
			my_ityp2mltply[ionit->first] = ionit->second;
		}

		//##MK::BEGIN DEBUGGING
		#pragma omp master
		{
			cout << "Composition analyses, autoROIs" << "\n";
			cout << "These are the disjoint elements:" << "\n";
			for ( auto kt = my_disjoint_elements.begin(); kt != my_disjoint_elements.end(); kt++ ) {
				cout << (int) *kt << "\n";
			}
			cout << "These are the ityp2mltply" << "\n";
			for( auto ionit = rng.ityp2mltply.begin(); ionit != rng.ityp2mltply.end(); ionit++ ) {
				cout << "ionit->first " << (int) ionit->first << "\n";
				for ( auto kt = ionit->second.begin(); kt != ionit->second.end(); kt++ ) {
					cout << " " << *kt;
				}
				cout << "\n";
			}

			cout << "Performing multi-threaded automated ROI placing and analyses ..." << "\n";
		}
		//##MK::END DEBUGGING

		//thread-local cache for storing pointers to computed ROIs with metadata and numerical results
		//vector<roi_rotated_cuboid*> my_rois_obb;
		vector<roi_rotated_cylinder*> my_rois_cyl;

		//start processing, get the triangle facets, place and orient a ROI at each,
		//check that the ROI does not intersect with the edge of the dataset, and compute 1D proxigram

		//###MK::define match filter for triangles to be analyzed based on patchids

		//multi-threaded processing of independent triangle facets
		//##MK::currently all cylindrical ROIs have the same shape and size
		#pragma omp for schedule(dynamic, 1)
		for( size_t cylID = 0; cylID < tskifo.cyl.size(); cylID++ ) {
			//place a ROI automatically (lower end of the oriented cylinder is local origin)
			//for 1D ROI align the longest axis of the ROI parallel to the (positive) direction of the triangle unit normal
			p3d bottom_cap = tskifo.cyl.at(cylID).p1;
			p3d top_cap = tskifo.cyl[cylID].p2;
			p3d direction = p3d(
				top_cap.x - bottom_cap.x,
				top_cap.y - bottom_cap.y,
				top_cap.z - bottom_cap.z );
			p3d bary = p3d(
				0.5 * (top_cap.x + bottom_cap.x),
				0.5 * (top_cap.y + bottom_cap.y),
				0.5 * (top_cap.z + bottom_cap.z)); //should be ahead and behind...
			apt_real SQRlen = SQR(direction.x)+SQR(direction.y)+SQR(direction.z);
			if ( SQRlen < EPSILON ) {
				continue;
			}

			//apt_uint roi_type = CG_CYLINDER;
			//##MK::switch between cuboid and cylinder
			roi_rotated_cylinder* roi_cyl = NULL;
			//##MK::roi_rotated_cuboid* roi_obb = NULL;

			roi_cyl = new roi_rotated_cylinder(	bottom_cap, top_cap, tskifo.cyl[cylID].r );

			if ( roi_cyl != NULL ) {
				roi_cyl->roiID = cylID + 1;

				roi_cyl->ownerID = 0;
				roi_cyl->nions = 0;
				roi_cyl->natoms = 0;
				//##MK::ownerID currently not used, could be used when different features are distinguished
				//##MK::for the similar iso-surface hdl auto roi functionalities this is used to store the object ID

				//##MK::BEGIN DEBUG
				//roi_cyl->itypes_whitelist = tskifo.roiifo.itypes_whitelist;
				//roi_cyl->elemtypes_whitelist = tskifo.roiifo.elemtypes_whitelist;
				//create a proxigram for each atom type
				roi_cyl->itypes_whitelist = vector<unsigned char>();
				roi_cyl->elemtypes_whitelist = vector<unsigned char>();
				for( auto kt = rng.disjoint_elements.begin(); kt != rng.disjoint_elements.end(); kt++ ) {
					roi_cyl->elemtypes_whitelist.push_back( *kt );
				}
				//##MK::END DEBUG

				roi_cyl->init_proxigrams();

				bool cylinder_edge_intersection = false; //assume cylinder does not intersect with the edge of the dataset
				aabb3d roi_in_a_box = roi_cyl->get_aabb();
				roi_in_a_box.scale();

				if ( my_edge_tris_in_a_bvh != NULL ) {
					apt_cylinder test_primitive = apt_cylinder( roi_cyl->p1, roi_cyl->p2, roi_cyl->r );
					//check if roi intersects with the edge of the dataset
					//inspect the neighboring triangles of the facet
					hedgesAABB nb_aabb( trpl( roi_in_a_box.xmi - EPSILON, roi_in_a_box.ymi - EPSILON, roi_in_a_box.zmi - EPSILON),
										trpl( roi_in_a_box.xmx + EPSILON, roi_in_a_box.ymx + EPSILON, roi_in_a_box.zmx + EPSILON) );

					vector<apt_uint> cand = my_edge_tris_in_a_bvh->query( nb_aabb );

					for( size_t j = 0; j < cand.size(); j++ ) { //strategy prove these candidate triangles for not intersecting
						tri3d T2 = my_edge_tris[cand[j]];

						if ( intersectTriangleCylinder( T2, test_primitive ) == true ) {
							//eventually the triangle intersects with the ROI cylinder...
							cylinder_edge_intersection = true;
							break; //...in this case there is no need to test further triangle candidates
						}
					} //...otherwise every eventually candidate has to be tested to be sure
				} //done checking for possible intersection of roi with edge triangles if such were loaded

				if ( cylinder_edge_intersection == false ) {
					//analyze a ROI cylinder only if it is completely embedded in the dataset, otherwise there are edge effects

					//find ions inside AABB about the roi
					vector<p3dmidx> cand_ions;
					ionbvh.query_noclear_nosort( roi_in_a_box, cand_ions );

					for( size_t k = 0; k < cand_ions.size(); k++ ) {
						//for those ions find those which are really inside or on the surface of the roi
						p3d p = p3d( cand_ions[k].x, cand_ions[k].y, cand_ions[k].z );
						if ( roi_cyl->is_inside( p ) == true ) {
							roi_cyl->nions++;
							//cout << "p is_inside " << p.x << ";" << p.y << ";" << p.z << ";" << (int) cand_ions[k].m1 << "\n";
							//for those ions in the cylinder find if they contribute atoms of elements of type in the elemtypes_whitelist
							map<unsigned char, vector<apt_uint>>::iterator thisone = my_ityp2mltply.find( cand_ions[k].m1 );
							if ( thisone != my_ityp2mltply.end() ) { //we found a multiplicator vector
								for( auto kt = roi_cyl->elemtypes_whitelist.begin(); kt != roi_cyl->elemtypes_whitelist.end(); kt++ ) {
									//for each elemtype-specific ROI
									size_t ii = 0;
									//get the multiplicity of this guy
									for( auto lt = my_disjoint_elements.begin(); lt != my_disjoint_elements.end(); lt++, ii++ ) {
										//current element we search is *kt
										if ( *kt != *lt ) {
											continue;
										}
										else { //found
											unsigned char element = *kt;
											apt_uint multiplicity = thisone->second.at(ii);
											if ( multiplicity > 0 ) {
												roi_cyl->proxigram_add( p, element, multiplicity, SIGNED_DISTANCE_BASED_ON_LOWEST_CAP ); //atomically decomposed
												roi_cyl->natoms += multiplicity;
													//project with a specific precomputed distance value
												//##MK::roi_cyl->proxigram_add( sgn_distance, element, multiplicity );
													break; //no need to test further disjoint element idx found
											}
										}
									}
								} //done with checking contributions of ion at position p for the elemtypes_whitelist
							}
							else {
								//##MK::should not happen but if it does its a sign that is relevant
								#pragma omp critical
								{
									cerr << "Thread " << omp_get_thread_num() << " found an ion for which there is no matching multiplicator vector" << "\n";
								}
							}
						} //done processing an ion inside roi_cyl
					} //next candidate ion

					roi_cyl->update_proxigrams(); //sorting etc.

					roi_cyl->compute_joint_proxigrams(); //fusing the individual ion/element-specific proxigrams
						//to a single one and store the position of each ion/element in the sorted sequence of signed distances

					roi_cyl->close_to_the_edge = false;
				} //done with a roi which should be processed
				else {
					roi_cyl->close_to_the_edge = true;
				}

				//register the result for roi_cyl
				my_rois_cyl.push_back( roi_cyl ); //so that the pointed to memory regions get not forgotten

			} //done with the case for an instantiated cylindrical roi

		} //done with the case of a facet that should be analyzed

		if ( (my_rois_cyl.size() > 0) && (my_rois_cyl.size() % 1000 == 0) ) {
			#pragma omp critical
			{
				cout << "Thread " << omp_get_thread_num() << " has processed my_rois_cyl.size() " << my_rois_cyl.size() << "\n";
			}
		}

		//register my results with the object
		#pragma omp critical
		{
			for( size_t jj = 0; jj < my_rois_cyl.size(); jj++ ) {
				rois_cyl.push_back( my_rois_cyl[jj] ); //hand over the pointer but do not delete it here by creating thread as
				//otherwise the results would no longer be accessible once out of the parallel region, causing memory flaw
			}
		}

		//release the thread-local triangle tree
		delete my_edge_tris_in_a_bvh;
		my_edge_tris_in_a_bvh = NULL;

	} //end of parallel region

	double toc = MPI_Wtime();
	cout << "Multithreaded placing and analyzing of user-defined ROIs for composition analysis taskid " << tskifo.taskid << " took " << (toc-tic) << " seconds" << "\n";

	return true;
}


size_t compositionHdl::write_automated_rois_for_composition_h5(
		composition_task_info const & tskifo )
{
	double tic = MPI_Wtime();

	if ( rois_cyl.size() >= (size_t) UMX ) {
		cerr << "The total number of objects or regions of interest to write out is larger than currently supported by the implementation !" << "\n";
		return 0;
		//##MK::change wuibuf to a 64-bit type , e.g. ui64 size_t
	}
	if ( rois_cyl.size() < 1 ) {
		cout << "WARNING:: There are no objects to report !" << "\n";
		return 0;
	}

	//currently reporting only objects far from the edge which have at least one ion
	apt_uint n_rois_cyl = 0;
	vector<apt_real> f32;
	vector<apt_uint> u32;
	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) { //an existent roi_rotated_cylinder instance
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				create_ngonal_prism( (*it)->p1, (*it)->p2, (*it)->r,
						f32, u32, ConfigNanochem::NGonalPrismNumberOfFacets );
				n_rois_cyl++;
	}}}

	string grpnm = "";
	string dsnm = "";

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );

	ioAttributes anno = ioAttributes();
	apt_uint entry_id = 1;
	//apt_uint proc_id = tskifo.taskid; only one is allowed currently + to_string(proc_id);
	grpnm = "/entry" + to_string(entry_id) + "/oned_profile";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	/*
	string sub_grpnm = grpnm + "/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_filter_boolean_mask") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) iinfo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/bitdepth";
	apt_uint bitdepth = 8;
	if ( h5w.nexus_write( dsnm, bitdepth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/mask";
	bool* bitfield = NULL;
	bitfield = new bool[n_ions];
	if ( bitfield != NULL ) {
		vector<unsigned char> u08_bitfield = vector<unsigned char>();
		for( size_t i = 0; i < iinfo.size(); i++ ) {
			bitfield[i] = ( iinfo[i].mask1 == ANALYZE_YES ) ? true : false;
		}
		BitPacker packbits;
		packbits.bool_to_bitpacked_uint8( bitfield,  n_ions, u08_bitfield );
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) {
				delete [] bitfield; bitfield = NULL;
				return false;
		}
		delete [] bitfield; bitfield = NULL;
	}
	*/

	string sub_grpnm = grpnm + "/xdmf_cylinder";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_polyhedron_set") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/dimensionality";
	apt_uint dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/cardinality";
	apt_uint card = n_rois_cyl;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, card, anno) != MYHDF5_SUCCESS ) { return false; }

	//ROI geometry
	dsnm = sub_grpnm + "/vertices";
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({f32.size() / 3, 3},
				{f32.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		f32,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	f32 = vector<apt_real>();

	dsnm = sub_grpnm + "/xdmf_topology";
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u32.size()}, {u32.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u32,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u32 = vector<apt_uint>();

	dsnm = sub_grpnm + "/center";
	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) { //an existent roi_rotated_cylinder instance
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				p3d bary = p3d( 0.5*( (*it)->p1.x + (*it)->p2.x ),
								0.5*( (*it)->p1.y + (*it)->p2.y ),
								0.5*( (*it)->p1.z + (*it)->p2.z ) );
				f32.push_back( bary.x );
				f32.push_back( bary.y );
				f32.push_back( bary.z );
	}}}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({f32.size() / 3, 3},
				{f32.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		f32,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	f32 = vector<apt_real>();

	dsnm = sub_grpnm + "/orientation";
	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) { //an existent roi_rotated_cylinder instance
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				p3d p2p1 = p3d(	(*it)->p2.x - (*it)->p1.x,
								(*it)->p2.y - (*it)->p1.y,
								(*it)->p2.z - (*it)->p1.z );
				f32.push_back( p2p1.x );
				f32.push_back( p2p1.y );
				f32.push_back( p2p1.z );
	}}}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({f32.size() / 3, 3},
				{f32.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		f32,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	f32 = vector<apt_real>();

	//ROI metadata
	apt_uint n_fcts = 1 +  ConfigNanochem::NGonalPrismNumberOfFacets + 1;
	dsnm = sub_grpnm + "/identifier";
	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) { //an existent roi_rotated_cylinder instance
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				//top and bottom caps
				//what is inconvenient with XDMF is that, given that there is no cylinder primitive
				//needs duplicates such that each primitive from which the cylinder ngons is constructed
				//this is however well compressible this array has a low signal entropy
				//do not duplicate these pieces of information though for each ngon, store it only for documentation purposes,
				//##MK::to enable e.g. spatial queries of ROIs within python code...
				apt_uint rrid = (*it)->roiID;
				u32.insert(u32.end(), n_fcts, rrid);
			}
		}
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u32.size()}, {u32.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u32,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u32 = vector<apt_uint>();

	/*
	dsnm = sub_grpnm + "/edge_contact";
	vector<unsigned char> u8;
	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) { //an existent roi_rotated_cylinder instance
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				unsigned char flag = ( (*it)->close_to_the_edge == false ) ? 0x00 : 0x01;
				u8.insert(u8.end(), n_fcts, flag);
			}
		}
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u8.size()}, {u8.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u8,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u8 = vector<unsigned char>();
	*/

	//optional from the NeXus perspective, currently for display in ParaView
	dsnm = sub_grpnm + "/number_of_atoms";
	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) { //an existent roi_rotated_cylinder instance
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				u32.insert( u32.end(), n_fcts, (*it)->natoms );
	}}}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u32.size()}, {u32.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u32,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u32 = vector<apt_uint>();

	dsnm = sub_grpnm + "/number_of_ions";
	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) { //an existent roi_rotated_cylinder instance
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				u32.insert( u32.end(), n_fcts, (*it)->nions );
	}}}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u32.size()}, {u32.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u32,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u32 = vector<apt_uint>();

	//individual proxigrams
	string sub_sub_grpnm = sub_grpnm + "/rois_far_from_edge";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXprocess") );
	if ( h5w.nexus_write_group( sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	for( auto it = rois_cyl.begin(); it != rois_cyl.end(); it++ ) {
		if ( *it != NULL ) {
			//##MK::write one sorted list of distances for each element see v0.3.1 src code
			if ( (*it)->elemtyp_global_proxigram.size() > 0 ) {
				//write one total list with sorted distances and a utility array which gives for each distance the isotope hash value
				sub_sub_grpnm = sub_grpnm + "/rois_far_from_edge/roi" + to_string((*it)->roiID);
				anno = ioAttributes();
				anno.add( "NX_class", string("NXobject") );
				if ( h5w.nexus_write_group( sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

				dsnm = sub_sub_grpnm + "/signed_distance";
				vector<apt_real> real;
				anno = ioAttributes();
				anno.add( "unit", string("nm") );
				for( auto jt = (*it)->elemtyp_global_proxigram.begin(); jt != (*it)->elemtyp_global_proxigram.end(); jt++ ) {
					real.push_back( jt->d ); //signed distance in nm
				}
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<apt_real>();

				dsnm = sub_sub_grpnm + "/nuclide_hash";
				anno = ioAttributes();
				vector<unsigned short> uint;
				for( auto jt = (*it)->elemtyp_global_proxigram.begin(); jt != (*it)->elemtyp_global_proxigram.end(); jt++ ) {
					uint.push_back( jt->isotope ); //isotope hash value
				}
				if ( h5w.nexus_write(
					dsnm,
					io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
					uint,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				uint = vector<unsigned short>();
			}
			//release resources for the entire roi_cyl
			(*it)->clear_proxigrams();
		} //an existent roi
	} //next cylindrical ROI

	double toc = MPI_Wtime();
	//memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	//isosurf_tictoc.prof_elpsdtime_and_mem( "WriteAutomatedRoisClosedPolyhedraH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	cout << "Write results from automated rois for composition analyses taskid " << tskifo.taskid << " took " << (toc-tic) << " seconds" << "\n";
	return (size_t) n_rois_cyl;
}
