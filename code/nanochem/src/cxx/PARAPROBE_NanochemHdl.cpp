/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemHdl.h"


nanochemHdl::nanochemHdl()
{
	window = vector<unsigned char>();
	feature = triangleSoup();
	ions_ion2feat = vector<apt_real>();
	ion_bvh = LinearTimeVolQuerier<p3dmidx>();
	nanochem_tictoc = profiler();
}


nanochemHdl::~nanochemHdl()
{
	window = vector<unsigned char>();
	feature = triangleSoup();
	ions_ion2feat = vector<apt_real>();
	ion_bvh = LinearTimeVolQuerier<p3dmidx>();
	nanochem_tictoc = profiler();
}


bool nanochemHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigNanochem::InputfileDataset, ConfigNanochem::ReconstructionDatasetName ) == true &&
			read_ranging_from_h5( ConfigNanochem::InputfileIonTypes, ConfigNanochem::IonTypesGroupName ) == true &&
				read_ionlabels_from_h5( ConfigNanochem::InputfileIonTypes, ConfigNanochem::IonTypesGroupName ) == true ) {

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//build also here a default array with pieces of information for all ions
		if ( ions.ionpp3.size() > 0 && ions.ionifo.size() != ions.ionpp3.size() ) {
			try {
				ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(),
						p3dinfo( RMX, UNKNOWNTYPE, UNKNOWNTYPE, 0x01, WINDOW_ION_EXCLUDE ) );
				//assume all ions are infinitely large from the edge, of unknown (default ion type), and multiplicity one (i.e. 0x01)
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of information array for the ions failed !" << "\n";
				return false;
			}
		}

		if ( ConfigNanochem::InputfileIonToEdgeDistances != "" ) {
			if ( read_ion2edge_distances() == true ) {
				cout << "Rank " << "MASTER" << " all ion-to-edge distances were loaded successfully" << "\n";
			}
			else {
				cerr << "Rank " << "MASTER" << " reading ion-to-edge distances failed !" << "\n";
				return false;
			}
		}

		//##MK::explain better in which cases this information is required and when not
		if ( ConfigNanochem::InputEdgeMesh.InputfilePrimitives != "" ) {
			if ( read_edge_mesh() == true ) {
				cout << "Rank " << "MASTER" << " edge mesh was loaded successfully" << "\n";
			}
			else {
				cerr << "Rank " << "MASTER" << " reading edge mesh failed !" << "\n";
				return false;
			}
		}

		if ( ConfigNanochem::AnalysisMethod == NANOCHEM_CHARACTERIZE_USER_INTERFACE_MODEL ) {

			//feature mesh, e.g. triangulated surface mesh of grain/or phase boundary patch
			if ( ConfigNanochem::InputFeatureMesh.InputfilePrimitives != "" &&
					ConfigNanochem::InputFeatureMesh.DatasetPrimitivesVertexPosition != "" &&
						ConfigNanochem::InputFeatureMesh.DatasetPrimitivesFacetIndices != "" &&
							ConfigNanochem::InputFeatureMesh.DatasetPrimitivesFacetNormal != "" ) {

				if ( read_feature_mesh() == true ) {
					cout << "Rank " << "MASTER" << " feature mesh was loaded successfully" << "\n";
				}
				else {
					cerr << "Rank " << "MASTER" << " reading feature mesh failed !" << "\n";
					return false;
				}

				//optional (signed) distance information of all ions to this feature mesh
				if ( ConfigNanochem::InputfileIonToFeatureDistances != "" &&
						ConfigNanochem::IonToFeatureDistancesDatasetName != "" ) {
					if ( read_ion2feature_distances() == true ) {
						cout << "Rank " << "MASTER" << " all ion-to-feature distances were loaded successfully" << "\n";
					}
					else {
						cerr << "Rank " << "MASTER" << " reading ion-to-feature distances failed !" << "\n";
						return false;
					}
				}
			}
		}

		//lets make a quick summary of labels, ##MK::move to mpiHdl base class
		map<unsigned char, apt_uint> nominal_ranging;
		for( auto it = ions.ionifo.begin(); it != ions.ionifo.end(); it++ ) {
			map<unsigned char, apt_uint>::iterator thisone = nominal_ranging.find( it->i_org );
			if ( thisone != nominal_ranging.end() ) {
				nominal_ranging[it->i_org]++;
			}
			else {
				nominal_ranging[it->i_org] = 1;
			}
		}
		cout << "Relevant input has the following total counts for the following ion types" << "\n";
		cout << "Total ion count " << ions.ionifo.size() << "\n";
		for( auto jt = nominal_ranging.begin(); jt != nominal_ranging.end(); jt++ ) {
			cout << (int) jt->first << "\t\t" << jt->second << "\n";
		}

		//##MK::see an example for how to make a quick check for spatial density differences in v0.3.1

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		nanochem_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;
}


bool nanochemHdl::read_ion2edge_distances()
{
	double tic = MPI_Wtime();

	if ( ions.ionpp3.size() > 0 && ions.ionifo.size() == ions.ionpp3.size() ) {

		if ( read_distance_dsetedge_from_h5( ConfigNanochem::InputfileIonToEdgeDistances,
				ConfigNanochem::IonToEdgeDistancesDatasetName ) == true ) {
			cout << "Rank " << "MASTER" << " all ion to primitive distances were loaded successfully" << "\n";
		}
		else {
			cerr << "Rank " << "MASTER" << " reading ion-to-edge distances failed !" << "\n";
			return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	nanochem_tictoc.prof_elpsdtime_and_mem( "ReadIon2EdgeDistances", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool nanochemHdl::read_edge_mesh()
{
	double tic = MPI_Wtime();

	if ( ConfigNanochem::InputEdgeMesh.InputfilePrimitives != "" &&
			ConfigNanochem::InputEdgeMesh.DatasetPrimitivesVertexPosition != "" &&
				ConfigNanochem::InputEdgeMesh.DatasetPrimitivesFacetIndices != "" ) {

		if ( read_mesh_dsetedge_from_h5( ConfigNanochem::InputEdgeMesh.InputfilePrimitives,
				ConfigNanochem::InputEdgeMesh.DatasetPrimitivesVertexPosition,
				ConfigNanochem::InputEdgeMesh.DatasetPrimitivesFacetIndices ) == true ) {
			cout << "Rank " << "MASTER" << " mesh of a representation of the edge of the dataset was loaded successfully" << "\n";
		}
		else {
			cerr << "Rank " << "MASTER" << " reading mesh representation of the edge failed !" << "\n";
			return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	nanochem_tictoc.prof_elpsdtime_and_mem( "ReadEdgeMesh", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool nanochemHdl::read_ion2feature_distances()
{
	double tic = MPI_Wtime();

	if ( ions.ionpp3.size() > 0 ) {

		ions_ion2feat = vector<apt_real>();

		cout << "Loading ion-to-feature distances..." << "\n";
		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( ConfigNanochem::InputfileIonToFeatureDistances );

		if ( h5r.nexus_read(
				ConfigNanochem::IonToFeatureDistancesDatasetName, ions_ion2feat ) != MYHDF5_SUCCESS ) { return false; }

		if ( ions_ion2feat.size() != ions.ionpp3.size() || ions_ion2feat.size() != ions.ionifo.size() ) {
			cerr << "Length of ion-to-feature distance array is different to ions.ionpp3 or was not readable !" << "\n";
			return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	nanochem_tictoc.prof_elpsdtime_and_mem( "ReadIon2FeatureDistances", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool nanochemHdl::read_feature_mesh()
{
	double tic = MPI_Wtime();

	HdfFiveSeqHdl h5r = HdfFiveSeqHdl( ConfigNanochem::InputFeatureMesh.InputfilePrimitives );

	vector<apt_real> real;
	if ( h5r.nexus_read( ConfigNanochem::InputFeatureMesh.DatasetPrimitivesVertexPosition, real ) != MYHDF5_SUCCESS ) { return false; }

	vector<apt_uint> uint;
	if ( h5r.nexus_read( ConfigNanochem::InputFeatureMesh.DatasetPrimitivesFacetIndices, uint ) != MYHDF5_SUCCESS ) { return false; }

	if ( uint.size() > 0 && (uint.size() % 3 == 0) && real.size() > 0 && (real.size() % 3 == 0) ) {
		feature.triangles = vector<tri3d>();
		size_t n_tris = uint.size() / 3;
		feature.triangles.reserve( n_tris );
		for( size_t triidx = 0; triidx < n_tris; triidx++ ) {
			size_t v1 = (size_t) uint[3*triidx+0];
			size_t v2 = (size_t) uint[3*triidx+1];
			size_t v3 = (size_t) uint[3*triidx+2];

			feature.triangles.push_back( tri3d(
					real[3*v1+0], real[3*v1+1], real[3*v1+2],
					real[3*v2+0], real[3*v2+1], real[3*v2+2],
					real[3*v3+0], real[3*v3+1], real[3*v3+2]  ) );
		}
	}
	else {
		cerr << "Geometry of the InputFeatureMesh is faulty!" << "\n";
		cerr << "Rank " << "MASTER" << " reading mesh representation of the feature(s) failed !" << "\n";
		return false;
	}
	real = vector<apt_real>();
	uint = vector<apt_uint>();

	//load vertex ##MK:: and facet normal vectors, (these should be oriented consistently, as they will be used to align automatically placed ROIs for composition analyses)
	if ( h5r.nexus_read( ConfigNanochem::InputFeatureMesh.DatasetPrimitivesFacetNormal, real ) != MYHDF5_SUCCESS ) { return false; }

	if ( real.size() / 3 != feature.triangles.size() ) {
		cerr << "Facet normal vector array is inconsistent with triangles array !" << "\n"; return false;
	}

	feature.vertex_normals = vector<p3d>();
	feature.facet_normals = vector<p3d>();
	feature.facet_normals.reserve( feature.triangles.size() / 3 );
	for( size_t triidx = 0; triidx < feature.triangles.size(); triidx++ ) {
		feature.facet_normals.push_back( p3d( real[3*triidx+0], real[3*triidx+1], real[3*triidx+2] ) );
	}
	real = vector<apt_real>();

	if ( ConfigNanochem::InputFeatureMesh.DatasetPrimitivesPatchIndices != "" ) {

		if ( h5r.nexus_read( ConfigNanochem::InputFeatureMesh.DatasetPrimitivesPatchIndices, uint ) != MYHDF5_SUCCESS ) { return false; }

		if ( uint.size() != feature.triangles.size() ) {
			cerr << "Facet patch indices is inconsistent with triangles array !" << "\n"; return false;
		}

		feature.patch_indices = uint;

		uint = vector<apt_uint>();
	}
	else {
		feature.patch_indices = vector<apt_uint>( feature.triangles.size(), UNDEFINED_PATCHID );
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	nanochem_tictoc.prof_elpsdtime_and_mem( "ReadFeatureMesh", APT_XX, APT_IS_SEQ, mm, tic, toc);
	cout << "Rank " << "MASTER" << " mesh of a representation of the feature(s) of the dataset was loaded successfully" << "\n";
	cout << "feature.triangles.size() " << feature.triangles.size() << " triangles" << "\n";
	cout << "feature.vertex_normals.size() " << feature.vertex_normals.size() << " vertex normals" << "\n";
	cout << "feature.facet_normals.size() " << feature.facet_normals.size() << " facet normals" << "\n";

	return true;
}


template<typename T>
void nanochemHdl::crop_p3d_in_simple_prims( vector<T> const & prims )
{
	#pragma omp parallel
	{
		vector<pair<apt_uint, unsigned char>> myres;
		apt_uint nions = (apt_uint) ions.ionpp3.size();
		#pragma omp for schedule(dynamic,1) nowait
		for( apt_uint i = 0; i < nions; i++ ) {
			unsigned char wclass = ANALYZE_NO; //WINDOW_ION_EXCLUDE;
			p3d p = ions.ionpp3[i];
			for( auto jt = prims.begin(); jt != prims.end(); jt++ ) {
				bool is_in = jt->is_inside( p );
				if ( is_in == true ) {
					wclass = ANALYZE_YES; //WINDOW_ION_INCLUDE_DEFAULT_CLASS;
					break; //do not test further
				}
			}
			if ( wclass != ANALYZE_NO ) { //WINDOW_ION_EXCLUDE
				myres.push_back( pair<apt_uint, unsigned char>( i, wclass) );
			}
		}
		#pragma omp critical
		{
			for( auto it = myres.begin(); it != myres.end(); it++ ) {
				window[it->first] = it->second;
			}
		}
	}
}

template void nanochemHdl::crop_p3d_in_simple_prims<roi_rotated_cuboid>( vector<roi_rotated_cuboid> const & prims );
template void nanochemHdl::crop_p3d_in_simple_prims<roi_rotated_cylinder>( vector<roi_rotated_cylinder> const & prims );
template void nanochemHdl::crop_p3d_in_simple_prims<roi_sphere>( vector<roi_sphere> const & prims );


/*
void nanochemHdl::crop_p3d_in_polyhedra()
{
}
*/

bool nanochemHdl::crop_reconstruction_to_analysis_window()
{
	double tic = MPI_Wtime();

	bool roi_has_accepted_ions = false;
	//assume first we work with the entire reconstruction, hence the ions.ionifo.mask1 is ANALYZE_YES for all
	//successively remove points from the analysis window that meet or not certain properties, like position, associated iontype, multiplicity, etc.
	if ( ConfigNanochem::WindowingMethod == ENTIRE_DATASET ) {
		apply_all_filter();
	}
	else if ( ConfigNanochem::WindowingMethod == UNION_OF_PRIMITIVES ) { //##MK::debug implementation
		apply_none_filter();

		//define which ions to include as a union of different geometric primitives
		apply_roi_ensemble_accept_filter( CG_SPHERE, ConfigNanochem::WindowingSpheres );
		apply_roi_ensemble_accept_filter( CG_CYLINDER, ConfigNanochem::WindowingCylinders );
		apply_roi_ensemble_accept_filter( CG_CUBOID, ConfigNanochem::WindowingCuboids );
	}
	else if ( ConfigNanochem::WindowingMethod == BITMASKED_POINTS ) {
		cerr << "Region-of-interest filtering mode BITMASKED_POINTS is not implemented yet!" << "\n";
		return roi_has_accepted_ions;
	}
	else {
		cerr << "Facing a yet unimplemented region-of-interest filtering mode !" << "\n";
		return roi_has_accepted_ions;
	}

	if ( ConfigNanochem::LinearSubSamplingRange.min > 0 ||
			ConfigNanochem::LinearSubSamplingRange.incr > 1 ||
				ConfigNanochem::LinearSubSamplingRange.max < ions.ionpp3.size() ) {
		//execute only when we can really restrict something
		apply_linear_subsampling_filter( ConfigNanochem::LinearSubSamplingRange );
	}
	else {
		//ignore this filter as it will have no effect
	}

	if ( ConfigNanochem::IontypeFilter.is_whitelist == true ) {
		apply_iontype_remove_filter( ConfigNanochem::IontypeFilter.candidates, false );
	}
	else if ( ConfigNanochem::IontypeFilter.is_blacklist == true ) {
		apply_iontype_remove_filter( ConfigNanochem::IontypeFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	if ( ConfigNanochem::HitMultiplicityFilter.is_whitelist == true ) {
		apply_multiplicity_remove_filter( ConfigNanochem::HitMultiplicityFilter.candidates, false );
	}
	else if ( ConfigNanochem::HitMultiplicityFilter.is_blacklist == true ) {
		apply_multiplicity_remove_filter( ConfigNanochem::HitMultiplicityFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	check_filter();

	//define at least two window classes, ions are assigned a class,
	//window class 0x00 ions are discarded, ions of all other classes are considered
	window = vector<unsigned char>( ions.ionpp3.size(), ANALYZE_NO ); //WINDOW_ION_EXCLUDE );
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			window[i] = ANALYZE_YES; //WINDOW_ION_INCLUDE_DEFAULT_CLASS;
			roi_has_accepted_ions = true;
		}
		//else, nothing to do, window[i] already WINDOW_ION_EXCLUDE
	}

	double toc = MPI_Wtime();
	memsnapshot mm = nanochem_tictoc.get_memoryconsumption();
	nanochem_tictoc.prof_elpsdtime_and_mem( "CropReconstructionToAnalysisWindow", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return roi_has_accepted_ions;
}


void nanochemHdl::execute_isosurface_workpackage()
{
	double tic = MPI_Wtime();
	double ctic = MYZERO;
	double ctoc = MYZERO;

	apt_uint taskid = 0 + 1;
	apt_uint gridID = 0 + 1;
	apt_uint dlzID = 0 + 1; //there are typically more delocalizations than grid resolutions tested
	apt_uint isrfID = 0 + 1; //and more iso-surfaces tasks for a given gridresolution and delocalization approach
	//because we typically want to inspect for a given delocalized grid the sensitivity of the iso-surface

	for( auto isrf_tsklstit = ConfigNanochem::IsoSurfacingTasks.begin(); isrf_tsklstit != ConfigNanochem::IsoSurfacingTasks.end(); isrf_tsklstit++ ) {
		//MK::it is possible to define multiple lists of isosurface tasks for instance when we want to scan for a number of ions working on different iontypes
		//and for each of them a combination of iso-surface values, for instance an exhaustive survey of all iso-surfaces for an ion
		//that is here defined as: compute the iso-surface for a given combination of ions with 1, 2, 3, ..., N ions in the 3D ion histogram/discretization/voxelization
		//MK::in addition each such task may have multiple gridresolutions, each creating a new set of sub-tasks
		//MK::in addition each such sub-task may have multiple isovalues, each creating yet another layer of subsubtasks
		//each subsubtask eventually has an own isosurface (with triangle ensemble)
		//we do not cache completed tasks but write them to file and proceed with the next task
		//MK::therefore, we distribute at the level of the isovalues, so processes work sequentially through tasks, but distribute processing
		//of isovalues, internally, each isosurface computation is multithreaded using OpenMP

		for( auto gridresu_it = isrf_tsklstit->gridresolutions.begin(); gridresu_it != isrf_tsklstit->gridresolutions.end(); gridresu_it++ ) {

			cout << "Rank " << get_myrank() << " processes gridding task " << gridID << "\n";

			for( auto sigma_it = isrf_tsklstit->deloc_sigma.begin(); sigma_it != isrf_tsklstit->deloc_sigma.end(); sigma_it++ ) {
				//##MK::create the delocalization at this level so that the grid has to be computed only once for all iso-surface tasks which follow

				ctic = MPI_Wtime();

				isosurfaceHdl* vxlizer = new isosurfaceHdl;
				vxlizer->info.taskid = taskid;
				vxlizer->info.gridid = gridID;
				vxlizer->info.dlzid = dlzID;
				vxlizer->info.isrfid = UMX; //means has no iso-surface yet
				vxlizer->info.resu = *gridresu_it;
				vxlizer->info.delocalization = isrf_tsklstit->delocalization;
				vxlizer->info.normalization = isrf_tsklstit->normalization;
				vxlizer->info.edge_handling = isrf_tsklstit->edge_handling;
				vxlizer->info.edge_threshold = isrf_tsklstit->edge_threshold;
				vxlizer->info.deloc_method = isrf_tsklstit->deloc_method;
				vxlizer->info.nuclide_hash_whitelist = isrf_tsklstit->nuclide_hash_whitelist;
				vxlizer->info.charge_state_whitelist = isrf_tsklstit->charge_state_whitelist;
				vxlizer->info.set_itypes( isrf_tsklstit->itypes, isrf_tsklstit->itypes_whitelist, rng );
				vxlizer->info.elemtypes_whitelist = isrf_tsklstit->elemtypes_whitelist;
				vxlizer->info.hasIsoSrfScalarField = isrf_tsklstit->IOStoreIsoSrfScalarFields;

				cout << "Rank " << get_myrank() << " processes delocalization task ..." << "\n";
				cout << vxlizer->info << "\n";

				bool delocalization_status = true;
				switch(vxlizer->info.delocalization)
				{
					/*
					case DELOCALIZATION_NONE:
					{
						delocalization_status = vxlizer->voxelize_delocalize_naive( window, ions.ionpp3, ions.ionifo );
						break;
					}
					*/
					case DELOCALIZATION_GAUSSIAN_ION_DECOMPOSE:
					{
						vxlizer->info.deloc_halfsize_a = isrf_tsklstit->deloc_halfsize_a;
						vxlizer->info.deloc_sigma = *sigma_it;

						//delocalization_status = vxlizer->voxelize_delocalize_gaussian_fast_ion_multithreaded(
						//		window, ions.ionpp3, ions.ionifo, rng );
						delocalization_status = vxlizer->voxelize_delocalize_gaussian_fast_per_iontype_multithreaded(
								window, ions.ionpp3, ions.ionifo, rng );
						if ( delocalization_status == true ) {
							delocalization_status = vxlizer->voxelize_delocalize_compose_from_iontype( rng );
						}
						break;
					}
					/*
					case DELOCALIZATION_USE_A_SINGLE_PRECOMPUTED:
					{
						vxlizer->info.deloc_precomputed_fnm = isrf_tsklstit->deloc_precomputed_fnm;
						vxlizer->info.deloc_precomputed_grpnm = isrf_tsklstit->deloc_precomputed_grpnm;

						cout << "BE CAREFUL :: DELOCALIZATION_USE_A_SINGLE_PRECOMPUTED IS AN EXPERIMENTAL FEATURE !" << "\n";
						cerr << "BE CAREFUL :: DELOCALIZATION_USE_A_SINGLE_PRECOMPUTED IS AN EXPERIMENTAL FEATURE !" << "\n";

						delocalization_status = vxlizer->voxelize_delocalize_load_existent();
						break;
					}
					*/
					default:
					{
						delocalization_status = false;
						break;
					}
				}

				if ( delocalization_status == true ) {
					cout << "Rank " << get_myrank() << " discretized the volume for " << vxlizer->info.resu << " successfully" << "\n";

					ctoc = MPI_Wtime();
					vxlizer->info.dt_discretization = (ctoc-ctic);
					ctic = MPI_Wtime();

					if ( vxlizer->info.hasIsoSrfScalarField == true ) {

						if ( vxlizer->write_delocalization_grid() == true ) {
							cout << "Writing delocalization grid information was successful" << "\n";
						}
						else {
							cerr << "Writing delocalization grid information failed" << "\n";
						}

						if ( vxlizer->write_delocalization_window( ions.ionifo ) == true ) {
							cout << "Writing details about the vxlizer task executed was successful" << "\n";
						}
						else {
							cerr << "Writing details about the vxlizer task executed failed !" << "\n";
						}

						if ( vxlizer->compute_scalarfield_gradient3d() == true ) {
							cout << "Computing gradients of the scalar field was successful" << "\n";
						}
						else {
							cerr << "Computing gradients of the scalar field failed !" << "\n";
						}

						if ( vxlizer->write_scalar_field_and_gradient_h5() == true ) {
							cout << "Writing scalar field was successful" << "\n";
						}
						else {
							cerr << "Writing scalar field failed !" << "\n";
						}
					}

					ctoc = MPI_Wtime();
					vxlizer->info.dt_h5io += (ctoc-ctic);
					ctic = MPI_Wtime();
				}
				else {
					cerr << "Rank " << get_myrank() << " discretizing the volume for " << vxlizer->info.resu << " failed !" << "\n";
					delete vxlizer; vxlizer = NULL; ctoc = MPI_Wtime();
					continue;
				}

				//construct linear time volume querier only once, instead for each iso-surface run

				//apt_real edge_distance_threshold = sqrt(3.) * (apt_real) vxlizer->info.deloc_halfsize_a * (*gridresu_it);
				apt_real edge_distance_threshold = isrf_tsklstit->edge_threshold;
				cout << "Using edge_distance_threshold " << edge_distance_threshold << " nm" << "\n";

				cout << "About to build LinearTimeVolQuerier" << "\n";
				cout << vxlizer->gridinfo << "\n";
				ion_bvh = LinearTimeVolQuerier<p3dmidx>( vxlizer->gridinfo.aabb, ConfigNanochem::LinearTimeVolVoxelEdgeLength );

				for( size_t ionidx = 0; ionidx < ions.ionpp3.size(); ionidx++ ) {

					if ( window[ionidx] != ANALYZE_NO ) {
						//##MK::add support to take only from selected regions of interest by scanning the mask
						bool interior_point = ( ions.ionifo[ionidx].dist >= edge_distance_threshold ) ? true : false;

						ion_bvh.add( p3dmidx(ions.ionpp3[ionidx].x, ions.ionpp3[ionidx].y, ions.ionpp3[ionidx].z,
								(apt_uint) ionidx, ions.ionifo[ionidx].i_org, interior_point) );
					}
				}

				ctoc = MPI_Wtime();
				memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
				vxlizer->isosurf_tictoc.prof_elpsdtime_and_mem( "LinearTimeVolQuerierDelocTask" + to_string(vxlizer->info.dlzid), APT_XX, APT_IS_SEQ, mm, ctic, ctoc);
				cout << "Rank " << get_myrank() << " created LinearTimeVolQuerier took " << (ctoc-ctic) << " seconds" << "\n";
				ctic = MPI_Wtime();

				//process different iso-surfaces for the same precomputed scalar field

				for ( auto isoval_it = isrf_tsklstit->isovalues.begin(); isoval_it != isrf_tsklstit->isovalues.end(); isoval_it++ ) {

					//##MK::implement distributing of task across processes

					if ( get_myrank() == MASTER ) {
						ctic = MPI_Wtime();

						isosurfaceHdl* isrf = new isosurfaceHdl;
						isrf->info.taskid = taskid;
						isrf->info.gridid = gridID;
						isrf->info.dlzid = dlzID;
						isrf->info.isrfid = isrfID;
						isrf->info.resu = *gridresu_it;
						isrf->info.delocalization = vxlizer->info.delocalization;
						isrf->info.deloc_halfsize_a = vxlizer->info.deloc_halfsize_a;
						isrf->info.deloc_sigma = vxlizer->info.deloc_sigma;
						isrf->info.normalization = vxlizer->info.normalization;
						isrf->info.edge_handling = vxlizer->info.edge_handling;
						isrf->info.edge_threshold = vxlizer->info.edge_threshold;
						isrf->info.deloc_method = vxlizer->info.deloc_method;
						isrf->info.nuclide_hash_whitelist = vxlizer->info.nuclide_hash_whitelist;
						isrf->info.charge_state_whitelist = vxlizer->info.charge_state_whitelist;
						isrf->info.set_itypes( isrf_tsklstit->itypes, isrf_tsklstit->itypes_whitelist, rng );
						isrf->info.elemtypes_whitelist = vxlizer->info.elemtypes_whitelist;
						isrf->info.isoval = *isoval_it;

						isrf->info.hasIsoSrfScalarField = isrf_tsklstit->IOStoreIsoSrfScalarFields;
						isrf->info.hasIsoSrfTriSoup = isrf_tsklstit->IOStoreIsoSrfTriSoup;
						isrf->info.hasObjects = isrf_tsklstit->IOStoreIsoSrfObjects;
						isrf->info.hasObjectsGeometry = isrf_tsklstit->IOStoreIsoSrfClosedGeometry;
						isrf->info.hasObjectsProps = isrf_tsklstit->IOStoreIsoSrfClosedProps;
						isrf->info.hasObjectsOBB = isrf_tsklstit->IOStoreIsoSrfClosedOBB;
						isrf->info.hasObjectsIons = isrf_tsklstit->IOStoreIsoSrfClosedIons;
						isrf->info.hasProxies = isrf_tsklstit->IOStoreIsoSrfProxy;
						isrf->info.hasProxiesGeometry = isrf_tsklstit->IOStoreIsoSrfProxyGeometry;
						isrf->info.hasProxiesProps = isrf_tsklstit->IOStoreIsoSrfProxyProps;
						isrf->info.hasProxiesOBB = isrf_tsklstit->IOStoreIsoSrfProxyOBB;
						isrf->info.hasProxiesIons = isrf_tsklstit->IOStoreIsoSrfProxyIons;

						cout << "Rank " << get_myrank() << " processes iso-surfacing task ..." << "\n";
						cout << isrf->info << "\n";

						cout << "Rank " << get_myrank() << " vxlizer handing over a copy of the pre-computed scalar field " << isrf->scalarfield.size() << "\n";

						//use the already computed grid and scalar field
						isrf->gridinfo = vxlizer->gridinfo;
						isrf->scalarfield = vxlizer->scalarfield;
						isrf->scalarfield_grad3d = vxlizer->scalarfield_grad3d;
						isrf->scalarfield_edge = vxlizer->scalarfield_edge;

						cout << "Rank " << get_myrank() << " isosurfacer received the field " << isrf->scalarfield.size() << "\n";

						//the delocalization task handler has exported the scalar field because this field is used for executing eventually multiple isosurfacing tasks

						if ( isrf->build_iso_surface() == true ) {
							cout << "Rank " << get_myrank() << " building the iso-surface for isovalue " << isrf->info.isoval << " was successful" << "\n";

							ctoc = MPI_Wtime();
							isrf->info.dt_marching_cubes = (ctoc-ctic);
							ctic = MPI_Wtime();

							if ( isrf->tscl.fcts.size() < 1 ) {
								cout << "Iso-surface for isovalue " << isrf->info.isoval << " has no triangles will continue with the next one!" << "\n";
								nanochem_tictoc.copy( isrf->isosurf_tictoc.evn );
								isrf->isosurf_tictoc = profiler();
								delete isrf; isrf = NULL;
								isrfID++;
								continue;
							}

							//until now the triangles of the isosurface have still the vertex-averaged normals from MC

							if ( isrf->compute_directed_triangle_normals( ConfigNanochem::MinScalarFieldGradientMagnitude ) == true ) {
								cout << "Rank " << get_myrank() << " computing guided directed normal vectors for iso-surface triangles for isovalue "
										<< isrf->info.isoval << " was successful !" << "\n";
							}
							else {
								cerr << "Rank " << get_myrank() << " computing guided directed normal vectors for iso-surface triangles for isovalue "
										<< isrf->info.isoval << " failed !" << "\n";
							}

							if ( isrf->approximate_unclear_triangle_normals( ConfigNanochem::TriSoupClusteringMaxDistance ) == true ) {
								cout << "Rank " << get_myrank() << " approximating guided directed normal vectors for iso-surface triangles for isovalue "
										<< isrf->info.isoval << " was successful !" << "\n";
							}
							else {
								cerr << "Rank " << get_myrank() << " approximating guided directed normal vectors for iso-surface triangles for isovalue "
										<< isrf->info.isoval << " failed !" << "\n";
							}

							isrf->check_closure_iso_surface();

							if ( isrf->info.hasIsoSrfTriSoup == true ) {

								if ( isrf->write_isrf_complex_h5() == true ) {
									cout << "Rank " << get_myrank() << " writing triangle soup for isovalue " << isrf->info.isoval << " was successful" << "\n";
								}
								else {
									cerr << "Rank " << get_myrank() << " writing triangle soup for isovalue " << isrf->info.isoval << " failed !" << "\n";
								}
							}

							ctoc = MPI_Wtime();
							isrf->info.dt_h5io += (ctoc-ctic);
							ctic = MPI_Wtime();
						}
						else {
							cerr << "Rank " << get_myrank() << " building the iso-surface for isovalue " << isrf->info.isoval << " failed !" << "\n";
							delete isrf; isrf = NULL; continue;
						}

						if ( isrf->info.hasObjects == true ) {

							apt_real tri_eps = ConfigNanochem::TriSoupClusteringMaxDistance;

							if ( isrf->find_connected_regions( tri_eps ) == true ) {

								cout << "Rank " << get_myrank() << " identifying connected regions with tri_eps " << tri_eps << " was successful" << "\n";

								ctoc = MPI_Wtime();
								isrf->info.dt_objects_find = (ctoc-ctic);
								ctic = MPI_Wtime();

								if ( isrf->write_triangle_soup_clusterid_h5() == true ) {
									cout << "Rank " << get_myrank() << " writing object IDs for tri_eps " << tri_eps << " for isovalue " << isrf->info.isoval << " was successful" << "\n";
								}
								else {
									cerr << "Rank " << get_myrank() << " writing cluster IDs for tri_eps " << tri_eps << " for isovalue " << isrf->info.isoval << " failed !" << "\n";
								}

								ctoc = MPI_Wtime();
								isrf->info.dt_h5io += (ctoc-ctic);
								ctic = MPI_Wtime();
							}
							else {
								cerr << "Rank " << get_myrank() << " identifying connected regions with tri_eps " << tri_eps << " failed !" << "\n";
								delete isrf; isrf = NULL; continue;
							}
							//cout << "Rank " << get_myrank() << " identifying connected regions yields " << isrf->tscl.nobjects << " objects separated analytically by at least " << tri_eps << " nm for isrfID " << isrfID << " took " << (ctoc-ctic) << " seconds" << "\n";

							if ( isrf->find_closed_polyhedra_and_proxies_for_objects_with_holes(
									ions.ionpp3, ions.ionifo, rng.iontypes.size() ) == true ) {

								cout << "Rank " << get_myrank() << " identifying closed polyhedra for isovalue " << isrf->info.isoval << " was successful" << "\n";

								ctoc = MPI_Wtime();
								isrf->info.dt_closed_and_proxies_find = (ctoc-ctic);
								ctic = MPI_Wtime();

								//continue with processing the closed polyhedra
								if ( isrf->info.hasObjectsProps == true ) {

									if ( isrf->write_objects_properties_h5() == true ) {
										cout << "Rank " << get_myrank() << " writing object properties for isovalue " << isrf->info.isoval << " was successful" << "\n";
									}
									else {
										cerr << "Rank " << get_myrank() << " writing object properties for isovalue " << isrf->info.isoval << " failed !" << "\n";
									}

									ctoc = MPI_Wtime();
									isrf->info.dt_h5io += (ctoc-ctic);
									ctic = MPI_Wtime();
								}

								if ( isrf->info.hasObjectsIons == true ) {

									if ( isrf->find_ions_in_closed_polyhedra_npoly_times_log_nions_no_tetrahedralization_multithreading(
											window,
											ions.ionpp3, ions.ionifo, rng, ion_bvh, edge.triangles,
											isrf_tsklstit->DetectObjectEdgeTruncation, edge_distance_threshold ) == true ) {

										cout << "Rank " << get_myrank() << " identified which ions are in which closed polyhedron" << "\n";

										ctoc = MPI_Wtime();
										isrf->info.dt_closed_quantify = (ctoc-ctic);
										ctic = MPI_Wtime();

										if ( isrf->write_objects_ions_h5( ions.ionpp3, rng ) == true ) {
											cout << "Rank " << get_myrank() << " writing ions inside polyhedra for isovalue " << isrf->info.isoval << " was successful" << "\n";
										}
										else {
											cerr << "Rank " << get_myrank() << " writing ions inside polyhedra for isovalue " << isrf->info.isoval << " failed !" << "\n";
										}

										ctoc = MPI_Wtime();
										isrf->info.dt_h5io += (ctoc-ctic);
										ctic = MPI_Wtime();
									}
								}

								if ( isrf->info.hasObjectsGeometry == true ) {

									if ( isrf->write_objects_geometry_h5() == true ) {
										cout << "Rank " << get_myrank() << " writing object geometries for isovalue " << isrf->info.isoval << " was successful" << "\n";
									}
									else {
										cerr << "Rank " << get_myrank() << " writing object geometries for isovalue " << isrf->info.isoval << " failed !" << "\n";
									}

									ctoc = MPI_Wtime();
									isrf->info.dt_h5io += (ctoc-ctic);
									ctic = MPI_Wtime();
								}

								ctoc = MPI_Wtime();
								isrf->info.dt_h5io += (ctoc-ctic);
								ctic = MPI_Wtime();

								/*
								if ( isrf->info.hasProxiesProps == true ) {
									//##MK::add
								}
								*/

								if ( isrf->info.hasProxiesIons == true ) {

									if ( isrf->find_ions_in_proxies_npoly_times_log_nions_no_tetrahedralization_multithreading(
											window, ions.ionpp3, ions.ionifo, rng, ion_bvh, edge.triangles,
											isrf_tsklstit->DetectProxyEdgeTruncation, edge_distance_threshold ) == true ) {
											//, edge.triangles, isrf_tsklstit->DetectObjectEdgeTruncation, edge_distance_threshold ) == true ) {

										cout << "Rank " << get_myrank() << " identified which ions are in which proxy" << "\n";

										ctoc = MPI_Wtime();
										isrf->info.dt_proxies_quantify = (ctoc-ctic);
										ctic = MPI_Wtime();

										if ( isrf->write_proxies_ions_h5( ions.ionpp3, rng ) == true ) {
											cout << "Rank " << get_myrank() << " writing ions inside proxies for isovalue " << isrf->info.isoval << " was successful" << "\n";
										}
										else {
											cerr << "Rank " << get_myrank() << " writing ions inside proxies for isovalue " << isrf->info.isoval << " failed !" << "\n";
										}

										ctoc = MPI_Wtime();
										isrf->info.dt_h5io += (ctoc-ctic);
										ctic = MPI_Wtime();
									}
								}

								if ( isrf->info.hasProxiesGeometry == true ) {

									if ( isrf->write_proxies_geometry_h5() == true ) {
										cout << "Rank " << get_myrank() << " writing proxies geometry for isovalue " << isrf->info.isoval << " was successful" << "\n";
									}
									else {
										cerr << "Rank " << get_myrank() << " writing proxies geometry for isovalue " << isrf->info.isoval << " failed !" << "\n";
									}

									ctoc = MPI_Wtime();
									isrf->info.dt_h5io += (ctoc-ctic);
									ctic = MPI_Wtime();
								}

								//if ( isrf->info.hasObjectsProps == true ) {
								//	###write the proxies tictoc
								//}

							} //done characterizing objects
							else {
								cout << "Rank " << get_myrank() << " identifying proxies for isovalue " << isrf->info.isoval << " failed !" << "\n";
							}

						} //all objects identified and those with a closed contour representing polyhedra were further quantified

						//take over profiling results from the current isosurfacer
						nanochem_tictoc.copy( isrf->isosurf_tictoc.evn );
						isrf->isosurf_tictoc = profiler();

						delete isrf; isrf = NULL;
					} //done with this iso-surface-based analysis

					isrfID++;
				} //next concentration

				//take over profiling results from the vxlizer
				nanochem_tictoc.copy( vxlizer->isosurf_tictoc.evn );
				vxlizer->isosurf_tictoc = profiler();

				delete vxlizer; vxlizer = NULL;
				dlzID++;
			} //next delocalization

			gridID++;
		} //next gridresolution

		taskid++;
	} //next iso-surfacing array task

	double toc = MPI_Wtime();
	memsnapshot mm = nanochem_tictoc.get_memoryconsumption();
	nanochem_tictoc.prof_elpsdtime_and_mem( "IsosurfaceWorkpackage", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void nanochemHdl::execute_interfacial_excess_workpackage()
{
	double tic = MPI_Wtime();

	interfaceHdl iex = interfaceHdl();
	iex.taskid = 0;

	//pre-processing: initial interface model
	if ( iex.step_1a_initial_interface_extract_roi_and_decorating_ions( ions.ionpp3, ions.ionifo, rng ) == false ) {
		cerr << "Step 1a failed !"; return;
	}
	if ( iex.step_1b_initial_interface_extract_report_roi( ions.ionpp3, ions.ionifo ) == false ) {
		cerr << "Step 1b failed !"; return;
	}
	if ( iex.step_2_initial_interface_create_pca_based() == false ) {
		cerr << "Step 2 failed !"; return;
	}
	if ( iex.step_3_initial_interface_clip_by_domain( ions.ionpp3 ) == false ) {
		cerr << "Step 3 failed !"; return;
	}
	if ( iex.step_4_initial_interface_triangulate() == false ) {
		cerr << "Step 4 failed !"; return;
	}

	//interface refinement
	if ( iex.step_5_initial_interface_refine_iteratively() == false ) {
		cerr << "Step 5 failed !"; return;
	}

	//post-processing: final interface model
	if ( iex.step_6_final_interface_clip_by_edge_model() == false ) {
		cerr << "Step 6 failed !"; return;
	}
	if ( iex.step_7_final_interface_report() == false ) {
		cerr << "Step 7 failed !"; return;
	}

	iex.kauri = kdTree<p3d>();

	double toc = MPI_Wtime();
	//memsnapshot mm = nanochem_tictoc.get_memoryconsumption();
	//nanochem_tictoc.prof_elpsdtime_and_mem( "IsosurfaceWorkpackage", APT_XX, APT_IS_PAR, mm, tic, toc);
	cout << "Single interface modeling took " << (toc-tic) << " seconds" << "\n";
}


void nanochemHdl::execute_characterize_composition_workpackage()
{
	//##MK::one-dimensional profiles with this function only
	//##MK::assuming currently that results fit into memory, so I/O can be done afterwards
	//for three-dimensional profiles such approach will substantially earlier fail, as then there are e.g. Kuehbach40962.size() many
	//differently rotated ROIs placed per facet, in which case the data cannot be cached if one is really after hunting CDF
	//that the work needs to be split per ROI across threads and have intermediate I/O

	double tic = MPI_Wtime();

	//construct linear time volume querier only once, to avoid having to check each ion of the dataset for inclusion in the ROI primitives
	aabb3d domain = aabb3d();
	for( size_t ionidx = 0; ionidx < ions.ionpp3.size(); ionidx++ ) {
		if ( window[ionidx] != ANALYZE_NO ) { //WINDOW_ION_EXCLUDE
			domain.possibly_enlarge_me( ions.ionpp3[ionidx] );
		}
	}
	domain.add_epsilon_guard();
	domain.scale();

	ion_bvh = LinearTimeVolQuerier<p3dmidx>( domain, 1.0 );
	for( size_t ionidx = 0; ionidx < ions.ionpp3.size(); ionidx++ ) {
		if ( window[ionidx] != ANALYZE_NO ) { //WINDOW_ION_EXCLUDE ) {
			//##MK::add support to take only from selected regions of interest by scanning the mask
			bool interior_point = true; // ( ions.ionifo[ionidx].dist >= edge_distance_threshold ) ? true : false;
			ion_bvh.add( p3dmidx(ions.ionpp3[ionidx].x, ions.ionpp3[ionidx].y, ions.ionpp3[ionidx].z,
					(apt_uint) ionidx, ions.ionifo[ionidx].i_org, interior_point) );
		}
	}

	//process the individual composition analyses

	for( auto tskit = ConfigNanochem::CompositionAnalysisTasks.begin(); tskit != ConfigNanochem::CompositionAnalysisTasks.end(); tskit++ ) {

		double ctic = MPI_Wtime();

		cout << "Placing ROIs automatically at feature mesh triangle facets when there is sufficient normal information" << "\n";
		cout << "Detecting eventual truncation of a ROI by the edge of the dataset via intersections between possible triangles of the edge to the ROI primitive" << "\n";

		compositionHdl cmp = compositionHdl();

		if ( tskit->roiifo.user_defined_rois == false ) {
			cmp.multithreaded_composition_analysis_with_autorois(
				*tskit,
				rng, ion_bvh,
				edge.triangles,
				feature.triangles,
				feature.facet_normals,
				feature.patch_indices );
		}
		else {
			cmp.multithreaded_composition_analysis_with_autorois(
				*tskit,
				rng, ion_bvh,
				edge.triangles);
		}

		size_t nrois = cmp.write_automated_rois_for_composition_h5(
				*tskit );

		//report convenience xdmf file to support visualizing all rois
		if ( nrois > 0 ) {
			nanochem_xdmf xdmfhdl;

			string xdmf_fn = strip_path_prefix(ConfigShared::OutputfileName) + ".CompositionAnalysis.EntryId."
					+ to_string(tskit->taskid) + ".AutoRois.xdmf";

			xdmfhdl.create_autorois_cylinder_file( xdmf_fn, ConfigShared::SimID,
					tskit->taskid, nrois, ConfigNanochem::NGonalPrismNumberOfFacets,
					strip_path_prefix(ConfigShared::OutputfileName) );
		}

		double ctoc = MPI_Wtime();
		memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
		nanochem_tictoc.prof_elpsdtime_and_mem( "AutomatedRoisCompositionAnalysisTask" + to_string(tskit->taskid), APT_XX, APT_IS_PAR, mm, ctic, ctoc);

	} //next composition analysis task

	double toc = MPI_Wtime();
	cout << "Automated placing and characterizing of ROIs for composition analysis tasks took " << (toc-tic) << " seconds" << "\n";
}
