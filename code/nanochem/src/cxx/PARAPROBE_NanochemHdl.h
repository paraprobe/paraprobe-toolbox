/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_HDL_H__
#define __PARAPROBE_NANOCHEM_HDL_H__

#include "PARAPROBE_NanochemXDMF.h"


class nanochemHdl : public mpiHdl
{
	//process-level class which implements the worker instance
public:
	nanochemHdl();
	~nanochemHdl();
	
	bool read_relevant_input();
	bool read_ion2edge_distances();
	bool read_edge_mesh();

	bool read_ion2feature_distances();
	bool read_feature_mesh();

	template<typename T>
	void crop_p3d_in_simple_prims( vector<T> const & prims );
	/*
	void crop_p3d_in_polyhedra();
	*/
	bool crop_reconstruction_to_analysis_window();

	void execute_isosurface_workpackage();
	void execute_interfacial_excess_workpackage();
	void execute_characterize_composition_workpackage();

	vector<unsigned char> window;					//the portion of the dataset that we want to analyze
	triangleSoup feature;
	vector<apt_real> ions_ion2feat;
	LinearTimeVolQuerier<p3dmidx> ion_bvh;
	profiler nanochem_tictoc;
};


#endif
