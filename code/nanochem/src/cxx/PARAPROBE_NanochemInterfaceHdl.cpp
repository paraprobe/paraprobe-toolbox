/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemInterfaceHdl.h"


interfaceHdl::interfaceHdl()
{
	candidates = vector<p3dm1>();
	kauri = kdTree<p3d>();
	tmp_out = vector<p3d>();
	plane = IE_Plane();
	domain_aabb = aabb3d();
	polylines = Polylines();
	patch = vector<CGAL::Triple<int, int, int>>();
	ply = CGAL_Polygon();
	parms = vector<mesh_refine_step_info>();
	mesh_initial = Mesh();
	mesh_curr = Mesh();
	mesh_next = Mesh();
	vnrm_curr = vector<p3d>();
	fnrm_curr = vector<p3d>();
	taskid = UMX;
}


interfaceHdl::~interfaceHdl()
{
}


bool interfaceHdl::step_1a_initial_interface_extract_roi_and_decorating_ions(
		vector<p3d> const & pp3, vector<p3dinfo> & ionifo, rangeTable & rngifo )
{
	//by default no ion is selected for analysis, the all filter explicitly says that all ions are analyzed

	//now inspect for each ion if it matches to ions of the target types (which are assumed decorating the defect)
	//one of the ityps, in this case add that ion mult many times
	vector<pair<unsigned char, unsigned char>> ityps_mult_whitelist =
			rngifo.generalized_ion_decomposition_using_search_pattern(
					ION_DECOMPOSITION_ATOM, ConfigNanochem::InterfaceModellingTasks.at(0).isotope_matrix );

	cout << "The multiplicity of the iontype with respect to the interface modelling task is..." << "\n";
	cout << "ityps_mult_whitelist" << "\n";
	for( auto it = ityps_mult_whitelist.begin(); it != ityps_mult_whitelist.end(); it++ ) {
		cout << "ityp " << (int) it->first << "\t\t multiplicity " << (int) it->second << "\n";
	}

	/*
	InterfaceModellingTasks.back().itypes_multiplicity = generalized_isotope_decomposition( isotope_whitelist );
	InterfaceModellingTasks.back().itypes_multiplicity = generalized_isotope_decomposition( vector<unsigned short> pattern, rng  );

	for ( size_t idx = 0; idx < isotope_whitelist.size(); idx += MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION ) {
		pair<unsigned char, unsigned char> protons_neutrons = isotope_unhash( isotope_whitelist.at(idx) );
		InterfaceModellingTasks.back().elemtypes_whitelist.push_back( protons_neutrons.first );
		cout << "Adding an element with " << (int) protons_neutrons.first << " to the elemtypes_whitelist" << "\n";
		//##MK::identify for now which iontypes represent
	}
	*/

	//the DCOM movement will have a bias at the edge, as potential ions within the DCOM radius
	//could have contributed to the voting where the vertex would have been moved
	//the same situation conceptual happens when we analyze only a region of the dataset,
	//this region eventually cuts the effective DCOM region further and would thus bias the movement
	//of the vertices of the mesh which are at the boundary of the analyzed region
	//(which can be much smaller than the entire dataset !)
	//to account strictly for this bias one would need to restrict the DCOM operations within a
	//region of DCOM radius away from the boundary of the dataset and ROI which often significantly
	//reduces the statistical significance of the analysis
	//here we at least avoid the bias by cutting the ROI by taking into account all ions in the kdtree

	//##MK::restrict analysis to not more than U32MX-1 ions

	vector<unsigned char> u08_all; //report the multiplicity of the ions in for an interface model
	vector<unsigned char> u08_cnd; //report the multiplicity of those which become candidates for an interface model

	kauri = kdTree<p3d>();
	vector<p3d> tmp_in = vector<p3d>();

	apt_uint n_ions = (apt_uint) pp3.size();
	candidates = vector<p3dm1>();
	u08_all = vector<unsigned char>( n_ions, 0x00 );
	u08_cnd = vector<unsigned char>( n_ions, 0x00 );

	for( apt_uint i = 0; i < n_ions; i++ ) {
		unsigned char ityp = ionifo[i].i_org;
		for( auto jt = ityps_mult_whitelist.begin(); jt != ityps_mult_whitelist.end(); jt++ ) {
			//usually number of decorating species is much lower than total number of ions
			if ( ityp != jt->first ) {
				continue;
			}
			else { //ityp == jt->first
				unsigned char multiplicity = jt->second;
				u08_all[i] = multiplicity;
				if ( multiplicity != 0x00 ) {
					p3dm1 cnd = p3dm1( pp3[i].x, pp3[i].y, pp3[i].z, i);
					for( int k = 0; k < (int) multiplicity; k++ ) {
						tmp_in.push_back( p3d(pp3[i].x, pp3[i].y, pp3[i].z) );
						//account for the ion in the kdtree to remove ROI-associated bias ...
						if ( ionifo[i].mask1 == ANALYZE_YES ) { //... but do account for a candidate only in the ROI
							candidates.push_back( cnd );
						}
					}
					if ( ionifo[i].mask1 == ANALYZE_YES ) {
						u08_cnd[i] = multiplicity;
					}
				}
				break;
			}
		}
	}

	//report multiplicity
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	//apt_uint proc_id = taskid + 1;
	grpnm = "/entry" + to_string(entry_id) + "/interface_meshing";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	//TODO::###add window

	dsnm = grpnm + "/ion_multiplicity";
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08_all.size()}, {u08_all.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u08_all,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08_all = vector<unsigned char>();

	dsnm = grpnm + "/decorator_multiplicity";
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08_cnd.size()}, {u08_cnd.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u08_cnd,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08_cnd = vector<unsigned char>();

	//##MK::for specific examples check v0.3.1 source code

	cout << "Identified " << tmp_in.size() << " ions (accounted for multiplicity) matching a candidate in the whitelist" << "\n";
	cout << "Identified " << candidates.size() << " ions (accounted for multiplicity) which are used to identify the location of the interface in the ROI" << "\n";

	//build the kdtree
	vector<apt_uint> tmp_prm = vector<apt_uint>();
	tmp_out = vector<p3d>();

	kauri.kd_build( tmp_in, tmp_prm, ConfigNanochem::InterfaceModellingKDTreeMaxLeafSize );
	kauri.kd_pack( tmp_in, tmp_prm );
	if ( kauri.kd_verify() == true ) {
		cout << "kdTree built and verification success" << "\n";
		cout << "kauri.nodes.size() " << kauri.nodes.size() << "\n";
		tmp_in = vector<p3d>();
		tmp_prm = vector<apt_uint>();
	}
	else {
		cerr << "kdTree built and verification failed !" << "\n"; return false;
	}

	return true;
}


bool interfaceHdl::step_1b_initial_interface_extract_report_roi( vector<p3d> const & pp3, vector<p3dinfo> & ionifo )
{
	//double tic = MPI_Wtime();

	/*
	vector<unsigned char> u08 = vector<unsigned char>( ionifo.size(), ANALYZE_NO );
	for( size_t i = 0; i < ionifo.size(); i++ ) {
		if ( ionifo[i].mask1 == ANALYZE_YES ) {
			u08[i] = ANALYZE_YES;
		}
	}
	*/

	return true;
}


bool interfaceHdl::step_2_initial_interface_create_pca_based()
{

	vector<IE_Point> p;
	if ( ConfigNanochem::InterfaceModellingTasks.at(0).control_points.size() < 3 ) {
		//if there are no control points, hope that the point cloud of candidates
		//is reasonably well spatially confined to create a useful plane...
		for( auto it = candidates.begin(); it != candidates.end(); it++ ) {
			p.push_back( IE_Point(it->x, it->y, it->z) );
		}
	}
	else {
		//...but if there are user-defined control point, use these instead
		for ( auto it = ConfigNanochem::InterfaceModellingTasks.at(0).control_points.begin();
				it != ConfigNanochem::InterfaceModellingTasks.at(0).control_points.end(); it++ ) {
			p.push_back( IE_Point(it->x, it->y, it->z) );
		}
	}

	//fit plane to points
	double fitting_quality = linear_least_squares_fitting_3(p.begin(), p.end(), plane, CGAL::Dimension_tag<0>());
	//##MK::fitting_quality is a K::FT

	cout << "Perform a PCA which yields the following plane equation: " << "\n";
	cout << "a = " << plane.a() << " b = " << plane.b() << " c = " << plane.c() << " d = " << plane.d() << "\n";

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	//apt_uint proc_id = taskid + 1;
	grpnm = "/entry" + to_string(entry_id) + "/interface_meshing/initial_interface";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXprocess") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/point_normal_form";
	vector<double> real;
	real.push_back( plane.a() ); //ax + by + cz + d = 0
	real.push_back( plane.b() );
	real.push_back( plane.c() );
	real.push_back( plane.d() );
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<double>();

	//##MK::could be exported
	cout << "Fitting quality " << fitting_quality << "\n";

	return true;
}


bool interfaceHdl::step_3_initial_interface_clip_by_domain( vector<p3d> const & pp3 )
{
	aabb3d domain_aabb = aabb3d();
	for( auto it = pp3.begin(); it != pp3.end(); it++ ) {
		domain_aabb.possibly_enlarge_me(*it);
	}
	cout << "Identified domain_aabb" << domain_aabb << "\n";

	vector<Point> domain_edges;
	//see Triangle cube intersection example
	domain_edges.push_back( Point(domain_aabb.xmi, domain_aabb.ymi, domain_aabb.zmi)); //0
	domain_edges.push_back( Point(domain_aabb.xmx, domain_aabb.ymi, domain_aabb.zmi)); //1
	domain_edges.push_back( Point(domain_aabb.xmx, domain_aabb.ymx, domain_aabb.zmi)); //2
	domain_edges.push_back( Point(domain_aabb.xmi, domain_aabb.ymx, domain_aabb.zmi)); //3
	domain_edges.push_back( Point(domain_aabb.xmi, domain_aabb.ymi, domain_aabb.zmx)); //4
	domain_edges.push_back( Point(domain_aabb.xmx, domain_aabb.ymi, domain_aabb.zmx)); //5
	domain_edges.push_back( Point(domain_aabb.xmx, domain_aabb.ymx, domain_aabb.zmx)); //6
	domain_edges.push_back( Point(domain_aabb.xmi, domain_aabb.ymx, domain_aabb.zmx)); //7
	cout << "domain_aabb.volume() " << domain_aabb.volume() << "\n";

	Mesh domain_sm;
	//boost::graph_traits<Graph>::halfedge_descriptor =
	CGAL::make_hexahedron(
			domain_edges[0], domain_edges[1], domain_edges[2], domain_edges[3],
	        domain_edges[4], domain_edges[5], domain_edges[6], domain_edges[7], domain_sm);

	if ( PMP::triangulate_faces(domain_sm) == true ) {
		cout << "Triangulating all faces of the mesh representing the domain success" << "\n";
	}
	else {
		cerr << "Triangulating all faces of the mesh representing the domain failed !" << "\n";
		return false;
	}

	CGAL::Polygon_mesh_slicer<Mesh, K> slicer(domain_sm);

	polylines = list<Polyline_type>();

	slicer(K::Plane_3(plane.a(), plane.b(), plane.c(), plane.d()), back_inserter(polylines));
	//##MK::catch return value

	if ( polylines.size() == 1 ) {
		cout << "The polyline lies here in R^3" << "\n";
		for( auto it = polylines.begin(); it != polylines.end(); it++ ) {
			cout << "polylines[0] has " << it->size() << " points" << "\n";
			for( auto jt = it->begin(); jt != it->end(); jt++ ) {
				cout << jt->x() << "\t\t" << jt->y() << "\t\t" << jt->z() << "\n";
			}
		}
	}
	else {
		cerr << "There should only be one valid polyline !" << "\n";
		return false;
	}
	cout << "Slicing the domain with the initial interface plane model created " << polylines.size() << " polylines" << "\n";

	//##MK::check that polyline is consistent

	patch.reserve(polylines.front().size() - 2); // there will be exactly n-2 triangles in the patch

	CGAL::Polygon_mesh_processing::triangulate_hole_polyline(polylines.front(), back_inserter(patch));

	for( size_t i = 0; i < patch.size(); i++ ) {
		cout << "Triangle " << i << ": " << patch[i].first << " " << patch[i].second << " " << patch[i].third << "\n";
	}

	//##MK::export triangles and polyline

	return true;
}


bool interfaceHdl::step_4_initial_interface_triangulate()
{
	//generate a consistently oriented and triangulated polygon from the surface patch bounded by the polylines[0]

	//##MK::maybe this assumption is unnecessary hyperphobic, but we can accept this for now and optimize later
	//##MK::get unique vertices
	/*
	set<int> unique_vrts;
	for( size_t i = 0; i < patch.size(); ++i ) {
		unique_vrts.insert(patch[i].first);
		unique_vrts.insert(patch[i].second);
		unique_vrts.insert(patch[i].third);
	}
	*/

	vector<array<FT, 3>> points;
	vector<CGAL_Polygon> polygons;
	size_t vertexID = 0;
	for ( size_t i = 0; i < patch.size(); i++ ) {
		Point u = polylines.front().at(patch[i].first);
		Point v = polylines.front().at(patch[i].second);
		Point w = polylines.front().at(patch[i].third);

		points.push_back(CGAL::make_array<FT>(u.x(), u.y(), u.z()));
		points.push_back(CGAL::make_array<FT>(v.x(), v.y(), v.z()));
		points.push_back(CGAL::make_array<FT>(w.x(), w.y(), w.z()));

		ply.clear();
		ply.push_back( vertexID ); vertexID++;
		ply.push_back( vertexID ); vertexID++;
		ply.push_back( vertexID ); vertexID++;
		polygons.push_back( ply );
	}
	cout << "After construction points.size() " << points.size() << " polygons.size() " << polygons.size() << "\n";
	//##MK::combinatorial repairing eventually modifies points and polygon arrays

	PMP::repair_polygon_soup(points, polygons, CGAL::parameters::geom_traits(Array_traits()));
	cout << "After repair_polygon_soup points.size() " << points.size() << " polygons.size() " << polygons.size() << "\n";

	if ( PMP::orient_polygon_soup(points, polygons) == true ) {

		PMP::polygon_soup_to_polygon_mesh(points, polygons, mesh_initial);

		cout << "After orient_polygon_soup points.size() " << points.size() << " polygons.size() " << polygons.size() << "\n";
		cout << "Step 4 patch to surface mesh success " << CGAL::num_faces(mesh_initial) << " faces " << "\n";
		cout << "vrts, fcts, edgs " << mesh_initial.number_of_vertices() << ", " << mesh_initial.number_of_faces() << ", " << mesh_initial.number_of_edges() << "\n";
	}
	else {
		cerr << "Initial interface triangulation failed !" << "\n";
		return false;
	}

	return true;
}


bool interfaceHdl::step_5a_intermediate_interface_remesh_isotropically( mesh_refine_step_info const & ifo )
{
	cout << "Remesh isotropically step " << ifo.stepid << " ..." << "\n";
	cout << "num_faces(mesh_curr) " << CGAL::num_faces(mesh_curr) << " faces " << "\n";
	cout << "mesh_curr.vertices() " << mesh_curr.number_of_vertices() << "\n";
	cout << "mesh_curr.number_of_faces() " << mesh_curr.number_of_faces() << "\n";
	cout << "mesh_curr.number_of_edges() " << mesh_curr.number_of_edges() << "\n";
	cout << "mesh_curr.number_of_halfedges() " << mesh_curr.number_of_halfedges() << "\n";
	cout << "mesh_curr.has_garbage() " << mesh_curr.has_garbage() << "\n";

	if ( CGAL::is_triangle_mesh(mesh_curr) == false ) {
		cerr << "mesh_curr is not a triangle mesh !" << "\n"; return false;
	}

	if ( mesh_curr.is_valid() == false ) {
		cerr << "mesh_curr is not valid !" << "\n"; return false;
	}

	if ( PMP::does_self_intersect(mesh_curr) == true ) {
		cerr << "mesh_curr self intersects !" << "\n"; return false;
	}


	double target_edge_length = ifo.target_edge_length; //nm
	unsigned int nb_iter = ifo.smoothing_iterations;

	cout << "Split border ..." << "\n";
	vector<edge_descriptor> border;
	PMP::border_halfedges(CGAL::faces(mesh_curr), mesh_curr, boost::make_function_output_iterator(halfedge2edge(mesh_curr, border)));

	PMP::split_long_edges(border, target_edge_length, mesh_curr);

	cout << "... done" << "\n";

	cout << "Start remeshing ..." << "\n";
	cout << "Target edge length " << target_edge_length << " nm" << "\n";
	cout << "Number of iterations " << nb_iter << "\n";
	//cout << "surface patch " << "(" << CGAL::num_faces(mesh_curr) << " faces)..." << "\n";

	PMP::isotropic_remeshing(CGAL::faces(mesh_curr), target_edge_length, mesh_curr,
			PMP::parameters::number_of_iterations(nb_iter).protect_constraints(true));

	//i.e. protect border, here
	cout << "... done" << "\n";
	cout << "num_faces(mesh_curr) " << CGAL::num_faces(mesh_curr) << " faces " << "\n";
	cout << "mesh_curr.vertices() " << mesh_curr.number_of_vertices() << "\n";
	cout << "mesh_curr.number_of_faces() " << mesh_curr.number_of_faces() << "\n";
	cout << "mesh_curr.number_of_edges() " << mesh_curr.number_of_edges() << "\n";
	cout << "mesh_curr.number_of_halfedges() " << mesh_curr.number_of_halfedges() << "\n";

	cout << "mesh_curr.number_of_removed_vertices() " << mesh_curr.number_of_removed_vertices() << "\n";
	cout << "mesh_curr.number_of_removed_faces() " << mesh_curr.number_of_removed_faces() << "\n";
	cout << "mesh_curr.number_of_removed_edges() " << mesh_curr.number_of_removed_edges() << "\n";
	cout << "mesh_curr.number_of_removed_halfedges() " << mesh_curr.number_of_removed_halfedges() << "\n";
	cout << "mesh_curr.has_garbage() " << mesh_curr.has_garbage() << "\n";

	//modifies the mesh and changes facet indices
	cout << "Collecting garbage ..." << "\n";

	mesh_curr.collect_garbage();

	cout << "... done" << "\n";

	cout << "num_faces(mesh_curr) " << CGAL::num_faces(mesh_curr) << " faces " << "\n";
	cout << "mesh_curr.vertices() " << mesh_curr.number_of_vertices() << "\n";
	cout << "mesh_curr.number_of_faces() " << mesh_curr.number_of_faces() << "\n";
	cout << "mesh_curr.number_of_edges() " << mesh_curr.number_of_edges() << "\n";
	cout << "mesh_curr.number_of_halfedges() " << mesh_curr.number_of_halfedges() << "\n";

	cout << "mesh_curr.number_of_removed_vertices() " << mesh_curr.number_of_removed_vertices() << "\n";
	cout << "mesh_curr.number_of_removed_faces() " << mesh_curr.number_of_removed_faces() << "\n";
	cout << "mesh_curr.number_of_removed_edges() " << mesh_curr.number_of_removed_edges() << "\n";
	cout << "mesh_curr.number_of_removed_halfedges() " << mesh_curr.number_of_removed_halfedges() << "\n";
	cout << "mesh_curr.has_garbage() " << mesh_curr.has_garbage() << "\n";

	return true;
}


bool interfaceHdl::step_5b_intermediate_interface_compute_normals()
{
	cout << "Compute normals..." << "\n";

	auto vnormals_curr = mesh_curr.add_property_map<sm_vertex_descriptor, Vector>("v:normals", CGAL::NULL_VECTOR).first;
	auto fnormals_curr = mesh_curr.add_property_map<sm_face_descriptor, Vector>("f:normals", CGAL::NULL_VECTOR).first;

	PMP::compute_normals(mesh_curr, vnormals_curr, fnormals_curr);

	vnrm_curr = vector<p3d>();
	for( const auto& vv : mesh_curr.vertices() ) {
		//cout << vnormals[vv] << "\n";
		vnrm_curr.push_back( p3d(
				vnormals_curr[vv].x(),
				vnormals_curr[vv].y(),
				vnormals_curr[vv].z()) );
	}

	fnrm_curr = vector<p3d>();
	for( const auto& fd: mesh_curr.faces()) {
		fnrm_curr.push_back( p3d(
				fnormals_curr[fd].x(),
				fnormals_curr[fd].y(),
				fnormals_curr[fd].z()) );
	}

	cout << "num_faces(mesh_curr) " << CGAL::num_faces(mesh_curr) << " faces " << "\n";
	cout << "mesh_curr.vertices() " << mesh_curr.number_of_vertices() << "\n";
	cout << "mesh_curr.number_of_faces() " << mesh_curr.number_of_faces() << "\n";
	cout << "mesh_curr.number_of_edges() " << mesh_curr.number_of_edges() << "\n";
	cout << "mesh_curr.number_of_halfedges() " << mesh_curr.number_of_halfedges() << "\n";
	cout << "vnrm_curr.size() " << vnrm_curr.size() << "\n";
	cout << "fnrm_curr.size() " << fnrm_curr.size() << "\n";

	return true;
}


bool interfaceHdl::step_5c_intermediate_interface_report_pre_dcom( mesh_refine_step_info const & ifo )
{
	string mesh_name = "mesh_state" + to_string((ifo.stepid * 2) + 1);
	cout << "Writing mesh_name " << mesh_name << " DCOM_PRE " << ifo.stepid << "\n";
	return write_interface_mesh( mesh_curr, mesh_name, ifo, DCOM_PRE );
}


bool interfaceHdl::step_5d_intermediate_interface_modify_dcom_step( mesh_refine_step_info const & ifo )
{
	//perform a single DCOM step on the vertices of the current mesh (mesh_curr)

	cout << "Performing DCOM step..." << "\n";

	cout << "num_faces(mesh_curr) " << CGAL::num_faces(mesh_curr) << " faces " << "\n";
	cout << "mesh_curr.vertices() " << mesh_curr.number_of_vertices() << "\n";
	cout << "mesh_curr.number_of_faces() " << mesh_curr.number_of_faces() << "\n";
	cout << "mesh_curr.number_of_edges() " << mesh_curr.number_of_edges() << "\n";
	cout << "mesh_curr.number_of_halfedges() " << mesh_curr.number_of_halfedges() << "\n";
	cout << "vnrm_curr.size() " << vnrm_curr.size() << "\n";
	cout << "fnrm_curr.size() " << fnrm_curr.size() << "\n";

	apt_real Rsqr = SQR(ifo.dcom_radius); //nm^2

	Mesh::Vertex_range vrng = mesh_curr.vertices();
	Mesh::Vertex_range::iterator vb = vrng.begin();
	size_t vv = 0;
	for(   ; vb != vrng.end(); vb++, vv++ ) {

		p3d me = p3d(
				mesh_curr.point(*vb).x(),
				mesh_curr.point(*vb).y(),
				mesh_curr.point(*vb).z() );

		//by virtue of construction the mesh vertex is not one of the ions
		//unless these positions coincide by pure chance
		vector<p3d> nbors_kdtree;
		kauri.kd_query_exclude_target( me, Rsqr, nbors_kdtree );
		/*
		kauri.range_rball_append_external( me, tmp_out, Rsqr, nbors_kdtree );
		*/

		/*
		//verification that kd_tree results are correct
		vector<p3d> nbors_naive;
		for( auto it = candidates.begin(); it != candidates.end(); it++ ) {
			if ( SQR(it->x - me.x)+SQR(it->y - me.y)+SQR(it->z - me.z) > Rsqr ) {
				continue;
			}
			else {
				nbors_naive.push_back(*it);
			}
		}

		//debugging
		if ( nbors_kdtree.size() != nbors_naive.size() ) {
			cerr << "Vertex " << ii << " nbors.size() " << nbors_kdtree.size() << ", " << nbors_naive.size() << "\n";
		}
		//else {
		//	cout << "Vertex " << ii << "\n";
		//}
		*/

		//cout << "nbors_kdtree.size() " << nbors_kdtree.size() << "\n";

		//##MK::GET VERTEX POSITION AND NORMAL VECTOR IN ONE LOOP
		if ( nbors_kdtree.size() >= 1 ) {
			//only move those vertices for which there are ions
			p3d barycenter = p3d(MYZERO, MYZERO, MYZERO);
			for( auto nbjt = nbors_kdtree.begin(); nbjt != nbors_kdtree.end(); nbjt++ ) {
				barycenter.x += (nbjt->x - me.x);
				barycenter.y += (nbjt->y - me.y);
				barycenter.z += (nbjt->z - me.z);
				//equal weight for each point
			}
			apt_real nrm = MYONE / ((apt_real) nbors_kdtree.size());
			barycenter.x *= nrm;
			barycenter.y *= nrm;
			barycenter.z *= nrm;

			//##MK::get the vertex normal more efficiently

			//cout << "Accessing normal vector *vb, vv " << *vb << ", " << vv << " vnrm_curr.size() " << vnrm_curr.size() << "\n";

			apt_real shift = dot( barycenter, vnrm_curr[vv] );

			//cout << "Shift known" << "\n";

		    p3d disp = p3d(
		    		vnrm_curr[vv].x * shift,
					vnrm_curr[vv].y * shift,
					vnrm_curr[vv].z * shift );

		    /*
		    cout << "Vertex " << ii << " shift " << shift << " disp "
		    		<< disp.x << " " << disp.y << " " << disp.z << " vertex "
					<< me.x << " " << me.y << " " << me.z << "\n";
			*/

		    //store the coordinates of displaced vertices and create a new mesh

		    //##MK::DISPLACE vertices and REPORT

		    me.x += disp.x;
		    me.y += disp.y;
		    me.z += disp.z;

		    //cout << " Relocating vertex *vb, vv " << *vb << ", " << vv << "\n";
		    //mesh_deform.point(*vdefb) = Point(me.x, me.y, me.z);
		    mesh_curr.point(*vb) = Point(me.x, me.y, me.z);
		}
	}

	cout << "... done" << "\n";

	mesh_next = mesh_curr;
	cout << "num_faces(mesh_next) " << CGAL::num_faces(mesh_next) << " faces " << "\n";
	cout << "mesh_next.vertices() " << mesh_next.number_of_vertices() << "\n";
	cout << "mesh_next.number_of_faces() " << mesh_next.number_of_faces() << "\n";
	cout << "mesh_next.number_of_edges() " << mesh_next.number_of_edges() << "\n";
	cout << "mesh_next.number_of_halfedges() " << mesh_next.number_of_halfedges() << "\n";

	return true;
}


bool interfaceHdl::step_5e_intermediate_interface_check_self_intersections()
{
	if ( PMP::does_self_intersect( mesh_curr ) == true ) {
		cerr << "CGAL detected that mesh_curr does self intersect !" << "\n"; return false;
	}

	if ( PMP::does_self_intersect( mesh_next ) == true ) {
		cerr << "CGAL detected that mesh_next does self intersect !" << "\n"; return false;
	}

	cout << "CGAL detected that both mesh_curr and mesh_next do not self intersect, so they to be healthy meshes" << "\n";
	return true;
}


bool interfaceHdl::step_5f_intermediate_interface_report_post_dcom( mesh_refine_step_info const & ifo )
{
	string mesh_name = "mesh_state" + to_string((ifo.stepid * 2) + 2);
	cout << "Writing mesh_name " << mesh_name << " DCOM_POST " << ifo.stepid << "\n";
	return write_interface_mesh( mesh_next, mesh_name, ifo, DCOM_POST );
}


bool interfaceHdl::step_5_initial_interface_refine_iteratively()
{
	parms = vector<mesh_refine_step_info>();
	if ( taskid >= ConfigNanochem::InterfaceModellingTasks.size() ) {
		cerr << "Attempting an unexpected access on modelling task" << "\n";
		return false;
	}
	for( size_t iter = 0; iter < ConfigNanochem::InterfaceModellingTasks[taskid].number_of_iterations; iter++ ) {
		parms.push_back( mesh_refine_step_info(
				ConfigNanochem::InterfaceModellingTasks[taskid].iteration_parms[iter].target_edge_length,
				ConfigNanochem::InterfaceModellingTasks[taskid].iteration_parms[iter].dcom_radius,
				ConfigNanochem::InterfaceModellingTasks[taskid].iteration_parms[iter].smoothing_iter,
				iter,
				DCOM_UNKNOWN) );
	}
	cout << "parms.size() " << parms.size() << "\n";

	//parms.push_back( mesh_refine_step_info( 10.0, 10.0, 10, 0) );
	//parms.push_back( mesh_refine_step_info(  5.0,  5.0, 10, 1) );
	//parms.push_back( mesh_refine_step_info(  1.0,  1.0, 10, 2) );

	for( size_t step = 0; step < parms.size(); step++ ) {
		string mesh_name = "";
		if ( step == 0) {
			mesh_curr = mesh_initial;
		}
		else {
			mesh_curr = mesh_next;
		}

		if ( step_5a_intermediate_interface_remesh_isotropically( parms[step] ) == false ) {
			cerr << "Interface refinement step " << step << " failed !" << "\n"; return false;
		}

		if ( step_5b_intermediate_interface_compute_normals() == false ) {
			cerr << "Compute normal step " << step << " failed !" << "\n"; return false;
		}

		if ( step_5c_intermediate_interface_report_pre_dcom( parms[step] ) == false ) {
			cerr << "Report pre dcom failed !" << "\n"; return false;
		}

		if ( step_5d_intermediate_interface_modify_dcom_step( parms[step] ) == false ) {
			//##MK::takes mesh_curr moves the vertices and creates a new mesh
			cerr << "DCOM step failed !" << "\n"; return false;
		}

		if ( step_5e_intermediate_interface_check_self_intersections() == false ) {
			cerr << "Check mesh_curr for self-intersection failed !" << "\n"; return false;
		}

		if ( step_5f_intermediate_interface_report_post_dcom( parms[step] ) == false ) {
			cerr << "Report post dcom failed !" << "\n"; return false;
		}

		/*
		//##MK::for now manually overwriting the role which the dcom step should take !!!
		//############mesh_next = mesh_curr;
		*/
	}
	return true;
}



bool interfaceHdl::step_6_final_interface_clip_by_edge_model()
{
	//##MK::not implemented for now
	return true;
}


bool interfaceHdl::step_7_final_interface_report()
{
	return true;
}


bool interfaceHdl::write_interface_mesh( Mesh & msh, const string dsnm_prefix, 
	mesh_refine_step_info const & ifo, const unsigned char when )
{
	apt_uint entry_id = 1;
	//apt_uint proc_id = taskid + 1;
	string mesh_state = "";
	string xmlfn = strip_path_prefix(ConfigShared::OutputfileName) 
		+ ".InterfaceMeshing.EntryId.1";
	if ( when == DCOM_PRE ) {
		xmlfn += ".PreDCOM";
		mesh_state = "before";
	}
	else if ( when == DCOM_POST ) {
		xmlfn += ".PostDCOM";
		mesh_state = "after";
	}
	else {
		return false;
	}
	xmlfn += ".Step." + to_string(ifo.stepid + 1) + ".xdmf";

	xdmfBaseHdl xml;
	xml.add_grid_uniform( "refined" );

	vector<apt_real> vrts;
	vector<apt_uint> fcts;
	vector<apt_uint> topo;
	vector<apt_uint> fid;

	vector<apt_real> area;
	vector<apt_real> edgelen;
	vector<apt_real> angles;

	Mesh::Vertex_range vrng = msh.vertices();
	Mesh::Vertex_range::iterator vb = vrng.begin();
	Mesh::Vertex_range::iterator ve = vrng.end();
	for(   ; vb != ve; ++vb ) {
		vrts.push_back( msh.point(*vb).x() );
		vrts.push_back( msh.point(*vb).y() );
		vrts.push_back( msh.point(*vb).z() );
	}

	Mesh::Face_range frng = msh.faces();
	Mesh::Face_range::iterator fb = frng.begin();
	Mesh::Face_range::iterator fe = frng.end();
	apt_uint facet_id = 0; //facet_ids are defined implicitly by how the for loop executes and populates the geometry/topology data
	for(   ; fb != fe; ++fb ) {
		vector<apt_uint> triangle; //https://doc.cgal.org/latest/Surface_mesh/index.html#circulators_example
		CGAL::Vertex_around_face_iterator<Mesh> vbegin, vend;
		for(boost::tie(vbegin, vend) = vertices_around_face(msh.halfedge(*fb), msh); vbegin != vend; ++vbegin ) {
			triangle.push_back( (apt_uint) *vbegin );
		}
		if ( triangle.size() == 3 ) {
			apt_uint u = triangle[0];
			apt_uint v = triangle[1];
			apt_uint w = triangle[2];

			tri3d tri = tri3d(
					vrts[3*u+0], vrts[3*u+1], vrts[3*u+2],
					vrts[3*v+0], vrts[3*v+1], vrts[3*v+2],
					vrts[3*w+0], vrts[3*w+1], vrts[3*w+2]   );
			//##MK::query more efficiently via the circulator instead

			area.push_back( tri.area() );

			edgelen.push_back( tri.edge_length_ab() );
			edgelen.push_back( tri.edge_length_bc() );
			edgelen.push_back( tri.edge_length_ca() );

			//a, 1 / b, 2 / c, 3
			apt_real ba_ca = tri.angle_ba_ca();
			apt_real cb_ab = tri.angle_cb_ab();
			apt_real ac_bc = tri.angle_ac_bc();
			angles.push_back( ba_ca );
			angles.push_back( cb_ab );
			angles.push_back( ac_bc );
			angles.push_back( ba_ca + cb_ab + ac_bc );  //angle sum (should be pi/2)

			fcts.push_back(u);
			fcts.push_back(v);
			fcts.push_back(w);

			topo.push_back(3); //xdmf keyword for polygon
			topo.push_back(3); //three vertices has a triangle
			topo.push_back(u);
			topo.push_back(v);
			topo.push_back(w);

			fid.push_back(facet_id);
			facet_id++;
		}
		else {
			cout << "ERROR::triangle.size() != 3 !" << "\n";
		}
	}

	string grpnm = "";
	string dsnm = "";

	grpnm = "/entry" + to_string(entry_id) + "/interface_meshing/" + dsnm_prefix;
	vector<size_t> dims = { topo.size() }; //xdmf keyword, xdmf number of vertices, three vertex IDs for each triangle
	xml.add_topology_mixed( topo.size() / 5, dims, H5_U32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/triangles/xdmf_topology" );
	dims = { vrts.size() / 3, 3 };
	xml.add_geometry_xyz( dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/triangles/vertices" );
	dims = { fid.size() };
	xml.add_attribute_scalar( "facet_id", "Cell", dims, H5_U32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/triangles/face_identifier" );
	dims = { vnrm_curr.size(), 3 };
	xml.add_attribute_vector( "vnormal", "Node", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/triangles/vertex_normal" );
	dims = { fnrm_curr.size(), 3 };
	xml.add_attribute_vector( "fnormal", "Cell", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/triangles/face_normal" );
	xml.write( xmlfn );

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();

	grpnm = "/entry" + to_string(entry_id) + "/interface_meshing/" + dsnm_prefix;
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_triangle_set") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/state";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, mesh_state, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/dcom_step";
	apt_uint dcom_id = ifo.stepid + 1;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dcom_id, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/dimensionality";
	apt_uint dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/cardinality";
	apt_uint card = (apt_uint) fcts.size() / 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, card, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/identifier_offset";
	apt_uint id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/area";
	anno = ioAttributes();
	anno.add( "unit", string("nm^2") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({area.size()}, {area.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		area,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	area = vector<apt_real>();

	dsnm = grpnm + "/edge_length";
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({edgelen.size() / 3, 3},
				{edgelen.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		edgelen,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	edgelen = vector<apt_real>();

	dsnm = grpnm + "/interior_angle";
	anno = ioAttributes();
	anno.add( "unit", string("rad") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({angles.size() / 4, 4},
				{angles.size() / 4, 4},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		angles,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	angles = vector<apt_real>();

	//triangles
	grpnm = "/entry" + to_string(entry_id) + "/interface_meshing/" + dsnm_prefix + "/triangles";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_face_list_data_structure") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/dimensionality";
	dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_vertices";
	apt_uint n_vrts = (apt_uint) vrts.size() / 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_vrts, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_faces";
	apt_uint n_fcts = (apt_uint) fcts.size() / 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_fcts, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/vertex_identifier_offset";
	apt_uint id_vrts_off = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_vrts_off, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/face_identifier_offset";
	apt_uint id_fcts_off = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_fcts_off, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/face_identifier";
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({fid.size()}, {fid.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		fid,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	fid = vector<apt_uint>();

	dsnm = grpnm + "/vertices";
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({vrts.size() / 3, 3},
				{vrts.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		vrts,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	vrts = vector<apt_real>();

	dsnm = grpnm + "/faces";
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({fcts.size() / 3, 3},
				{fcts.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		fcts,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	fcts = vector<apt_uint>();

	dsnm = grpnm + "/xdmf_topology";
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({topo.size()}, {topo.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		topo,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	topo = vector<apt_uint>();

	//face normals
	cout << "Face normals :" << "\n";
	grpnm = "/entry" + to_string(entry_id) + "/interface_meshing/" + dsnm_prefix + "/triangles";
	dsnm = grpnm + "/face_normal";
	vector<apt_real> fnrm;
	for( auto jt = fnrm_curr.begin(); jt != fnrm_curr.end(); jt++ ) {
		fnrm.push_back(jt->x);
		fnrm.push_back(jt->y);
		fnrm.push_back(jt->z);
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({fnrm.size() / 3, 3},
				{fnrm.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		fnrm,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	fnrm = vector<apt_real>();

	dsnm = grpnm + "/face_normal_orientation";
	vector<unsigned char> u08;
	u08 = vector<unsigned char>( fnrm_curr.size(), 0x00 ); //undefined
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	//vertex normals
	cout << "Vertex normals :" << "\n";
	grpnm = "/entry" + to_string(entry_id) + "/interface_meshing/" + dsnm_prefix + "/triangles";
	dsnm = grpnm + "/vertex_normal";
	vector<apt_real> vnrm;
	for( auto it = vnrm_curr.begin(); it != vnrm_curr.end(); it++ ) {
		vnrm.push_back(it->x);
		vnrm.push_back(it->y);
		vnrm.push_back(it->z);
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({vnrm.size() / 3, 3},
				{vnrm.size() / 3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		vnrm,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	vnrm = vector<apt_real>();

	dsnm = grpnm + "/vertex_normal_orientation";
	u08 = vector<unsigned char>( vnrm_curr.size(), 0x00 ); //undefined
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	return true;
}
