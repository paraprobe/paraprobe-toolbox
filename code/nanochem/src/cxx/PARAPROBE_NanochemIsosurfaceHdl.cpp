/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemIsosurfaceHdl.h"

isosurfaceHdl::isosurfaceHdl()
{
	gridinfo = voxelgrid();
	scalarfield = vector<apt_real>();
	scalarfield_grad3d = vector<apt_real>();
	scalarfield_edge = vector<unsigned char>();
	mc = MarchingCubes();
	tscl = triangleSoupClustering();
	rois = vector<roi_rotated_cuboid*>();
	objs = vector<manifoldHdl*>();

	closed_polyhedra_bvh = NULL;
	info = isosurf_task_info();
	isosurf_tictoc = profiler();
}


isosurfaceHdl::~isosurfaceHdl()
{
	scalarfield = vector<apt_real>();
	scalarfield_grad3d = vector<apt_real>();
	scalarfield_edge = vector<unsigned char>();
	mc = MarchingCubes();
	tscl = triangleSoupClustering();
	for( size_t i = 0; i < rois.size(); i++ ) {
		if ( rois.at(i) != NULL ) {
			delete rois.at(i);
			rois.at(i) = NULL;
		}
	}
	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs.at(i) != NULL ) {
			delete objs.at(i);
			objs.at(i) = NULL;
		}
	}

	if ( closed_polyhedra_bvh != NULL ) {
		delete closed_polyhedra_bvh;
		closed_polyhedra_bvh = NULL;
	}
	info = isosurf_task_info();
}


bool isosurfaceHdl::voxelize_delocalize_gaussian_fast_per_iontype_multithreaded(
	vector<unsigned char> const & wdw, vector<p3d> const & pp3, vector<p3dinfo> const & ityps, rangeTable & rngifo )
{
	double tic = MPI_Wtime();

	//wdw is a mask indicating which ions should be included in the analysis, these are marked with 0xFF all other points are omitted
	//this mask can encode the result of a previous filtering for position and iontype or only one of these two filters

	//multithreaded finding of the axis-aligned bounding box to pp3
	if ( wdw.size() != pp3.size() || wdw.size() != ityps.size() ) {
		cerr << "We expect that the point cloud and iontype arrays have the same length and encode pairs of position and iontype label !" << "\n";
		return false;
	}

	//get the axis-aligned bounding box to the specified windowed dataset
	vector<vector<apt_uint>*> evapid_ityp_lu = vector<vector<apt_uint>*>( rngifo.iontypes.size(), NULL );
	for( size_t ityp = 0; ityp < rngifo.iontypes.size(); ityp++ ) {
		vector<apt_uint>* evapid_ityp = NULL;
		evapid_ityp = new vector<apt_uint>();
		if ( evapid_ityp != NULL ) {
			evapid_ityp_lu[ityp] = evapid_ityp;
		}
		else {
			for( size_t j = 0; j < ityp; j++ ) {
				if ( evapid_ityp_lu[j] != NULL ) {
					delete evapid_ityp_lu[j];
					evapid_ityp_lu[j] = NULL;
				}
			}
			cerr << "Allocating master evapid_ityp_lu lookup bucket list failed for ityp " << ityp << " !" << "\n";
			return false;
		}
	}

	bool healthy = true;
	aabb3d box = aabb3d();
	vector<threadedDelocalizer*> distr_grid;
	vector<apt_uint> zhist; //ityp-specific histogram of ion locations along z direction for static load partitioning and distributing on threads
	#pragma omp parallel shared(evapid_ityp_lu, healthy, box, distr_grid, zhist)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		int mystatus = MYDELOC_SUCCESS;
		vector<vector<apt_uint>*> myevapid_ityp_lu = vector<vector<apt_uint>*>( rngifo.iontypes.size(), NULL );
		for( size_t ityp = 0; ityp < rngifo.iontypes.size(); ityp++ ) {
			vector<apt_uint>* myevapid_ityp = NULL;
			myevapid_ityp = new vector<apt_uint>();
			if ( myevapid_ityp != NULL ) {
				myevapid_ityp_lu[ityp] = myevapid_ityp;
			}
			else {
				mystatus = MYDELOC_LU_ALLOC_ERROR;
				for( size_t j = 0; j < ityp; j++ ) {
					if ( myevapid_ityp_lu[j] != NULL ) {
						delete myevapid_ityp_lu[j]; myevapid_ityp_lu[j] = NULL;
					}
				}
			}
		}
		aabb3d mybox = aabb3d();
		#pragma omp barrier

		//all threads are ready to walk only once the large position array and categorize base on z location using thread-local memory
		#pragma omp for schedule(dynamic, 1) nowait
		for( size_t i = 0; i < wdw.size(); i++ ) {
			if ( wdw[i] != ANALYZE_NO ) { //WINDOW_ION_EXCLUDE
				mybox.possibly_enlarge_me( pp3[i] );
				size_t ityp = (size_t) ityps[i].i_org;
				myevapid_ityp_lu[ityp]->push_back( i );
			}
		}
		#pragma omp critical
		{
			box.possibly_enlarge_me( mybox );
			for( size_t ityp = 0; ityp < rngifo.iontypes.size(); ityp++ ) {
				if ( myevapid_ityp_lu[ityp] != NULL ) {
					evapid_ityp_lu[ityp]->insert(
						evapid_ityp_lu[ityp]->end(),
						myevapid_ityp_lu[ityp]->begin(),
						myevapid_ityp_lu[ityp]->end() );
					delete myevapid_ityp_lu[ityp]; myevapid_ityp_lu[ityp] = NULL;
				}
			}
		}
		myevapid_ityp_lu = vector<vector<apt_uint>*>();
		//now the master has a full z-position bucket list while
		//the threads have theirs emptied and box is known with halos
		#pragma omp barrier

		#pragma omp master
		{
			box.scale();
			cout << "Defining a box for the delocalization" << "\n" << box << "\n";

			gridinfo = voxelgrid( box, info.resu, info.deloc_halfsize_a + 1); //one voxel guard zone on either sides always plus the one for needed for the kernel

			cout << "Defining the voxelgrid with the guard zone including the halo for the kernels" << "\n" << gridinfo << "\n";

			distr_grid = vector<threadedDelocalizer*>( (size_t) nt, NULL );
			zhist = vector<apt_uint>( gridinfo.nz, 0 );
		}
		//necessary, master needs to define the delocalization grid first, and carrier for pointers to distributed grid portions has to exist
		#pragma omp barrier

		voxelgrid mygridifo = gridinfo;
		//##MK::BEGIN DEBUG
		#pragma omp critical
		{
			cout << "Thread " << mt << " mygrid " << "\n" << mygridifo << "\n";
		}
		//##MK::END DEBUG

		for( size_t ityp = 0; ityp < rngifo.iontypes.size(); ityp++ ) { //main loop multithreaded reusing voxelgrids
			vector<apt_uint> myzhist = vector<apt_uint>( gridinfo.nz, 0 );
			#pragma omp master
			{
				cout << "Start delocalizing for ityp " << ityp << "\n";
				cout << "kernel_type: gaussian" << "\n";
				cout << "kernel_halfsize: " << info.deloc_halfsize_a << "\n";
				cout << "kernel_sigma: " << info.deloc_sigma << "\n";
				zhist = vector<apt_uint>( gridinfo.nz, 0 );
			}
			//not strictly necessary but makes cout cleaner
			#pragma omp barrier
			#pragma omp for schedule(dynamic, 1) nowait
			//if ( mystatus == MYDELOC_SUCCESS ) {
			for( auto it = evapid_ityp_lu[ityp]->begin(); it != evapid_ityp_lu[ityp]->end(); it++ ) {
				apt_int iz = mygridifo.where_z( pp3[*it].z );
				myzhist[iz]++;
			}
			//}
			//necessary because zhist has to be initialized to zero!
			#pragma omp barrier
			#pragma omp critical
			{
				for( apt_int iz = 0; iz < mygridifo.nz; iz++ ) {
					zhist[iz] += myzhist[iz];
				}
			}
			//necessary barrier, we need to let all threads coorperatively identify
			//how much work there is for each layer of that ityp
			#pragma omp barrier

			/*
			//##MK::BEGIN DEBUG
			#pragma omp master
			{
				for( apt_int iz = 0; iz < gridinfo.nz; iz++ ) {
					cout << "\t\t" << iz << "\t" << zhist[iz] << "\n";
				}
			}
			#pragma omp barrier
			//##MK::END DEBUG
			*/

			//given the conical shape of APT tips we do not balance the load based on the number of layers per thread
			//but such that each threads gets approximately the same number of ions
			//therefore threads which are responsible for regions in the top of the reconstruction get a relatively
			//larger number of zlayers than threads processing a region close to the base the larger it is the shank angle
			//the code automatically adapts to the shape of the specimen via init_equilibrate_number_of_ions

			//now distribute this voxel grid into cuboidal sections, splitting planes normal to z, the longest axis of the dataset
			//and perform a multithreaded delocalization thread-parallel allocation and first-touch

			threadedDelocalizer* mygrid = NULL;
			if ( mystatus == MYDELOC_SUCCESS ) {
				mygrid = new threadedDelocalizer;
				if ( mygrid == NULL ) {
					mystatus = MYDELOC_SOLVER_ALLOC_ERROR;
				}
			}
			//now we compute a spatial partitioning of a voxelgrid as large as the above defined, for each thread the box has a guard on either
			//apt_uint myion_budget
			mygrid->init_and_load_distributing(
				mygridifo, zhist, info.deloc_halfsize_a, info.deloc_sigma, mt, nt );
			/*
			#pragma omp critical
			{
				//##MK::BEGIN DEBUG
				cout << "Thread " << omp_get_thread_num() << " participating " << (int) mygrid->participating
						<< " zbounds_ions " << mygrid->zbounds_ions.first << ", " << mygrid->zbounds_ions.second
						<< " zbounds_halo " << mygrid->zbounds_halo.first << ", " << mygrid->zbounds_halo.second
						<< " myionbudget " << myion_budget << "\n";
				//##MK::END DEBUG
			}
			*/

			mygrid->build_bvh_ityp( evapid_ityp_lu[ityp], pp3 );
			/*
			#pragma omp critical
			{
				//##MK::BEGIN DEBUG
				cout << "Thread " << omp_get_thread_num() << " cnts_f64.size() " << mygrid->cnts_f64.size()
						<< " myions.size() " << mygrid->myions.size() << " bvh_mgn.size() "
						<< mygrid->bvh_mgn.size() << " points.size() " << mygrid->points.size() << "\n";
				//##MK:END DEBUG
			}
			*/

			mygrid->compute_delocalization_ityp();
			#pragma omp critical
			{
				distr_grid.at(mt) = mygrid;
			}
			//necessary, threads from all results should be ready before master summarizes
			#pragma omp barrier

			//master collects results from threads
			#pragma omp master
			{
				vector<double> f64 = vector<double>( (size_t) gridinfo.nxyz, 0. );
				//collect thread-local result for storing that ityp-specific delocalization result
				for( size_t thr = 0; thr < distr_grid.size(); thr++ ) {
					if ( distr_grid[thr] != NULL ) {
						if ( distr_grid[thr]->status == MYDELOC_SUCCESS ) {
							vector<double> & thisone = distr_grid[thr]->cnts_f64;
							size_t xyzoffset = (size_t) distr_grid[thr]->zbounds_halo.first * (size_t) gridinfo.nxy;
							size_t nlocal_xyz = (size_t) (distr_grid[thr]->zbounds_halo.second - distr_grid[thr]->zbounds_halo.first) * (size_t) gridinfo.nxy;
							for( size_t local_xyz = 0; local_xyz < nlocal_xyz; local_xyz++ ) {
								f64[xyzoffset+local_xyz] += thisone[local_xyz];
							}
						}
						else {
							healthy = false;
						}
						delete distr_grid[thr]; distr_grid[thr] = NULL;
					}
				}

				//write raw i-typ specific results to HDF5 file and discard
				write_iontype_specific_delocalization_result( ityp, rngifo, f64 );
			}
			//necessary to keep the threads in sync, now all threads know if still healthy
			#pragma omp barrier
			if ( healthy == false ) {
				break;
			}
		} //next ityp
	} //end of parallel region

	for( size_t ityp = 0; ityp < rngifo.iontypes.size(); ityp++ ) {
		if ( evapid_ityp_lu[ityp] != NULL ) {
			delete evapid_ityp_lu[ityp]; evapid_ityp_lu[ityp] = NULL;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "DelocalizeGaussianIonDecomposeDelocTask"+to_string(info.dlzid), APT_XX, APT_IS_PAR, mm, tic, toc);
	cout << "Delocalization gaussian fast including the decomposing of molecular ions in their elements took " << (toc-tic) << " s" << "\n";
	if ( healthy == true ) {
		return true;
	}
	return false;
}


bool isosurfaceHdl::voxelize_delocalize_compose_from_iontype( rangeTable & rngifo )
{
	scalarfield = vector<apt_real>( (size_t) gridinfo.nxyz, 0. );
	scalarfield_edge = vector<unsigned char>( (size_t) gridinfo.nxyz, VOXEL_EVALUATE_YES );

	//interpret the mode and identify relevant iontypes to load and the factorization whereby they are composed into the scalar_field
	vector<pair<int, apt_uint>> ityp_all;
	vector<pair<int, apt_uint>> ityp_cand;
	cout << "voxelize_delocalize_compose_from_iontype" << "\n";
	for( auto it = rngifo.iontypes.begin(); it != rngifo.iontypes.end(); it++ ) {
		apt_uint multiplicity_sum = 1;
		//for deloc_method == resolve_point, resolve_ion, resolve_ion_charge
		if ( info.deloc_method.compare("resolve_element") == 0 ||
			info.deloc_method.compare("resolve_element_charge") == 0 ||
				info.deloc_method.compare("resolve_isotope") == 0 ||
					info.deloc_method.compare("resolve_isotope_charge") == 0 ) {
			multiplicity_sum = it->second.get_multiplicity(
				"resolve_atom", info.nuclide_hash_whitelist, info.charge_state_whitelist );
		}

		apt_uint multiplicity_trg = 0;
		multiplicity_trg = it->second.get_multiplicity(
			info.deloc_method, info.nuclide_hash_whitelist, info.charge_state_whitelist );

		cout << "ityp: " << (int) it->first
			<< " multiplicity sum: " << multiplicity_sum
			<< " multiplicity trg: " << multiplicity_trg << "\n";
		ityp_all.push_back( pair<int, apt_uint>((int) it->first, multiplicity_sum) );
		if ( multiplicity_trg > 0 ) {
			ityp_cand.push_back( pair<int, apt_uint>((int) it->first, multiplicity_trg) );
		}
	}
	cout << "ityp_all.size() " << ityp_all.size() << "\n";
	cout << "ityp_cand.size() " << ityp_cand.size() << "\n";

	vector<double> f64_trg = vector<double>( (size_t) gridinfo.nxyz, 0. );
	vector<double> f64_sum;
	if ( info.normalization == ISOSURF_NRMLZ_NONE ||
			info.normalization == ISOSURF_NRMLZ_COMPOSITION ) {
		f64_sum = vector<double>( (size_t) gridinfo.nxyz, 0. );
	}
	vector<double> f64_ityp;

	HdfFiveSeqHdl h5r = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	string grpnm = "/entry1/delocalization" + to_string(info.dlzid) + "/grid";
	string dsnm = "";

	for( auto it = ityp_all.begin(); it != ityp_all.end(); it++ ) { //for normalization total, candidates, and composition
		//load and decompress from HDF5
		dsnm = grpnm + "/scalar_field_magn_ion" + to_string(it->first) + "/xdmf_intensity";
		//for cases where the delocalization is just reloading previous results we have to assume that
		//the iontype is really the same as the one for which the scalar_field_magn_ion was computed
		//as well as the dimensions of the grid
		if ( h5r.nexus_read( dsnm, f64_ityp ) == MYHDF5_SUCCESS ) {
			cout << "Loaded ityp-specific delocalization field values for ityp " << it->first << " success " << "\n";
		}
		else {
			cerr << "Loaded ityp-specific delocalization field values for ityp " << it->first << " failed !" << "\n";
			return false;
		}
		if ( f64_ityp.size() != f64_trg.size() ) {
			cerr << "Unexpectedly delocalization field" << "\n";
		}

		//accumulate for all to get denominator values for the normalization
		if ( info.normalization == ISOSURF_NRMLZ_NONE ||
				info.normalization == ISOSURF_NRMLZ_COMPOSITION ) {
			double mult_sum = (double) it->second;
			/*
			if ( info.normalization == ISOSURF_NRMLZ_NONE ) {
				mult_sum = MYONE;
			}
			*/
			//FMA3-style fuse multiply add relevant
			for( size_t j = 0; j < f64_ityp.size(); j++ ) {
				f64_sum[j] = mult_sum * f64_ityp[j] + f64_sum[j];  //a = b * c + a
			}
			//do not delete f64_ityp yet, still required
		}

		//info.normalization ==	ISOSURF_NRMLZ_COUNTS_SUM nothing needed can take the f64_sum
		if ( info.normalization == ISOSURF_NRMLZ_COMPOSITION ||
				info.normalization == ISOSURF_NRMLZ_CONCENTRATION ) {
			for ( auto jt = ityp_cand.begin(); jt != ityp_cand.end(); jt++ ) {
				if ( it->first == jt->first ) {
					//FMA3-style fuse multiply add relevant
					double mult_trg = (double) jt->second; //not it->second !!
					for( size_t j = 0; j < f64_ityp.size(); j++ ) {
						f64_trg[j] = mult_trg * f64_ityp[j] + f64_trg[j];  //a = b * c + a
						//do not add to f64_sum again because this has already been done
						//if it is required above != .. CONCENTRATION !
					}
					break; //because we found the candidate
				}
			}
		}
		f64_ityp = vector<double>();
	}

	if ( info.normalization == ISOSURF_NRMLZ_NONE ) {
		for( size_t i = 0; i < scalarfield.size(); i++ ) {
			scalarfield[i] = (apt_real) f64_sum[i];
			//scalarfield[i] = (apt_real) f64_trg[i];
		}
	}
	else if ( info.normalization == ISOSURF_NRMLZ_COMPOSITION ) {
		for ( size_t i = 0; i < scalarfield.size(); i++ ) {
			double divisor = f64_sum[i];
			if ( divisor >= EPSILON_F64 ) { //catch division by zero
				double val = f64_trg[i] / divisor;
				scalarfield[i] = (apt_real) val;
				//when computing compositions with intensities a/b, with a, b > 0 in \mathcal{R} we face
				//issues when a and b become smaller than 1 have to be dealt with
				//the quotient will increase regardless the value of b at the edge of the dataset this
				//creates systematically larger composition values these are in particular dominant when
				//the tails of the Gaussians are very low, for instance a voxel with maybe yes dozens of
				//ions contributing intensity but all small tails so maybe a = 0.0001 and
				//and b = 0.001, resulting in high compositions, so high that iso-surface MC algorithms will place triangles there
				//we can prevent this edge effect by instructing MC to skip voxels whose cnts_bin was less than MYONE
				if ( info.edge_handling == ISOSURF_EDGE_TRIANGLES_REMOVE ) {
					if ( f64_trg[i] < MYONE ) {
						scalarfield_edge[i] = VOXEL_EVALUATE_NO;
					}
				}
			}
			if ( f64_trg[i] <= divisor ) {
				continue;
			}
			else {
				cerr << "Unexpected division by zero error during ISOSURF_NRMLZ_COMPOSITION !" << "\n";
				scalarfield = vector<apt_real>( gridinfo.nxyz, 0. );
				return false;
			}
		}
	}
	else { //ISOSURF_NRMLZ_CONCENTRATION
		double divisor = gridinfo.dx * gridinfo.dy * gridinfo.dz; //nm^3
		if ( divisor >= EPSILON_F64 ) {
			for ( size_t i = 0; i < scalarfield.size(); i++ ) {
				double val = f64_trg[i] / divisor;
				scalarfield[i] = (apt_real) val;
			}
		}
		else {
			cerr << "Unexpected division by zero error during ISOSURF_NRMLZ_COMPOSITION !" << "\n";
			scalarfield = vector<apt_real>( gridinfo.nxyz, 0. );
			return false;
		}
	}
	return true;
}


bool isosurfaceHdl::compute_scalarfield_gradient3d()
{
	double tic = MPI_Wtime();

	//##MK::currently this is only an implementation with a testing purpose code quality and sequential implementation,
	//##MK::no problem because there are thousands of well-documented cases how such finite difference implementations
	//##MK::for regular 3D grids can be improved performance-wise
	//##MK::factually entire PhDs and books were written on how to get these stencil algorithms optimized for cache misses and flops, and GPUs
	if ( scalarfield.size() != (size_t) gridinfo.nxyz ) {
		cerr << "scalarfield is not of the correct size!" << "\n";
		return false;
	}

	try {
		scalarfield_grad3d = vector<apt_real>(  3 * (size_t) gridinfo.nxyz, 0. ); //(0,1,2+3*(x+y*NX+z*NX*NY)) 0<=>x, 1<=>y, 2<=>z
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of scalarfield_gradient3d failed !" << "\n"; return false;
	}

	apt_real one_h = gridinfo.dx; //*1; //##MK::
	if ( one_h < EPSILON ) {
		cerr << "one_h needs to be at least as large as EPSILON !" << "\n";
		return false;
	}
	apt_real two_h = 2.*one_h;
	apt_int nx = gridinfo.nx;
	apt_int ny = gridinfo.ny;
	apt_int nz = gridinfo.nz;
	apt_int nx1 = gridinfo.nx - 1;
	apt_int ny1 = gridinfo.ny - 1;
	apt_int nz1 = gridinfo.nz - 1;
	size_t nxx = (size_t) gridinfo.nx;
	size_t nyy = (size_t) gridinfo.ny;
	//size_t nzz = (size_t) gridinfo.nz;
	size_t nxxyy = nxx * nyy;

	cout << "Computing 3D gradient of the scalar field ..." << "\n";

	for( apt_int z = 0; z < nz; z++ ) { //x = 0, y, z
		size_t zz = (size_t) z;
		for( apt_int y = 0; y < ny; y++ ) {
			size_t yy = (size_t) y;
			for( apt_int x = 0; x < nx; x++ ) { //
				size_t xx = (size_t) x;
				apt_real dcdx = MYZERO;
				apt_real dcdy = MYZERO;
				apt_real dcdz = MYZERO;
				//$\frac{\partial c}{\partial x}$
				if ( x > 0 ) { //central difference on (i,ni-1), forward difference on i = 0, backward difference on i == ni-1
					if ( x < nx1 )
						dcdx = (scalarfield[(xx+1)+yy*nxx+zz*nxxyy] - scalarfield[(xx-1)+yy*nxx+zz*nxxyy]) / two_h;
					else
						dcdx = (scalarfield[(xx+0)+yy*nxx+zz*nxxyy] - scalarfield[(xx-1)+yy*nxx+zz*nxxyy]) / one_h;
				}
				else {
					dcdx = (scalarfield[(xx+1)+yy*nxx+zz*nxxyy] - scalarfield[(xx+0)+yy*nxx+zz*nxxyy]) / one_h;
				}
				//$\frac{\partial c}{\partial y}$
				if ( y > 0 ) {
					if ( y < ny1 )
						dcdy = (scalarfield[xx+(yy+1)*nxx+zz*nxxyy] - scalarfield[xx+(yy-1)*nxx+zz*nxxyy]) / two_h;
					else
						dcdy = (scalarfield[xx+yy*nxx+zz*nxxyy] - scalarfield[xx+(yy-1)*nxx+zz*nxxyy]) / one_h;
				}
				else {
					dcdy = (scalarfield[xx+(yy+1)*nxx+zz*nxxyy] - scalarfield[xx+yy*nxx+zz*nxxyy]) / one_h;
				}
				//$\frac{\partial c}{\partial z}$
				if ( z > 0 ) {
					if ( z < nz1 )
						dcdz = (scalarfield[xx+yy*nxx+(zz+1)*nxxyy] - scalarfield[xx+yy*nxx+(zz-1)*nxxyy]) / two_h;
					else
						dcdz = (scalarfield[xx+yy*nxx+zz*nxxyy] - scalarfield[xx+yy*nxx+(zz-1)*nxxyy]) / one_h;
				}
				else {
					dcdz = (scalarfield[xx+yy*nxx+(zz+1)*nxxyy] - scalarfield[xx+yy*nxx+zz*nxxyy]) / one_h;
				}
				//##MK::some unnecessary ifs here, could be optimized (see the comments above-mentioned) here only for clarity what we compute

				scalarfield_grad3d[0+3*(xx+yy*nxx+zz*nxxyy)] = dcdx;
				scalarfield_grad3d[1+3*(xx+yy*nxx+zz*nxxyy)] = dcdy;
				scalarfield_grad3d[2+3*(xx+yy*nxx+zz*nxxyy)] = dcdz;
			}
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "ComputeGradient3dDelocTask"+to_string(info.dlzid), APT_XX, APT_IS_SEQ, mm, tic, toc);
	cout << "Computing 3D gradient of the scalar field took " << (toc-tic) << " s" << "\n";
	return true;
}


bool isosurfaceHdl::build_iso_surface()
{
	//hand over to the Lewiner marching cubes main routine
	double tic = MPI_Wtime();

	if ( scalarfield.size() != (size_t) gridinfo.nxyz || scalarfield_edge.size() != (size_t) gridinfo.nxyz ) {
		cerr << "Required fields scalarfield and scalarfield_edge do not have the correct size!" << "\n";
		return false;
	}

	//here following Lewiner2002.h marching_cubes_jgt, main.cpp, main

	mc = MarchingCubes( gridinfo.nx, gridinfo.ny, gridinfo.nz, true );

	mc.set_ext_data( scalarfield.data() );
	//data live in MarchingCubes voxel space, voxel.ve tells how to translate to recon space again

	mc.init_all();

	mc.run( scalarfield_edge, info.isoval ) ;

	//##MK::write results to HDF5 file, works here only if we have one process, because otherwise
	//we would get concurrency for the HDF5 results file and file lock access conflicts

	//MK::marching cubes operates on a discrete sampling of a portion of the reconstruction space
	//so we need to scale and translate the vertex coordinates from integer coordinates within the
	//marching cubes voxel grid to coordinates in the reconstruction space
	//an example marching cubes knows only coordinates in multiples of pixel, i.e. scale factor is 1.0
	//so 1.4 px length represents voxel.gridcells.xmi [nm] + 1.4 px * voxel.binwidths.x [nm/px] nm in reconstruction space

	p3d mc2recon_offset = p3d( 	gridinfo.aabb.xmi + 0.5*gridinfo.dx,
								gridinfo.aabb.ymi + 0.5*gridinfo.dy,
								gridinfo.aabb.zmi + 0.5*gridinfo.dz );

	p3d mc2recon_scaler = p3d( gridinfo.dx, gridinfo.dy, gridinfo.dz );

	//marcher hands over the triangles to clusterer
	mc.get_triangles( tscl.vrts, tscl.fcts, tscl.nrms, mc2recon_offset, mc2recon_scaler );

	//no quality information available on the normals
	tscl.nrms_quality = vector<unrm_info>( tscl.nrms.size(), unrm_info() );

	mc.clean_temps();
	mc.clean_all();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "BuildIsoSurfaceDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::compute_directed_triangle_normals( const apt_real grad3dnorm )
{
	double tic = MPI_Wtime();

	//we would like to compute specifically oriented triangle normal vectors
	//here make them point along the positive direction of the 3D gradient of the underlying scalar field
	//but computing the gradient of a arbitrary scalar field has no guarantee that the vector is of substantial magnitude
	//so we use a threshold parameter telling for which triangles we can find a guiding voxel (that is a voxel with substantial normal information)
	//for flipping the normal accordingly, all other triangle normals from the MC are resetted to a very small length indicating no normal information is available

	//size_t TrianglesTotal = 0;
	size_t TrianglesWithGuidingVoxel = 0;
	size_t TrianglesWithLargeEnoughGradient = 0;
	size_t TrianglesWithLargeEnoughCrossProduct = 0;

	for ( size_t triID = 0; triID < tscl.fcts.size(); triID++ ) { //inspect each triangle
		//reset eventual old information on normals, representing also when we have no qualifying normal information
		tscl.nrms[triID] = p3d( EPSILON, EPSILON, EPSILON ); //flag values telling no useful normal information available
		//do not use MYZERO, MYZERO, MYZERO so that the vector can still be rendered with e.g. paraview

		tscl.nrms_quality[triID].SQRmagn = RMX; //flag value specifying an unknown quality

		//inspect relative location of triangle wrt to the underlying discretized scalar field on the delocalization grid
		tri3u thisone = tscl.fcts[triID];
		tri3d me = tri3d(
				tscl.vrts[thisone.v1].x, tscl.vrts[thisone.v1].y, tscl.vrts[thisone.v1].z,
				tscl.vrts[thisone.v2].x, tscl.vrts[thisone.v2].y, tscl.vrts[thisone.v2].z,
				tscl.vrts[thisone.v3].x, tscl.vrts[thisone.v3].y, tscl.vrts[thisone.v3].z );

		//find relevant voxel of the delocalization grid that could guide us during the eventual
		//flipping of the triangle normal into the direction of the scalar field gradient
		apt_real xmi = fmin( fmin(me.x1, me.x2), me.x3 ) - gridinfo.dx - EPSILON;
		apt_real xmx = fmax( fmax( me.x1, me.x2 ), me.x3 ) + gridinfo.dx + EPSILON;
		apt_real ymi = fmin( fmin(me.y1, me.y2), me.y3 ) - gridinfo.dy - EPSILON;
		apt_real ymx = fmax( fmax( me.y1, me.y2 ), me.y3 ) + gridinfo.dy + EPSILON;
		apt_real zmi = fmin( fmin(me.z1, me.z2), me.z3 ) - gridinfo.dz - EPSILON;
		apt_real zmx = fmax( fmax( me.z1, me.z2 ), me.z3 ) + gridinfo.dz + EPSILON;

		pair<apt_int, apt_int> xsxe = pair<apt_int, apt_int>(0, -1); //i start, i end (one past the end index)
		for( apt_int xs = 0; xs < gridinfo.nx;     ) { //voxel imx end vs triangle bounding box xmi
			if ( gridinfo.aabb.xmi + (((apt_real) (xs+1))*gridinfo.dx) < xmi ) { xs++; }
			else { xsxe.first = xs; break; }
		}
		for( apt_int xe = xsxe.first; xe < gridinfo.nx;    ) { //voxel imi start vs triangle bounding box xmx
			if ( gridinfo.aabb.xmi + (((apt_real) xe)*gridinfo.dx) < xmx ) { xe++; }
			else { xsxe.second = xe; break; }
		}
		pair<apt_int, apt_int> ysye = pair<apt_int, apt_int>(0, -1);
		for( apt_int ys = 0; ys < gridinfo.ny;     ) {
			if ( gridinfo.aabb.ymi + (((apt_real) (ys+1))*gridinfo.dy) < ymi ) { ys++; }
			else { ysye.first = ys; break; }
		}
		for( apt_int ye = ysye.first; ye < gridinfo.ny;    ) {
			if ( gridinfo.aabb.ymi + (((apt_real) ye)*gridinfo.dy) < ymx ) { ye++; }
			else { ysye.second = ye; break; }
		}
		pair<apt_int, apt_int> zsze = pair<apt_int, apt_int>(0, -1);
		for( apt_int zs = 0; zs < gridinfo.nz;     ) {
			if ( gridinfo.aabb.zmi + (((apt_real) (zs+1))*gridinfo.dz) < zmi ) { zs++; }
			else { zsze.first = zs; break; }
		}
		for( apt_int ze = zsze.first; ze < gridinfo.nz;    ) {
			if ( gridinfo.aabb.zmi + (((apt_real) ze)*gridinfo.dz) < zmx ) { ze++; }
			else { zsze.second = ze; break; }
		}

		//for all candidate boxes find shortest distance between center of mass of box and center of mass of triangle
		p3d t = me.barycenter();
		apt_real guiding_voxel_dsqr = RMX;
		p3i guiding_voxel_xyz = p3i();
		bool guiding_voxel_found = false;
		for( apt_int z = zsze.first; z < zsze.second; z++ ) {
			for( apt_int y = ysye.first; y < ysye.second; y++ ) {
				for( apt_int x = xsxe.first; x < xsxe.second; x++ ) {
					p3d b = p3d(
							gridinfo.aabb.xmi + (0.5 + (apt_real) x)*gridinfo.dx,
							gridinfo.aabb.ymi + (0.5 + (apt_real) y)*gridinfo.dy,
							gridinfo.aabb.zmi + (0.5 + (apt_real) z)*gridinfo.dz );
					apt_real current_voxel_dsqr = SQR(t.x - b.x) + SQR(t.y - b.y) + SQR(t.z - b.z);
					if ( current_voxel_dsqr <= guiding_voxel_dsqr ) {
						guiding_voxel_dsqr = current_voxel_dsqr;
						guiding_voxel_xyz = p3i( x, y, z );
						guiding_voxel_found = true;
					}
				}
			}
		}

		//guide the process of using the gradient vector of the field to decide what is a consistent orientation of the normal
		apt_real decisive_grad3dnorm = MYZERO;
		if ( guiding_voxel_found == true ) {
			TrianglesWithGuidingVoxel++;

			size_t xyzoff = (size_t) guiding_voxel_xyz.x + (size_t) guiding_voxel_xyz.y * (size_t) gridinfo.nx + (size_t) guiding_voxel_xyz.z * (size_t) gridinfo.nxy;
			//cout << "xyz " << guiding_voxel_xyz << "\n";
			//cout << "xyzoff " << xyzoff << "\n";
			//cout << "gridinfo.nxyz " << gridinfo.nxyz << "\n";
			//cout << "scalarfield_grad3d.size() " << scalarfield_grad3d.size() << "\n";
			p3d nabla = p3d( scalarfield_grad3d[0+3*xyzoff], scalarfield_grad3d[1+3*xyzoff], scalarfield_grad3d[2+3*xyzoff] );
			decisive_grad3dnorm = SQR(nabla.x) + SQR(nabla.y) + SQR(nabla.z);
			if ( decisive_grad3dnorm >= SQR(grad3dnorm) ) {
				//normalize nabla vector
				if ( decisive_grad3dnorm >= EPSILON ) {
					TrianglesWithLargeEnoughGradient++;

					apt_real nrm = 1. / sqrt(decisive_grad3dnorm);
					p3d guiding_normal = p3d( nrm*nabla.x, nrm*nabla.y, nrm*nabla.z );

					//compute right-hand side normal vector to the triangle
					p3d t2t1 = p3d( me.x2-me.x1, me.y2-me.y1, me.z2-me.z1 );
					p3d t3t1 = p3d( me.x3-me.x1, me.y3-me.y1, me.z3-me.z1 );
					p3d crs = cross( t3t1, t2t1 );
					apt_real lensqr = SQR(crs.x)+SQR(crs.y)+SQR(crs.z);
					if ( lensqr >= EPSILON ) {
						TrianglesWithLargeEnoughCrossProduct++;

						nrm = 1. / sqrt(lensqr);
						p3d dangling_normal = p3d( nrm*crs.x, nrm*crs.y, nrm*crs.z ); //the dangling normal may point arbitrarily

						//the dangling normal and the guiding normal are now unit vectors compute cos(angle between them)
						apt_real cos_phi = dot( guiding_normal, dangling_normal );
						//cos_phi is a quality measure how inclined the vectors are, i.e. how "robust" the dot product evaluation enabled us to flip the normal

						tscl.nrms_quality[triID] = unrm_info( decisive_grad3dnorm, lensqr, cos_phi );
						if ( cos_phi < MYZERO ) {
							//guiding_normal and dangling_normal point in opposite directions, so flip the dangling_normal
							//by virtue of construction now the flipped dangling_normal is the normal for the triangle
							//and pointing as desired into the direction of the negative gradient vector of the guiding field
							tscl.nrms[triID] = p3d( dangling_normal.x, dangling_normal.y, dangling_normal.z );
						}
						else { //normals point in the same half space direction
							tscl.nrms[triID] = p3d( -1.*dangling_normal.x, -1.*dangling_normal.y, -1.*dangling_normal.z );
						}
					}
					//else case "Trying to normalize a very short cross product triangle plane vector is numerically not stable !" << "\n"; //continue; //and leave the normal uncomputed eps,eps,eps
				}
				//else case "Trying to normalize a very short vector is numerically not stable !" << "\n"; //continue; //and leave the normal uncomputed eps,eps,eps
			}
			//else case "Gradient information is not large enough to guide the normal directing so mark that we have no qualified normal information for the triangle!" << "\n";
		}
		/*
		else {
			cerr << "No guiding voxel found for a voxel, likely because the triangle was so small that it ended up in a single voxel completely !" << "\n";
		}
		*/
	} //next triangle

	cout << "TrianglesTotal " << tscl.fcts.size() << "\n";
	cout << "TrianglesWithGuidingVoxel " << TrianglesWithGuidingVoxel << "\n";
	cout << "TrianglesWithLargeEnoughGradient " << TrianglesWithLargeEnoughGradient << "\n";
	cout << "TrianglesWithLargeEnoughCrossProduct " << TrianglesWithLargeEnoughCrossProduct << "\n";
	if ( tscl.fcts.size() >= 1 ) {
		cout << "Triangles above minimum gradient threshold " << (double) TrianglesWithLargeEnoughGradient / (double) tscl.fcts.size() << "\n";
		cout << "Triangles above minimum quality threshold " << (double) TrianglesWithLargeEnoughCrossProduct / (double) tscl.fcts.size() << "\n";
	}
	if ( TrianglesWithGuidingVoxel < tscl.fcts.size() ) {
		cerr << "WARNING::There were triangle facets for which no guiding voxel was found." << "\n";
		cerr << "WARNING::A consequence of this is that the normals for these triangles cannot safely be computed right now" << "\n";
		cerr << "WARNING::as it requires a propagation of normal information from neighboring triangles." << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "ComputeDirectedTriangleNormalsDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::approximate_unclear_triangle_normals( const apt_real eps )
{
	double tic = MPI_Wtime();

	//for all those triangles whose normals were not directly identifiable i.e. nrms_quality.SQRmagn >= MYTWO we use (if existent) the ring of first order adjacent triangles
	//and take the average normal of these, all other triangles can currently not be processed
	cout << "Starting triangle facet circulator to approximate facet normals based on neighboring normals..." << "\n";

	//builds an AABBTree / RTree of triangles with respect to the global space the triangles occupy in $\mathcal{R}^3$
	if ( tscl.fcts.size() < 1 ) {
		cout << "approximate_unclear_triangle_normals::build_triangles_rtree thread " << omp_get_thread_num() << " there are no fcts to build an RTree for !" << "\n";
		return true;
	}
	if ( tscl.fcts.size() >= (size_t) IMX ) {
		cerr << "approximate_unclear_triangle_normals::build_triangles_rtree thread " << omp_get_thread_num() << " currently at most " << IMX-1 << " triangles are supported !" << "\n";
		return false;
	}

	Tree* aabb_tree = NULL;
	try {
		aabb_tree = new class Tree( tscl.fcts.size() );
	}
	catch (bad_alloc &trisoupexc) {
		cerr << "approximate_unclear_triangle_normals::build_triangles_rtree thread " << omp_get_thread_num() << " unable to allocate memory for the rtree !" << "\n";
		return false;
	}

	size_t TrianglesNearestNeighborProcessed = 0;
	size_t TrianglesNearestNeighborRecoverable = 0;

	apt_real xmi = RMX;
	apt_real xmx = RMI;
	apt_real ymi = RMX;
	apt_real ymx = RMI;
	apt_real zmi = RMX;
	apt_real zmx = RMI;
	apt_uint triID = 0;
	for ( auto it = tscl.fcts.begin(); it != tscl.fcts.end(); it++ ) {
		//##MK::fattening to assure that edge touching of boxes results in inclusion tests
		xmi = fmin( fmin(tscl.vrts[it->v1].x, tscl.vrts[it->v2].x), tscl.vrts[it->v3].x );
		xmx = fmax( fmax(tscl.vrts[it->v1].x, tscl.vrts[it->v2].x), tscl.vrts[it->v3].x );
		ymi = fmin( fmin(tscl.vrts[it->v1].y, tscl.vrts[it->v2].y), tscl.vrts[it->v3].y );
		ymx = fmax( fmax(tscl.vrts[it->v1].y, tscl.vrts[it->v2].y), tscl.vrts[it->v3].y );
		zmi = fmin( fmin(tscl.vrts[it->v1].z, tscl.vrts[it->v2].z), tscl.vrts[it->v3].z );
		zmx = fmax( fmax(tscl.vrts[it->v1].z, tscl.vrts[it->v2].z), tscl.vrts[it->v3].z );

		aabb_tree->insertParticle( triID,
				trpl(xmi - EPSILON, ymi - EPSILON, zmi - EPSILON),
				trpl(xmx + EPSILON, ymx + EPSILON, zmx + EPSILON) );
		triID++;
	}

	cout << "triangleSoupClustering::build_triangles_rtree thread " << omp_get_thread_num() << " rtree built" << "\n";

	for ( size_t triID = 0; triID < tscl.fcts.size(); triID++ ) { //inspect each triangle
		if ( tscl.nrms_quality[triID].SQRmagn < 2. * MYONE ) {
			continue;
		}
		else {
			TrianglesNearestNeighborProcessed++;

			apt_real R = eps; //ConfigNanochem::TriSoupClusteringMaxDistance;
			p3d normal_sum = p3d();
			apt_uint n_nbors = 0;
			tri3u thisone = tscl.fcts[triID];
			tri3d me = tri3d(
					tscl.vrts[thisone.v1].x, tscl.vrts[thisone.v1].y, tscl.vrts[thisone.v1].z,
					tscl.vrts[thisone.v2].x, tscl.vrts[thisone.v2].y, tscl.vrts[thisone.v2].z,
					tscl.vrts[thisone.v3].x, tscl.vrts[thisone.v3].y, tscl.vrts[thisone.v3].z );

			//find potential neighboring triangles, make a coarse query first using cuboids with diagonals larger than R/2 adds a little bit too many tests
			apt_real xmi = fmin( fmin(me.x1, me.x2), me.x3 );
			apt_real xmx = fmax( fmax( me.x1, me.x2 ), me.x3 );
			apt_real ymi = fmin( fmin(me.y1, me.y2), me.y3 );
			apt_real ymx = fmax( fmax( me.y1, me.y2 ), me.y3 );
			apt_real zmi = fmin( fmin(me.z1, me.z2), me.z3 );
			apt_real zmx = fmax( fmax( me.z1, me.z2 ), me.z3 );
			hedgesAABB e_aabb( 	trpl( xmi - R - EPSILON, ymi - R - EPSILON, zmi - R - EPSILON),
								trpl( xmx + R + EPSILON, ymx + R + EPSILON, zmx + R + EPSILON) );

			vector<apt_uint> nbids = aabb_tree->query( e_aabb );
			//identify what are the corresponding triangles to check the minimum analytical distance for to check if they are really within R
			//replace this with a half-edge circulator graph walker which would be much more efficient
			for( auto nbt = nbids.begin(); nbt != nbids.end(); nbt++ ) {
				if ( *nbt != triID ) {
					if ( tscl.nrms_quality[*nbt].SQRmagn < 2. * MYONE ) {
						tri3u nbor = tscl.fcts[*nbt];
						if ( thisone.v1 != nbor.v1 && thisone.v1 != nbor.v2 && thisone.v1 != nbor.v3 &&
								thisone.v2 != nbor.v1 && thisone.v2 != nbor.v2 && thisone.v2 != nbor.v3 &&
									thisone.v3 != nbor.v1 && thisone.v3 != nbor.v2 && thisone.v3 != nbor.v3 ) {
							continue;
						}
						else {
							//me and nbor share a vertex but as nbor is not me we found an adjacent triangle
							normal_sum.x += tscl.nrms[*nbt].x;
							normal_sum.y += tscl.nrms[*nbt].y;
							normal_sum.z += tscl.nrms[*nbt].z;
							n_nbors++;
						}
					}
				}
			}
			if ( n_nbors >= 1 ) {
				apt_real multiplier = 1.f / (apt_real) n_nbors;
				normal_sum.x *= multiplier;
				normal_sum.y *= multiplier;
				normal_sum.z *= multiplier;
				apt_real lensqr = SQR(normal_sum.x)+SQR(normal_sum.y)+SQR(normal_sum.z);
				if ( lensqr >= EPSILON ) {
					apt_real nrm = 1. / sqrt(lensqr);
					tscl.nrms[triID] = p3d( nrm * normal_sum.x, nrm * normal_sum.y, nrm * normal_sum.z );
					tscl.nrms_quality[triID] = unrm_info( MYZERO, lensqr, RMX );
					TrianglesNearestNeighborRecoverable++;
				}
			}
			/*
			else {
				cout << "Signed normal vector of unclear IsoSurface triangle facet " << triID << " is not recoverable, n_nbors " << n_nbors << "\n";
			}
			*/

			if ( triID % 100000 == 0 ) {
				cout << "Processing " << triID << "\n";
			}
		}
	}
	cout << "TrianglesNearestNeighborProcessed " << TrianglesNearestNeighborProcessed << "\n";
	cout << "TrianglesNearestNeighborRecoverable " << TrianglesNearestNeighborRecoverable << "\n";

	delete aabb_tree; aabb_tree = NULL;

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "ApproximateUnclearTriangleNormalsDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);
	cout << "approximate_unclear_triangle_normals took " << (toc-tic) << " seconds" << "\n";
	return true;
}


void isosurfaceHdl::check_closure_iso_surface()
{
	pair<double, int> volume_and_closure_state = pair<double, int>( MYZERO, TRIANGLE_SOUP_ANALYSIS_NO_SUPPORT );
	cout << "Performing a water-tightness check for the entire iso-surface complex" << "\n";

	volume_and_closure_state = check_if_closed_polyhedron( tscl.fcts, tscl.vrts );
	cout << "Double " << volume_and_closure_state.first << " Int " << volume_and_closure_state.second << "\n";
}


bool isosurfaceHdl::find_connected_regions( const apt_real dminmax )
{
	double tic = MPI_Wtime();

	if ( tscl.clustering_triangle_distance_based( dminmax ) != UMX ) {

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
		isosurf_tictoc.prof_elpsdtime_and_mem( "FindConnectedRegionsDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

		return true;
	}

	return false;
}


bool isosurfaceHdl::find_closed_polyhedra_and_proxies_for_objects_with_holes(
		vector<p3d> const & pp3, vector<p3dinfo> const & ityps, const size_t nityps )
{
	//hand over results from triangle soup clusterer to closed oriented 2-manifold processor
	//link in CGAL
	//manifoldHdl
	double tic = MPI_Wtime();

	objs = vector<manifoldHdl*>( tscl.nobjects + 2, NULL );

#ifdef I_ASSUME_CGAL_PMP_IS_THREADSAFE
	#pragma omp parallel
	{
		vector<pair<apt_uint, manifoldHdl*>> myres; //thread-local results

		//workload is per object, large differences
		#pragma omp for schedule(dynamic,1) nowait
#endif
		for( apt_uint objID = 1; objID <= tscl.nobjects; objID++ ) {
			//nobjects is a valid object ID
			//DO NOT INCLUDE objID == 0, the zero cluster, that is triangles without neighbors
			//thread-local calling of CGAL library functionalities to check if triangle soup portion/cluster can be
			//converted in a (closed) oriented 2-manifold, polyhedron
			//	because then we can safely define an inner and outer unit normal vector
			//	and only in this case of such consistently signed normals computing a proxigram is acceptable

			manifoldHdl* soup_to_manifold = NULL;
			soup_to_manifold = new manifoldHdl;

			if ( soup_to_manifold != NULL ) {
				soup_to_manifold->info.obj_id = objID;

				soup_to_manifold->fetch_triangles( objID, tscl );

				//use the CGAL library to identify what we can build from this triangle soup, hopefully a closed polyhedron
				//MK::most isosurfaces do not represent a closed polyhedron, take for instance a simple triangulated grain boundary plane
				//the implication is significant because without a clear substantiation that the triangle soup does represent a closed polyhedron
				//we cannot logically assign a physically meaningful sign to the local unit normals to the triangles
				//and hence computing proximity diagrams, i.e. signed distances to the interface is an ill-posed task!
				//this can be an equally ill-posed task when trying to build consistent signed gradients for any arbitrary scalar field
				//take for instance a simple dirac source, the "water from such source flows in all directions, what is a sensible inside outside then?"
				//each direction from the source is equally valid as such a source emits isotropically

				//##MK::MAKE SURE TO DEFINE CGAL_HAS_THREADS such that CGAL uses thread-safety functions

				int what_am_i = soup_to_manifold->characterize_triangle_soup_convertability_to_mesh();

				if ( what_am_i == TRIANGLE_SOUP_ANALYSIS_POLYHEDRON_CLOSED ) {

					soup_to_manifold->characterize_closed_polyhedron_mesh();

					soup_to_manifold->characterize_closed_polyhedron_volume();

					if ( info.hasObjectsProps == true ) {

						if ( info.hasObjectsOBB == true ) {

							soup_to_manifold->characterize_closed_polyhedron_obb();
						}

						/*
						soup_to_manifold->characterize_minimum_ellipsoid();
						*/

						//##MK::compute a convex hull
					}
				}
				else if ( what_am_i == TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH ) { //irrespective of whether this patch has a proxy or not

					soup_to_manifold->characterize_surface_patch();

					soup_to_manifold->characterize_surface_patch_proxy_mesh();

					soup_to_manifold->characterize_proxy_volume();

					//##MK::eventually compute volume of bounding boxes of the proxy
					//however, these objects are created in most cases from objects which were
					//clipped by the edge of the dataset and should thus not be analyzed as
					//they will bias descriptors...

					if ( info.hasProxiesProps == true ) {

						if ( info.hasProxiesOBB == true ) {

							soup_to_manifold->characterize_proxy_obb();
						}
					}
				}
				else {
					//nothing to do
				}
			}

			//register the object in the storage of the isosurfaceHdl
			//if ( soup_to_manifold->ifo.obj_id != 0 ) { //do not include the always non-existent zero-cluster
#ifdef I_ASSUME_CGAL_PMP_IS_THREADSAFE
			myres.push_back( pair<apt_uint, manifoldHdl*>(objID, soup_to_manifold) );
#else
			objs[objID] = soup_to_manifold;
#endif
		}

#ifdef I_ASSUME_CGAL_PMP_IS_THREADSAFE
		//fuse myres to global results
		#pragma omp critical
		{
			for( size_t ii = 0; ii < myres.size(); ii++ ) {
				objs[myres[ii].first] = myres[ii].second;
			}
		}
	} //end of parallel region
#endif

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "FindClosedPolyhedraAndProxiesDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::build_closed_polyhedra_bvh()
{
	double tic = omp_get_wtime();

	try {
		closed_polyhedra_bvh = new class Tree();
	}
	catch (bad_alloc &trisoupexc) {
		cerr << "Thread " << omp_get_thread_num() << " unable to allocate memory for the closed_polyhedra_bvh !" << "\n";
		return false;
	}

	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			if ( objs[i]->info.is_tessellated == true ) {
				if ( objs[i]->info.is_valid_aabb == true ) {

					//##MK::fattening to assure that edge touching of boxes results in inclusion tests
					closed_polyhedra_bvh->insertParticle( i,
							trpl(	objs[i]->info.aabb.xmi - EPSILON,
									objs[i]->info.aabb.ymi - EPSILON,
									objs[i]->info.aabb.zmi - EPSILON  ),
							trpl(	objs[i]->info.aabb.xmx + EPSILON,
									objs[i]->info.aabb.ymx + EPSILON,
									objs[i]->info.aabb.zmx + EPSILON  ) );
					//we can use i as the particle ID because hedgesAABB uses a map<particle, node>
				}
			}
		}
	}

	double toc = omp_get_wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "BuildClosedPolyhedraDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::find_ions_in_closed_polyhedra_npoly_times_log_nions_no_tetrahedralization_multithreading(
		vector<unsigned char> const & wdw, vector<p3d> const & pp3,
			vector<p3dinfo> const & ityps, rangeTable & rng, LinearTimeVolQuerier<p3dmidx> & ionbvh,
				vector<tri3d> const & edgemesh, const bool check_for_edge_contact, const apt_real edge_contact_dist_threshold )
{
	//MK::we must not thread-parallelize the following calls to TetGen, because TetGen is not thread-safe !!!

	double tic = MPI_Wtime();

	double ctic = MPI_Wtime();
	double ctoc = 0.0;

	cout << "Calculating which ions are inside which object" << "\n";
	cout << "We detected eventual truncation of object by the edge of the dataset via evaluating the closest ion to the edge" << "\n";
	cout << "The threshold distance used is " << edge_contact_dist_threshold << " nm" << "\n";

	double Vdiscr = (double) gridinfo.nxyz;
	Vdiscr *= (double) gridinfo.dx;
	Vdiscr *= (double) gridinfo.dy;
	Vdiscr *= (double) gridinfo.dz;
	cout << "Vdiscretized about the reconstructed volume " << Vdiscr << " nm^3" << "\n";
	if ( Vdiscr < EPSILON_F64 ) {
		cerr << "Vdiscr needs to be at least as large as EPSILON_F64 !" << "\n";
		return false;
	}

	vector<size_t> small_objects;
	vector<size_t> large_objects;
	//we found for many datasets that they have a considerable number of objects which make contact with the edge of the dataset
	//so we better filter these objects out as their shape and properties are biased because of arbitrary truncation by the edge of the dataset
	//recall that one practical reason for having AMETEK/Cameca Invizo 6000 instruments is to get exactly more field of view i.e.
	//reduce the fraction of objects in contact with the edge of the dataset
	//it would not make sense to identify first costly which ions are in there to then find that this object should anyway not be considered
	//a compromise is necessary because for a very large object it might be necessary to test many triangle-to-triangle distances, which
	//itself can be as costly as performing the point-in-object inclusion, anyway there is a price we have to pay
	//for an arbitrary collection of 3D objects embedded in an arbitrary dataset one has to invest time to find which objects
	//make contact with the edge of the dataset and which objects do not

	//##MK::remove the num_threads debug statement
	#pragma omp parallel shared(small_objects, large_objects) num_threads(1)
	{
		vector<size_t> mysmall;
		vector<size_t> mylarge;

		//thread-local edge_mesh and BVH of these triangles, triangle tree
		vector<tri3d> myedgetris = edgemesh;

		Tree* my_edge_tris_in_a_bvh = NULL;

		if ( myedgetris.size() > 0 ) { //only when there is a mesh
			my_edge_tris_in_a_bvh = new class Tree();

			for( size_t k = 0; k < myedgetris.size(); k++ ) {
				my_edge_tris_in_a_bvh->insertParticle( k,
					trpl( 	fmin(fmin(myedgetris[k].x1, myedgetris[k].x2), myedgetris[k].x3) - EPSILON,
							fmin(fmin(myedgetris[k].y1, myedgetris[k].y2), myedgetris[k].y3) - EPSILON,
							fmin(fmin(myedgetris[k].z1, myedgetris[k].z2), myedgetris[k].z3) - EPSILON  ),
					trpl(  	fmax(fmax(myedgetris[k].x1, myedgetris[k].x2), myedgetris[k].x3) + EPSILON,
							fmax(fmax(myedgetris[k].y1, myedgetris[k].y2), myedgetris[k].y3) + EPSILON,
							fmax(fmax(myedgetris[k].z1, myedgetris[k].z2), myedgetris[k].z3) + EPSILON  ) );
				//we can use k as the particle ID because hedgesAABB uses a map<particle, node>
			}
		}

		/*
		#pragma omp critical
		{
			cout << "Thread " << omp_get_thread_num() << " myedgetris.size() " << myedgetris.size() << " my_edge_tris_in_a_bvh->getNodeCount() " << my_edge_tris_in_a_bvh->getNodeCount() << "\n";
		}
		*/

		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < objs.size(); i++ ) {
			if ( objs[i] != NULL ) {
				if ( objs[i]->info.obj_typ == OBJECT_IS_POLYHEDRON ) {
					//MK::overwriting settings only relevant when tetrahedralizing
					objs[i]->info.is_tessellated = true;
					objs[i]->info.is_valid_volume = true;

					objs[i]->build_polyhedron_aabb();

					if ( objs[i]->info.is_valid_aabb == true ) {

						objs[i]->check_if_interior_object( edge_contact_dist_threshold, myedgetris, my_edge_tris_in_a_bvh );

						aabb3d obj_in_a_box = objs[i]->info.aabb;
						obj_in_a_box.scale();

						double Vobj = (double) (obj_in_a_box.xsz * obj_in_a_box.ysz * obj_in_a_box.zsz);
						if ( Vobj / Vdiscr < ConfigNanochem::VolFracQualifyingLargeObjects ) { //normally, most objects are small
							mysmall.push_back(i);
						}
						else {
							//for a large object, solving it alone would eventually force other threads to wait after them having already finished their work
							mylarge.push_back(i);
						}
					} //is valid aabb
				} //is valid polyhedron
			} //object exists
		} //next object

		//release the thread-local edge_mesh and corresponding BVH, these support data structures where only required to check if an object is close to the edge of the dataset
		myedgetris = vector<tri3d>();
		if ( my_edge_tris_in_a_bvh != NULL ) {
			delete my_edge_tris_in_a_bvh; my_edge_tris_in_a_bvh = NULL;
		}

		//threads fuse local results, no waiting while others are still processing, not problematic other threads dont access small_, large_objects in between
		#pragma omp critical
		{
			small_objects.insert( small_objects.end(), mysmall.begin(), mysmall.end() );
			large_objects.insert( large_objects.end(), mylarge.begin(), mylarge.end() );
		}

		//MK::necessary barrier, otherwise threads see different view of shared arrays small_, large_objects
		#pragma omp barrier

		//thread team processes small objects, each thread a different small object
		#pragma omp for schedule(dynamic,1)
		for( size_t ii = 0; ii < small_objects.size(); ii++ ) {
			size_t i = small_objects[ii];

			/*//find ions in some reasonable proximity to the object that we are querying
			//O(N), the brute force way, check every ion in the analysis window of the dataset for whether it is in obj_in_a_box
			vector<p3dmidx> cand_ions;
			for( size_t ionidx = 0; ionidx < pp3.size(); ionidx++ ) {
				if ( wdw[ionidx] != WINDOW_ION_EXCLUDE ) {
					if ( obj_in_a_box.is_inside( pp3[ionidx] ) == false ) { continue; }
					else {
						bool interior_point = ( ityps[ionidx].dist >= edge_contact_dist_threshold ) ? true : false;
						cand_ions.push_back( p3dmidx(pp3[ionidx].x, pp3[ionidx].y, pp3[ionidx].z, (apt_uint) ionidx, ityps[ionidx].i_org, interior_point) ); //(apt_uint) ityps[ionidx].i_org, (apt_uint) ionidx) );
					}
				}
			}*/

			aabb3d obj_in_a_box = objs[i]->info.aabb;
			obj_in_a_box.scale();

			//O(k), the linear time bvh querying way, import only those ions in voxels that overlap with the obj_in_a_box
			vector<p3dmidx> cand_ions;
			ionbvh.query_noclear_nosort( obj_in_a_box, cand_ions );

			//for a small object thread solves it alone directly
			bool mystatus = objs[i]->characterize_ions_cgal_point_in_polyhedron_sequential( cand_ions );
			if ( mystatus == false ) {
				#pragma omp critical
				{
					cerr << "characterize_ions_cgal_point_in_polyhedron_sequential object " << i << " had issues!" << "\n";
				}
			}
		}

	} //end of parallel region, explicit barrier included

	//large_object list is shared, so seen by every thread and processed in the same sequence
	for( size_t jj = 0; jj < large_objects.size(); jj++ ) {
		size_t i = large_objects[jj];

		aabb3d obj_in_a_box = objs[i]->info.aabb;
		obj_in_a_box.scale();

		//O(k), the linear time bvh querying way, import only those ions in voxels that overlap with the obj_in_a_box
		vector<p3dmidx> cand_ions;
		ionbvh.query_noclear_nosort( obj_in_a_box, cand_ions );

		//one parallel region opened to sync thread team, quite some overhead, therefore only for the large objects
		bool mystatus = objs[i]->characterize_ions_cgal_point_in_polyhedron_multithreaded( cand_ions );
		if ( mystatus == false ) {
			#pragma omp critical
			{
				cerr << "characterize_ions_cgal_point_in_polyhedron_multithreaded " << i << " had issues!" << "\n";
			}
		}
	}

	//ctoc = omp_get_wtime();
	//cout << "Thread team " << (ctoc-ctic) << " seconds spent for processing objects" << "\n";
	//ctic = omp_get_wtime();

	//perform the bookkeeping for the decomposed ion types
	vector<size_t> relevant_objects;
	relevant_objects = small_objects;
	relevant_objects.insert( relevant_objects.end(), large_objects.begin(), large_objects.end() );
	small_objects = vector<size_t>();
	large_objects = vector<size_t>();

	for( size_t ii = 0; ii < relevant_objects.size(); ii++ ) {
		size_t i = relevant_objects[ii];

		//apt_uint this_obj_idx = objs[i]->info.obj_id;
		//instantiate the bookkeeping dictionary for this object and its element type counts
//		objs[i]->info.elem_cnts = map<unsigned char, apt_uint>();
//cout << this_obj_idx << " before instantiation elemcnts" << "\n";
//		for( auto kt = rng.disjoint_elements.begin(); kt != rng.disjoint_elements.end(); kt++ ) {
//			objs[i]->info.elem_cnts[*kt] = 0; //.insert( pair<unsigned char, apt_uint>(*kt, 0) );
//		}

//cout << this_obj_idx << "\n";

		//scan all iontypes for the object and multiply count of (molecular) ions with respective multiplicity
//		for( auto it = objs[i]->info.ion_cnts.begin(); it != objs[i]->info.ion_cnts.end(); it++ ) {
		for( auto it = objs[i]->myres_iontypes.begin(); it != objs[i]->myres_iontypes.end(); it++ ) {
			//for every accounted for ion find the respective multiplicator
			unsigned char ityp = (unsigned char) it->first;
			apt_uint cnts = it->second;
//cout << this_obj_idx << " cnts " << cnts << "\n";
			if ( cnts > 0 ) {
				map<unsigned char, vector<apt_uint>>::iterator thisone = rng.ityp2mltply.find( ityp );
				if ( thisone != rng.ityp2mltply.end() ) { //we found a multiplicator vector
					size_t ii = 0;
					for( auto lt = rng.disjoint_elements.begin(); lt != rng.disjoint_elements.end(); lt++, ii++ ) {
						//objs[i]->info.elem_cnts[*lt] += (cnts * thisone->second.at(ii));
						apt_uint etyp = *lt;
						map<apt_uint,apt_uint>::iterator here = objs[i]->myres_elemtypes.find( etyp );
						if ( here != objs[i]->myres_elemtypes.end() ) {
							objs[i]->myres_elemtypes[etyp] += (cnts * thisone->second.at(ii));
						}
						else {
							objs[i]->myres_elemtypes.insert( pair<apt_uint,apt_uint>(
									etyp, cnts * thisone->second.at(ii) ) );
						}
					}
				}
				else {
					cerr << "There is an inconsistence while bookkeeping the element counts for the decomposed iontypes" << "\n";
					return false;
				}
			}
		} //for each iontype
	} //for each relevant object

	ctoc = MPI_Wtime();
	cout << "Ion inclusion tests took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = MPI_Wtime();

	//finally, to get the same formatted ion_evap index array for each object irrespective of the number of threads used we do a multithreaded sorting of the ion_evap array
	#pragma omp parallel
	{
		#pragma omp for schedule(dynamic,1)
		for( size_t i = 0; i < objs.size(); i++ ) {
			if ( objs[i] != NULL ) {
				if ( objs[i]->info.obj_typ == OBJECT_IS_POLYHEDRON ) {
					if ( objs[i]->info.is_tessellated == true ) {
						if ( objs[i]->info.is_valid_volume == true ) {
							if ( objs[i]->info.is_valid_aabb == true ) {
								if ( objs[i]->ion_evapid.size() > 0 ) {
									sort( objs[i]->ion_evapid.begin(), objs[i]->ion_evapid.end() );
								}
							}
						}
					}
				}
			}
		}
	}

	ctoc = MPI_Wtime();
	cout << "Sorting ion_evap arrays for convenience took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = MPI_Wtime();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "FindIonsInClosedPolyhedraDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


//##MK::remove redundant code sections which are also used in the above function
bool isosurfaceHdl::find_ions_in_proxies_npoly_times_log_nions_no_tetrahedralization_multithreading(
		vector<unsigned char> const & wdw, vector<p3d> const & pp3,
			vector<p3dinfo> const & ityps, rangeTable & rng, LinearTimeVolQuerier<p3dmidx> & ionbvh,
			vector<tri3d> const & edgemesh, const bool check_for_edge_contact, const apt_real edge_contact_dist_threshold )
{
	//,	vector<tri3d> const & edgemesh, const bool check_for_edge_contact, const apt_real edge_contact_dist_threshold )
	double tic = MPI_Wtime();
	double ctic = tic;
	double ctoc = 0.0;

	cout << "Calculating which ions are inside the proxy objects" << "\n";
	//cout << "We detected eventual truncation of object by the edge of the dataset via evaluating the closest ion to the edge" << "\n";
	//cout << "The threshold distance used is " << edge_contact_dist_threshold << " nm" << "\n";

	double Vdiscr = (double) gridinfo.nxyz * ((double) gridinfo.dx*gridinfo.dy*gridinfo.dz);
	cout << "Volume of the discretized grid about the reconstructed dataset " << Vdiscr << " nm^3" << "\n";
	if ( Vdiscr < EPSILON_F64 ) {
		cerr << "Vdiscr needs to be at least as large as EPSILON_F64 !" << "\n";
		return false;
	}

	vector<size_t> small_objects;
	vector<size_t> large_objects;
	//we found that in many dataset there is a considerable number of objects making contact with the edge of the dataset
	//so we better filter these objects out as their shape and properties is anyway biased because of arbitrary truncation by the edge of the dataset
	//it would not make sense to identify first costly which ions are in there to then find that this object should anyway not be considered
	//a compromise is necessary because for a very large object it might be necessary to test many triangle-to-triangle distances, which
	//itself can be as costly as performing the point-in-object inclusion, anyway this there is a price we have to pay
	//for an arbitrary collection of 3D objects embedded in an abritrary one has to invest time to find rigorously who makes contact with the
	//boundary an who not...
	//##MK::remove the num_threads debug statement
	#pragma omp parallel shared(small_objects,large_objects) num_threads(1)
	{
		cout << "find_ions_in_proxies_npoly_times_log_nions_no_tetrahedralization_multithreading executing with " << omp_get_num_threads() << " threads" << "\n";
		vector<size_t> mysmall;
		vector<size_t> mylarge;

		//thread-local edge_mesh and BVH of these triangles, triangle tree
		vector<tri3d> myedgetris = edgemesh;

		Tree* my_edge_tris_in_a_bvh = NULL;
		if ( myedgetris.size() > 0 ) { //only when there is a mesh
			my_edge_tris_in_a_bvh = new class Tree();

			for( size_t k = 0; k < myedgetris.size(); k++ ) {
				my_edge_tris_in_a_bvh->insertParticle( k,
					trpl( 	fmin(fmin(myedgetris[k].x1, myedgetris[k].x2), myedgetris[k].x3) - EPSILON,
							fmin(fmin(myedgetris[k].y1, myedgetris[k].y2), myedgetris[k].y3) - EPSILON,
							fmin(fmin(myedgetris[k].z1, myedgetris[k].z2), myedgetris[k].z3) - EPSILON  ),
					trpl(  	fmax(fmax(myedgetris[k].x1, myedgetris[k].x2), myedgetris[k].x3) + EPSILON,
							fmax(fmax(myedgetris[k].y1, myedgetris[k].y2), myedgetris[k].y3) + EPSILON,
							fmax(fmax(myedgetris[k].z1, myedgetris[k].z2), myedgetris[k].z3) + EPSILON  ) );
				//we can use k as the particle ID because hedgesAABB uses a map<particle, node>
			}
		}

		#pragma omp for schedule(dynamic,1) nowait
		for( size_t i = 0; i < objs.size(); i++ ) {
			if ( objs[i] != NULL ) {
				if ( objs[i]->info.obj_typ == OBJECT_IS_SURFACEMESH ) {

					if ( objs[i]->info.has_valid_proxy == true ) {
						objs[i]->info.is_tessellated = true; //##MK::strictly speaking not necessary as we no longer do Tetrahedralization
						objs[i]->info.is_valid_volume = true;

						objs[i]->build_surface_patch_aabb();

						if ( objs[i]->info.is_valid_aabb == true ) {
							//##MK::proxies are usually in contact with the edge of the dataset thats their purpose - to handle incompletely captured microstructural features
							objs[i]->check_if_interior_object( edge_contact_dist_threshold, myedgetris, my_edge_tris_in_a_bvh );

							aabb3d obj_in_a_box = objs[i]->info.aabb;
							obj_in_a_box.scale();

							double Vobj = (double) (obj_in_a_box.xsz * obj_in_a_box.ysz * obj_in_a_box.zsz);
							if ( Vobj / Vdiscr < ConfigNanochem::VolFracQualifyingLargeObjects ) { //normally, most objects are small
								mysmall.push_back(i);
							}
							else {
								//for a large object, solving it alone would eventually force other threads to wait after them having already finished their work
								mylarge.push_back(i);
							}
						} //is valid aabb
					} //has a valid proxy
				} //is valid surface mesh
			} //object exists
		} //next object

		//release the thread-local edge_mesh and corresponding BVH, these support data structures where only required to check if an object is close to the edge of the dataset
		myedgetris = vector<tri3d>();
		if ( my_edge_tris_in_a_bvh != NULL ) {
			delete my_edge_tris_in_a_bvh; my_edge_tris_in_a_bvh = NULL;
		}

		//threads fuse local results, no waiting while others are still processing, not problematic other threads dont access small_, large_objects in between
		#pragma omp critical
		{
			small_objects.insert( small_objects.end(), mysmall.begin(), mysmall.end() );
			large_objects.insert( large_objects.end(), mylarge.begin(), mylarge.end() );
		}

		//MK::necessary barrier, otherwise threads see different view of shared arrays small_, large_objects
		#pragma omp barrier

		//thread team processes small objects, each thread a different small object
		#pragma omp for schedule(dynamic,1)
		for( size_t ii = 0; ii < small_objects.size(); ii++ ) {
			size_t i = small_objects[ii];

			aabb3d obj_in_a_box = objs[i]->info.aabb;
			obj_in_a_box.scale();

			//O(k), the linear time bvh querying way, import only those ions in voxels that overlap with the obj_in_a_box
			vector<p3dmidx> cand_ions;
			ionbvh.query_noclear_nosort( obj_in_a_box, cand_ions );

			//for a small object thread solves it alone directly
			bool mystatus = objs[i]->characterize_ions_cgal_point_in_proxy_sequential( cand_ions );
		}

	} //end of parallel region, explicit barrier included

	//large_object list is shared, so seen by every thread and processed in the same sequence
	for( size_t jj = 0; jj < large_objects.size(); jj++ ) {
		size_t i = large_objects[jj];

		aabb3d obj_in_a_box = objs[i]->info.aabb;
		obj_in_a_box.scale();

		//O(k), the linear time bvh querying way, import only those ions in voxels that overlap with the obj_in_a_box
		vector<p3dmidx> cand_ions;
		ionbvh.query_noclear_nosort( obj_in_a_box, cand_ions );

		//one parallel region opened to sync thread team, quite some overhead, therefore only for the large objects
		bool mystatus = objs[i]->characterize_ions_cgal_point_in_proxy_multithreaded( cand_ions );
	}

	//perform the bookkeeping for the decomposed ion types
	vector<size_t> relevant_objects;
	relevant_objects = small_objects;
	relevant_objects.insert( relevant_objects.end(), large_objects.begin(), large_objects.end() );
	small_objects = vector<size_t>();
	large_objects = vector<size_t>();

	for( size_t ii = 0; ii < relevant_objects.size(); ii++ ) {
		size_t i = relevant_objects[ii];

		//scan all iontypes for the object and multiply count of (molecular) ions with respective multiplicity
		for( auto it = objs[i]->myres_iontypes.begin(); it != objs[i]->myres_iontypes.end(); it++ ) {
			//for every accounted for ion find the respective multiplicator
			unsigned char ityp = (unsigned char) it->first;
			apt_uint cnts = it->second;
			//cout << this_obj_idx << " cnts " << cnts << "\n";
			if ( cnts > 0 ) {
				map<unsigned char, vector<apt_uint>>::iterator thisone = rng.ityp2mltply.find( ityp );
				if ( thisone != rng.ityp2mltply.end() ) { //we found a multiplicator vector
					size_t ii = 0;
					for( auto lt = rng.disjoint_elements.begin(); lt != rng.disjoint_elements.end(); lt++, ii++ ) {
						//objs[i]->info.elem_cnts[*lt] += (cnts * thisone->second.at(ii));
						apt_uint etyp = *lt;
						map<apt_uint,apt_uint>::iterator here = objs[i]->myres_elemtypes.find( etyp );
						if ( here != objs[i]->myres_elemtypes.end() ) {
							objs[i]->myres_elemtypes[etyp] += (cnts * thisone->second.at(ii));
						}
						else {
							objs[i]->myres_elemtypes.insert( pair<apt_uint,apt_uint>(
									etyp, cnts * thisone->second.at(ii) ) );
						}
					}
				}
				else {
					cerr << "There is an inconsistence while bookkeeping the element counts for the decomposed iontypes" << "\n";
					return false;
				}
			}
		} //for each iontype
	} //for each relevant object

	ctoc = MPI_Wtime();
	cout << "Ion inclusion tests took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = MPI_Wtime();

	//finally, to get the same formatted ion_evap index array for each object irrespective of the number of threads used we do a multithreaded sorting of the ion_evap array
	#pragma omp parallel
	{
		#pragma omp for schedule(dynamic, 1)
		for( size_t i = 0; i < objs.size(); i++ ) {
			if ( objs[i] != NULL ) {
				if ( objs[i]->info.obj_typ == OBJECT_IS_SURFACEMESH ) {
					if ( objs[i]->info.has_valid_proxy == true ) {
						if ( objs[i]->info.is_valid_volume == true ) {
							if ( objs[i]->info.is_valid_aabb == true ) {
								if ( objs[i]->ion_evapid.size() > 0 ) {
									sort( objs[i]->ion_evapid.begin(), objs[i]->ion_evapid.end() );
								}
							}
						}
					}
				}
			}
		}
	}

	ctoc = MPI_Wtime();
	cout << "Sorting ion_evap arrays for convenience took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = MPI_Wtime();

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "FindIonsInProxiesDelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_PAR, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_iontype_specific_delocalization_result(
	size_t const ityp, rangeTable & rng, vector<double> & cnts_f64 )
{
	double sum = 0.;
	for( auto it = cnts_f64.begin(); it != cnts_f64.end(); it++ ) {
		sum += *it;
	}
	cout << "IMPLEMENT write_iontype_specific_delocalization_result() for ityp " << ityp << " sum " << setprecision(32) << sum << " !!!" << "\n";

	double tic = MPI_Wtime();

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXdelocalization") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	string sub_grpnm = grpnm + "/grid";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_grid") );
	if ( h5w.nexus_path_exists( sub_grpnm ) == false ) {
		if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}
	//much of the grid metadata will be added later for now add only scalar_field_ionID container

	string sub_sub_grpnm = sub_grpnm + "/scalar_field_magn_ion" + to_string(ityp);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXdata") );
	/*
	anno.add( "signal", string("intensity") );
	vector<string> axes = { "zpos", "ypos", "xpos" }; //##MK::add array of strings
	anno.add( "axes", axes);
	anno.add( "zpos_indices", (apt_uint) 2 );
	anno.add( "ypos_indices", (apt_uint) 1 );
	anno.add( "xpos_indices", (apt_uint) 0 );
	*/
	if ( h5w.nexus_write_group( sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	/*
	dsnm = grpnm + "/intensity";
	//write cnts_f64 in place;
	anno = ioAttributes();
	//raw integrated intensity values, no normalization
	if ( h5w.nexus_write(
		dsnm,
		io_info({(apt_uint) gridinfo.nz, (apt_uint) gridinfo.ny, (apt_uint) gridinfo.nx },
				{(apt_uint) gridinfo.nz, (apt_uint) gridinfo.ny, (apt_uint) gridinfo.nx },
				MYHDF5_COMPRESSION_GZIP, 0x01),
		cnts_f64,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	//do not delete cnts_f64

	dsnm = sub_sub_grpnm + "/zpos";
	for( apt_int iz = 0; iz < gridinfo.nz; iz++ ) {
		real.push_back( gridinfo.where_z( iz ) );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_sub_grpnm + "/ypos";
	for( apt_int iy = 0; iy < gridinfo.ny; iy++ ) {
		real.push_back( gridinfo.where_y( iy ) );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_sub_grpnm + "/xpos";
	for( apt_int ix = 0; ix < gridinfo.nx; ix++ ) {
		real.push_back( gridinfo.where_x( ix ) );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_sub_grpnm + "/title";
	string title = "Iontype-specific delocalized field for iontype " + to_string(ityp);
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, title, anno ) != MYHDF5_SUCCESS ) { return false; }

	//##MK::check that the structure of the array is correct,
	//##MK::check that the chunksize  matches well
	//you must not delete scalar_field here!
	//##MK::IMPLEMENT 3D CASE ##### !!!
	*/

	//##MK::for XDMF visualization
	dsnm = sub_sub_grpnm + "/xdmf_intensity";
	//use cnts_f64 in place
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({cnts_f64.size()}, {cnts_f64.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		cnts_f64,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	//do not delete cnts_f64 !

	dsnm = sub_sub_grpnm + "/xdmf_xyz";
	vector<apt_real> real;
	for( apt_int iz = 0; iz < gridinfo.nz; iz++ ) {
		apt_real barycenterz = gridinfo.where_z( iz );
		for ( apt_int iy = 0; iy < gridinfo.ny; iy++ ) {
			apt_real barycentery = gridinfo.where_y( iy );
			for ( apt_int ix = 0; ix < gridinfo.nx; ix++ ) {
				apt_real barycenterx = gridinfo.where_x( ix );
				real.push_back( barycenterx );
				real.push_back( barycentery );
				real.push_back( barycenterz );
			}
		}
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_sub_grpnm + "/xdmf_topology";
	vector<apt_uint> uint = vector<apt_uint>( (size_t) 3*gridinfo.nxyz, 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	for ( apt_int i = 0; i < gridinfo.nxyz; i++ ) {  //XDMF type key 1, how many 1 because vertices
		uint[3*i+2] = (apt_uint) i;
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	string sub_sub_sub_grpnm = sub_sub_grpnm + "/ion" + to_string(ityp);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXion"));
	if ( h5w.nexus_write_group( sub_sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_sub_grpnm + "/ion_type";
	apt_uint itypid = ( ityp < 256 ) ? (apt_uint) ityp : 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, itypid, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_sub_grpnm + "/nuclide_hash";
	vector<unsigned short> u16 = vector<unsigned short>( MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
	for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
		u16[j] = rng.iontypes[ityp].ivec[j];
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u16.size()}, {u16.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u16,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u16 = vector<unsigned short>();

	dsnm = sub_sub_sub_grpnm + "/charge_state";
	apt_int chrg = 0;
	if ( rng.iontypes[ityp].charge_sgn == MYPOSITIVE ) {
		chrg = (int) + 1 * (int) rng.iontypes[ityp].charge_state;
	}
	if ( rng.iontypes[ityp].charge_sgn == MYNEGATIVE ) {
		chrg = (int) -1 * (int) rng.iontypes[ityp].charge_state;
	}
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, chrg, anno ) != MYHDF5_SUCCESS ) { return false; }

	//##MK::add complementary output to XDMF

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteIonType" + to_string(ityp) + "DelocalizationRawH5DelocTask" + to_string(info.dlzid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_delocalization_grid()
{
	double tic = MPI_Wtime();

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string sub_grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXdelocalization") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) {
			return false;
		}
	}
	sub_grpnm = grpnm + "/grid";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_grid") );
	if ( h5w.nexus_path_exists( sub_grpnm) == false ) {
		if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	dsnm = sub_grpnm + "/dimensionality";
	apt_uint dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/cardinality";
	apt_uint card = (apt_uint) gridinfo.nxyz;
	if ( h5w.nexus_write( dsnm, card, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/origin";
	vector<apt_real> origin = { gridinfo.origin.x, gridinfo.origin.y, gridinfo.origin.z };
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({origin.size()}, {origin.size()}, MYHDF5_COMPRESSION_NONE, 0x00),
		origin, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/symmetry";
	string symm = "cubic";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, symm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/cell_dimensions";
	vector<apt_real> aabb_size = { gridinfo.dx, gridinfo.dy, gridinfo.dz };
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({aabb_size.size()}, {aabb_size.size()}, MYHDF5_COMPRESSION_NONE, 0x00),
		aabb_size, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/extent";
	vector<apt_uint> dims = { (apt_uint) gridinfo.nx, (apt_uint) gridinfo.ny, (apt_uint) gridinfo.nz };
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({dims.size()}, {dims.size()}, MYHDF5_COMPRESSION_NONE, 0x00),
		dims, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/identifier_offset";
	apt_uint cell_id_offset = 0; //##MK::identifier and offset integer or unsigned integer?
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, cell_id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	string sub_sub_grpnm = sub_grpnm + "/bounding_box";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_hexahedron_set") );
	if ( h5w.nexus_write_group( sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_grpnm + "/is_axis_aligned";
	unsigned char option = 0x01;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, option, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_grpnm + "/identifier_offset";
	apt_uint id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	string sub_sub_sub_grpnm = sub_sub_grpnm + "/hexahedron";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_face_list_data_structure") );
	if ( h5w.nexus_write_group( sub_sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_sub_grpnm + "/vertex_identifier_offset";
	apt_uint vrts_id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, vrts_id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_sub_grpnm + "/face_identifier_offset";
	apt_uint fcts_id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, fcts_id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_sub_grpnm + "/vertices";
	vector<apt_real> real = gridinfo.aabb.nexus_get_vertices();
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()/3, 3},
				{real.size()/3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_sub_sub_grpnm + "/faces";
	vector<apt_uint> uint;
	uint = gridinfo.aabb.nexus_get_faces_as_quads( false );
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size()/3, 3},
				{uint.size()/3, 3},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	dsnm = sub_sub_sub_grpnm + "/xdmf_topology";
	uint = gridinfo.aabb.nexus_get_faces_as_quads( true );
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	//##MK::number_of_boundaries, boundaries, boundary_conditions

	dsnm = sub_grpnm + "/kernel_size";
	vector<apt_int> ksize = {
		info.deloc_halfsize_a, info.deloc_halfsize_a, info.deloc_halfsize_a
	};
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({ksize.size()}, {ksize.size()}, MYHDF5_COMPRESSION_NONE, 0x00),
		ksize, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/kernel_sigma";
	vector<double> ksigma = { info.deloc_sigma, info.deloc_sigma, 0.5 * info.deloc_sigma }; //##MK::0.5 ?
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({ksigma.size()}, {ksigma.size()}, MYHDF5_COMPRESSION_NONE, 0x00),
		ksigma, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/kernel_mu";
	vector<double> kmu = { MYZERO, MYZERO, MYZERO };
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({kmu.size()}, {kmu.size()}, MYHDF5_COMPRESSION_NONE, 0x00),
		kmu, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/normalization";
	anno = ioAttributes();
	string nrm = "";
	switch(info.normalization)
	{
		case ISOSURF_NRMLZ_COMPOSITION: {
			nrm = "composition"; break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION: {
			nrm = "concentration";
			break;
		}
		default: {
			nrm = "none";
		}
	}
	if ( h5w.nexus_write(dsnm, nrm, anno ) != MYHDF5_SUCCESS ) { return false; }

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteDelocalizationGridH5DelocTask"+to_string(info.dlzid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_delocalization_window( vector<p3dinfo> const & iinfo )
{
	double tic = MPI_Wtime();

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string sub_grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXdelocalization") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) {
			return false;
		}
	}

	sub_grpnm = grpnm + "/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_filter_boolean_mask") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) iinfo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/bitdepth";
	apt_uint bitdepth = 8;
	if ( h5w.nexus_write( dsnm, bitdepth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/mask";
	bool* bitfield = NULL;
	bitfield = new bool[n_ions];
	if ( bitfield != NULL ) {
		vector<unsigned char> u08_bitfield = vector<unsigned char>();
		for( size_t i = 0; i < iinfo.size(); i++ ) {
			bitfield[i] = ( iinfo[i].mask1 == ANALYZE_YES ) ? true : false;
		}
		BitPacker packbits;
		packbits.bool_to_bitpacked_uint8( bitfield,  n_ions, u08_bitfield );
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) {
				delete [] bitfield; bitfield = NULL;
				return false;
		}
		delete [] bitfield; bitfield = NULL;
	}
	else {
		cerr << "write_vxlizer_task_info bitfield array was not allocatable!" << "\n";
		return false;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteDelocalizationWindowH5DelocTask"+to_string(info.dlzid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_scalar_field_and_gradient_h5()
{
	double tic = MPI_Wtime();

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string sub_grpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid) + "/grid";
	anno = ioAttributes();
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	sub_grpnm = grpnm + "/scalar_field_magn_total";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXdata") );
	/*
	anno.add( "signal", string("intensity") );
	vector<string> axes = { "zpos", "ypos", "xpos" }; //##MK::add array of strings
	anno.add( "axes", axes);
	anno.add( "zpos_indices", (apt_uint) 2 );
	anno.add( "ypos_indices", (apt_uint) 1 );
	anno.add( "xpos_indices", (apt_uint) 0 );
	*/
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	/*
	dsnm = sub_grpnm + "/intensity";
	vector<apt_real> real = scalarfield;
	anno = ioAttributes();
	switch(info.normalization)
	{
		//case ISOSURF_NRMLZ_NONE: { anno.add( "unit", string("1") ); break; }
		case ISOSURF_NRMLZ_COMPOSITION:
		{
			anno.add( "unit", string("at.-%") );
			for( size_t i = 0; i < real.size(); i++ ) {
				real[i] *= 100.;
			}
			break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION: { anno.add( "unit", string("1/nm^3") ); break; }
		default: { break; }
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({(apt_uint) gridinfo.nz, (apt_uint) gridinfo.ny, (apt_uint) gridinfo.nx },
				{(apt_uint) gridinfo.nz, (apt_uint) gridinfo.ny, (apt_uint) gridinfo.nx },
				MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_grpnm + "/zpos";
	for( apt_int iz = 0; iz < gridinfo.nz; iz++ ) {
		real.push_back( gridinfo.where_z( iz ) );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_grpnm + "/ypos";
	for( apt_int iy = 0; iy < gridinfo.ny; iy++ ) {
		real.push_back( gridinfo.where_y( iy ) );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_grpnm + "/xpos";
	for( apt_int ix = 0; ix < gridinfo.nx; ix++ ) {
		real.push_back( gridinfo.where_x( ix ) );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_grpnm + "/title";
	string title = "Delocalized point cloud";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, title, anno ) != MYHDF5_SUCCESS ) { return false; }

	//##MK::check that the structure of the array is correct,
	//##MK::check that the chunksize  matches well
	//you must not delete scalar_field here!
	//##MK::IMPLEMENT 3D CASE ##### !!!
	*/

	//##MK::for XDMF visualization
	dsnm = sub_grpnm + "/xdmf_intensity";
	//use scalar_field in-place
	anno = ioAttributes();
	vector<apt_real> real = scalarfield;
	switch(info.normalization)
	{
		//case ISOSURF_NRMLZ_NONE: { anno.add( "unit", string("1") ); break; }
		case ISOSURF_NRMLZ_COMPOSITION:
		{
			anno.add( "unit", string("at.-%") );
			for( size_t i = 0; i < real.size(); i++ ) {
				real[i] *= 100.;
			}
			break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION: { anno.add( "unit", string("1/nm^3") ); break; }
		default: { break; }
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();
	//do not delete scalarfield !

	dsnm = sub_grpnm + "/xdmf_xyz";
	for( apt_int iz = 0; iz < gridinfo.nz; iz++ ) {
		apt_real barycenterz = gridinfo.where_z( iz );
		for ( apt_int iy = 0; iy < gridinfo.ny; iy++ ) {
			apt_real barycentery = gridinfo.where_y( iy );
			for ( apt_int ix = 0; ix < gridinfo.nx; ix++ ) {
				apt_real barycenterx = gridinfo.where_x( ix );
				real.push_back( barycenterx );
				real.push_back( barycentery );
				real.push_back( barycenterz );
			}
		}
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_grpnm + "/xdmf_topology";
	vector<apt_uint> uint = vector<apt_uint>( (size_t) 3*gridinfo.nxyz, 1 ); //MK::we need to store a triplet of XDMF topology type count and vertex ID
	for ( apt_int i = 0; i < gridinfo.nxyz; i++ ) {  //XDMF type key 1, how many 1 because vertices
		uint[3*i+2] = (apt_uint) i;
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	//##MK::only if the scalarfield is mapped correctly we should
	//start with exporting the gradient field

	sub_grpnm = grpnm + "/scalar_field_grad_total";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXdata") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	/*
	//H5Web cannot currently display >3d fields
	dsnm = sub_grpnm + "/intensity";
	real = scalar_field_gradient;
	anno = ioAttributes();
	switch(info.normalization)
	{
		case ISOSURF_NRMLZ_NONE:
		{
			anno.add( "unit", string("1/nm") );
			break;
		}
		case ISOSURF_NRMLZ_COMPOSITION:
		{
			anno.add( "unit", string("at.-%/nm") ); break;
			for ( size_t i = 0; i < real.size(); i++ ) {
				real[i] *= 100.;
			}
			break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION: { anno.add( "unit", string("(1/nm^3)/nm") ); break; }
		default: { break; }
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({(apt_uint) gridinfo.nz, (apt_uint) gridinfo.ny, (apt_uint) gridinfo.nx, 3 },
				{(apt_uint) gridinfo.nz, (apt_uint) gridinfo.ny, (apt_uint) gridinfo.nx, 3 },
				MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();
	//##MK::check that the structure of the array is correct, check that the chunksize  matches well
	//you must not delete scalarfield_grad3d here!
	*/

	dsnm = sub_grpnm + "/xdmf_gradient";
	//use scalarfield_grad3d in-place
	anno = ioAttributes();
	real = scalarfield_grad3d;
	switch(info.normalization)
	{
		case ISOSURF_NRMLZ_NONE:
		{
			anno.add( "unit", string("1/nm") );
			break;
		}
		case ISOSURF_NRMLZ_COMPOSITION:
		{
			anno.add( "unit", string("at.-%/nm") );
			for( size_t i = 0; i < real.size(); i++ ) {
				real[i] *= 100.;
			}
			break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION: { anno.add( "unit", string("(1/nm^3)/nm") ); break; }
		default: { break; }
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();
	//do not delete scalarfield_grad3d

	//##MK::only if the scalarfield is written out we will arrive here

//complementary output to XDMF
	xdmfBaseHdl xml;
	xml.add_grid_uniform( "delocalization" + to_string(info.dlzid) );
	vector<size_t> dims = { (size_t) gridinfo.nxyz, (size_t) 3 };
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid) + "/grid/scalar_field_magn_total";
	dsnm = grpnm + "/xdmf_xyz";
	xml.add_geometry_xyz( dims, H5_F32,
		strip_path_prefix(ConfigShared::OutputfileName) + ":" + dsnm );
	dims = { (size_t) 3*gridinfo.nxyz, (size_t) 1 };
	dsnm = grpnm + "/xdmf_topology";
	xml.add_topology_mixed( (size_t) gridinfo.nxyz, dims, H5_U32,
		strip_path_prefix(ConfigShared::OutputfileName) + ":" + dsnm );
	dims = { (size_t) gridinfo.nxyz, 1 };
	dsnm = grpnm + "/xdmf_intensity";
	xml.add_attribute_scalar( "fieldvalue", "Node", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + dsnm );
	dims = { (size_t) gridinfo.nxyz, 3 };

	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid) + "/grid/scalar_field_grad_total";
	dsnm = grpnm + "/xdmf_gradient";
	xml.add_attribute_vector( "gradient", "Node", dims, H5_F32,
		strip_path_prefix(ConfigShared::OutputfileName) + ":" + dsnm );
	const string xmlfn = strip_path_prefix(ConfigShared::OutputfileName)
			+ ".EntryId." + to_string(entry_id) + ".DelocTaskID."
			+ to_string(info.dlzid) + ".ScalarField.xdmf";
	xml.write( xmlfn );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteScalarFieldH5DelocTask" + to_string(info.dlzid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_isrf_complex_h5()
{
	double tic = MPI_Wtime();

	if ( 3*tscl.vrts.size() >= (size_t) UMX ) {
		cerr << "The total number of triangle vertices to write out is larger than currently supported by the implementation !" << "\n";
		return false;
	}
	if ( tscl.fcts.size() < 1 ) {
		cout << "WARNING:: There are no triangles to report" << "\n";
		return true;
	}

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string sub_grpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXisocontour") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/dimensionality";
	apt_uint dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/isovalue";
	apt_real phi = info.isoval;
	anno = ioAttributes();
	switch(info.normalization)
	{
		//case ISOSURF_NRMLZ_NONE: { anno.add( "unit", string("1") ); break; }
		case ISOSURF_NRMLZ_COMPOSITION:
		{
			anno.add( "unit", string("at.-%") );
			phi *= 100.;
			break;
		}
		case ISOSURF_NRMLZ_CONCENTRATION: { anno.add( "unit", string("1/nm^3") ); break; }
		default: { break; }
	}
	if ( h5w.nexus_write( dsnm, phi, anno ) != MYHDF5_SUCCESS ) { return false; }

	sub_grpnm = grpnm + "/marching_cubes";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_marching_cubes") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/implementation";
	string doi = "https://doi.org/10.1080/10867651.2003.10487582"; //##MK::check in detail
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, doi, anno ) != MYHDF5_SUCCESS ) { return false; }

	sub_grpnm = grpnm + "/triangle_soup";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_triangle_set") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/dimensionality";
	dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/cardinality";
	apt_uint card = tscl.fcts.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, card, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/identifier_offset";
	apt_uint id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	string sub_sub_grpnm = sub_grpnm + "/triangles";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_face_list_data_structure") );
	if ( h5w.nexus_write_group( sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_grpnm + "/number_of_vertices";
	apt_uint n_vrts = tscl.vrts.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_vrts, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_grpnm + "/number_of_faces";
	apt_uint n_fcts = tscl.fcts.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_fcts, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_grpnm + "/vertex_identifier_offset";
	id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_grpnm + "/face_identifier_offset";
	id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_grpnm + "/vertices";
	vector<apt_real> real;
	for( auto it = tscl.vrts.begin(); it != tscl.vrts.end(); it++ ) {
		real.push_back( it->x );
		real.push_back( it->y );
		real.push_back( it->z );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_sub_grpnm + "/faces";
	vector<apt_uint> uint;
	for( auto it = tscl.fcts.begin(); it != tscl.fcts.end(); it++ ) {
		uint.push_back( it->v1 );
		uint.push_back( it->v2 );
		uint.push_back( it->v3 );
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size() / 3, 3}, {uint.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	dsnm = sub_sub_grpnm + "/xdmf_topology";
	for( auto it = tscl.fcts.begin(); it != tscl.fcts.end(); it++ ) {
		uint.push_back( 3 ); //xdmf keyword for polygon
		uint.push_back( 3 ); //three vertices has the triangle
		uint.push_back( it->v1 );
		uint.push_back( it->v2 );
		uint.push_back( it->v3 );
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	string sub_sub_sub_grpnm = sub_sub_grpnm + "/face_normal";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_unit_normal_set") );
	if ( h5w.nexus_write_group( sub_sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_sub_sub_grpnm + "/normals";
	for( auto it = tscl.nrms.begin(); it != tscl.nrms.end(); it++ ) {
		real.push_back( it->x );
		real.push_back( it->y );
		real.push_back( it->z );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	dsnm = sub_sub_sub_grpnm + "/gradient_guide_magnitude";
	for( auto it = tscl.nrms_quality.begin(); it != tscl.nrms_quality.end(); it++ ) {
		real.push_back( it->SQRmagn );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm^2") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

	/*
	dsnm = sub_sub_sub_grpnm + "/gradient_guide_quality";
	for( auto it = tscl.nrms_quality.begin(); it != tscl.nrms_quality.end(); it++ ) {
		real.push_back( it->dangling_normal_magn );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm^2") );
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();
	*/

	dsnm = sub_sub_sub_grpnm + "/gradient_guide_projection";
	for( auto it = tscl.nrms_quality.begin(); it != tscl.nrms_quality.end(); it++ ) {
		real.push_back( it->cosPhi );
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<apt_real>();

//complementary XDMF visualization
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles";

	xdmfBaseHdl xml;
	xml.add_grid_uniform( "isosurface" + to_string(info.isrfid) ); // + " (" + to_string(info.isoval) + ")" );
	vector<size_t> dims = { tscl.fcts.size()*(1+1+3), 1 };
	//xdmf keyword, xdmf number of vertices, three vertex IDs for each triangle
	//##MK::make adaptive for EMPLOY_DOUBLE_PRECISION
	xml.add_topology_mixed( tscl.fcts.size(), dims, H5_U32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/xdmf_topology" );
	dims = { tscl.vrts.size(), 3 };
	xml.add_geometry_xyz( dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/vertices" );
	dims = { tscl.nrms.size(), 3 };
	xml.add_attribute_vector( "grad3d-guided normalvector", "Cell", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/face_normal/normals" );
	dims = { tscl.nrms_quality.size(), 1 };
	xml.add_attribute_scalar( "grad3d sqr magnitude", "Cell", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/face_normal/gradient_guide_magnitude" );
	/*
	dims = { tscl.nrms_quality.size(), 1 };
	xml.add_attribute_scalar( "dangling normal cross", "Cell", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/face_normal/gradient_guide_quality" );
	*/
	dims = { tscl.nrms_quality.size(), 1 };
	xml.add_attribute_scalar( "grad3d cosPhi to naive tri normal", "Cell", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/face_normal/gradient_guide_projection" );

	const string xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id)
		+ ".DelocTaskID." + to_string(info.dlzid) + ".IsoSrfTaskID." + to_string(info.isrfid) + ".IsoSurface.xdmf";
	xml.write( xmlfn );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteTriangleSoupH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_triangle_soup_clusterid_h5()
{
	double tic = MPI_Wtime();

	if ( 3*tscl.vrts.size() >= (size_t) UMX ) {
		cerr << "The total number of triangle vertices to write out is larger than currently supported by the implementation !" << "\n";
		return false;
	}
	if ( 3*tscl.fcts.size() >= (size_t) UMX ) {
		cerr << "The total number of triangle vertices to write out is larger than currently supported by the implementation !" << "\n";
		return false;
		//##MK::change wuibuf to a 64-bit type , e.g. ui64 size_t
	}
	if ( tscl.fcts2objid.size() != tscl.fcts.size() ) {
		cerr << "The number of cluster labels and non-degenerate triangle facets is inconsistent !" << "\n";
		return false;
	}
	if ( tscl.fcts.size() < 1 ) {
		cout << "WARNING:: There are no triangles to report" << "\n";
		return true;
	}

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string sub_grpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid) +
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles/volumetric_features";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXprocess") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/triangle_cluster_identifier";
	//use tscl.fcts2objid in-place
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({tscl.fcts2objid.size()}, {tscl.fcts2objid.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		tscl.fcts2objid,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	//do not delete fcts2objid

//complementary XDMF visualization
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles";

	xdmfBaseHdl xml;
	xml.add_grid_uniform( "triangle cluster isosurface " + to_string(info.isoval) );
	vector<size_t> dims = { tscl.fcts.size()*(1+1+3), 1 };
	//xdmf keyword, xdmf number of vertices, three vertex IDs for each triangle
	xml.add_topology_mixed( tscl.fcts.size(), dims, H5_U32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/xdmf_topology" );
	dims = { tscl.vrts.size(), 3 };
	xml.add_geometry_xyz( dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/vertices" );
	dims = { tscl.nrms.size(), 3 };
	xml.add_attribute_vector( "gradient_guide_normal", "Cell", dims, H5_F32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/face_normal/normals" );
	dims = { tscl.fcts.size(), 1 };
	xml.add_attribute_scalar( "feature_identifier", "Cell", dims, H5_U32,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + grpnm + "/volumetric_features/triangle_cluster_identifier" );

	const string xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id)
			+ ".DelocTaskID." + to_string(info.dlzid) + ".IsoSrfTaskID." + to_string(info.isrfid) + ".TriSoupCluster.xdmf";
	xml.write( xmlfn );

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteTriangleSoupClusterIdH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_objects_properties_h5()
{
	double tic = MPI_Wtime();

	if ( objs.size() >= (size_t) UMX ) {
		cerr << "The total number of objects to write out is larger than currently supported by the implementation !" << "\n";
		return false;
	}
	if ( objs.size() < 1 ) {
		cout << "WARNING:: There are no objects to report !" << "\n";
		return true;
	}

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string sub_grpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles/volumetric_features";

	//inspect commit a8b4fca0f8fc85bbf4b126ac17753ebeb5efa117 for code to export feature dictionary

	dsnm = grpnm + "/feature_identifier";
	vector<apt_uint> uint;
	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			uint.push_back( objs[i]->info.obj_id );
		}
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		uint,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	dsnm = grpnm + "/feature_type";
	vector<unsigned char> u08;
	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			u08.push_back( objs[i]->info.obj_typ );
		}
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	vector<size_t> objs_idx; //indices of non-NULL pointer to polyhedron objects
	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			if ( objs[i]->info.obj_typ == OBJECT_IS_POLYHEDRON ) {
				objs_idx.push_back(i);
			}
		}
	}

	if ( objs_idx.size() > 0 ) {
		sub_grpnm = grpnm + "/objects";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXprocess") );
		if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = sub_grpnm + "/feature_identifier";
		for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
			uint.push_back( objs[*jt]->info.obj_id );
		}
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		dsnm = sub_grpnm + "/volume";
		vector<double> f64;
		for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
			f64.push_back( objs[*jt]->info.obj_volume );
		}
		anno = ioAttributes();
		anno.add( "unit", string("nm^3") );
		if ( h5w.nexus_write(
			dsnm,
			io_info({f64.size()}, {f64.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			f64,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		f64 = vector<double>();

		if ( info.hasObjectsOBB == true ) {
			string sub_sub_grpnm = sub_grpnm + "/obb";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_hexahedron_set") );
			if ( h5w.nexus_write_group( sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = sub_sub_grpnm + "/size";
			vector<apt_real> aptreal;
			for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
				aptreal.push_back( objs[*jt]->info.obb_dims_0 );
				aptreal.push_back( objs[*jt]->info.obb_dims_1 );
				aptreal.push_back( objs[*jt]->info.obb_dims_2 );
			}
			anno = ioAttributes();
			anno.add( "unit", string("nm") );
			if ( h5w.nexus_write(
				dsnm,
				io_info({aptreal.size() / 3, 3}, {aptreal.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
				aptreal,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			aptreal = vector<apt_real>();

			dsnm = sub_sub_grpnm + "/aspect";
			for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
				aptreal.push_back( objs[*jt]->info.obb_yx_10 );
				aptreal.push_back( objs[*jt]->info.obb_zy_21 );
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({aptreal.size() / 2, 2}, {aptreal.size() / 2, 2}, MYHDF5_COMPRESSION_GZIP, 0x01),
				aptreal,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			aptreal = vector<apt_real>();

			dsnm = sub_sub_grpnm + "/center";
			for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
				//objs[*jt]->info.obb_points.size() == 8 ) {
				p3d center = p3d(0., 0., 0.);
				for( apt_uint j = 0; j < 8; j++ ) {
					center.x += objs[*jt]->info.obb_points[j].x;
					center.y += objs[*jt]->info.obb_points[j].y;
					center.z += objs[*jt]->info.obb_points[j].z;
				}
				aptreal.push_back( center.x / 8. );
				aptreal.push_back( center.y / 8. );
				aptreal.push_back( center.z / 8. );
				//##MK::it can be possible that if OBB failed there is not for every OBJECT_IS_POLYHEDRON an OBB visualization
			}
			anno = ioAttributes();
			anno.add( "unit", string("nm") );
			if ( h5w.nexus_write(
				dsnm,
				io_info({aptreal.size() / 3, 3}, {aptreal.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
				aptreal,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			aptreal = vector<apt_real>();

			string sub_sub_sub_grpnm = sub_sub_grpnm + "/hexahedra";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_face_list_data_structure") );
			if ( h5w.nexus_write_group( sub_sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = sub_sub_sub_grpnm + "/vertices";
			for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
				//objs[*jt]->info.obb_points.size() == 8 ) {
				for( apt_uint j = 0; j < 8; j++ ) {
					aptreal.push_back( objs[*jt]->info.obb_points[j].x );
					aptreal.push_back( objs[*jt]->info.obb_points[j].y );
					aptreal.push_back( objs[*jt]->info.obb_points[j].z );
				}
				//##MK::it can be possible that if OBB failed there is not for every OBJECT_IS_POLYHEDRON an OBB visualization
			}
			anno = ioAttributes();
			anno.add( "unit", string("nm") );
			if ( h5w.nexus_write(
				dsnm,
				io_info({aptreal.size() / 3, 3}, {aptreal.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
				aptreal,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			aptreal = vector<apt_real>();

			dsnm = sub_sub_sub_grpnm + "/xdmf_topology";
			apt_uint v = 0;
			for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
				//objs[*jt]->info.obb_points.size() == 8 ) {
				uint.push_back( 9 ); //xdmf hexahedron type key
				uint.push_back( 6 ); //xdmf number of faces of hexahedron as it is a polyhedron
				uint.push_back( 4 ); //xdmf number of nodes each face is made of quad
				uint.push_back( 1 + v ); //+x point at the viewer, right
				uint.push_back( 2 + v );
				uint.push_back( 6 + v );
				uint.push_back( 5 + v );
				uint.push_back( 4 );
				uint.push_back( 3 + v ); //-x point at the viewer, left
				uint.push_back( 0 + v );
				uint.push_back( 4 + v );
				uint.push_back( 7 + v );
				uint.push_back( 4 );
				uint.push_back( 2 + v ); //+y point at the viewer, rear
				uint.push_back( 3 + v );
				uint.push_back( 7 + v );
				uint.push_back( 6 + v );
				uint.push_back( 4 );
				uint.push_back( 0 + v ); //-y point at the viewer, front
				uint.push_back( 1 + v );
				uint.push_back( 5 + v );
				uint.push_back( 4 + v );
				uint.push_back( 4 );
				uint.push_back( 4 + v ); //+z point at the viewer, top
				uint.push_back( 5 + v );
				uint.push_back( 6 + v );
				uint.push_back( 7 + v );
				uint.push_back( 4 );
				uint.push_back( 3 + v ); //-z point at the viewer, bottom
				uint.push_back( 2 + v );
				uint.push_back( 1 + v );
				uint.push_back( 0 + v );
				v += 8;
				//##MK::it can be possible that if OBB failed there is not for every OBJECT_IS_POLYHEDRON an OBB visualization
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				uint,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			uint = vector<apt_uint>();

			dsnm = sub_sub_sub_grpnm + "/xdmf_feature_identifier";
			apt_uint identifier = 0;
			for( auto jt = objs_idx.begin(); jt != objs_idx.end(); jt++ ) {
				//objs[*jt]->info.obb_points.size() == 8 ) {
				for( apt_uint j = 0; j < 6; j++ ) {
					uint.push_back( identifier );
				}
				identifier++;
				//##MK::it can be possible that if OBB failed there is not for every OBJECT_IS_POLYHEDRON an OBB visualization
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				uint,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			uint = vector<apt_uint>();
		}
	}

	//##MK::xdmf support file
	//#########

	cout << "Written objects properties successfully" << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteObjectPropertiesH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_objects_ions_h5( vector<p3d> const & pp3, rangeTable & rng )
{
	double tic = MPI_Wtime();

	if ( objs.size() >= (size_t) UMX ) {
		cerr << "The total number of objects to write out is larger than currently supported by the implementation !" << "\n";
		return false;
	}
	if ( objs.size() < 1 ) {
		cout << "WARNING:: There are no objects to report !" << "\n";
		return true;
	}

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called

	apt_uint entry_id = 1;
	string prefix = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles/volumetric_features";
	string grpnm_interior = prefix + "/objects_far_from_edge";
	string grpnm_exterior = prefix + "/objects_close_to_edge";
	//##MK::do not recreate the grpnm_interior and grpnm_exterior!

	size_t MinIonSupport = ConfigNanochem::MinIonSupportForClosedObjects;
	vector<apt_uint> uint;

	vector<size_t> objs_interior_idx;
	vector<size_t> objs_exterior_idx;
	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			if ( objs[i]->is_object_interior( MinIonSupport ) == true ) {
				objs_interior_idx.push_back(i);
			}
			else if ( objs[i]->is_object_exterior( MinIonSupport ) == true ) {
				objs_exterior_idx.push_back(i);
			}
			else {
				continue;
			}
		}
	}

	if ( objs_interior_idx.size() > 0 ) {
		anno = ioAttributes();
		anno.add( "NX_class", string("NXprocess") );
		if ( h5w.nexus_write_group( grpnm_interior, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = grpnm_interior + "/feature_identifier";
		uint = vector<apt_uint>();
		for( auto jt = objs_interior_idx.begin(); jt != objs_interior_idx.end(); jt++ ) {
			uint.push_back( objs[*jt]->info.obj_id );
		}
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		dsnm = grpnm_interior + "/volume";
		vector<double> real;
		for( auto jt = objs_interior_idx.begin(); jt != objs_interior_idx.end(); jt++ ) {
			real.push_back( objs[*jt]->info.obj_volume );
		}
		anno = ioAttributes();
		anno.add( "unit", string("nm^3") );
		if ( h5w.nexus_write(
			dsnm,
			io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			real,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		real = vector<double>();

		//composition bookkeeping, interior objects
		grpnm = grpnm_interior + "/composition";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXchemical_composition") );
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

		vector<apt_uint> total = vector<apt_uint>( objs.size(), 0 );
		for ( auto itypit = rng.iontypes.begin(); itypit != rng.iontypes.end(); itypit++ ) {
			unsigned char itypid = itypit->first;

			grpnm = grpnm_interior + "/composition/ion" + to_string((int) itypid);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXion") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/charge_state";
			apt_int chrg = 0;
			if ( itypit->second.charge_sgn == MYPOSITIVE ) {
				chrg = (int) + 1 * (int) itypit->second.charge_state;
			}
			if ( itypit->second.charge_sgn == MYNEGATIVE ) {
				chrg = (int) -1 * (int) itypit->second.charge_state;
			}
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, chrg, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/nuclide_hash";
			vector<unsigned short> u16 = vector<unsigned short>(
					MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				u16[j] = itypit->second.ivec[j];
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION}, {MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/nuclide_list";
			u16 = vector<unsigned short>( 2 * MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				pair<unsigned char, unsigned char> ZN = isotope_unhash( itypit->second.ivec[j] );
				if ( ZN.second > 0 ) {
					u16[(j*2)+0] = (unsigned short) ZN.first + (unsigned short) ZN.second;
				}
				u16[(j*2)+1] = (unsigned short) ZN.first;  //column matrix
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/count";
			uint = vector<apt_uint>();
			for( auto jt = objs_interior_idx.begin(); jt != objs_interior_idx.end(); jt++ ) {
				map<apt_uint, apt_uint>::iterator thisone = objs[*jt]->myres_iontypes.find( itypid );
				if( thisone == objs[*jt]->myres_iontypes.end() ) {
					uint.push_back( 0 );
				}
				else {
					uint.push_back( thisone->second );
					total[*jt] += thisone->second;
				}
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				uint,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			uint = vector<apt_uint>();
		}

		dsnm = grpnm_interior + "/composition/total";
		//must not use total in-place
		for( auto jt = objs_interior_idx.begin(); jt != objs_interior_idx.end(); jt++ ) {
			uint.push_back(total[*jt]);
		}
		total = vector<apt_uint>();
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		//evaporation_identifier of ions inside or on the surface of the triangulated surface mesh of each object
		//MK::here we also instantiate the group for each objectID(NXcg_face_list_data_structure)
		for( auto jt = objs_interior_idx.begin(); jt != objs_interior_idx.end(); jt++ ) {
			grpnm = grpnm_interior + "/object" + to_string(objs[*jt]->info.obj_id);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_polyhedron_set") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			grpnm += "/polyhedron";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_face_list_data_structure") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/ion_identifier";
			//use objs[*jt]->ion_evapid in-place
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({objs[*jt]->ion_evapid.size()}, {objs[*jt]->ion_evapid.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				objs[*jt]->ion_evapid,
				anno ) != MYHDF5_SUCCESS ) { return false; }
		}
	}

	if ( objs_exterior_idx.size() > 0 ) {
		anno = ioAttributes();
		anno.add( "NX_class", string("NXprocess") );
		if ( h5w.nexus_write_group( grpnm_exterior, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = grpnm_exterior + "/feature_identifier";
		uint = vector<apt_uint>();
		for( auto jt = objs_exterior_idx.begin(); jt != objs_exterior_idx.end(); jt++ ) {
			uint.push_back( objs[*jt]->info.obj_id );
		}
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		dsnm = grpnm_exterior + "/volume";
		vector<double> real;
		for( auto jt = objs_exterior_idx.begin(); jt != objs_exterior_idx.end(); jt++ ) {
			real.push_back( objs[*jt]->info.obj_volume );
		}
		anno = ioAttributes();
		anno.add( "unit", string("nm^3") );
		if ( h5w.nexus_write(
			dsnm,
			io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			real,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		real = vector<double>();

		grpnm = grpnm_exterior + "/composition";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXchemical_composition") );
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

		vector<apt_uint> total = vector<apt_uint>( objs.size(), 0 );
		for ( auto itypit = rng.iontypes.begin(); itypit != rng.iontypes.end(); itypit++ ) {
			unsigned char itypid = itypit->first;

			grpnm = grpnm_exterior + "/composition/ion" + to_string((int) itypid);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXion") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/charge_state";
			apt_int chrg = 0;
			if ( itypit->second.charge_sgn == MYPOSITIVE ) {
				chrg = (int) + 1 * (int) itypit->second.charge_state;
			}
			if ( itypit->second.charge_sgn == MYNEGATIVE ) {
				chrg = (int) -1 * (int) itypit->second.charge_state;
			}
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, chrg, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/nuclide_hash";
			vector<unsigned short> u16 = vector<unsigned short>(
					MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				u16[j] = itypit->second.ivec[j];
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
						{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/nuclide_list";
			u16 = vector<unsigned short>( 2 * MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				pair<unsigned char, unsigned char> ZN = isotope_unhash( itypit->second.ivec[j] );
				if ( ZN.second > 0 ) {
					u16[(j*2)+0] = (unsigned short) ZN.first + (unsigned short) ZN.second;
				}
				u16[(j*2)+1] = (unsigned short) ZN.first;  //column vector
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/count";
			uint = vector<apt_uint>();
			for( auto jt = objs_exterior_idx.begin(); jt != objs_exterior_idx.end(); jt++ ) {
				map<apt_uint, apt_uint>::iterator thisone = objs[*jt]->myres_iontypes.find( itypid );
				if( thisone == objs[*jt]->myres_iontypes.end() ) {
					uint.push_back( 0 );
				}
				else {
					uint.push_back( thisone->second );
					total[*jt] += thisone->second;
				}
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				uint,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			uint = vector<apt_uint>();
		}

		dsnm = grpnm_exterior + "/composition/total";
		//must not use total in-place
		for( auto jt = objs_exterior_idx.begin(); jt != objs_exterior_idx.end(); jt++ ) {
			uint.push_back(total[*jt]);
		}
		total = vector<apt_uint>();
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		//evaporation_identifier of ions inside or on the surface of the triangulated surface mesh of each object
		//MK::here we also instantiate the group for each objectID(NXcg_face_list_data_structure)
		for( auto jt = objs_exterior_idx.begin(); jt != objs_exterior_idx.end(); jt++ ) {
			grpnm = grpnm_exterior + "/object" + to_string(objs[*jt]->info.obj_id);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_polyhedron_set") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			grpnm += "/polyhedron";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_face_list_data_structure") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/ion_identifier";
			//use objs[*jt]->ion_evapid in-place
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({objs[*jt]->ion_evapid.size()}, {objs[*jt]->ion_evapid.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				objs[*jt]->ion_evapid,
				anno ) != MYHDF5_SUCCESS ) { return false; }
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteIonsInClosedObjectsH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_objects_geometry_h5()
{
	double tic = MPI_Wtime();

	if ( objs.size() >= (size_t) UMX ) {
		cerr << "The total number of objects to write out is larger than currently supported by the implementation !" << "\n";
		return false;
	}
	if ( objs.size() < 1 ) {
		cout << "WARNING:: There are no objects to report !" << "\n";
		return true;
	}

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string subgrpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called
	apt_uint entry_id = 1;
	string prefix = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles/volumetric_features";

	//xdmf support file for rendering all objects inside the dataset
	vector<xdmfBaseHdl*> xdmf;
	for( size_t thisone = 0; thisone < 2; thisone++ ) {
		xdmfBaseHdl* xdmf_ptr = new xdmfBaseHdl();
		if ( xdmf_ptr != NULL ) {
			xdmf.push_back( xdmf_ptr ); //XDMF_INTERIOR
			xdmf.back()->init_collection();
		}
		else {
			for ( size_t ii = 0; ii < thisone; ii++ ) {
				delete xdmf.at(ii); xdmf[ii] = NULL;
			}
			cerr << "Allocation of xdmfBaseHdl objects failed !" << "\n";
			return false;
		}
	}

	size_t MinIonSupport = ConfigNanochem::MinIonSupportForClosedObjects;

	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			if ( objs[i]->is_object( MinIonSupport ) == true ) {
				string subgrpnm = "";
				size_t thisone = XDMF_INTERIOR;
				if ( objs[i]->is_object_interior( MinIonSupport ) == true ) {
					subgrpnm = prefix + "/objects_far_from_edge/object" + to_string(objs[i]->info.obj_id) + "/polyhedron";
				}
				else if ( objs[i]->is_object_exterior( MinIonSupport ) == true ) {
					subgrpnm = prefix + "/objects_close_to_edge/object" + to_string(objs[i]->info.obj_id) + "/polyhedron";
					thisone = XDMF_EXTERIOR;
				}
				else {
					continue;
				}

				/* ##MK::a group with the name within subgrpnm should have already been created !
				anno = ioAttributes();
				anno.add( "NX_class", "NXcg_face_list_data_structure");
				if ( h5w.nexus_write_group( subgrpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
				number_of_vertices(NX_POSINT):
				number_of_faces(NX_POSINT):
				vertex_identifier_offset(NX_UINT):
				*/

				dsnm = subgrpnm + "/vertices";
				vector<apt_real> real;
				for( auto jt = objs[i]->ply_mesh_vrts.begin(); jt != objs[i]->ply_mesh_vrts.end(); jt++ ) {
					real.push_back( jt->x );
					real.push_back( jt->y );
					real.push_back( jt->z );
				}
				anno = ioAttributes();
				anno.add( "unit", string("nm") );
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<apt_real>();

				dsnm = subgrpnm + "/faces";
				vector<apt_uint> uint;
				for( auto jt = objs[i]->ply_mesh_fcts_trgls.begin(); jt != objs[i]->ply_mesh_fcts_trgls.end(); jt++ ) {
					uint.push_back( jt->v1 );
					uint.push_back( jt->v2 );
					uint.push_back( jt->v3 );
				}
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({uint.size() / 3, 3}, {uint.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
					uint,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				uint = vector<apt_uint>();

				dsnm = subgrpnm + "/xdmf_topology";
				uint = vector<apt_uint>(
						objs[i]->ply_mesh_fcts_trgls.size() * (1+1+3), 3 ); //xdmf keyword, three facets
				size_t k = 2;
				for( auto jt = objs[i]->ply_mesh_fcts_trgls.begin(); jt != objs[i]->ply_mesh_fcts_trgls.end(); jt++ ) {
					uint[k+0] = jt->v1;
					uint[k+1] = jt->v2;
					uint[k+2] = jt->v3;
					k += (1+1+3);
				}
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
					uint,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				uint = vector<apt_uint>();

				dsnm = subgrpnm + "/xdmf_feature_identifier";
				uint = vector<apt_uint>( objs[i]->ply_mesh_fcts_trgls.size(), objs[i]->info.obj_id );
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
					uint,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				uint = vector<apt_uint>();

				dsnm = subgrpnm + "/face_normals";
				for( auto jt = objs[i]->ply_mesh_fcts_ounrm_eigen.begin(); jt != objs[i]->ply_mesh_fcts_ounrm_eigen.end(); jt++ ) {
					real.push_back( jt->x );
					real.push_back( jt->y );
					real.push_back( jt->z );
				}
				anno = ioAttributes();
				anno.add( "unit", string("nm") );
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<apt_real>();

//add XDMF visualization support
				xdmf[thisone]->add_collection_entry();
				xdmf[thisone]->add_collection_grid_uniform(
					"object" + to_string(objs[i]->info.obj_id) );
				xdmf[thisone]->add_collection_geometry_xyz(
					{objs[i]->ply_mesh_vrts.size(), 3}, H5_F32,
					strip_path_prefix(ConfigShared::OutputfileName) + ":" + subgrpnm + "/vertices" );
				xdmf[thisone]->add_collection_topology_mixed(
					objs[i]->ply_mesh_fcts_trgls.size(),
					{objs[i]->ply_mesh_fcts_trgls.size() * (1+1+3), 1}, H5_U32,
					strip_path_prefix(ConfigShared::OutputfileName) + ":" + subgrpnm + "/xdmf_topology" );
				xdmf[thisone]->add_collection_attribute_scalar(
					"object_identifier", "Cell",
					{objs[i]->ply_mesh_fcts_trgls.size(), 1}, H5_U32,
					strip_path_prefix(ConfigShared::OutputfileName) + ":" + subgrpnm + "/xdmf_feature_identifier" );
			}
		}
	}

	for( size_t thisone = 0; thisone < 2; thisone++ ) {
		string xmlfn = "";
		if ( thisone == (size_t) XDMF_INTERIOR ) {
			xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id)
				+ ".DelocTaskID." + to_string(info.dlzid) + ".IsoSrfTaskID." + to_string(info.isrfid) + ".ObjectsFarFromEdgeGeometry.xdmf";
		}
		if ( thisone == (size_t) XDMF_EXTERIOR ) {
			xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id)
				+ ".DelocTaskID." + to_string(info.dlzid) + ".IsoSrfTaskID." + to_string(info.isrfid) + ".ObjectsCloseToEdgeGeometry.xdmf";
		}
		xdmf[thisone]->write_collection( xmlfn );
		delete xdmf[thisone]; xdmf[thisone] = NULL;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteObjectGeometryH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_proxies_ions_h5( vector<p3d> const & pp3, rangeTable & rng )
{
	double tic = MPI_Wtime();

	if ( objs.size() >= (size_t) UMX ) {
		cerr << "The total number of objects to write out is larger than currently supported by the implementation !" << "\n";
		return false;
	}
	if ( objs.size() < 1 ) {
		cout << "WARNING:: There are no objects to report !" << "\n";
		return true;
	}
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	string prefix = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles/volumetric_features";

	size_t MinIonSupport = ConfigNanochem::MinIonSupportForClosedObjects;
	vector<apt_uint> uint;
	vector<double> real;

	vector<size_t> prxy_interior_idx;
	vector<size_t> prxy_exterior_idx;
	//these vectors store indices to non-NULL pointers on objs that point to specific type of proxies not their IDs!
	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			if ( objs[i]->is_proxy_interior( MinIonSupport ) == true ) {
				prxy_interior_idx.push_back(i);
			}
			if ( objs[i]->is_proxy_exterior( MinIonSupport ) == true ) {
				prxy_exterior_idx.push_back(i);
			}
		}
	}

	if ( prxy_interior_idx.size() > 0 ) {
		string grpnm_interior = prefix + "/proxies_far_from_edge";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXprocess") );
		if ( h5w.nexus_write_group( grpnm_interior, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = grpnm_interior + "/feature_identifier";
		for( auto it = prxy_interior_idx.begin(); it != prxy_interior_idx.end(); it++ ) {
			uint.push_back( objs[*it]->info.obj_id );
		}
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		dsnm = grpnm_interior + "/volume";
		for( auto it = prxy_interior_idx.begin(); it != prxy_interior_idx.end(); it++ ) {
			real.push_back( objs[*it]->info.obj_volume );
		}
		anno = ioAttributes();
		anno.add( "unit", string("nm^3") );
		if ( h5w.nexus_write(
			dsnm,
			io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			real,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		real = vector<double>();

		//if ( info.hasProxiesOBB == true ) {}

		grpnm = grpnm_interior + "/composition";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXchemical_composition") );
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

		vector<apt_uint> total = vector<apt_uint>( objs.size(), 0 );
		for ( auto itypit = rng.iontypes.begin(); itypit != rng.iontypes.end(); itypit++ ) {
			unsigned char itypid = itypit->first;

			grpnm = grpnm_interior + "/composition/ion" + to_string((int) itypid);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXion") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/charge_state";
			apt_int chrg = 0;
			if ( itypit->second.charge_sgn == MYPOSITIVE ) {
				chrg = (int) + 1 * (int) itypit->second.charge_state;
			}
			if ( itypit->second.charge_sgn == MYNEGATIVE ) {
				chrg = (int) -1 * (int) itypit->second.charge_state;
			}
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, chrg, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/nuclide_hash";
			vector<unsigned short> u16 = vector<unsigned short>(
					MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				u16[j] = itypit->second.ivec[j];
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
						{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/nuclide_list";
			u16 = vector<unsigned short>( 2 * MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				pair<unsigned char, unsigned char> ZN = isotope_unhash( itypit->second.ivec[j] );
				if ( ZN.second > 0 ) {
					u16[(j*2)+0] = (unsigned short) ZN.first + (unsigned short) ZN.second;
				}
				u16[(j*2)+1] = (unsigned short) ZN.first;
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/count";
			uint = vector<apt_uint>();
			for( auto jt = prxy_interior_idx.begin(); jt != prxy_interior_idx.end(); jt++ ) {
				map<apt_uint, apt_uint>::iterator thisone = objs[*jt]->myres_iontypes.find( itypid );
				if( thisone == objs[*jt]->myres_iontypes.end() ) {
					uint.push_back( 0 );
				}
				else {
					uint.push_back( thisone->second );
					total[*jt] += thisone->second;
				}
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				uint,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			uint = vector<apt_uint>();
		}

		dsnm = grpnm_interior + "/composition/total";
		//total must not be used in-place because it accounts for convenience in the account for all objs!
		for( auto jt = prxy_interior_idx.begin(); jt != prxy_interior_idx.end(); jt++ ) {
			uint.push_back( total[*jt] );
		}
		total = vector<apt_uint>();
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		//evaporation_identifier of ions inside or on the surface of the triangulated surface mesh of each proxies
		//MK::here we also instantiate the group for each objectID(NXcg_face_list_data_structure)
		for( auto jt = prxy_interior_idx.begin(); jt != prxy_interior_idx.end(); jt++ ) {
			grpnm = grpnm_interior + "/object" + to_string(objs[*jt]->info.obj_id);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_polyhedron_set") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			grpnm += "/polyhedron";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_face_list_data_structure") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/ion_identifier";
			//use objs[*jt]->ion_evapid in-place
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({objs[*jt]->ion_evapid.size()}, {objs[*jt]->ion_evapid.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				objs[*jt]->ion_evapid,
				anno ) != MYHDF5_SUCCESS ) { return false; }
		}
	}

	if ( prxy_exterior_idx.size() > 0 ) {
		string grpnm_exterior = prefix + "/proxies_close_to_edge";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXprocess") );
		if ( h5w.nexus_write_group( grpnm_exterior, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = grpnm_exterior + "/feature_identifier";
		for( auto jt = prxy_exterior_idx.begin(); jt != prxy_exterior_idx.end(); jt++ ) {
			uint.push_back( objs[*jt]->info.obj_id );
		}
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		dsnm = grpnm_exterior + "/volume";
		for( auto jt = prxy_exterior_idx.begin(); jt != prxy_exterior_idx.end(); jt++ ) {
			real.push_back( objs[*jt]->info.obj_volume );
		}
		anno = ioAttributes();
		anno.add( "unit", string("nm^3") );
		if ( h5w.nexus_write(
			dsnm,
			io_info({real.size()}, {real.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			real,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		real = vector<double>();

		//if ( info.hasProxiesOBB == true ) {}

		grpnm = grpnm_exterior + "/composition";
		anno = ioAttributes();
		anno.add( "NX_class", string("NXchemical_composition") );
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

		vector<apt_uint> total = vector<apt_uint>( objs.size(), 0 );
		for ( auto itypit = rng.iontypes.begin(); itypit != rng.iontypes.end(); itypit++ ) {
			unsigned char itypid = itypit->first;

			grpnm = grpnm_exterior + "/composition/ion" + to_string((int) itypid);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXion") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/charge_state";
			apt_int chrg = 0;
			if ( itypit->second.charge_sgn == MYPOSITIVE ) {
				chrg = (int) + 1 * (int) itypit->second.charge_state;
			}
			if ( itypit->second.charge_sgn == MYNEGATIVE ) {
				chrg = (int) -1 * (int) itypit->second.charge_state;
			}
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, chrg, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/nuclide_hash";
			vector<unsigned short> u16 = vector<unsigned short>(
					MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				u16[j] = itypit->second.ivec[j];
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
						{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/nuclide_list";
			u16 = vector<unsigned short>( 2 * MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
			for( int j = 0; j < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; j++ ) {
				pair<unsigned char, unsigned char> ZN = isotope_unhash( itypit->second.ivec[j] );
				if ( ZN.second > 0 ) {
					u16[(j*2)+0] = (unsigned short) ZN.first + (unsigned short) ZN.second;
				}
				u16[(j*2)+1] = (unsigned short) ZN.first;
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						{MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 2},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u16,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			u16 = vector<unsigned short>();

			dsnm = grpnm + "/count";
			uint = vector<apt_uint>();
			for( auto jt = prxy_exterior_idx.begin(); jt != prxy_exterior_idx.end(); jt++ ) {
				map<apt_uint, apt_uint>::iterator thisone = objs[*jt]->myres_iontypes.find( itypid );
				if( thisone == objs[*jt]->myres_iontypes.end() ) {
					uint.push_back( 0 );
				}
				else {
					uint.push_back( thisone->second );
					total[*jt] += thisone->second;
				}
			}
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				uint,
				anno ) != MYHDF5_SUCCESS ) { return false; }
			uint = vector<apt_uint>();
		}

		dsnm = grpnm_exterior + "/composition/total";
		//also for exterior total must not be used in-place
		for( auto jt = prxy_exterior_idx.begin(); jt != prxy_exterior_idx.end(); jt++ ) {
			uint.push_back( total[*jt] );
		}
		total = vector<apt_uint>();
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
		uint = vector<apt_uint>();

		//evaporation_identifier of ions inside or on the surface of the triangulated surface mesh of each proxies
		//MK::here we also instantiate the group for each objectID(NXcg_face_list_data_structure)
		for( auto jt = prxy_exterior_idx.begin(); jt != prxy_exterior_idx.end(); jt++ ) {
			grpnm = grpnm_exterior + "/object" + to_string(objs[*jt]->info.obj_id);
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_polyhedron_set") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			grpnm += "/polyhedron";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_face_list_data_structure") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/ion_identifier";
			//use objs[*jt]->ion_evapid in-place
			anno = ioAttributes();
			if ( h5w.nexus_write(
				dsnm,
				io_info({objs[*jt]->ion_evapid.size()}, {objs[*jt]->ion_evapid.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
				objs[*jt]->ion_evapid,
				anno ) != MYHDF5_SUCCESS ) { return false; }
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteIonsInProxiesH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool isosurfaceHdl::write_proxies_geometry_h5()
{
	double tic = MPI_Wtime();

	if ( objs.size() >= (size_t) UMX ) {
		cerr << "The total number of objects to write out is larger than currently supported by the implementation !" << "\n";
		return false;
	}
	if ( objs.size() < 1 ) {
		cout << "WARNING:: There are no objects to report !" << "\n";
		return true;
	}

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	//##MK::path grpnm should have already been created by now given how this function gets called
	apt_uint entry_id = 1;
	string prefix = "/entry" + to_string(entry_id) + "/delocalization" + to_string(info.dlzid)
		+ "/grid/iso_surface" + to_string(info.isrfid) + "/triangle_soup/triangles/volumetric_features";

	//xdmf support file for rendering all objects inside the dataset
	vector<xdmfBaseHdl*> xdmf;
	for( size_t thisone = 0; thisone < 2; thisone++ ) {
		xdmfBaseHdl* xdmf_ptr = new xdmfBaseHdl();
		if ( xdmf_ptr != NULL ) {
			xdmf.push_back( xdmf_ptr ); //XDMF_INTERIOR
			xdmf.back()->init_collection();
		}
		else {
			for ( size_t ii = 0; ii < thisone; ii++ ) {
				delete xdmf.at(ii); xdmf[ii] = NULL;
			}
			cerr << "Allocation of xdmfBaseHdl objects failed !" << "\n";
			return false;
		}
	}

	size_t MinIonSupport = ConfigNanochem::MinIonSupportForClosedObjects;

	//object supporting points of the Polyhedron
	for( size_t i = 0; i < objs.size(); i++ ) {
		if ( objs[i] != NULL ) {
			if ( objs[i]->is_proxy( MinIonSupport ) == true ) {
				string subgrpnm = "";
				size_t thisone = XDMF_INTERIOR;
				if ( objs[i]->is_proxy_interior( MinIonSupport ) == true ) {
					subgrpnm = prefix + "/proxies_far_from_edge/object" + to_string(objs[i]->info.obj_id) + "/polyhedron";
					thisone = XDMF_INTERIOR;
				}
				else if ( objs[i]->is_proxy_exterior( MinIonSupport ) == true ) {
					subgrpnm = prefix + "/proxies_close_to_edge/object" + to_string(objs[i]->info.obj_id) + "/polyhedron";
					thisone = XDMF_EXTERIOR;
				}
				else {
					continue;
				}

				/* ##MK::group should have already been created !
				anno = ioAttributes();
				anno.add( "NX_class", "NXcg_face_list_data_structure");
				if ( h5w.nexus_write_group( subgrpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
				number_of_vertices(NX_POSINT):
				number_of_faces(NX_POSINT):
				vertex_identifier_offset(NX_UINT):
				*/

				dsnm = subgrpnm + "/vertices";
				vector<apt_real> real;
				for( auto jt = objs[i]->proxy_mesh_vrts.begin(); jt != objs[i]->proxy_mesh_vrts.end(); jt++ ) {
					real.push_back( jt->x );
					real.push_back( jt->y );
					real.push_back( jt->z );
				}
				anno = ioAttributes();
				anno.add( "unit", string("nm") );
				if ( h5w.nexus_write(
					dsnm,
					io_info({real.size() / 3, 3}, {real.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
					real,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				real = vector<apt_real>();

				dsnm = subgrpnm + "/faces";
				vector<apt_uint> uint;
				for( auto jt = objs[i]->proxy_mesh_fcts_trgls.begin(); jt != objs[i]->proxy_mesh_fcts_trgls.end(); jt++ ) {
					uint.push_back( jt->v1 );
					uint.push_back( jt->v2 );
					uint.push_back( jt->v3 );
				}
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({uint.size() / 3, 3}, {uint.size() / 3, 3}, MYHDF5_COMPRESSION_GZIP, 0x01),
					uint,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				uint = vector<apt_uint>();

				dsnm = subgrpnm + "/xdmf_topology";
				uint = vector<apt_uint>(
						objs[i]->proxy_mesh_fcts_trgls.size() * (1+1+3), 3 ); //xdmf keyword, three facets
				size_t k = 2;
				for( auto jt = objs[i]->proxy_mesh_fcts_trgls.begin(); jt != objs[i]->proxy_mesh_fcts_trgls.end(); jt++ ) {
					uint[k+0] = jt->v1;
					uint[k+1] = jt->v2;
					uint[k+2] = jt->v3;
					k += (1+1+3);
				}
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
					uint,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				uint = vector<apt_uint>();

				dsnm = subgrpnm + "/xdmf_feature_identifier";
				uint = vector<apt_uint>( objs[i]->proxy_mesh_fcts_trgls.size(), objs[i]->info.obj_id );
				anno = ioAttributes();
				if ( h5w.nexus_write(
					dsnm,
					io_info({uint.size()}, {uint.size()}, MYHDF5_COMPRESSION_GZIP, 0x01),
					uint,
					anno ) != MYHDF5_SUCCESS ) { return false; }
				uint = vector<apt_uint>();

//add XDMF visualization support
				xdmf[thisone]->add_collection_entry();
				xdmf[thisone]->add_collection_grid_uniform(
					"object" + to_string(objs[i]->info.obj_id) );
				xdmf[thisone]->add_collection_geometry_xyz(
					{objs[i]->proxy_mesh_vrts.size(), 3}, H5_F32,
					strip_path_prefix(ConfigShared::OutputfileName) + ":" + subgrpnm + "/vertices" );
				xdmf[thisone]->add_collection_topology_mixed(
					objs[i]->proxy_mesh_fcts_trgls.size(),
					{objs[i]->proxy_mesh_fcts_trgls.size() * (1+1+3), 1}, H5_U32,
					strip_path_prefix(ConfigShared::OutputfileName) + ":" + subgrpnm + "/xdmf_topology" );
				xdmf[thisone]->add_collection_attribute_scalar(
					"object_identifier", "Cell",
					{objs[i]->proxy_mesh_fcts_trgls.size(), 1}, H5_U32,
					strip_path_prefix(ConfigShared::OutputfileName) + ":" + subgrpnm + "/xdmf_feature_identifier" );
			}
		}
	}

	for( size_t thisone = 0; thisone < 2; thisone++ ) {
		string xmlfn = "";
		if ( thisone == (size_t) XDMF_INTERIOR ) {
			xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id)
				+ ".DelocTaskID." + to_string(info.dlzid) + ".IsoSrfTaskID." + to_string(info.isrfid) + ".ProxiesFarFromEdgeGeometry.xdmf";
		}
		if ( thisone == (size_t) XDMF_EXTERIOR ) {
			xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id)
				+ ".DelocTaskID." + to_string(info.dlzid) + ".IsoSrfTaskID." + to_string(info.isrfid) + ".ProxiesCloseToEdgeGeometry.xdmf";
		}
		xdmf[thisone]->write_collection( xmlfn );
		delete xdmf[thisone]; xdmf[thisone] = NULL;
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot(); //isosurf_tictoc.get_memoryconsumption();
	isosurf_tictoc.prof_elpsdtime_and_mem( "WriteProxiesGeometryH5DelocTask" + to_string(info.dlzid) + "IsrfTask" + to_string(info.isrfid), APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}

/*
//##MK::inspect a8b4fca0f8fc85bbf4b126ac17753ebeb5efa117 for code snippets of deprecated functions
bool isosurfaceHdl::write_automated_rois_for_surface_patches_h5( rangeTable & rng )
bool isosurfaceHdl::write_automated_rois_for_closed_polyhedra_h5( rangeTable & rng )
*/
