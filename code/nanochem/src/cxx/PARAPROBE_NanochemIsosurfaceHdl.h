/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_ISOSURF_HDL_H__
#define __PARAPROBE_NANOCHEM_ISOSURF_HDL_H__


#include "PARAPROBE_NanochemManifoldHdl.h"


class isosurfaceHdl
{
public:
	isosurfaceHdl();
	~isosurfaceHdl();

	bool voxelize_delocalize_gaussian_fast_per_iontype_multithreaded(
			vector<unsigned char> const & wdw, vector<p3d> const & pp3,
					vector<p3dinfo> const & ityps, rangeTable & rngifo );
	bool voxelize_delocalize_compose_from_iontype( rangeTable & rngifo );

	bool compute_scalarfield_gradient3d();
	bool build_iso_surface();
	bool compute_directed_triangle_normals( const apt_real grad3dmin );
	bool approximate_unclear_triangle_normals( const apt_real eps );
	void check_closure_iso_surface();
	bool find_connected_regions( const apt_real dminmax );
	bool find_closed_polyhedra_and_proxies_for_objects_with_holes(
			vector<p3d> const & pp3, vector<p3dinfo> const & ityps, const size_t nityps );
	bool build_closed_polyhedra_bvh();

	/*
	//older versions of the first function below had limited scalability when there is at least one object with a volume comparable to more than 1-10% of the dataset
	//in this case the querying of ions in this object is still eventually efficient but as the above function employs no multithreading
	//at the object level but only the processing of the list of objects is delegated to threads it can take a much longer time to
	//process the very large object sequentially while all other threads have already completed the processing of the remaining smaller objects
	//however enforcing now a multithreaded ion in tetrahedron testing for each object is not smart because it would enforce eventually
	//too many barriers or more complicated memory management for buffers
	//so in this alternative implementation we make a two-step processing, first we find the number of candidate ions for each object
	//those with less than a threshold fraction of the total number of ions in the dataset get processed sequentially while threads process different
	//small objects, larger objects get queue into a list for the second step. therein we then process each object via multithreading to increase
	//the amount of time we spent in parallel and speed up thereby the processing of large objects
	//so the function implements a mixture of per-object-multithreading and per-object-ensemble multithreading
	//even better would be to use rotated bounding boxes and even smarter load balancing heuristics
	//the above functions still have as the bottleneck and potential source of inaccuracy as TetGen is required
	//to compute a constrainted tetrahedralization, as TetGen is a sequential library this can be a bottleneck
	*/
	bool find_ions_in_closed_polyhedra_npoly_times_log_nions_no_tetrahedralization_multithreading(
			vector<unsigned char> const & wdw, vector<p3d> const & pp3,
				vector<p3dinfo> const & ityps, rangeTable & rng, LinearTimeVolQuerier<p3dmidx> & ionbvh,
					vector<tri3d> const & edgemesh, const bool check_for_edge_contact, const apt_real edge_contact_dist_threshold );
	bool find_ions_in_proxies_npoly_times_log_nions_no_tetrahedralization_multithreading(
			vector<unsigned char> const & wdw, vector<p3d> const & pp3,
				vector<p3dinfo> const & ityps, rangeTable & rng, LinearTimeVolQuerier<p3dmidx> & ionbvh,
				vector<tri3d> const & edgemesh, const bool check_for_edge_contact, const apt_real edge_contact_dist_threshold );
	//so in the no_tetrahedralization version we work directly with the triangle mesh of the closed object and perform a point-in-object test
	//these test can run in parallel, thus removing the TetGen bottleneck

	bool write_iontype_specific_delocalization_result(
		size_t const ityp, rangeTable & rng, vector<double> & cnts_f64 );
	bool write_delocalization_grid();
	bool write_delocalization_window( vector<p3dinfo> const & iinfo );
	bool write_scalar_field_and_gradient_h5();

	bool write_isrf_complex_h5();
	bool write_triangle_soup_clusterid_h5();

	bool write_objects_properties_h5();
	bool write_objects_ions_h5( vector<p3d> const & pp3, rangeTable & rng );
	bool write_objects_geometry_h5();

	bool write_proxies_ions_h5( vector<p3d> const & pp3, rangeTable & rng );
	bool write_proxies_geometry_h5();

	voxelgrid gridinfo;
	vector<apt_real> scalarfield;			//implicit 3d array, (x+y*NX+z*NX*NY), holding composition
	vector<apt_real> scalarfield_grad3d;	//implicit 3d array of triplets (0,1,2+3*(x+y*NX+z*NX*NY)) for x,y, z components
	vector<unsigned char> scalarfield_edge;
	MarchingCubes mc;
	triangleSoupClustering tscl;
	vector<roi_rotated_cuboid*> rois;
	vector<manifoldHdl*> objs;
	Tree* closed_polyhedra_bvh;
	isosurf_task_info info;
	profiler isosurf_tictoc;
};

#endif
