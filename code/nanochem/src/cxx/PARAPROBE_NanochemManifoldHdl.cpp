/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_NanochemManifoldHdl.h"


manifoldHdl::manifoldHdl()
{
	info = manifold_info();
	/*
	ply_mesh_tets_bvh = NULL;
	*/
}


manifoldHdl::~manifoldHdl()
{
	//##MK::make sure memory for locals is really freed
	/*
	if ( ply_mesh_tets_bvh != NULL ) {
		delete ply_mesh_tets_bvh; ply_mesh_tets_bvh = NULL;
	}
	*/

	/*
	for( size_t i = 0; i < ply_rois_obb.size(); i++ ) {
		if ( ply_rois_obb[i] != NULL ) {
			for( auto it = ply_rois_obb[i]->ityp_proxigrams.begin(); it != ply_rois_obb[i]->ityp_proxigrams.end(); it++ ) {
				if ( it->second != NULL ) {
					delete it->second; it->second = NULL;
				}
			}
			ply_rois_obb[i]->ityp_proxigrams = map<unsigned char,vector<apt_real>*>();
			for( auto jt = ply_rois_obb[i]->elemtyp_proxigrams.begin(); jt != ply_rois_obb[i]->elemtyp_proxigrams.end(); jt++ ) {
				if  ( jt->second != NULL ) {
					delete jt->second; jt->second = NULL;
				}
			}
			ply_rois_obb[i]->elemtyp_proxigrams = map<unsigned char,vector<apt_real>*>();
			delete ply_rois_obb[i]; ply_rois_obb[i] = NULL;
		}
	}
	ply_rois_obb = vector<roi_rotated_cuboid*>();
	*/

	for( size_t i = 0; i < ply_rois_cyl.size(); i++ ) {
		if ( ply_rois_cyl[i] != NULL ) {
			for( auto it = ply_rois_cyl[i]->ityp_proxigrams.begin(); it != ply_rois_cyl[i]->ityp_proxigrams.end(); it++ ) {
				if ( it->second != NULL ) {
					delete it->second; it->second = NULL;
				}
			}
			ply_rois_cyl[i]->ityp_proxigrams = map<unsigned char,vector<apt_real>*>();
			for( auto jt = ply_rois_cyl[i]->elemtyp_proxigrams.begin(); jt != ply_rois_cyl[i]->elemtyp_proxigrams.end(); jt++ ) {
				if  ( jt->second != NULL ) {
					delete jt->second; jt->second = NULL;
				}
			}
			ply_rois_cyl[i]->elemtyp_proxigrams = map<unsigned char,vector<apt_real>*>();
			delete ply_rois_cyl[i]; ply_rois_cyl[i] = NULL;
		}
	}
	ply_rois_cyl = vector<roi_rotated_cylinder*>();

	//##MK::add support for patch_rois_obb = vector<roi_rotated_obb*>();

	for( size_t i = 0; i < patch_rois_cyl.size(); i++ ) {
		if ( patch_rois_cyl[i] != NULL ) {
			for( auto it = patch_rois_cyl[i]->ityp_proxigrams.begin(); it != patch_rois_cyl[i]->ityp_proxigrams.end(); it++ ) {
				if ( it->second != NULL ) {
					delete it->second; it->second = NULL;
				}
			}
			patch_rois_cyl[i]->ityp_proxigrams = map<unsigned char,vector<apt_real>*>();
			for( auto jt = patch_rois_cyl[i]->elemtyp_proxigrams.begin(); jt != patch_rois_cyl[i]->elemtyp_proxigrams.end(); jt++ ) {
				if  ( jt->second != NULL ) {
					delete jt->second; jt->second = NULL;
				}
			}
			patch_rois_cyl[i]->elemtyp_proxigrams = map<unsigned char,vector<apt_real>*>();
			delete patch_rois_cyl[i]; patch_rois_cyl[i] = NULL;
		}
	}
	patch_rois_cyl = vector<roi_rotated_cylinder*>();
}


void manifoldHdl::fetch_triangles( apt_uint clustID, triangleSoupClustering const & triHdl )
{
	//eventually called from within threaded region
	if ( triHdl.fcts2objid.size() != triHdl.fcts.size() ) {
		cerr << "manifoldHdl::fetch_triangles thread " << omp_get_thread_num() << " fcts2objid.size() != fcts.size() !" << "\n";
		return;
	}

	//cout << "manifoldHdl::fetch triangles thread " << omp_get_thread_num() << " clustID " << clustID << "\n";
	triangles = vector<tri3d>();
	fcts_normal = vector<p3d>();
	fcts_normal_quality = vector<unrm_info>();
	for( auto it = triHdl.fcts2objid.begin(); it != triHdl.fcts2objid.end(); it++ ) {
		if ( *it != clustID ) {
			continue;
		}
		else {
			tri3u thisone = triHdl.fcts[it - triHdl.fcts2objid.begin()];
			triangles.push_back( tri3d(
					triHdl.vrts[thisone.v1].x, triHdl.vrts[thisone.v1].y, triHdl.vrts[thisone.v1].z,
					triHdl.vrts[thisone.v2].x, triHdl.vrts[thisone.v2].y, triHdl.vrts[thisone.v2].z,
					triHdl.vrts[thisone.v3].x, triHdl.vrts[thisone.v3].y, triHdl.vrts[thisone.v3].z ) );
			fcts_normal.push_back( triHdl.nrms[it - triHdl.fcts2objid.begin()] );
			fcts_normal_quality.push_back( triHdl.nrms_quality[it - triHdl.fcts2objid.begin()] );
		}
	}

	//#pragma omp critical
	//{
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::fetch_triangles thread " << omp_get_thread_num() << " cluster " << clustID << " has " << triangles.size() << " triangles with fcts_normal.size() " << fcts_normal.size() << "\n";
#endif
	//}
}


//required for computing approximate optimal bounding boxes

int manifoldHdl::characterize_triangle_soup_convertability_to_mesh()
{
	double ctic = 0.0;
	double ctoc = 0.0;

	ctic = omp_get_wtime();

	info.obj_typ = OBJECT_IS_UNKNOWNTYPE;

	if ( triangles.size() < 1 ) {
		return TRIANGLE_SOUP_ANALYSIS_NO_SUPPORT;
	}
	if ( triangles.size() < 4 ) { //smallest closed polyhedron is a tetrahedron
		return TRIANGLE_SOUP_ANALYSIS_INSUFFICIENT_SUPPORT;
	}

	//MK::not every triangle mesh is a closed polyhedron, there can be problems, like inconsistent orientation of triangles, holes in the mesh, i.e. missing triangles, connectivity issues
	//MK::therefore a sophisticated mesh processing is needed, here using the CGAL library, to understand better for which triangle clusters we can at all define an inner/outer unit normal
	//MK::only for those objects we can extract signed distances parallel to the normal to the contour facets without taking into account the underlying concentration fields of iontypes
	//https://doc.cgal.org/latest/Polygon_mesh_processing/Polygon_mesh_processing_2repair_polygon_soup_example_8cpp-example.html
	std::vector<std::array<FT, 3> > points;
	std::vector<CGAL_Polygon> polygons;
	for ( auto it = triangles.begin(); it != triangles.end(); it++ ) {
		points.push_back( CGAL::make_array<FT>( it->x1, it->y1, it->z1 ) );
		points.push_back( CGAL::make_array<FT>( it->x2, it->y2, it->z2 ) );
		points.push_back( CGAL::make_array<FT>( it->x3, it->y3, it->z3 ) );
	}
	CGAL_Polygon p;
	size_t vertexID = 0;
	for ( auto kt = triangles.begin(); kt != triangles.end(); kt++ ) {
		p.clear();
		p.push_back( vertexID ); vertexID++;
		p.push_back( vertexID ); vertexID++;
		p.push_back( vertexID ); vertexID++;
		polygons.push_back( p );
	}

	//##MK::could here reduce the memory footprint when we avoid passing and processing triangles as tri3d but instead passing as unique vertex ensemble and vertex ID triplet ensemble ...

	ctoc = omp_get_wtime();
	info.dt_init_cgal = (ctoc-ctic);
	ctic = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	ctoc = omp_get_wtime();
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " handshaking data between triangleClusterer class instance and CGAL interfacing took " << (ctoc-ctic) << "\n";
	info.dt_init_cgal = (ctoc-ctic);
	ctic = omp_get_wtime();
#endif

	//build and repair a surface mesh
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " initialized the triangle cluster has " << points.size() << " vertices and " << polygons.size() << " faces" << "\n";
#endif

	//MK::combinatorial repairing eventually modifies points and polygon arrays
	PMP::repair_polygon_soup(points, polygons, CGAL::parameters::geom_traits(Array_traits()));

	ctoc = omp_get_wtime();
	info.dt_trisoup_repair = (ctoc-ctic);
	ctic = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " combinatorial repairing of the triangle cluster took " << (ctoc-ctic) << " resulting in " << points.size() << " vertices and " << polygons.size() << " faces" << "\n";
#endif

	if ( points.size() < 4 || polygons.size() < 4 ) {
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		#pragma omp critical
		{
			cerr << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " detected a case where after the combinatorial repairing not even a tetrahedron remained !" << "\n";
		}
#endif
		info.obj_typ = OBJECT_COMBINATORIAL_REPAIRING_WAS_FAULTY;
		return TRIANGLE_SOUP_ANALYSIS_ERROR_REPAIRING;
	}
	else {
		//repairing was possible so the triangle soup can at least represent a surface patch
		info.obj_typ = OBJECT_IS_SURFACEMESH;
	}

	//but can it represent more, we know now that this mesh is a surface_mesh
	//but not every surface mesh is also a (closed) Polyhedron
	//e.g. take a triangle strip: that strip can be surface_mesh but that strip is not a necessarily a polyhedron, maybe a polyhedron cut open and unfolded but definately not a polyhedron...
	//a surface_mesh is also not necessarily built only from triangles could also be build from prims like quads or other type of polygons

	Mesh mesh;
	if ( PMP::orient_polygon_soup(points, polygons) == true ) {
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " orient_polygon_soup succeeded" << "\n";
#endif
	}
	else {
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		#pragma omp critical
		{
			cerr << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " orient_polygon_soup is self-intersecting" << "\n";
		}
#endif
		return TRIANGLE_SOUP_ANALYSIS_SELFINTERSECTING;
	}

	ctoc = omp_get_wtime();
	info.dt_trisoup_orient = (ctoc-ctic);
	ctic = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	ctoc = omp_get_wtime();
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " orient_polygon_soup took " << (ctoc-ctic) << "\n";
	ctic = omp_get_wtime();
#endif

	PMP::polygon_soup_to_polygon_mesh(points, polygons, mesh);

	ctoc = omp_get_wtime();
	info.dt_trisoup_tomesh = (ctoc-ctic);
	ctic = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	ctoc = omp_get_wtime();
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " converting the soup to a surface mesh took " << (ctoc-ctic) << " resulting in " << num_vertices(mesh) << " vertices and " << num_faces(mesh) << " faces" << "\n";
#endif
	/*
	//Number the faces because 'orient_to_bound_a_volume' needs a face <--> index map
	int index = 0;
	for( Polyhedron::Face_iterator fb = mesh.facets_begin(), fe = mesh.facets_end(); fb != fe; ++fb ) {
		fb->id() = index++;
	}
 	*/

	//##MK::in a seemingly ackward move to eventually check if the volume is bounded I go from Surface_mesh to Polyhedron (3D Polyhedral mesh processing)
	//##MK::here eventually copies can be improved but that seems to me need expert knowledge of CGAL, that I dont have at the moment to spent time on and optimize...

	vector<Point> ply_points; //K::Point_3> ply_points;
	vector<vector<size_t>> ply_polygons;

	//convert the Surface mesh into a polyhedron with which to test
	//cout << "Mesh::Vertex range " << "\n";
	Mesh::Vertex_range vrng = mesh.vertices();
	Mesh::Vertex_range::iterator vb = vrng.begin();
	Mesh::Vertex_range::iterator ve = vrng.end();
	for(   ; vb != ve; ++vb ) {
		//size_t vertexID = *vb;
		//cout << vertexID << "\t\t" << mesh.point(*vb).x() << "\t\t" << mesh.point(*vb).y() << "\t\t" << mesh.point(*vb).z() << "\n";

		ply_points.push_back( Point( mesh.point(*vb).x(), mesh.point(*vb).y(), mesh.point(*vb).z() ) ); //Point_3( mesh.point(*vb).x(), mesh.point(*vb).y(), mesh.point(*vb).z() ) );
	}

	//cout << "Mesh::Face range " << "\n";
	Mesh::Face_range frng = mesh.faces();
	Mesh::Face_range::iterator fb = frng.begin();
	Mesh::Face_range::iterator fe = frng.end();
	//size_t vertexID = 0;
	for(   ; fb != fe; ++fb ) {

		Polygon p;

		//https://doc.cgal.org/latest/Surface_mesh/index.html#circulators_example

		//size_t faceID = *fb;
		//cout << "FaceID " << faceID << "\n";

		/*
		// or the same again, but directly with a range based loop
		for( Mesh::Vertex_index vd : vertices_around_face(mesh.halfedge(*fb), mesh) ) {
			cout << vd << "\t\t";
		}
		cout << "\n";
		*/

	    CGAL::Vertex_around_face_iterator<Mesh> vbegin, vend;
	    for(boost::tie(vbegin, vend) = vertices_around_face(mesh.halfedge(*fb), mesh); vbegin != vend; ++vbegin ) {
	    	//cout << *vbegin << "\t\t";
	    	p.push_back( (size_t) *vbegin );
	    }
	    //cout << "\n";
	    ply_polygons.push_back( p );
	}

	//cout << "Compare to off formatted output " << "\n";
	//cout << mesh << "\n";

	//https://gist.github.com/afabri/f1fa1552e8421c95d35a52f471429d35

	//so far all we have is a polygon soup, meaning we have no connectivity information so far
	//##MK::or am I wrong and there is some redundancy in the processing above that we could get rid of?)

	//Polyhedron ply_mesh;
	PMP::polygon_soup_to_polygon_mesh( ply_points, ply_polygons, ply_mesh );

	ctoc = omp_get_wtime();
	info.dt_polygon_tomesh = (ctoc-ctic);
	ctic = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " building the polygon soup to polygon mesh using the surface_mesh vertex and facet order OFF-file format took " << (ctoc-ctic) << "\n";
#endif

	// Number the faces because 'orient_to_bound_a_volume' needs a face <--> index map
	int index = 0;
	for( Polyhedron::Face_iterator ply_fb = ply_mesh.facets_begin(), ply_fe = ply_mesh.facets_end(); ply_fb != ply_fe; ++ply_fb ) {
		ply_fb->id() = index++;
	}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " indexing the polyhedron/polygon_mesh facets took " << (ctoc-ctic) << " seconds" << "\n";
#endif

	//check if the polygon is closed
	bool ply_mesh_closed = CGAL::is_closed(ply_mesh);

	ctoc = omp_get_wtime();
	info.dt_mesh_closure = (ctoc-ctic);
	ctic = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " checking if the polygon/polygon_mesh is closed took " << (ctoc-ctic) << "\n";
#endif

	if( ply_mesh_closed == true ) {
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " checking if the polygon/polygon_mesh is closed voted positive" << "\n";
#endif
		//##export polygon mesh when not closed, export polygon mesh after orient_to_bound_a_volume if closed
		//##export no mesh topology only coordinates of unique XYZ vertices and facet vertex ID list in off format, give XDMF topology array
		//#######surface mesh for unclosed

		PMP::orient_to_bound_a_volume(ply_mesh);

		ctoc = omp_get_wtime();
		info.dt_mesh_bound = (ctoc-ctic);
		ctic = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " orienting to bound a volume took " << (ctoc-ctic) << " seconds " << "\n";
#endif

		/*
		//http://cgal-discuss.949826.n4.nabble.com/normal-vector-of-a-facet-td1580004.html
		//the so computed normals result not in the same order as the facets so correct normals
		//will be assigned to the wrong facets, better follow the last hint on above with the parity of the facet
		PMP::compute_face_normals( ply_mesh, boost::make_assoc_property_map(ply_mesh_fcts_ounrm) );


		ctoc = omp_get_wtime();
		info.dt_mesh_ounrm = (ctoc-ctic);
		*/

		info.obj_typ = OBJECT_IS_POLYHEDRON;

		return TRIANGLE_SOUP_ANALYSIS_POLYHEDRON_CLOSED;
	}
	else {
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " checking if the polygon/polygon_mesh is closed voted NEGATIVE, i.e. not closed" << "\n";
#endif
		info.obj_typ = OBJECT_IS_SURFACEMESH;
		info.has_valid_proxy = false;

		//when an object is not a closed (2-)manifold it is possible that the triangles have a very complicated arrangement,
		//it might be possible that these triangles come from an in principle closed microstructural feature/object but
		//which was clipped because of edge effects eventually. Maybe the manifold is then resembling the shape of some (rotated) tea cup
		//anyway, what is inside the tea cup and what outside? there is no clear cut answer to this, because:
		//in the example of a tea cup it depends on the direction of gravity and
		//the rotation of the cup. Indeed, if we pour tea into the cup the cup will hold some
		//amount of tea until the cup is full. The problem is that then the cup is full depends on the
		//number of holes, where they are etc., maybe the cup may leak earlier if it holds at all some tea
		//Applied to the analysis here what means in this context the manifold contains ions?
		//Evidently there is no clear cut answer, the problem is caused in atom probe by the fact that the dataset is
		//finite, so we have in most cases an edge effect. Unfortunately many atom probe datasets are so small that they
		//hardly contain many closed objects completely. Especially not when the objects are large compared to the volume
		//of the dataset. Therefore scientsts might still be interested in accepting that objects are truncated and just "wish to close them"
		//so that they can still analyze the dataset "somehow". Clearly, this is not without criticism but if documented and reproducible
		//taking clipped objects into account just gives a different bias to an analysis which is anyway biased for the following reason
		//The really clean solution is that the analysis volume has to be large enough that the characterized statistics has converged
		//reasonably well asymptotically to the true statistics (provided the sample from which the specimen was taken is at all reasonably large...

		//we should also not forget though that even if we cannot find a proxy polyhedron which is closed the surface patch
		//can still be useful for analyses, e.g. when it is a free-standing surface patch, like it was documented in the CGM paper for D. Mayweg dataset

		//so lets try to find a proxy

		//we employ a two-fold strategy to handle open objects:
		//first we attempt to iteratively close all holes with fairing
		//##MK::maybe we can continue here with the Mesh mesh instead we make a copy as we want to keep the proxy's mesh for further analysis
		//##MK::at this point we no longer need mesh!
		proxy_mesh = mesh;
		mesh = Mesh();

		//we checked already that this mesh is not self-intersecting

		//both of these must be positive in order to be considered
		//double max_hole_diam   = (argc > 2) ? boost::lexical_cast<double>(argv[2]): -1.0;
		//int max_num_hole_edges = (argc > 3) ? boost::lexical_cast<int>(argv[3]) : -1;
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		unsigned int nb_holes = 0;
#endif
		vector<halfedge_descriptor> border_cycles;

		//collect one halfedge per boundary cycle
		PMP::extract_boundary_cycles(proxy_mesh, std::back_inserter(border_cycles));

		//if there are no holes this loop will not be executed
		for(halfedge_descriptor h : border_cycles) {
			//if ( max_hole_diam > 0 && max_num_hole_edges > 0 && !is_small_hole(h, mesh, max_hole_diam, max_num_hole_edges) ) {
			//	continue;
			//}

			vector<sm_face_descriptor>  patch_facets;
			vector<sm_vertex_descriptor> patch_vertices;

			//##MK::eventually refine only

			bool fairing_status = std::get<0>(PMP::triangulate_refine_and_fair_hole(
					proxy_mesh, h, std::back_inserter(patch_facets), std::back_inserter(patch_vertices)));

#ifdef MANIFOLD_ANALYSIS_VERBOSE
			cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " patch_facets.size() " << patch_facets.size() << "\n";
			cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " patch_vertices.size() " << patch_vertices.size() << "\n";
			cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " fairing success ? " << (int) fairing_status << "\n";
#endif
			if ( fairing_status == false ) {

				//info.obj_typ = OBJECT_IS_SURFACEMESH_FAIRING_WAS_FAULTY;
				proxy_mesh = Mesh();
				return TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH; //TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH_FAIRING_ERROR;
			}
#ifdef MANIFOLD_ANALYSIS_VERBOSE
			nb_holes++;
#endif
		}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " number of holes filled " << nb_holes << "\n";
#endif

		//##MK::if this is turns out to be unsuccessful, we compute a convex hull
		//##MK::if this is turns out to be unsuccessful, we compute a 3D alpha wrapping

		ctoc = omp_get_wtime();
		info.dt_mesh_proxy = (ctoc-ctic);
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " hole filling, refinement, and fairing took " << (ctoc-ctic) << " seconds " << "\n";
#endif

		//check if the identified proxy is really a closed polyhedron now
		bool proxy_mesh_closed = CGAL::is_closed(proxy_mesh);

		ctoc = omp_get_wtime();
		//info.dt_prox_closure = (ctoc-ctic);
		ctic = omp_get_wtime();
//#ifdef MANIFOLD_ANALYSIS_VERBOSE
//		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " checking if the polygon/polygon_mesh is closed took " << (ctoc-ctic) << "\n";
//#endif

		if( proxy_mesh_closed == true ) {
//#ifdef MANIFOLD_ANALYSIS_VERBOSE
//			cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " checking if the polygon/polygon_mesh is closed voted positive" << "\n";
//#endif

			PMP::orient_to_bound_a_volume(proxy_mesh);

			ctoc = omp_get_wtime();
			//info.dt_mesh_bound = (ctoc-ctic);
			ctic = omp_get_wtime();
//#ifdef MANIFOLD_ANALYSIS_VERBOSE
//		cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " cluster " << info.obj_id << " orienting to bound a volume took " << (ctoc-ctic) << " seconds " << "\n";
//#endif
			info.has_valid_proxy = true;
			//keep the proxy's mesh for downstream processing
		}
		else {
			proxy_mesh = Mesh();
			//info.obj_typ = OBJECT_IS_SURFACEMESH_HOLEFILLING_FAULTY;
			//return TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH;
		}

		//info.obj_type will have the value OBJECT_IS_SURFACEMESH
		return TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH; //_WITH_PROXY;
	}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "\n";
#endif

	//cout << mesh << "\n";
	/*
	ctoc = omp_get_wtime();
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " numbering the faces took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = omp_get_wtime();

	if( CGAL::is_closed(mesh) == true ) {
	cout << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " closure check positive" << "\n";

	PMP::orient_to_bound_a_volume(mesh);
	}
	else {
	cerr << "manifoldHdl::characterize_triangle_soup_convertability_to_mesh thread " << omp_get_thread_num() << " closure check negative" << "\n";
	return TRIANGLE_SOUP_ANALYSIS_BORDEREDGES;
	}
	*/

	//we should not get here, then sth was really unexpected or going wrong
	info.obj_typ = OBJECT_IS_UNKNOWNTYPE;
	return TRIANGLE_SOUP_ANALYSIS_ERROR;
}


void manifoldHdl::characterize_surface_patch()
{
	double ctic = omp_get_wtime();

	//extract the triangle hull of the mesh
	//patch_vrts = vector<p3d>();
	patch_fcts_trgls = vector<tri3d>();
	patch_fcts_nrm_grad3d = vector<p3d>();
	for( size_t triID = 0; triID < triangles.size(); triID++ ) {
		//filter out triangles with insufficient normal quality
		if ( fcts_normal_quality[triID].SQRmagn >= SQR(ConfigNanochem::MinScalarFieldGradientMagnitude) ) {
			patch_fcts_trgls.push_back( triangles[triID] );
			patch_fcts_nrm_grad3d.push_back( fcts_normal[triID] );
		}
	}

	//##MK::we ignore the CGAL processed results and load just the original triangles from the triangle soup
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	//cout << "patch_vrts.size() " << patch_vrts.size() << "\n";
	cout << "patch_fcts_trgls.size() " << patch_fcts_trgls.size() << "\n";
	cout << "patch_fcts_nrm_grad3d.size() " << patch_fcts_nrm_grad3d.size() << "\n";
#endif

	double ctoc = omp_get_wtime();
	info.dt_obj_mesh = (ctoc-ctic);
}


void manifoldHdl::characterize_closed_polyhedron_mesh()
{
	//MK::REQUIRES CURRENTLY THAT MESH IS CLOSED AND COMING FROM A CONSISTENTLY BUILD CGAL Polyhedron 3 with oriented facets

	/*
	if ( CGAL::is_pure_triangle(ply_mesh) == false ) {
		cerr << "Polyhedron mesh of the object is has not only triangles as mesh objects!" << "\n";
		return;
	}
	*/

	double ctic = omp_get_wtime();

	//extract the triangle hull of the mesh
	ply_mesh_vrts = vector<p3d>();
	ply_mesh_fcts_trgls = vector<tri3u>();
	//ply_mesh_fcts_quads = vector<quad3u>();

	/*
	 * map<Eigen::Vector3d, apt_uint, vectorComparator> p3d2oid; //finding the unique points and giving them new IDs

	for ( size_t f = 0; f < facets.size(); f++ ) {
		//To have a consistent orientation of the facet, always consider an exterior cell
		if ( as.classify( facets[f].first ) != Alpha_shape_3::EXTERIOR ) {
			facets[f] = as.mirror_facet( facets[f] );
		}
		CGAL_assertion( as.classify(facets[f].first) == Alpha_shape_3::EXTERIOR );

		int indices[3] = { (facets[f].second+1)%4, (facets[f].second+2)%4, (facets[f].second+3)%4 };

		//according to the encoding of vertex indices, this is needed to get a consistent orientation
		if ( facets[f].second%2 != 0 ) {
		}
		else {
			swap(indices[0], indices[1]);
		}

		//filter out unique points and give new IDs, mainly to avoid storing redundant triangle geometry information (i.e. lossless ##MK data compression)
		Eigen::Vector3d p1( facets[f].first->vertex(indices[0])->point().x(), facets[f].first->vertex(indices[0])->point().y(), facets[f].first->vertex(indices[0])->point().z() );
		map<Eigen::Vector3d, apt_uint>::iterator v1it = p3d2oid.find( p1 );
		apt_uint v1 = UMX;
		if ( v1it == p3d2oid.end() ) { //add a so far unknown point
			p3d2oid.insert( pair<Eigen::Vector3d, apt_uint>(p1, cur_vertex_id) );
			vrts.push_back( p3d64( p1.x(), p1.y(), p1.z() ) );
			v1 = cur_vertex_id;
			cur_vertex_id++;
		}
		else {
			v1 = v1it->second;
		}
		Eigen::Vector3d p2( facets[f].first->vertex(indices[1])->point().x(), facets[f].first->vertex(indices[1])->point().y(), facets[f].first->vertex(indices[1])->point().z() );
		map<Eigen::Vector3d, apt_uint>::iterator v2it = p3d2oid.find( p2 );
		apt_uint v2 = UMX;
		if ( v2it == p3d2oid.end() ) { //add a so far unknown point
			p3d2oid.insert( pair<Eigen::Vector3d, apt_uint>(p2, cur_vertex_id) );
			vrts.push_back( p3d64( p2.x(), p2.y(), p2.z() ) );
			v2 = cur_vertex_id;
			cur_vertex_id++;
		}
		else {
			v2 = v2it->second;
		}
		Eigen::Vector3d p3( facets[f].first->vertex(indices[2])->point().x(), facets[f].first->vertex(indices[2])->point().y(), facets[f].first->vertex(indices[2])->point().z() );
		map<Eigen::Vector3d, apt_uint>::iterator v3it = p3d2oid.find( p3 );
		apt_uint v3 = UMX;
		if ( v3it == p3d2oid.end() ) { //add a so far unknown point
			p3d2oid.insert( pair<Eigen::Vector3d, apt_uint>(p3, cur_vertex_id) );
			vrts.push_back( p3d64( p3.x(), p3.y(), p3.z() ) );
			v3 = cur_vertex_id;
			cur_vertex_id++;
		}
		else {
			v3 = v3it->second;
		}
		fcts.push_back( tri3u( v1, v2, v3 ) );
	 */


	//convert the Surface mesh into a polyhedron with which to test
	//cout << "Mesh::Vertex range " << "\n";
	map<Eigen::Vector3d, apt_uint, vectorComparator> p3d2id; //finding the unique points and giving them new IDs
	apt_uint cur_vertex_id = 0;
	for( Polyhedron::Point_iterator vb = ply_mesh.points_begin(); vb != ply_mesh.points_end(); ++vb ) {
		Eigen::Vector3d v( vb->x(), vb->y(), vb->z() );
		map<Eigen::Vector3d, apt_uint>::iterator vit = p3d2id.find( v );
		if ( vit == p3d2id.end() ) {
			p3d2id.insert( pair<Eigen::Vector3d, apt_uint>( v, cur_vertex_id ) );
			cur_vertex_id++;
		}
		/*else {
			cerr << "The mesh has unexpectedly a duplicated point !" << "\n";
		}*/
	}
	//MK::by virtue of construction are all elements in p3d2id[i]->second on the apt_uint ID interval [0, cur_vertex_id)
	ply_mesh_vrts = vector<p3d>( p3d2id.size(), p3d() );
	for( auto it = p3d2id.begin(); it != p3d2id.end(); it++ ) {
		ply_mesh_vrts[it->second] = p3d( it->first.x(), it->first.y(), it->first.z() );
	}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "ply_mesh_vrts.size() " << ply_mesh_vrts.size() << "\n";
#endif

    //CGAL::VRML_1_ostream out( std::cout);
	//cout << ply_mesh << "\n";

	//cout << "Mesh::Face range " << "\n";
	apt_uint nvrts_per_fct = 0;
	for( Polyhedron::Facet_iterator fctit = ply_mesh.facets_begin(); fctit != ply_mesh.facets_end(); ++fctit ) {

		Polyhedron::Halfedge_around_facet_circulator j = fctit->facet_begin();
		nvrts_per_fct = 0;
		vector<apt_uint> vvisited;
		do
		{
			Eigen::Vector3d v( j->vertex()->point().x(), j->vertex()->point().y(), j->vertex()->point().z() );
			//does this guy exist? It has to otherwise there are problems anyway...
			map<Eigen::Vector3d, apt_uint>::iterator vit = p3d2id.find( v );
			if ( vit != p3d2id.end() ) {
				vvisited.push_back( vit->second );
				nvrts_per_fct++;
			}
			else {
				cerr << "While exporting the polygons of the mesh we unexpectedly faced an unknown point !" << "\n";
				ply_mesh_vrts = vector<p3d>();
				ply_mesh_fcts_trgls = vector<tri3u>();
				//ply_mesh_fcts_quads = vector<quad3u>();
				return;
			}
		} while ( ++j != fctit->facet_begin() ); //circulate around the facet until we reach again the node where we started

		if ( fctit->is_triangle() == true && nvrts_per_fct == 3 ) {
			//cout << "Facet is a triangle" << "\n";
			ply_mesh_fcts_trgls.push_back( tri3u( vvisited[0], vvisited[1], vvisited[2] ) );


			Eigen::Vector3d nxtnxt( 	j->next()->next()->vertex()->point().x(),
										j->next()->next()->vertex()->point().y(),
										j->next()->next()->vertex()->point().z()   );

			Eigen::Vector3d nxt( 		j->next()->vertex()->point().x(),
										j->next()->vertex()->point().y(),
										j->next()->vertex()->point().z()   );

			Eigen::Vector3d cur( 		j->vertex()->point().x(),
										j->vertex()->point().y(),
										j->vertex()->point().z()  );

			Eigen::Vector3d a = nxtnxt - nxt;
			Eigen::Vector3d b = nxt - cur;
			Eigen::Vector3d perp = a.cross( b );
			perp.normalize();

			//Eigen cross product and CGAL opposite, so flip the normal to make normal pointing out of the polyhedron instead of inside
			ply_mesh_fcts_ounrm_eigen.push_back( p3d( -1.*perp.x(), -1.*perp.y(), -1.*perp.z() ) );

			/*
			Point perp = CGAL::cross_product( j->next()->vertex()->point() - j->vertex()->point(),
			           - j->next()->vertex()->point() );
			*/
			//##MK::building on the assumption that for CGAL::closed polyhedra the facets are consistently oriented when viewed from outside

		}
		else if ( fctit->is_quad() == true && nvrts_per_fct == 4 ) {
			cerr << "While exporting the mesh of a polygon we unexpectedly found a quad facet !" << "\n";
			ply_mesh_vrts = vector<p3d>();
			ply_mesh_fcts_trgls = vector<tri3u>();
			//ply_mesh_fcts_quads.push_back( quad3u( vvisited[0], vvisited[1], vvisited[2], vvisited[3] ) );
			//cout << "Facet is a quad" << "\n";
			return;
		}
		else {
			cerr << "While exporting the mesh of a polygon we found a facet with is neither a triangle nor a quad !" << "\n";
			ply_mesh_vrts = vector<p3d>();
			ply_mesh_fcts_trgls = vector<tri3u>();
			//ply_mesh_fcts_quads = vector<quad3u>();
			return;
		}
	}
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	cout << "ply_mesh_fcts_trgls.size() " << ply_mesh_fcts_trgls.size() << "\n";
	//cout << "ply_mesh_fcts_quads.size() " << ply_mesh_fcts_quads.size() << "\n";
#endif

	double ctoc = omp_get_wtime();
	info.dt_obj_mesh = (ctoc-ctic);
}


void manifoldHdl::characterize_surface_patch_proxy_mesh()
{
	//MK::REQUIRES CURRENTLY THAT MESH IS A CLOSED TRIANGLE MESH oriented facets

	double ctic = omp_get_wtime();

	//extract the triangle hull of the mesh
	proxy_mesh_vrts = vector<p3d>();
	proxy_mesh_fcts_trgls = vector<tri3u>();

	if ( info.has_valid_proxy == true ) {
		//##MK::no reduction for disjointness for now...
		//convert the Surface mesh into a polyhedron with which to test
		Mesh::Vertex_range vrng = proxy_mesh.vertices();
		Mesh::Vertex_range::iterator vb = vrng.begin();
		Mesh::Vertex_range::iterator ve = vrng.end();
		for(   ; vb != ve; ++vb ) {
			proxy_mesh_vrts.push_back( p3d(
					proxy_mesh.point(*vb).x(),
					proxy_mesh.point(*vb).y(),
					proxy_mesh.point(*vb).z()) );
		}
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "proxy_mesh_vrts.size() " << proxy_mesh_vrts.size() << "\n";
#endif

		Mesh::Face_range frng = proxy_mesh.faces();
		Mesh::Face_range::iterator fb = frng.begin();
		Mesh::Face_range::iterator fe = frng.end();
		for(   ; fb != fe; ++fb ) {

			vector<apt_uint> vrts;
			//https://doc.cgal.org/latest/Surface_mesh/index.html#circulators_example
		    CGAL::Vertex_around_face_iterator<Mesh> vbegin, vend;
		    for(boost::tie(vbegin, vend) = vertices_around_face(proxy_mesh.halfedge(*fb), proxy_mesh); vbegin != vend; ++vbegin ) {
		    	if ( ((size_t) *vbegin) <= ((size_t) UMX) ) {
		    		vrts.push_back( (apt_uint) *vbegin );
		    	}
		    	else {
					cerr << "While exporting a facet of a proxy's mesh we unexpectedly found a facet whose supporting vertex ID does not map to the prescribed uint32 range !" << "\n";
					proxy_mesh_vrts = vector<p3d>();
					proxy_mesh_fcts_trgls = vector<tri3u>();
					return;
		    	}
		    }
		    if ( vrts.size() == 3 ) {
		    	proxy_mesh_fcts_trgls.push_back( tri3u( vrts[0], vrts[1], vrts[2] ) );
		    }
		    else {
				cerr << "While exporting a facet of a proxy's mesh we unexpectedly faced found a facet which is not a triangle even though the Mesh is a surface mesh !" << "\n";
				proxy_mesh_vrts = vector<p3d>();
				proxy_mesh_fcts_trgls = vector<tri3u>();
				return;
		    }
		}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "proxy_mesh_fcts_trgls.size() " << proxy_mesh_fcts_trgls.size() << "\n";
#endif

	}

	double ctoc = omp_get_wtime();
	info.dt_obj_mesh = (ctoc-ctic);
}


void manifoldHdl::characterize_closed_polyhedron_volume()
{
	double ctic = omp_get_wtime();
	double ctoc = 0.;

	//compute the volume of the mesh
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	ctic = omp_get_wtime();
#endif

	//if ( info.obj_typ == OBJECT_IS_POLYHEDRON ) {
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_closed_polyhedron_volume thread " << omp_get_thread_num() << " cluster " << info.obj_id << "\n";
#endif
		info.obj_volume = PMP::volume(ply_mesh);
	//}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
	ctoc = omp_get_wtime();
	cout << "manifoldHdl::characterize_closed_polyhedron_volume thread " << omp_get_thread_num() << " cluster " << info.obj_id << " volume is " << info.obj_volume << " nm^3" << "\n";
#endif
	ctoc = omp_get_wtime();
	info.dt_obj_vol = (ctoc-ctic);
}


void manifoldHdl::characterize_closed_polyhedron_obb()
{
	double ctic = omp_get_wtime();
	double ctoc = 0.;

	//approximate optimal bounding box
	// Compute the extreme points of the mesh, and then a tightly fitted oriented (i.e. normally rotated) bounding box to the point cloud
	vector<Point> points;
	for( auto it = ply_mesh_vrts.begin(); it != ply_mesh_vrts.end(); it++ ) {
		points.push_back( Point(it->x, it->y, it->z) );
	}

	array<Point, 8> obb_points;
	//CGAL::oriented_bounding_box(sm, obb_points, CGAL::parameters::use_convex_hull(true));
	CGAL::oriented_bounding_box( points, obb_points, CGAL::parameters::use_convex_hull(true));

	//https://doc.cgal.org/latest/Linear_cell_complex/classLinearCellComplex.html#a8786996ef191013fcee5a90a4ceb22ae
	//p0, p1, p2, p3 end up in the same plane, p4, p5, p6, p7 above so
	apt_real p7p4 = SQR(obb_points[7].x() - obb_points[4].x() ) + SQR(obb_points[7].y() - obb_points[4].y() ) + SQR(obb_points[7].z() - obb_points[4].z() );
	apt_real p6p5 = SQR(obb_points[6].x() - obb_points[5].x() ) + SQR(obb_points[6].y() - obb_points[5].y() ) + SQR(obb_points[6].z() - obb_points[5].z() );
	apt_real p1p0 = SQR(obb_points[1].x() - obb_points[0].x() ) + SQR(obb_points[1].y() - obb_points[0].y() ) + SQR(obb_points[1].z() - obb_points[0].z() );
	apt_real p2p3 = SQR(obb_points[2].x() - obb_points[3].x() ) + SQR(obb_points[2].y() - obb_points[3].y() ) + SQR(obb_points[2].z() - obb_points[3].z() );

	apt_real p5p4 = SQR(obb_points[5].x() - obb_points[4].x() ) + SQR(obb_points[5].y() - obb_points[4].y() ) + SQR(obb_points[5].z() - obb_points[4].z() );
	apt_real p0p3 = SQR(obb_points[0].x() - obb_points[3].x() ) + SQR(obb_points[0].y() - obb_points[3].y() ) + SQR(obb_points[0].z() - obb_points[3].z() );
	apt_real p6p7 = SQR(obb_points[6].x() - obb_points[7].x() ) + SQR(obb_points[6].y() - obb_points[7].y() ) + SQR(obb_points[6].z() - obb_points[7].z() );
	apt_real p1p2 = SQR(obb_points[1].x() - obb_points[2].x() ) + SQR(obb_points[1].y() - obb_points[2].y() ) + SQR(obb_points[1].z() - obb_points[2].z() );

	apt_real p5p0 = SQR(obb_points[5].x() - obb_points[0].x() ) + SQR(obb_points[5].y() - obb_points[0].y() ) + SQR(obb_points[5].z() - obb_points[0].z() );
	apt_real p4p3 = SQR(obb_points[4].x() - obb_points[3].x() ) + SQR(obb_points[4].y() - obb_points[3].y() ) + SQR(obb_points[4].z() - obb_points[3].z() );
	apt_real p6p1 = SQR(obb_points[6].x() - obb_points[1].x() ) + SQR(obb_points[6].y() - obb_points[1].y() ) + SQR(obb_points[6].z() - obb_points[1].z() );
	apt_real p7p2 = SQR(obb_points[7].x() - obb_points[2].x() ) + SQR(obb_points[7].y() - obb_points[2].y() ) + SQR(obb_points[7].z() - obb_points[2].z() );

	/*
	cout << "p7p4 " << setprecision(32) << p7p4 << "\n";
	cout << "p6p5 " << setprecision(32) << p6p5 << "\n";
	cout << "p1p0 " << setprecision(32) << p1p0 << "\n";
	cout << "p2p3 " << setprecision(32) << p2p3 << "\n";

	cout << "p5p4 " << setprecision(32) << p5p4 << "\n";
	cout << "p0p3 " << setprecision(32) << p0p3 << "\n";
	cout << "p6p7 " << setprecision(32) << p6p7 << "\n";
	cout << "p1p2 " << setprecision(32) << p1p2 << "\n";

	cout << "p5p0 " << setprecision(32) << p5p0 << "\n";
	cout << "p4p3 " << setprecision(32) << p4p3 << "\n";
	cout << "p6p1 " << setprecision(32) << p6p1 << "\n";
	cout << "p7p2 " << setprecision(32) << p7p2 << "\n";
	*/

	info.obb_dims_0 = MYZERO;
	info.obb_dims_1 = MYZERO;
	info.obb_dims_2 = MYZERO;
	info.obb_yx_10 = MYZERO;
	info.obb_zy_21 = MYZERO;

	if ( fabs(p7p4 - p6p5) < EPSILON && fabs(p7p4 - p1p0) < EPSILON && fabs(p7p4 - p2p3) < EPSILON &&
			fabs(p5p4 - p0p3) < EPSILON && fabs(p5p4-p6p7) < EPSILON && fabs(p5p4-p1p2) < EPSILON &&
				fabs(p5p0 - p4p3) < EPSILON && fabs(p5p0 - p6p1) < EPSILON && fabs(p5p0 -p7p2) < EPSILON ) {
		vector<apt_real> tmp = { sqrt(p7p4), sqrt(p5p4), sqrt(p5p0) };
		stable_sort( tmp.begin(), tmp.end() );
		reverse( tmp.begin(), tmp.end() );
		//dimensions in descending order
		/*
		cout << "Dimensions in decreasing order" << "\n";
		for( auto it = tmp.begin(); it != tmp.end(); it++ ) {
			cout << it - tmp.begin() << "\t\t" << setprecision(32) << *it << "\n";
		}
		*/
		info.obb_dims_0 = tmp[0];
		info.obb_dims_1 = tmp[1];
		info.obb_dims_2 = tmp[2];
		if ( tmp[0] >= EPSILON ) {
			info.obb_yx_10 = tmp[1] / tmp[0];
		}
		else {
			info.obb_yx_10 = MYZERO;
			cerr << "A numerical inconsistence (almost division by zero) occurred while computing obb_yx_10 for object " << info.obj_id << " !" << "\n";
		}
		if ( tmp[1] >= EPSILON ) {
			info.obb_zy_21 = tmp[2] / tmp[1];
		}
		else {
			info.obb_zy_21 = MYZERO;
			cerr << "A numerical inconsistence (almost division by zero) occurred while computing obb_zy_21 for object " << info.obj_id << " !" << "\n";
		}
		info.obb_points = vector<p3d>();
		for( int kk = 0; kk < 8; kk++ ) {
			info.obb_points.push_back( p3d( obb_points[kk].x(), obb_points[kk].y(), obb_points[kk].z() ) );
		}
	}
	else {
		cerr << "A numerical inconsistence occurred while computing the OBB for object " << info.obj_id << " !" << "\n";
	}

	ctoc = omp_get_wtime();
	info.dt_obj_obb = (ctoc-ctic);

	/*
	else {
		cerr << "Resulting hexahedron is unexpectedly distorted !" << "\n";
	}
	*/
}


void manifoldHdl::characterize_proxy_volume()
{
	if ( info.has_valid_proxy == true ) {
		double ctic = omp_get_wtime();
		double ctoc = 0.;

		//compute the volume of the mesh
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		ctic = omp_get_wtime();
#endif

		//if ( info.obj_typ == OBJECT_IS_POLYHEDRON ) {
#ifdef MANIFOLD_ANALYSIS_VERBOSE
		cout << "manifoldHdl::characterize_proxy_volume thread " << omp_get_thread_num() << " cluster " << info.obj_id << "\n";
#endif
		info.obj_volume = PMP::volume(proxy_mesh);
		//}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
		ctoc = omp_get_wtime();
		cout << "manifoldHdl::characterize_proxy_volume thread " << omp_get_thread_num() << " cluster " << info.obj_id << " volume is " << info.obj_volume << " nm^3" << "\n";
#endif
		ctoc = omp_get_wtime();
		info.dt_obj_vol = (ctoc-ctic);
	}
}


void manifoldHdl::characterize_proxy_obb()
{
	if ( info.has_valid_proxy == true ) {
		double ctic = omp_get_wtime();
		double ctoc = 0.;

		//approximate optimal bounding box
		// Compute the extreme points of the mesh, and then a tightly fitted oriented (i.e. normally rotated) bounding box to the point cloud
		vector<Point> points;
		for( auto it = proxy_mesh_vrts.begin(); it != proxy_mesh_vrts.end(); it++ ) {
			points.push_back( Point(it->x, it->y, it->z) );
		}

		array<Point, 8> obb_points;
		//CGAL::oriented_bounding_box(sm, obb_points, CGAL::parameters::use_convex_hull(true));
		CGAL::oriented_bounding_box( points, obb_points, CGAL::parameters::use_convex_hull(true));

		//https://doc.cgal.org/latest/Linear_cell_complex/classLinearCellComplex.html#a8786996ef191013fcee5a90a4ceb22ae
		//p0, p1, p2, p3 end up in the same plane, p4, p5, p6, p7 above so
		apt_real p7p4 = SQR(obb_points[7].x() - obb_points[4].x() ) + SQR(obb_points[7].y() - obb_points[4].y() ) + SQR(obb_points[7].z() - obb_points[4].z() );
		apt_real p6p5 = SQR(obb_points[6].x() - obb_points[5].x() ) + SQR(obb_points[6].y() - obb_points[5].y() ) + SQR(obb_points[6].z() - obb_points[5].z() );
		apt_real p1p0 = SQR(obb_points[1].x() - obb_points[0].x() ) + SQR(obb_points[1].y() - obb_points[0].y() ) + SQR(obb_points[1].z() - obb_points[0].z() );
		apt_real p2p3 = SQR(obb_points[2].x() - obb_points[3].x() ) + SQR(obb_points[2].y() - obb_points[3].y() ) + SQR(obb_points[2].z() - obb_points[3].z() );

		apt_real p5p4 = SQR(obb_points[5].x() - obb_points[4].x() ) + SQR(obb_points[5].y() - obb_points[4].y() ) + SQR(obb_points[5].z() - obb_points[4].z() );
		apt_real p0p3 = SQR(obb_points[0].x() - obb_points[3].x() ) + SQR(obb_points[0].y() - obb_points[3].y() ) + SQR(obb_points[0].z() - obb_points[3].z() );
		apt_real p6p7 = SQR(obb_points[6].x() - obb_points[7].x() ) + SQR(obb_points[6].y() - obb_points[7].y() ) + SQR(obb_points[6].z() - obb_points[7].z() );
		apt_real p1p2 = SQR(obb_points[1].x() - obb_points[2].x() ) + SQR(obb_points[1].y() - obb_points[2].y() ) + SQR(obb_points[1].z() - obb_points[2].z() );

		apt_real p5p0 = SQR(obb_points[5].x() - obb_points[0].x() ) + SQR(obb_points[5].y() - obb_points[0].y() ) + SQR(obb_points[5].z() - obb_points[0].z() );
		apt_real p4p3 = SQR(obb_points[4].x() - obb_points[3].x() ) + SQR(obb_points[4].y() - obb_points[3].y() ) + SQR(obb_points[4].z() - obb_points[3].z() );
		apt_real p6p1 = SQR(obb_points[6].x() - obb_points[1].x() ) + SQR(obb_points[6].y() - obb_points[1].y() ) + SQR(obb_points[6].z() - obb_points[1].z() );
		apt_real p7p2 = SQR(obb_points[7].x() - obb_points[2].x() ) + SQR(obb_points[7].y() - obb_points[2].y() ) + SQR(obb_points[7].z() - obb_points[2].z() );

		info.obb_dims_0 = MYZERO;
		info.obb_dims_1 = MYZERO;
		info.obb_dims_2 = MYZERO;
		info.obb_yx_10 = MYZERO;
		info.obb_zy_21 = MYZERO;

		if ( fabs(p7p4 - p6p5) < EPSILON && fabs(p7p4 - p1p0) < EPSILON && fabs(p7p4 - p2p3) < EPSILON &&
				fabs(p5p4 - p0p3) < EPSILON && fabs(p5p4-p6p7) < EPSILON && fabs(p5p4-p1p2) < EPSILON &&
					fabs(p5p0 - p4p3) < EPSILON && fabs(p5p0 - p6p1) < EPSILON && fabs(p5p0 -p7p2) < EPSILON ) {
			vector<apt_real> tmp = { sqrt(p7p4), sqrt(p5p4), sqrt(p5p0) };
			stable_sort( tmp.begin(), tmp.end() );
			reverse( tmp.begin(), tmp.end() );
			//dimensions in descending order
			/*
			cout << "Dimensions in decreasing order" << "\n";
			for( auto it = tmp.begin(); it != tmp.end(); it++ ) {
				cout << it - tmp.begin() << "\t\t" << setprecision(32) << *it << "\n";
			}
			*/
			info.obb_dims_0 = tmp[0];
			info.obb_dims_1 = tmp[1];
			info.obb_dims_2 = tmp[2];
			if ( tmp[0] >= EPSILON ) {
				info.obb_yx_10 = tmp[1] / tmp[0];
			}
			else {
				info.obb_yx_10 = MYZERO;
				cerr << "A numerical inconsistence (almost division by zero) occurred while computing obb_yx_10 for proxy " << info.obj_id << " !" << "\n";
			}
			if ( tmp[1] >= EPSILON ) {
				info.obb_zy_21 = tmp[2] / tmp[1];
			}
			else {
				info.obb_zy_21 = MYZERO;
				cerr << "A numerical inconsistence (almost division by zero) occurred while computing obb_zy_21 for proxy " << info.obj_id << " !" << "\n";
			}
			info.obb_points = vector<p3d>();
			for( int kk = 0; kk < 8; kk++ ) {
				info.obb_points.push_back( p3d( obb_points[kk].x(), obb_points[kk].y(), obb_points[kk].z() ) );
			}
		}
		else {
			cerr << "A numerical inconsistence occurred while computing the OBB for proxy " << info.obj_id << " !" << "\n";
		}

		ctoc = omp_get_wtime();
		info.dt_obj_obb = (ctoc-ctic);
	}
}


/*
void manifoldHdl::characterize_minimum_ellipsoid()
{
	double ctic = omp_get_wtime();
	double ctoc = 0.0;

	//const int      n = 1000;                // number of points
	const int      d = 3;                   // dimension
	const double eps = 0.01;                // approximation ratio is (1+eps)
	Point_list P;
	//CGAL::Random_points_in_cube_d<AppPoint> rpg( d, 100.0);
	if ( ply_mesh_vrts.size() < 4 ) {
		cout << "ply_mesh_vrts.size() < 4 !" << "\n";
		return;
	}

	for( auto it = ply_mesh_vrts.begin(); it != ply_mesh_vrts.end(); it++ ) {
		AppPoint pp( (double) it->x, (double) it->y, (double) it->z );
		cout << *it << "\n";
		P.push_back( pp ); //AppPoint( d, (FT) it->x, (FT) it->y, (FT) it->z ) );
	}

	// compute approximation:
	Traits traits;
	AME ame(eps, P.begin(), P.end(), traits);

	//  // write EPS file:
	//if (ame.is_full_dimensional() && d == 2)
	//	ame.write_eps("example.eps");

	// output center coordinates:
	if ( ame.is_full_dimensional() == true ) {
		cout << "AME is full dimensional" << "\n";
	}
	else {
		cout << "AME is not full dimensional" << "\n";
	}

	for (AME::Center_coordinate_iterator c_it = ame.center_cartesian_begin(); c_it != ame.center_cartesian_end(); ++c_it) {
	    cout << *c_it << ' ';
	}
	cout << "\n";

	// if (d == 2 || d == 3) { //d == 3
	// output  axes:
	AME::Axes_lengths_iterator axes = ame.axes_lengths_begin();
	for (int i = 0; i < d; ++i) {
		cout << "Semiaxis " << i << " has length " << *axes++  << "\n";
		//	<< "and Cartesian coordinates ";
		//for (AME::Axes_direction_coordinate_iterator
		//	 d_it = ame.axis_direction_cartesian_begin(i);
		//  d_it != ame.axis_direction_cartesian_end(i); ++d_it)
		//std::cout << *d_it << ' ';
		//std::cout << ".\n";
		//}
	}

#ifdef MANIFOLD_ANALYSIS_VERBOSE
	ctoc = omp_get_wtime();
	cout << "manifoldHdl::characterize_minimum_ellipsoid thread " << omp_get_thread_num() << " cluster " << info.obj_id << " took " << (ctoc-ctic) << "\n";
#endif
	ctoc = omp_get_wtime();
	info.dt_obj_obb = (ctoc-ctic);
}
*/


/*
bool manifoldHdl::build_tetrahedra_ensemble()
{
	double ctic = omp_get_wtime();

	info.is_tessellated = false;

	//if ( ply_mesh_fcts_quads.size() > 0 ) {
	//	cerr << "Quad facets are currently not supported but in principle can be handled by TetGen !" << "\n";
	//	return false;
	//}

	if ( ply_mesh_vrts.size() >= (size_t) I32MX ) {
		cerr << "Number of vertices exceeds I32MX !" << "\n"; return false;
	}
	if ( ply_mesh_fcts_trgls.size() >= (size_t) I32MX ) {
		cerr << "Number of facets exceeds I32MX !" << "\n"; return false;
	}


	if ( ply_mesh_fcts_trgls.size() < 4 || ply_mesh_vrts.size() < 4 ) {
		cerr << "Polyhedron has either no vertices, no facets, or insufficient of both !" << "\n"; return false;
	}
	//else {
	//	cout << "Tetrahedralizing polyhedron " << info.obj_id << " ply_mesh_vrts.size() " <<
	//			ply_mesh_vrts.size() << " ply_mesh_fcts_trgls.size() " << ply_mesh_fcts_trgls.size() << "\n";
	//}

	//wias-berlin.de/software/tetgen/1.5/doc/manual/manual.pdf
	tetgenio in, out;
	tetgenio::facet *f;
	tetgenio::polygon *p;

	//initialize();

	tetgenbehavior* calls = NULL;
	try {
		calls = new tetgenbehavior;
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of tetgenbehavior call object failed !" << "\n"; return false;
	}

	string what = ConfigNanochem::TetGenFlags; //"pq", "pY"; //"pYv"; //"pq2.0a10.0v";
	//cout << "what " << what << "\n";
	char* cstr = NULL;
	try {
		cstr = new char[what.length() + 1];
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation of tetrahedralization parameter failed !" << "\n"; return false;
	}
	strcpy(cstr, what.c_str());
	bool tetstatus = calls->parse_commandline( cstr ); //cstr //"pYv", "pYv" "pq2.0a10.0v"
	delete [] cstr; cstr = NULL;

	in.firstnumber = 0;	 //facet indices start at 0
	int nvertices_in = (int) ply_mesh_vrts.size(); //with above sanity check now safe to convert downwards
	int nfaces_in = (int) ply_mesh_fcts_trgls.size();

	in.numberofpoints = nvertices_in;
	in.pointlist = NULL;
	try {
		in.pointlist = new double[nvertices_in*3];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of in.pointlist failed !" << "\n"; return false;
	}
	//cout << "Allocated pointlist to tetgen" << "\n";

	//pass vertices into tetgen data structure
	for ( int v = 0; v < nvertices_in; v++ ) {
		in.pointlist[v*3+0] = (double) ply_mesh_vrts[v].x;
		in.pointlist[v*3+1] = (double) ply_mesh_vrts[v].y;
		in.pointlist[v*3+2] = (double) ply_mesh_vrts[v].z;
	}
	//cout << "Passed trimesh_v to tetgen" << "\n";

	in.numberoffacets = nfaces_in;
	in.facetlist = NULL;
	try {
		in.facetlist = new tetgenio::facet[in.numberoffacets];
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation of in.facetlist failed !" << "\n"; return false;
	}
	//cout << "Allocated facet in tetgen" << "\n";

	in.facetmarkerlist = NULL; //##MK::we have no marks for this PLC

	for ( int ft = 0; ft < nfaces_in; ft++ ) { //add facets
		f = &in.facetlist[ft];
		f->numberofpolygons = 1;
		f->polygonlist = NULL;
		try {
			f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation of f.polygonlist failed!" << "\n"; return false;
		}
		f->numberofholes = 0;
		f->holelist = NULL;

		p = &f->polygonlist[0];
		int nverts_polygon = 3;
		p->numberofvertices = nverts_polygon;
		p->vertexlist = NULL;
		try {
			p->vertexlist = new int[p->numberofvertices];
		}
		catch (bad_alloc &croak) {
			cerr << "Allocation for p.vertexlist failed!" << "\n"; return false;
		}
		//for( int vv = 0; vv < nverts_polygon; vv++ ) {
			p->vertexlist[0] = (int) ply_mesh_fcts_trgls[ft].v1;
			p->vertexlist[1] = (int) ply_mesh_fcts_trgls[ft].v2;
			p->vertexlist[2] = (int) ply_mesh_fcts_trgls[ft].v3;
		//}
	}
	//cout << "Allocated facet vertices in tetgen" << "\n";

	// Set 'in.facetmarkerlist'
	//in.facetmarkerlist[0] = -1; in.facetmarkerlist[1] = -2; in.facetmarkerlist[2] = 0; in.facetmarkerlist[3] = 0;
	//in.facetmarkerlist[4] = 0; in.facetmarkerlist[5] = 0;

	// Output the PLC to files 'barin.node' and 'barin.poly'.
	//in.save_nodes("barin"); in.save_poly("barin");

	//use tetgen library to perform actual meshing
	try {
		tetrahedralize( calls, &in, &out);
	}
	catch (int tetgencroak) {
		cerr << "TetGen internal error " << tetgencroak << "\n"; return false;
	}
	//cout << "Tetrahedralized complex" << "\n";

	//transfer results of the tetgen intermediate data structure
	int nvertices_out = out.numberofpoints;
	int ntetrahedra_out = out.numberoftetrahedra;

	if ( nvertices_out < 1 || ntetrahedra_out < 1 ) {
		cerr << "NumberOfPoints or number of tetraeder is <1 !" << "\n"; return false;
	}
	try {
		//tet_v.reserve( nvertices_out );
		//tet_id.reserve( ntetrahedra_out );
		ply_mesh_tets_quads.reserve( ntetrahedra_out );
	}
	catch (bad_alloc &croak) {
		cerr << "Allocation error for tet_v or tet_o !" << "\n"; return false;
	}
	////cout << "Number of vertices out " << nvertices_out << "\n";
	////cout << "Number of tetraedra out " << ntetrahedra_out << "\n";
	//for ( int v = 0; v < nvertices_out; v++ ) {
	//	tet_v.push_back( p3d64( out.pointlist[v*3+0], out.pointlist[v*3+1], out.pointlist[v*3+2] ) );
	//	//cout << "xyz\t\t" << tet_v.back().x << "\t\t" << tet_v.back().y << "\t\t" << tet_v.back().z << "\n";
	//}
	////cout << "Transferred " << nvertices_out << "/" << tet_v.size() << " tetrahedron vertices" << endl;

	for ( int cc = 0; cc < ntetrahedra_out; cc++ ) {
		//quad3u qd = quad3u(	out.tetrahedronlist[cc*4+0],
		//				out.tetrahedronlist[cc*4+1],
		//				out.tetrahedronlist[cc*4+2],
		//				out.tetrahedronlist[cc*4+3] );
		//tet_id.push_back( qd );
		int q1 = out.tetrahedronlist[cc*4+0];
		int q2 = out.tetrahedronlist[cc*4+1];
		int q3 = out.tetrahedronlist[cc*4+2];
		int q4 = out.tetrahedronlist[cc*4+3];
		//tet_id.push_back( quad3u( 	static_cast<unsigned int>(q1),
		//							static_cast<unsigned int>(q2),
		//							static_cast<unsigned int>(q3),
		//							static_cast<unsigned int>(q4)    ) );
		//tet_xyz.push_back( tet3d64( tet_v.at(q1), tet_v.at(q2), tet_v.at(q3), tet_v.at(q4) ) );
		//tet_v3d.push_back( tet3d( tet_v.at(qd.v1), tet_v.at(qd.v2), tet_v.at(qd.v3), tet_v.at(qd.v4) ) );
		//cout << "v1234\t\t" << tet_o.back().v1 << "\t\t" << tet_o.back().v2 << "\t\t" << tet_o.back().v3 << "\t\t" << tet_o.back().v4 << "\n";
		ply_mesh_tets_quads.push_back( roi_tetrahedron() );
		ply_mesh_tets_quads.back().v1 = p3d( out.pointlist[q1*3+0], out.pointlist[q1*3+1], out.pointlist[q1*3+2] );
		ply_mesh_tets_quads.back().v2 = p3d( out.pointlist[q2*3+0], out.pointlist[q2*3+1], out.pointlist[q2*3+2] );
		ply_mesh_tets_quads.back().v3 = p3d( out.pointlist[q3*3+0], out.pointlist[q3*3+1], out.pointlist[q3*3+2] );
		ply_mesh_tets_quads.back().v4 = p3d( out.pointlist[q4*3+0], out.pointlist[q4*3+1], out.pointlist[q4*3+2] );
	}
	//cout << "Transferred " << ntetrahedra_out << "/" << tet_o.size() << " tetraeder" << "\n";

	//cout << "Tetraeder in polyhedron has ply_mesh_tets_quad.size() " << ply_mesh_tets_quads.size() << "\n";

	//Output mesh to files 'barout.node', 'barout.ele' and 'barout.face'.
	//out.save_nodes("barout");	out.save_elements("barout"); out.save_faces("barout");
	//deinitialize();
	delete calls;
	//cout << "TetGen completed" << "\n";

	info.is_tessellated = true;

	double ctoc = omp_get_wtime();
	info.dt_obj_tets = (ctoc-ctic);

	return true;
}


bool manifoldHdl::validate_tetrahedra_volume()
{
	//we perform a constrained Delaunay tetrahedralization so the volume of the tetrahedra should sum up to the volume
	//of the object
	info.is_valid_volume = false;
	if ( info.is_tessellated == true ) {

		double vol_accumulated_tets = 0.0;
		for( auto it = ply_mesh_tets_quads.begin(); it != ply_mesh_tets_quads.end(); it++ ) {
			vol_accumulated_tets += (double) it->get_volume();
		}
		if ( info.obj_volume >= (double) EPSILON ) {
			double Vdiff = fabs(info.obj_volume - vol_accumulated_tets) / info.obj_volume;
			if ( Vdiff < ConfigNanochem::PLCtoTetsRelativeVolumeMismatch ) {
				info.is_valid_volume = true;
			}
			else {
				#pragma omp critical
				{
					cerr << "Strong volume mismatch PLC/tets, obj_id, obj_volume, accumulated, Vdiff " << info.obj_id << ", " << info.obj_volume << ", " << vol_accumulated_tets << ", " << setprecision(32) << Vdiff << "\n";
				}
			}
		}
		else {
			#pragma omp critical
			{
				cerr << "Object obj_id " << info.obj_id << " discarded because of very small size !" << "\n";
			}
		}
	}
	return true;
}


bool manifoldHdl::build_tetrahedra_ensemble_aabb()
{
	double ctic = omp_get_wtime();

	info.is_valid_aabb = false;
	if ( info.is_tessellated == true ) {
		if ( info.is_valid_volume == true ) {
			info.is_valid_aabb = false;

			if ( ply_mesh_tets_bvh != NULL ) {
				delete ply_mesh_tets_bvh; ply_mesh_tets_bvh = NULL;
			}

			try {
				ply_mesh_tets_bvh = new class Tree( ply_mesh_tets_quads.size() );
			}
			catch (bad_alloc &trisoupexc) {
				#pragma omp critical
				{
					cerr << "Thread " << omp_get_thread_num() << " experienced an error while"
							" constructing ply_mesh_tets_bvh for object " << info.obj_id << "\n";
				}
				return false;
			}

			aabb3d tmp = aabb3d();
			for( apt_uint tetidx = 0; tetidx < (apt_uint) ply_mesh_tets_quads.size(); tetidx++ ) {
				//##MK::fattening to assure that edge touching of boxes results in inclusion tests
				//add tetrahedra aabbs to BVH
				aabb3d tet_in_a_box = ply_mesh_tets_quads[tetidx].get_aabb();

				ply_mesh_tets_bvh->insertParticle( tetidx,
						trpl(	tet_in_a_box.xmi - EPSILON,
								tet_in_a_box.ymi - EPSILON,
								tet_in_a_box.zmi - EPSILON  ),
						trpl(	tet_in_a_box.xmx + EPSILON,
								tet_in_a_box.ymx + EPSILON,
								tet_in_a_box.zmx + EPSILON  ) );

				//compute AABB to the closed polyhedron
				//we can use i as the particle ID because hedgesAABB uses a map<particle, node>
				tmp.possibly_enlarge_me( ply_mesh_tets_quads[tetidx].v1 );
				tmp.possibly_enlarge_me( ply_mesh_tets_quads[tetidx].v2 );
				tmp.possibly_enlarge_me( ply_mesh_tets_quads[tetidx].v3 );
				tmp.possibly_enlarge_me( ply_mesh_tets_quads[tetidx].v4 );
			}

			//fatten this AABB a little bit to be sure
			tmp.add_epsilon_guard();
			tmp.scale();
			info.aabb = tmp; //copying over trivial type

			//cout << "obj_id " << info.obj_id << "\t\t" << info.aabb << "\n";
			info.is_valid_aabb = true;
		}
	}

	double ctoc = omp_get_wtime();
	info.dt_obj_bvh = (ctoc-ctic);

	return true;
}
*/


bool manifoldHdl::build_polyhedron_aabb()
{
	double ctic = omp_get_wtime();

	info.is_valid_aabb = false;
	if ( info.is_tessellated == true ) {
		if ( info.is_valid_volume == true ) {
			info.is_valid_aabb = false;

			if ( ply_mesh_vrts.size() >= 4 ) {

				aabb3d tmp = aabb3d();
				for( size_t v = 0; v < ply_mesh_vrts.size(); v++ ) { //##MK::was this->ply_mesh_vrts.size()
					tmp.possibly_enlarge_me( ply_mesh_vrts[v] );
				}
				//fatten this AABB a little bit to be sure
				tmp.add_epsilon_guard();
				tmp.scale();
				info.aabb = tmp; //copying over trivial type

				//cout << "obj_id " << info.obj_id << "\t\t" << info.aabb << "\n";
				info.is_valid_aabb = true;
			}
			else {
				#pragma omp critical
				{
					cerr << "Thread " << omp_get_thread_num() << " experienced an error while computing polyhedron aabb for object " << info.obj_id << "\n";
				}
				return false;
			}
		}
	}

	double ctoc = omp_get_wtime();
	info.dt_obj_bvh = (ctoc-ctic);

	return true;
}


bool manifoldHdl::build_surface_patch_aabb()
{
	double ctic = omp_get_wtime();

	info.is_valid_aabb = false;
	if ( patch_fcts_trgls.size() >= 1 ) {

		aabb3d tmp = aabb3d();
		for( size_t facetID = 0; facetID < patch_fcts_trgls.size(); facetID++ ) {
			tmp.possibly_enlarge_me( patch_fcts_trgls[facetID] );
		}
		//fatten this AABB a little bit to be sure
		tmp.add_epsilon_guard();
		tmp.scale();
		info.aabb = tmp; //copying over trivial type

		//cout << "obj_id " << info.obj_id << "\t\t" << info.aabb << "\n";
		info.is_valid_aabb = true;
	}
	else {
		#pragma omp critical
		{
			cerr << "Thread " << omp_get_thread_num() << " experienced an error while computing surface patch aabb for object " << info.obj_id << "\n";
		}
		return false;
	}

	double ctoc = omp_get_wtime();
	info.dt_obj_bvh = (ctoc-ctic);

	return true;
}


void manifoldHdl::check_if_interior_object( const apt_real dprx, vector<tri3d> const & edge_tris, Tree* const edge_tri_bvh )
{
	double ctic = omp_get_wtime();
	if ( edge_tris.size() > 0 && edge_tri_bvh != NULL ) {
		if ( info.is_valid_aabb == true ) {
			//we assume that an object is interior if it is not close to the boundary
			//the mesh of an object is close to the boundary if at least one of its triangles has a shortest Euclidean distance to at least one triangle of the edge
			//that is shorter or equal to dprx, the proximity of an object mesh to edge mesh threshold
			if ( ply_mesh_fcts_trgls.size() >= 1 ) {
				for( size_t i = 0; i < ply_mesh_fcts_trgls.size(); i++ ) {
					tri3u t1 = ply_mesh_fcts_trgls[i];
					tri3d T1 = tri3d( 	ply_mesh_vrts[t1.v1].x, ply_mesh_vrts[t1.v1].y, ply_mesh_vrts[t1.v1].z,
										ply_mesh_vrts[t1.v2].x, ply_mesh_vrts[t1.v2].y, ply_mesh_vrts[t1.v2].z,
										ply_mesh_vrts[t1.v3].x, ply_mesh_vrts[t1.v3].y, ply_mesh_vrts[t1.v3].z  );

					//filter the neighboring triangles of neighbor nbtriID
					apt_real xmi = fmin( fmin( T1.x1, T1.x2 ), T1.x3 );
					apt_real xmx = fmax( fmax( T1.x1, T1.x2 ), T1.x3 );
					apt_real ymi = fmin( fmin( T1.y1, T1.y2 ), T1.y3 );
					apt_real ymx = fmax( fmax( T1.y1, T1.y2 ), T1.y3 );
					apt_real zmi = fmin( fmin( T1.z1, T1.z2 ), T1.z3 );
					apt_real zmx = fmax( fmax( T1.z1, T1.z2 ), T1.z3 );
					hedgesAABB nb_aabb( trpl( xmi - dprx - EPSILON, ymi - dprx - EPSILON, zmi - dprx - EPSILON),
										trpl( xmx + dprx + EPSILON, ymx + dprx + EPSILON, zmx + dprx + EPSILON) );

					vector<apt_uint> cand = edge_tri_bvh->query( nb_aabb );

					//try to prove that the object is close because then we can reject early and return false, object is not interior
					//for an object embedded inside we would query ply_mesh_fcts_trgls.size() * log(N_edge) on average, every time finding no candidate, so unable to prove close, so object inside, return true
					for( size_t j = 0; j < cand.size(); j++ ) {
						//check if distance between any two points on triangle T1 and neighboring triangles on the edge is shorter or equal dprx
						//PQP_REAL TriDist(PQP_REAL P[3], PQP_REAL Q[3], const PQP_REAL S[3][3], const PQP_REAL T[3][3])
						PQP_REAL PP[3] = {0.0, 0.0, 0.0};
						PQP_REAL QQ[3] = {0.0, 0.0, 0.0};
						PQP_REAL SS[3][3];
						SS[0][0] = T1.x1; SS[0][1] = T1.y1; SS[0][2] = T1.z1;
						SS[1][0] = T1.x2; SS[1][1] = T1.y2; SS[1][2] = T1.z2;
						SS[2][0] = T1.x3; SS[2][1] = T1.y3; SS[2][2] = T1.z3;

						tri3d T2 = edge_tris[cand[j]];
						PQP_REAL TT[3][3];
						TT[0][0] = T2.x1; TT[0][1] = T2.y1; TT[0][2] = T2.z1;
						TT[1][0] = T2.x2; TT[1][1] = T2.y2; TT[1][2] = T2.z2;
						TT[2][0] = T2.x3; TT[2][1] = T2.y3; TT[2][2] = T2.z3;

						if ( TriDist( PP, QQ, SS, TT ) > dprx ) { //most likely
							continue;
						}
						else {
							double ctoc = omp_get_wtime();
							info.dt_obj_ions = (ctoc-ctic);
							info.close_to_dset_edge = IS_EXTERIOR;
							return;
						}
					}
				}
				double ctoc = omp_get_wtime();
				info.dt_obj_ions = (ctoc-ctic);
				info.close_to_dset_edge = IS_INTERIOR;
				return;
			}
		}
	}
	double ctoc = omp_get_wtime();
	info.dt_obj_ions = (ctoc-ctic);
	info.close_to_dset_edge = IS_UNCLEAR;
}


bool manifoldHdl::characterize_ions_cgal_point_in_polyhedron_sequential( vector<p3dmidx> const & candidates )
{
	//CALLED FROM WITHIN PARALLEL REGION WITHOUT OMP NESTING so do not spawn further OpenMP parallelism work sharing constructs in this function
	//double ctic = omp_get_wtime();

	if ( info.obj_typ == OBJECT_IS_POLYHEDRON ) {
		if ( info.is_tessellated == true ) {
			if ( info.is_valid_volume == true ) {
				if ( info.is_valid_aabb == true ) {

					//apt_uint point_in_polyhedron_tests = 0;

					//for all ions query AABB
					CGAL::Side_of_triangle_mesh<Polyhedron, K> inside(ply_mesh);

					roi_rotated_cuboid obb = roi_rotated_cuboid( info.obb_points );

					for( size_t cand = 0; cand < candidates.size(); cand++ ) {

						//an AABB has typically much more volume than an OBB in particular for elongated rotated objects

						//##MK::check first if the point is in the OBB about the object
						p3d here = p3d( candidates[cand].x, candidates[cand].y, candidates[cand].z );
						if ( obb.is_inside( here ) == true ) {
							//point_in_polyhedron_tests++;

							p3dmidx thision = candidates[cand];
							Point testpoint = Point(thision.x, thision.y, thision.z);

							CGAL::Bounded_side res = inside(testpoint);
							if ( res == CGAL::ON_BOUNDED_SIDE || res == CGAL::ON_BOUNDARY ) {
								//thision is inside the polyhedron

								//bookkeep that the ion is of a particular type
								map<apt_uint,apt_uint>::iterator thistype = myres_iontypes.find( thision.m1 );
								if ( thistype != myres_iontypes.end() ) {
									thistype->second++;
								}
								else {
									myres_iontypes.insert( pair<apt_uint,apt_uint>(thision.m1, 1) );
								}

								//bookkeep the evaporation ID of ion so that we can identify which ions of the reconstruction "belong" to the closed object
								ion_evapid.push_back( thision.idx );
								if ( thision.m2 == false ) { //ions is not deeply embedded inside the point cloud
									//if there exists at least one point whose distance to the edge is below edge_contact_dist_threshold
									//then we assume the object is very close to the edge of the dataset and therefore the object is
									//likely truncated
									info.close_to_dset_edge = IS_EXTERIOR;
								}
							} //point in polyhedron test
						} //next candidate ion in obb
					} //next candidate ion in aabb
				} //is valid aabb
			} //is valid volume
		} //is tessellated
	}

	//double ctoc = omp_get_wtime();
	//info.dt_obj_bvh = (ctoc-ctic);

	return true;
}


bool manifoldHdl::characterize_ions_cgal_point_in_polyhedron_multithreaded( vector<p3dmidx> const & candidates )
{
	//double ctic = omp_get_wtime();

	if ( info.obj_typ == OBJECT_IS_POLYHEDRON ) {
		if ( info.is_tessellated == true ) {
			if ( info.is_valid_volume == true ) {
				if ( info.is_valid_aabb == true ) {

					//apt_uint point_in_polyhedron_tests = 0;

					//##MK::remove the num_threads debug statement
					//shared(point_in_polyhedron_tests)
					#pragma omp parallel num_threads(1)
					{
						//thread-local buffers
						//apt_uint mythr_point_in_polyhedron_tests = 0;
						map<apt_uint,apt_uint> mythr_myres_iontypes;
						vector<apt_uint> mythr_ion_evapid;
						bool mythr_close_to_dset_edge = false;

						//thread local copy of the mesh, http://www.cs.tau.ac.il/~efif/doc_output2/Manual/preliminaries.html
						//it is not safe to access the same object from different threads at the same time, unless otherwise specified in the class documentation.
						Polyhedron my_ply_mesh;
						#pragma omp critical
						{
							my_ply_mesh = ply_mesh;
						}

						CGAL::Side_of_triangle_mesh<Polyhedron, K> mythr_inside(my_ply_mesh);

						roi_rotated_cuboid mythr_obb = roi_rotated_cuboid( info.obb_points );

						#pragma omp for schedule(dynamic,1)
						for( size_t cand = 0; cand < candidates.size(); cand++ ) {

							//an AABB has typically much more volume than an OBB in particular for elongated rotated objects

							//##MK::check first if the point is in the OBB about the object
							p3d here = p3d( candidates[cand].x, candidates[cand].y, candidates[cand].z );
							if ( mythr_obb.is_inside( here ) == true ) {
								//mythr_point_in_polyhedron_tests++;

								p3dmidx thision = candidates[cand];
								Point testpoint = Point(thision.x, thision.y, thision.z);

								CGAL::Bounded_side res = mythr_inside(testpoint);
								if ( res == CGAL::ON_BOUNDED_SIDE || res == CGAL::ON_BOUNDARY ) {
									//thision is inside the polyhedron

									//bookkeep that the ion is of a particular type
									map<apt_uint,apt_uint>::iterator thistype = mythr_myres_iontypes.find( thision.m1 );
									if ( thistype != mythr_myres_iontypes.end() ) {
										thistype->second++;
									}
									else {
										mythr_myres_iontypes.insert( pair<apt_uint,apt_uint>(thision.m1, 1) );
									}

									//bookkeep the evaporation ID of ion so that we can identify which ions of the reconstruction "belong" to the closed object
									mythr_ion_evapid.push_back( thision.idx );
									if ( thision.m2 == false ) { //ions is not deeply embedded inside the point cloud
										//if there exists at least one point whose distance to the edge is below edge_contact_dist_threshold
										//then we assume the object is very close to the edge of the dataset and therefore the object is
										//likely truncated
										mythr_close_to_dset_edge = true;
									}
								} //next candidate in polyhedron
							} //next candidate in obb
						} //next candidate ion

						#pragma omp critical
						{
							//point_in_polyhedron_tests += mythr_point_in_polyhedron_tests;
							for( auto it = mythr_myres_iontypes.begin(); it != mythr_myres_iontypes.end(); it++ ) {
								map<apt_uint,apt_uint>::iterator thistype = myres_iontypes.find( it->first );
								if ( thistype != myres_iontypes.end() ) {
									thistype->second += it->second;
								}
								else {
									myres_iontypes.insert( pair<apt_uint,apt_uint>(it->first, it->second) );
								}
							}
							ion_evapid.insert( ion_evapid.end(), mythr_ion_evapid.begin(), mythr_ion_evapid.end() );
							if ( mythr_close_to_dset_edge == true ) {
								info.close_to_dset_edge = IS_EXTERIOR;
							}
						}

					} //end of parallel region

				} //is valid aabb
			} //is valid volume
		} //is tessellated
	}

	//double ctoc = omp_get_wtime();
	//info.dt_obj_bvh = (ctoc-ctic);

	return true;
}


bool manifoldHdl::characterize_ions_cgal_point_in_proxy_sequential( vector<p3dmidx> const & candidates )
{
	//CALLED FROM WITHIN PARALLEL REGION WITHOUT OMP NESTING so do not spawn further OpenMP parallelism work sharing constructs in this function
	//double ctic = omp_get_wtime();

	if ( info.obj_typ == OBJECT_IS_SURFACEMESH ) {
		if ( info.has_valid_proxy == true ) {
			if ( info.is_valid_volume == true ) {
				if ( info.is_valid_aabb == true ) {

					//apt_uint point_in_proxy_tests = 0;

					//for all ions query AABB
					CGAL::Side_of_triangle_mesh<Mesh, K> inside(proxy_mesh);

					roi_rotated_cuboid obb = roi_rotated_cuboid( info.obb_points );

					for( size_t cand = 0; cand < candidates.size(); cand++ ) {

						//an AABB has typically much more volume than an OBB in particular for elongated rotated objects

						//##MK::check first if the point is in the OBB about the object
						p3d here = p3d( candidates[cand].x, candidates[cand].y, candidates[cand].z );
						if ( obb.is_inside( here ) == true ) {
							//point_in_proxy_tests++;

							p3dmidx thision = candidates[cand];
							Point testpoint = Point(thision.x, thision.y, thision.z);

							CGAL::Bounded_side res = inside(testpoint);
							if ( res == CGAL::ON_BOUNDED_SIDE || res == CGAL::ON_BOUNDARY ) {
								//thision is inside the proxy

								//bookkeep that the ion is of a particular type
								map<apt_uint,apt_uint>::iterator thistype = myres_iontypes.find( thision.m1 );
								if ( thistype != myres_iontypes.end() ) {
									thistype->second++;
								}
								else {
									myres_iontypes.insert( pair<apt_uint,apt_uint>(thision.m1, 1) );
								}

								//bookkeep the evaporation ID of ion so that we can identify which ions of the reconstruction "belong" to the closed object
								ion_evapid.push_back( thision.idx );
								if ( thision.m2 == false ) { //ions is not deeply embedded inside the point cloud
									//if there exists at least one point whose distance to the edge is below edge_contact_dist_threshold
									//then we assume the object is very close to the edge of the dataset and therefore the object is
									//likely truncated
									info.close_to_dset_edge = IS_EXTERIOR;
								}
							} //point in proxy test
						} //next candidate ion in obb
					} //next candidate ion in aabb
				} //is valid aabb
			} //is valid volume
		} //has valid proxy
	}

	//double ctoc = omp_get_wtime();
	//info.dt_obj_bvh = (ctoc-ctic);

	return true;
}


bool manifoldHdl::characterize_ions_cgal_point_in_proxy_multithreaded( vector<p3dmidx> const & candidates )
{
	//double ctic = omp_get_wtime();

	if ( info.obj_typ == OBJECT_IS_SURFACEMESH ) {
		if ( info.has_valid_proxy == true ) {
			if ( info.is_valid_volume == true ) {
				if ( info.is_valid_aabb == true ) {

					//apt_uint point_in_proxy_tests = 0;

					//##MK::remove the num_threads debug statement
					//shared(point_in_proxy_tests)
					#pragma omp parallel num_threads(1)
					{
						//thread-local buffers
						//apt_uint mythr_point_in_proxy_tests = 0;
						map<apt_uint,apt_uint> mythr_myres_iontypes;
						vector<apt_uint> mythr_ion_evapid;
						bool mythr_close_to_dset_edge = false;

						//thread local copy of the mesh, http://www.cs.tau.ac.il/~efif/doc_output2/Manual/preliminaries.html
						//it is not safe to access the same object from different threads at the same time, unless otherwise specified in the class documentation.
						Mesh my_proxy_mesh;
						#pragma omp critical
						{
							my_proxy_mesh = proxy_mesh;
						}

						CGAL::Side_of_triangle_mesh<Mesh, K> mythr_inside(my_proxy_mesh);

						roi_rotated_cuboid mythr_obb = roi_rotated_cuboid( info.obb_points );

						#pragma omp for schedule(dynamic,1)
						for( size_t cand = 0; cand < candidates.size(); cand++ ) {

							//an AABB has typically much more volume than an OBB in particular for elongated rotated objects

							//##MK::check first if the point is in the OBB about the object
							p3d here = p3d( candidates[cand].x, candidates[cand].y, candidates[cand].z );
							if ( mythr_obb.is_inside( here ) == true ) {
								//mythr_point_in_proxy_tests++;

								p3dmidx thision = candidates[cand];
								Point testpoint = Point(thision.x, thision.y, thision.z);

								CGAL::Bounded_side res = mythr_inside(testpoint);
								if ( res == CGAL::ON_BOUNDED_SIDE || res == CGAL::ON_BOUNDARY ) {
									//thision is inside the proxy

									//bookkeep that the ion is of a particular type
									map<apt_uint,apt_uint>::iterator thistype = mythr_myres_iontypes.find( thision.m1 );
									if ( thistype != mythr_myres_iontypes.end() ) {
										thistype->second++;
									}
									else {
										mythr_myres_iontypes.insert( pair<apt_uint,apt_uint>(thision.m1, 1) );
									}

									//bookkeep the evaporation ID of ion so that we can identify which ions of the reconstruction "belong" to the closed object
									mythr_ion_evapid.push_back( thision.idx );
									if ( thision.m2 == false ) { //ions is not deeply embedded inside the point cloud
										//if there exists at least one point whose distance to the edge is below edge_contact_dist_threshold
										//then we assume the object is very close to the edge of the dataset and therefore the object is
										//likely truncated
										mythr_close_to_dset_edge = true;
									}
								} //next candidate in proxy
							} //next candidate in obb
						} //next candidate ion

						#pragma omp critical
						{
							//point_in_proxy_tests += mythr_point_in_proxy_tests;
							for( auto it = mythr_myres_iontypes.begin(); it != mythr_myres_iontypes.end(); it++ ) {
								map<apt_uint,apt_uint>::iterator thistype = myres_iontypes.find( it->first );
								if ( thistype != myres_iontypes.end() ) {
									thistype->second += it->second;
								}
								else {
									myres_iontypes.insert( pair<apt_uint,apt_uint>(it->first, it->second) );
								}
							}
							ion_evapid.insert( ion_evapid.end(), mythr_ion_evapid.begin(), mythr_ion_evapid.end() );
							if ( mythr_close_to_dset_edge == true ) {
								info.close_to_dset_edge = IS_EXTERIOR;
							}
						}

					} //end of parallel region

				} //is valid aabb
			} //is valid volume
		} //has valid proxy
	}

	//double ctoc = omp_get_wtime();
	//info.dt_obj_bvh = (ctoc-ctic);

	return true;
}


bool manifoldHdl::is_object( const size_t min_ion_support )
{
	if ( 	info.obj_typ == OBJECT_IS_POLYHEDRON &&
			info.is_tessellated == true &&
			info.is_valid_volume == true &&
			info.is_valid_aabb == true &&
			ion_evapid.size() >= min_ion_support ) {
		return true;
	}
	return false;
}


bool manifoldHdl::is_object_interior( const size_t min_ion_support )
{
	if ( 	info.obj_typ == OBJECT_IS_POLYHEDRON &&
			info.is_tessellated == true &&
			info.is_valid_volume == true &&
			info.is_valid_aabb == true &&
			ion_evapid.size() >= min_ion_support &&
			info.close_to_dset_edge == IS_INTERIOR ) {
		return true;
	}
	return false;
}


bool manifoldHdl::is_object_exterior( const size_t min_ion_support )
{
	if ( 	info.obj_typ == OBJECT_IS_POLYHEDRON &&
			info.is_tessellated == true &&
			info.is_valid_volume == true &&
			info.is_valid_aabb == true &&
			ion_evapid.size() >= min_ion_support &&
			info.close_to_dset_edge == IS_EXTERIOR ) {
		return true;
	}
	return false;
}


bool manifoldHdl::is_proxy( const size_t min_ion_support )
{
	if ( 	info.obj_typ == OBJECT_IS_SURFACEMESH &&
			info.has_valid_proxy == true &&
			ion_evapid.size() >= min_ion_support ) {
		return true;
	}
	return false;
}


bool manifoldHdl::is_proxy_interior( const size_t min_ion_support )
{
	if ( 	info.obj_typ == OBJECT_IS_SURFACEMESH &&
			info.has_valid_proxy == true &&
			ion_evapid.size() >= min_ion_support &&
			info.close_to_dset_edge == IS_INTERIOR ) {
		return true;
	}
	return false;
}


bool manifoldHdl::is_proxy_exterior( const size_t min_ion_support ) {
	if ( 	info.obj_typ == OBJECT_IS_SURFACEMESH &&
			info.has_valid_proxy == true &&
			ion_evapid.size() >= min_ion_support &&
			info.close_to_dset_edge == IS_EXTERIOR ) {
		return true;
	}
	return false;
}

/*
void manifoldHdl::characterize_ioncounts( vector<p3d> const ipos, vector<p3dinfo> const ityp, const size_t nitypes )
{
	double ctic = omp_get_wtime();
	double ctoc = 0.0;

			//prune all ions against this AABB to eliminate most ions (##MK::later build BVH of tetrahedra directly
		vector<apt_uint> cnts = vector<apt_uint>( nitypes, 0 );
		for( auto ionit = ipos.begin(); ionit != ipos.end(); ionit++ ) {
			if ( bvh.is_inside( *ionit ) == false ) {
				continue;
			}
			else {
				//only for those ions inside this aabb, check for further a possible inclusion in one tetrahedron of the tessellating the closed polyhedron
				//works for concave and convex because closed polyhedron and robustly created polyhedron after cgal processing
				//CPU point in tetrahedron inclusion test
				for( auto kt = ply_mesh_tets_quads.begin(); kt != ply_mesh_tets_quads.end(); kt++ ) {
					//u1*(p1-p0)+u2*(p2-p0)+u3*(p3-p0)=(p-p0)
					//https://math.stackexchange.com/questions/3698021/how-to-find-if-a-3d-point-is-in-on-outside-of-tetrahedron
					Eigen::Matrix3d A;
					Eigen::Vector3d b;
					A << (kt->v2.x - kt->v1.x), (kt->v3.x - kt->v1.x), (kt->v4.x - kt->v1.x),
						 (kt->v2.y - kt->v1.y), (kt->v3.y - kt->v1.y), (kt->v4.y - kt->v1.y),
						 (kt->v2.z - kt->v1.z), (kt->v3.z - kt->v1.z), (kt->v4.z - kt->v1.z);
					b << (ionit->x - kt->v1.x), (ionit->y - kt->v1.y), (ionit->z - kt->v1.z);
					Eigen::Vector3d sol = A.fullPivHouseholderQr().solve(b);
					if ( sol.x() < MYZERO || sol.y() < MYZERO || sol.z() < MYZERO ||
							(MYONE - sol.x() - sol.y() - sol.z()) < MYZERO ) {

						//if ( kt->is_inside( *ionit ) == false ) {
						//implement point-in-tetrahedron test
						continue;
					}
					else {
						//which iontype
						unsigned char thisityp = ityp.at(ionit-ipos.begin()).i_org;
						cnts[(size_t) thisityp]++;
						//no need to test further we can break out of test
						break;
					}
				}
			}
		}
		//complete the accounting
		for( size_t ityp = 0; ityp < nitypes; ityp++ ) {
			info.ion_cnts[(unsigned char) ityp] = cnts[ityp];
		}
	}

	//double ctoc = omp_get_wtime();
#ifdef MANIFOLD_ANALYSIS_VERBOSE
	ctoc = omp_get_wtime();
	cout << "manifoldHdl::characterize_ioncounts thread " << omp_get_thread_num() << " cluster " << info.obj_id << " done took " << (ctoc-ctic) << "\n";
	for( auto itypit = info.ion_cnts.begin(); itypit != info.ion_cnts.end(); itypit++ ) {
		cout << (int) itypit->first << "\t\t" << itypit->second << "\n";
	}
#endif
}
*/


/*
int manifoldHdl::check_if_closed_polyhedron()
{
	double tic = omp_get_wtime();
	double toc = 0.0;
	double ctic = 0.0;
	double ctoc = 0.0;

	ctic = omp_get_wtime();

	if ( triangles.size() < 1 ) {
		return TRIANGLE_SOUP_ANALYSIS_NOSUPPORT;
	}

	//const char* filename = (argc > 1) ? argv[1] : "data/tet-shuffled.off";
	//std::ifstream input(filename);
	vector<K::Point_3> points;
	vector<vector<size_t>> polygons;
	//if(!input || !CGAL::read_OFF(input, points, polygons) || points.empty())
	//{
	//	std::cerr << "Cannot open file " << std::endl;
	//	return EXIT_FAILURE;
	//}

	//https://gist.github.com/afabri/f1fa1552e8421c95d35a52f471429d35
	for ( auto it = triangles.begin(); it != triangles.end(); it++ ) {
		points.push_back( Point_3( it->x1, it->y1, it->z1 ) );
		points.push_back( Point_3( it->x2, it->y2, it->z2 ) );
		points.push_back( Point_3( it->x3, it->y3, it->z3 ) );
	}
	size_t vertexID = 0;
	for ( auto kt = triangles.begin(); kt != triangles.end(); kt++ ) {
		Polygon p;
		p.push_back( vertexID ); vertexID++;
		p.push_back( vertexID ); vertexID++;
		p.push_back( vertexID ); vertexID++;

		polygons.push_back( p );
	}

	if ( points.size() != 3*polygons.size() || vertexID != 3*triangles.size() ) { //all polygons are triangles
		return TRIANGLE_SOUP_ANALYSIS_ERROR;
	}

	ctoc = omp_get_wtime();
	cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " loading data into CGAL took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = omp_get_wtime();

	if ( PMP::orient_polygon_soup(points, polygons) == true ) {
		cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " orientation operation succeeded" << "\n";
	}
	else {
		cerr << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " some points were duplicated, thus producing a self-intersecting polyhedron !" << "\n";
		return TRIANGLE_SOUP_ANALYSIS_SELFINTERSECTING;
	}

	ctoc = omp_get_wtime();
	cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " orient_polygon_soup took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = omp_get_wtime();

	Polyhedron mesh;
	PMP::polygon_soup_to_polygon_mesh(points, polygons, mesh);

	ctoc = omp_get_wtime();
	cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " polygon_soup_to_polygon_mesh took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = omp_get_wtime();

	//Number the faces because 'orient_to_bound_a_volume' needs a face <--> index map
	int index = 0;
	for( Polyhedron::Face_iterator fb = mesh.facets_begin(), fe = mesh.facets_end(); fb != fe; ++fb ) {
		fb->id() = index++;
	}

	ctoc = omp_get_wtime();
	cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " numbering the faces took " << (ctoc-ctic) << " seconds" << "\n";
	ctic = omp_get_wtime();

	if( CGAL::is_closed(mesh) == true ) {
		cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " closure check positive" << "\n";

		PMP::orient_to_bound_a_volume(mesh);
	}
	else {
		cerr << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " closure check negative" << "\n";
		return TRIANGLE_SOUP_ANALYSIS_BORDEREDGES;
	}

	ctoc = omp_get_wtime();
	cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " closure test and orient to bound a volume took " << (ctoc-ctic) << " seconds" << "\n";

	toc = omp_get_wtime();
	cout << "manifoldHdl::check_if_closed_polyhedron thread " << omp_get_thread_num() << " check_if_closed_polyhedron took " << (toc-tic) << " seconds" << "\n";

	return TRIANGLE_SOUP_ANALYSIS_POLYHEDRON_CLOSED;

	//std::ofstream out("tet-oriented1.off");
	//out.precision(17);
	//out << mesh;
	//out.close();
	//PMP::reverse_face_orientations(mesh);
	//std::ofstream out2("tet-oriented2.off");
	//out2.precision(17);
	//out2 << mesh;
	//out2.close();
}
*/
