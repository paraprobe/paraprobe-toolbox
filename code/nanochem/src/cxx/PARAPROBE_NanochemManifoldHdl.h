/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_MANIFOLD_HDL_H__
#define __PARAPROBE_NANOCHEM_MANIFOLD_HDL_H__

#include "PARAPROBE_NanochemTriangleSoupClustering.h"

#define OBJECT_IS_UNKNOWNTYPE							0x00
#define OBJECT_IS_SURFACEMESH							0x11
#define OBJECT_IS_SURFACEMESH_FAIRING_WAS_FAULTY		0x22
#define OBJECT_IS_SURFACEMESH_HOLEFILLING_FAULTY		0x33
#define OBJECT_COMBINATORIAL_REPAIRING_WAS_FAULTY		0x66
#define OBJECT_IS_POLYHEDRON							0xFF

#define UNKNOWNPHASE									0x00
#define AMBIGUOUS_CASE									0xAA
#define ERRORNEOUS_CASE									0xFF

#define IS_EXTERIOR		0
#define IS_INTERIOR		1
#define IS_UNCLEAR		2

//#define MANIFOLD_ANALYSIS_VERBOSE

struct manifold_info
{
	aabb3d aabb;
	map<unsigned char, apt_uint> ion_cnts;		//counts for (molecular) ion types
	map<unsigned char, apt_uint> elem_cnts;		//decomposed iontypes
	//vector<unsigned int> ion_cnts;	//how many ions inside or on the boundary, maximum_itypes + 1, last entry total count
	double obj_volume;
	apt_uint obj_id;

	vector<p3d> obb_points;
	apt_real obb_dims_0;				//descendingly ordered dimensions
	apt_real obb_dims_1;
	apt_real obb_dims_2;
	apt_real obb_yx_10;
	apt_real obb_zy_21;

	unsigned char obj_typ;	//unknown type (0x00), triangle surface (0x01) or closed polyhedron (0xFF)
	unsigned char obj_not_used;
	unsigned char close_to_dset_edge;
	bool has_valid_proxy;	//was the surface_patch modifiable via (iterative) hole filling refinement and fairing operations to yield a proxy?
	bool is_tessellated;	//was TetGen-based tetrahedralization successful
	bool is_valid_volume;	//is volume of object similar enough to volume of accumulated volume of its tetrahedra
	bool is_valid_aabb;		//do we have a valid AABB for the object
	bool is_valid_ions;		//do we have a valid analysis of the ions inside this object?


	double dt_init_cgal;
	double dt_trisoup_repair;
	double dt_trisoup_orient;
	double dt_trisoup_tomesh;
	double dt_polygon_tomesh;
	double dt_mesh_closure;
	double dt_mesh_bound;
	double dt_mesh_proxy;
	double dt_mesh_ounrm;
	double dt_obj_mesh;
	double dt_obj_vol;
	double dt_obj_obb;
	double dt_obj_ori;
	double dt_obj_tets;
	double dt_obj_bvh;
	double dt_obj_ions;
	manifold_info() : aabb(aabb3d()), ion_cnts(map<unsigned char, apt_uint>()),
						obj_volume(0.), obj_id(0), obb_points(vector<p3d>()), obb_dims_0(0.), obb_dims_1(0.), obb_dims_2(0.),
						obb_yx_10(0.), obb_zy_21(0.), obj_typ(OBJECT_IS_UNKNOWNTYPE), obj_not_used(UNKNOWNPHASE),
						close_to_dset_edge(IS_UNCLEAR),
						has_valid_proxy(false), is_tessellated(false), is_valid_volume(false), is_valid_aabb(false),
						is_valid_ions(false),
				dt_init_cgal(0.), dt_trisoup_repair(0.), dt_trisoup_orient(0.), dt_trisoup_tomesh(0.),
			dt_polygon_tomesh(0.), dt_mesh_closure(0.), dt_mesh_bound(0.), dt_mesh_proxy(0.), dt_mesh_ounrm(0.),
				dt_obj_mesh(0.), dt_obj_vol(0.), dt_obj_obb(0.), dt_obj_ori(0.),
					dt_obj_tets(0.), dt_obj_bvh(0.), dt_obj_ions(0.) {} //there is no object with ID zero !
};


class manifoldHdl
{
public:
	manifoldHdl();
	~manifoldHdl();

	void fetch_triangles( apt_uint clustID, triangleSoupClustering const & triHdl );

	int characterize_triangle_soup_convertability_to_mesh();

	void characterize_surface_patch();
	void characterize_closed_polyhedron_mesh();
	void characterize_surface_patch_proxy_mesh();

	//OBB computation uses the approximately exact tight bounding boxes algorithm by Chang et al. which is practically O(Npoints) time complex
	//exact/analytical solutions of the OBB would be possible theoretically using O'Rourke algorithm but practically unfeasible because O(Npoints^3) time complex
	//points are the ions in each object ! could compute convex hull and pass only vertices of convex hull but this
	//trick is what is one of the first tricks played by the two above algorithms anyway!
	void characterize_closed_polyhedron_volume();
	void characterize_closed_polyhedron_obb();
	//##MK::replace the two for one as the only difference is which mesh they use polyhedron uses ply_mesh, proxy uses proxy_mesh not each object has a proxy!
	void characterize_proxy_volume();
	void characterize_proxy_obb(); //same algorithm as above

	/*
	void characterize_minimum_ellipsoid(); //a minimum fitting ellipsoid to the point cloud
	*/

	/*
	bool build_tetrahedra_ensemble();
	bool validate_tetrahedra_volume();
	bool build_tetrahedra_ensemble_aabb();
	*/
	bool build_polyhedron_aabb();
	bool build_surface_patch_aabb();

	void check_if_interior_object( const apt_real dprx, vector<tri3d> const & edge_tris, Tree* const edge_tri_bvh );
	bool characterize_ions_cgal_point_in_polyhedron_sequential( vector<p3dmidx> const & candidates );
	bool characterize_ions_cgal_point_in_polyhedron_multithreaded( vector<p3dmidx> const & candidates );
	//##MK::fuse the above and below pair as the only difference is that the above uses ply_mesh while below it uses proxy_mesh
	bool characterize_ions_cgal_point_in_proxy_sequential( vector<p3dmidx> const & candidates );
	bool characterize_ions_cgal_point_in_proxy_multithreaded( vector<p3dmidx> const & candidates );

	bool is_object( const size_t min_ion_support );
	bool is_object_interior( const size_t min_ion_support );
	bool is_object_exterior( const size_t min_ion_support );
	bool is_proxy( const size_t min_ion_support );
	bool is_proxy_interior( const size_t min_ion_support );
	bool is_proxy_exterior( const size_t min_ion_support );
	/*
	void characterize_ioncounts( vector<p3d> const ipos, vector<p3dinfo> const ityp, const size_t nitypes );
	*/

	vector<tri3d> triangles;			//##MK::could be optimized for memory
	vector<p3d> fcts_normal;
	vector<unrm_info> fcts_normal_quality;

	//for closed objects, i.e. polyhedra
	Polyhedron ply_mesh;
	map<face_descriptor, Vector> ply_mesh_fcts_ounrm;	//outer unit normals
	vector<p3d> ply_mesh_vrts;			//disjoint vertices
	vector<tri3u> ply_mesh_fcts_trgls;	//triangular facets
	//vector<quad3u> ply_mesh_fcts_quads; //quad facets
	vector<p3d> ply_mesh_fcts_ounrm_eigen; //##MK::


	//for free standing surfaces or proxies...
	//vector<p3d> patch_vrts;				//##MK::memory consumption needs to be optimized
	vector<tri3d> patch_fcts_trgls;
	vector<p3d> patch_fcts_nrm_grad3d;
	//...if additionally these surfaces can be approximated by a hole-filled refined and faired proxy/stand-in proxy
	//polyhedra from e.g. hole-filling algorithms or an 3D Alpha Wrapping procedure we also want to keep track of this proxy
	Mesh proxy_mesh;
	map<face_descriptor, Vector> proxy_mesh_fcts_ounrm;
	vector<p3d> proxy_mesh_vrts;
	vector<tri3u> proxy_mesh_fcts_trgls;

	manifold_info info;

	/*
	vector<p3d> ply_mesh_tets_vrts;					//points from ply_mesh_vrts and Steiner points !!!
	vector<roi_tetrahedron> ply_mesh_tets_quads;	//the collection of tetrahedra tessellating
	//the piecewise-linear complex which the closed polyhedron build
	Tree* ply_mesh_tets_bvh;
	*/

	/*
	vector<roi_rotated_cuboid*> ply_rois_obb;
	*/
	vector<roi_rotated_cylinder*> ply_rois_cyl;

	map<apt_uint, apt_uint> myres_iontypes; //behind is an array holding intermediate results of counts per iontype
	map<apt_uint, apt_uint> myres_elemtypes;
	vector<apt_uint> ion_evapid;				//list of the evaporationIDs of the ions inside if the object info.is_closed == true

	/*
	vector<roi_rotated_cuboid*> patch_rois_obb;
	*/
	vector<roi_rotated_cylinder*> patch_rois_cyl;
};


#endif
