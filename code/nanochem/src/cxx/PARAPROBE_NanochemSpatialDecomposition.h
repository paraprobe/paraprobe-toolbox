/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_NANOCHEM_HDL_SPATIAL_DECOMPOSITION_H__
#define __PARAPROBE_NANOCHEM_HDL_SPATIAL_DECOMPOSITION_H__

#include "PARAPROBE_NanochemStructs.h"

struct gridportioninfo
{
	p3i gmi;		//minimum and maximum voxel coordinates of the entire grid
	p3i gmx;
	p3i lmi;		//minimum and maximum voxel coordinates of the local grid
	p3i lmx;
	apt_int gnxy;
	apt_int gnxyz;
	apt_int lnxy;
	apt_int lnxyz;
	p3i halo;		//extent of the halo
	aabb3d laabb;	//local box (!! has an epsilon guard zone about it !!
	gridportioninfo() : gmi(p3i()), gmx(p3i()), lmi(p3i()), lmx(p3i()),
			gnxy(0), gnxyz(0), lnxy(0), lnxyz(0), halo(p3i(0,0,0)), laabb(aabb3d()) {}
};

ostream& operator<<(ostream& in, gridportioninfo const & val);

//status codes about the last state of a threadedDelocalizer when used in OpenMP parallel region
#define MYDELOC_SUCCESS 			 0
#define MYDELOC_LU_ALLOC_ERROR		-1
#define MYDELOC_TMP_ALLOC_ERROR		-2
#define MYDELOC_SOLVER_ALLOC_ERROR	-3
#define MYDELOC_BINNING_ERROR		-4


class threadedDelocalizer
{
public:
	threadedDelocalizer();
	~threadedDelocalizer();

	//instantiates the portion of thread thred_num of the spatially distributed entire_grid that is shared among OpenMP threads num_threads
	void init_equilibrate_number_of_ions( voxelgrid const & entire_grid, vector<apt_uint> const & ions_per_z,
			const apt_int halfsize, const double sigma, const int thread_num, const int num_threads,
				pair<size_t, size_t> const & nionsnelements, vector<double> const & mltply  );
	apt_uint get_ion_budget( vector<apt_uint> const & ions_per_z );
	void build_bvh( vector<unsigned char> const & window, vector<p3d> const & pos, vector<p3dinfo> const & ifo );
	void compute_delocalization();

	void init_and_load_distributing( voxelgrid const & entire_grid, vector<apt_uint> const & ions_per_z,
		const apt_int halfsize, const double sigma, const int thread_num, const int num_threads );
	void build_bvh_ityp( vector<apt_uint>* const cand, vector<p3d> const & pos );
	void compute_delocalization_ityp();

	//implicit 4D array, number of disjoint elements changes fastest, thereafter ordinary x,y,z implict 3D array
	vector<binidx_ionidx> myions;
	vector<bin_mgn> bvh_mgn;
	vector<p3dm1> sortedpoints; //all ions with their position and iontype sorted in contiguous blocks,
	//bvh_mgn.size() tells how many blocks, bvh_mgn, tells which indices on this array are ions that belong to a particular bin
	vector<p3d> points;

	vector<double> cnts_f64;
	apt_int kernel_halfsize;
	double kernel_sigma;
	voxelgrid glo_grid_info;
	voxelgrid loc_grid_info;
	pair<apt_int, apt_int> zbounds_halo; //the global z-coordinates of the halo-ed sub-grid [mi,mx)
	pair<apt_int, apt_int> zbounds_ions; //the global z-coordinates of the sub-grid without the halo [mi,mx)
	//gridportioninfo info;
	pair<size_t, size_t> nions_nelements;
	vector<double> ion2element_multiplier;
	int status;
	bool participating;
	bool healthy;
};

#endif
