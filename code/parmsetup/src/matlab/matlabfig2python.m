% paraprobe-parmsetup
% interface data from FAU Erlangen-N\"urnberg with paraprobe-ranger
clear;

fau_matlab_fig_range_file_name = 'R04_11350-v01.rng.fig';

%% load dictionary of atomic numbers for all elements

symbols = {'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', ...
    'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', ...
    'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', ...
    'As', 'Se', 'Br', 'Kr', 'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', ...
    'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', ...
    'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', ...
    'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re', 'Os', ...
    'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', ...
    'Ra', 'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', ...
    'Es', 'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', ...
    'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og'};
pse = containers.Map;
for atomic_number = 1:1:118
    pse(symbols{atomic_number}) = atomic_number;
end
MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION = 32;

%% load fig file and parse relevant ion types
tmp = openfig(fau_matlab_fig_range_file_name);
fig = gcf;
rng = fig.Children.Children;
iontype_id = 1;
file_name = strcat(fau_matlab_fig_range_file_name, '.h5');
for i = 1:1:length(rng)
    if rng(i).Type == 'area' 
        ionname = rng(i).DisplayName;
        if ~strcmp(ionname, 'mass spectrum')
            charge_positive = count(ionname, '+');
            charge_negative = count(ionname, '-');
            if charge_positive > 0 & charge_negative == 0
                charge = charge_positive;
            elseif charge_negative > 0 & charge_positive == 0
                charge = charge_negative;
            else
                charge = 0;
            end
            [sidx, eidx] = regexp(ionname, '^[0-9]+');
            % ##MK::check bounds
            mass_number = str2num(ionname(sidx:eidx));
            symbol = ionname(isstrprop(ionname,'alpha'));
            if isKey(pse, symbol) == true
                disp(ionname)
            else
                'Symbol not in PSE !'
            end
            range = [rng(i).XData(1), rng(i).XData(length(rng(i).XData))];

            isotope_vector = uint16(zeros([1, MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION]));
            protons = pse(symbol);
            neutrons = mass_number - protons;
            isotope_vector(1) = uint16(protons + neutrons*256);
            
            % write into hdf5
            dsnm = strcat('/IonSpecies/ion', num2str(iontype_id), '/charge_state');
            h5create(file_name, dsnm, 1, 'Datatype','uint8');
            h5write(file_name, dsnm, uint8(charge));
            h5writeatt(file_name, dsnm, 'unit', 'eV');
            dsnm = strcat('/IonSpecies/ion', num2str(iontype_id), '/ranges');
            h5create(file_name, dsnm, 2, 'Datatype','single');
            h5write(file_name, dsnm, (single(range))');
            h5writeatt(file_name, dsnm, 'unit', 'amu');
            dsnm = strcat('/IonSpecies/ion', num2str(iontype_id), '/isotope_vector');
            h5create(file_name, dsnm, MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 'Datatype','uint16');
            h5write(file_name, dsnm, uint16(isotope_vector));
            h5writeatt(file_name, dsnm, 'hashing rule', 'number_of_protons+256*number_of_neutrons');
            
            iontype_id = iontype_id + 1;
        end
    end
end
dsnm = '/IonSpecies/number_of_iontypes';
h5create(file_name, dsnm, 1, 'Datatype','uint8');
h5write(file_name, dsnm, uint8(iontype_id-1));
dsnm = '/IonSpecies/number_of_molecular_ions';
h5create(file_name, dsnm, 1, 'Datatype','uint8');
h5write(file_name, dsnm, uint8(MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION));
% ... 'ChunkSize',[50 80],'Deflate',9)
'Successfully parsed from Matlab FAU Erlangen-N\"urnberg FIG to paraprobe'