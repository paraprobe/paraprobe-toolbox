# What is paraprobe-parmsetup ?

paraprobe-parmsetup defines for each tool one class (in the tools sub-directory) which
implement Python functions for writing a NeXus/HDF5 configuration file for each
respective tool of the paraprobe-toolbox.

As an example, you may wish to apply existent ranging definitions that were using ranging
definitions from results files generated with paraprobe-transcoder.
In this case, use the paraprobe-ranger tool which interprets the mass-to-charge-state ratio
range intervals to inspect how each ion of the reconstruction matches a range if any.
For this analysis task, the paraprobe-ranger configuration file stores references to the
transcoder results file from where to read the reconstruction and the mass-to-charge-state
ratios from. This configuration file bookkeeps the state of these files and parameter and
settings used for the paraprobe-ranger run.

# How to use this tool and the paraprobe-toolbox

Please inspect the Jupyter notebooks in the teaching directory which detail how the
classes for each tool can be used for practical examples and real world atom probe datasets
when scripting the configuration.
Jupyter notebooks are the primary mechanism how paraprobe-toolbox should be used.
In this way you can blend your custom Python or machine learning-based analysis
tools with individual tools and functionalities offered by the paraprobe-toolbox.
Furthermore, you can take advantage of the ESRF's H5Web widget which enables you
to inspect the content of HDF5 files in the Jupyter notebook interactively.

# Using tools of the paraprobe-toolbox, the usual workflow

Usually the workflow is as follows: Create a paraprobe-parmsetup class object:
tool = paraprobe_ranger()

Like a wizard you can instruct now your analysis via calling the convenience functions
which each class object for each tool has to offer. In the ranging example here e.g.:
tool.set_recon_filename('PARAPROBE.Transcoder.0.h5')

Based on these choices and scripted commands, the parmsetup tool creates a NeXus/HDF5 file
(PARAPROBE.\*.Config.SimID.\*.h5). The first star is a placeholder for the name of the tool,
e.g. Ranger, or Surfacer, etc. The second star is a placeholder for an integer of your choice
\[1, 2^32) to help you organize your analyses.

In effect, these choices enable you to instruct specific paraprobe-ranger jobs
for a specific input creating specific documented output (aka results).

Upon executing the C++ executables, like in the above example paraprobe-ranger,
such a configuration file (PARAPROBE.Ranger.Config.SimID.\*.h5) is loaded
from which the C++ application paraprobe-ranger parses the relevant tasks and
settings and performs these.

Using the jupyter-h5web software package [H5Web](https://h5web.panosc.eu) the
content of HDF5 files generated can be explored and for many tasks also visualized
directly inside the jupyter notebook.

Results of every tool of the paraprobe-toolbox are stored in another HDF5 file
named PARAPROBE.*.Results.SimID.*.h5. The stars here again are placeholders for
the name of the tool and the simulation ID respectively.

# Questions

Feel free to contact me for any questions you have related to the tool
[Markus Kühbach](https://www.fairmat-nfdi.eu/fairmat/about-fairmat/team-fairmat)
You can write me an e-mail or contact me via creating an issue on the repository.