#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Create NeXus/HDF5 configuration files for paraprobe-clusterer."""

from string import digits

import numpy as np

from ifes_apt_tc_data_modeling.utils.utils import symbol_lst_to_matrix_of_nuclide_vector
from utils.src.python.numerics import EPSILON
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase



class ClustererTask(ParmsetupTaskBase):
    """Metadata of a task for clusterer."""

    def __init__(self):
        super().__init__("clusterer", "cluster_analysis")
        self.cfg = {}

    def set_ion_types_filter(self, method="resolve_element", symbol_lst=[], **kwargs):
        """##MK."""
        method_used, ivec_matrix, charge_vector \
            = symbol_lst_to_matrix_of_nuclide_vector(
                method, symbol_lst, **kwargs)
        self.cfg["ion_type_filter"] = NodeInfo(method_used)
        if ivec_matrix is not None:
            self.cfg["ion_query_nuclide_vector"] \
                = NodeInfo(ivec_matrix, "", np.uint16)
        # if charge_vector is not None:
        #    self.cfg["charge_state_array_source"] \
        #         = NodeInfo(np.asarray(charge_vector, np.uint8), "", np.uint8)
        # ##MK::should be int8 instead of uint8 !!

    def set_dbscan_task(self,
                        high_throughput_method="combinatorics",
                        eps=None,
                        min_pts=None):
        """Specify parameterization of a collection of DBScan tasks."""
        allowed = ["tuple", "combinatorics"]
        if high_throughput_method not in allowed:
            raise ValueError(f"Argument high_throughput_method needs to be in {allowed}!")
        if not isinstance(eps, (list, np.ndarray)) or not isinstance(min_pts, (list, np.ndarray)):
            raise TypeError("Arguments eps and min_pts need to be either a list or numpy ndarray respectively!")
        for val in eps:
            if not isinstance(val, float) or val < EPSILON or val > np.finfo(np.float32).max:
                raise ValueError(f"Argument eps needs to be a float in [{EPSILON}, {np.finfo(np.float32).max}]")
        for val in min_pts:
            if not isinstance(val, int) or val <= 0 or val > np.iinfo(np.uint32).max:
                raise ValueError(f"Argument min_pts needs to be an integer in [1, {np.iinfo(np.uint32).max}]")
        self.cfg["dbscan/@NX_class"] = NodeInfo("NXprocess")
        self.cfg["dbscan/high_throughput_method"] = NodeInfo(high_throughput_method)
        self.cfg["dbscan/eps"] \
            = NodeInfo(np.asarray(eps, np.float32), "nm", np.float32)
        self.cfg["dbscan/min_pts"] \
            = NodeInfo(np.asarray(min_pts, np.uint32), "", np.uint32)

    def set_hdbscan_task(self,
                         high_throughput_method="combinatorics",
                         min_cluster_size=None,
                         min_samples=None,
                         cluster_selection_epsilon=None,
                         alpha=None):
        """Specify parameterization of a collection of HDBScan tasks."""
        allowed = ["tuple", "combinatorics"]
        if high_throughput_method not in allowed:
            raise ValueError(f"Argument high_throughput_method needs to be in {allowed}!")
        if min_cluster_size is None or min_samples is None or cluster_selection_epsilon is None or alpha is None:
            raise ValueError("Arguments min_cluster_size, min_samples, cluster_selection_epsilon, and alpha must not be None!")
        if not isinstance(min_cluster_size, (list, np.ndarray)) \
            or not isinstance(min_samples, (list, np.ndarray)) \
            or not isinstance(cluster_selection_epsilon, (list, np.ndarray)) \
            or not isinstance(alpha, (list, np.ndarray)):
            raise TypeError("Arguments min_cluster_size, min_samples, cluster_selection_epsilon, " \
                            " and alpha need to be either a list or numpy ndarray respectively!")
        # ##MK::range checks
        self.cfg["hdbscan/@NX_class"] = NodeInfo("NXprocess")
        self.cfg["hdbscan/high_throughput_method"] = NodeInfo(high_throughput_method)
        self.cfg["hdbscan/min_cluster_size"] \
            = NodeInfo(np.asarray(min_cluster_size, np.float32), "NX_ANY", np.float32)
        self.cfg["hdbscan/min_samples"] \
            = NodeInfo(np.asarray(min_samples, np.float32), "NX_ANY", np.float32)
        self.cfg["hdbscan/cluster_selection_epsilon"] \
            = NodeInfo(np.asarray(cluster_selection_epsilon, np.float32), "NX_ANY", np.float32)
        self.cfg["hdbscan/alpha"] \
            = NodeInfo(np.asarray(alpha, np.float32), "NX_ANY", np.float32)


class ParmsetupClusterer(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the spatstat tool."""

    def __init__(self):
        super().__init__("clusterer", "cluster_analysis1")
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        self.number_of_tasks += 1
        if isinstance(task_type, ClustererTask) is True:
            self.prefix = f"/ENTRY[entry{self.entry_id}]/APM_PARAPROBE_TOOL_CONFIG" \
                          f"[{self.taskname.rstrip(digits)}{self.number_of_tasks}]/"
        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value  # will this induce a deep-copy?
        # add also the filter settings which are childs of
        # the task_types cfg_filter class instance
        for trailing_path, value in task_type.flt.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value

    def configure(self, simid, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        self.nodes[f"/ENTRY[entry{self.entry_id}]/number_of_tasks"] \
            = NodeInfo(self.number_of_tasks, "", np.uint32)
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)
