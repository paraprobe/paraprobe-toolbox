#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Create NeXus/HDF5 configuration files for paraprobe-distancer."""

import numpy as np

from utils.src.python.numerics import EPSILON
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase
from utils.src.python.hashing import get_file_hashvalue


class PointToTriangleSetDistancing(ParmsetupTaskBase):
    """Metadata of a task for computing ion position to triangle set distances."""

    def __init__(self):
        super().__init__("distancer", "point_to_triangle")
        self.n_triangle_set = 0

    def add_triangle_set(self, fpath: str = "",
                         vertices_dset_name: str = "",
                         facet_indices_dset_name: str = "",
                         **kwargs):
        """Specify set of triangles in an HDF5 file."""
        self.n_triangle_set += 1
        trg = f"SERIALIZED[triangle_set{self.n_triangle_set}]/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] \
            = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}vertices"] \
            = NodeInfo(vertices_dset_name)
        self.cfg[f"{trg}indices"] \
            = NodeInfo(facet_indices_dset_name)
        # kwargs
        if "vertex_normals_dset_name" in kwargs:
            self.cfg[f"{trg}vertex_normals"] \
                = NodeInfo(kwargs["vertex_normals_dset_name"])
        if "facet_normals_dset_name" in kwargs:
            self.cfg[f"{trg}face_normals"] \
                = NodeInfo(kwargs["facet_normals_dset_name"])
        if "facet_patch_dset_name" in kwargs:
            self.cfg[f"{trg}patch_identifier"] \
                = NodeInfo(kwargs["facet_patch_dset_name"])
        if ("method" in kwargs) and ("patch_identifier" in kwargs):
            if kwargs["method"] not in ["whitelist", "blacklist"]:
                raise ValueError("Keyword argument method has to be in (whitelist, blacklist)!")
            if not (isinstance(kwargs["patch_identifier"], list)) \
                or not (all(isinstance(i, int) for i in kwargs["patch_identifier"])) \
                or not (all((i >= 0) for i in kwargs["patch_identifier"])):
                raise ValueError("Keyword argument patch_identifier has to be a list of integer >= 0!")
            self.cfg[f"{trg}MATCH_FILTER[patch_filter]"] \
                = NodeInfo("NXmatch_filter")
            self.cfg[f"{trg}MATCH_FILTER[patch_filter]/method"] \
                = NodeInfo(kwargs["method"])
            self.cfg[f"{trg}MATCH_FILTER[patch_filter]/match"] \
                = NodeInfo(kwargs["method"], "", np.uint32)

    def set_distancing_rules(self, method="default", **kwargs):
        """Specify details for the distancing."""
        allowed = ["default", "skin"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        self.cfg[f"method"] = NodeInfo(method)
        if method == "skin":
            if "threshold_distance" not in kwargs:
                raise KeyError("Keyword argument threshold_distance is required for method skin!")
            if not isinstance(kwargs["threshold_distance"], float) or kwargs["threshold_distance"] < EPSILON:
                raise ValueError(f"Keyword argument threshold_distance needs to be a float at least as large as {EPSILON} !")
            self.cfg[f"threshold_distance"] \
                = NodeInfo(kwargs["threshold_distance"], "nm", np.float32)


class ParmsetupDistancer(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the distancer tool."""

    def __init__(self):
        super().__init__("distancer", "point_to_triangle")
        self.toolname = "distancer"
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        if isinstance(task_type, PointToTriangleSetDistancing) is True:
            self.number_of_tasks += 1
            if self.number_of_tasks > 1:
                raise ValueError("Only one point-to-triangle task!")
            self.prefix = f"/ENTRY[entry{self.entry_id}]/{self.taskname}/"
        else:
            print("Argument task_type is not supported !")
            return

        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value
        # do not forget the filters!
        for trailing_path, value in task_type.flt.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value

    def compute_ion_to_edge_model_distances(self,
                                            recon_fpath: str = "",
                                            range_fpath: str = "",
                                            edge_fpath: str = "",
                                            jobid: int = 0,
                                            verbose=False):
        """Define single task.

        Assumptions for which this convenience function can be used:
        * Entire dataset is analyzed
        * Reconstruction, ranging definitions, and edge model are stored
          in HDF5 files with the same jobid.
        * Distances are computed for all ions.
        For everything more involved use or script
        like exemplified below.
        """
        task = PointToTriangleSetDistancing()

        if jobid <= 0:
            raise ValueError("Argument jobid must not be zero!")
        task.load_reconstruction(recon_fpath=recon_fpath)
        task.load_ranging(iontypes_fpath=range_fpath)
        # optional filters

        task.add_triangle_set(
            fpath=edge_fpath,
            vertices_dset_name=f"/entry{self.entry_id}/point_set_wrapping/"
                               f"alpha_complex1/triangle_set/triangles/vertices",
            facet_indices_dset_name=f"/entry{self.entry_id}/point_set_wrapping/"
                                    f"alpha_complex1/triangle_set/triangles/faces")
        # patch identifier
        # and then inject these like so:
        # self.set_triangles_add(
        #     vertices=np.ndarray(float32),
        #     facet_indices=np.ndarray(uint32),
        #     facet_ids=np.ndarray(uint32))
        # register all these triangles for the process
        task.set_distancing_rules()
        # task.set_distancing_rules(method="skin", threshold_distance=2.)

        self.add_task(task)
        return self.configure(jobid, verbose=verbose)

    def configure(self, simid, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)
