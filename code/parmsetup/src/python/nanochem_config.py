#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
""""Create NeXus/HDF5 configuration files for paraprobe-nanochem."""

from string import digits
import h5py
import numpy as np
from os.path import exists
from ase.data import atomic_numbers, chemical_symbols

from utils.src.python.numerics import \
    TOOLS_IN_TOOLBOX, EPSILON, MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION
from utils.src.python.hashing import get_file_hashvalue
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase
from utils.src.python.arg_validation import validate_argument_str
from utils.src.python.primscontinuum import RoiRotatedCylinder
from ifes_apt_tc_data_modeling.utils.utils import \
    get_smart_chemical_symbols, isotope_to_hash
from ifes_apt_tc_data_modeling.utils.nist_isotope_data import isotopes
from ifes_apt_tc_data_modeling.utils.utils import \
    is_convertible_to_isotope_hash, element_or_nuclide_to_hash


# check for additional code in f607536719bb6cb8a227e91c7fd902dc532ff248

class Delocalization(ParmsetupTaskBase):
    """Metadata for a delocalization."""

    def __init__(self):
        super().__init__("nanochem", "delocalization")

    def load_edge_model(self, fpath: str = "", vertices_dset_name: str = "", facet_indices_dset_name: str = ""):
         """Specify HDF5 file where triangle data should be loaded from."""
         super().load_edge_model(fpath, "surface/", vertices_dset_name, facet_indices_dset_name)

    def load_ion_to_edge_distances(self, fpath: str = "", dset_name: str = ""):
        """Specify HDF5 file with precomputed ion-to-edge distances."""
        super().load_ion_to_edge_distances(fpath, "surface_distance/", dset_name)

    def set_delocalization_input(self, method: str = "compute"):
        """Define if the delocalization should be computed anew or loaded."""
        allowed = ["compute"]
        if method not in allowed:
            raise ValueError(f"Argument source needs to be in {allowed}!")
        self.cfg["method"] = NodeInfo(method)

    def set_delocalization_normalization(self, method="none"):
        """Define how to eventually normalize the results of delocalization."""
        allowed = ["none", "composition", "concentration"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        self.cfg["normalization"] = NodeInfo(method)

    def set_delocalization_whitelist(self,
                                     method: str = "resolve_element",
                                     nuclide_hash = [],
                                     **kwargs):
        """Define the decomposition filter to identify from which intensity to build the delocalization."""
        allowed = ["resolve_none",
                   "resolve_point",
                   "resolve_atom",
                   "resolve_unknown",
                   "resolve_element", "resolve_element_charge",
                   "resolve_isotope", "resolve_isotope_charge",
                   "resolve_ion", "resolve_ion_charge"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        if not isinstance(nuclide_hash, (list, np.ndarray)):
            raise TypeError("Argument nuclide_hash needs to be list or np.ndarray!")
        uniq_symbols = list(set(nuclide_hash))
        # accept only unique symbols to support users with obtaining a correct configuration
        for symbol in uniq_symbols:
            if (symbol not in get_smart_chemical_symbols()) and (symbol < 0 or symbol > np.iinfo(np.uint16).max):
                raise ValueError(f"Value {symbol} needs to be either a chemical_symbol of an element or nuclide hash on [0, {np.iinfo(np.uint16).max}]!")
        if len(uniq_symbols) > MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION:
            raise ValueError(f"Currently one can only define one isotope vector")
        if "charge_state" in kwargs:
            if not isinstance(kwargs["charge_state"], (list, np.ndarray)):
                raise TypeError("Keyword argument charge_state needs to be a list or np.ndarray!")
            for val in kwargs["charge_state"]:
                if isinstance(val, str):
                    if not val.isnumeric() and not (-8 < int(val) < 8):
                        raise ValueError("Each value in keyword argument charge_state needs to be an int on [-7, +7]!")
                if isinstance(val, int):
                    if not (-8 < val < 8):
                        raise ValueError("Each value in keyword argument charge_state needs to be an int on [-7, +7]!")
        # in principle one could use a list of lists and thus compose very complex cases like
        # create a composite delocalization where all signal from 14C++ and 12C++++ is added
        # here implemented though is only the example of a single contribution if one is interested
        # in e.g. iso-surface from Ti and Cr one would use method resolve_element and then add Ti, Cr
        # create a matrix (for now with only one row!) with an isotope vector
        matrix = np.zeros((MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION,), np.uint16)
        for idx, symbol in enumerate(uniq_symbols):
            if symbol in get_smart_chemical_symbols():
                matrix[idx] = isotope_to_hash(atomic_numbers[symbol], 0)
            else:
                matrix[idx] = symbol
        matrix = np.sort(np.asarray(matrix), kind="mergesort")[::-1]
        self.cfg["decomposition/@NX_class"] = NodeInfo("NXmatch_filter")
        self.cfg["decomposition/method"] = NodeInfo(method)
        self.cfg["decomposition/nuclide_whitelist"] = NodeInfo(matrix, "", np.uint16)
        if "charge_state" in kwargs:
            self.cfg["decomposition/charge_state_whitelist"] \
                = NodeInfo(np.asarray(list(set(kwargs["charge_state"])), np.int32), "", np.int32)

    def set_delocalization_gridresolutions(self, length):
        """Define a list of delocalization grid voxel_edge_length values."""
        if not isinstance(length, (list, np.ndarray)):
            raise TypeError("Argument length needs to be a list!")
        for val in length:
            if not isinstance(val, float) or val < EPSILON:
                raise ValueError(f"Value {val} needs to a float at least as large as {EPSILON} nm !")
        self.cfg["grid_resolution"] = NodeInfo(length, "nm", np.float32)

    def set_delocalization_kernel(self, sigma, size: int = 2):
        """Define a list of kernel variance values and the size of kernel."""
        if not isinstance(sigma, (list, np.ndarray)):
            raise TypeError("Argument sigma needs to be a list!")
        for val in sigma:
            if not isinstance(val, float) or val < EPSILON:
                raise ValueError(f"Argument sigma needs to be a float at least as large as {EPSILON} nm !")
        if not isinstance(size, int) or size < 1 or size > 10:
            raise ValueError(f"Argument size needs to be an integer on [1, 10] !")
        self.cfg["kernel_variance"] = NodeInfo(sigma, "nm", np.float32)
        self.cfg["kernel_size"] = NodeInfo(size, "", np.uint32)

    def set_delocalization_edge_handling(self, method: str = "default"):
        """Define how triangles at the edge of the dataset should be handled."""
        allowed = ["default", "keep_edge_triangles"]
        if method not in allowed:
            raise ValueError(f"Argument methods needs to be in {allowed}!")
        self.cfg["PROCESS[isosurfacing]/edge_method"] = NodeInfo(method)

    def set_delocalization_edge_threshold(self, d_edge: float = 1.):
        """Define which ion-to-edge distance to decide proximity of objects."""
        if not isinstance(d_edge, float) or d_edge < EPSILON:
            raise ValueError(f"Argument d_edge needs to be a float at least as large as {EPSILON} nm !")
        self.cfg["PROCESS[isosurfacing]/edge_threshold"] = NodeInfo(d_edge, "nm", np.float32)

    def set_delocalization_isosurfaces(self, phi):
        """Define which iso-contour values to probe."""
        if not isinstance(phi, (list, np.ndarray)):
            raise TypeError("Argument phi needs to be a list!")
        uniq_phi = list(set(phi))
        for val in uniq_phi:
            if not isinstance(val, float) or val < EPSILON:
                raise ValueError(f"Value {val} needs to be a float at least as large as {EPSILON} nm !")
        self.cfg["PROCESS[isosurfacing]/phi"] = NodeInfo(phi, "", np.float32)
        # uses implicit h5py numpy dtype conversion

    def report_fields_and_gradients(self, boolean: bool = False):
        self.report_named_option("has_scalar_fields", boolean)

    def report_triangle_soup(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_triangle_soup", boolean)

    def report_objects(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object", boolean)

    def report_objects_geometry(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object_geometry", boolean)

    def report_objects_properties(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object_properties", boolean)

    def report_objects_optimal_bounding_box(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object_obb", boolean)

    def report_objects_ions(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object_ions", boolean)

    def report_objects_edge_contact(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object_edge_contact", boolean)

    def report_proxies(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_proxy", boolean)

    def report_proxies_geometry(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_proxy_geometry", boolean)

    def report_proxies_properties(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_proxy_properties", boolean)

    def report_proxies_optimal_bounding_box(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_proxy_obb", boolean)

    def report_proxies_ions(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_proxy_ions", boolean)

    def report_proxies_edge_contact(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_proxy_edge_contact", boolean)

    def report_objects_autoproxigrams(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object_proxigram", boolean)

    def report_objects_autoproxigrams_edge_contact(self, boolean: bool = False):
        self.report_named_option("PROCESS[isosurfacing]/has_object_proxigram_edge_contact", boolean)


class InterfaceMeshing(ParmsetupTaskBase):
    """Metadata to an automated interface meshing task."""

    def __init__(self, *args, **kwargs):
        super().__init__("nanochem", "interface_meshing")
        self.set_mesh_initialization("default")  # fpath="controlpoints.h5")
        self.set_dcom_mesh_refinement(
            target_edge_length=[12., 10., 8., 6., 4., 2.],
            target_dcom_radius=[12., 10., 8., 6., 4., 2.],
            target_smoothing_step=[10, 10, 10, 10, 10, 10])

    def load_edge_model(self, fpath: str = "", vertices_dset_name: str = "", facet_indices_dset_name: str = ""):
         """Specify HDF5 file where triangle data should be loaded from."""
         super().load_edge_model(fpath, "surface/", vertices_dset_name, facet_indices_dset_name)

    def load_ion_to_edge_distances(self, fpath: str = "", dset_name: str = ""):
        """Specify HDF5 file with precomputed ion-to-edge distances."""
        super().load_ion_to_edge_distances(fpath, "surface_distance/", dset_name)

    def set_mesh_initialization(self, method: str = "default", **kwargs):
        """Define how to initialize the plane prior the PCA."""
        allowed = ["default", "control_point_file"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        self.cfg["initialization"] = NodeInfo(method)
        if "fpath" in kwargs:
            trg = "control_point/"
            self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
            self.cfg[f"{trg}type"] = NodeInfo("file")
            self.cfg[f"{trg}path"] = NodeInfo(kwargs["fpath"])
            self.cfg[f"{trg}checksum"] = NodeInfo(get_file_hashvalue(kwargs["fpath"]))
            self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
            if not exists(kwargs["fpath"]):
                raise ValueError(f"Control_point_file {kwargs['fpath']} does not exist !")
            # the pandas/PyTables conda route
            # dat = pd.read_hdf(fnm, "data")
            # xyz = dat.drop_duplicates().to_numpy(dtype=np.float32)
            # alternatively the h5py native route
            with h5py.File(kwargs["fpath"], "r") as h5r:
                if "xyz" not in h5r:
                    raise ValueError("Control point file does not contain dataset named xyz!")
                if len(np.shape(h5r["xyz"])) != 2 or (np.shape(h5r["xyz"])[0] < 3) or (np.shape(h5r["xyz"])[1] != 3):
                    raise ValueError("Control point file needs dataset named xyz coordinate value set of shape (N(>=3), 3)!")
                xyz = h5r["xyz"]
                print(f"np.shape(xyz) {np.shape(xyz)}")
                self.cfg[f"{trg}control_points"] \
                    = NodeInfo(np.asarray(xyz), "nm", np.float32)

    def set_decoration(self, symbol_lst):
        """Define a list of element symbols to use as the decorating species."""
        uniq_symbols = list(set(symbol_lst))
        for symbol in uniq_symbols:
            if is_convertible_to_isotope_hash(symbol) not in (1, 2):
                raise ValueError(f"Symbol {symbol} is neither <symbol> nor <symbol>-<mass_number> string!")
        matrix = np.zeros((len(uniq_symbols), MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION), np.uint16)
        for idx, symbol in enumerate(list(set(symbol_lst))):
            matrix[idx][0] = element_or_nuclide_to_hash(symbol)
        self.cfg["MATCH_FILTER[decoration_filter]/@NX_class"] = NodeInfo("NXmatch_filter")
        self.cfg["MATCH_FILTER[decoration_filter]/method"] = NodeInfo("whitelist")
        self.cfg["MATCH_FILTER[decoration_filter]/match"] = NodeInfo(matrix, "", np.uint16)

    def set_dcom_mesh_refinement(self,
                                 target_edge_length=[],
                                 target_dcom_radius=[],
                                 target_smoothing_step=[]):
        """Specify the setting of the iterative mesh refinement."""
        # target_edge_length=[10.0, 8.0, 6.0, 4.0, 2.0, 1.0]
        # target_dcom_radius=[10.0, 8.0, 6.0, 4.0, 2.0, 1.0]
        # target_smoothing_step=[10, 10, 10, 10, 10, 10]
        # defines six iteration
        if not isinstance(target_edge_length, list) \
            or not isinstance(target_dcom_radius, list) \
            or not isinstance(target_smoothing_step, list):
            raise ValueError("Arguments target_* edge_length, dcom_radius and smoothing_step have to be lists!")
        if not all(isinstance(val, float) for val in target_edge_length) \
            or not all(isinstance(val, float) for val in target_dcom_radius) \
            or not all(isinstance(val, int) for val in target_smoothing_step):
            raise ValueError("Arguments target_* edge_length and to be dcom_radius list of floats and smoothing_step of int")
        if len(target_edge_length) == 0 \
            or len(target_edge_length) != len(target_dcom_radius) \
            or len(target_edge_length) != len(target_smoothing_step):
            raise ValueError("Arguments target_* edge_length, dcom_radius and smoothing_step have to be non-empty lists!")
        if not all(val >= 1. for val in target_edge_length) \
            or not all(val >= 1. for val in target_dcom_radius) \
            or not all(val >= 1 for val in target_smoothing_step):
            raise ValueError("Arguments target_* edge_length, dcom_radius, smoothing_step have to be >= 1 !")
        if (np.asarray(target_edge_length) != np.sort(np.asarray(target_edge_length), kind="mergesort")[::-1]).all():
            raise ValueError("Argument target_edge_length array is not sorted in decreasing order!")
        if (np.asarray(target_dcom_radius) != np.sort(np.asarray(target_dcom_radius), kind="mergesort")[::-1]).all():
            raise ValueError("Argument target_dcom_radius array is not sorted in decreasing order!")
        # no constraint on target_smoothing_step!
        self.cfg["method"] = NodeInfo("pca_plus_dcom")
        self.cfg["number_of_iterations"] = NodeInfo(len(target_edge_length), "", np.uint32)
        self.cfg["target_edge_length"] = NodeInfo(np.asarray(target_edge_length), "nm", np.float32)
        self.cfg["target_dcom_radius"] = NodeInfo(np.asarray(target_dcom_radius), "nm", np.float32)
        self.cfg["target_smoothing_step"] = NodeInfo(np.asarray(target_smoothing_step), "", np.float32)


class OnedProfiles(ParmsetupTaskBase):
    """Create one-dimensional ROIs with local profiles throughout the dataset."""

    def __init__(self, *args, **kwargs):
        super().__init__("nanochem", "oned_profile")

    def load_edge_model(self,
                        fpath: str = "",
                        vertices_dset_name: str = "",
                        facet_indices_dset_name: str = ""):
         """Specify HDF5 file where triangle data should be loaded from."""
         super().load_edge_model(fpath, "surface/", vertices_dset_name, facet_indices_dset_name)

    def load_ion_to_edge_distances(self, fpath: str = "", dset_name: str = ""):
        """Specify HDF5 file with precomputed ion-to-edge distances."""
        super().load_ion_to_edge_distances(fpath, "surface_distance/", dset_name)

    def load_feature_mesh(self,
                          fpath: str = "",
                          vertices_dset_name: str = "",
                          facet_indices_dset_name: str = "",
                          facet_normals_dset_name: str = "",
                          **kwargs):
        """Load precomputed feature triangulated surface mesh."""
        # vertex normals are currently not implemented/supported as for meshes
        # as fine as 2nm edge length triangle length the vertex normals converge
        # to facet_normals for smooth manifold objects
        trg = "feature/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}vertices"] = NodeInfo(vertices_dset_name)
        self.cfg[f"{trg}indices"] = NodeInfo(facet_indices_dset_name)
        # self.cfg[f"{trg}vertex_normals"] = NodeInfo(vnormals_dset_name)
        self.cfg[f"{trg}facet_normals"] = NodeInfo(facet_normals_dset_name)
        if "patch_identifier" in kwargs:
             # implements the whitelist case only
             if not isinstance(kwargs["patch_identifier"], (list, np.ndarray)):
                raise ValueError("Kwarg patch_identifier needs to be a non-empty list")
             if not all((isinstance(val, int) and val >= 0) for val in kwargs["patch_identifier"]):
                 raise ValueError("Kwarg patch_identifier needs have only >= 0 integer!")
             self.cfg[f"{trg}MATCH_FILTER[patch_filter]/method"] = NodeInfo("whitelist")
             self.cfg[f"{trg}MATCH_FILTER[patch_filter]/match"] \
                = NodeInfo(kwargs["patch_identifier"], "", np.uint32)

    def load_ion_to_feature_distances(self, fpath: str = "", dset_name: str = ""):
        """Specify HDF5 file with precomputed ion-to-edge distances."""
        validate_argument_str(fpath)
        validate_argument_str(dset_name)
        trg = "feature_distance/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}distance"] = NodeInfo(dset_name)

    def set_distancing_model(self, method: str = "project_to_triangle_plane"):
        """Specify how to compute the distance of the ion to the ROI feature."""
        allowed = ["project_to_triangle_plane", "ion_to_feature"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        self.cfg["distancing_model"] = NodeInfo("project_to_triangle_plane")

    def set_direction_model(self, method: str = "triangle_outer_unit_normal"):
        """Specify how to orient each ROI when placed at triangle facet."""
        allowed = ["triangle_outer_unit_normal"]  # "angularly_geodesic_sphere"
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        self.cfg["direction_model"] = NodeInfo("triangle_outer_unit_normal")

    def set_roi_cylinder_dimensions(self, height: float = 10., radius: float = 2.):
        """Specify the dimensions of the cylindrical ROI to place."""
        if not isinstance(height, float) or not isinstance(radius, float):
            raise ValueError("Arguments height and radius have to be float!")
        if height < EPSILON or radius < EPSILON:
            raise ValueError(f"Arguments height and radius needs to be at least {EPSILON} nm !")
        self.cfg["roi_cylinder_height"] = NodeInfo(height, "nm", np.float32)
        self.cfg["roi_cylinder_radius"] = NodeInfo(radius, "nm", np.float32)

    def set_user_defined_rois(self, primitive_list=None):
        """Import ROIs as defined by the user."""
        n_cyl = 0
        n_cyl_center = []
        n_cyl_height = []
        n_cyl_radii = []
        for prim in primitive_list:
            if isinstance(prim, RoiRotatedCylinder) is True:
                xyz = [0., 0., 0.]
                uvw = [0., 0., 0.]
                for i in np.arange(0, 3):
                    xyz[i] = 0.5 * (prim.center_bottom_cap[i] + prim.center_top_cap[i])
                    uvw[i] = prim.center_top_cap[i] - prim.center_bottom_cap[i]
                n_cyl_center.append(xyz)
                n_cyl_height.append(uvw)
                n_cyl_radii.append(prim.radius)
                n_cyl += 1
        if n_cyl > 0:
            self.cfg[f"user_defined_roi/@NX_class"] = NodeInfo("NXobject")
            trg = "user_defined_roi/cylinder_set/"
            self.cfg[f"{trg}@NX_class"] = NodeInfo("NXcg_cylinder_set")
            self.cfg[f"{trg}dimensionality"] = NodeInfo(3, "", np.uint32)
            self.cfg[f"{trg}cardinality"] = NodeInfo(n_cyl, "", np.uint32)
            self.cfg[f"{trg}identifier_offset"] = NodeInfo(0, "", np.uint32)
            self.cfg[f"{trg}center"] = NodeInfo(
                np.asarray(n_cyl_center, np.float32), "nm", np.float32)
            self.cfg[f"{trg}height"] = NodeInfo(
                np.asarray(n_cyl_height, np.float32), "nm", np.float32)
            self.cfg[f"{trg}radii"] = NodeInfo(
                np.asarray(n_cyl_radii, np.float32), "nm", np.float32)


class ParmsetupNanochem(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the nanochem tool."""

    def __init__(self):
        super().__init__("nanochem", "")
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        if isinstance(task_type, Delocalization):
            # self.number_of_tasks += 1
            self.prefix = f"/ENTRY[entry{self.entry_id}]/APM_PARAPROBE_TOOL_CONFIG[delocalization]/"
            # make case-dependent configuration which can only be processed when the task has been defined completely
            if ("normalization" not in task_type.cfg) or ("PROCESS[isosurfacing]/phi" not in task_type.cfg):
                raise ValueError("Normalization and isosurface values have to be defined!")
            normalization = task_type.cfg["normalization"].value
            # if normalization == "none":
            #     task_type.cfg["PROCESS[isosurfacing]/phi/@units"] = NodeInfo("1")
            if normalization == "composition":
                task_type.cfg["PROCESS[isosurfacing]/phi/@units"] = NodeInfo("at.-%")
                task_type.cfg["PROCESS[isosurfacing]/phi"] = NodeInfo(
                    np.multiply(task_type.cfg["PROCESS[isosurfacing]/phi"].value, 100.), "", np.float32)
            elif normalization == "concentration":
                task_type.cfg["PROCESS[isosurfacing]/phi/@units"] = NodeInfo("1/nm^3")
            elif normalization != "none":
                raise ValueError("Normalization is not a supported one!")
        elif isinstance(task_type, InterfaceMeshing):
            # self.number_of_tasks += 1
            self.prefix = f"/ENTRY[entry{self.entry_id}]/APM_PARAPROBE_TOOL_CONFIG[interface_meshing]/"
        elif isinstance(task_type, OnedProfiles):
            self.prefix = f"/ENTRY[entry{self.entry_id}]/APM_PARAPROBE_TOOL_CONFIG[oned_profile]/"
        else:
            raise ValueError("Argument task_type needs to be an instance of Delocalization, InterfaceMeshing, or OnedProfiles")

        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise ValueError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value
        for trailing_path, value in task_type.flt.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes.keys():
                raise ValueError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value

    def configure(self, simid, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)

# convenience functions follow which exemplify configurations for frequently performed tasks
