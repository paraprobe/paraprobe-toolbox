#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Create NeXus/HDF5 configuration files for paraprobe-selector."""

from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase


class RoiSelectionTask(ParmsetupTaskBase):
    """Metadata of a task for region-of-interest (ROI) selection."""

    def __init__(self):
        super().__init__("selector", "select")


class ParmsetupSelector(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the selector tool."""

    def __init__(self):
        super().__init__("selector", "select")
        self.toolname = "selector"
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        if isinstance(task_type, RoiSelectionTask) is True:
            self.number_of_tasks += 1
            if self.number_of_tasks > 1:
                raise ValueError("Only one select task!")
            self.prefix = f"/ENTRY[entry{self.entry_id}]/{self.taskname}/"
        else:
            print("Argument task_type is not supported !")
            return

        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value  # will this induce a deep-copy?

        for trailing_path, value in task_type.flt.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value

    def configure(self, simid: int = 0, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)
