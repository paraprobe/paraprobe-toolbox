#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Create NeXus/HDF5 configuration files for paraprobe-spatstat."""

from string import digits

import numpy as np

from ifes_apt_tc_data_modeling.utils.utils import symbol_lst_to_matrix_of_nuclide_vector
from utils.src.python.numerics import EPSILON
from utils.src.python.hashing import get_file_hashvalue
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase


class SpatstatTask(ParmsetupTaskBase):
    """Metadata of a task for spatial statistics."""

    def __init__(self):
        super().__init__("spatstat", "spatial_statistics")
        self.cfg = {}
        self.set_random_number_generator()

    def load_ion_to_edge_distances(self,
                                   fpath: str = "",
                                   dset_name: str = "",
                                   d_edge: float = EPSILON):
        """Specify HDF5 file with precomputed ion-to-edge distances."""
        trg = "surface_distance/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}distance"] = NodeInfo(dset_name)
        if not isinstance(d_edge, float) or d_edge < EPSILON:
            raise ValueError(f"Argument d_edge needs to be a float that is at least as large as {EPSILON} nm !")
        self.cfg[f"{trg}edge_distance"] \
            = NodeInfo(np.float32(d_edge), "nm", np.float32)

    def load_ion_to_feature_distances(self,
                                      fpath: str = "",
                                      dset_name: str = "",
                                      d_feature: float = np.finfo(np.float32).max):
        """Specify HDF5 file with precomputed ion-to-feature-set distances."""
        trg = "feature_distance/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}distance"] = NodeInfo(dset_name)
        if not isinstance(d_feature, float) or d_feature < EPSILON:
            raise ValueError(f"Argument d_feature needs to be a float that is at least as large as {EPSILON} nm !")
        self.cfg[f"{trg}feature_distance"] \
            = NodeInfo(np.float32(d_feature), "nm", np.float32)

    def ion_types_source(self,
                         method="resolve_all",
                         symbol_lst=[],
                         **kwargs):
        """Specify the elem., isot., or mol. ion accepted as sources for ROI."""
        method_used, ivec_matrix, charge_vector \
            = symbol_lst_to_matrix_of_nuclide_vector(method, symbol_lst, **kwargs)
        self.cfg["ion_query_type_source"] = NodeInfo(method)
        if ivec_matrix is not None:
            self.cfg["ion_query_nuclide_source"] \
                = NodeInfo(ivec_matrix, "", np.uint16)
        if charge_vector is not None:
            self.cfg["charge_state_source"] \
                = NodeInfo(np.asarray(charge_vector, np.int8), "", np.int8)

    def ion_types_target(self,
                         method="resolve_all",
                         symbol_lst=[],
                         **kwargs):
        """Specify the elem., isot., or mol. ion accept as targets in a ROI."""
        method_used, ivec_matrix, charge_vector \
            = symbol_lst_to_matrix_of_nuclide_vector(method, symbol_lst, **kwargs)
        self.cfg["ion_query_type_target"] = NodeInfo(method)
        if ivec_matrix is not None:
            self.cfg["ion_query_nuclide_target"] \
                = NodeInfo(ivec_matrix, "", np.uint16)
        if charge_vector is not None:
            self.cfg["charge_state_array_target"] \
                = NodeInfo(np.asarray(charge_vector, np.int8), "", np.int8)

    def set_random_number_generator(self):
        """Specify the random number generator."""
        self.cfg["randomize_ion_types"] \
            = NodeInfo(np.uint8(True), "", np.uint8)
        self.cfg["random_number_generator/@NX_class"] = NodeInfo("NXcs_prng")
        self.cfg["random_number_generator/type"] = NodeInfo("mt19937")
        self.cfg["random_number_generator/seed"] \
            = NodeInfo(-12345678, "", np.int32)
        self.cfg["random_number_generator/warmup"] \
            = NodeInfo(700000, "", np.uint32)

    def set_knn(self, kth=1, binwidth=0.1, rmax=2.):
        """Specify parameter for k-th nearest neighbour statistics."""
        if not isinstance(kth, int):
            raise TypeError("Argument kth needs to be an int!")
        if kth < 1 or kth >= np.iinfo(np.uint32).max:
            raise ValueError(f"Argument kth needs to be on [1, {np.iinfo(np.uint32).max})")
        if not isinstance(binwidth, float):
            raise TypeError("Argument binwidth needs to be a float!")
        if binwidth < 0.001:
            raise ValueError("Argument binwidth has to be a float of at least 0.001 nm!")
        if not isinstance(rmax, float):
            raise TypeError("Argument rmax needs to be a float!")
        if rmax < binwidth:
            raise ValueError(f"Argument rmax has to be a float of at least {binwidth} !")
        self.cfg["statistics/@NX_class"] = NodeInfo("NXprocess")
        self.cfg["statistics/knn/@NX_class"] = NodeInfo("NXprocess")
        self.cfg["statistics/knn/kth"] \
            = NodeInfo(np.uint32(kth), "", np.uint32)
        self.cfg["statistics/knn/min_incr_max"] \
            = NodeInfo(np.asarray([0., binwidth, rmax], np.float32), "nm", np.float32)

    def set_rdf(self, binwidth=0.1, rmax=2.):
        """Specify parameter for radial distribution function."""
        if not isinstance(binwidth, float):
            raise TypeError("Argument binwidth needs to be a float!")
        if binwidth < 0.001:
            raise ValueError("Argument binwidth has to be a float of at least 0.001 nm!")
        if not isinstance(rmax, float):
            raise TypeError("Argument rmax needs to be a float!")
        if rmax < binwidth:
            raise ValueError(f"Argument rmax has to be a float of at least {binwidth} !")
        self.cfg["statistics/@NX_class"] = NodeInfo("NXprocess")
        self.cfg["statistics/rdf/@NX_class"] = NodeInfo("NXprocess")
        self.cfg["statistics/rdf/min_incr_max"] \
            = NodeInfo(np.asarray([0., binwidth, rmax], np.float32),
                       "nm", np.float32)


class ParmsetupSpatstat(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the spatstat tool."""

    def __init__(self):
        super().__init__("spatstat", "spatial_statistics1")
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        self.number_of_tasks += 1
        if isinstance(task_type, SpatstatTask) is True:
            self.prefix = f"/ENTRY[entry{self.entry_id}]/APM_PARAPROBE_TOOL_CONFIG" \
                          f"[{self.taskname.rstrip(digits)}{self.number_of_tasks}]/"
            for trailing_path, value in task_type.cfg.items():
                full_path = f"{self.prefix}{trailing_path}"
                if full_path in self.nodes:
                    raise KeyError(f"Keyword {full_path} was already defined !")
                self.nodes[full_path] = value
            # add filter settings, the childs of the task_type's flt class
            for trailing_path, value in task_type.flt.cfg.items():
                full_path = f"{self.prefix}{trailing_path}"
                if full_path in self.nodes:
                    raise KeyError(f"Keyword {full_path} was already defined !")
                self.nodes[full_path] = value

    def configure(self, simid, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        self.nodes[f"/ENTRY[entry{self.entry_id}]/number_of_tasks"] \
            = NodeInfo(self.number_of_tasks, "", np.uint32)
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)
