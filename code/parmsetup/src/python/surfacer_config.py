#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Create NeXus/HDF5 configuration files for paraprobe-surfacer."""

import numpy as np

from utils.src.python.numerics import EPSILON
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase


class SurfaceMeshingTask(ParmsetupTaskBase):
    """Metadata of a task for computing alpha shapes and wrappings to ions."""

    def __init__(self):
        super().__init__("surfacer", "surface_meshing")

    def set_point_cloud_preprocessing(self,
                                      method: str = "default",
                                      width: int = 1):
        """How is the point cloud preprocessed before passing to ashapes."""
        allowed = ["default", "kuehbach"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        if not isinstance(width, int) or width < 1 or width > 10:
            raise ValueError(f"Argument width needs to be an integer from [1, 10] !")
        trg = ""
        self.cfg[f"{trg}/preprocessing/@NX_class"] = NodeInfo("NXprocess")
        self.cfg[f"{trg}/preprocessing/method"] = NodeInfo(method)
        self.cfg[f"{trg}/preprocessing/kernel_width"] \
            = NodeInfo(np.uint32(width), "", np.uint32)

    def set_alphashape_choice(self,
                              choice: str = "convex_hull_naive",
                              **kwargs):
        """Specify the method to decide which alpha value to choose."""
        allowed = ["convex_hull_naive", "convex_hull_refine", "smallest_solid",
                   "cgal_optimal", "set_of_values", "set_of_alpha_wrappings"]
        if choice not in allowed:
            raise ValueError("Argument choice needs to be in {allowed}!")
        trg = ""
        self.cfg[f"{trg}alpha_value_choice"] = NodeInfo(choice)
        if choice in ("set_of_values", "set_of_alpha_wrappings"):
            if "alpha_values" not in kwargs:
                raise KeyError("Option set_of_values requires kwarg alpha_values!")
            if not isinstance(kwargs["alpha_values"], list):
                raise TypeError("Keyword argument alpha_values should be a list!")
            for alpha in kwargs["alpha_values"]:
                if not isinstance(alpha, float) or alpha < EPSILON:
                    raise ValueError(f"Argument alpha {alpha} needs to be a float at least as large as {EPSILON}!")
            if choice == "set_of_values":
                self.cfg[f"{trg}alpha_values"] \
                    = NodeInfo(np.asarray(kwargs["alpha_values"], np.float32), "nm^2", np.float32)
            else:
                self.cfg[f"{trg}alpha_values"] \
                    = NodeInfo(np.asarray(kwargs["alpha_values"], np.float32), "nm", np.float32)

            if choice == "set_of_alpha_wrappings":
                if "offset_values" not in kwargs:
                    raise KeyError("Option set_of_alpha_wrappings requires kwarg offset_values!")
                if not isinstance(kwargs["offset_values"], list):
                    raise TypeError("Keyword argument offset_values should be a list!")
                if len(kwargs["alpha_values"]) != len(kwargs["offset_values"]):
                    raise ValueError("Keyword arguments alpha_values and offset_values need to have the same length!")
                for offset in kwargs["offset_values"]:
                    if not isinstance(offset, float) or offset < EPSILON:
                        raise ValueError(f"Argument alpha {offset} needs to be a float at least as large as {EPSILON}!")
                self.cfg[f"{trg}offset_values"] \
                    = NodeInfo(np.asarray(kwargs["offset_values"], np.float32), "nm", np.float32)

    def report_exterior_facets(self, boolean: bool = True):
        """Set option to report exterior triangles of the alpha complex."""
        self.report_named_option("has_exterior_facets", boolean)
 
    def report_interior_tetrahedra(self, boolean: bool = False):
        """Set option to report interior tetrahedra of alpha complex."""
        self.report_named_option("has_interior_tetrahedra", boolean)

    def check_mesh_closure(self, boolean: bool = True):
        """Set option to check if the alpha complex is watertight."""
        self.report_named_option("has_closure", boolean)


class ParmsetupSurfacer(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the surfacer tool."""

    def __init__(self):
        super().__init__("surfacer", "surface_meshing")
        self.toolname = "surfacer"
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        if isinstance(task_type, SurfaceMeshingTask) is True:
            self.number_of_tasks += 1
            if self.number_of_tasks > 1:
                raise ValueError("Only one range task!")
            self.prefix = f"/ENTRY[entry{self.entry_id}]/{self.taskname}/"
        else:
            print("Argument task_type is not supported !")
            return

        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value
        # do not forget the filters!
        for trailing_path, value in task_type.flt.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value

    def compute_convex_hull_edge_model(self,
                                       recon_fpath: str = "",
                                       range_fpath: str = "",
                                       jobid: int = 0,
                                       verbose=False):
        """Define single task.

        Assumptions for which this convenience function can be used:
        * Entire dataset is analyzed
        * Reconstructed positions are available
        * Ranging definitions (i.e. NXion instances) are available to evaluate
        * Computes a convex hull only, including ions of unknown type.
        For everything more involved config task and add like shown below.
        """
        task = SurfaceMeshingTask()
        if not isinstance(recon_fpath, str) or recon_fpath == "":
            raise ValueError("Argument recon_fpath needs to be a non-empty string!")
        if not isinstance(jobid, int) or jobid <= 0:
            raise ValueError("Argument jobid needs to be an integer > 0!")

        task.load_reconstruction(recon_fpath)
        task.load_ranging(range_fpath)
        # optional filters
        # task.cfg_filter.add_evaporation_id_filter(
        #     lival=(0, 10, np.iinfo(np.uint32).max))  # each 10-th ion
        # task.cfg_filter.add_iontyp_filter(
        #     method="blacklist", iontypes=[0])  # remove unranged ions
        # task.cfg_filter.add_hit_multiplicity_filter(
        #     method="whitelist", multiplicities=[1])
        # task.cfg_filter.add_spatial_filter(
        #     windowing_mode="entire_dataset")

        # task-specific settings
        task.set_point_cloud_preprocessing("default")
        # task.set_point_cloud_preprocessing(method="kuehbach", width=1)
        task.set_alphashape_choice("convex_hull_naive")  # _refine")
        # infty = np.iinfo(np.float32).max
        # task.set_alphashape_choice(
        #     "set_of_values", alpha=[1.0, 10., 100., 1000., infty])
        task.report_exterior_facets(True)
        task.report_interior_tetrahedra(False)
        task.check_mesh_closure(True)

        self.add_task(task)
        return self.configure(jobid, verbose=verbose)

    def configure(self, simid: int = 0, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be at least 1. 0 is for developers only !")
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)
