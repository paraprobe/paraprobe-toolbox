#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Create NeXus/HDF5 configuration files for paraprobe-tessellator."""

import numpy as np

from utils.src.python.hashing import get_file_hashvalue
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase


class TessellationTask(ParmsetupTaskBase):
    """Metadata of a task for tessellations."""

    def __init__(self):
        super().__init__("tessellator", "tessellate")

    def load_ion_to_edge_distances(self,
                                   fpath: str = "",
                                   dset_name: str = ""):
        """Specify HDF5 file with precomputed ion-to-edge distances."""
        trg = "surface_distance/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] \
            = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}distance"] = NodeInfo(dset_name)

    def set_tessellation_method(self, method="default"):
        """Specify details for tessellation."""
        allowed = ["default"]  # "control_point"
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed}!")
        self.cfg["method"] = NodeInfo(method)
        # if (method == "control_point") \
        #     and ("file_name" in kwargs.keys()) \
        #         and ("dataset_name" in kwargs.keys()):
        #     if not isinstance(kwargs["file_name"], str) or kwargs["file_name"] == "":
        #         raise ValueError("Keyword argument file_name needs to be a non-empty string!")
        #     if not isinstance(kwargs["dataset_name"], str) or kwargs["dataset_name"] == "":
        #         raise ValueError("Keyword argument dataset_name needs to be a non-empty string!")
        #     grpnm += "PROCESS[overlay_control_points]/"
        #     self.cfg[f"{trg}@NX_class"] = NodeInfo("NXprocess")
        #     self.cfg[f"{trg}file_name"] = NodeInfo(kwargs["file_name"])
        #     self.cfg[f"{trg}file_name/@version"] \
        #         = NodeInfo(get_file_hashvalue(kwargs["file_name"]))
        #     self.cfg[f"{trg}dataset_name"] \
        #         = NodeInfo(kwargs["dataset_name"])

    def report_cell_volume(self, boolean: bool = True):
        """Set option to report cell volume."""
        self.report_named_option("has_cell_volume", boolean)

    def report_cell_neighbors(self, boolean: bool = False):
        """Set option to report first-order neighbors of cells."""
        self.report_named_option("has_cell_neighbors", boolean)

    def report_cell_geometry(self, boolean: bool = False):
        """Set option to report vertices and facets of cells."""
        self.report_named_option("has_cell_geometry", boolean)

    def report_cell_edge_contact(self, boolean: bool = False):
        """Set option to report if the cell touches the domain boundary."""
        self.report_named_option("has_cell_edge_detection", boolean)


class ParmsetupTessellator(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the distancer tool."""

    def __init__(self):
        super().__init__("tessellator", "tessellate")
        self.toolname = "tessellator"
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        if isinstance(task_type, TessellationTask) is True:
            self.number_of_tasks += 1
            self.prefix = f"/ENTRY[entry{self.entry_id}]/{self.taskname}/"
        else:
            print("Argument task_type is not supported !")
            return

        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value
        # do not forget the filters!
        for trailing_path, value in task_type.flt.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value

    def compute_complete_voronoi_tessellation(self,
                                              recon_fpath: str = "",
                                              range_fpath: str = "",
                                              jobid: int = 0,
                                              verbose=False):
        """Define single task.

        Assumptions for which this convenience function can be used:
        * Entire dataset is analyzed
        * Reconstruction, ranging definitions, and distances of ions to
          edge model are stored in HDF5 files with the same jobid.
        * Distances are computed for all ions.
        For everything more involved use the guru interface:
        * Copy the code below into your jupyter notebook and replace
          self with the respective class instance.
        * Then comment in or out required commands and customize them to your
          specific analysis needs.
        """
        task = TessellationTask()
        if jobid <= 0:
            raise ValueError("Argument jobid needs to be > 0!")

        task.load_reconstruction(recon_fpath=recon_fpath)
        task.load_ranging(iontypes_fpath=range_fpath)

        # optional filters
        task.load_ion_to_edge_distances(
            fpath=f"PARAPROBE.Distancer.Results.SimID.{jobid}.nxs",
            dset_name=f"/entry{self.entry_id}/point_to_triangle/distance")
        task.set_tessellation_method()
        task.report_cell_volume(True)
        task.report_cell_edge_contact(True)
        task.report_cell_neighbors(False)
        task.report_cell_geometry(False)

        self.add_task(task)
        return self.configure(jobid, verbose=verbose)

    def configure(self, simid, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)
