#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Create NeXus/HDF5 configuration files for paraprobe-transcoder."""

from utils.src.python.hashing import get_file_hashvalue
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.supertool import ParmsetupBase, ParmsetupTaskBase


class TranscodingTask(ParmsetupTaskBase):
    """Metadata of a task for transcoding data from vendor to paraprobe format."""

    def __init__(self):
        super().__init__("transcoder", "transcode")

    def set_reconstruction_filename(self, recon_fpath: str = ""):
        """Specify name of file where to read reconstructed x,y,z and m_z from."""
        if recon_fpath == "":
            return
        trg = "reconstruction/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}path"] = NodeInfo(recon_fpath)
        self.cfg[f"{trg}checksum"] \
            = NodeInfo(get_file_hashvalue(recon_fpath))
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")

    def set_ranging_filename(self, range_fpath: str = ""):
        """Specify name of file where to read iontypes from."""
        if range_fpath == "":
            return
        trg = "ranging/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}path"] = NodeInfo(range_fpath)
        self.cfg[f"{trg}checksum"] \
            = NodeInfo(get_file_hashvalue(range_fpath))
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")


class ParmsetupTranscoder(ParmsetupBase):
    """Be a wizard by creating a config NeXus file for the transcoder tool."""

    def __init__(self):
        super().__init__("transcoder", "transcode")
        self.toolname = "transcoder"
        self.prefix = None
        self.number_of_tasks = 0

    def add_task(self, task_type):
        """Register the task as a process in the task list."""
        if isinstance(task_type, TranscodingTask) is True:
            self.number_of_tasks += 1
            if self.number_of_tasks > 1:
                raise ValueError("Only one transcode task!")
            self.prefix = f"/entry{self.entry_id}/{self.taskname}/"
        else:
            print("Argument task_type is not supported !")
            return

        for trailing_path, value in task_type.cfg.items():
            full_path = f"{self.prefix}{trailing_path}"
            if full_path in self.nodes:
                raise KeyError(f"Keyword {full_path} was already defined !")
            self.nodes[full_path] = value  # will this induce a deep-copy?

    def load_reconstruction_and_ranging(self,
                                        recon_fpath: str = "",
                                        range_fpath: str = "",
                                        jobid: int = 0,
                                        verbose=False):
        """Define single task.

        Assumptions for which this convenience function can be used:
        * Entire dataset is analyzed

        For everything more involved customize calls to functions below:
        * Copy the code below into your jupyter notebook and replace
          self with the respective class instance.
        * Then comment in or out required commands and customize them to your
          specific analysis needs.
        """

        if not isinstance(recon_fpath, str) or recon_fpath == "":
            raise ValueError("Argument recon_fpath needs to be a non-empty string!")
        # range_fpath can be an empty string which means we do not have ranged something
        if not isinstance(jobid, int) or jobid <= 0:
            raise ValueError("Argument jobid must be an integer > 0!")

        task = TranscodingTask()
        task.set_reconstruction_filename(recon_fpath)
        task.set_ranging_filename(range_fpath)
        # no filters for the transcoder, always working on the entire file
        self.add_task(task)
        return self.configure(jobid, verbose=verbose)

    def configure(self, simid: int = 0, verbose=False):
        """Write config file."""
        if not isinstance(simid, int) or simid <= 0:
            raise ValueError("Argument simid needs to be an integer > 0 !")
        # the simid == 0 is reserved for development purposes
        # in which case the C/C++ tool operate differently
        # ##MK::NodeInfo unit should be NX_UNITLESS if dtype is not string
        # and unit is None, so unit NX_DIMENSIONLESS should be specified explicitly
        print("Inspecting whether NeXus/HDF5 is used...")
        trg = f"/entry{self.entry_id}/{self.taskname}/"
        if f"{trg}reconstruction/path" not in self.nodes:
            raise ValueError("No reconstruction was specified!")
        if f"{trg}ranging/path" not in self.nodes:
            raise ValueError("No ranging definitions were specified!")
        recon_path = self.nodes[f"{trg}reconstruction/path"].value
        range_path = self.nodes[f"{trg}ranging/path"].value
        no_transcoding_needed = (recon_path.endswith(".nxs")) \
            and (range_path.endswith(".nxs")) \
            and (recon_path == range_path)
        if no_transcoding_needed is True:
            print("The reconstruction and ranging is provided via NeXus already")
            print("In this case, paraprobe-transcoder will not duplicate data.")
            print("Instead, the tool will read directly from the NeXus/HDF5 file.")
            print("The transcoder results file will only be needed for visualization.")
        else:
            print("The reconstruction and ranging come from files of technology")
            print("partners but the paraprobe-toolbox uses NeXus/HDF5.")
            print("Hence, paraprobe-transcoder will transcode to NeXus/HDF5.")

        if verbose is True:
            for key, val in self.nodes.items():
                print(f"{key}\t{val.value}")

        print("Writing configuration file ...")
        return super().write_config_to_nexus(self.toolname, simid, self.nodes)

        # the C/C++ tools do not implement specifically sophisticated fall-back routines
        # when it comes to the reading of the NeXus config files. The tool just assumes
        # that all required entries are present and then uses the HDF5 library to read
        # from the file. If this fails the C HDF5 library will throw and the C++ tool
        # terminate. Therefore, it is this python tools responsibility
        # to assure that the NeXus file contains what the appdef specifies, i.e.
        # that the contract between the data deliverer (this tool) and the tool is fulfilled.
        # The reason for this design is that implementing configuration file
        # creation at the level of Python is that this enables also people
        # unfamiliar with the C/C++ programming language to take advantage
        # of the tool and enable a larger number of developers to implement changes.
