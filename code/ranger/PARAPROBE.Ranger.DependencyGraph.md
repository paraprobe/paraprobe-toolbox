v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-ranger source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigRanger.h/.cpp
 2. PARAPROBE_RangerStructs.h/.cpp (obsolete)
 3. PARAPROBE_RangerHDF5.h/.cpp (obsolete)
 4. PARAPROBE_RangerXDMF.h/.cpp (obsolete)
 5. PARAPROBE_RangerIonBuilder.h/.cpp
 6. PARAPROBE_RangerHdl.h/.cpp
 7. PARAPROBE_Ranger.cpp
