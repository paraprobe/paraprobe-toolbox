/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigRanger.h"


ostream& operator << (ostream& in, molecular_ion_search_task const & val)
{
	in << "taskid " << val.taskid << "\n";
	in << "Isotopes taken into account" << "\n";
	//in << "Skipping the reporting of isotope_set.size() " << val.isotope_set.size() << " isotopes" << "\n";
	in << "isotope_set.size() " << val.isotope_set.size() << " contains isotopes with the following hashvalues:" << "\n";
	for( auto it = val.isotope_set.begin(); it != val.isotope_set.end(); it++ ) {
		in << *it << "\n";
	}
	in << "mass-to-charge state ratio interval " << val.mq_region << "\n";
	in << "maximum number of nuclids " << val.max_nuclids << " of at most " << MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION << "\n";
	in << "maximum charge " << val.max_charge << " of at most 7" << "\n";
	if ( val.IOStoreNatAbun == true )
		in << "store natural abundance product " << "yes" << "\n";
	else
		in << "store natural abundance product " << "no" << "\n";
	if ( val.IOStoreMass == true )
		in << "store mass " << "yes" << "\n";
	else
		in << "store mass " << "no" << "\n";
	if ( val.IOStoreCharge == true )
		in << "store charge " << "yes" << "\n";
	else
		in << "store charge " << "no" << "\n";
	if ( val.IOStoreMultiplicity == true )
		in << "store multiplicity " << "yes" << "\n";
	else
		in << "store multiplicity " << "no" << "\n";
	return in;
}


string ConfigRanger::InputfileDataset = "";
string ConfigRanger::ReconstructionDatasetName = "";
string ConfigRanger::MassToChargeDatasetName = "";
string ConfigRanger::InputfileIonTypes = "";
string ConfigRanger::IonTypesGroupName = "";
RANGING_METHOD ConfigRanger::RangingMethod = APPLY_EXISTENT_RANGING;

vector<molecular_ion_search_task> ConfigRanger::IonSearchTasks = vector<molecular_ion_search_task>();

apt_real ConfigRanger::PracticalMassToChargeResolution = 1.0 / 2000.0; // mass/charge


bool ConfigRanger::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigRanger::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string dsnm = "";

		RangingMethod = RANGING_METHOD_UNKNOWN;
		apt_uint number_of_processes = 1;
		apt_uint entry_id = 1;

		for ( apt_uint tskid = 0; tskid < number_of_processes; tskid++ ) {
			//##MK::currently this tool assumes and works only with a single analysis task defined
			cout << "Reading configuration for task " << tskid << "\n";

			grpnm = "/entry" + to_string(entry_id) + "/range";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				RangingMethod = APPLY_EXISTENT_RANGING;
				cout << "Ranging with existent ranging definitions" << "\n";
				string path = "";
				if ( h5r.nexus_read( grpnm + "/reconstruction/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileDataset = path_handling( path );
				cout << "InputfileDataset " << InputfileDataset << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";
				path = "";
				if ( h5r.nexus_read( grpnm + "/ranging/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonTypes = path_handling( path );
				cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
				if ( h5r.nexus_read( grpnm + "/ranging/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IonTypesGroupName " << IonTypesGroupName << "\n";
			}
		} //next task

		cout << "Sensible defaults" << "\n";
		cout << "PracticalMassToChargeResolution " << PracticalMassToChargeResolution << "\n";

		return true;
	}
	//special developer case SimID == 0

	return false;
}
