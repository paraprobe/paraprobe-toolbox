/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_RANGER_CONFIG_H__
#define __PARAPROBE_RANGER_CONFIG_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"

enum RANGING_METHOD
{
	RANGING_METHOD_UNKNOWN,
	APPLY_EXISTENT_RANGING,		//external ranging information, ranging was performed with APSuite/IVAS or manually
	MOLECULAR_ION_SEARCH,		//identify which molecular ions to expect in a given mass-to-charge interval
	CHECK_EXISTENT_RANGING		//identify charge states for given ranging definitions
};


struct molecular_ion_search_task
{
	apt_uint taskid;						//taskID
	mqival mq_region;						//mass-to-charge-state region [low, high] in mass/charge
	apt_uint max_nuclids;					//the maximum number of nuclids the molecular ion is composed of must not exceed MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION
	apt_uint max_charge;					//the highest possible (given we are working with APT) charge state
	bool IOStoreNatAbun;
	bool IOStoreMass;
	bool IOStoreCharge;
	bool IOStoreMultiplicity;				//how many disjoint isotopes
	vector<isotope> isotope_set;			//the allowed isotopes (and thus elements) taken into account)

	molecular_ion_search_task() :
		taskid(0), mq_region(mqival()), max_nuclids(0), max_charge(0),
		IOStoreNatAbun(false), IOStoreMass(false), IOStoreCharge(false), IOStoreMultiplicity(false),
		isotope_set(vector<isotope>())  {}

	molecular_ion_search_task( const apt_uint _tskid,
			const mqival _mqival, const apt_uint _maxnuclids, const apt_uint _maxcharge,
			const bool _io_natabun, const bool _io_mass, const bool _io_charge, vector<isotope> const & _isotopes ) :
				taskid(_tskid), mq_region(_mqival), max_nuclids(_maxnuclids), max_charge(_maxcharge),
				IOStoreNatAbun(_io_natabun), IOStoreMass(_io_mass), IOStoreCharge(_io_charge), IOStoreMultiplicity(false) {
		this->isotope_set = _isotopes;
	}
};

ostream& operator << (ostream& in, molecular_ion_search_task const & val);


class ConfigRanger
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static string InputfileDataset;
	static string ReconstructionDatasetName;
	static string MassToChargeDatasetName;
	static string InputfileIonTypes;
	static string IonTypesGroupName;

	static RANGING_METHOD RangingMethod;

	static vector<molecular_ion_search_task> IonSearchTasks;

	//sensible defaults
	static apt_real PracticalMassToChargeResolution;
};

#endif
