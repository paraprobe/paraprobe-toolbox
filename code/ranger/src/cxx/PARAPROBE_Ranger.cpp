/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_RangerHdl.h"


bool parse_configuration_from_nexus()
{
	tool_startup( "paraprobe-ranger" );
	if ( ConfigRanger::read_config_from_nexus( ConfigShared::ConfigurationFile ) == true ) {
		cout << "Configuration from file " << ConfigShared::ConfigurationFile << " was accepted" << "\n";
		cout << "This analysis has SimulationID " << "SimID." <<  ConfigShared::SimID << "\n";
		cout << "Results of this analysis are written to " << ConfigShared::OutputfileName << "\n";
		cout << endl;
		return true;
	}
	cerr << "Parsing configuration failed !" << "\n";
	return false;
}


void ranging( const int r, const int nr )
{
	//allocate process-level instance of a rangerHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	rangerHdl* rngr = NULL;
	int localhealth = 1;
	try {
		rngr = new rangerHdl;
		rngr->set_myrank(r);
		rngr->set_nranks(nr);
		//rngr->init_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Allocating rangerHdl class object failed !" << "\n"; localhealth = 0;
	}

	//the rangeTable class instance that is a member of the rangerHdl//load periodic table of elements and isotope to prepare ranging
	//builds the periodicTable of elements upon construction

	//do we have all processes on board?
	if ( rngr->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete rngr; rngr = NULL; return;
	}
	
	//currently, we have an I/O strategy where only the master reads from file and broadcasts all data to all slaves
	//if ( ConfigRanger::RangingMethod != MOLECULAR_ION_SEARCH ) {
	if ( rngr->get_myrank() == MASTER ) {

		if ( ConfigRanger::RangingMethod != MOLECULAR_ION_SEARCH) {
			if ( rngr->read_relevant_input() == false ) {
				cout << "Rank " << rngr->get_myrank() << " failed reading essential pieces of the relevant input !" << "\n";
				localhealth = 0;
			}
		}
	}
	//##MK::strictly speaking not necessary, but second order issue for about 80 processes as on TALOS...?
	//MPI_Barrier( MPI_COMM_WORLD );
	if ( rngr->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << r << " has recognized that not all data have arrived!" << "\n";
		delete rngr; rngr = NULL; return;
	}


	if ( rngr->get_myrank() == MASTER ) {

		if ( ConfigRanger::RangingMethod == APPLY_EXISTENT_RANGING ) {
			//rngr->identify_charge_states(); //##MK::no longer needed ifes_apt_tc_data_modeling extract charge state now!
			rngr->apply_existent_ranging();
			rngr->compute_composition();
			if ( rngr->write_apply_results_h5() == false ) {
				cerr << "Rank " << "MASTER" << " failed to write results of applied !" << "\n";
			}
		}
		else if ( ConfigRanger::RangingMethod == MOLECULAR_ION_SEARCH ) {
			rngr->search_molecularions();
			//writes results in-place
		}
		/*
		else if ( ConfigRanger::RangingMethod == CHECK_EXISTENT_RANGING ) {
			rngr->identify_charge_states();
			if ( rngr->write_check_results_h5() == false ) {
				cerr << "Rank " << "MASTER" << " failed to write results for checks !" << "\n";
			}
		}
		*/
		else {
			//nothing to do
		}
	}

	cout << "Rank " << r << " has completed its workpackage" << "\n";
	//no barrier required SINGLE_PROCESS only MPI_Barrier(MPI_COMM_WORLD);

	//MPI_Barrier( MPI_COMM_WORLD );
	//release resources
	delete rngr; rngr = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	string start_time = timestamp_now_iso8601();

	if ( argc == 1 || argc == 2 ) {
		string cmd_option = "--help";
		if ( argc == 2 ) { cmd_option = argv[1]; };
		command_line_help( cmd_option, "ranger" );
		return 0;
	}
	else if ( argc == 3 ) {
		ConfigShared::SimID = stoul( argv[SIMID] );
		ConfigShared::ConfigurationFile = argv[CONTROLFILE];
		ConfigShared::OutputfilePrefix = "PARAPROBE.Ranger.Results.SimID." + to_string(ConfigShared::SimID);
		ConfigShared::OutputfileName = ConfigShared::OutputfilePrefix + ".nxs";
		if ( init_results_nxs( ConfigShared::OutputfileName, "ranger", start_time, 1 ) == false ) {
			return 0;
		}
	}
	else {
		return 0;
	}

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( parse_configuration_from_nexus() == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize();
		return 0;
	}
	else { 
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	
	//##MK::currently using MPI to be prepared for the future but only one process is required
	if ( nr == SINGLEPROCESS ) {
        
        cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";
    
//EXECUTE SPECIFIC TASK
        ranging( r, nr );
    }
    else {
        cerr << "Rank " << r << " currently paraprobe-ranger is implemented for a single process only!" << "\n";
    }
    
//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	if ( finish_results_nxs( ConfigShared::OutputfileName, start_time, tic, 1 ) == true ) {
		cout << "paraprobe-ranger success" << endl;
	}
	return 0;
}
