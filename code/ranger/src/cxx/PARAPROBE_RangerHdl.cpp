/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_RangerHdl.h"


rangerHdl::rangerHdl()
{
}


rangerHdl::~rangerHdl()
{
}


bool rangerHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_ranging_from_h5( ConfigRanger::InputfileIonTypes, ConfigRanger::IonTypesGroupName ) == true ) {

		if ( ConfigRanger::RangingMethod == APPLY_EXISTENT_RANGING ) {
			if ( read_mq_from_h5( ConfigRanger::InputfileDataset,
					ConfigRanger::MassToChargeDatasetName ) == true ) {

				cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";
				double toc = MPI_Wtime();
				memsnapshot mm = memsnapshot();
				ranger_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
				return true;
			}
		}
	}

	return false;
}


/*
void rangerHdl::identify_charge_states()
{
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		//extract ivec and charge if none

		//ranges are non-overlapping so only one solution if any
		for( auto jt = it->second.ranges.begin(); jt != it->second.ranges.end(); jt++ ) {
			//find all possible specifically charged molecular ions which match with this mass-to-charge
			molecular_ion_builder crawler;

			crawler.init( periodicTable::MyIsotopes );

			cout << "molecular ion search starting search for task ( " << jt->lo << ", " << jt->hi << " )..." << "\n";

			crawler.enter_recursive_build_of_molecular_ion( it->second, *jt );

			cout << "crawler found " << crawler.candidates.size() << " candidates.size() " << "\n";
			////##MK::BEGIN DEBUG, be careful for large molecular ions this will become unfeasible!
			//for( auto kt = crawler.candidates.begin(); kt != crawler.candidates.end(); kt++ ) {
			//	cout << *kt;
			//}
			cout << "\n";

			//##MK::END DEBUG

			//filter out candidates with a very low abundance

			cout << "Attempt to reduce relevant_candidates to a single candidate" << "\n";
			vector<apt_molecular_ion> relevant_candidates;
			for( auto kt = crawler.candidates.begin(); kt != crawler.candidates.end(); kt++ ) {
				if ( kt->nat_abundance_prod >= (double) EPSILON ) {
					if ( kt->is_member_of( relevant_candidates ) == false ) {
						relevant_candidates.push_back( *kt );
					}
				}
			}
			cout << "relevant_candidates.size() " << relevant_candidates.size() << "\n";
			//only when relevant_candidates.size() == 1 or each member in relevant_candidates has the same charge
			//then the best candidate is the ion it with charge relevant_changes.at(0).charge
			cout << "relevant_candidates are" << "\n";
			for( auto kt = relevant_candidates.begin(); kt != relevant_candidates.end(); kt++ ) {
				cout << *kt;
			}
			cout << "\n";

			int charge = 0;
			ion choice = it->second;
			choice.id = 0xFF;
			choice.charge_sgn = MYPOSITIVE; //because of the physical principles how the method works
			choice.charge_state = (unsigned char) 0;
			choice.ranges = vector<mqival>();
			if ( relevant_candidates.size() == 0 ) {
				charge = 0;
				choice.charge_state = (unsigned char) charge;
				cout << "No relevant candidate identifiable, thus +" << charge << " taken !" << "\n";
				bool exists_already = false;
				for( size_t ii = 0; ii < charged_itypes.size(); ii++ ) {
					if (choice.is_the_same_as( charged_itypes[ii] ) == true ) {
						charged_itypes[ii].ranges.push_back( *jt );
						exists_already = true; break;
					}
				}
				if ( exists_already == false ) {
					choice.ranges.push_back( *jt );
					charged_itypes.push_back( choice ); //##MK::will this invoke the copy-constructor?
				}
			}
			else if ( relevant_candidates.size() == 1 ) {
				charge = relevant_candidates.at(0).charge;
				choice.charge_state = (unsigned char) charge;
				cout << "Unique candidate with +" << charge << " identified" << "\n";
				//also pass over the ion or the additional range that is also attributed to this ion to charged_itypes
				bool exists_already = false;
				for( size_t ii = 0; ii < charged_itypes.size(); ii++ ) {
					if (choice.is_the_same_as( charged_itypes[ii] ) == true ) {
						charged_itypes[ii].ranges.push_back( *jt );
						exists_already = true; break;
					}
				}
				if ( exists_already == false ) {
					choice.ranges.push_back( *jt );
					charged_itypes.push_back( choice ); //##MK::will this invoke the copy-constructor?
				}
			}
			else { //>1
				charge = relevant_candidates.at(0).charge;
				choice.charge_state = (unsigned char) charge;
				bool all_the_same_charge = true;
				for( auto kt = relevant_candidates.begin(); kt != relevant_candidates.end(); kt++ ) {
					if ( kt->charge == charge ) {
						continue;
					}
					else {
						all_the_same_charge = false;
						break;
					}
				}
				if ( all_the_same_charge == true ) {
					cout << "Multiple candidates all with +" << charge << " identified" << "\n";
					choice.charge_state = (unsigned char) charge;
				}
				else {
					charge = 0;
					choice.charge_state = (unsigned char) charge;
					cout << "No unique candidate identifiable, thus +" << charge << " taken !" << "\n";
				}
				bool exists_already = false;
				for( size_t ii = 0; ii < charged_itypes.size(); ii++ ) {
					if (choice.is_the_same_as( charged_itypes[ii] ) == true ) {
						charged_itypes[ii].ranges.push_back( *jt );
						exists_already = true; break;
					}
				}
				if ( exists_already == false ) {
					choice.ranges.push_back( *jt );
					charged_itypes.push_back( choice ); //##MK::will this invoke the copy-constructor?
				}
			}
		} //next range same iontype
	}

	rng.charged_iontypes = map<unsigned char, ion>();
	if ( charged_itypes.size() < 256 ) {
		for( size_t ii = 0; ii < charged_itypes.size(); ii++ ) {
			unsigned char chrg_ityp_id = ( ii < 256 ) ? (unsigned char) ii : 0xFF;
			charged_itypes[ii].id = chrg_ityp_id;
			rng.charged_iontypes[chrg_ityp_id] = charged_itypes[ii]; //##MK::will this invoke the copy-constructor?
		}
	}
	else {
		cerr << "Identifying charged iontypes revealed that more than 256 different would be necessary. This is currently not supported !" << "\n";
	}
	//##MK::BEGIN DEBUG
	cout << "Reporting final assignment of new iontypes:" << "\n";
	for( auto it = rng.charged_iontypes.begin(); it != rng.charged_iontypes.end(); it++ ) {
		cout << (int) it->first << "\n";
		cout << it->second;
	}
	//##MK::END DEBUG
}
*/


void rangerHdl::apply_existent_ranging()
{
    double tic = MPI_Wtime();
    
    cout << "Rank " << get_myrank() << " applying existent ranging ..." << "\n";

    //assign every ion first the default type
    try {
        //itype = vector<evapion3>( xyz.size(), evapion3() );
        itypes = vector<unsigned char>( ions.ionmq.size(), UNKNOWNTYPE );
    }
    catch( bad_alloc &croak) {
        cerr << "Allocation failed for itypes!" << "\n"; return;
    }
    
    //assign physical meaningful iontypes based on models, ##MK::could be OpenMP parallelized in the future
	#pragma omp parallel
	{
		int nt = omp_get_num_threads();
		int mt = omp_get_thread_num();
		if ( mt == MASTER ) {
			cout << "Multi-threaded ranging using " << nt << " OpenMP threads" << "\n";
		}
		//multi-threading identifying into which range the ions fall
		//##need to check whether this is really useful
		//map<size_t, unsigned char> myres;
		vector<pair<apt_uint, unsigned char>> myres;
		myres.reserve( ions.ionmq.size() / (size_t) nt + 1);
		#pragma omp for
		for( size_t i = 0; i < ions.ionmq.size(); i++ ) {
			apt_real mq = ions.ionmq[i];
			//##MK::currently implementation is a brute-force approach which scales O(Nions*Mranges)
			//##MK::here is optimization potential that becomes more relevant the more ranges are distinguished
			//##MK::e.g. we could build a one-dimensional kd-tree of ranges and then query this tree would result
			//##MK::this would result in O(Nions*Mranges)
			//##MK::further optimization could to avoid writing solution == UNKNOWNTYPE back to itypes
			unsigned int solution = UNKNOWNTYPE;
			for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
				//ranges are non-overlapping so only one solution if any
				for( auto jt = it->second.ranges.begin(); jt != it->second.ranges.end(); jt++ ) {
					if ( jt->inrange( mq ) == false ) { //most likely case is not in the range
						continue;
					}
					else {
						solution = it->first;
						break; //it suffices that the mq value is within one of the ranges of an ion
					}
				} //next range same iontype
				//##MK::more robust ion should match only one type
			} //next possible candidate
			myres.push_back( pair<apt_uint, unsigned char>( (apt_uint) i, solution) );
		}
		#pragma omp barrier
		#pragma omp critical
		{
			for( auto it = myres.begin(); it != myres.end(); it++ ) {
				itypes[it->first] = it->second;
			}
		}
	}

	cout << "Rank " << get_myrank() << " completed local workpackage" << "\n";
    
    double toc = MPI_Wtime();
    memsnapshot mm = memsnapshot();
    ranger_tictoc.prof_elpsdtime_and_mem( "ApplyingExistentRanging", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void rangerHdl::compute_composition()
{
    double tic = MPI_Wtime();

    cout << "Rank " << get_myrank() << " computing naive composition ..." << "\n";

    map<unsigned char, apt_uint> composition;
    for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
    	composition[it->first] = 0;
    }

    #pragma omp parallel
    {
    	int nt = omp_get_num_threads();
    	int mt = omp_get_thread_num();
    	if ( mt == MASTER ) {
    		cout << "Multi-threaded computing of a naive composition using " << nt << " OpenMP threads" << "\n";
    	}
    	//multi-threading identifying into which range the ions fall
    	map<unsigned char, apt_uint> myres;
        for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
        	myres[it->first] = 0;
        }
   		#pragma omp for
    	for( size_t i = 0; i < itypes.size(); i++ ) {
    		myres[itypes[i]]++;
    	}
    	#pragma omp barrier
		#pragma omp critical
    	{
    		for( auto it = myres.begin(); it != myres.end(); it++ ) {
    			composition[it->first] += it->second;
    		}
    	}
    }
    cout << "Rank " << get_myrank() << " completed local workpackage" << "\n";

    cout << "Reporting naive composition" << "\n";
    apt_uint sum = 0;
    for( auto it = composition.begin(); it != composition.end(); it++ ) {
    	cout << (int) it->first << "\t\t" << it->second << "\n";
    	sum += it->second;
    }
    cout << "Ion total computed " << sum << "\n";
    cout << "Ion total expected " << itypes.size() << "\n";

    double toc = MPI_Wtime();
    memsnapshot mm = memsnapshot();
    ranger_tictoc.prof_elpsdtime_and_mem( "ComputingNaiveComposition", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void rangerHdl::search_molecularions()
{
	double tic = 0.0;
	double toc = 0.0;

	for ( auto tskit = ConfigRanger::IonSearchTasks.begin(); tskit != ConfigRanger::IonSearchTasks.end(); tskit++ ) {
		tic = MPI_Wtime();

		molecular_ion_builder crawler;

		crawler.init( tskit->isotope_set );

		cout << "molecular ion search starting search for task " << tskit->taskid << "..." << "\n";

		crawler.enter_recursive_search_for_molecular_ions( tskit->mq_region, tskit->max_nuclids, tskit->max_charge );

		//##MK::BEGIN DEBUG
		cout << "crawler found " << crawler.candidates.size() << " candidates.size() " << "\n";
		/*
		for( auto kt = crawler.candidates.begin(); kt != crawler.candidates.end(); kt++ ) {
			cout << *kt; // << "\n";
		}
		cout << "\n";
		//##MK::END DEBUG
		*/

		toc = MPI_Wtime();
		cout << "molecular_ion_search_task " << tskit->taskid << " ion search task took " << (toc-tic) << " seconds wall-clock time" << "\n";

		tic = MPI_Wtime();
		if ( crawler.candidates.size() > 0 ) {

			if ( crawler.write_ion_search_results_h5( *tskit, ConfigShared::OutputfileName ) == true ) {

				cout << "Rank " << get_myrank() << " writing results to file for ion search task " << tskit->taskid << " was successful" << "\n";
			}
			else {
				cerr << "Rank " << get_myrank() << " writing results to file for ion search task " << tskit->taskid << " failed !" << "\n";
			}
		}
		toc = MPI_Wtime();
		cout << "molecular_ion_search_task " << tskit->taskid << " ion search task reporting to H5 took " << (toc-tic) << " seconds wall-clock time" << "\n";
	}
}


bool rangerHdl::write_apply_results_h5()
{
	double tic = MPI_Wtime();

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	apt_uint proc_id = 1;

	grpnm = "/entry" + to_string(entry_id) + "/iontypes";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/maximum_number_of_atoms_per_molecular_ion";
	apt_uint n = MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n, anno ) != MYHDF5_SUCCESS ) { return false; }

//ions without charge state information
	for( auto it = rng.iontypes.begin(); it != rng.iontypes.end(); it++ ) {
		string subgrpnm = grpnm + "/ion" + to_string((int) it->first);
		anno = ioAttributes();
		anno.add( "NX_class", string("NXion") );
		if ( h5w.nexus_write_group( subgrpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = subgrpnm + "/charge_state";
		short i16 = (short) it->second.get_charge();
		anno = ioAttributes();
		if ( h5w.nexus_write( dsnm, i16, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = subgrpnm + "/mass_to_charge_range";
		vector<apt_real> real;
		for( auto jt = it->second.ranges.begin(); jt != it->second.ranges.end(); jt++ ) {
			real.push_back( jt->lo );
			real.push_back( jt->hi );
		}
		anno = ioAttributes();
		anno.add( "unit", string("Da") );
		if ( h5w.nexus_write(
			dsnm,
			io_info({real.size()/2, 2}),
			real,
			anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = subgrpnm + "/nuclide_hash";
		vector<unsigned short> u16_ivec = vector<unsigned short>( 1 * MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
		vector<unsigned short> u16_nlst = vector<unsigned short>( 2 * MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
		for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
			u16_ivec[i] = it->second.ivec[i];
			pair<unsigned char, unsigned char> ZN = isotope_unhash( it->second.ivec[i] );
			if ( ZN.second != 0 ) {
				u16_nlst[(2*i)+0] = (unsigned short) ZN.first + (unsigned short) ZN.second;
				// mass number or zero if no isotope information
			}
			u16_nlst[(2*i)+1] = (unsigned short) ZN.first;  // proton number
		}
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u16_ivec.size()}, {u16_ivec.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u16_ivec,
			anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = subgrpnm + "/nuclide_list";
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u16_nlst.size()/2, 2},
					{u16_nlst.size()/2, 2},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u16_nlst,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	//actual ion type labels
	dsnm = grpnm + "/iontypes";
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({itypes.size()}, {itypes.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			itypes,
			anno ) != MYHDF5_SUCCESS ) { return false; }

	xdmfBaseHdl paraview_support;
	paraview_support.add_grid_uniform( "IonLabels" );
	paraview_support.add_topology_mixed( itypes.size(), {itypes.size()*(1+1+1)}, H5_U32,
			strip_path_prefix(ConfigRanger::InputfileDataset) + ":" + "/entry" + to_string(entry_id) + "/atom_probe/reconstruction/visualization/xdmf_topology" );
	paraview_support.add_geometry_xyz( {itypes.size(), 3}, H5_F32,
			strip_path_prefix(ConfigRanger::InputfileDataset) + ":" + "/entry" + to_string(entry_id) + "/atom_probe/reconstruction/reconstructed_positions" );
	paraview_support.add_attribute_scalar( "MassToChargeRatio", "Node", {itypes.size()}, H5_F32,
			strip_path_prefix(ConfigRanger::InputfileDataset) + ":" + "/entry" + to_string(entry_id) + "/atom_probe/mass_to_charge_conversion/mass_to_charge" );
	paraview_support.add_attribute_scalar( "IonType", "Node", {itypes.size()}, H5_U8,
			strip_path_prefix(ConfigShared::OutputfileName) + ":" + "/entry" + to_string(entry_id) + "/iontypes/iontypes" );

	const string xmlfn = ConfigShared::OutputfileName + ".EntryId." + to_string(entry_id) + ".TaskId." + to_string(proc_id) + ".VisIonTypesAndRecon.xdmf";
	paraview_support.write( xmlfn );

	//window
	grpnm = "/entry" + to_string(entry_id) + "/iontypes/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_boolean_filter_mask") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) ions.ionmq.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/bitdepth";
	apt_uint bit_depth = sizeof(unsigned char) * 8;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, bit_depth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/mask";
	vector<unsigned char> u08_wdw = vector<unsigned char>( ions.ionmq.size(), ANALYZE_YES );
	//##MK::currently all ions are ranged otherwise check state of ions.ionifo[i].mask1 == ANALYZE_YES )
	vector<unsigned char> u08_bitfield;
	BitPacker pack_bits;
	pack_bits.uint8_to_bitpacked_uint8( u08_wdw, u08_bitfield );
	u08_wdw = vector<unsigned char>();
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) { return false; }

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	ranger_tictoc.prof_elpsdtime_and_mem( "WriteResults", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}

// inspect a8b4fca0f8fc85bbf4b126ac17753ebeb5efa117 for rangerHdl::write_check_results_h5()
