/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_RANGER_HDL_H__
#define __PARAPROBE_RANGER_HDL_H__

#include "PARAPROBE_RangerIonBuilder.h"


class rangerHdl : public mpiHdl
{
	//process-level class which implements the worker instance which executes ranging of ions using OpenMP
	//specimens result are written to a specifically-formatted HDF5 file

public:
	rangerHdl();
	~rangerHdl();

	bool read_relevant_input();

	//void identify_charge_states();
	void apply_existent_ranging();
	void compute_composition();
	void search_molecularions();

	bool write_apply_results_h5();
	//bool write_check_results_h5();

	vector<unsigned char> itypes;
	vector<unsigned char> charged_itypes;

	profiler ranger_tictoc;
};


#endif

