/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


#include "PARAPROBE_RangerIonBuilder.h"


apt_molecular_ion::apt_molecular_ion()
{
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		this->ivec[i] = 0;
	}
	this->isotope_mass_sum = MYZERO;
	this->nat_abundance_prod = MYZERO;
	this->charge = 0;
}


apt_molecular_ion::apt_molecular_ion( apt_molecular_ion const & in )
{
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		this->ivec[i] = in.ivec[i];
	}
	this->isotope_mass_sum = in.isotope_mass_sum;
	this->nat_abundance_prod = in.nat_abundance_prod;
	this->charge = in.charge;
}


bool apt_molecular_ion::is_equal( apt_molecular_ion const & test )
{
	//in most cases molecular ions differ
	vector<unsigned short> this_ivec_sorted;
	vector<unsigned short> test_ivec_sorted;
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		this_ivec_sorted.push_back( this->ivec[i] );
		test_ivec_sorted.push_back( test.ivec[i] );
	}
	stable_sort( this_ivec_sorted.begin(), this_ivec_sorted.end(), std::greater<>() ); //in descending order
	stable_sort( test_ivec_sorted.begin(), test_ivec_sorted.end(), std::greater<>() );

	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		if ( this_ivec_sorted[i] != test_ivec_sorted[i] ) {
			return false;
		}
		//optionally
		//if ( this->ivec[i] == 0 ) { //in most cases users allow only for a low number of nuclids to build the molecular ion
		//	break;
		//}
	}
	//if not returned yet, the entire nuclid building code (the isotope_vector/ivec) is the same
	//so checking for the total charge equality remains
	if ( this->charge != test.charge ) {
		return false;
	}

	//##MK::isotope_mass_sum and nat_abundance_prod are not checked for
	return true;
}


bool apt_molecular_ion::is_member_of( vector<apt_molecular_ion> const & test )
{
	for( auto it = test.begin(); it != test.end(); it++ ) {
		if ( this->is_equal( *it ) == false ) {
			continue;
		}
		else {
			if ( this->charge == it->charge ) {
				return true;
			}
		}
	}
	return false;
}


molecular_ion_builder::molecular_ion_builder()
{
}


molecular_ion_builder::~molecular_ion_builder()
{
}


ostream& operator<<(ostream& in, apt_molecular_ion const & val)
{
	in << "mass/abun/charge/ivec " << val.isotope_mass_sum << "\t\t" << val.nat_abundance_prod << "\t\t" << val.charge << "\t\t";
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		if ( val.ivec[i] != 0 ) {
			in << val.ivec[i];
			pair<unsigned char, unsigned char> ZN = isotope_unhash( val.ivec[i] );
			in << " ( " << (int) ZN.first << ", " << (int) ZN.second << " )";
			if ( i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION-1 ) {
				if ( val.ivec[i+1] != 0 ) {
					in << ", ";
				}
			}
		}
		else {
			break;
		}
	}
	in << "\n";
	return in;
}


bool molecular_ion_builder::init( vector<isotope> const & isotope_whitelist )
{
	nuclids = vector<apt_nuclid>();
	nuclid_mass = map<apt_nuclid, double>();
	nuclid_abun = map<apt_nuclid, double>();
	cout << "isotope_whitelist.size() " << isotope_whitelist.size() << "\n";

	for ( auto it = isotope_whitelist.begin(); it != isotope_whitelist.end(); it++ ) {

		apt_nuclid hashvalue = isotope_hash( it->nprotons, it->nneutrons );

		nuclids.push_back(hashvalue);

		nuclid_mass[hashvalue] = (double) it->mass;
		nuclid_abun[hashvalue] = (double) it->abundance;
	}

	//nuclids need to be sorted strictly in descending order
	sort(nuclids.rbegin(), nuclids.rend());

	//the hashvalue 0 must neither be in the nuclid_mass nor in the nuclid_abun
	map<apt_nuclid, double>::iterator this_mass = nuclid_mass.find( 0 );
	if ( this_mass != nuclid_mass.end() ) {
		cerr << "The nuclid_mass look-up table is configured incorrectly !" << "\n";
		return false;
	}

	map<apt_nuclid, double>::iterator this_abun = nuclid_abun.find( 0 );
	if ( this_abun != nuclid_abun.end() ) {
		cerr << "The nuclid_abun look-up table is configured incorrectly !" << "\n";
		return false;
	}

	//consistence check
	for( auto jt = nuclids.begin(); jt != nuclids.end(); jt++ ) {
		//cout << "Checking look-up table for apt_nuclid " << *jt << " ... ";
		map<apt_nuclid, double>::iterator this_mass = nuclid_mass.find( *jt );
		if ( this_mass == nuclid_mass.end() ) {
			cerr << "nuclid_mass[" << *jt << "] is not available !" << "\n";
			return false;
		}
		map<apt_nuclid, double>::iterator this_abun = nuclid_abun.find( *jt );
		if ( this_abun == nuclid_abun.end() ) {
			cerr << "nuclid_abun[" << *jt << "] is not available !" << "\n";
			return false;
		}
		//cout << "OK" << "\n";
	}

	cout << "molecular_ion_builder initialized with nuclids.size() " << nuclids.size() << "\n";
	return true;
}


double molecular_ion_builder::get_isotope_mass_sum( apt_molecular_ion const & test )
{
	//evaluate the mass, no relatistic effects taken into account
	double m = MYZERO;
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; ++i ) {
		apt_nuclid hashvalue = test.ivec[i];
		if ( hashvalue != 0 ) {
			map<apt_nuclid, double>::iterator thisone = nuclid_mass.find(hashvalue);
			if ( thisone != nuclid_mass.end() ) {
				m += thisone->second;
				continue;
			}
			cerr << "molecular_ion_builder::get_mass unable to get mass for hash " << hashvalue << "\n";
			return MYZERO;
		}
		else { //because ivec are systematically filled up from left to right !
			break;
		}
	}
	return m;
}


double molecular_ion_builder::get_natural_abundance_product( apt_molecular_ion const & test )
{
	//evaluate the abundance score
	double a = MYONE;
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; ++i ) {
		apt_nuclid hashvalue = test.ivec[i];
		if ( hashvalue != 0 ) {
			map<apt_nuclid, double>::iterator thisone = nuclid_abun.find(hashvalue);
			if ( thisone != nuclid_abun.end() ) {
				a *= thisone->second;
				continue;
			}
			cerr << "molecular_ion_builder::get_abundance unable to get abundance for hash " << hashvalue << "\n";
			return MYZERO;
		}
		else {
			break;
		}
	}
	return a;
}


void molecular_ion_builder::enter_recursive_build_of_molecular_ion(
		ion & molecular_ion, mqival const & interval )
{
	int max_n = 0; //##MK::number of non-zero entries in molecular_ion.ivec
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		if ( molecular_ion.ivec[i] != 0 ) {
			max_n++;
		}
		else {
			break;
		}
	}
	//cout << "enter_recursive max_n " << max_n << "\n";
	cout << molecular_ion << "\n";

	if ( max_n > 0 ) {
		apt_molecular_ion new_in = apt_molecular_ion();
		int j = 0;
		vector<apt_nuclid> jth_nuclids = get_nuclids( molecular_ion.ivec[j] );
		//cout << "j/nuclids.size() " << j << ", " << jth_nuclids.size() << "\n";

		iterate_molecular_ion(
				molecular_ion,
				jth_nuclids,
				new_in,
				j,
				max_n,
				interval );
	}
}


void molecular_ion_builder::iterate_molecular_ion(
		ion & molecular_ion,
		vector<apt_nuclid> & nuclids,
		apt_molecular_ion & in,
		const int i,
		const int max_n,
		mqival const & iv )
{
	if ( i < (max_n - 1) ) {
		for( vector<apt_nuclid>::iterator it = nuclids.begin(); it != nuclids.end(); it++ ) {

			apt_molecular_ion new_in = apt_molecular_ion( in );
			new_in.ivec[i] = *it;

			vector<apt_nuclid> jxxth_nuclids = get_nuclids( molecular_ion.ivec[i+1] );
			//cout << "jxx/nuclids.size() " << i+1 << ", " << jxxth_nuclids.size() << "\n";

			iterate_molecular_ion( molecular_ion, jxxth_nuclids, new_in, i+1, max_n, iv );
		}
	}
	else if ( i == (max_n - 1) ) {
		for( vector<apt_nuclid>::iterator it = nuclids.begin(); it != nuclids.end(); it++ ) {

			apt_molecular_ion new_in = apt_molecular_ion( in );
			new_in.ivec[i] = *it;
			//##MK::by this design the ivec does not necessarily remain ordered

			double new_mass = get_isotope_mass_sum( new_in );
			double new_abun = get_natural_abundance_product( new_in );

			//cout << "nuclid_iterator " << *it << " iterate_molecular_ion";
			//for( int k = 0; k < i; k++ ) { cout << " " << new_in.ivec[k]; }
			//cout << "\n";

			for( int chrg = 1; chrg <= 7; chrg++ ) {
				double mass_to_charge = new_mass / (double) chrg;
				if ( mass_to_charge < iv.lo ) {
					break;
					//MK::one can break the entire charge state generation here already instead of continue
					//because already the current mq is left out of interval [mqmin, mqmax]
					//and all mq in the next iterations will result in even lower mass-to-charge you walk to the left
					//increasing your distance to the left bound
				}
				if ( mass_to_charge > iv.hi ) { //##MK::can be optimized and broken out of earlier if testing first chrg == 1 and then chrg == APTMOLECULARION_MAX_CHANGE
					continue;
					//MK::must not be break! because with adding more charge you might walk from right to left so possibly into your interval!
				}
				//molecular ion is within user-specified bounds
				new_in.charge = chrg;
				new_in.isotope_mass_sum = new_mass;
				new_in.nat_abundance_prod = new_abun;

				candidates.push_back( new_in );
			}

			//do not start a new recursion
		}
	}
	else { //break the recursion
		return;
	}
}


void molecular_ion_builder::enter_recursive_search_for_molecular_ions(
		mqival const & interval, const int max_n, const int max_chrg )
{
	apt_molecular_ion tmp = apt_molecular_ion();

	build_molecular_ion( nuclids.begin(), tmp, 0, max_n, max_chrg, interval );
}


void molecular_ion_builder::build_molecular_ion( vector<apt_nuclid>::iterator itstart,
		apt_molecular_ion & in, const int i, const int max_n, const int max_chrg, mqival const & iv )
{
	if ( i < max_n ) {
		for( vector<apt_nuclid>::iterator it = itstart; it != nuclids.end(); it++ ) {
			//recursively add ions with the same or lower ID

			apt_molecular_ion new_in = apt_molecular_ion( in );
			new_in.ivec[i] = *it;

			double new_mass = get_isotope_mass_sum( new_in );
			double new_abun = get_natural_abundance_product( new_in );

			for( int chrg = 1; chrg <= max_chrg; chrg++ ) {
				double mass_to_charge = new_mass / (double) chrg;
				if ( mass_to_charge < iv.lo ) {
					break;
					//MK::one can break the entire charge state generation here already instead of continue
					//because already the current mq is left out of interval [mqmin, mqmax]
					//and all mq in the next iterations will result in even lower mass-to-charge you walk to the left
					//increasing your distance to the left bound
				}
				if ( mass_to_charge > iv.hi ) { //##MK::can be optimized and broken out of earlier if testing first chrg == 1 and then chrg == APTMOLECULARION_MAX_CHANGE
					continue;
					//MK::must not be break! because with adding more charge you might walk from right to left so possibly into your interval!
				}
				//molecular ion is within user-specified bounds
				new_in.charge = chrg;
				new_in.isotope_mass_sum = new_mass;
				new_in.nat_abundance_prod = new_abun;

				candidates.push_back( new_in );
			}
			//recursively add the next ion
			build_molecular_ion( it, new_in, i+1, max_n, max_chrg, iv );
		}
	}
	else { //break the recursion
		return;
	}
}


bool molecular_ion_builder::write_ion_search_results_h5( molecular_ion_search_task const & ifo, const string h5fn )
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	string grpnm = "";
	string dsnm = "";
	ioAttributes anno = ioAttributes();

	apt_uint entry_id = 1;
	apt_uint proc_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/process" + to_string(proc_id) + "/molecular_ion_search";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXprocess") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/isotope_vector_matrix";
	anno = ioAttributes();
	vector<unsigned short> u16 = vector<unsigned short>(
			candidates.size() * MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 0 );
	size_t offset = 0;
	for( auto it = candidates.begin(); it != candidates.end(); it++, offset += MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION ) {
		for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
			if ( it->ivec[i] != 0 ) {
				u16[offset+i] = it->ivec[i];
			}
			else { //because an ivec is strictly arranged in descending order
				break;
			}
		}
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({u16.size()/MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
				{u16.size()/MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		u16,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u16 = vector<unsigned short>();

	vector<double> real;
	dsnm = grpnm + "/mass_to_charge_state_ratio";
	anno = ioAttributes();
	anno.add( "unit", string("Da") );
	for( auto it = candidates.begin(); it != candidates.end(); it++ ) {
		real.push_back( it->isotope_mass_sum / (double) it->charge );
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<double>();

	dsnm = grpnm + "/mass";
	anno = ioAttributes();
	anno.add( "unit", string("amu") );
	for( auto it = candidates.begin(); it != candidates.end(); it++ ) {
		real.push_back( it->isotope_mass_sum );
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<double>();

	vector<unsigned char> u08;
	dsnm = grpnm + "/charge_state";
	anno = ioAttributes();
	//anno.add( "unit", string("eV") );
	for( auto it = candidates.begin(); it != candidates.end(); it++ ) {
		if ( it->charge >= 0 && it->charge < 256 ) {
			u08.push_back( (unsigned char) it->charge );
		}
		else {
			cerr << "Unexpectedly found a charge which is out of the range of uint8 !" << "\n";
			return false;
		}
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	dsnm = grpnm + "/natural_abundance_product";
	anno = ioAttributes();
	//anno.add( "unit", string("NX_DIMENSIONLESS") );
	for( auto it = candidates.begin(); it != candidates.end(); it++ ) {
		real.push_back( it->nat_abundance_prod );
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({real.size()}, {real.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		real,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<double>();

	//##MK::no composition product

	dsnm = grpnm + "/number_of_disjoint_nuclids";
	anno = ioAttributes();
	for( auto it = candidates.begin(); it != candidates.end(); it++ ) {
		unordered_set<unsigned short> unique;
		for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
			if ( it->ivec[i] != 0 ) {
				unique.insert(it->ivec[i]);
			}
			else { //because an ivec is strictly arranged in descending order
				if ( unique.size() >= 0 && unique.size() < 256 ) {
					u08.push_back( (unsigned char) unique.size() );
				}
				else {
					cerr << "Unexpectedly found a charge which is out of the range of uint8 !" << "\n";
					return false;
				}
				break;
			}
		}
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	dsnm = grpnm + "/number_of_nuclids";
	anno = ioAttributes();
	for( auto it = candidates.begin(); it != candidates.end(); it++ ) { //##MK::fuse with the loop before
		int cnt = 0;
		for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
			if ( it->ivec[i] != 0 ) {
				cnt++;
			}
			else { //because an ivec is strictly arranged in descending order
				if ( cnt >= 0 && cnt < 256 ) {
					u08.push_back( (unsigned char) cnt );
				}
				else {
					cerr << "Unexpectedly found a number of nuclids which is out of the range of uint8 !" << "\n";
					return false;
				}
				break;
			}
		}
	}
	if ( h5w.nexus_write(
		dsnm,
		io_info({u08.size()}, {u08.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
		u08,
		anno ) != MYHDF5_SUCCESS ) { return false; }
	u08 = vector<unsigned char>();

	return true;
}
