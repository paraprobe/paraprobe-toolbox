/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_RANGER_IONBUILDER_H__
#define __PARAPROBE_RANGER_IONBUILDER_H__

#include "PARAPROBE_RangerXDMF.h"


struct apt_molecular_ion
{
	unsigned short ivec[MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION];
	double isotope_mass_sum; 					//sum of the masses of the individual
	double nat_abundance_prod; 					//product of natural abundance of nuclids
	int charge;

	apt_molecular_ion();
	apt_molecular_ion( apt_molecular_ion const & in );

	bool is_equal( apt_molecular_ion const & test );
	bool is_member_of( vector<apt_molecular_ion> const & test );
};

ostream& operator<<(ostream& in, apt_molecular_ion const & val);


typedef unsigned short apt_nuclid;


class molecular_ion_builder
{
public:
	molecular_ion_builder();
	~molecular_ion_builder();

	double get_isotope_mass_sum( apt_molecular_ion const & test );
	double get_natural_abundance_product( apt_molecular_ion const & test );

	bool init( vector<isotope> const & isotope_whitelist );
	//##MK
	void enter_recursive_build_of_molecular_ion( ion & molecular_ion, mqival const & interval );
	void iterate_molecular_ion( ion & molecular_ion, vector<apt_nuclid> & nuclids,
			apt_molecular_ion & in, const int i, const int max_n, mqival const & iv );
	//##MK
	void enter_recursive_search_for_molecular_ions( mqival const & interval, const int max_n, const int max_chrg );
	void build_molecular_ion( vector<apt_nuclid>::iterator itstart,
			apt_molecular_ion & in, const int i, const int max_n, const int max_chrg, mqival const & iv );

	bool write_ion_search_results_h5( molecular_ion_search_task const & ifo, const string h5fn );

	vector<apt_molecular_ion> candidates;

private:
	vector<apt_nuclid> nuclids; //MK::this list MUST be sorted in descending order!
	map<apt_nuclid, double> nuclid_mass;
	map<apt_nuclid, double> nuclid_abun;
};


#endif
