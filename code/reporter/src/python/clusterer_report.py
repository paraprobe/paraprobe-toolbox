#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Post-processing of results from paraprobe-clusterer for frequently asked analyses."""


class ReporterClusterer():
    """Support the user with using results from paraprobe-clusterer runs."""
    def __init__(self, results_file, entry_id=1):
        self.tool_name = "Clusterer"
        # self.tool_hash = "0"
        self.results_file = results_file
        self.entry_id = entry_id
        self.results = {}

    # def get_number_of_cluster_over_epsilon(self, task_id=1):
    #     deactivated, find src in commit 02dec700e89f6cc4990fdd2378137500ae952f6d
