#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Post-processing of results from paraprobe-distancer for frequently asked analyses."""

import numpy as np
import h5py
import reporter.src.python.plotting.default.line_plot_property_set_for_fixed_value as mycdf


class ReporterDistancer():
    """Support the user with using results from paraprobe-distancer runs."""

    def __init__(self, results_file, entry_id=1):
        self.tool_name = "Distancer"
        self.tool_hash = "0"
        self.results_file = results_file
        self.entry_id = entry_id
        self.results = {}

    def get_summary(self, quantiles=[0.01, 0.99], threshold=1.):
        """Create default cumulated distribution of distances."""
        with h5py.File(self.results_file, "r") as h5r:
            grpnm = f"/entry{self.entry_id}/point_to_triangle"
            assert grpnm in h5r, f"Group {grpnm} does not exist in results file !"
            dsnm = f"{grpnm}/distance"
            assert dsnm in h5r, f"Dataset {dsnm} does not exist in results file !"

            d = np.asarray(h5r[dsnm][:], np.float32)  # d and threshold in nm
            fraction = 100. * np.count_nonzero(np.where(d <= threshold)) / np.shape(d)[0]

            print(f"Your chosen threshold distance to the edge of the dataset is "
                  f"{np.around(threshold, decimals=2)} nm")
            print(f"Fraction of ions (compared to total) which lay closer than "
                  f"this threshold distance {np.around(fraction, decimals=2)} %")
            print(f"Minimum distance is {np.min(d)} nm")
            # often ions on the surface of e.g. a convex hull
            print(f"Average distance is {np.mean(d)} nm")
            print(f"Median distance is {np.median(d)} nm")
            print(f"Maximum distance is {np.max(d)} nm")
            for quantile in quantiles:
                print(f"Specific distance at the {np.around(quantile*100., decimals=3)} % percentile is "
                      f"{np.around(np.quantile(d, quantile), decimals=3)} nm")
            # print("Maybe we would like ask turn the question on quantiles around and find the quantile value?")

    def get_ion2mesh_distance_cdf(self, task_id=1, quantile_based=True):
        """Compute cumulated distribution of distance of ions to primitives."""
        with h5py.File(self.results_file, "r") as h5r:
            grpnm = f"/entry{self.entry_id}/point_to_triangle"
            assert grpnm in h5r, f"Group {grpnm} does not exist in results file !"
            dsnm = f"{grpnm}/distance"
            assert dsnm in h5r, f"Dataset {dsnm} does not exist in results file !"

            d = np.sort(np.asarray(h5r[dsnm][:], np.float32), kind="mergesort")
            n = np.shape(d)[0]
            cdf = np.asarray(np.linspace(1. / n, 1., n, endpoint=True), np.float32)
            print(np.shape(d))
            print(np.shape(cdf))
            if quantile_based is True:
                quantiles = np.asarray([0.0001, 0.001])
                quantiles = np.append(quantiles, np.linspace(0.01, 0.99, num=99, endpoint=True))
                quantiles = np.append(quantiles, [0.999, 0.9999])
                d = np.quantile(d, quantiles, method='linear')
                cdf = quantiles
                lgd = "Euclidean distance ion-to-closest triangle (quantile based)"
            else:
                lgd = "Euclidean distance ion-to-closest triangle"
            print("Cumulated distribution of distances computed")

            xaxis_name = r"Distance (nm)"
            yaxis_name = r"Cumulated fraction"

            img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.TaskId.{task_id}.Ion2MeshCDF"
            return mycdf.line_plot_property_set_for_fixed_value(
                [d],
                [cdf],
                [lgd],
                img_fnm,
                xaxis_name=xaxis_name,
                yaxis_name=yaxis_name,
                xaxis_log=False, yaxis_log=False,
                manual_scaling=False)
