#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Python post-processing of frequently executed analyses for the nanochem tool
#

import re
import numpy as np
import h5py
from ase.data import chemical_symbols, atomic_numbers

from utils.src.python.hdf_five_string_io import read_strings_from_dataset
from utils.src.python.bisection import bisection
# from utils.src.python.filteredmean import filtered_mean
import reporter.src.python.plotting.default.scatter_plot_property_set_over_value_set as mysct
import reporter.src.python.plotting.default.line_plot_property_set_for_fixed_value as mycdf
import reporter.src.python.plotting.default.composition_plots as mycmp
from ifes_apt_tc_data_modeling.utils.utils import isotope_to_hash


def delocalization_formatting(sigma):
    """Return formatted delocalization parameter."""
    return fr"$\sigma = {{{np.around(sigma, 3)}}} nm$"


class DelocalizationTask():
    """Represent metadata of a delocalization task."""

    def __init__(self, results_file, entry_id=1, deloc_task_id=1):
        with h5py.File(results_file, "r") as h5r:
            grpnm = f"/entry{entry_id}/delocalization{deloc_task_id}"
            if grpnm not in h5r:
                raise LookupError(f"Group {grpnm} does not exist in results file!")
            delocalization_task_names = list(h5r[f"{grpnm}/grid"])
            # extract metadata specific and common for all child tasks
            dsnm = f"{grpnm}/grid/normalization"
            if dsnm not in h5r:
                raise LookupError(f"Dataset {dsnm} does not exist in results file!")
            self.normalization = read_strings_from_dataset(h5r[dsnm][()])
            dsnm = f"{grpnm}/grid/kernel_sigma"
            if dsnm not in h5r:
                raise LookupError(f"Dataset {dsnm} does not exist in results file!")
            self.sigma = h5r[dsnm][:][0]
            dsnm = f"{grpnm}/grid/kernel_size"
            if dsnm not in h5r:
                raise LookupError(f"Dataset {dsnm} does not exist in results file!")
            self.halfsize = (h5r[dsnm][:][0] - 1) / 2
            dsnm = f"{grpnm}/grid/cell_dimensions"
            if dsnm not in h5r:
                raise LookupError(f"Dataset {dsnm} does not exist in results file!")
            self.voxelsize = h5r[dsnm][:][0]

            # extract metadata for iso-surface child tasks
            self.isosurface_tasks = {}
            isosurface_task_names = []
            for sub_group_name in delocalization_task_names:
                if "iso_surface" in sub_group_name:  # better use a regex
                    isosurface_task_names.append(sub_group_name)
            for name in isosurface_task_names:
                isrf_task_id = int(name.replace("iso_surface", ""))
                # extract metadata for specific iso-surface tasks
                self.isosurface_tasks[isrf_task_id] \
                    = IsoSurfaceTask(results_file, entry_id, deloc_task_id, isrf_task_id)


class IsoSurfaceTask():
    """Represent metadata of an iso-surface task."""

    def __init__(self, results_file, entry_id=1, deloc_task_id=1, isrf_task_id=1):
        # instead of opening the file on demand again like here rather pass ref
        with h5py.File(results_file, "r") as h5r:
            grpnm = f"/entry{entry_id}/delocalization{deloc_task_id}/grid/iso_surface{isrf_task_id}"
            if grpnm not in h5r:
                raise LookupError(f"Group {grpnm} does not exist in results file!")
            # tell me which groups, sub-groups, and datasets exists in the HDF5 file
            # isosurface_metadata = list(h5r[grpnm])
            self.deloc_task_id = deloc_task_id
            dsnm = f"{grpnm}/isovalue"
            if dsnm not in h5r:
                raise LookupError(f"Dataset {dsnm} does not exist in results file!")
            self.isovalue = h5r[dsnm][()]


class ReporterNanochem():
    """Support the user with using results from paraprobe-nanochem runs."""

    def __init__(self, results_file, entry_id=1):
        self.tool_name = "Nanochem"
        self.tool_hash = "0"
        self.results_file = results_file
        self.entry_id = entry_id
        self.delocalization = None
        self.results = {}
        # MK::auto-identify the relevant configuration file
        # the key trick is here that we do not allow the user to specify
        # the config_file, instead the tool automatically extracts the
        # specific config_file which was used for running this particular analysis
        self.config_file = None
        with h5py.File(self.results_file, "r") as h5r:
            dsnm = f"/entry{self.entry_id}/common/config/path"
            if dsnm not in h5r:
                raise LookupError("Unable to identify the specific config_file from this analysis!")
            self.config_file = read_strings_from_dataset(h5r[dsnm][()])

    def get_delocalization(self, deloc_task_id=1):
        """Read in metadata for a specific delocalization task."""
        self.delocalization = DelocalizationTask(self.results_file, self.entry_id, deloc_task_id)

    def get_composition_analyses(self,
                                 analysis_type="composition_1d_cumulated",
                                 element_whitelist=[], **kwargs):
        """Compute composition profile and interfacial excess."""
        allowed = ["composition_1d_cumulated", "composition_1d_histogram", "interfacial_excess"]
        if analysis_type not in allowed:
            raise ValueError(f"Argument analysis_type needs to be one of {allowed}!")
        target_element_symbols = []
        target_hash_values = []
        for entry in element_whitelist:
            if entry in atomic_numbers:
                target_element_symbols.append(entry)
                target_hash_values.append(isotope_to_hash(atomic_numbers[entry], 0))
            else:
                print(f"Entry {entry} ignored, no chemical element symbol !")

        # check existence of additional arguments for specific analysis types
        if analysis_type == "composition_1d_histogram":
            if "binwidth" not in kwargs:
                raise ValueError("For composition_1d_histogram keyword argument binwidth is required!")
            if kwargs["binwidth"] < 0.1:
                raise ValueError("For composition_1d_histogram bins should at least be 0.1 nm wide!")
        if analysis_type == "interfacial_excess":
            if "linear_interpolation_bound" not in kwargs:
                raise ValueError("For interfacial excess you need to specify "
                                 "linear_interpolation_bound, i.e. fraction of "
                                 "the total number of atoms linearly interpolated")
            if not 0.01 <= kwargs["linear_interpolation_bound"] < 0.50:
                raise ValueError("For interfacial_excess linear_interpolation_bound " \
                                 "should be at least 0.01 of the total cumulated " \
                                 "number of atoms !")

        # read relevant configuration parameter
        with h5py.File(self.config_file, "r") as h5r:
            grpnm = f"/entry{self.entry_id}/oned_profile"
            if grpnm not in h5r:
                raise LookupError(f"Group {grpnm} does not exist in config file!")
            roi_cylinder_h = float(h5r[f"{grpnm}/roi_cylinder_height"][()])
            roi_cylinder_r = float(h5r[f"{grpnm}/roi_cylinder_radius"][()])
            print(f"roi_cylinder_height: {roi_cylinder_h}" \
                  f"roi_cylinder_radius: {roi_cylinder_r}")

        # get a list of group names for all existent rois which have profile data
        with h5py.File(self.results_file, "r") as h5r:
            # ##MK::will create an analysis for each ROI if "roi_ids" not in kwargs.keys()
            grpnm = f"/entry{self.entry_id}/oned_profile/xdmf_cylinder/rois_far_from_edge"
            if grpnm not in h5r:
                raise LookupError(f"Group {grpnm} does not exist in results file!")
            dataset_names = h5r[grpnm]
            roi_cylinder_ids = []
            for grp in dataset_names:
                tmp = re.findall("roi[0-9]+", grp)
                if tmp != []:
                    if len(tmp) == 1:
                        roi_cylinder_ids.append(tmp[0])

            roi_cylinder_ids_report = []
            if "roi_ids" in kwargs:
                if not isinstance(kwargs["roi_ids"], list):
                    raise TypeError("Keyword argument roi_ids needs to be a list of integer IDs!")
                for identifier in kwargs["roi_ids"]:
                    if not isinstance(identifier, int):
                        raise TypeError("Values in keyword argument roi_ids need to be integer IDs!")
                    roi_identifier = f"roi{identifier}"
                    if roi_identifier in roi_cylinder_ids:
                        roi_cylinder_ids_report.append(roi_identifier)
                    # else this roi does not even exist, so ignore it
                print(f"Reporting composition analyses for {len(roi_cylinder_ids_report)} ROIs...")
            else:
                roi_cylinder_ids_report = roi_cylinder_ids
            del roi_cylinder_ids
            del dataset_names

            # main loop to report the results
            for roi_identifier in roi_cylinder_ids_report:
                print(f"{roi_identifier}")
                subgrpnm = f"{grpnm}/{roi_identifier}"
                sorted_distances = np.asarray(h5r[f"{subgrpnm}/signed_distance"][:], np.float32)
                nuclid_hashes = np.asarray(h5r[f"{subgrpnm}/nuclide_hash"][:], np.uint16)

                # to be consistent one has created a model of the interface
                # so we should use the location of that interface as to specify
                # the location of sigma for Gibbsian interfacial excess
                x_closest_to_interface = bisection(sorted_distances, 0.)

                if analysis_type == "composition_1d_cumulated":
                    # ##MK::plot cumulated counts for all atoms in element_whitelist = f(d)
                    element_symbols = ".".join(target_element_symbols)
                    img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.RoiId." \
                              f"{roi_identifier.replace('roi', '')}.Cdf.{element_symbols}"

                    is_target = np.isin(nuclid_hashes, target_hash_values)
                    # cumulated counts for all atoms
                    cdf_all = np.linspace(1, stop=np.shape(is_target)[0],
                                        num=np.shape(is_target)[0],
                                        endpoint=True, dtype=np.uint32)
                    # cumulated counts for atoms which are a target
                    cdf_target = np.cumsum(np.asarray(is_target, np.uint32))

                    mycmp.composition_1d_cumulated(
                        cdf_all, cdf_target, roi_identifier,
                        img_fnm,
                        xaxis_name="Cumulated count all atoms",
                        yaxis_name=f"Cumulated count {' + '.join(target_element_symbols)} atoms",
                        isocontour_location=x_closest_to_interface)
                elif analysis_type == "composition_1d_histogram":
                    element_symbols = ".".join(target_element_symbols)
                    img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.RoiId." \
                              f"{roi_identifier.replace('roi','')}.Hst1d.{element_symbols}"
                    is_target = np.isin(nuclid_hashes, target_hash_values)

                    mycmp.composition_1d_histogram(
                        sorted_distances, is_target,
                        roi_identifier,
                        img_fnm,
                        xaxis_name="Signed distance (nm)",
                        yaxis_name=" + ".join(target_element_symbols) + " composition (at.-%)",
                        binwidth=kwargs["binwidth"],
                        isocontour_location=x_closest_to_interface)
                else:  # has to be analysis_type == "interfacial_excess" then
                    element_symbols = ".".join(target_element_symbols)
                    img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.RoiId." \
                              f"{roi_identifier.replace('roi', '')}.Iex.{element_symbols}"

                    is_target = np.isin(nuclid_hashes, target_hash_values)
                    # cumulated counts for all atoms
                    cdf_all = np.linspace(1, stop=np.shape(is_target)[0],
                                        num=np.shape(is_target)[0],
                                        endpoint=True, dtype=np.uint32)
                    # cumulated counts for atoms which are a target
                    cdf_target = np.cumsum(np.asarray(is_target, np.uint32))

                    mycmp.interfacial_excess_plot(
                        cdf_all, cdf_target, roi_identifier,
                        img_fnm,
                        xaxis_name="Cumulated count all atoms",
                        yaxis_name=f"Cumulated count {' + '.join(target_element_symbols)} atoms",
                        linear_interpolation_bound=kwargs["linear_interpolation_bound"],
                        isocontour_location=x_closest_to_interface,
                        roi_cylinder_h=roi_cylinder_h,
                        roi_cylinder_r=roi_cylinder_r)

    def get_isosurface_objects_volume_and_number_over_isovalue(self, deloc_task_id=1):
        """
        Report accumulated volume and count of objects.

        Report as a function of iso-value separate stats for edge contact.
        """
        if deloc_task_id < 1:
            raise ValueError(f"Argument deloc_task_id needs to be >= 1!")
        self.delocalization = DelocalizationTask(self.results_file, deloc_task_id)

        self.isovalue = []
        self.objects_vol_far = []  # those far away from the edge
        self.objects_vol_close = []  # those close to the edge
        self.objects_vol_all = []  # all of them
        self.objects_n_far = []
        self.objects_n_close = []
        self.objects_n_all = []

        with h5py.File(self.results_file, "r") as h5r:
            for key, val in self.delocalization.isosurface_tasks.items():
                grpnm = f"/entry{self.entry_id}/delocalization{deloc_task_id}" \
                        f"/grid/iso_surface{key}/triangle_soup/triangles/volumetric_features"
                if grpnm not in h5r:
                    raise LookupError(f"Group {grpnm} does not exist in results file!")

                self.isovalue.append(val.isovalue)

                dsnm_far_vol = f"{grpnm}/objects_far_from_edge/volume"
                dsnm_close_vol = f"{grpnm}/objects_close_to_edge/volume"

                vol_far = 0.
                n_far = 0
                if dsnm_far_vol in h5r:
                    vol = np.asarray(h5r[dsnm_far_vol][:], np.float64)
                    if isinstance(vol, np.ndarray):
                        vol_far = np.sum(vol)
                        n_far = np.shape(vol)[0]
                        del vol
                self.objects_vol_far.append(vol_far)
                self.objects_n_far.append(n_far)

                vol_close = 0.
                n_close = 0
                if dsnm_close_vol in h5r:
                    vol = np.asarray(h5r[dsnm_close_vol][:], np.float64)
                    if isinstance(vol, np.ndarray):
                        vol_close = np.sum(vol)
                        n_close = np.shape(vol)[0]
                        del vol
                self.objects_vol_close.append(vol_close)
                self.objects_n_close.append(n_close)
                self.objects_vol_all.append(vol_far + vol_close)
                self.objects_n_all.append(n_far + n_close)

            print(f"len(self.isovalue) {len(self.isovalue)}")
            print(f"len(self.objects_vol_far) {len(self.objects_vol_far)}")
            print(f"len(self.objects_vol_close) {len(self.objects_vol_close)}")
            print(f"len(self.objects_vol_all) {len(self.objects_vol_all)}")
            print(f"len(self.objects_n_far) {len(self.objects_n_far)}")
            print(f"len(self.objects_n_close) {len(self.objects_n_close)}")
            print(f"len(self.objects_n_all) {len(self.objects_n_all)}")

            # ##MK::one plot for volume
            xaxis_name = "Iso-value"
            suffix = "IsoValue"
            if self.delocalization.normalization == "none":
                xaxis_name = "Ion count (atoms)"
                suffix = "IsoIonCount"
            elif self.delocalization.normalization == "composition":
                xaxis_name = "Iso-composition (at.-%)"
                suffix = "IsoComposition"
            elif self.delocalization.normalization == "concentration":
                xaxis_name = r"Iso-concentration (atoms/${nm}^3$)"
                suffix = "IsoConcentration"
            else:
                xaxis_name = "Iso-value"
                suffix = "IsoValue"

            yaxis_name = r"Volume (${nm}^3$)"
            img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.DelocTaskId.{deloc_task_id}.VolOver{suffix}"
            mysct.scatter_plot_property_set_over_value_set(
                [self.isovalue],
                [self.objects_vol_far],
                ["Interior objects"],
                img_fnm,
                xaxis_name=xaxis_name,
                yaxis_name=yaxis_name,
                xaxis_log=False, yaxis_log=False)

            # ##MK::one plot for counts
            yaxis_name = "Number of objects"
            img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.DelocTaskId.{deloc_task_id}.NumberOver{suffix}"
            mysct.scatter_plot_property_set_over_value_set(
                [self.isovalue],
                [self.objects_n_far],
                ["Interior objects"],
                img_fnm,
                xaxis_name=xaxis_name,
                yaxis_name=yaxis_name,
                xaxis_log=False, yaxis_log=False,
                manual_scaling=True, yaxis_min=0.)

    def get_isosurface_objects_volume_cdf_for_isovalue(
            self, deloc_task_id=1, **kwargs):
        """
        Report cumulated distribution of object volume.

        Report as a function of iso-value separate stats for edge contact.
        """
        if deloc_task_id < 1:
            raise ValueError(f"Argument deloc_task_id needs to be >= 1!")
        self.delocalization = DelocalizationTask(self.results_file, self.entry_id, deloc_task_id)
        self.cdf_for_isovalue = {}
        self.cdf_vol_far = {}
        self.cdf_vol_close = {}
        self.cdf_vol_all = {}

        if "isosurface_task_id_lst" in kwargs:
            if not isinstance(kwargs["isosurface_task_id_lst"], list):
                raise TypeError("Keyword argument isosurface_task_id_lst needs to be a list!")

        with h5py.File(self.results_file, "r") as h5r:
            for key, val in self.delocalization.isosurface_tasks.items():
                if "isosurface_task_id_lst" in kwargs:
                    if key not in kwargs["isosurface_task_id_lst"]:
                        continue

                grpnm = f"/entry{self.entry_id}/delocalization{deloc_task_id}" \
                        f"/grid/iso_surface{key}/triangle_soup/triangles/volumetric_features"
                if grpnm not in h5r:
                    raise LookupError(f"Group {grpnm} does not exist in results file!")

                dsnm_vol_far = f"{grpnm}/objects_far_from_edge/volume"
                dsnm_vol_close = f"{grpnm}/objects_close_to_edge/volume"

                vol_far = None
                n_far = 0
                vol_close = None
                n_close = 0
                vol_all = None
                n_all = 0
                if dsnm_vol_far in h5r:
                    vol_far = np.asarray(h5r[dsnm_vol_far][:], np.float64)
                    n_far = np.shape(vol_far)[0]
                if dsnm_vol_close in h5r:
                    vol_close = np.asarray(h5r[dsnm_vol_close][:], np.float64)
                    n_close = np.shape(vol_close)[0]

                self.cdf_for_isovalue[key] = val.isovalue
                if n_far > 0:
                    self.cdf_vol_far[key] = np.zeros([n_far, 2], np.float64)
                    self.cdf_vol_far[key][:, 0] = np.sort(vol_far, kind="mergesort")
                    self.cdf_vol_far[key][:, 1] = np.linspace(1. / n_far, 1., n_far, endpoint=True)
                else:
                    self.cdf_vol_far[key] = []
                if n_close > 0:
                    self.cdf_vol_close[key] = np.zeros([n_close, 2], np.float64)
                    self.cdf_vol_close[key][:, 0] = np.sort(vol_close, kind="mergesort")
                    self.cdf_vol_close[key][:, 1] = np.linspace(1. / n_close, 1., n_close, endpoint=True)
                else:
                    self.cdf_vol_close[key] = []

                print(f"key {key}")
                print(f"np.shape(self.cdf_for_isovalue[key]) {np.shape(self.cdf_for_isovalue[key])}")
                print(f"np.shape(self.cdf_vol_far[key]) {np.shape(self.cdf_vol_far[key])}")
                print(f"np.shape(self.cdf_vol_close[key]) {np.shape(self.cdf_vol_close[key])}")

                xaxis_name = r"Volume (${nm}^3$)"
                yaxis_name = r"Cumulated fraction"

                img_fnm_far = f"{self.results_file}.EntryId.{self.entry_id}.DelocTaskId.{deloc_task_id}.IsrfTaskId.{key}.VolEdgeNo"
                mycdf.line_plot_property_set_for_fixed_value(
                    [self.cdf_vol_far[key][:, 0]],
                    [self.cdf_vol_far[key][:, 1]],
                    ["Volume"],
                    img_fnm_far,
                    xaxis_name=xaxis_name,
                    yaxis_name=yaxis_name,
                    xaxis_log=False, yaxis_log=False,
                    manual_scaling=False)

                img_fnm_close = f"{self.results_file}.EntryId.{self.entry_id}.DelocTaskId.{deloc_task_id}.IsrfTaskId.{key}.VolEdgeYes"
                mycdf.line_plot_property_set_for_fixed_value(
                    [self.cdf_vol_close[key][:, 0]],
                    [self.cdf_vol_close[key][:, 1]],
                    ["Volume"],
                    img_fnm_close,
                    xaxis_name=xaxis_name,
                    yaxis_name=yaxis_name,
                    xaxis_log=False, yaxis_log=False,
                    manual_scaling=False)
                # title="mytitle")
                # ##MK::three plots (in, out, all) for each key

    # deactivated find code in commit 02dec700e89f6cc4990fdd2378137500ae952f6d
    # def get_summary_for_objects_and_proxies(self, deloc_task_id=1, isrf_task_id=1):
    # def get_isosurface_objects_aspect_over_isovalue(self, deloc_task_id=1):
