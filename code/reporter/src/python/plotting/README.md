# Customized plotting
This directory is a place where you can leave your plotting routines.
paraprobe-autoreporter splits up the post-processing of data in two steps.

First, results from tools like paraprobe are imported, e.g. from NeXus/HDF5 results
files. The respective tool-specific autoreporter extracts the data and processes
these according to specific desires and community-agreed upon plots and representations.
Second, these data are passed to the plotting functions.

The idea behind this is that users can customize the plotting, as oftentimes this
is more artistic work how people like figures and diagrams. This customization
should be a user's choice. The plotting itself though has often many common
code sections and thus specific wrappers are available in this directory
which wrap around the matplotlib-specific plot functions.

# Getting started
Autoreporter comes with a set of predefined plotting routines, which can be
used but might not offer all the flexibility that you require for your project.
This is why the plotting and processing is instructed with Python so that everybody
can benefit from the paraprobe-toolbox even if they have no direct experience
with developing software in C/C++. In most cases this is also not needed as
the the compiled tools of the paraprobe-toolbox have been designed that they
are sufficiently abstracted to be used with data which describe atoms with
position and ion-type information.