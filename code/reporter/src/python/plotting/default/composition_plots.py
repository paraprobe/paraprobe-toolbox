#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#

import numpy as np

import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression

from .default_design import \
    MYCOLORMAP, MYMARGIN, MYALPHA, MYWIDTH, MYFIG_WIDTH, MYFIG_DPI


def composition_1d_cumulated(
        x, y, legend_richtext, file_name,
        xaxis_name="", yaxis_name="",
        isocontour_location=None):
    """Plot one-dimensional proxigram with cumulated counts for atoms."""
    # assert x is not None and x != [], \
    #     "Argument x must contain at least one entry !"
    assert isinstance(x, (list, np.ndarray)), \
        "Argument x in x_lst needs to be either a list or an np.ndarray !"
    # assert y is not None and y != [], \
    #     "Argument y must contain at least one entry !"
    assert isinstance(y, (list, np.ndarray)), \
        "Argument y in y_lst needs to be either a list or an np.ndarray !"
    assert np.shape(x) == np.shape(y), \
        "Arguments x and y must have equal np.shape !"
    assert np.shape(x)[0] >= 1, \
        "Arguments x and y must have at least one entry !"
    # assert isinstance(isocontour_location, int), \
    #     "Argument isocontour_location has to be an integer !"

    xmi = np.min(np.append(x, [np.finfo(np.float64).max], axis=0))
    xmx = np.max(np.append(x, [np.finfo(np.float64).min], axis=0))
    ymi = np.min(np.append(y, [np.finfo(np.float64).max], axis=0))
    ymx = np.max(np.append(y, [np.finfo(np.float64).min], axis=0))

    xlim_mi = -MYMARGIN * (xmx - xmi) + xmi
    xlim_mx = +MYMARGIN * (xmx - xmi) + xmx
    ylim_mi = -MYMARGIN * (ymx - ymi) + ymi
    ylim_mx = +MYMARGIN * (ymx - ymi) + ymx

    fig, ((xy)) = plt.subplots(1, 1, constrained_layout=True)
    fig.set_size_inches(MYFIG_WIDTH, MYFIG_WIDTH)

    xy.plot(x, y, color=MYCOLORMAP[0], alpha=MYALPHA, linewidth=MYWIDTH)

    plt.legend([legend_richtext], loc="upper left")
    plt.xlabel(xaxis_name)
    plt.ylabel(yaxis_name)

    plt.xlim([xlim_mi, xlim_mx])
    plt.ylim([ylim_mi, ylim_mx])

    plt.vlines(isocontour_location, ymi, ymx,
               transform=xy.get_xaxis_transform(),
               colors="black",
               linestyles="dotted")  # MYCOLORMAP[2]

    fig = plt.gcf()

    fig_file_name = file_name + ".png"
    fig.savefig(
        fig_file_name,
        dpi=MYFIG_DPI,
        facecolor="w",
        edgecolor="w",
        orientation="landscape",
        format="png",
        transparent=False,
        bbox_inches="tight",
        pad_inches=0.1,
        metadata=None)

    plt.close("all")
    print(f"{fig_file_name} saved to disk.")

    return fig_file_name


def composition_1d_histogram(
        sgn_d, is_target, legend_richtext, file_name,
        xaxis_name="", yaxis_name="",
        binwidth=None,
        isocontour_location=None):
    """Plot one-dimensional proxigram with binned distances bin counts for atoms."""
    # assert sgn_d is not None and sgn_d != [], \
    #     "Argument sgn_d must contain at least one entry !"
    assert isinstance(sgn_d, (list, np.ndarray)), \
        "Argument sgn_d needs to be either a list or an np.ndarray !"
    assert np.shape(sgn_d)[0] >= 1, \
        "Arguments sgn_d needs to have at least one entry !"
    assert isinstance(is_target, (list, np.ndarray)), \
        "Argument is_target needs to be either a list or an np.ndarray !"
    assert np.shape(sgn_d)[0] == np.shape(is_target)[0], \
        "Arguments sgn_d and is_target need to have the same shape !"
    assert isinstance(binwidth, float), \
        "Argument binwidth needs to be a float !"
    assert binwidth >= 0.1, \
        "Argument binwidth should be at least 0.1 nm !"
    # assert isinstance(isocontour_location, int), \
    #     "Argument isocontour_location has to be an integer !"

    # binning x, i.e. distances of all atoms, into a fixed number of bins
    # xmi = np.floor(np.min(sgn_d))
    # xmx = np.ceil(np.max(sgn_d))
    # sgn_d = sorted_distances
    # legend_richtext = "test"
    # binwidth = 1.

    xmi = np.floor(np.min(sgn_d) / binwidth) * binwidth
    xmx = np.ceil(np.max(sgn_d) / binwidth) * binwidth
    nbins = int(np.ceil((xmx - xmi) / binwidth))

    bin_edges = np.linspace(xmi + binwidth,
                            stop=xmx, num=int(nbins),
                            endpoint=True)

    hst1d_targets, bin_edges = np.histogram(
        sgn_d[is_target == True], bins=bin_edges,
        density=False)  # bins=int(dist_nbins))  # , density=False)
    hst1d_all, bin_edges = np.histogram(
        sgn_d, bins=bin_edges,
        density=False)

    ymi = np.min(hst1d_targets / hst1d_all * 100.)  # at.-%
    ymx = np.max(hst1d_targets / hst1d_all * 100.)

    fig, ((xy)) = plt.subplots(1, 1, constrained_layout=True)
    # fig.set_size_inches(MYFIG_WIDTH, MYFIG_WIDTH)

    xy.stairs(hst1d_targets / hst1d_all * 100., bin_edges)
    # s=50, marker=".", facecolors=None, edgecolors=MYCOLORMAP[0])

    plt.legend([legend_richtext], loc="upper left")
    plt.xlabel(xaxis_name)
    plt.ylabel(yaxis_name)

    plt.xlim([xmi - 0.5 * binwidth, xmx + 0.5 * binwidth])

    plt.vlines(isocontour_location, ymi, ymx,
               transform=xy.get_xaxis_transform(),
               colors="black",
               linestyles="dotted")  # MYCOLORMAP[2]

    fig = plt.gcf()

    fig_file_name = file_name + ".png"
    fig.savefig(
        fig_file_name,
        dpi=MYFIG_DPI,
        facecolor="w",
        edgecolor="w",
        orientation="landscape",
        format="png",
        transparent=False,
        bbox_inches="tight",
        pad_inches=0.1,
        metadata=None)

    plt.close("all")
    print(f"{fig_file_name} saved to disk.")

    return fig_file_name


def interfacial_excess_plot(
        x, y, legend_richtext,
        file_name,
        xaxis_name="", yaxis_name="",
        linear_interpolation_bound=None,
        isocontour_location=None,
        roi_cylinder_h=None,
        roi_cylinder_r=None):
    """Plot interfacial excess construction and compute IE value."""
    # assert x is not None and x != [], \
    #     "Argument x must contain at least one entry !"
    assert isinstance(x, (list, np.ndarray)), \
        "Argument x in x_lst needs to be either a list or an np.ndarray !"
    # assert y is not None and y != [], \
    #     "Argument y must contain at least one entry !"
    assert isinstance(y, (list, np.ndarray)), \
        "Argument y in y_lst needs to be either a list or an np.ndarray !"
    assert np.shape(x) == np.shape(y), \
        "Arguments x and y must have equal np.shape !"
    assert np.shape(x)[0] >= 1, \
        "Arguments x and y must have at least one entry !"
    assert isinstance(linear_interpolation_bound, float), \
        "Argument linear_interpolation_bound needs to be a float !"
    assert 0.01 <= linear_interpolation_bound < 0.50, \
        "Argument linear_interpolation_bound needs to from interval [0.1, 0.5) !"
    assert isinstance(isocontour_location, int), \
        "Argument isocontour_location has to be an int !"
    assert isinstance(roi_cylinder_h, float), \
        "Argument roi_cylinder_h needs to be a float !"
    assert roi_cylinder_h > 0., \
        "Argument roi_cylinder_h needs to larger than 0. nm !"
    assert isinstance(roi_cylinder_r, float), \
        "Argument roi_cylinder_r needs to be a float !"
    assert roi_cylinder_r > 0., \
        "Argument roi_cylinder_r needs to be larger than 0. nm !"

    # graphical analysis as proposed by Krakauer, Seidman
    # ##MK::add references

    n_total = np.shape(x)[0]
    # alpha = 0.1
    # lower bound
    # physically the lower_line needs to go through 0, 0
    lower_model = LinearRegression(fit_intercept=False)
    # fit_intercept=False sets the y-intercept to 0
    lower_bound = linear_interpolation_bound
    lower_x = x[0:int(lower_bound * n_total)].reshape(-1, 1)
    lower_y = y[0:int(lower_bound * n_total)]
    lower_model.fit(lower_x, lower_y)
    # print(lower_model.coef_)
    # print(lower_model.intercept_)
    # print(lower_model.score(lower_x, lower_y))
    # lower line equation
    # cnts[:] = coeff_[0][0] * cnts_total[:] + 0

    # upper bound
    # physically the upper_line needs to go through ntotal, n_targets
    upper_model = LinearRegression(fit_intercept=True)
    upper_bound = 1. - linear_interpolation_bound
    upper_x = x[int(upper_bound * n_total):n_total].reshape(-1, 1)  # - n_total
    upper_y = y[int(upper_bound * n_total):n_total]  # - n_targets
    upper_model.fit(upper_x, upper_y)
    # print(upper_model.coef_)
    # print(upper_model.intercept_)
    # print(upper_model.score(upper_x, upper_y))

    # linear models
    lower_x_line = x[:].reshape(-1, 1)
    lower_y_line = lower_model.predict(lower_x_line)

    upper_x_line = x[:].reshape(-1, 1)  # + n_total
    upper_y_line = upper_model.predict(upper_x_line)  # + n_targets

    # compute specific interfacial excess value
    # based on specific $\Phi^{\sigma}_i$ value
    n_total_i = y[-1]
    n_lower_i = lower_model.predict(
        np.asarray([isocontour_location]).reshape(-1, 1))
    #    [int(linear_interpolation_bound * n_total)]).reshape(-1, 1))
    n_upper_i = upper_model.predict(
        np.asarray([isocontour_location]).reshape(-1, 1))
    #    [int((1. - linear_interpolation_bound) * n_total)]).reshape(-1, 1))
    n_sigma_i = n_total_i - n_lower_i - n_upper_i

    ie_value = n_sigma_i[0] / (np.pi * roi_cylinder_r**2)  # at/nm^2
    print(f"Interfacial excess value {ie_value} atoms/nm^2")

    # cumulated count for interfacial excess
    fig, ((xy)) = plt.subplots(1, 1, constrained_layout=True)
    fig.set_size_inches(MYFIG_WIDTH, MYFIG_WIDTH)

    xy.plot(x, y, color=MYCOLORMAP[0], alpha=MYALPHA, linewidth=MYWIDTH)
    xy.plot(lower_x_line, lower_y_line,
            color="black", linestyle="dotted", linewidth=1)
    xy.plot(upper_x_line, upper_y_line,
            color="black", linestyle="dotted", linewidth=1)

    # xy.set_title(grpnm)
    plt.legend([legend_richtext], loc="upper left")
    # plt.legend(["cumulated", "lower", "upper", "surface"], loc="upper left")
    plt.xlabel(xaxis_name)
    plt.ylabel(yaxis_name)

    plt.xlim([0, n_total])

    ymi = np.min(y)
    ymx = np.max(y)

    plt.vlines(isocontour_location, ymi, ymx,
               transform=xy.get_xaxis_transform(),
               colors="black",
               linestyles="dotted")  # MYCOLORMAP[2]

    fig = plt.gcf()

    fig_file_name = file_name + ".png"
    fig.savefig(
        fig_file_name,
        dpi=MYFIG_DPI,
        facecolor="w",
        edgecolor="w",
        orientation="landscape",
        format="png",
        transparent=False,
        bbox_inches="tight",
        pad_inches=0.1,
        metadata=None)

    plt.close("all")
    print(f"{fig_file_name} saved to disk.")

    return fig_file_name


# ##MK::proof-of-concept, fuse with results from others

def interfacial_excess_value(
        x, y, linear_interpolation_bound=None,
        isocontour_location=None,
        roi_cylinder_r=None):
    """Compute IE value."""
    # assert x is not None and x != [], \
    #     "Argument x must contain at least one entry !"
    assert isinstance(x, (list, np.ndarray)), \
        "Argument x in x_lst needs to be either a list or an np.ndarray !"
    # assert y is not None and y != [], \
    #     "Argument y must contain at least one entry !"
    assert isinstance(y, (list, np.ndarray)), \
        "Argument y in y_lst needs to be either a list or an np.ndarray !"
    assert np.shape(x) == np.shape(y), \
        "Arguments x and y must have equal np.shape !"
    assert np.shape(x)[0] >= 1, \
        "Arguments x and y must have at least one entry !"
    assert isinstance(linear_interpolation_bound, float), \
        "Argument linear_interpolation_bound needs to be a float !"
    assert 0.01 <= linear_interpolation_bound < 0.5, \
        "Argument linear_interpolation_bound needs to from interval [0.1, 0.5) !"
    assert isinstance(isocontour_location, int), \
        "Argument isocontour_location has to be an int !"
    assert isinstance(roi_cylinder_r, float), \
        "Argument roi_cylinder_r needs to be a float !"
    assert roi_cylinder_r > 0., \
        "Argument roi_cylinder_r needs to be larger than 0. nm !"

    # graphical analysis as proposed by Krakauer, Seidman
    # ##MK::add references

    n_total = np.shape(x)[0]
    # alpha = 0.1
    # lower bound
    # physically the lower_line needs to go through 0, 0
    lower_model = LinearRegression(fit_intercept=False)
    # fit_intercept=False sets the y-intercept to 0
    lower_bound = linear_interpolation_bound
    lower_x = x[0:int(lower_bound * n_total)].reshape(-1, 1)
    lower_y = y[0:int(lower_bound * n_total)]
    lower_model.fit(lower_x, lower_y)
    # print(lower_model.coef_)
    # print(lower_model.intercept_)
    # print(lower_model.score(lower_x, lower_y))
    # lower line equation
    # cnts[:] = coeff_[0][0] * cnts_total[:] + 0

    # upper bound
    # physically the upper_line needs to go through ntotal, n_targets
    upper_model = LinearRegression(fit_intercept=True)
    upper_bound = 1. - linear_interpolation_bound
    upper_x = x[int(upper_bound * n_total):n_total].reshape(-1, 1)  # - n_total
    upper_y = y[int(upper_bound * n_total):n_total]  # - n_targets
    upper_model.fit(upper_x, upper_y)
    # print(upper_model.coef_)
    # print(upper_model.intercept_)
    # print(upper_model.score(upper_x, upper_y))

    # compute specific interfacial excess value
    # based on specific $\Phi^{\sigma}_i$ value
    n_total_i = y[-1]
    n_lower_i = lower_model.predict(
        np.asarray([isocontour_location]).reshape(-1, 1))
    #    [int(linear_interpolation_bound*n_total)]).reshape(-1, 1))
    n_upper_i = upper_model.predict(
        np.asarray([isocontour_location]).reshape(-1, 1))
    #    [int((1. - linear_interpolation_bound)*n_total)]).reshape(-1, 1))
    n_sigma_i = n_total_i - n_lower_i - n_upper_i

    ie_value = n_sigma_i[0] / (np.pi * roi_cylinder_r**2)  # at/nm^2

    return ie_value
