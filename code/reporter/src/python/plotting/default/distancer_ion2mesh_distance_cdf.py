#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#

import numpy as np

import matplotlib.pyplot as plt

from .default_design import \
    MYCOLORMAP, MYALPHA, MYWIDTH, MYFIG_WIDTH, MYFIG_DPI, MYMARGIN


def plot_cdf_ion2mesh_distance(d, cdf, legend, file_name, **kwargs):
    """Plot CDF of shortest Euclidean distance ion-to-surface-mesh-triangles."""
    xmi = np.min(d)
    xmx = np.max(d[d < 1.0e19])  # see comment on maximum value
    ymi = np.min(cdf)
    ymx = np.max(cdf)

    xlim_mi = -MYMARGIN * (xmx - xmi) + xmi
    xlim_mx = +MYMARGIN * (xmx - xmi) + xmx
    # xlim_len = xlim_mx - xlim_mi

    ylim_mi = -MYMARGIN * (ymx - ymi) + ymi
    ylim_mx = +MYMARGIN * (ymx - ymi) + ymx
    # ylim_len = ylim_mx - ylim_mi

    fig, ((xy)) = plt.subplots(1, 1, constrained_layout=True)

    xy.plot(d, cdf,
            color=MYCOLORMAP[0],
            alpha=MYALPHA,
            linewidth=MYWIDTH)

    plt.legend(legend, loc="upper left")
    plt.xlabel(r"Ion-to-mesh distance (nm)")
    plt.ylabel(r"Cumulative distribution")

    plt.xlim([xlim_mi, xlim_mx])
    plt.ylim([ylim_mi, ylim_mx])

    if "title" in kwargs:
        plt.set_title(kwargs["title"])

    if "vlines" in kwargs:
        for line in kwargs["vlines"]:
            plt.vlines(
                [line], 0, 1, transform=xy.get_xaxis_transform(),
                colors=MYCOLORMAP[2], linestyles="dotted")

    fig = plt.gcf()
    fig.set_size_inches(MYFIG_WIDTH, MYFIG_WIDTH)

    fig_file_name = f"{file_name}.png"
    fig.savefig(
        fig_file_name,
        dpi=MYFIG_DPI,
        facecolor="w",
        edgecolor="w",
        orientation="landscape",
        format="png",
        transparent=False,
        bbox_inches="tight",
        pad_inches=0.1,
        metadata=None)

    plt.close("all")
    print(f"{fig_file_name} saved to disk.")

    return fig_file_name
