#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#

import numpy as np

import matplotlib.pyplot as plt

from .default_design import \
    MYCOLORMAP, MYMARGIN, MYFIG_WIDTH, MYFIG_DPI


def scatter_plot_property_set_over_value_set(
        x_lst, y_lst, legend, file_name,
        xaxis_name="", yaxis_name="",
        xaxis_log=False, yaxis_log=False,
        manual_scaling=False,
        **kwargs):
    """Plot property as a function of isovalue."""
    assert x_lst is not None and x_lst != [], \
        "Argument x_lst must contain at least one entry !"
    assert y_lst is not None and y_lst != [], \
        "Argument property_lst must contain at least one entry !"
    assert len(x_lst) == len(y_lst), \
        "Arguments x_lst and y_lst must have equal length !"
    assert len(x_lst) <= 3, \
        "Plotting more than three curves is currently not implemented !"
    for x in x_lst:
        assert isinstance(x, (list, np.ndarray)), \
            "Argument x in x_lst needs to be either a list or an np.ndarray !"
    for y in y_lst:
        assert isinstance(y, (list, np.ndarray)), \
            "Argument y in y_lst needs to be either a list or an np.ndarray !"
    # customize this for you if you want ...
    # ... just extend number of entries in MYCOLORMAP
    assert isinstance(legend, list), \
        "Argument legend needs to be a list !"

    # test data

    if "xaxis_min" not in kwargs:
        xmi = np.finfo(np.float64).max
        for x in x_lst:
            xmi = np.min(np.append(x, [xmi], axis=0))
    else:
        assert isinstance(kwargs["xaxis_min"], float), \
            "Keyword argument xaxis_min needs to be a float !"
        xmi = kwargs["xaxis_min"]
    if xaxis_log is True:
        assert xmi > 0., \
            "Plotting negative x on logarithmic xaxis is not supported !"
    if "xaxis_max" not in kwargs:
        xmx = np.finfo(np.float64).min
        for x in x_lst:
            xmx = np.max(np.append(x, [xmx], axis=0))
    else:
        assert isinstance(kwargs["xaxis_max"], float), \
            "Keyword argument xaxis_max needs to be a float !"
        xmx = kwargs["xaxis_max"]
    if "yaxis_min" not in kwargs:
        ymi = np.finfo(np.float64).max
        for y in y_lst:
            ymi = np.min(np.append(y, [ymi], axis=0))
    else:
        assert isinstance(kwargs["yaxis_min"], float), \
            "Keyword argument yaxis_min needs to be a float !"
        ymi = kwargs["yaxis_min"]
    if yaxis_log is True:
        assert ymi > 0., \
            "Plotting negative y on logarithmic yaxis is not supported !"
    if "yaxis_max" not in kwargs:
        ymx = np.finfo(np.float64).min
        for y in y_lst:
            ymx = np.max(np.append(y, [ymx], axis=0))
    else:
        assert isinstance(kwargs["yaxis_max"], float), \
            "Keyword argument yaxis_max needs to be a float !"
        ymx = kwargs["yaxis_max"]

    # print(f"xmi {xmi}")
    # print(f"xmx {xmx}")
    # print(f"ymi {ymi}")
    # print(f"ymx {ymx}")

    if manual_scaling is True:
        # identify axes bounds which some safe space
        xlim_mi = -MYMARGIN * (xmx - xmi) + xmi
        if "xaxis_min" in kwargs:
            xlim_mi = xmi
        if xaxis_log is True:
            assert xlim_mi > 0., \
                "Plotting negative xlim_mi on logarithmic xaxis is not supported !"
        xlim_mx = +MYMARGIN * (xmx - xmi) + xmx
        if "xaxis_max" in kwargs:
            xlim_mx = xmx
        ylim_mi = -MYMARGIN * (ymx - ymi) + ymi
        if "yaxis_min" in kwargs:
            ylim_mi = ymi
        if yaxis_log is True:
            assert ylim_mi > 0., \
                "Plotting negative ylim_mi on logarithmic yaxis is not supported !"
        ylim_mx = +MYMARGIN * (ymx - ymi) + ymx
        if "yaxis_max" in kwargs:
            ylim_mx = ymx

    fig, ((xy)) = plt.subplots(1, 1, constrained_layout=True)
    fig.set_size_inches(MYFIG_WIDTH, MYFIG_WIDTH)

    for i in np.arange(0, len(x_lst)):
        marker_radii = 50
        if "custom_marker_scaling" in kwargs:
            assert np.shape(kwargs["custom_marker_scaling"])[1] == np.shape(x_lst[i])[0], \
                "Custom marker scaling does not match with length of data point array !"
            marker_radii = kwargs["custom_marker_scaling"]
        xy.scatter(x_lst[i], y_lst[i],
                   s=marker_radii,
                   marker=".",
                   facecolors=None,
                   edgecolors=MYCOLORMAP[i])

    plt.legend(legend)  # loc="upper right")
    plt.xlabel(xaxis_name)
    plt.ylabel(yaxis_name)

    if xaxis_log is True:
        plt.xscale("log")
    if yaxis_log is True:
        plt.yscale("log")

    if manual_scaling is True:
        plt.xlim([xlim_mi, xlim_mx])
        plt.ylim([ylim_mi, ylim_mx])

    # https://matplotlib.org/3.1.1/gallery/subplots_axes_and_figures/figure_title.html
    if "title" in kwargs:
        fig.suptitle(kwargs["title"])

    if "vlines" in kwargs:
        for line in kwargs["vlines"]:
            plt.vlines(
                [line], ymi, ymx, transform=xy.get_xaxis_transform(),
                colors=MYCOLORMAP[2], linestyles="dotted")

    fig = plt.gcf()

    fig_file_name = file_name + ".png"
    fig.savefig(
        fig_file_name,
        dpi=MYFIG_DPI,
        facecolor="w",
        edgecolor="w",
        orientation="landscape",
        format="png",
        transparent=False,
        bbox_inches="tight",
        pad_inches=0.1,
        metadata=None)

    plt.close("all")
    print(f"{fig_file_name} saved to disk.")

    return fig_file_name
