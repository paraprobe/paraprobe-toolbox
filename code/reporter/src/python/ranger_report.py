#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Post-processing of results from paraprobe-ranger for frequently asked analyses."""

import numpy as np
import h5py

from ifes_apt_tc_data_modeling.utils.utils import nuclide_hash_to_human_readable_name


class ReporterRanger():
    """Support the user with using results from paraprobe-ranger runs."""

    def __init__(self, h5fn, entry_id=1):
        self.tool_name = "Ranger"
        # self.tool_hash = "0"
        self.results_file = h5fn
        self.entry_id = entry_id
        self.results = {}

    def get_summary(self):
        """Get summary statistics per iontype(s)."""
        pivot_charge_yes = {}
        pivot_charge_no = {}
        with h5py.File(self.results_file, "r") as h5r:

            # attempt loading applied ranging definitions (NXion) instances named ion<iontype_id>
            dsnm = f"/entry{self.entry_id}/iontypes/iontypes"
            print(f"Reading iontypes from {dsnm}")
            assert dsnm in h5r, f"Dataset {dsnm} does not exist in results file !"
            unique, counts = np.unique(
                np.asarray(h5r[dsnm][:], np.uint8),
                return_counts=True)
            iontypes_dict = dict(zip(unique, counts))
            print(f"Ions total {np.sum(counts)}, i.e. {np.around(np.sum(counts)/1.0e6, decimals=3)} mio")

            # display ranging results annotated with information only
            # available via provenance
            # ranger results knows its ranger (ranger config)
            # ranger config knows its input transcoder result
            for iontype_idx, number_of_ions in iontypes_dict.items():
                grpnm = f"/entry{self.entry_id}/iontypes/ion{iontype_idx}"
                assert grpnm in h5r, f"Group {grpnm} does not exist in results file !"
                dsnm = f"{grpnm}/nuclide_hash"
                assert dsnm in h5r, f"Dataset {dsnm} does not exist in results file !"
                ivec = h5r[dsnm][:]
                dsnm = f"{grpnm}/charge_state"
                assert dsnm in h5r, f"Dataset {dsnm} does not exist in results file !"
                charge_state = int(h5r[dsnm][()])
                ion_name = nuclide_hash_to_human_readable_name(ivec, charge_state)

                print(f"\t{np.around(number_of_ions / np.sum(counts) * 100., decimals=4)}"
                      f" at.-%,\t{ion_name}\t{[iontype_idx]}, {number_of_ions}")

                if ion_name in pivot_charge_yes:
                    pivot_charge_yes[ion_name]["count"] += number_of_ions
                    pivot_charge_yes[ion_name]["iontypes"].append(iontype_idx)
                else:
                    pivot_charge_yes[ion_name] \
                        = {"count": number_of_ions, "iontypes": [iontype_idx]}

                unique_name = ion_name.replace("+", "").replace("-", "").strip()
                if unique_name in pivot_charge_no:
                    pivot_charge_no[unique_name]["count"] += number_of_ions
                    pivot_charge_no[unique_name]["iontypes"].append(iontype_idx)
                else:
                    pivot_charge_no[unique_name] \
                        = {"count": number_of_ions, "iontypes": [iontype_idx]}

        print("Aggregate information for unique molecular ions (considering charge state)")
        for ion_name, dct in pivot_charge_yes.items():
            number_of_ions = dct["count"]
            ityp_lst = dct["iontypes"]
            print(f"\t{np.around(number_of_ions / np.sum(counts) * 100., decimals=4)}"
                  f" at.-%,\t{ion_name}\t{ityp_lst}, {number_of_ions}")

        print("Aggregate information for unique molecular ions (ignoring charge state)")
        for ion_name, dct in pivot_charge_no.items():
            number_of_ions = dct["count"]
            ityp_lst = dct["iontypes"]
            print(f"\t{np.around(number_of_ions / np.sum(counts) * 100., decimals=4)}"
                  f" at.-%,\t{ion_name}\t{ityp_lst}, {number_of_ions}")
