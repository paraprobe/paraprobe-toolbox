#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Post-processing of results from paraprobe-selector for frequently asked analyses."""

import numpy as np
import h5py


class ReporterSelector():
    """Support the user with using results from paraprobe-selector runs."""

    def __init__(self, h5fn, entry_id=1):
        self.tool_name = "Selector"
        self.results_file = h5fn
        self.entry_id = entry_id
        self.results = {}

    def get_summary(self):
        """Get summary occupation of the ROI."""
        with h5py.File(self.results_file, "r") as h5r:
            n_ions = h5r["/entry1/roi/window/number_of_ions"][()]
            print(f"The entire dataset contains a total of {n_ions} ions.")
            decompressed_bitmask = h5r["/entry1/roi/window/mask"][:]
            # print(np.shape(decompressed_bitmask))
            # print(type(decompressed_bitmask))
            # print(decompressed_bitmask.dtype)
            mask = np.unpackbits(decompressed_bitmask, count=n_ions, bitorder="little")
            # print(np.shape(mask))
            # print(type(mask))
            # print(mask.dtype)
            print(f"The selected ROI contains a total of {np.sum(mask)} ions.")
