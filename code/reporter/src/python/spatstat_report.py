#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Post-processing of results from paraprobe-spatstat for frequently asked analyses."""

import numpy as np
import h5py

from utils.src.python.numerics import EPSILON
import reporter.src.python.plotting.default.line_plot_property_set_for_fixed_value as mycdf


class ReporterSpatstat():
    """Support the user with using results from paraprobe-spatstat runs."""

    def __init__(self, results_file, entry_id=1):
        self.tool_name = "Spatstat"
        self.results_file = results_file
        self.entry_id = entry_id
        self.results = {}

    def get_knn(self, task_id=1):
        """Compute probability and cumulated of kth nearest neighbor."""
        with h5py.File(self.results_file, "r") as h5r:
            # load config file
            grpnm = f"/entry{self.entry_id}/spatial_statistics{task_id}/knn/"
            assert grpnm in h5r, f"Group {grpnm} does not exist in results file !"
            dsnms = ["distance", "probability_mass", "cumulated", "cumulated_normalized"]
            for dsnm in dsnms:
                assert f"{grpnm}{dsnm}" in h5r, \
                    f"Dataset {grpnm}{dsnm} does not exist in results file !"

            r = h5r[f"{grpnm}distance"][0:-1]
            pdf = h5r[f"{grpnm}probability_mass"][0:-1]

            if (np.shape(r)[0] == np.shape(pdf)[0]) and (np.shape(r)[0] >= 3):
                xaxis_name = r"Distance (nm)"
                yaxis_name = r"Probability mass"
                # yaxis_name = r"Cumulated fraction"

                img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.TaskId.{task_id}.Knn.Pdf"
                return mycdf.line_plot_property_set_for_fixed_value(
                    [r],
                    [pdf],
                    ["k-th order nearest neighbour"],
                    img_fnm,
                    xaxis_name=xaxis_name,
                    yaxis_name=yaxis_name,
                    xaxis_log=False, yaxis_log=False,
                    manual_scaling=False)
            return None

    def get_rdf(self, task_id=1, normalizer=1.):
        """Compute radial distribution function."""
        assert isinstance(normalizer, float), \
            "Argument normalizer has to be a float !"
        with h5py.File(self.results_file, "r") as h5r:
            # load config file
            grpnm = f"/entry{self.entry_id}/process{task_id}/rdf/"
            assert grpnm in h5r, f"Group {grpnm} does not exist in results file !"
            dsnms = ["distance", "probability_mass", "cumulated", "cumulated_normalized"]
            for dsnm in dsnms:
                assert f"{grpnm}{dsnm}" in h5r, \
                    f"Dataset {grpnm}{dsnm} does not exist in results file !"

            r = h5r[f"{grpnm}distance"][0:-1]
            pdf = h5r[f"{grpnm}probability_mass"][0:-1]
            # following the definitions on page 281ff of
            # B. Gault, M. P. Moody, J. M. Cairney and S. P. Ringer
            # Atom Probe Microscopy, dx.doi.org/10.1007/978-1-4614-3436-8
            # mind that n_{RDF}(r) in between we find that accumulated
            # what is a scaled estimate for the RipleyK statistics
            if (np.shape(r)[0] == np.shape(pdf)[0]) and (np.shape(r)[0] >= 3):
                nrows = np.shape(r)[0]
                rdf = np.zeros([nrows, 4], np.float64)
                for i in np.arange(1, nrows - 1):
                    # ##MK::possibly an r-offset here by half the bin-width!
                    # likely can be made faster using numpy
                    rdf[i, 0] = 0.5 * (r[i] + r[i - 1])  # mid-point r
                    rdf[i, 1] = 0.5 * (r[i] - r[i - 1])  # \Delta r/2
                    rdf[i, 2] = 4. / 3. * np.pi \
                        * (((rdf[i, 0] + rdf[i, 1])**3) - ((rdf[i, 0] - rdf[i, 1])**3))
                    if rdf[i, 2] > EPSILON:
                        rdf[i, 3] = normalizer * (pdf[i] / rdf[i, 2])

                xaxis_name = r"Distance (nm)"
                yaxis_name = r"Radial distribution"

                img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.TaskId.{task_id}.Rdf"
                return mycdf.line_plot_property_set_for_fixed_value(
                    [rdf[0:-1, 0]],
                    [rdf[0:-1, 3]],
                    ["Radial distribution"],
                    img_fnm,
                    xaxis_name=xaxis_name,
                    yaxis_name=yaxis_name,
                    xaxis_log=False, yaxis_log=False,
                    manual_scaling=False)
            return None
