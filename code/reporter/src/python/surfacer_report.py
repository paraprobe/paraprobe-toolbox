#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Post-processing of results from paraprobe-surfacer for frequently asked analyses."""


class ReporterSurfacer():
    """Support the user with using results from paraprobe-surfacer runs."""

    def __init__(self, h5fn, entry_id=1):
        self.tool_name = "Surfacer"
        # self.tool_hash = "0"
        self.results_file = h5fn
        self.entry_id = entry_id
        self.results = {}

# base functionality where on passes an NXcg_face_list and gets a summary
# of the primitive statistics e.g triangle area distribution, counts, angles,
# etc.
