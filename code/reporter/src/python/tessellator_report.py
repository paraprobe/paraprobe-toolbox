#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Post-processing of results from paraprobe-tessellator for frequently asked analyses."""

import numpy as np
import h5py

import reporter.src.python.plotting.default.line_plot_property_set_for_fixed_value as mycdf


class ReporterTessellator():
    """Support the user with using results from paraprobe-tessellator runs."""

    def __init__(self, results_file, entry_id=1):
        self.toolname = "Tessellator"
        self.tool_hash = "0"
        self.results_file = results_file
        self.entry_id = entry_id
        self.results = {}

    def get_summary(self):
        """Report summary statistics for Voronoi tessellation."""
        with h5py.File(self.results_file, "r") as h5r:
            grpnm = f"/entry{self.entry_id}/tessellation"
            assert grpnm in h5r, \
                f"Group {grpnm} does not exist in results file !"
            dsnm_vol = f"{grpnm}/voronoi_cells/volume"
            assert dsnm_vol in h5r, \
                f"Dataset {dsnm_vol} does not exist in results file !"
            dsnm_n = f"{grpnm}/wall_contact_global/number_of_objects"
            assert dsnm_n in h5r, \
                f"Dataset {dsnm_n} does not exist in results file !"
            dsnm_con = f"{grpnm}/wall_contact_global/mask"
            assert dsnm_con in h5r, \
                f"Dataset {dsnm_con} does not exist in results file !"

            vol = np.asarray(h5r[dsnm_vol][:], np.float64)
            n_ions = h5r[dsnm_n][()]
            assert n_ions == np.shape(vol)[0], \
                "Bitmask wall_contact_global needs to have the same dim than vol !"
            wall = np.unpackbits(h5r[dsnm_con][:], count=n_ions, bitorder="little")
            print("Statistics without taking edge effects into account")
            print(f"Minimum volume is {np.min(vol)} nm^3")
            print(f"Average volume is {np.mean(vol)} nm^3")
            print(f"Maximum volume is {np.max(vol)} nm^3")
            print(f"Median volume is {np.median(vol)} nm^3")
            print("Statistics with taking edge effects into account")
            print(f"Minimum volume is {np.min(vol[wall != 1])} nm^3")
            print(f"Average volume is {np.mean(vol[wall != 1])} nm^3")
            print(f"Maximum volume is {np.max(vol[wall != 1])} nm^3")
            print(f"Median volume is {np.median(vol[wall != 1])} nm^3")

    def get_cell_volume_cdf(self, task_id=1, quantile_based=True):
        """Compute cumulated distribution of cell volume of ions in the ROI."""
        with h5py.File(self.results_file, "r") as h5r:
            grpnm = f"/entry{self.entry_id}/tessellation"
            assert grpnm in h5r, \
                f"Group {grpnm} does not exist in results file !"

            dsnm_vol = f"{grpnm}/voronoi_cells/volume"
            assert dsnm_vol in h5r, \
                f"Dataset {dsnm_vol} does not exist in results file !"
            dsnm_n = f"{grpnm}/wall_contact_global/number_of_objects"
            assert dsnm_n in h5r, \
                f"Dataset {dsnm_n} does not exist in results file !"
            dsnm_con = f"{grpnm}/wall_contact_global/mask"
            assert dsnm_con in h5r, \
                f"Dataset {dsnm_con} does not exist in results file !"

            vol = np.sort(np.asarray(h5r[dsnm_vol][:], np.float64), kind="mergesort")
            n_ions = h5r[dsnm_n][()]
            assert n_ions == np.shape(vol)[0], \
                "Bitmask wall_contact_global needs to have the same dim than vol !"
            wall = np.unpackbits(h5r[dsnm_con][:], count=n_ions, bitorder="little")
            # use array masking
            # https://stackoverflow.com/questions/20917703/deleting-certain-elements-from-numpy-array-using-conditional-checks
            # how to efficient rearrange using numpy only
            # a1 = np.asarray([4, 1, 2, 3], np.uint32)
            # a2 = np.asarray([4.4, 1.1, 2.2, 3.3], np.float32)
            # sort_idxs =np.asarray(np.argsort(a1, axis=0, kind="mergesort"), np.uint32)
            # a1[sort_idxs]
            # a2[sort_idxs]
            assert n_ions == np.shape(wall)[0], \
                "Arrays vol and wall need to have the same length !"
            cdf = np.asarray(np.linspace(1. / n_ions, 1., n_ions, endpoint=True), np.float64)
            # without filtering vol for contact or not reports the biased distribution!
            print(np.shape(vol))
            print(np.shape(cdf))
            if quantile_based is True:
                quantiles = np.asarray([0.0001, 0.001])
                quantiles = np.append(quantiles, np.linspace(0.01, 0.99, num=99, endpoint=True))
                quantiles = np.append(quantiles, [0.999, 0.9999])
                vol = np.quantile(vol, quantiles, method='linear')
                cdf = quantiles
                lgd = "All cells (quantile based)"
            else:
                lgd = "All cells"
            print("Cumulated distribution of cell volume computed")

            xaxis_name = r"Cell volume $({nm}^3)$"
            yaxis_name = r"Cumulated fraction"

            img_fnm = f"{self.results_file}.EntryId.{self.entry_id}.TaskId.{task_id}.CellsAllVolCDF"
            return mycdf.line_plot_property_set_for_fixed_value(
                [vol],
                [cdf],
                [lgd],
                img_fnm,
                xaxis_name=xaxis_name,
                yaxis_name=yaxis_name,
                xaxis_log=True, yaxis_log=False,
                manual_scaling=True,
                xaxis_min=0.001)
