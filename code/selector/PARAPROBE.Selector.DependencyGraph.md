v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-spatstat source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigSelector.h/.cpp
 2. PARAPROBE_SelectorHdl.h/.cpp
 3. PARAPROBE_Selector.cpp
