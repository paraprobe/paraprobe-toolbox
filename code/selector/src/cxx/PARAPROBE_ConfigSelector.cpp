/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigSelector.h"


ostream& operator << (ostream& in, roi_selection_task const & val)
{
	in << "ROI selection task" << "\n";
	in << "taskid " << val.taskid << "\n";
	switch(val.WdwMethod)
	{
		case ENTIRE_DATASET:
		{
			cout << "using the entire dataset" << "\n"; break;
		}
		case UNION_OF_PRIMITIVES:
		{
			cout << "using a union of sets with different primitives" << "\n";
			cout << "val.WdwSpheres.size() " << val.WdwSpheres.size() << "\n";
			cout << "val.WdwCylinders.size() " << val.WdwCylinders.size() << "\n";
			cout << "val.WdwCuboids.size() " << val.WdwCuboids.size() << "\n";
			break;
		}
		//##MK::implement other cases
		default:
			break;
	}

	//implement other filters
	return in;
}


string ConfigSelector::InputfileDataset = "";
string ConfigSelector::ReconstructionDatasetName = "";
string ConfigSelector::MassToChargeDatasetName = "";
string ConfigSelector::InputfileIonTypes = "";
string ConfigSelector::IonTypesGroupName = "";

vector<roi_selection_task> ConfigSelector::RoiSelectionTasks = vector<roi_selection_task>();

//sensible defaults

bool ConfigSelector::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigSelector::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string dsnm = "";

		//##MK::currently this tool assumes and works only with a single analysis task defined

		apt_uint number_of_processes = 1;
		apt_uint entry_id = 1;
		grpnm = "/entry" + to_string(entry_id);

		RoiSelectionTasks = vector<roi_selection_task>();
		for ( apt_uint tskid = 0; tskid < number_of_processes; tskid++ ) {
			//##MK::currently this tool assumes and works only with a single analysis task defined
			cout << "Reading configuration for task " << tskid << "\n";
			//apt_uint proc_id = tskid + 1;

			RoiSelectionTasks.push_back( roi_selection_task() );
			RoiSelectionTasks.back().taskid = tskid + 1;

			grpnm = "/entry" + to_string(entry_id) + "/select/reconstruction";
			string path = "";
			if ( h5r.nexus_read( grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileDataset = path_handling( path );
			cout << "InputfileDataset " << InputfileDataset << "\n";
			if ( h5r.nexus_read( grpnm + "/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
			if ( h5r.nexus_read( grpnm + "/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";

			grpnm = "/entry" + to_string(entry_id) + "/select/ranging";
			path = "";
			if ( h5r.nexus_read( grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
			InputfileIonTypes = path_handling( path );
			cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
			if ( h5r.nexus_read( grpnm + "/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
			cout << "IonTypesGroupName " << IonTypesGroupName << "\n";

			//optional spatial filtering
			RoiSelectionTasks.back().WdwMethod = ENTIRE_DATASET;
			grpnm = "/entry" + to_string(entry_id) + "/select/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				//currently only UNION_OF_PRIMITIVES is implemented as a windowing method
				RoiSelectionTasks.back().WdwMethod = UNION_OF_PRIMITIVES;

				grpnm = "/entry" + to_string(entry_id) + "/select/spatial_filter/ellipsoid_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								RoiSelectionTasks.back().WdwSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/select/spatial_filter/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								RoiSelectionTasks.back().WdwCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/select/spatial_filter/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								RoiSelectionTasks.back().WdwCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << RoiSelectionTasks.back().WdwSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << RoiSelectionTasks.back().WdwCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << RoiSelectionTasks.back().WdwCuboids.size() << "\n";
			}
			//##MK::BITMASKED_POINTS is not implemented yet

			//optional sub-sampling filter for ion/evaporation ID
			grpnm = "/entry" + to_string(entry_id) + "/select/evaporation_id_filter";
			if( h5r.nexus_path_exists( grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					RoiSelectionTasks.back().LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << RoiSelectionTasks.back().LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			grpnm = "/entry" + to_string(entry_id) + "/select/iontype_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				RoiSelectionTasks.back().IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << RoiSelectionTasks.back().IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			grpnm = "/entry" + to_string(entry_id) + "/select/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				RoiSelectionTasks.back().HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << RoiSelectionTasks.back().HitMultiplicityFilter << "\n";

			cout << RoiSelectionTasks.back() << "\n";

		} //done with defining all tasks

		//sensible defaults

		return true;
	}

	//special developer case SimID == 0

	return false;
}
