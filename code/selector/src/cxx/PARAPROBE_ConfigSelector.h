/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_CONFIG_SELECTOR_H__
#define __PARAPROBE_CONFIG_SELECTOR_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"


struct roi_selection_task
{
	apt_uint taskid;							//taskID

	WINDOWING_METHOD WdwMethod;
	vector<roi_sphere> WdwSpheres;
	vector<roi_rotated_cylinder> WdwCylinders;
	vector<roi_rotated_cuboid> WdwCuboids;
	//vector<roi_polyhedron> WdwPolyhedra;
	lival<apt_uint> LinearSubSamplingRange;
	match_filter<unsigned char> IontypeFilter;
	match_filter<unsigned char> HitMultiplicityFilter;

	roi_selection_task() :
		taskid(UMX),
		WdwMethod(ENTIRE_DATASET),
		WdwSpheres(vector<roi_sphere>()),
		WdwCylinders(vector<roi_rotated_cylinder>()),
		WdwCuboids(vector<roi_rotated_cuboid>()),
		LinearSubSamplingRange(lival<apt_uint>( 0, 1, UMX )),
		IontypeFilter(match_filter<unsigned char>()),
		HitMultiplicityFilter(match_filter<unsigned char>()) {}
};

ostream& operator << (ostream& in, roi_selection_task const & val);


class ConfigSelector
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static string InputfileDataset;
	static string ReconstructionDatasetName;
	static string MassToChargeDatasetName;
	static string InputfileIonTypes;
	static string IonTypesGroupName;

	static vector<roi_selection_task> RoiSelectionTasks;

	//sensible defaults
};

#endif
