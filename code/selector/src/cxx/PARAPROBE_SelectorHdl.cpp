/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_SelectorHdl.h"


selectorHdl::selectorHdl()
{
	window = NULL;
	selector_tictoc = profiler();
}


selectorHdl::~selectorHdl()
{
	if ( window != NULL ) {
		delete [] window; window = NULL;
	}
}


bool selectorHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigSelector::InputfileDataset, ConfigSelector::ReconstructionDatasetName ) == true &&
			read_ranging_from_h5( ConfigSelector::InputfileIonTypes, ConfigSelector::IonTypesGroupName ) == true &&
				read_ionlabels_from_h5( ConfigSelector::InputfileIonTypes, ConfigSelector::IonTypesGroupName ) == true ) {

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//build also here a default array with pieces of information for all ions
		if ( ions.ionpp3.size() > 0 && ions.ionifo.size() != ions.ionpp3.size() ) {
			try {
				ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(),
						p3dinfo( RMX, UNKNOWNTYPE, UNKNOWNTYPE, 0x01, WINDOW_ION_EXCLUDE ) );
				//assume all ions are infinitely large from the edge, of unknown (default ion type), and multiplicity one (i.e. 0x01)
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of information array for the ions failed !" << "\n";
				return false;
			}
		}

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		selector_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;
}


void selectorHdl::crop_reconstruction_to_analysis_window( roi_selection_task const & ifo )
{
	double mytic = MPI_Wtime();

	if ( ions.ionpp3.size() >= UMX ) {
		cerr << "Current implementation does not allow to handle datasets with more than UMX ions !" << "\n";
		return;
	}

	if ( window != NULL ) {
		delete [] window; window = NULL;
	}
	try {
		window = new bool[ions.ionpp3.size()];
	}
	catch(bad_alloc &croak) {
		cerr << "Allocation error window for spatial statistics task " << ifo.taskid << " !" << "\n"; return;
	}

	//assume first that all ions are taken, set then those to false which we wish to filter out
	//the filter pipeline is additive, first take all, optionally filter out spatially, optionally sub-sample, optionally filter out based on types, hit_multiplicity
	for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
		window[i] = true;
	}

	switch(ifo.WdwMethod)
	{
		case ENTIRE_DATASET:
		{
			//for this filter nothing has to be done, window is already true for all
			break;
		}
		case UNION_OF_PRIMITIVES:
		{
			//for this filter we need to check, for each ion, if the ion is at least inside or on the boundary of one of the primitives
			#pragma omp parallel
			{
				vector<apt_uint> myres;
				apt_uint nions = (apt_uint) ions.ionpp3.size();
				#pragma omp for
				for( apt_uint idx = 0; idx < nions; idx++ ) {
					bool keep = false; //optimize for filtering strongly
					if ( keep == false ) {
						for( auto it = ifo.WdwSpheres.begin(); it != ifo.WdwSpheres.end(); it++ ) {
							if ( it->is_inside( ions.ionpp3[idx] ) == true ) { //in most cases ROI is much smaller than entire dataset
								keep = true;
							}
						}
					}
					if ( keep == false ) {
						for( auto jt = ifo.WdwCylinders.begin(); jt != ifo.WdwCylinders.end(); jt++ ) {
							if ( jt->is_inside( ions.ionpp3[idx] ) == true ) {
								keep = true;
							}
						}
					}
					if ( keep == false ) {
						for( auto kt = ifo.WdwCuboids.begin(); kt != ifo.WdwCuboids.end(); kt++ ) {
							if ( kt->is_inside( ions.ionpp3[idx] ) == true ) {
								keep = true;
							}
						}
					}
					if ( keep == false ) {
						myres.push_back( idx );
					}
				}

				#pragma omp critical
				{
					//flag those ions which should not be kept, i.e. filtered out, so that these wont get considered in the analysis
					for( auto it = myres.begin(); it != myres.end(); it++ ) {
						window[*it] = false;
					}
					myres = vector<apt_uint>();
				}
			}
			break;
		}
		case BITMASKED_POINTS:
		{
			cerr << "Bitmasked points is not implemented yet !" << "\n";
			break;
		}
		default:
		{
			break;
		}
	}

	//post-process linear-subsampling
	apt_uint idmin = ifo.LinearSubSamplingRange.min;
	apt_uint idincr = ifo.LinearSubSamplingRange.incr;
	apt_uint idmax = ifo.LinearSubSamplingRange.max;
	apt_uint nions = (apt_uint) ions.ionpp3.size();
	if ( idmax >= nions ) {
		idmax = nions - 1;
	}
	//MK::the filter switches those ions off (sets mask1 to ANALYZE_NO) which
	//should not be analyzed, because for sub-sampling very often the number of
	//ions chosen is smaller than those filtered off
	for( apt_uint i = 0; i < idmin; i++ ) { //clip evaporation IDs too small
		window[i] = false;
	}
	//omitted += (idmin - 0);
	for( apt_uint j = idmin; j <= idmax; j++ ) {
		if ( ((j - idmin) + 1) % idincr != 0 ) {
			window[j] = false;
			//omitted++; //##MK::make a more elegant solution
		}
	}
	for( apt_uint k = idmax+1; k < nions; k++ ) { //clip evaporation IDs too large
		window[k] = false;
	}
	//omitted += (nions - (idmax+1));

	//iontypes filter
	//apt_uint omitted = 0;
	//shared(omitted)
	#pragma omp parallel
	{
		//thread-local cache
		vector<apt_uint> my_omit_res;
		apt_uint nions = (apt_uint) ions.ionpp3.size();
		#pragma omp for
		for( apt_uint idx = 0; idx < nions; idx++ ) {
			unsigned char ityp = ions.ionifo[idx].i_org;
			if ( ifo.IontypeFilter.is_blacklist == true ) { //filtering for ityp_list acting as a blacklist
				for( auto it = ifo.IontypeFilter.candidates.begin(); it != ifo.IontypeFilter.candidates.end(); it++ ) {
					if ( *it == ityp ) {
						my_omit_res.push_back(idx);
						break;
					}
				}
			}
			if( ifo.IontypeFilter.is_whitelist == true ) { //filtering for ityp_list acting as a whitelist
				bool found = false;
				for( auto it = ifo.IontypeFilter.candidates.begin(); it != ifo.IontypeFilter.candidates.end(); it++ ) {
					if ( *it == ityp ) {
						found = true;
						break;
					}
				}
				if ( found == false ) {
					my_omit_res.push_back(idx);
				}
			}
		}
		#pragma omp barrier
		#pragma omp critical
		{
			//for( auto it = myres.begin(); it != myres.end(); it++ ) {
			for ( auto it = my_omit_res.begin(); it != my_omit_res.end(); it++ ) {
				//ions.ionifo.at(it->first).mask1 = it->second;
				//do not check whether ions have already set to ANALYZE_NO
				window[*it] = false;
			}
			//omitted += (apt_uint) my_omit_res.size();
			//myres = map<apt_uint, unsigned char>();
			my_omit_res = vector<apt_uint>();
		}
	}
	//cout << "Applied iontype remove filter omitted " << omitted << "\n";

	//##MK::hit multiplicity not implemented

	//check filter
	apt_uint accepted = 0;
	apt_uint omitted = 0;
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( window[i] == true ) {
			continue;
		}
		else {
			omitted++;
		}
	}

	accepted = ((apt_uint) ions.ionifo.size()) - omitted;
	cout << "Filter state accepted " << accepted << " omitted " << omitted <<
			" total ion fraction accepted " << ((double) accepted) / ((double) (accepted + omitted)) << "\n";

	double mytoc = MPI_Wtime();
	cout << "Multithreaded cropping of the reconstructed volume to the analysis window took " << (mytoc-mytic) << "\n";
}


void selectorHdl::execute_workpackage()
{
	double tic = MPI_Wtime();

	//##MK::inject here a sophisticated task scheduler which executes tasks that could reutilized queried positions
	//##MK::in such a way that you only create the query structure once and reutilize

	for( auto tskit = ConfigSelector::RoiSelectionTasks.begin(); tskit != ConfigSelector::RoiSelectionTasks.end(); tskit++ ) {

		//filter from all ions of the dataset those which are in the analysis window
		crop_reconstruction_to_analysis_window( *tskit );

		write_results( *tskit);
	}

	double toc = MPI_Wtime();
	cout << "execute_workpackage took " << (toc-tic) << "\n";
}


int selectorHdl::write_results( roi_selection_task const & ifo )
{
	//write randomized labels to file although the above randomization is deterministic
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/roi";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	grpnm = "/entry" + to_string(entry_id) + "/roi/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_filter_boolean_mask") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) ions.ionifo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/bitdepth";
	apt_uint bitdepth = 8;
	if ( h5w.nexus_write( dsnm, bitdepth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/mask";
	if ( window != NULL ) {
		vector<unsigned char> u08_bitfield = vector<unsigned char>();
		BitPacker packbits;
		packbits.bool_to_bitpacked_uint8( window, n_ions, u08_bitfield );
		anno = ioAttributes();
		if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) {
				return false;
		}
		//do not delete window, will be removed by the destructor of selectorHdl !
	}
	else {
		cerr << "window array was not allocatable!" << "\n";
		return false;
	}

	return MYHDF5_SUCCESS;
}
