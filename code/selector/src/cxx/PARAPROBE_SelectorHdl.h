/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SELECTOR_HDL_H__
#define __PARAPROBE_SELECTOR_HDL_H__


#include "PARAPROBE_ConfigSelector.h"


class selectorHdl : public mpiHdl
{
public:
	selectorHdl();
	~selectorHdl();

	bool read_relevant_input();

	void crop_reconstruction_to_analysis_window( roi_selection_task const & ifo );

	void execute_workpackage();

	int write_results( roi_selection_task const & ifo );

	bool* window;

	profiler selector_tictoc;
};

#endif
