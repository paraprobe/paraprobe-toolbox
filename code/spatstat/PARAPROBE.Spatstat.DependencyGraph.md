v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-spatstat source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigSpatstat.h/.cpp
 2. PARAPROBE_SpatstatStructs.h/.cpp
 3. PARAPROBE_SpatstatHDF5.h/.cpp (obsolete)
 4. PARAPROBE_SpatstatXDMF.h/.cpp (obsolete)
 5. PARAPROBE_SpatstatHdl.h/.cpp
 6. PARAPROBE_Spatstat.cpp
