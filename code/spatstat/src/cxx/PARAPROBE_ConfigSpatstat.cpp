/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigSpatstat.h"


ostream& operator << (ostream& in, knn_info const & val)
{
	in << "kth " << val.kth << "\n";
	in << "[ " << val.hist1d.min << ", " << val.hist1d.max << " ]" << " step " << val.hist1d.incr << "\n";
	return in;
}


ostream& operator << (ostream& in, rdf_info const & val)
{
	in << "[ " << val.hist1d.min << ", " << val.hist1d.max << " ]" << " step " << val.hist1d.incr << "\n";
	return in;
}


ostream& operator << (ostream& in, spatstat_task const & val)
{
	in << "Spatial statistics task" << "\n";
	in << "taskid " << val.taskid << "\n";
	switch(val.WdwMethod)
	{
		case ENTIRE_DATASET:
		{
			cout << "using the entire dataset" << "\n"; break;
		}
		case UNION_OF_PRIMITIVES:
		{
			cout << "using a union of sets with different primitives" << "\n";
			cout << "val.WdwSpheres.size() " << val.WdwSpheres.size() << "\n";
			cout << "val.WdwCylinders.size() " << val.WdwCylinders.size() << "\n";
			cout << "val.WdwCuboids.size() " << val.WdwCuboids.size() << "\n";
			break;
		}
		//##MK::implement other cases
		default:
			break;
	}

	//implement other filters

	switch(val.src_qtyp)
	{
		case RESOLVE_NONE: { in << "src_qtyp none" << "\n"; break; }
		case RESOLVE_ALL: { in << "src_qtyp all" << "\n"; break; }
		case RESOLVE_UNKNOWN: { in << "src_qtyp unranged" << "\n"; break; }
		case RESOLVE_ION: { in << "src_qtyp ion" << "\n"; break; }
		case RESOLVE_ELEMENT: { in << "src_qtyp element" << "\n"; break; }
		case RESOLVE_ISOTOPE: { in << "src_qtyp isotope" << "\n"; break; }
		default:
			break;
	}
	switch(val.trg_qtyp)
	{
		case RESOLVE_NONE: { in << "trg_qtyp none" << "\n"; break; }
		case RESOLVE_ALL: { in << "trg_qtyp all" << "\n"; break; }
		case RESOLVE_UNKNOWN: { in << "trg_qtyp unranged" << "\n"; break; }
		case RESOLVE_ION: { in << "trg_qtyp ion" << "\n"; break; }
		case RESOLVE_ELEMENT: { in << "trg_qtyp element" << "\n"; break; }
		case RESOLVE_ISOTOPE: { in << "trg_qtyp isotope" << "\n"; break; }
		default:
			break;
	}
	in << "source ions " << "\n";
	for( auto it = val.src.begin(); it != val.src.end(); it++ ) {
		in << *it << "\n";
	}
	in << "target ions " << "\n";
	for( auto jt = val.trg.begin(); jt != val.trg.end(); jt++ ) {
		in << *jt << "\n";
 	}
	if ( val.randomized == RANDOMIZE_YES )
		cout << "using randomized iontypes" << "\n";
	else
		cout << "using original iontypes" << "\n";
	switch(val.statstyp)
	{
		case SPATSTAT_KNN:
		{
			in << "k-th nearest neighbor task" << "\n";
			in << "kth " << val.knn_ifo.kth << "\n";
			in << "rmin " << val.knn_ifo.hist1d.min << "\n";
			in << "rincr " << val.knn_ifo.hist1d.incr << "\n";
			in << "rmax " << val.knn_ifo.hist1d.max << "\n"; break;
		}
		case SPATSTAT_RDF:
		{
			in << "rmin " << val.rdf_ifo.hist1d.min << "\n";
			in << "rincr " << val.rdf_ifo.hist1d.incr << "\n";
			in << "rmax " << val.rdf_ifo.hist1d.max << "\n"; break;
		}
		/*
		implement other statistics
		*/
		default:
			break;
	}
	in << "far_from_dedge " << val.far_from_dedge << "\n";
	in << "close_to_dfeat " << val.close_to_dfeat << "\n";
	if ( val.IOPdfAndCdf == true )
		in << "store pdf and cdf " << "yes" << "\n";
	else
		in << "store pdf and cdf " << "no" << "\n";
	if ( val.IODescriptor == true )
		in << "store descriptor " << "yes" << "\n";
	else
		in << "store descriptor " << "no" << "\n";
	return in;
}


string ConfigSpatstat::InputfileDataset = "";
string ConfigSpatstat::ReconstructionDatasetName = "";
string ConfigSpatstat::MassToChargeDatasetName = "";
string ConfigSpatstat::InputfileIonTypes = "";
string ConfigSpatstat::IonTypesGroupName = "";

string ConfigSpatstat::InputfileIonToEdgeDistances = "";
string ConfigSpatstat::IonToEdgeDistancesDatasetName = "";
string ConfigSpatstat::InputfileIonToFeatureDistances = "";
string ConfigSpatstat::IonToFeatureDistancesDatasetName = "";

vector<spatstat_task> ConfigSpatstat::SpatialStatisticsTasks = vector<spatstat_task>();

//sensible defaults
string ConfigSpatstat::PRNGType = "MT19937";
size_t ConfigSpatstat::PRNGWarmup = 700000;
long ConfigSpatstat::PRNGWorldSeed = -12345678;


bool ConfigSpatstat::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigSpatstat::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string dsnm = "";

		//##MK::currently this tool assumes and works only with a single analysis task defined

		apt_uint number_of_tasks = 0;
		apt_uint entry_id = 1;
		grpnm = "/entry" + to_string(entry_id);
		if ( h5r.nexus_read(grpnm + "/number_of_tasks", number_of_tasks ) != MYHDF5_SUCCESS ) {
			return false;
		}

		if ( number_of_tasks != 1 ) {
			cerr << "Stopping because currently only number_of_tasks == 1 !" << "\n";
			return false;
		}

		SpatialStatisticsTasks = vector<spatstat_task>();
		for ( apt_uint tskid = 0; tskid < number_of_tasks; tskid++ ) {
			//##MK::currently this tool assumes and works only with a single analysis task defined
			cout << "Reading configuration for task " << tskid << "\n";
			apt_uint proc_id = tskid + 1;

			SpatialStatisticsTasks.push_back( spatstat_task() );
			SpatialStatisticsTasks.back().taskid = tskid;

			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/reconstruction";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string path = "";
				if ( h5r.nexus_read( grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileDataset = path_handling( path );
				cout << "InputfileDataset " << InputfileDataset << "\n";
				if ( h5r.nexus_read( grpnm + "/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
				if ( h5r.nexus_read( grpnm + "/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";

				grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/ranging";
				path = "";
				if ( h5r.nexus_read( grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonTypes = path_handling( path );
				cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
				if ( h5r.nexus_read( grpnm + "/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IonTypesGroupName " << IonTypesGroupName << "\n";
			}

			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/surface_distance";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string path = "";
				if ( h5r.nexus_read( grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonToEdgeDistances = path_handling( path );
				cout << "InputfileIonToEdgeDistances " << InputfileIonToEdgeDistances << "\n";
				if ( h5r.nexus_read( grpnm + "/distance", IonToEdgeDistancesDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IonToEdgeDistancesDatasetName " << IonToEdgeDistancesDatasetName << "\n";
				if ( h5r.nexus_read( grpnm + "/edge_distance", SpatialStatisticsTasks.back().far_from_dedge ) != MYHDF5_SUCCESS ) { return false; }
				cout << "FarFromDedge threshold " << SpatialStatisticsTasks.back().far_from_dedge << "\n";
			}

			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/feature_distance";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string path = "";
				if ( h5r.nexus_read( grpnm + "/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonToFeatureDistances = path_handling( path );
				cout << "InputfileIonToFeatureDistances " << InputfileIonToFeatureDistances << "\n";
				if ( h5r.nexus_read( grpnm + "/distance", IonToFeatureDistancesDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "IonToFeatureDistancesDatasetName " << IonToFeatureDistancesDatasetName << "\n";
				if ( h5r.nexus_read( grpnm + "/feature_distance", SpatialStatisticsTasks.back().close_to_dfeat ) != MYHDF5_SUCCESS ) { return false; }
				cout << "FeatureDistance threshold " << SpatialStatisticsTasks.back().close_to_dfeat << "\n";
			}

			//optional spatial filtering
			SpatialStatisticsTasks.back().WdwMethod = ENTIRE_DATASET;
			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				//currently only UNION_OF_PRIMITIVES is implemented as a windowing method
				SpatialStatisticsTasks.back().WdwMethod = UNION_OF_PRIMITIVES;

				grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/spatial_filter/ellipsoid_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								SpatialStatisticsTasks.back().WdwSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/spatial_filter/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								SpatialStatisticsTasks.back().WdwCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/spatial_filter/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								SpatialStatisticsTasks.back().WdwCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << SpatialStatisticsTasks.back().WdwSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << SpatialStatisticsTasks.back().WdwCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << SpatialStatisticsTasks.back().WdwCuboids.size() << "\n";
			}
			//##MK::BITMASK is not implemented yet

			//optional sub-sampling filter for ion/evaporation ID
			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/evaporation_id_filter";
			if( h5r.nexus_path_exists( grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					SpatialStatisticsTasks.back().LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << SpatialStatisticsTasks.back().LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/iontype_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				SpatialStatisticsTasks.back().IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << SpatialStatisticsTasks.back().IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				SpatialStatisticsTasks.back().HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << SpatialStatisticsTasks.back().HitMultiplicityFilter << "\n";

			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id) + "/statistics";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				if ( h5r.nexus_path_exists( grpnm + "/knn" ) == true ) {
					SpatialStatisticsTasks.back().statstyp = SPATSTAT_KNN;
					apt_uint nth = 1;
					if ( h5r.nexus_read( grpnm + "/knn/kth", nth ) != MYHDF5_SUCCESS ) { return false; }
					cout << "nth " << nth << "\n";
					vector<apt_real> real;
					if ( h5r.nexus_read( grpnm + "/knn/min_incr_max", real ) != MYHDF5_SUCCESS ) { return false; }
					if ( real.size() != 3 ) {
						cerr << "real.size() != 3 ! for " << grpnm << "\n"; return false;
					}
					SpatialStatisticsTasks.back().knn_ifo = knn_info( nth, real[1], real[2] );
					//nearest neighbor kth=nth, binned on [0., ..] nm with .. nm step
				}
				else if ( h5r.nexus_path_exists( grpnm + "/rdf" ) == true ) {
					SpatialStatisticsTasks.back().statstyp = SPATSTAT_RDF;
					vector<apt_real> real;
					if ( h5r.nexus_read( grpnm + "/rdf/min_incr_max", real ) != MYHDF5_SUCCESS ) { return false; }
					if ( real.size() != 3 ) {
						cerr << "real.size() != 3 ! for " << grpnm << "\n"; return false;
					}
					SpatialStatisticsTasks.back().rdf_ifo = rdf_info( real[1], real[2] );
					//radial distribution function binned on [0., ..] nm with .. nm step
				}
				else {
					cerr << "No statistics defined for me to analyze !" << "\n"; return false;
				}
			}

			//make the spatial filtering a property of a base task then have all tasks taking from it
			//consider the source ions, i.e. those ions where ROIs for quantifying spatial statistics should be placed
			SpatialStatisticsTasks.back().src = vector<ion>();

			grpnm = "/entry" + to_string(entry_id) + "/spatial_statistics" + to_string(proc_id);

			string ion_query_type_source = "";
			if ( h5r.nexus_read( grpnm + "/ion_query_type_source", ion_query_type_source ) != MYHDF5_SUCCESS ) { return false; }
			cout << "ion_query_type_source " << ion_query_type_source << "\n";
			bool isotope_vector_needed = false;
			bool charge_state_needed = false;
			if ( ion_query_type_source.compare( "resolve_all" ) == 0 ) { //all ions irrespective their isotope_vector
				SpatialStatisticsTasks.back().src_qtyp = RESOLVE_ALL;
			}
			else if ( ion_query_type_source.compare( "resolve_unknown" ) == 0 ) { //only ions of UNKNOWN_TYPE
				SpatialStatisticsTasks.back().src_qtyp = RESOLVE_UNKNOWN;
			}
			else if ( ion_query_type_source.compare( "resolve_ion" ) == 0 ) { //only ions with a specific isotope_vector
				SpatialStatisticsTasks.back().src_qtyp = RESOLVE_ION;
				isotope_vector_needed = true;
				charge_state_needed = true;
			}
			else if ( ion_query_type_source.compare( "resolve_element" ) == 0 ) { //only ions with specific elements in their isotope vector
				SpatialStatisticsTasks.back().src_qtyp = RESOLVE_ELEMENT;
				isotope_vector_needed = true;
			}
			else if (  ion_query_type_source.compare( "resolve_isotope" ) == 0 ) { //only ions with specific isotopes in their isotope vector
				SpatialStatisticsTasks.back().src_qtyp = RESOLVE_ISOTOPE;
				isotope_vector_needed = true;
			}
			else {
				SpatialStatisticsTasks.back().src_qtyp = RESOLVE_NONE;
				cerr << "ion_query_type_source is not one of the supported !" << "\n"; return false;
			}

			if ( isotope_vector_needed == true ) {
				vector<unsigned short> u16 = vector<unsigned short>();
				vector<unsigned char> u08 = vector<unsigned char>();
				if ( h5r.nexus_read( grpnm + "/ion_query_nuclide_source", u16 ) != MYHDF5_SUCCESS ) { return false; }
				size_t n_rows = u16.size() / MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION;
				if ( n_rows == 0 ) {
					cerr << "Loading ion_query_type_source nuclide array is empty !" << "\n"; return false;
				}
				/*
				if ( charge_state_needed == true ) {
					if ( h5r.nexus_read( grpnm + "/charge_state_array_source", u08 ) != MYHDF5_SUCCESS ) { return false; }
					if ( u08.size() == 0 ) {
						cerr << "Loading charge_state_array_source array is empty !" << "\n"; return false;
					}
					if ( n_rows != u08.size() ) {
						cerr << "Loading charge_state_array_source charge_state array does not match dimensions of isotope_vector !" << "\n";
						return false;
					}
				}
				*/
				for( size_t row_idx = 0; row_idx < n_rows; row_idx++ ) {
					SpatialStatisticsTasks.back().src.push_back( ion() );
					for ( int ii = 0; ii < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; ii++ ) {
						SpatialStatisticsTasks.back().src.back().ivec[ii]
							= u16.at(row_idx*MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION+ii);
					}
					SpatialStatisticsTasks.back().src.back().charge_sgn = MYPOSITIVE;
					if ( charge_state_needed == true ) {
						SpatialStatisticsTasks.back().src.back().charge_state = u08.at(row_idx);
					}
					else {
						SpatialStatisticsTasks.back().src.back().charge_state = 0;
					}
				}
			}

			//define targets, i.e. those ions in the ROI of a source ion which should be considered for the spatial statistics
			SpatialStatisticsTasks.back().trg = vector<ion>();
			string ion_query_type_target = "";
			if ( h5r.nexus_read( grpnm + "/ion_query_type_target", ion_query_type_target ) != MYHDF5_SUCCESS ) { return false; }
			cout << "ion_query_type_target " << ion_query_type_target << "\n";
			isotope_vector_needed = false;
			charge_state_needed = false;
			if ( ion_query_type_target.compare( "resolve_all" ) == 0 ) { //all ions irrespective their isotope_vector
				SpatialStatisticsTasks.back().trg_qtyp = RESOLVE_ALL;
			}
			else if ( ion_query_type_target.compare( "resolve_unknown" ) == 0 ) { //only ions of UNKNOWN_TYPE
				SpatialStatisticsTasks.back().trg_qtyp = RESOLVE_UNKNOWN;
			}
			else if ( ion_query_type_target.compare( "resolve_ion" ) == 0 ) { //only ions with a specific isotope_vector
				SpatialStatisticsTasks.back().trg_qtyp = RESOLVE_ION;
				isotope_vector_needed = true;
				charge_state_needed = true;
			}
			else if ( ion_query_type_target.compare( "resolve_element" ) == 0 ) { //only ions with specific elements in their isotope vector
				SpatialStatisticsTasks.back().trg_qtyp = RESOLVE_ELEMENT;
				isotope_vector_needed = true;
			}
			else if (  ion_query_type_target.compare( "resolve_isotope" ) == 0 ) { //only ions with specific isotopes in their isotope vector
				SpatialStatisticsTasks.back().trg_qtyp = RESOLVE_ISOTOPE;
				isotope_vector_needed = true;
			}
			else {
				SpatialStatisticsTasks.back().trg_qtyp = RESOLVE_NONE;
				cerr << "ion_query_type_target is not one of the supported !" << "\n"; return false;
			}

			if ( isotope_vector_needed == true ) {
				vector<unsigned short> u16 = vector<unsigned short>();
				vector<unsigned char> u08 = vector<unsigned char>();
				if ( h5r.nexus_read( grpnm + "/ion_query_nuclide_target", u16 ) != MYHDF5_SUCCESS ) { return false; }
				size_t n_rows = u16.size() / MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION;
				if ( n_rows == 0 ) {
					cerr << "Loading ion_query_type_target nuclide array is empty !" << "\n";
					return false;
				}
				/*
				if ( charge_state_needed == true ) {
					if ( h5r.nexus_read( grpnm + "/charge_state_array_target", u08 ) != MYHDF5_SUCCESS ) { return false; }
					if ( u08.size() == 0 ) {
						cerr << "Loading charge_state_array_target array is empty !" << "\n";
						return false;
					}
					if ( n_rows != u08.size() ) {
						cerr << "Loading charge_state_array_target charge_state array does not match dimensions of isotope_vector !" << "\n";
						return false;
					}
				}
				*/
				for( size_t row_idx = 0; row_idx < n_rows; row_idx++ ) {
					SpatialStatisticsTasks.back().trg.push_back( ion() );
					for ( int ii = 0; ii < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; ii++ ) {
						SpatialStatisticsTasks.back().trg.back().ivec[ii]
							= u16.at(row_idx*MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION+ii);
					}
					SpatialStatisticsTasks.back().trg.back().charge_sgn = MYPOSITIVE;
					if ( charge_state_needed == true ) {
						SpatialStatisticsTasks.back().trg.back().charge_state = u08.at(row_idx);
					}
					else {
						SpatialStatisticsTasks.back().trg.back().charge_state = 0;
					}
				}
			}

			unsigned char option = 0x00;
			if ( h5r.nexus_read( grpnm + "/randomize_ion_types", option ) != MYHDF5_SUCCESS ) { return false; }
			cout << "randomized " << (int) option << "\n";
			SpatialStatisticsTasks.back().randomized = ( option == 0x01 ) ? RANDOMIZE_YES : RANDOMIZE_NO;
			SpatialStatisticsTasks.back().IOPdfAndCdf = true;
			SpatialStatisticsTasks.back().IODescriptor = true;

			cout << SpatialStatisticsTasks.back() << "\n";

		} //done with defining all tasks

		//sensible defaults
		cout << "PRNGType " << PRNGType << "\n";
		cout << "PRNGWarmup " << PRNGWarmup << "\n";
		cout << "PRNGWorldSeed " << PRNGWorldSeed << "\n";

		return true;
	}

	//special developer case SimID == 0

	return false;
}
