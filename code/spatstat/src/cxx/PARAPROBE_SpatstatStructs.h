/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SPATSTAT_STRUCTS_H__
#define __PARAPROBE_SPATSTAT_STRUCTS_H__

#include "PARAPROBE_ConfigSpatstat.h"


struct query_info
{
	apt_uint ionidx;
	unsigned char src_mult;			//=0, means do not place a ROI here, regardless values for close_to_dfeat and far_from_dedge
									//>0, translates into multiplicity of factoring this ion for the current task
	//e.g. if the task is to filter H atoms and we have an ion type H the multiplicity is 1, if ion type is H2 it is 2 if ion type is OH its 1, if ion type is Y its 0
	unsigned char trg_mult;			//same meaning as src_mult
	bool far_from_dedge;						//true if at least enough away from the edge of the dataset
	bool close_to_dfeat;						//true if within close enough to feature
	query_info() : ionidx(UMX), src_mult(0), trg_mult(0), far_from_dedge(false), close_to_dfeat(false) {}
	query_info( const apt_uint _ionidx, const unsigned char _srcmlt, const unsigned char _trgmlt, const bool _dedge, const bool _dfeat ) :
		ionidx(_ionidx), src_mult(_srcmlt), trg_mult(_trgmlt), far_from_dedge(_dedge), close_to_dfeat(_dfeat) {}
};

ostream& operator << (ostream& in, query_info const & val);


#endif
