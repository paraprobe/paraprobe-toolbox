v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-surfacer source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigSurfacer.h/.cpp
 2. PARAPROBE_SurfacerCGAL.h
 3. PARAPROBE_SurfacerEigen.h
 4. PARAPROBE_SurfacerStructs.h/.cpp
 5. PARAPROBE_SurfacerHDF5.h/.cpp (obsolete)
 6. PARAPROBE_SurfacerXDMF.h/.cpp (obsolete)
 7. PARAPROBE_SurfacerHoshenKopelman.h/.cpp
 8. PARAPROBE_SurfacerOutsideInsideMask.h/cpp
 9. PARAPROBE_SurfacerMesher.h/.cpp (compute, export, and test of alpha shapes and window for prefiltering, offset of alpha wrapping)
10. PARAPROBE_SurfacerHdl.h/.cpp
11. PARAPROBE_Surfacer.cpp
