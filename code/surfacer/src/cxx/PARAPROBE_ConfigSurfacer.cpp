/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigSurfacer.h"


ostream& operator << (ostream& in, surface_detection_task const & val)
{
	in << "Alpha-shape task info" << "\n";
	in << "TaskID " << val.taskid << "\n";
	switch(val.edge_quant_type)
	{
		case SURFACE_ALPHASHAPE_FAST:
		{
			in << "Edge quantification method based on M. Kühbach et al. HK-based pruning strategy" << "\n"; break;
		}
		case SURFACE_ALPHASHAPE_NAIVE:
		{
			in << "Edge quantification based on a naive passing of point cloud data" << "\n"; break;
		}
		default:
		{
			in << "Edge quantification nothing" << "\n"; break;
		}
	}
	in << "Edge quantification kernel width " << val.edge_kernel_width << "\n";
	switch(val.alpha_model)
	{
		case CONVEX_HULL_NAIVE:
		{
			in << "Alpha shape value representing infinity i.e. the convex hull using a convex hull computation algorithm" << "\n"; break;
		}
		case CONVEX_HULL_REFINE:
		{
			in << "Alpha shape value representing infinity i.e. the convex hull using a convex hull computation algorithm followed by mesh refinement" << "\n"; break;
		}
		case ASHAPE_SMALLEST_SOLID:
		{
			in << "Alpha shape value selection model takes alpha so that the alpha shape is the smallest solid" << "\n"; break;
		}
		case ASHAPE_CGAL_OPTIMAL:
		{
			in << "Alpha shape value selection model takes alpha based on what CGAL considers as optimal" << "\n"; break;
		}
		case ASHAPE_ALPHAVALUE_ENSEMBLE:
		{
			in << "Alpha shape value selection model probe a range of alpha values and eventually computes the volume for each" << "\n"; break;
		}
		case ASHAPE_ALPHAVALUE_WRAPPINGS:
		{
			in << "Alpha shape value selection model probe a range of alpha wrappings" << "\n"; break;
		}
		default:
		{
			in << "Alpha shape value nothing" << "\n"; break;
		}
	}
	in << "Alpha values " << "\n";
	for( auto it = val.alpha_values.begin(); it != val.alpha_values.end(); it++ ) {
		in << *it << "\n";
	}
	if ( val.alpha_model == ASHAPE_ALPHAVALUE_WRAPPINGS ) {
		in << "Offset values " << "\n";
		for( auto jt = val.offset_values.begin(); jt != val.offset_values.end(); jt++ ) {
			in << *jt << "\n";
		}
	}
	if ( val.hasExtFacets == true )
		in << "AlphaShape has exterior facets " << "yes" << "\n";
	else
		in << "AlphaShape has exterior facets " << "no" << "\n";
	if ( val.hasExtFacetsPolyhedronCheck == true )
		in << "AlphaShape has exterior facet polyhedron checked for self-intersections and closure " << "yes" << "\n";
	else
		in << "AlphaShape has exterior facet polyhedron checked for self-intersections and closure " << "no" << "\n";
	if ( val.hasIntTetrahedra == true )
		in << "AlphaShape has interior tetrahedra " << "yes" << "\n";
	else
		in << "AlphaShape has interior tetrahedra " << "no" << "\n";
	if ( val.hasFacetAppearanceAlphas == true )
		in << "AlphaShape has vector of alpha values when facets appear " << "yes" << "\n";
	else
		in << "AlphaShape has vector of alpha values when facets appear " << "no" << "\n";

	return in;
}


string ConfigSurfacer::InputfileDataset = "";
string ConfigSurfacer::ReconstructionDatasetName = "";
string ConfigSurfacer::MassToChargeDatasetName = "";
string ConfigSurfacer::InputfileIonTypes = "";
string ConfigSurfacer::IonTypesGroupName = "";

WINDOWING_METHOD ConfigSurfacer::WindowingMethod = ENTIRE_DATASET;
vector<roi_sphere> ConfigSurfacer::WindowingSpheres = vector<roi_sphere>();
vector<roi_rotated_cylinder> ConfigSurfacer::WindowingCylinders = vector<roi_rotated_cylinder>();
vector<roi_rotated_cuboid> ConfigSurfacer::WindowingCuboids = vector<roi_rotated_cuboid>();
vector<roi_polyhedron> ConfigSurfacer::WindowingPolyhedra = vector<roi_polyhedron>();
lival<apt_uint> ConfigSurfacer::LinearSubSamplingRange = lival<apt_uint>( 0, 1, UMX );
match_filter<unsigned char> ConfigSurfacer::IontypeFilter = match_filter<unsigned char>();
match_filter<unsigned char> ConfigSurfacer::HitMultiplicityFilter = match_filter<unsigned char>();

ALPHASHAPE_QUANTIFICATION_METHOD ConfigSurfacer::AlphaShapeSelectionMethod = ASHAPE_NOTHING;
EDGE_QUANTIFICATION_METHOD ConfigSurfacer::EdgeQuantificationMethod = SURFACE_NOTHING;
apt_int ConfigSurfacer::EdgeDetectionKernelWidth = 1;

vector<surface_detection_task> ConfigSurfacer::AlphaShapeTasks = vector<surface_detection_task>();

apt_uint ConfigSurfacer::NumberOfConnectedComponents = 1;
lival<apt_real> ConfigSurfacer::AdvIonPruningBinWidthRange = lival<apt_real>( MYONE, MYONE, MYONE ); //1nm practice guess useful for many cases
apt_real ConfigSurfacer::RefinementDensityControlFactor = 2.;


bool ConfigSurfacer::read_config_from_nexus( const string nx5fn )
{
	if ( ConfigShared::SimID > 0 ) {
		cout << "ConfigSurfacer::" << "\n";
		cout << "Reading configuration from " << nx5fn << "\n";

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( nx5fn );
		string grpnm = "";
		string dsnm = "";

		//##MK::currently this tool assumes and works only with a single analysis task defined

		apt_uint number_of_processes = 1;
		apt_uint entry_id = 1;

		for ( apt_uint tskid = 0; tskid < number_of_processes; tskid++ ) {
			//##MK::currently this tool assumes and works only with a single analysis task defined
			cout << "Reading configuration for task " << tskid << "\n";
			//apt_uint proc_id = tskid + 1;

			grpnm = "/entry" + to_string(entry_id) + "/surface_meshing";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string path = "";
				if ( h5r.nexus_read( grpnm + "/reconstruction/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileDataset = path_handling( path );
				cout << "InputfileDataset " << InputfileDataset << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/position", ReconstructionDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "ReconstructionDatasetName " << ReconstructionDatasetName << "\n";
				if ( h5r.nexus_read( grpnm + "/reconstruction/mass_to_charge", MassToChargeDatasetName ) != MYHDF5_SUCCESS ) { return false; }
				cout << "MassToChargeDatasetName " << MassToChargeDatasetName << "\n";
				path = "";
				if ( h5r.nexus_read( grpnm + "/ranging/path", path ) != MYHDF5_SUCCESS ) { return false; }
				InputfileIonTypes = path_handling( path );
				cout << "InputfileIonTypes " << InputfileIonTypes << "\n";
				if ( h5r.nexus_read( grpnm + "/ranging/ranging_definitions", IonTypesGroupName ) != MYHDF5_SUCCESS ) { return false; }
		    	cout << "IonTypesGroupName " << IonTypesGroupName << "\n";
			}

			AlphaShapeTasks = vector<surface_detection_task>();
			AlphaShapeTasks.push_back( surface_detection_task() );
			AlphaShapeTasks.back().taskid = tskid;

			//optional spatial filtering
			WindowingMethod = ENTIRE_DATASET;
			grpnm = "/entry" + to_string(entry_id) + "/surface_meshing/spatial_filter";
			string str = "";
			if ( h5r.nexus_read( grpnm + "/windowing_method", str ) != MYHDF5_SUCCESS ) { return false; }

			if ( str.compare("union_of_primitives") == 0 ) {
				//currently only UNION_OF_PRIMITIVES is implemented as a windowing method
				WindowingMethod = UNION_OF_PRIMITIVES;

				grpnm = "/entry" + to_string(entry_id) + "/surface_meshing/spatial_filter/ellipsoid_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_ellipsoids = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_ellipsoids ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_ellipsoids > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> half_axes;
						if ( h5r.nexus_read( grpnm + "/half_axes_radii", half_axes ) != MYHDF5_SUCCESS ) { return false; }
						//orientation is not read because ellipsoids are here assumed as spheres
						if ( center.size() / 3 == n_ellipsoids && half_axes.size() / 3 == n_ellipsoids ) {
							for ( apt_uint i = 0; i < n_ellipsoids; i++ ) {
								WindowingSpheres.push_back( roi_sphere(
									p3d(center[3*i+0], center[3*i+1], center[3*i+2]), half_axes[3*i+0]) );
							}
						}
						center = vector<apt_real>();
						half_axes = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/surface_meshing/spatial_filter/cylinder_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_cylinders = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_cylinders ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_cylinders > 0 ) {
						vector<apt_real> center;
						if ( h5r.nexus_read( grpnm + "/center", center ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> height;
						if ( h5r.nexus_read( grpnm + "/height", height ) != MYHDF5_SUCCESS ) { return false; }
						vector<apt_real> radii;
						if ( h5r.nexus_read( grpnm + "/radii", radii ) != MYHDF5_SUCCESS ) { return false; }
						if ( center.size() / 3 == n_cylinders &&
								height.size() / 3 == n_cylinders &&
									radii.size() == n_cylinders ) {
							for ( apt_uint i = 0; i < n_cylinders; i++ ) {
								WindowingCylinders.push_back( roi_rotated_cylinder(
										p3d(center[3*i+0] - 0.5*height[3*i+0],
											center[3*i+1] - 0.5*height[3*i+1],
											center[3*i+2] - 0.5*height[3*i+2]),
										p3d(center[3*i+0] + 0.5*height[3*i+0],
											center[3*i+1] + 0.5*height[3*i+1],
											center[3*i+2] + 0.5*height[3*i+2]),
										radii[i] ) );
							}
						}
						center = vector<apt_real>();
						height = vector<apt_real>();
						radii = vector<apt_real>();
					}
				}

				grpnm = "/entry" + to_string(entry_id) + "/surface_meshing/spatial_filter/hexahedron_set";
				if ( h5r.nexus_path_exists( grpnm ) == true ) {
					apt_uint n_hexahedra = 0;
					if ( h5r.nexus_read( grpnm + "/cardinality", n_hexahedra ) != MYHDF5_SUCCESS ) { return false; }
					if ( n_hexahedra > 0 ) {
						vector<apt_real> vrts;
						if ( h5r.nexus_read( grpnm + "/hexahedra/vertices", vrts ) != MYHDF5_SUCCESS ) { return false; }
						if ( vrts.size() / (8 * 3) == n_hexahedra ) {
							for ( apt_uint i = 0; i < n_hexahedra; i++ ) {
								vector<p3d> corners;
								for ( int j = 0; j < 8; j++ ) {
									corners.push_back( p3d(
											vrts[8*3*i+3*j+0], vrts[8*3*i+3*j+1], vrts[8*3*i+3*j+2]) );
								}
								WindowingCuboids.push_back( roi_rotated_cuboid( corners ) );
								corners = vector<p3d>();
							}
						}
						vrts = vector<apt_real>();
					}
				}

				cout << "Analyzed union_of_primitives" << "\n";
				cout << "WindowingSpheres.size() " << WindowingSpheres.size() << "\n";
				cout << "WindowingCylinders.size() " << WindowingCylinders.size() << "\n";
				cout << "WindowingCuboids.size() " << WindowingCuboids.size() << "\n";
			}
			//bitmask not implemented

			//optional sub-sampling filter for ion/evaporation ID
			grpnm = "/entry" + to_string(entry_id) + "/surface_meshing/evaporation_id_filter";
			if( h5r.nexus_path_exists( grpnm ) == true ) {
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/min_incr_max", uint ) != MYHDF5_SUCCESS ) { return false; }
				if ( uint.size() == 3 ) {
					LinearSubSamplingRange = lival<apt_uint>( uint[0], uint[1], uint[2] );
				}
			}
			cout << LinearSubSamplingRange << "\n";

			//optional match filter for iontypes
			grpnm = "/entry" + to_string(entry_id) + "/surface_meshing/iontype_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				IontypeFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << IontypeFilter << "\n";

			//optional match filter for hit multiplicity
			grpnm = "/entry" + to_string(entry_id) + "/surface_meshing/hit_multiplicity_filter";
			if ( h5r.nexus_path_exists( grpnm ) == true ) {
				string filter_kind = "";
				if ( h5r.nexus_read( grpnm + "/method", filter_kind ) != MYHDF5_SUCCESS ) { return false; }
				vector<apt_uint> uint;
				if ( h5r.nexus_read( grpnm + "/match", uint ) != MYHDF5_SUCCESS ) { return false; }
				vector<unsigned char> lst;
				for( auto it = uint.begin(); it != uint.end(); it++ ) {
					if ( *it < 256 ) {
						lst.push_back( (unsigned char) (int) *it );
					}
				}
				HitMultiplicityFilter = match_filter<unsigned char>( lst, filter_kind );
			}
			cout << HitMultiplicityFilter << "\n";

			grpnm = "/entry" + to_string(entry_id) + "/surface_meshing";
			string preprocessing = "";
			if ( h5r.nexus_read( grpnm + "/preprocessing/method", preprocessing ) != MYHDF5_SUCCESS ) { return false; }
			if ( preprocessing.compare("default") == 0 ) {
				EdgeQuantificationMethod = SURFACE_ALPHASHAPE_NAIVE;
			}
			else if ( preprocessing.compare("kuehbach") == 0) {
				EdgeQuantificationMethod = SURFACE_ALPHASHAPE_FAST;
			}
			else {
				EdgeQuantificationMethod = SURFACE_NOTHING;
			}
			cout << "EdgeQuantificationMethod " << EdgeQuantificationMethod << "\n";

			apt_int kernel_width = 1;
			if ( h5r.nexus_read( grpnm + "/preprocessing/kernel_width", kernel_width ) != MYHDF5_SUCCESS ) { return false; }
			if ( kernel_width < 1 ) {
				kernel_width = 1;
			}
			EdgeDetectionKernelWidth = kernel_width;
			cout << "EdgeDetectionKernelWidth " << EdgeDetectionKernelWidth << "\n";
			//##MK::should be unsigned int !

			AlphaShapeTasks.back().edge_quant_type = EdgeQuantificationMethod;
			AlphaShapeTasks.back().edge_kernel_width = EdgeDetectionKernelWidth;

			//alpha-value choices
			grpnm = "/entry" + to_string(entry_id) + "/surface_meshing";
			string alpha_model = "convex_hull_naive";
			if ( h5r.nexus_read( grpnm + "/alpha_value_choice", alpha_model ) != MYHDF5_SUCCESS ) { return false; }
			if ( alpha_model.compare("convex_hull_naive") == 0 ) {
				AlphaShapeSelectionMethod = CONVEX_HULL_NAIVE;
			}
			else if ( alpha_model.compare("convex_hull_refine") == 0 ) {
				AlphaShapeSelectionMethod = CONVEX_HULL_REFINE;
			}
			else if ( alpha_model.compare("smallest_solid") == 0 ) {
				AlphaShapeSelectionMethod = ASHAPE_SMALLEST_SOLID;
			}
			else if ( alpha_model.compare("cgal_optimal") == 0 ) {
				AlphaShapeSelectionMethod = ASHAPE_CGAL_OPTIMAL;
			}
			else if ( alpha_model.compare("set_of_values") == 0 ) {
				AlphaShapeSelectionMethod = ASHAPE_ALPHAVALUE_ENSEMBLE;
			}
			else if ( alpha_model.compare("set_of_alpha_wrappings") == 0 ) {
				AlphaShapeSelectionMethod = ASHAPE_ALPHAVALUE_WRAPPINGS;
			}
			else {
				AlphaShapeSelectionMethod = ASHAPE_NOTHING;
			}
			AlphaShapeTasks.back().alpha_model = AlphaShapeSelectionMethod;
			cout << "AlphaShapeSelectionMethod " << AlphaShapeSelectionMethod << "\n";

			if ( AlphaShapeSelectionMethod == ASHAPE_ALPHAVALUE_ENSEMBLE || AlphaShapeSelectionMethod == ASHAPE_ALPHAVALUE_WRAPPINGS ) {
				//squared radius of carving spoon, alpha --> infty gives convex hull
				AlphaShapeTasks.back().alpha_values = vector<apt_real>();
				vector<apt_real> real;
				if ( h5r.nexus_read( grpnm + "/alpha_values", real ) != MYHDF5_SUCCESS ) { return false; }
				for( auto it = real.begin(); it != real.end(); it++ ) {
					if ( *it >= EPSILON ) {
						AlphaShapeTasks.back().alpha_values.push_back(*it);
					}
					else {
						cout << "WARNING::Ignoring alpha value " << *it << " because its smaller than " << EPSILON << " !" << "\n";
					}
				}
				real = vector<apt_real>();

				//offset_values in case of alpha_wrappings
				if ( AlphaShapeSelectionMethod == ASHAPE_ALPHAVALUE_WRAPPINGS ) {
					AlphaShapeTasks.back().offset_values = vector<apt_real>();
					if ( h5r.nexus_read( grpnm + "/offset_values", real ) != MYHDF5_SUCCESS ) { return false; }
					for( auto it = real.begin(); it != real.end(); it++ ) {
						if ( *it >= EPSILON ) {
							AlphaShapeTasks.back().offset_values.push_back(*it);
						}
						else {
							cout << "WARNING::Ignoring offset value " << *it << " because its smaller than " << EPSILON << " !" << "\n";
						}
					}
					real = vector<apt_real>();
					if ( AlphaShapeTasks.back().alpha_values.size() != AlphaShapeTasks.back().offset_values.size() ) {
						cerr << "The length of the alpha and the offset values arrays need to be the same for alpha wrappings!" << "\n";
						return false;
					}
				}
			}

			unsigned char option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_exterior_facets", option ) != MYHDF5_SUCCESS ) { return false; }
			AlphaShapeTasks.back().hasExtFacets = ( option == 0x01 ) ? true : false;
			cout << "has_exterior_facets " << (int) option << "\n";

			option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_closure", option ) != MYHDF5_SUCCESS ) { return false; }
			AlphaShapeTasks.back().hasExtFacetsPolyhedronCheck = ( option == 0x01 ) ? true : false;
			cout << "has_closure " << (int) option << "\n";

			option = 0x00;
			if ( h5r.nexus_read( grpnm + "/has_interior_tetrahedra", option ) != MYHDF5_SUCCESS ) { return false; }
			AlphaShapeTasks.back().hasIntTetrahedra = ( option == 0x01 ) ? true : false;
			cout << "has_interior_tetrahedra " << (int) option << "\n";

			AlphaShapeTasks.back().hasFacetAppearanceAlphas = false;

			cout << AlphaShapeTasks.back() << "\n";
		}

		cout << "NumberOfConnectedComponents " << NumberOfConnectedComponents << "\n";
		cout << "AdvIonPruningBinWidthRange " << AdvIonPruningBinWidthRange << "\n";
		cout << "RefinementDensityControlFactor " << RefinementDensityControlFactor << "\n";

		return true;
	}

	//special developer case SimID == 0

	return false;
}
