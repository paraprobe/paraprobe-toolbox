/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef __PARAPROBE_CONFIG_SURFACER_H__
#define __PARAPROBE_CONFIG_SURFACER_H__

#include "../../../utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"


enum EDGE_QUANTIFICATION_METHOD {
	SURFACE_NOTHING,
	SURFACE_ALPHASHAPE_FAST,				//needs the CGAL library, using M. Kuehbach et al. method for pruning ions in the interior of the data to speed up the computation
	SURFACE_ALPHASHAPE_NAIVE				//needs the CGAL library, pass naively all points to CGAL when computing an alpha shape
};


enum ALPHASHAPE_QUANTIFICATION_METHOD {
	ASHAPE_NOTHING,
	CONVEX_HULL_NAIVE,						//compute only the convex hull for which a specialized implementation is available
	CONVEX_HULL_REFINE,						//compute only the convex hull for which a specialized implementation is available and refine the mesh
	ASHAPE_SMALLEST_SOLID,					//which alpha to use for alpha shape
	ASHAPE_CGAL_OPTIMAL,					//what CGAL considers as optimal, check https://doc.cgal.org/latest/Alpha_shapes_3/classCGAL_1_1Alpha__shape__3.html#accd959f5316156b0c9cd425a7ad073ac
	ASHAPE_ALPHAVALUE_ENSEMBLE,				//probe systematically a range of alpha values
	ASHAPE_ALPHAVALUE_WRAPPINGS				//probe systematically a range of alpha wrappings
};


struct surface_detection_task
{
	apt_uint taskid;						//task ID
	apt_uint edge_quant_type;				//enum value which edge quantification method
	apt_int edge_kernel_width;				//kernel width for edge quantification
	apt_uint alpha_model;					//how was the alpha value chosen from a collection of alpha complexes
	vector<apt_real> alpha_values;			//actual alpha values
	vector<apt_real> offset_values;			//actual offset values (only for alpha wrappings), same length as alpha values
	bool hasExtFacets;
	bool hasExtFacetsPolyhedronCheck;
	bool hasIntTetrahedra;
	bool hasFacetAppearanceAlphas;

	surface_detection_task() : taskid(UMX),
			edge_quant_type(SURFACE_ALPHASHAPE_FAST), edge_kernel_width(1),
			alpha_model(ASHAPE_SMALLEST_SOLID),
			alpha_values(vector<apt_real>()), offset_values(vector<apt_real>()),
			hasExtFacets(false), hasExtFacetsPolyhedronCheck(false),
			hasIntTetrahedra(false), hasFacetAppearanceAlphas(false) {}
};

ostream& operator << (ostream& in, surface_detection_task const & val);


class ConfigSurfacer
{
public:
	static bool read_config_from_nexus( const string nx5fn );

	static string InputfileDataset;
	static string ReconstructionDatasetName;
	static string MassToChargeDatasetName;
	static string InputfileIonTypes;
	static string IonTypesGroupName;

	//add ROI filtering and sub-sampling functionalities
	static WINDOWING_METHOD WindowingMethod;
	static vector<roi_sphere> WindowingSpheres;
	static vector<roi_rotated_cylinder> WindowingCylinders;
	static vector<roi_rotated_cuboid> WindowingCuboids;
	static vector<roi_polyhedron> WindowingPolyhedra;
	//sub-sampling
	static lival<apt_uint> LinearSubSamplingRange;
	static match_filter<unsigned char> IontypeFilter;
	static match_filter<unsigned char> HitMultiplicityFilter;

	static ALPHASHAPE_QUANTIFICATION_METHOD AlphaShapeSelectionMethod;
	static EDGE_QUANTIFICATION_METHOD EdgeQuantificationMethod;
	static apt_int EdgeDetectionKernelWidth;

	static vector<surface_detection_task> AlphaShapeTasks;

	//sensible defaults
	static apt_uint NumberOfConnectedComponents;
	static lival<apt_real> AdvIonPruningBinWidthRange;
	static apt_real RefinementDensityControlFactor;
};

#endif
