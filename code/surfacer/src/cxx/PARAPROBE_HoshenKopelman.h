/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SURFACER_HOSHENKOPELMAN_H__
#define __PARAPROBE_SURFACER_HOSHENKOPELMAN_H__

#include "PARAPROBE_SurfacerXDMF.h"

#define IS_VACUUM						0x00
#define IS_BOUNDARY						0x01
#define IS_INSIDE						0xFF


//data structure for path/compressed union-find
class UF
{
	//##MK::potentially encapsulate
public:
	// Create an empty union find data structure with N isolated sets.
	UF() : id(NULL), sz(NULL), stillgood(false) {}
	UF( const apt_uint N );
	~UF();

	apt_uint initialAssgn( const apt_uint i);
	apt_uint find_root(apt_uint p);
	apt_uint merge( apt_uint x, apt_uint y);
	bool still_good( void );
	
	apt_uint* id;
	apt_uint* sz;

private:
	bool stillgood;
};


struct hk_info
{
	apt_uint ncluster;
	apt_uint largest_npoints;
	apt_uint largest_id;
	double dt_initializing;
	double dt_kopeling;
	double dt_compactifying;
	double dt_checking;
	double dt_characterizing;
	hk_info() : ncluster(0), largest_npoints(0), largest_id(UMX),
			dt_initializing(MYZERO), dt_kopeling(MYZERO), dt_compactifying(MYZERO),
				dt_checking(MYZERO), dt_characterizing(MYZERO) {}
};

ostream& operator<<(ostream& in, hk_info const & val);


class percolationAnalyzer
{
public:
	percolationAnalyzer();
	~percolationAnalyzer();

	void set_gridsize( const apt_uint nx, const apt_uint ny, const apt_uint nz );
	bool initialize( const bool* inputdata, const apt_uint nxx, const apt_uint nyy, const apt_uint nzz );
	void hk3d_core_nonperiodic( const apt_uint x, const apt_uint y, const apt_uint z );
	bool hoshen_kopelman();
	bool compactify();
	bool check_labeling();
	bool determine_clustersize_distr();
	bool rebinarize( const apt_uint target, const apt_uint nsz, bool* bitmap );

	apt_uint* idd;
	UF* finder;

	apt_uint NX;
	apt_uint NY;
	apt_uint NZ;
	apt_uint NXY;
	apt_uint NXYZ;

	hk_info info;
};


#endif
