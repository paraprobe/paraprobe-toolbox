/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_OutsideInsideMask.h"


oiMask::oiMask()
{
	gridinfo = voxelgrid();
	kernelwidth = 1;

	IsOutside = NULL;
	IsInside = NULL;
	IsSurface = NULL;
}


oiMask::~oiMask()
{
	delete [] IsOutside; IsOutside = NULL;
	delete [] IsInside; IsInside = NULL;
	delete [] IsSurface; IsSurface = NULL;
}


void oiMask::init_mask3d( aabb3d const & box, const apt_real edgelength, const apt_int edgesize  )
{
	if ( IsOutside == NULL && IsInside == NULL && IsSurface == NULL ) {

		gridinfo = voxelgrid( box, edgelength, edgesize );
		cout << gridinfo << "\n";
		if ( gridinfo.nxyz > 0 ) {
			//allocate the masks
			try {
				IsOutside = new bool[gridinfo.nxyz];
			}
			catch(bad_alloc &croak) {
				cerr << "Allocation of IsOutside binary mask failed !" << "\n";
				return;
			}
			try {
				IsInside = new bool[gridinfo.nxyz];
			}
			catch(bad_alloc &croak) {
				cerr << "Allocation of IsSurface binary container failed!" << "\n";
				delete [] IsOutside; IsOutside = NULL;
				return;
			}
			try {
				IsSurface = new bool[gridinfo.nxyz];
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of IsInside binary container failed!" << "\n";
				delete [] IsOutside; IsOutside = NULL;
				delete [] IsInside; IsInside = NULL;
				return;
			}

			//initialize the masks
			size_t ni = (size_t) gridinfo.nxyz;
			for( size_t i = 0; i < ni; i++ ) {
				IsOutside[i] = false;
				IsInside[i] = false;
				IsSurface[i] = false;
			}
		}
		else {
			cerr << "Initializing outsideInsideMask failed, gridsize is 0 !" << "\n";
			return;
		}

		kernelwidth = edgesize;
		if ( gridinfo.nx < 1+2*kernelwidth || gridinfo.ny < 1+2*kernelwidth || gridinfo.nz < 1+2*kernelwidth ) {
			cerr << "Kernel is too large for the given grid !" << "\n";
			return;
		}
	}
	else {
		cerr << "Initializing mask failed, it seems to exist already !" << "\n";
		return;
	}
}


bool oiMask::identify_outside_voxels()
{
	//MK::pruning strategy, hole filling strategy
	//now we perform a HoshenKopelman percolation analysis on IsVacuum to find the outer percolating shell of false's culling the tip and at the same time
	//identify any potential percolating cluster or individual arbitrarily arranged flicker noise of individual bins physically inside the tip but
	//with the given resolution too small a bin size that it is likely to find any ion inside (lateral resolution, detector efficiency)
	//knowing that the AABB about the tip was extended by a guardzone surplus the fact that APT tips do not touch the upper corner voxel at the AABB in particular
	//not the one in the guardzone we can now include the following:
	//all embedded flicker and separate from the non-connected culling outer vacuum cluster with id as that of the
	//upper corner guardzone voxel label ID and update bitmap1 excluding the guardzone
	//when we are sure that there are no isolated arbitrarily false voxel cluster inside the tip and we can safely now the Moore based surface patch identification

	//##MK::NUMA memory issues! this analyzer is at the moment running in the memory of the master thread only!
	//##empirical evidence so far even for 0.5nm binning and 1 billion ions though showed that total core time taken
	//is too insignificant to be a prime target for code optimization...

	percolationAnalyzer hk;

	if ( gridinfo.nx < 1 || gridinfo.nx >= IMX || gridinfo.ny < 1 || gridinfo.ny >= IMX || gridinfo.nz < 1 || gridinfo.nz >= IMX  ) {
		cerr << "oiMask number of voxels exceeds implemented dimensions !" << "\n";
		return false;
	}

	//now the cast from size_t to unsigned int is safe
	apt_uint nx = (apt_uint) gridinfo.nx;
	apt_uint ny = (apt_uint) gridinfo.ny;
	apt_uint nz = (apt_uint) gridinfo.nz;
	apt_uint nxy = (apt_uint) gridinfo.nxy;
	apt_uint nxyz = (apt_uint) gridinfo.nxyz;

	//pass binarized ion occupancy bin bitfield to the HK analyzer to perform clustering analysis as one would do for percolation studies
	if ( hk.initialize( IsOutside, nx, ny, nz) == false ) {
		cerr << "HoshenKopelman initialization failed !" << "\n";
		return false;
	}

	if ( hk.hoshen_kopelman() == false ) {
		cerr << "HoshenKopelman run failed during binning attempt !" << "\n";
		return false;
	}

	if ( hk.compactify() == false ) {
		cerr << "HoskenKopelman label compactification failed during binning attempt !" << "\n";
		return false;
	}

	//##MK::activate label checking !
	//if ( hk->checkLabeling() == false ) {
	//	stopping( "HoshenKopelman found labeling inconsistency");
	//	delete hk; return false;
	//}

	if ( hk.determine_clustersize_distr() == false ) {
		cerr << "HoshenKopelman cluster size distribution failed during binning attempt !" << "\n";
		return false;
	}

	//if not yet returned everything worked out so far so
	//MK::find HK label of frontmost top left voxel which is guaranteed to be in the guardzone because we added +1 guard on each side
	//and hence has no ion we can use the ID of this bin to reassign which bins are vacuum and which not
	apt_uint tfl_corner_b = 0 + 0*nx + (nz-1)*nxy;
	//dont take ghost voxel 0, 0, 0 here because its label will always be 0 !! see HK core and case of 0,0,0 which gets the initial label

	if ( hk.rebinarize( tfl_corner_b, nxyz, IsOutside ) == false ) {
		cerr << "HoshenKopelman rebinarization attempt failed !" << "\n";
		return false;
		//##MK::clarify here the description true for bins in vacuum and false for tip
	}

	cout << "HoshenKopelman clustering analysis was successful" << "\n";
	return true;
}


void oiMask::identify_surface_adjacent_voxels()
{
	//IsVacuum is true if bin is part of the vacuum cluster enclosing the tip, these are bins that do not containing an ion
	if ( gridinfo.nx < 1 || gridinfo.nx >= IMX || gridinfo.ny < 1 || gridinfo.ny >= IMX || gridinfo.nz < 1 || gridinfo.nz >= IMX  ) {
		cerr << "oiMask number of voxels exceeds implemented dimensions !" << "\n";
		return;
	}

	//now the cast from size_t to unsigned int is safe
	apt_int nx = gridinfo.nx;
	apt_int ny = gridinfo.ny;
	apt_int nz = gridinfo.nz;
	apt_int nxy = gridinfo.nxy;
	apt_int nxyz = gridinfo.nxyz;

	//MK::assume first each bin is not part of the surface, i.e. initialize...
	for ( apt_int b = 0; b < nxyz; ++b ) {
		IsSurface[(size_t) b] = false;
	}

	//now reset IsSurface to true only if any of the bins' Moore neighbors IsVacuum[i] == false...
	//query in Moore environment which bins have not all Moore neighbors trues ie. are not deep in tip (occupied with points)
	//MK::remember that IsVacuum is true if this is a bin belonging to the large cluster contacted to vacuum and false only if it is some bin in the tip even if isolated and empty inside the tip volume this is the trick!
	//start querying at 1 because guardzone is definately not in tip surplus we need to probe non-periodic Moore neighbors

	for ( apt_int bz = kernelwidth; bz < (nz - kernelwidth); ++bz ) {
		apt_uint bzoff = (apt_uint) bz * (apt_uint) nxy;
		for ( apt_int by = kernelwidth; by < (ny - kernelwidth); ++by ) {
			apt_uint byzoff = (apt_uint) by * (apt_uint) nx + bzoff;
			for ( apt_int bx = kernelwidth; bx < (nx - kernelwidth); ++bx ) {
				apt_uint here = (apt_uint) bx + byzoff;

				if( IsOutside[here] == true ) { //MK::two bitmaps to avoid successive overwriting and data invalidating image buffer1 as input and buffer2 as output
					for( apt_int kz = -1*kernelwidth; kz <= +1*kernelwidth; kz++ ) {
						apt_uint izoff = (apt_uint) (bz+kz) * (apt_uint) nxy;
						for( apt_int ky = -1*kernelwidth; ky <= +1*kernelwidth; ky++ ) {
							apt_uint iyzoff = (apt_uint) (by+ky) * (apt_uint) nx + izoff;
							for ( apt_int kx = -1*kernelwidth; kx <= +1*kernelwidth; kx++ ) {
								//edge checks can be improved, see CA implementation M. Kuehbach et al. Acta Materialia, A Statistical Cellular Automaton Model for RX
								apt_uint there = (apt_uint) (bx+kx) + iyzoff;
								if ( here != there ) { //exclude myself
									if(	IsOutside[there] == false )
										IsSurface[there] = true;
								}
							}
						}
					}

					/*
					//a bin in vacuum --- is it surrounded by nonempty bins i.e. bins that obviously intrude the tip volume?

					//MK::mind that access order such to improve cache temporal and spatial locality
					there = (bx-1)	+	(by-1)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by-1)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true; //MK::order optimized for cache reutilization using the implicit indexing x+y*nx+z*nxy
					there = (bx+1)	+	(by-1)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+0)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+0)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+0)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+1)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+1)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+1)	*nx	+	(bz-1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;

					there = (bx-1)	+	(by-1)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by-1)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by-1)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+0)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					//exclude myself
					there = (bx+1)	+	(by+0)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+1)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+1)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+1)	*nx	+	(bz+0)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;

					there = (bx-1)	+	(by-1)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by-1)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by-1)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+0)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+0)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+0)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx-1)	+	(by+1)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+0)	+	(by+1)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					there = (bx+1)	+	(by+1)	*nx	+	(bz+1)	*nxy;		if(	IsOutside[there] == false ) IsSurface[there] = true;
					*/
				} //evaluated the kernel
			} //next bin along +x
		} //next xline along +y
	} //next xyslab along +z
	cout << "Surface adjacent bins identified" << "\n";	
}


void oiMask::identify_inside_voxels()
{
	//get volume and count of bins entirely inside
	if ( gridinfo.nx < 1 || gridinfo.nx >= IMX || gridinfo.ny < 1 || gridinfo.ny >= IMX || gridinfo.nz < 1 || gridinfo.nz >= IMX  ) {
		cerr << "oiMask number of voxels exceeds implemented dimensions !" << "\n";
		return;
	}

	apt_uint nxyz = (apt_uint) gridinfo.nxyz;

	//MK::IsOutside is true if bin belonging to the large cluster contacted to vacuum and false if some bin in the tip even if isolated and empty inside the tip volume this is the trick!
	//MK::IsSurface by now is true if a bin to consider containing candidate ions to use in tip surface reconstruction
	//hence if IsVacuum == false && (in tip potentially surfacecontact) and IsSurface == false (excluding surface contact) inside
	for ( apt_uint b = 0; b < nxyz; ++b ) {
		if (IsOutside[b] == false && IsSurface[b] == false)
			IsInside[b] = true;
		else
			IsInside[b] = false;
	}
	cout << "Inside bins identified" << "\n";
}
