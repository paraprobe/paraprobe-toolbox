/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SURFACER_OUTSIDE_INSIDE_MASK_H__
#define __PARAPROBE_SURFACER_OUTSIDE_INSIDE_MASK_H__

#include "PARAPROBE_HoshenKopelman.h"


class oiMask
{
	//the class is a utility tool which holds a voxelgrid and marks which voxels represent positions outside of a closed contoured object
	//positions inside the object at some distance from the boundary of the contour and points somewhat close to the contour
public:
	oiMask();
	~oiMask();
	
	void init_mask3d( aabb3d const & box, const apt_real edgelength, const apt_int edgesize );
	bool identify_outside_voxels();
	/*
	void identify_outside_voxels_withmoore_nonoutside_contact();
	*/
	void identify_surface_adjacent_voxels();
	void identify_inside_voxels();
	
	voxelgrid gridinfo;
	apt_int kernelwidth;
	bool* IsOutside;	//a voxel with no points laying outside the contour
	bool* IsInside;		//a voxel with points laying inside the contour
	bool* IsSurface;	//a voxel for all others, here written out explicitly
};

#endif
