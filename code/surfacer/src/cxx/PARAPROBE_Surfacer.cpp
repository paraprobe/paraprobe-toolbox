/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


#include "PARAPROBE_SurfacerHdl.h"


bool parse_configuration_from_nexus()
{
	tool_startup( "paraprobe-surfacer" );
	if ( ConfigSurfacer::read_config_from_nexus( ConfigShared::ConfigurationFile ) == true ) {
		cout << "Configuration from file " << ConfigShared::ConfigurationFile << " was accepted" << "\n";
		cout << "This analysis has SimulationID " << "SimID." <<  ConfigShared::SimID << "\n";
		cout << "Results of this analysis are written to " << ConfigShared::OutputfileName << "\n";
		cout << endl;
		return true;
	}
	cerr << "Parsing configuration failed !" << "\n";
	return false;
}


void edge_quantification( const int r, const int nr )
{
	//allocate process-level instance of a surfacerHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	surfacerHdl* srf = NULL;
	int localhealth = 1;
	try {
		srf = new surfacerHdl;
		srf->set_myrank(r);
		srf->set_nranks(nr);
		//srf->commit_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " allocating surfacerHdl class object failed !" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	if ( srf->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete srf; srf = NULL; return;
	}

	if ( ConfigSurfacer::EdgeQuantificationMethod != SURFACE_NOTHING ) {
		if ( srf->get_myrank() == MASTER ) {

			if ( srf->read_relevant_input() == false ) {
				cout << "Rank " << srf->get_myrank() << " failed reading essential pieces of the relevant input !" << "\n";
				localhealth = 0;
			}
		}

		//##MK::strictly speaking not necessary, but second order issue for about 80 processes as on TALOS...?
		//MPI_Barrier( MPI_COMM_WORLD );
		if ( srf->all_healthy_still( localhealth ) == false ) {
			cerr << "Rank " << srf->get_myrank() << " has recognized that not all data have arrived at the master !" << "\n";
			delete srf; srf = NULL; return;
		}

		if ( srf->crop_reconstruction_to_analysis_window() == true ) {

			//apt_uint ashpID = 0;
			for ( auto ashp_tskit = ConfigSurfacer::AlphaShapeTasks.begin(); ashp_tskit != ConfigSurfacer::AlphaShapeTasks.end(); ashp_tskit++ ) {

				/*
				//assume first we work with the entire reconstruction, hence the ions.ionifo.mask1 is ANALYZE_YES for all
				if ( ashp_tskit->roitype == ENTIRE_DATASET ) {
					srf->apply_all_filter();
				}
				else if ( ashp_tskit->roitype == COMPOSITE_REGION ) { //##MK::debug implementation
					srf->apply_none_filter();
					//define which ions to include as a union of different geometric primitives
					srf->apply_roi_ensemble_accept_filter( CG_CUBOID, ConfigSurfacer::WindowingCuboids );
					srf->apply_roi_ensemble_accept_filter( CG_CYLINDER, ConfigSurfacer::WindowingCylinders );
					srf->apply_roi_ensemble_accept_filter( CG_SPHERE, ConfigSurfacer::WindowingSpheres );
				}
				else {
					cerr << "Facing a yet unimplemented region-of-interest filtering mode !" << "\n";
					break;
				}

				//successively remove points from the analysis window that meet or not certain properties, like position, associated iontype, multiplicity, etc.
				ConfigSurfacer::LinearSubSamplingRange.min = 0;
				//leave the user-defined sub-sampling increment unchanged
				ConfigSurfacer::LinearSubSamplingRange.max = srf->ions.ionpp3.size();

				if ( ConfigSurfacer::LinearSubSamplingRange.min >= 0 &&
						ConfigSurfacer::LinearSubSamplingRange.incr > 1 &&
							ConfigSurfacer::LinearSubSamplingRange.max <= srf->ions.ionpp3.size() ) { //only when evaporationID constraint or incr > 1

					srf->apply_linear_subsampling_filter( ConfigSurfacer::LinearSubSamplingRange );
				}

				//##MK::currently we can only process different alpha values for a given filter setting
				//if ( ashp_tskit->roitype != COMPOSITE_REGION ) {
				//	srf->apply_iontype_remove_filter( ConfigSurfacer::AlphaShapeTasks.back().itypes_blacklist );
				//}

				srf->check_filter();

				ashp_tskit->taskid = ashpID;
				//ashp_tskit->linspace = ConfigSurfacer::LinearSubSamplingRange;
				//do not reset itypes, itypes_whitelist, and itypes_blacklist, alpha_model, alpha_value, hasVolume, hasTetrahedra
				*/

				cout << "Characterizing edge of the dataset" << "\n";
				cout << *ashp_tskit << "\n";

				srf->compute_dataset_edge( *ashp_tskit );

				/*
				if ( srf->get_myrank() == MASTER ) {


					if ( srf->write_alphashape_task_info_h5( *ashp_tskit, srf->rng ) == true ) {
						//
					}
					else {
						cerr << "Writing info results for alpha-shaping task " << ashp_tskit->taskid << " failed !" << "\n";
					}
					if ( ashp_tskit->alpha_model != ASHAPE_ALPHAVALUE_ENSEMBLE ) {
						if ( srf->write_alphashape_single_geometry( *ashp_tskit, 0 ) == true ) {
							//nothing to do
						}
						else {
							cerr << "Writing geometry results for alpha-shaping task " << ashp_tskit->taskid << " failed !" << "\n";
						}
					}
					else {
						if ( srf->write_alphashape_set_geometry( *ashp_tskit ) == true ) {
							//
						}
						else {
							cerr << "Writing geometry results for alpha-shaping task " << ashp_tskit->taskid << " failed !" << "\n";
						}
					}
				}
				*/

				srf->ashp.clear();

				//ashpID++;
			} //next alpha shaping task
		}
	}

	//release resources
	delete srf; srf = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	string start_time = timestamp_now_iso8601();

	if ( argc == 1 || argc == 2 ) {
		string cmd_option = "--help";
		if ( argc == 2 ) { cmd_option = argv[1]; };
		command_line_help( cmd_option, "surfacer" );
		return 0;
	}
	else if ( argc == 3 ) {
		ConfigShared::SimID = stoul( argv[SIMID] );
		ConfigShared::ConfigurationFile = argv[CONTROLFILE];
		ConfigShared::OutputfilePrefix = "PARAPROBE.Surfacer.Results.SimID." + to_string(ConfigShared::SimID);
		ConfigShared::OutputfileName = ConfigShared::OutputfilePrefix + ".nxs";
		if ( init_results_nxs( ConfigShared::OutputfileName, "surfacer", start_time, 1 ) == false ) {
			return 0;
		}
	}
	else {
		return 0;
	}

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( parse_configuration_from_nexus() == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks !" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else {
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}

	if ( nr == SINGLEPROCESS ) {

		cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

//EXECUTE SPECIFIC TASK
		edge_quantification( r, nr );
	}
	else {
		cerr << "Rank " << r << " currently paraprobe-surfacer is implemented for a single process only !" << "\n";
	}

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	if ( finish_results_nxs( ConfigShared::OutputfileName, start_time, tic, 1 ) == true ) {
		cout << "paraprobe-surfacer success" << endl;
	}
	return 0;
}
