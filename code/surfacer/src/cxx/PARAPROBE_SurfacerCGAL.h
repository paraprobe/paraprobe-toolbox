/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef __PARAPROBE_SURFACER_CGAL_H__
#define __PARAPROBE_SURFACER_CGAL_H__

#include "PARAPROBE_ConfigSurfacer.h"

//##MK::for how to change to exact predicates and exact constructions see here
//https://doc.cgal.org/latest/Alpha_shapes_3/index.html#Alpha_shapes_3AlphaShape3OrFixedAlphaShape3

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

//alpha shapes
#include <CGAL/Alpha_shape_3.h>
#include <CGAL/Alpha_shape_cell_base_3.h>
#include <CGAL/Alpha_shape_vertex_base_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel 			K;
typedef CGAL::Alpha_shape_vertex_base_3<K>               				Vb;
typedef CGAL::Alpha_shape_cell_base_3<K>                 				Fb;
typedef CGAL::Triangulation_data_structure_3<Vb, Fb>      				Tds;
typedef CGAL::Delaunay_triangulation_3<K, Tds, CGAL::Fast_location>  	Delaunay;
typedef CGAL::Alpha_shape_3<Delaunay>                    				Alpha_shape_3;


typedef K::Point_3                                       				Point;
typedef Alpha_shape_3::Alpha_iterator                    				Alpha_iterator;
typedef Alpha_shape_3::NT                                				NT;



//support for another algorithm that is generally faster when computing convex hulls only
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/convex_hull_3.h>
typedef CGAL::Polyhedron_3<K>                    						Polyhedron;
typedef CGAL::Surface_mesh<Point>               						Surface;
typedef Polyhedron::Vertex_handle                         				Vertex_handle;
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/Polygon_mesh_processing/refine.h>
#include <CGAL/Polygon_mesh_processing/measure.h>


//support for alpha wrappings
#include <CGAL/alpha_wrap_3.h>

namespace PMP = CGAL::Polygon_mesh_processing;
namespace AW3 = CGAL::Alpha_wraps_3;


#endif


