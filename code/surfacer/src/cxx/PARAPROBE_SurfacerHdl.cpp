/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_SurfacerHdl.h"


surfacerHdl::surfacerHdl()
{
	oiMask ca = oiMask();
	ions_at_approximate_edge = vector<p3d>();
	window = vector<unsigned char>();
	ashp = alphaShape();

}


surfacerHdl::~surfacerHdl()
{
}


bool surfacerHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigSurfacer::InputfileDataset, ConfigSurfacer::ReconstructionDatasetName ) == true &&
			read_ranging_from_h5( ConfigSurfacer::InputfileIonTypes, ConfigSurfacer::IonTypesGroupName ) == true &&
				read_ionlabels_from_h5( ConfigSurfacer::InputfileIonTypes, ConfigSurfacer::IonTypesGroupName ) == true ) {

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//build also here a default array with pieces of information for all ions
		if ( ions.ionpp3.size() > 0 && ions.ionifo.size() != ions.ionpp3.size() ) {
			try {
				ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(),
						p3dinfo( RMX, UNKNOWNTYPE, UNKNOWNTYPE, 0x01, WINDOW_ION_EXCLUDE ) );
				//assume all ions are infinitely large from the edge, of unknown (default ion type), and multiplicity one (i.e. 0x01)
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of information array for the ions failed !" << "\n";
				return false;
			}
		}

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		surf_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;
}


bool surfacerHdl::crop_reconstruction_to_analysis_window()
{
	double tic = MPI_Wtime();

	bool roi_has_accepted_ions = false;
	//assume first we work with the entire reconstruction, hence the ions.ionifo.mask1 is ANALYZE_YES for all
	//successively remove points from the analysis window that meet or not certain properties, like position, associated iontype, multiplicity, etc.
	if ( ConfigSurfacer::WindowingMethod == ENTIRE_DATASET ) {
		apply_all_filter();
	}
	else if ( ConfigSurfacer::WindowingMethod == UNION_OF_PRIMITIVES ) { //##MK::debug implementation
		apply_none_filter();

		//define which ions to include as a union of different geometric primitives
		apply_roi_ensemble_accept_filter( CG_SPHERE, ConfigSurfacer::WindowingSpheres );
		apply_roi_ensemble_accept_filter( CG_CYLINDER, ConfigSurfacer::WindowingCylinders );
		apply_roi_ensemble_accept_filter( CG_CUBOID, ConfigSurfacer::WindowingCuboids );
	}
	else if ( ConfigSurfacer::WindowingMethod == BITMASKED_POINTS ) {
		cerr << "Region-of-interest filtering mode BITMASKED_POINTS is not implemented yet!" << "\n";
		return roi_has_accepted_ions;
	}
	else {
		cerr << "Facing a yet unimplemented region-of-interest filtering mode !" << "\n";
		return roi_has_accepted_ions;
	}

	if ( ConfigSurfacer::LinearSubSamplingRange.min > 0 ||
			ConfigSurfacer::LinearSubSamplingRange.incr > 1 ||
				ConfigSurfacer::LinearSubSamplingRange.max < ions.ionpp3.size() ) {
		//execute only when we can really restrict something
		apply_linear_subsampling_filter( ConfigSurfacer::LinearSubSamplingRange );
	}
	else {
		//ignore this filter as it will have no effect
	}

	if ( ConfigSurfacer::IontypeFilter.is_whitelist == true ) {
		apply_iontype_remove_filter( ConfigSurfacer::IontypeFilter.candidates, false );
	}
	else if ( ConfigSurfacer::IontypeFilter.is_blacklist == true ) {
		apply_iontype_remove_filter( ConfigSurfacer::IontypeFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	if ( ConfigSurfacer::HitMultiplicityFilter.is_whitelist == true ) {
		apply_multiplicity_remove_filter( ConfigSurfacer::HitMultiplicityFilter.candidates, false );
	}
	else if ( ConfigSurfacer::HitMultiplicityFilter.is_blacklist == true ) {
		apply_multiplicity_remove_filter( ConfigSurfacer::HitMultiplicityFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	check_filter();

	//define at least two window classes, ions are assigned a class,
	//window class 0x00 ions are discarded, ions of all other classes are considered
	window = vector<unsigned char>( ions.ionpp3.size(), WINDOW_ION_EXCLUDE );
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			window[i] = ANALYZE_YES; //WINDOW_ION_INCLUDE_DEFAULT_CLASS;
			roi_has_accepted_ions = true;
		}
		//else, nothing to do, window[i] already WINDOW_ION_EXCLUDE
	}

	if ( write_analysis_window() == true ) {
		cout << "Writing analysis window was successful" << "\n";
	}
	else {
		cerr << "Writing analysis window failed !" << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = surf_tictoc.get_memoryconsumption();
	surf_tictoc.prof_elpsdtime_and_mem( "CropReconstructionToAnalysisWindow", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return roi_has_accepted_ions;
}


bool surfacerHdl::write_analysis_window()
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/point_set_wrapping";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	//window
	grpnm += "/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_boolean_filter_mask") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) ions.ionifo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/bitdepth";
	apt_uint bit_depth = sizeof(unsigned char) * 8;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, bit_depth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/mask";
	//use window in-place
	vector<unsigned char> u08_bitfield;
	BitPacker pack_bits;
	pack_bits.uint8_to_bitpacked_uint8( window, u08_bitfield );
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) { return false; }

	return true;
}


void surfacerHdl::identify_ioncloud_dimensions()
{
	double tic = MPI_Wtime();

	ions.get_aabb();
	cout << ions.aabb << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	surf_tictoc.prof_elpsdtime_and_mem( "IdentifyIonCloudDims", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void surfacerHdl::identify_edge_approximately( surface_detection_task const & tsk )
{
	//##execute CA functionalities
	//thread-local binning of ion positions into global vxlgrid
	double tic = MPI_Wtime();

	//define the binning used for pruning vxlgrid
	ca.init_mask3d( ions.aabb, 0.5*(ConfigSurfacer::AdvIonPruningBinWidthRange.min + ConfigSurfacer::AdvIonPruningBinWidthRange.max), tsk.edge_kernel_width );

	//we are interested here only in bins with at least one ion !
	#pragma omp parallel
	{
		/*
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		*/

		//identify which voxels are at all occupied
		vector<size_t> myres;
		#pragma omp for
		for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
			if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
				size_t vxl = ca.gridinfo.where_xyz( ions.ionpp3[i] );
				if ( vxl != -1 ) {
					myres.push_back( vxl );
				}
				else {
					#pragma omp critical
					{
						cerr << "While identifying the edge approximately, I detected that an ion did not end up in a voxel, this is unexpected !" << "\n";
					}
				}
			}
		}
		#pragma omp barrier
		#pragma omp critical
		{
			for( auto it = myres.begin(); it != myres.end(); it++ ) {
				if ( ca.IsOutside[*it] == true ) //visited already, most likely case
					continue;
				else
					ca.IsOutside[*it] = true;
			}
			//release resources
			myres = vector<size_t>();
		}
		//MK::it appears counter-intuitive that we set here the outside to true here
		//but keep in mind, what we do is an inverted HK for finding of a percolating cluster in the outside,
		//against which we can invert the assignment, whose part of this large outside cluster is definately outside, all others are either inside or at the boundary
		//that is infact constituting vacuum using HK algorithm

		//MK::we need to wait before a single thread can run the percolation analysis
		#pragma omp barrier

		#pragma omp master
		{
			if ( ca.identify_outside_voxels() == false ) {
				cerr << "HK identification of outside voxels percolating cluster failed !" << "\n";
			}
			//run the identification algorithm to find where approximately the contour is
			ca.identify_surface_adjacent_voxels();
			ca.identify_inside_voxels();
		}
		//MK::required because omp master construct has no implicit barrier
		#pragma omp barrier

		//filter which ions (those in the voxels adjacent to the surface!) are approximately close to the edge within (1+2*ConfigSurfacer::EdgeDetectionKernelWidth)^3
		vector<p3d> mycand;
		#pragma omp for
		for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
			if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
				size_t vxl = ca.gridinfo.where_xyz( ions.ionpp3[i] );
				if ( vxl != -1 ) {
					if ( ca.IsSurface[vxl] == false ) { //most likely ion is not adjacent to surface for large datasets
						continue;
					}
					else {
						mycand.push_back( ions.ionpp3[i] );
					}
				}
				else {
					#pragma omp critical
					{
						cerr << "While identifying the edge approximately, I detected that an ion did not end up in a voxel, this is unexpected !" << "\n";
					}
				}
			}
		}
		#pragma omp barrier
		#pragma omp critical
		{
			for( auto it = mycand.begin(); it != mycand.end(); it++ ) {
				ions_at_approximate_edge.push_back( *it );
			}
			mycand = vector<p3d>();
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = surf_tictoc.get_memoryconsumption();
	surf_tictoc.prof_elpsdtime_and_mem( "ApproximateEdgeDetection", APT_XX, APT_IS_PAR, mm, tic, toc );

	cout << "Approximate edge detection was successful" << "\n";
	cout << "Analysis revealed that from a total of " << ions.ionpp3.size() << " it suffices to focus on" << "\n";
	cout << ions_at_approximate_edge.size() << " ions, giving a reduction to " << ((double) ions_at_approximate_edge.size()) / ((double) ions.ionpp3.size()) << "\n";
}


void surfacerHdl::compute_dataset_edge( surface_detection_task const & info )
{
	double tic = MPI_Wtime();
	double toc = tic;

	cout << "Rank " << get_myrank() << " computing a model for the edge of the dataset ..." << "\n";

	//identify_ioncloud_dimensions() respecting the window
	ions.aabb = aabb3d();
	for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			ions.aabb.possibly_enlarge_me( ions.ionpp3[i] );
		}
	}
	ions.aabb.scale();

	toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	surf_tictoc.prof_elpsdtime_and_mem( "ComputeAABBofAnalysisWindow", APT_XX, APT_IS_SEQ, mm, tic, toc );
	tic = MPI_Wtime();


	vector<p3d> candidates;
	if ( info.edge_quant_type == SURFACE_ALPHASHAPE_FAST ) {

		identify_edge_approximately( info );

		toc = MPI_Wtime();
		surf_tictoc.prof_elpsdtime_and_mem( "IdentifyEdgeApproximately", APT_XX, APT_IS_SEQ, mm, tic, toc );
		tic = MPI_Wtime();
	}
	else if ( info.edge_quant_type == SURFACE_ALPHASHAPE_NAIVE ) {

		for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
			if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
				candidates.push_back( ions.ionpp3[i] );
			}
		}
	}
	else {
		//nop
	}

	ashp = alphaShape();
	switch(info.alpha_model)
	{
		case CONVEX_HULL_NAIVE:
		{
			if ( info.edge_quant_type == SURFACE_ALPHASHAPE_FAST ) {
				ashp.compute_convex_hull( ions_at_approximate_edge, info, false );
			}
			else if ( info.edge_quant_type == SURFACE_ALPHASHAPE_NAIVE ) {
				ashp.compute_convex_hull( candidates, info, false );
			}
			else {
				//nop
			}
			break;
		}
		case CONVEX_HULL_REFINE:
		{
			if ( info.edge_quant_type == SURFACE_ALPHASHAPE_FAST ) {
				ashp.compute_convex_hull( ions_at_approximate_edge, info, true );
			}
			else if ( info.edge_quant_type == SURFACE_ALPHASHAPE_NAIVE ) {
				ashp.compute_convex_hull( candidates, info, true );
			}
			else {
				//nop
			}
			break;
		}
		case ASHAPE_SMALLEST_SOLID:
		{
			if ( info.edge_quant_type == SURFACE_ALPHASHAPE_FAST ) {
				ashp.compute_single_alpha_shape( ions_at_approximate_edge, info );
			}
			else if ( info.edge_quant_type == SURFACE_ALPHASHAPE_NAIVE ) {
				ashp.compute_single_alpha_shape( candidates, info );
			}
			else {
				//nop
			}
			break;
		}
		case ASHAPE_CGAL_OPTIMAL:
		{
			if ( info.edge_quant_type == SURFACE_ALPHASHAPE_FAST ) {
				ashp.compute_single_alpha_shape( ions_at_approximate_edge, info );
			}
			else if ( info.edge_quant_type == SURFACE_ALPHASHAPE_NAIVE ) {
				ashp.compute_single_alpha_shape( candidates, info );
			}
			else {
				//nop
			}
			break;
		}
		case ASHAPE_ALPHAVALUE_ENSEMBLE:
		{
			if ( info.edge_quant_type == SURFACE_ALPHASHAPE_FAST ) {
				ashp.compute_multiple_alpha_shapes( ions_at_approximate_edge, info );
			}
			else if ( info.edge_quant_type == SURFACE_ALPHASHAPE_NAIVE ) {
				ashp.compute_multiple_alpha_shapes( candidates, info );
			}
			else {
				//nop
			}
			break;
		}
		case ASHAPE_ALPHAVALUE_WRAPPINGS:
		{
			if ( info.edge_quant_type == SURFACE_ALPHASHAPE_FAST ) {
				ashp.compute_multiple_alpha_wrappings( ions_at_approximate_edge, info );
			}
			else if ( info.edge_quant_type == SURFACE_ALPHASHAPE_NAIVE ) {
				ashp.compute_multiple_alpha_wrappings( candidates, info );
			}
			else {
				//nop
			}
			break;
		}
	}

	toc = MPI_Wtime();
	surf_tictoc.prof_elpsdtime_and_mem( "BuildEdgeAsAlphaShape", APT_XX, APT_IS_SEQ, mm, tic, toc );
}

//code for further alpha-shape properties is available in commits prior v0.5
