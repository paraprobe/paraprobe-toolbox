/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_SurfacerMesher.h"


alphaShape::alphaShape()
{
	vrts = vector<p3d64>();
	fcts = vector<tri3u>();
	info = ashape_info();

	the_objects = vector<CGAL::Object>();
	the_ft = vector<Alpha_shape_3::FT>();

	poly = Polyhedron();
	cvh = Surface();
	//Alpha_shape_3 as;
	wrap = Surface();
}


alphaShape::~alphaShape()
{
	vrts = vector<p3d64>();
	fcts = vector<tri3u>();
	info = ashape_info();

	the_objects = vector<CGAL::Object>();
	the_ft = vector<Alpha_shape_3::FT>();

	poly = Polyhedron();
	cvh = Surface();
	//Alpha_shape_3 as;
	wrap = Surface();
}


//see also https://stackoverflow.com/questions/15905833/saving-cgal-alpha-shape-surface-mesh
//for using exact alpha see here
//https://doc.cgal.org/latest/Alpha_shapes_3/Alpha_shapes_3_2ex_alpha_shapes_exact_alpha_8cpp-example.html


void alphaShape::alpha_shape_perform_facet_filtration( Alpha_shape_3 & as )
{
	//filtration
	cout << "Debugging computing of facet filtration ..." << "\n";
	the_objects = vector<CGAL::Object>();
	the_ft = vector<Alpha_shape_3::FT>();

	typedef CGAL::Dispatch_output_iterator<
	CGAL::cpp11::tuple<CGAL::Object, Alpha_shape_3::FT>,
	CGAL::cpp11::tuple<std::back_insert_iterator< vector<CGAL::Object> >,
		back_insert_iterator< std::vector<Alpha_shape_3::FT> >
			> > Dispatch;

	Dispatch disp = CGAL::dispatch_output<
			CGAL::Object, Alpha_shape_3::FT>(back_inserter(the_objects), back_inserter(the_ft) );

	as.filtration_with_alpha_values(disp);

	//toc = omp_get_wtime();
	//info.dt_ashape_getalphas = (toc-tic);
}


void alphaShape::alpha_shape_extract_interior_tetrahedra( Alpha_shape_3 & as)
{
	//accumulated volume of the alpha-shape's interior tetrahedra
	vector<Alpha_shape_3::Cell_handle> cells;
	as.get_alpha_shape_cells( back_inserter(cells), Alpha_shape_3::INTERIOR );
	info.ninteriortets = cells.size();

	double volume = MYZERO;
	for ( vector<Alpha_shape_3::Cell_handle>::iterator it = cells.begin(); it != cells.end(); it++ ) {
		Point p1 = (*it)->vertex(0)->point();
		Point p2 = (*it)->vertex(1)->point();
		Point p3 = (*it)->vertex(2)->point();
		Point p4 = (*it)->vertex(3)->point();
		Alpha_shape_3::Tetrahedron t(p1,p2,p3,p4);
		volume += t.volume();
	}
	cout << "Accumulated interior tetrahedra volume is " << setprecision(32) << volume << " nm^3" << "\n";
	info.volume_interior_tetrahedra = volume;
	/*
	toc = omp_get_wtime();
	info.dt_ashape_getvol = (toc-tic);
	tic = omp_get_wtime();
	info.volume_interior_tetrahedra = volume;
	*/
}


void alphaShape::alpha_shape_extract_exterior_facets( Alpha_shape_3 & as)
{
	//output triangulation of (optimal) alpha shape
	//in CGAL-4.11 the "outer" surface is what we are looking for, i.e. all facets of kind REGULAR
	//https://stackoverflow.com/questions/15905833/saving-cgal-alpha-shape-surface-mesh
	vector<Alpha_shape_3::Facet> facets;
	as.get_alpha_shape_facets(back_inserter(facets), Alpha_shape_3::REGULAR);

	cout << "facets.size() " << facets.size() << "\n";

	//int old_vertex_id = 0;
	apt_uint cur_vertex_id = 0;
	//apt_uint new_vertex_id = 0;
	//map<int, apt_uint> old2new_unique;
	//map<int, int>::iterator which;
	//vector<p3d> vuni;
	//vector<triref3d> tri;
	map<Eigen::Vector3d, apt_uint, vectorComparator> p3d2oid; //finding the unique points and giving them new IDs

	for ( size_t f = 0; f < facets.size(); f++ ) {
		//To have a consistent orientation of the facet, always consider an exterior cell
		if ( as.classify( facets[f].first ) != Alpha_shape_3::EXTERIOR ) {
			facets[f] = as.mirror_facet( facets[f] );
		}
		CGAL_assertion( as.classify(facets[f].first) == Alpha_shape_3::EXTERIOR );

		int indices[3] = { (facets[f].second+1)%4, (facets[f].second+2)%4, (facets[f].second+3)%4 };

		//according to the encoding of vertex indices, this is needed to get a consistent orientation
		if ( facets[f].second%2 != 0 ) {
		}
		else {
			swap(indices[0], indices[1]);
		}

		//filter out unique points and give new IDs, mainly to avoid storing redundant triangle geometry information (i.e. lossless ##MK data compression)
		Eigen::Vector3d p1( facets[f].first->vertex(indices[0])->point().x(), facets[f].first->vertex(indices[0])->point().y(), facets[f].first->vertex(indices[0])->point().z() );
		map<Eigen::Vector3d, apt_uint>::iterator v1it = p3d2oid.find( p1 );
		apt_uint v1 = UMX;
		if ( v1it == p3d2oid.end() ) { //add a so far unknown point
			p3d2oid.insert( pair<Eigen::Vector3d, apt_uint>(p1, cur_vertex_id) );
			vrts.push_back( p3d64( p1.x(), p1.y(), p1.z() ) );
			v1 = cur_vertex_id;
			cur_vertex_id++;
		}
		else {
			v1 = v1it->second;
		}
		Eigen::Vector3d p2( facets[f].first->vertex(indices[1])->point().x(), facets[f].first->vertex(indices[1])->point().y(), facets[f].first->vertex(indices[1])->point().z() );
		map<Eigen::Vector3d, apt_uint>::iterator v2it = p3d2oid.find( p2 );
		apt_uint v2 = UMX;
		if ( v2it == p3d2oid.end() ) { //add a so far unknown point
			p3d2oid.insert( pair<Eigen::Vector3d, apt_uint>(p2, cur_vertex_id) );
			vrts.push_back( p3d64( p2.x(), p2.y(), p2.z() ) );
			v2 = cur_vertex_id;
			cur_vertex_id++;
		}
		else {
			v2 = v2it->second;
		}
		Eigen::Vector3d p3( facets[f].first->vertex(indices[2])->point().x(), facets[f].first->vertex(indices[2])->point().y(), facets[f].first->vertex(indices[2])->point().z() );
		map<Eigen::Vector3d, apt_uint>::iterator v3it = p3d2oid.find( p3 );
		apt_uint v3 = UMX;
		if ( v3it == p3d2oid.end() ) { //add a so far unknown point
			p3d2oid.insert( pair<Eigen::Vector3d, apt_uint>(p3, cur_vertex_id) );
			vrts.push_back( p3d64( p3.x(), p3.y(), p3.z() ) );
			v3 = cur_vertex_id;
			cur_vertex_id++;
		}
		else {
			v3 = v3it->second;
		}
		fcts.push_back( tri3u( v1, v2, v3 ) );
	}

	//toc = omp_get_wtime();
	//info.dt_ashape_getprims = (toc-tic);
	//tic = omp_get_wtime();
}


void alphaShape::surface_mesh_extract_exterior_facets( Surface & msh )
{
	//##MK::report me the triangles
	//export unique vertices and facets of the mesh, now cvh is guaranteed triangle mesh
	//##MK::reduce number of vertices with vectorComparator
	Surface::Vertex_range vrng = msh.vertices();
	Surface::Vertex_range::iterator vb = vrng.begin();
	Surface::Vertex_range::iterator ve = vrng.end();
	for(   ; vb != ve; ++vb ) {
		//size_t vertexID = *vb;
		vrts.push_back( p3d64( msh.point(*vb).x(), msh.point(*vb).y(), msh.point(*vb).z() ) );
	}

	Surface::Face_range frng = msh.faces();
	Surface::Face_range::iterator fb = frng.begin();
	Surface::Face_range::iterator fe = frng.end();
	for(   ; fb != fe; ++fb ) {
		vector<apt_uint> triangle; //https://doc.cgal.org/latest/Surface_mesh/index.html#circulators_example
		CGAL::Vertex_around_face_iterator<Surface> vbegin, vend;
		for(boost::tie(vbegin, vend) = vertices_around_face( msh.halfedge(*fb), msh); vbegin != vend; ++vbegin ) {
			triangle.push_back( (apt_uint) *vbegin ); //##MK::(size_t)
		}
		fcts.push_back( tri3u( triangle[0], triangle[1], triangle[2] ) );
	}
}


void alphaShape::polyhedron_extract_exterior_facets()
{
	map<Eigen::Vector3d, apt_uint, vectorComparator> p3d2id; //finding the unique points and giving them new IDs
	apt_uint cur_vertex_id = 0;
	for( Polyhedron::Point_iterator vb = poly.points_begin(); vb != poly.points_end(); ++vb ) {
		Eigen::Vector3d v( vb->x(), vb->y(), vb->z() );
		map<Eigen::Vector3d, apt_uint>::iterator vit = p3d2id.find( v );
		if ( vit == p3d2id.end() ) {
			p3d2id.insert( pair<Eigen::Vector3d, apt_uint>( v, cur_vertex_id ) );
			cur_vertex_id++;
		}
		/*else {
			cerr << "The mesh has unexpectedly a duplicated point !" << "\n";
		}*/
	}
	//MK::by virtue of construction are all elements in p3d2id[i]->second on the apt_uint ID interval [0, cur_vertex_id)
	vrts = vector<p3d64>( p3d2id.size(), p3d64() );
	for( auto it = p3d2id.begin(); it != p3d2id.end(); it++ ) {
		vrts[it->second] = p3d64( it->first.x(), it->first.y(), it->first.z() );
	}

	apt_uint nvrts_per_fct = 0;
	for( Polyhedron::Facet_iterator fctit = poly.facets_begin(); fctit != poly.facets_end(); ++fctit ) {

		Polyhedron::Halfedge_around_facet_circulator j = fctit->facet_begin();
		nvrts_per_fct = 0;
		vector<apt_uint> vvisited;
		do
		{
			Eigen::Vector3d v( j->vertex()->point().x(), j->vertex()->point().y(), j->vertex()->point().z() );
			//does this guy exist? It has to otherwise there are problems anyway...
			map<Eigen::Vector3d, apt_uint>::iterator vit = p3d2id.find( v );
			if ( vit != p3d2id.end() ) {
				vvisited.push_back( vit->second );
				nvrts_per_fct++;
			}
			else {
				cerr << "While exporting the polygons of the mesh we unexpectedly faced an unknown point !" << "\n";
				vrts = vector<p3d64>();
				fcts = vector<tri3u>();
				return;
			}
		} while ( ++j != fctit->facet_begin() ); //circulate around the facet until we reach again the node where we started

		if ( fctit->is_triangle() == true && nvrts_per_fct == 3 ) {
			//cout << "Facet is a triangle" << "\n";
			fcts.push_back( tri3u( vvisited[0], vvisited[1], vvisited[2] ) );

			//##MK::building on the assumption that for CGAL::closed polyhedra the facets are consistently oriented when viewed from outside
		}
		else if ( fctit->is_quad() == true && nvrts_per_fct == 4 ) {
			cerr << "While exporting the mesh of a polygon we unexpectedly found a quad facet !" << "\n";
			vrts = vector<p3d64>();
			fcts = vector<tri3u>();
			//ply_mesh_fcts_quads.push_back( quad3u( vvisited[0], vvisited[1], vvisited[2], vvisited[3] ) );
			return;
		}
		else {
			cerr << "While exporting the mesh of a polygon we found a facet with is neither a triangle nor a quad !" << "\n";
			vrts = vector<p3d64>();
			fcts = vector<tri3u>();
			//ply_mesh_fcts_quads = vector<quad3u>();
			return;
		}
	}
}


void alphaShape::mesh_check_closure()
{
	pair<double, apt_int> volume_and_closure_state = pair<double, apt_int>( MYZERO, TRIANGLE_SOUP_ANALYSIS_NO_SUPPORT );

	//remove this redundancy
	vector<p3d> pts;
	for( auto it = vrts.begin(); it != vrts.end(); it++ ) {
		pts.push_back( p3d( it->x, it->y, it->z ) );
	}

	volume_and_closure_state = check_if_closed_polyhedron( fcts, pts );
	info.volume_watertight_mesh = volume_and_closure_state.first;
	if ( volume_and_closure_state.second == TRIANGLE_SOUP_ANALYSIS_CLOSEDPOLYHEDRON ) {
		info.is_closed = true;
		info.is_watertight = true;
	}
	else {
		info.is_closed = false;
		info.is_watertight = false;
	}
}


void alphaShape::polyhedron_check_closure()
{
	double tic = omp_get_wtime();
	double toc = tic;

	if ( CGAL::is_closed( poly ) == true ) {

		//assume that the polyhedron has already been consistently instantiated and oriented
		//CGAL::Polygon_mesh_processing::orient_to_bound_a_volume(ply_mesh);

		double volume = CGAL::Polygon_mesh_processing::volume( poly );

		toc = omp_get_wtime();
		cout << "Watertight model: yes, volume is " << volume << " nm^3, checking watertightness took " << (toc-tic) << " seconds" << "\n";

		info.volume_watertight_mesh = volume;
		info.is_closed = true;
		info.is_watertight = true;
	}
	else {
		toc = omp_get_wtime();
		cout << "Watertight model: no, volume is undefined, checking watertightness took " << (toc-tic) << " seconds" << "\n";
		info.volume_watertight_mesh = MYZERO;
		info.is_closed = false;
		info.is_watertight = false;
	}
}


void alphaShape::compute_convex_hull( vector<p3d> const & p, surface_detection_task const & tsk, const bool mesh_refinement )
{
	double tic = omp_get_wtime();
	double toc = tic;

	vrts = vector<p3d64>();
	fcts = vector<tri3u>();
	info = ashape_info();

	if ( p.size() > 0 && p.size() <= (UMX-1) ) {

		cout << "Computing a convex hull from " << p.size() << " points using the CGAL library ..." << "\n";

		vector<Point> pts;
		for( auto it = p.begin(); it != p.end(); it++ ) {
			pts.push_back( Point(it->x, it->y, it->z) );
		}

		toc = omp_get_wtime();
		info.dt_delaunay = (toc-tic);
		tic = omp_get_wtime();

		CGAL::convex_hull_3( pts.begin(), pts.end(), poly);
		//std::cout << "The convex hull contains " << poly.size_of_vertices() << " vertices" << std::endl;

		if ( is_triangle_mesh(poly) == true ) {
			cout << "The convex hull has " << poly.size_of_vertices() << " vertices and " << poly.size_of_facets() << " faces" << "\n";
		}
		else {
			cerr << "The convex hull mesh is unexpectedly not a triangle mesh !" << "\n";
			return;
		}

		/*
		CGAL::convex_hull_3( pts.begin(), pts.end(), cvh);
		cout << "The convex hull has " << num_vertices(cvh) << " vertices and " << num_faces(cvh) << " faces" << "\n";

		if ( PMP::triangulate_faces(cvh) == true ) {
			//confirm that all faces are triangle
			for(boost::graph_traits<Surface>::face_descriptor fit : faces(cvh)) {
				if (next(next(halfedge(fit, cvh), cvh), cvh) != prev(halfedge(fit, cvh), cvh)) {
					cerr << "The triangulation of the convex hull failed ! At least one non-triangular face is left in the mesh !" << "\n";
					return;
				}
			}
		}
		*/

		if ( mesh_refinement == true ) {
			cout << "Refining the convex hull to improve the quality of its triangles ..." << "\n";
			//refine the convex hull to arrive at a better shape and size distribution of the triangular facets
			//but do not perform a fairing

			vector<Polyhedron::Facet_handle> new_facets;
			vector<Vertex_handle> new_vertices;

			//the polyhedron will be changed
			PMP::refine(poly, faces(poly), back_inserter(new_facets), back_inserter(new_vertices),
					CGAL::parameters::density_control_factor(ConfigSurfacer::RefinementDensityControlFactor));
			cout << "Refining the triangle mesh of the convex hull added " << new_vertices.size() << " vertices." << "\n";

			if ( is_triangle_mesh(poly) == true ) {
				cout << "After refinement, the modified convex hull mesh is still a triangle mesh" << "\n";
				cout << "This mesh has " << poly.size_of_vertices() << " vertices and " << poly.size_of_facets() << " faces" << "\n";
			}
			else {
				cerr << "After refinement, the modified convex hull mesh is unexpectedly no longer a triangle mesh!" << "\n";
				return;
			}
		}

		info.alpha = F64MX;
		info.ncomponents = 1;

		//##MK::refine the convex hull so that we do not have so many skinny triangles
		//https://doc.cgal.org/latest/Polygon_mesh_processing/Polygon_mesh_processing_2refine_fair_example_8cpp-example.html#a4

		toc = omp_get_wtime();
		info.dt_ashape_compute = (toc-tic);
		tic = omp_get_wtime();

		if ( tsk.hasExtFacetsPolyhedronCheck == true ) {
			polyhedron_check_closure();
		}

		if ( tsk.hasExtFacets == true ) {
			/*
			surface_mesh_extract_exterior_facets( cvh );
			*/
			polyhedron_extract_exterior_facets();
		}

		if ( tsk.hasIntTetrahedra == true ) {
			//##MK::not implemented yet
			info.ninteriortets = 0;
		}

		cout << "Reporting details about the successfully computed convex hull" << "\n";
		cout << info << "\n";
		cout << "vrts.size() " << vrts.size() << "\n";
		cout << "fcts.size() " << fcts.size() << "\n";

		if ( write_alphashape_geometry( tsk, 1 ) == true ) {
			cout << "Writing alphashape geometry was successful" << "\n";
		}
		else {
			cerr << "Writing alphashape geometry failed !" << "\n";
		}
	}
	else {
		cerr << "There are either no or too many points in the input dataset to construct a convex hull from !" << "\n";
	}
}


void alphaShape::compute_single_alpha_shape( vector<p3d> const & p, surface_detection_task const & tsk )
{
	double tic = omp_get_wtime();
	double toc = tic;

	vrts = vector<p3d64>();
	fcts = vector<tri3u>();
	info = ashape_info();

	//utilize the CGAL library to compute the alpha shapes
	if ( p.size() > 0 && p.size() <= (UMX-1) ) {

		cout << "Computing an alpha shape from " << p.size() << " points using the CGAL library ..." << "\n";

		Delaunay dt;
		for ( auto it = p.begin(); it != p.end(); it++ ) {
			dt.insert( Point( it->x, it->y, it->z ) );
		}

		toc = omp_get_wtime();
		info.dt_delaunay = (toc-tic);
		tic = omp_get_wtime();

		Alpha_shape_3 as( dt );

		cout << "Alpha shape initialized, will be computed in REGULARIZED mode by default" << "\n";

		toc = omp_get_wtime();
		info.dt_ashape_compute = (toc-tic);
		tic = omp_get_wtime();

		if ( tsk.alpha_model == ASHAPE_SMALLEST_SOLID ) {
			//no input parameter, let CGAL decide the alpha value
			//cout << "Taking the smallest alpha value to get a solid through data points is " << alpha_solid << " for triangulation" << "\n";
			Alpha_shape_3::NT alpha_solid = as.find_alpha_solid();
			as.set_alpha(alpha_solid);

			cout << "Smallest alpha value to get a solid through data points is " << alpha_solid << "\n";
			info.alpha = alpha_solid;
		}
		else { //tsk.alpha_model == ASHAPE_CGAL_OPTIMAL
			//input parameter number of connected components
			//cout << "Taking the CGAL optimal value to get a solid through data points is " << *opt << " for triangulation" << "\n";
			Alpha_iterator opt = as.find_optimal_alpha( ConfigSurfacer::NumberOfConnectedComponents );
			as.set_alpha(*opt);

			cout << "Optimal alpha value to get one connected component is " << *opt << "\n";
			info.alpha = *opt;
		}

		info.ncomponents = as.number_of_solid_components();
		//cout << "The alpha shape with alpha " << info.alpha << " has " << num_vertices(as) << " vertices and " << num_faces(as) << " faces" << "\n";

		//Delaunay dt is no longer needed
		dt = Delaunay();

		//currently CGAL requires the alpha shape to be private...

		if ( tsk.hasExtFacets == true ) {
			alpha_shape_extract_exterior_facets( as );
		}

		if ( tsk.hasExtFacetsPolyhedronCheck == true ) {
			mesh_check_closure();
		}

		if ( tsk.hasIntTetrahedra == true ) {
			alpha_shape_extract_interior_tetrahedra( as );
		}

		if ( tsk.hasFacetAppearanceAlphas == true ) {
			alpha_shape_perform_facet_filtration( as );
		}

		cout << "Reporting details about the successfully computed alpha-shape" << "\n";
		cout << info << "\n";
		cout << "vrts.size() " << vrts.size() << "\n";
		cout << "fcts.size() " << fcts.size() << "\n";

		if ( write_alphashape_geometry( tsk, 1 ) == true ) {
			cout << "Writing alphashape geometry was successful" << "\n";
		}
		else {
			cerr << "Writing alphashape geometry failed !" << "\n";
		}
	}
	else {
		cerr << "There are either no or too many points in the input dataset to construct a single alpha shape from !" << "\n";
	}
}


void alphaShape::compute_multiple_alpha_shapes( vector<p3d> const & p, surface_detection_task const & tsk )
{
	double tic = omp_get_wtime();
	double toc = tic;

	//utilize the CGAL library to compute the alpha shapes
	if ( p.size() > 0 && p.size() <= (UMX-1) ) {

		cout << "Computing a set of alpha shapes from " << p.size() << " points using the CGAL library ..." << "\n";

		Delaunay dt;
		for ( auto it = p.begin(); it != p.end(); it++ ) {
			dt.insert( Point( it->x, it->y, it->z ) );
		}

		toc = omp_get_wtime();
		info.dt_delaunay = (toc-tic);
		tic = omp_get_wtime();

		Alpha_shape_3 as( dt );
		cout << "Alpha shape initialized, will be computed in REGULARIZED mode by default" << "\n";

		toc = omp_get_wtime();
		info.dt_ashape_compute = (toc-tic);
		tic = omp_get_wtime();

		//clear the internal cache for storing geometrical data
		vrts = vector<p3d64>();
		fcts = vector<tri3u>();

		apt_uint ashp_id = 1;
		for( auto alpha_iter = tsk.alpha_values.begin(); alpha_iter != tsk.alpha_values.end(); alpha_iter++ ) {

			//use value linearly
			Alpha_shape_3::NT alpha_current = *alpha_iter;

			/*
			//use exponential, with base10, alpha_iter-1, "0.1" subcyling i.e. when alpha_iter = 0 and 1 we get 1.0, 2.0, 3.0, ..., 10.0
			for( int subcycle = 0; subcycle < 9; subcycle++ ) {
				if ( alpha_iter > -8 && alpha_iter < 12 ) {
					alpha_current = (MYONE + ((apt_real) subcycle)) * pow(10.0, alpha_iter);
				}
			*/

			//ashape_info a_info = ashape_info();

			as.set_alpha( alpha_current );
			info.alpha = alpha_current;
			info.ncomponents = as.number_of_solid_components();

			cout << "Current alpha value is " << alpha_current << "\n";
			//cout << "Current number of solid components is " << as.number_of_solid_components() << "\n";

			if ( tsk.hasExtFacets == true ) { //extract a triangularized "view" of the alpha shape complex
				alpha_shape_extract_exterior_facets( as );
			}

			if ( tsk.hasExtFacetsPolyhedronCheck == true ) {
				mesh_check_closure();
			}

			if ( tsk.hasIntTetrahedra == true ) {
				alpha_shape_extract_interior_tetrahedra( as );
			}

			if ( tsk.hasFacetAppearanceAlphas == true ) {
				alpha_shape_perform_facet_filtration( as );
			}

			cout << "Reporting details about the successfully computed alpha shape" << "\n";
			cout << info << "\n";
			cout << "vrts.size() " << vrts.size() << "\n";
			cout << "fcts.size() " << fcts.size() << "\n";

			if ( write_alphashape_geometry( tsk, ashp_id ) == true ) {
				cout << "Writing alphashape geometry was successful" << "\n";
			}
			else {
				cerr << "Writing alphashape geometry failed !" << "\n";
			}

			ashp_id++;
		} //next alpha range value
	}
	else {
		cerr << "There are either no or too many points in the input dataset to construct a set of alpha shapes from !" << "\n";
	}
}


void alphaShape::compute_multiple_alpha_wrappings( vector<p3d> const & p, surface_detection_task const & tsk )
{
	double tic = omp_get_wtime();
	double toc = tic;

	vrts = vector<p3d64>();
	fcts = vector<tri3u>();
	info = ashape_info();

	//utilize the CGAL library to compute the alpha shapes
	if ( p.size() > 0 && p.size() <= (UMX-1) ) {

		cout << "Computing a set of alpha wrappings from " << p.size() << " points using the CGAL library ..." << "\n";

		vector<Point> pts;
		for ( auto it = p.begin(); it != p.end(); it++ ) {
			pts.push_back( Point( it->x, it->y, it->z ) );
		}

		toc = omp_get_wtime();
		info.dt_delaunay = (toc-tic);
		tic = omp_get_wtime();

		apt_uint awrp_id = 1;
		for( size_t i = 0; i < tsk.alpha_values.size(); i++ ) {

			vrts = vector<p3d64>();
			fcts = vector<tri3u>();
			info = ashape_info();

			// Compute the alpha and offset values
			/*
			const double relative_alpha = (argc > 2) ? std::stod(argv[2]) : 10.;
			const double relative_offset = (argc > 3) ? std::stod(argv[3]) : 300.;
			CGAL::Bbox_3 bbox = CGAL::bbox_3(std::cbegin(points), std::cend(points));
			const double diag_length = std::sqrt(CGAL::square(bbox.xmax() - bbox.xmin()) +
										   CGAL::square(bbox.ymax() - bbox.ymin()) +
										   CGAL::square(bbox.zmax() - bbox.zmin()));
			*/

			const double alpha = tsk.alpha_values.at(i);
			const double offset = tsk.offset_values.at(i);
			cout << "alpha-wrapping " << i << " absolute alpha " << alpha << " absolute offset " << offset << "\n";

			tic = omp_get_wtime();

			wrap = Surface();

			CGAL::alpha_wrap_3(pts, alpha, offset, wrap);

			toc = omp_get_wtime();
			cout << "The alpha wrapping with alpha " << alpha << " and offset " << offset << " has "
					<< num_vertices(wrap) << " vertices and " << num_faces(wrap) << " faces" << "\n";
			cout << "Computing the alpha wrapping took " << (toc-tic) << " s" << "\n";

			info.alpha = alpha;
			info.offset = offset;

			if ( tsk.hasExtFacets == true ) {

				surface_mesh_extract_exterior_facets( wrap );

				if ( tsk.hasExtFacetsPolyhedronCheck == true ) {

					this->mesh_check_closure();
				}
			}

			if ( tsk.hasIntTetrahedra == true ) {
				//##MK::not implemented yet
				info.ninteriortets = 0;
			}

			cout << "Reporting details about the successfully computed alpha-wrapping" << "\n";
			cout << info << "\n";
			cout << "vrts.size() " << vrts.size() << "\n";
			cout << "fcts.size() " << fcts.size() << "\n";

			if ( write_alphashape_geometry( tsk, awrp_id ) == true ) {
				cout << "Writing alphashape geometry was successful" << "\n";
			}
			else {
				cerr << "Writing alphashape geometry failed !" << "\n";
			}
			awrp_id++;
		}
	}
	else {
		cerr << "There are either no or too many points in the input dataset to construct a set of alpha wrappings from !" << "\n";
	}
}


bool alphaShape::write_alphashape_geometry( surface_detection_task const & tsk,
		apt_uint const identifier )
{
	if ( 3 * vrts.size() >= (size_t) UMX ) {
		cerr << "The total number of triangle vertices to write out is larger than currently supported by the implementation !" << "\n";
		return false;
		//##MK::change wuibuf to a 64-bit type , e.g. ui64 size_t
	}
	if ( fcts.size() < 1 ) {
		cerr << "WARNING:: There are no triangular facets of the alpha shape that I could report" << "\n";
		return false;
	}

	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	apt_uint proc_id = 1;
	grpnm = "/entry" + to_string(entry_id) + "/point_set_wrapping";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_path_exists( grpnm ) == false ) {
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	//##MK::implement the documentation of which ions where taken for the alpha shape
	//##############

	grpnm = "/entry" + to_string(entry_id) + "/point_set_wrapping/alpha_complex" + to_string(identifier);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_alpha_complex") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/dimensionality";
	apt_uint dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/type";
	string type = "";
	if ( tsk.alpha_model == CONVEX_HULL_NAIVE || tsk.alpha_model == CONVEX_HULL_REFINE ) {
		type = "convex_hull";
	}
	else if ( tsk.alpha_model == ASHAPE_SMALLEST_SOLID ||
				tsk.alpha_model == ASHAPE_CGAL_OPTIMAL ||
					tsk.alpha_model == ASHAPE_ALPHAVALUE_ENSEMBLE ) {
		type = "alpha_shape";
	}
	else if ( tsk.alpha_model == ASHAPE_ALPHAVALUE_WRAPPINGS ) {
		type = "alpha_wrapping";
	}
	else {
		type = "other";
	}
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, type, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/mode";
	string alpha_mode = "regularized";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, alpha_mode, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/alpha";
	double alpha = info.alpha;
	anno = ioAttributes();
	if ( tsk.alpha_model != ASHAPE_ALPHAVALUE_WRAPPINGS ) {
		anno.add( "unit", string("nm^2") ); //squared radius for alpha shapes, nm for alpha wrappings
	}
	else {
		anno.add( "unit", string("nm") ); //nm for alpha wrappings
	}
	if ( h5w.nexus_write( dsnm, alpha, anno ) != MYHDF5_SUCCESS ) { return false; }

	if ( tsk.alpha_model == ASHAPE_ALPHAVALUE_WRAPPINGS ) {
		dsnm = grpnm + "/offset";
		double offset = info.offset;
		anno = ioAttributes();
		anno.add( "unit", string("nm") ); //##MK:: or nm^2 ??
		if ( h5w.nexus_write( dsnm, offset, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	grpnm = "/entry" + to_string(entry_id) + "/point_set_wrapping/alpha_complex" + to_string(identifier) + "/triangle_set";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_triangle_set") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/identifier_offset";
	int id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/is_watertight";
	unsigned char u08 = ( info.is_watertight == true ) ? 0x01 : 0x00;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, u08, anno ) != MYHDF5_SUCCESS ) { return false; }

	if ( info.is_watertight == true ) {
		dsnm = grpnm + "/volume";
		double volume = info.volume_watertight_mesh;
		anno = ioAttributes();
		anno.add( "unit", string("nm^3") );
		if ( h5w.nexus_write( dsnm, volume, anno ) != MYHDF5_SUCCESS ) { return false; }
	}

	grpnm = "/entry" + to_string(entry_id) + "/point_set_wrapping/alpha_complex" + to_string(identifier) + "/triangle_set/triangles";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_face_list_data_structure") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/dimensionality";
	dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_vertices";
	apt_uint n_vrts = (apt_uint) vrts.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_vrts, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_faces";
	apt_uint n_fcts = (apt_uint) fcts.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_fcts, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/vertex_identifier_offset";
	int vrts_id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, vrts_id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/face_identifier_offset";
	int fcts_id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, fcts_id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/vertices";
	string dsnm_vrts = dsnm; //needed for XDMF
	vector<double> real;
	for( auto it = vrts.begin(); it != vrts.end(); it++ ) {
		real.push_back( it->x );
		real.push_back( it->y );
		real.push_back( it->z );
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm") );
	//##MK::add NXtransformations depends on paraprobe
	if ( h5w.nexus_write(
			dsnm,
			io_info({real.size() / 3, 3}, {real.size() / 3, 3},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			real,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<double>();

	dsnm = grpnm + "/faces";
	vector<apt_uint> uint;
	for( auto it = fcts.begin(); it != fcts.end(); it++ ) {
		uint.push_back( it->v1 );
		uint.push_back( it->v2 );
		uint.push_back( it->v3 );
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({uint.size() / 3, 3}, {uint.size() / 3, 3},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	//supplementary file for visualization in e.g. ParaView using XDMF
	xdmfBaseHdl xml;
	if ( tsk.alpha_model == CONVEX_HULL_NAIVE || tsk.alpha_model == CONVEX_HULL_REFINE ) {
		xml.add_grid_uniform( "alpha_shape infty" );
	}
	else if ( tsk.alpha_model != ASHAPE_ALPHAVALUE_WRAPPINGS ) {
		xml.add_grid_uniform( "alpha_shape " + to_string(info.alpha) );
	}
	else {
		xml.add_grid_uniform( "alpha_wrapping " + to_string(info.alpha) + " " + to_string(info.offset) );
	}

	dsnm = grpnm + "/xdmf_topology";
	string dsnm_fcts = dsnm;
	for( auto it = fcts.begin(); it != fcts.end(); it++ ) {
		uint.push_back( 3 ); //xdmf keyword for polygon
		uint.push_back( 3 ); //xdmf each triangle has three vertices
		uint.push_back( it->v1 );
		uint.push_back( it->v2 );
		uint.push_back( it->v3 );
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm_fcts,
			io_info({uint.size()}, {uint.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			uint,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	uint = vector<apt_uint>();

	vector<size_t> dims = { fcts.size()*(1+1+3) }; //xdmf keyword, xdmf number of vertices, three vertex IDs for each triangle
	xml.add_topology_mixed( fcts.size(), dims, H5_U32, ConfigShared::OutputfileName + ":" + dsnm_fcts );

	dims = { vrts.size(), 3 };
	xml.add_geometry_xyz( dims, H5_F64, ConfigShared::OutputfileName + ":" + dsnm_vrts );

	//##MK::no attributes values for now for each face
	//dims = { gridinfo.nxyz, 1 };
	//xml.add_attribute_scalar( "fieldvalue", "Node", dims, H5_F32, ConfigNanochem::OutputfilePrefix + ".h5" + ":" + dsnm );

	string xmlfn = strip_path_prefix(ConfigShared::OutputfileName) + ".EntryId." + to_string(entry_id) + ".TaskId." + to_string(proc_id) + ".ComplexID." + to_string(identifier);
	if ( tsk.alpha_model != ASHAPE_ALPHAVALUE_WRAPPINGS ) {
		xmlfn += ".AlphaShapeExterior.xdmf";
	}
	else {
		xmlfn += ".AlphaWrappingExterior.xdmf";
	}
	xml.write( xmlfn );

	if ( tsk.alpha_model == ASHAPE_SMALLEST_SOLID || tsk.alpha_model == ASHAPE_CGAL_OPTIMAL || tsk.alpha_model == ASHAPE_ALPHAVALUE_ENSEMBLE ) {
		if ( tsk.hasIntTetrahedra == true ) {
			grpnm = "/entry" + to_string(entry_id) + "/point_set_wrapping/alpha_complex" + to_string(identifier) + "/interior_tetrahedra";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_tetrahedron_set") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/identifier_offset";
			id_offset = 0;
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/volume";
			double volume = info.volume_interior_tetrahedra;
			anno = ioAttributes();
			anno.add( "unit", string("nm^3") );
			if ( h5w.nexus_write( dsnm, volume, anno ) != MYHDF5_SUCCESS ) { return false; }

			/*
			grpnm = "/entry" + to_string(entry_id) + "/point_set_wrapping/alpha_complex" + to_string(identifier) + "/interior_tetrahedra/tetrahedra";
			anno = ioAttributes();
			anno.add( "NX_class", string("NXcg_face_list_data_structure") );
			if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/dimensionality";
			dim = 3;
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/number_of_vertices";
			n_vrts = (apt_uint) ashp.vrts.size();
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, n_vrts, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/number_of_faces";
			n_fcts = (apt_uint) ashp.fcts.size();
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, n_fcts, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/vertex_identifier_offset";
			vrts_id_offset = 0;
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, vrts_id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

			dsnm = grpnm + "/face_identifier_offset";
			fcts_id_offset = 0;
			anno = ioAttributes();
			if ( h5w.nexus_write( dsnm, fcts_id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }
			*/
		}
	}

	return true;
}


void alphaShape::clear()
{
	vrts = vector<p3d64>();
	fcts = vector<tri3u>();
	info = ashape_info();

	the_objects = vector<CGAL::Object>();
	the_ft = vector<Alpha_shape_3::FT>();

	poly = Polyhedron();
	cvh = Surface();
	//Alpha_shape_3 as;
	wrap = Surface();
}
