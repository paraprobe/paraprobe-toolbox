/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_SURFACER_STRUCTS_H__
#define __PARAPROBE_SURFACER_STRUCTS_H__

#include "PARAPROBE_SurfacerEigen.h"


struct ashape_info
{
	size_t ncomponents;					//number of components of its regularized version
	size_t ninteriortets;				//number of interior tetrahedra
	double alpha;						//alpha value (1/nm)
	double offset; 						//offset value (nm) in case of alpha_wrappings
	double volume_interior_tetrahedra;	//accumulated volume of interior tetrahedra
	double volume_watertight_mesh;		//volume of the mesh if watertight, zero otherwise
	double dt_delaunay;
	double dt_ashape_compute;
	double dt_ashape_filter;			//e.g. find optimal to specific alpha shape
	double dt_ashape_getprims;
	double dt_ashape_getvol;
	double dt_ashape_getalphas;
	bool is_watertight;
	bool is_closed;					//eventually redundant
	ashape_info() : ncomponents(0), ninteriortets(0), alpha(MYZERO), offset(EPSILON),
			volume_interior_tetrahedra(MYZERO), volume_watertight_mesh(MYZERO),
			dt_delaunay(MYZERO), dt_ashape_compute(MYZERO), dt_ashape_filter(MYZERO),
				dt_ashape_getprims(MYZERO), dt_ashape_getvol(MYZERO), dt_ashape_getalphas(MYZERO),
				is_watertight(false), is_closed(false) {}
};

ostream& operator<<(ostream& in, ashape_info const & val);


struct p3d64
{
	double x;
	double y;
	double z;
	p3d64() : x(0.), y(0.), z(0.) {}
	p3d64(const double _x, const double _y, const double _z) :
		x(_x), y(_y), z(_z) {}
};

ostream& operator<<(ostream& in, p3d64 const & val);


struct vectorComparator
{
	bool operator()( const Eigen::Vector3d & a, const Eigen::Vector3d & b) const
	{
		return a[0] < b[0] ?
				true :
				(a[0] > b[0] ?
						false :
						(a[1] < b[1] ?
								true :
								(a[1] > b[1] ?
										false : (a[2] < b[2] ? true : false))));
	}
};


#endif
