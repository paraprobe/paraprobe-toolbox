v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-tessellator source file inclusion dependency graph

The header files are included in the following chain:
 1. PARAPROBE_ConfigTessellator.h/.cpp
 2. PARAPROBE_TessellatorStructs.h/.cpp
 3. PARAPROBE_TessellatorCore.h/.cpp
 4. PARAPROBE_TessellatorHDF5.h/.cpp (obsolete)
 5. PARAPROBE_TessellatorXDMF.h/.cpp (obsolete)
 6. PARAPROBE_TessellatorHdl.h/.cpp
 7. PARAPROBE_Tessellator.cpp
