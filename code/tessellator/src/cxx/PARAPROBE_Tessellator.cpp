/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_TessellatorHdl.h"


bool parse_configuration_from_nexus()
{
	tool_startup( "paraprobe-tessellator" );
	if ( ConfigTessellator::read_config_from_nexus( ConfigShared::ConfigurationFile ) == true ) {
		cout << "Configuration from file " << ConfigShared::ConfigurationFile << " was accepted" << "\n";
		cout << "This analysis has SimulationID " << "SimID." <<  ConfigShared::SimID << "\n";
		cout << "Results of this analysis are written to " << ConfigShared::OutputfileName << "\n";
		cout << endl;
		return true;
	}
	cerr << "Parsing configuration failed !" << "\n";
	return false;
}


void tessellate_reconstructed_dataset( const int r, const int nr )
{
	//allocate process-level instance of a tessellatorHdl. The instance handles all process-internal
	//processing, the instances synchronize across and communicate with each other during execution
	tessellatorHdl* tes = NULL;
	int localhealth = 1;
	try {
		tes = new tessellatorHdl;
		tes->set_myrank(r);
		tes->set_nranks(nr);
		//tes->commit_mpidatatypes();
	}
	catch (bad_alloc &exc) {
		cerr << "Rank " << r << " allocating surfacerHdl class object failed !" << "\n"; localhealth = 0;
	}

	//do we have all processes on board?
	if ( tes->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << r << " has recognized that not all processes are on board!" << "\n";
		delete tes; tes = NULL; return;
	}

	if ( tes->get_myrank() == MASTER ) {

		if ( tes->read_relevant_input() == false ) {
			cout << "Rank " << tes->get_myrank() << " failed reading essential pieces of the relevant input !" << "\n";
			localhealth = 0;
		}
	}

	//##MK::strictly speaking not necessary, but second order issue for about 80 processes as on TALOS...?
	//MPI_Barrier( MPI_COMM_WORLD );
	if ( tes->all_healthy_still( localhealth ) == false ) {
		cerr << "Rank " << tes->get_myrank() << " has recognized that not all data have arrived at the master !" << "\n";
		delete tes; tes = NULL; return;
	}

	if ( tes->crop_reconstruction_to_analysis_window() == true ) { //##MK:at some point make the analysis window specific for the task

		//process tessellations
		for ( auto tess_tskit = ConfigTessellator::TessellationTasks.begin(); tess_tskit != ConfigTessellator::TessellationTasks.end(); tess_tskit++ ) {

			cout << "Characterizing a tessellation of the dataset" << "\n";
			cout << *tess_tskit << "\n";

			tes->execute_local_workpackage( *tess_tskit );

			if ( tes->get_myrank() == MASTER ) {

				if ( tes->write_cell_info_h5( *tess_tskit ) == true ) {
					cout << "Writing info results for tessellation task " << tess_tskit->taskid << " was successful" << "\n";
				}
				else {
					cerr << "Writing info results for tessellation task " << tess_tskit->taskid << " failed !" << "\n";
				}

				/*
				if ( tes->write_cell_geometry_h5( *tess_tskit ) == true ) {
					cout << "Writing cell geometry results for tessellation task " << tess_tskit->taskid << " was successful" << "\n";
				}
				else {
					cerr << "Writing cell geometry results for tessellation task " << tess_tskit->taskid << " failed !" << "\n";
				}
				*/
			}
		} //next tessellation task

	}
	else {
		cout << "With the given filter settings no ion remains in the ROI !" << "\n";
	}

	//release resources
	delete tes; tes = NULL;
}


int main(int argc, char** argv)
{
	double tic = omp_get_wtime();
	string start_time = timestamp_now_iso8601();

	if ( argc == 1 || argc == 2 ) {
		string cmd_option = "--help";
		if ( argc == 2 ) { cmd_option = argv[1]; };
		command_line_help( cmd_option, "tessellator" );
		return 0;
	}
	else if ( argc == 3 ) {
		ConfigShared::SimID = stoul( argv[SIMID] );
		ConfigShared::ConfigurationFile = argv[CONTROLFILE];
		ConfigShared::OutputfilePrefix = "PARAPROBE.Tessellator.Results.SimID." + to_string(ConfigShared::SimID);
		ConfigShared::OutputfileName = ConfigShared::OutputfilePrefix + ".nxs";
		if ( init_results_nxs( ConfigShared::OutputfileName, "tessellator", start_time, 1 ) == false ) {
			return 0;
		}
	}
	else {
		return 0;
	}

//SETUP PROGRAM AND PARAMETER BUT DO NOT YET LOAD MEASUREMENT DATA
	if ( parse_configuration_from_nexus() == false ) {
		return 0;
	}

//SETUP MPI PARALLELISM
	int supportlevel_desired = MPI_THREAD_FUNNELED;
	int supportlevel_provided = 0;
	int nr = 1;
	int r = MASTER;
	MPI_Init_thread( &argc, &argv, supportlevel_desired, &supportlevel_provided); //lets go MPI parallel...
	if ( supportlevel_provided < supportlevel_desired ) {
		cerr << "Insufficient threading capabilities of the MPI library to accomplish tasks!" << "\n";
		MPI_Finalize(); //required because threading insufficiency does not imply process is incapable to work at all
		return 0;
	}
	else {
		MPI_Comm_size(MPI_COMM_WORLD, &nr);
		MPI_Comm_rank(MPI_COMM_WORLD, &r);
	}
	cout << r << "-th MPI process initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

	if ( nr == SINGLEPROCESS ) {

		cout << "Rank " << r << " initialized, we are now MPI_COMM_WORLD parallel using MPI_THREAD_FUNNELED" << "\n";

//EXECUTE SPECIFIC TASK
		tessellate_reconstructed_dataset( r, nr );
	}
	else {
		cerr << "Rank " << r << " currently paraprobe-tessellator is implemented for a single process only !" << "\n";
	}

//DESTROY MPI
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();
	cout << "Rank " << r << " left MPI_COMM_WORLD deconstructed" << "\n";

	if ( finish_results_nxs( ConfigShared::OutputfileName, start_time, tic, 1 ) == true ) {
		cout << "paraprobe-tessellator success" << endl;
	}
	return 0;
}
