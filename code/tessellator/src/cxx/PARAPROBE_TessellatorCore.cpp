/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_TessellatorCore.h"

tess::tess()
{
	io_cellifo = vector<cellinfo>();
	io_cellgeom = vector<cellgeom>();
	io_cell_vertices = vector<p3d>();
	io_cell_topology = vector<apt_uint>();
	io_cell_evapid = vector<apt_uint>();

	globalworld = aabb3d();
	localhalo = aabb1d();
	localinside = aabb1d();
	has_zmi_halo = false;
	has_zmx_halo = false;
}


tess::~tess()
{
}


p3i tess::identify_blockpartitioning(
		const apt_uint p_total, const int p_perblock_target, aabb3d const & roi )
{
	p3i out = p3i( 1, 1, 1);
	if ( roi.xsz > EPSILON ) {
		apt_real yrel = roi.ysz / roi.xsz;
		apt_real zrel = roi.zsz / roi.xsz;
		apt_real ix = pow(
				(static_cast<apt_real>(p_total) / static_cast<apt_real>(p_perblock_target))
				/ (yrel * zrel), (1.f/3.f) );

		out.x = ( static_cast<int>(floor(ix)) > 0 ) ? static_cast<int>(floor(ix)) : 1;
		apt_real iy = yrel * ix;
		out.y = ( static_cast<int>(floor(iy)) > 0 ) ? static_cast<int>(floor(iy)) : 1;
		apt_real iz = zrel * ix;
		out.z = ( static_cast<int>(floor(iz)) > 0 ) ? static_cast<int>(floor(iz)) : 1;
	}
	return out;
}


wallinfo tess::identify_cell_wallcontact( vector<int> const & nbors )
{
	//does cell make boundary contact or not ? (indicated by negative neighbor
	//numeral info of neighbor details which domain wall cuts the cell into a potential incorrect geometry
	wallinfo out = wallinfo();
	for( auto it = nbors.begin(); it != nbors.end(); ++it ) {
		if ( *it >= 0 ) { //most likely case
			continue;
		}
		else { //a cell that has boundary contact
			int thisone = *it; //abs(*it); //-1 to 1, -2 to 2, and so forth, Voro++ domain wall neighbors are -1,-2,-3,-4,-5,-6
			switch (thisone)
			{
				case -1:
				{
					out.xmi_touch = true; out.global_touch = true; break;
				}
				case -2:
				{
					out.xmx_touch = true; out.global_touch = true; break;
				}
				case -3:
				{
					out.ymi_touch = true; out.global_touch = true; break;
				}
				case -4:
				{
					out.ymx_touch = true; out.global_touch = true; break;
				}
				//##MK::we use a partitioning into threadlocal z domains of a global nonperiodic tess
				//##MK::hence check additionally whether haloregion was sufficient for
				//truncating the cell only through the halo regions and not the domain walls
				//##MK::this consistency check identifies whether cells close to the
				//threadlocal domain boundaries have correct geometry
				case -5:
				{
					out.local_touch = true;
					if ( has_zmi_halo == false ) {
						out.zmi_touch = true;
						out.global_touch = true;
					}
					break; //with respect to the global container !
				}
				case -6:
				{
					out.local_touch = true;
					if ( has_zmx_halo == false ) {
						out.zmx_touch = true;
						out.global_touch = true;
					}
					break; //with respect to the global container !
				}
				default:
				{
					break;
				}
			}
		}
	} //all neighbors have to be tested to know for sure with which boundaries we make contact
	return out;
}


//allbox_global, mybox_total, mybox_interior, myions_interiors, myions_ghosts, ions.ionpp3 );
bool tess::tessellate( aabb3d const & globalbox, aabb1d const & halobox, aabb1d const & insidebox,
		vector<apt_uint> const & inside, vector<apt_uint> const & ghost, vector<p3d> const & pp3,
		vector<p3dinfo> const & pp3ifo, const bool has_zmi, const bool has_zmx, tess_task_info const & info )
{
	double mytic = omp_get_wtime();
	double mytoc = 0.;

	//5.0 as target value based on empirical result by Rycroft, http://math.lbl.gov/voro++/examples/timing_test/
	if ( (inside.size() + ghost.size()) >= static_cast<size_t>(I32MX) ) {
		#pragma omp critical
		{
			cerr << "Thread " << omp_get_thread_num() << " inside + ghost exceeds INT32MX-1 which this implementation does not support!" << "\n";
		}
		return false;
	}

	globalworld = globalbox;
	localhalo = halobox;
	localinside = insidebox;
	has_zmi_halo = has_zmi;
	has_zmx_halo = has_zmx;

	//blockpartitioning is the essential trick in Voro++ to reduce spatial querying costs
	//too few blocks too many points to test against
	//too many blocks too much memory overhead and cache trashing (L1,L2 and not to forget "page" TLB cache)
	aabb3d box = aabb3d( 	globalworld.xmi - EPSILON,
							globalworld.xmx + EPSILON,
							globalworld.ymi - EPSILON,
							globalworld.ymx + EPSILON,
							localhalo.zmi - EPSILON,
							localhalo.zmx + EPSILON    );
	box.scale();

	//##MK::if inside + ghost is already checked to be within <= I32MX then it for sure matches on U32MX so
	p3i blocks = identify_blockpartitioning(
			(apt_uint) (inside.size() + ghost.size()), ConfigTessellator::IonsPerBlock, box );
	bool periodicbox = false;
	container con(	box.xmi, box.xmx, box.ymi, box.ymx, box.zmi, box.zmx,
					blocks.x, blocks.y, blocks.z,
					periodicbox, periodicbox, periodicbox,
					ConfigTessellator::IonsPerBlock );

	io_cellifo = vector<cellinfo>();
	io_cellifo.reserve( inside.size() + ghost.size() );

	//buildstats stats = buildstats( blocks.x, blocks.y, blocks.z, ionportion, con.total_particles() );
	int i = 0; //temporary ion ID to map ion position in the container to the implicit order of the above linear walk through along ionpp1 and ionifo data arrays
	//MK::need to start i at zero only then
	//Voro++ .pid() getter will return us this ion ID
	//MK:: DO NOT JUMBLE THIS LOCAL ION ID UP WITH THE GLOBAL EVAPORATION ID OF AN ION !
	unsigned short myrankid = (unsigned short) MASTER; //##MK::single process
	unsigned short mythreadid = ( omp_get_thread_num() < static_cast<int>(U16MX) )
			? (unsigned short) omp_get_thread_num() : U16MX;
	for( auto it = inside.begin(); it != inside.end(); it++ ) {
		io_cellifo.push_back( cellinfo(*it, pp3ifo[*it].dist, myrankid, mythreadid, false) );
		con.put( i, pp3[*it].x, pp3[*it].y, pp3[*it].z );
		//we need to accumulate local ionID
		i++;
	}
	for( auto jt = ghost.begin(); jt != ghost.end(); jt++ ) {
		io_cellifo.push_back( cellinfo(*jt, RMX, myrankid, mythreadid, true) ); //edge distance info not relevant for ghosts
		con.put( i, pp3[*jt].x, pp3[*jt].y, pp3[*jt].z );
		//we need to accumulate local ionID
		i++;
	}

	/*
	apt_real derosion = ConfigTessellator::CellErosionDistance;
	unsigned short rr = static_cast<unsigned short>(roundrobin_rankid);
	unsigned short thr = static_cast<unsigned short>(omp_get_thread_num());
	*/

	bool compute_cell_geometry = info.hasGeometry;
	//bool compute_cell_edge_threshold = false;
	if ( compute_cell_geometry == true ) {
		//compute_cell_edge_threshold = info.hasEdgeThresholds; //only available in combination with rendering the geometry of cells
		io_cellgeom = vector<cellgeom>();
		i = 0;
		for( auto it = inside.begin(); it != inside.end(); it++ ) {
			io_cellgeom.push_back( cellgeom( 0, 0, *it, 0, 0 ) );
			i++;
		}
		io_cell_vertices = vector<p3d>();
		io_cell_topology = vector<apt_uint>();
		io_cell_evapid = vector<apt_uint>();
	}

	apt_uint nvertices_cur = 0;
	apt_uint nfacets_cur = 0;
	c_loop_all cl(con);
	voronoicell_neighbor c;
	if (cl.start()) { //this is an incremental computing of the tessellation which holds at no point the entire tessellation in main memory
		do {
			//Voro++ is not order conserving inasmuch as the library does not guaranteed that the iteration over cl.start() to end() machines
			//off the cells in ascending order we should not assume an order instead use the identifier of each cell which we have set to the
			//evaporation ID/ion ID
			int clpid = cl.pid();

			size_t clpid_idx = (size_t) clpid;
			if ( io_cellifo[clpid_idx].ghost == false ) { //ghost cells do not have to be computed as they only assure that cells get their
				//correct bisectioning planes towards the halo of the thread-local domain

				//mystats.CellsInsideAll++;
				//if ( here->second.d >= derosion ) { //an ion sufficiently far enough from the dataset edge
				//mystats.CellsInsideNotEroded++;

				if ( con.compute_cell(c, cl) == true ) { //an ion for which the tessellation was successful

					//mystats.CellsInsideConstructible++;

					io_cellifo[clpid_idx].success = true;
					io_cellifo[clpid_idx].volume = c.volume(); //MK::cell volume

					vector<int> first_neigh; //MK::number of first order neighbors including negative values in case of domain contact
					c.neighbors(first_neigh);

					/*
					vector<int> first_f_vert; //MK::vertices and facets
					c.face_vertices(first_f_vert);
					vector<double> first_v;
					double xx, yy, zz; //barycenter
					cl.pos( xx, yy, zz );
					c.vertices(xx, yy, zz, first_v);
					*/

					io_cellifo[clpid_idx].wallcontact = identify_cell_wallcontact( first_neigh );

					//extract polygon mesh of the polyhedral Voronoi cell
					if ( compute_cell_geometry == false ) { //in most cases we do not want to report
						//the geometry of the cell as this is memory and storage demanding
						continue;
					}
					else {
						//apt_uint evapid_i = io_cellifo[clpid_idx].evapid;

						//characterize center of the cell
						double x, y, z;
						cl.pos(x, y, z);

						//characterize convex polyhedron surface facet for visualizing the cell
						vector<int> fvert; //MK::vertex indices
						c.face_vertices(fvert);
						vector<double> verts; //MK::vertex coordinate values, not coordinate triplets!
						c.vertices(x, y, z, verts);

						io_cellgeom[clpid_idx].offset_vertices = io_cell_vertices.size();
						io_cellgeom[clpid_idx].offset_facets = io_cell_topology.size();

						//report entire geometry of the cell
						//##MK::export geometry of the cells
						/*
						if ( compute_cell_edge_threshold == false ) {
							//http://math.lbl.gov/voro++/examples/polygons/
							apt_uint nvertices_old = nvertices_cur; //##MK::reduce to unique vertices to save storage
							apt_uint nfacets_old = nfacets_cur;
							int j = 0;
							for( size_t i = 0; i < first_neigh.size(); i++ ) { //O(N) processing
								io_cell_topology.push_back((apt_uint) 3); //XDMF keyword to visualize an n-polygon
								io_cell_topology.push_back((apt_uint) fvert[j]); //how many edges does the polygon have promotion uint32 to size_t no problem
								for( int k = 0; k < fvert[j]; k++) { //generating 3d points with implicit indices 0, 1, 2, ....
									//the structure of an fvert array is a contiguous set of blocks, neigh.size() blocks i.e. number of non-degenerate facets
									//each block has first the number of vertices per facet, followed by indices which double numbers to use in verts to compose a 3d coordinate triplet
									//the structure of an verts array is a collection of double triplets for vertex coordinates supporting the the facet polygons
									int l = 3*fvert[j+k+1];
									io_cell_vertices.push_back( p3d((apt_real) verts[l+0], (apt_real) verts[l+1], (apt_real) verts[l+2]) );
									//##MK::narrowing conversion from double to float suffices for visualization purposes but not necessarily for subsequent processing of the Voronoi cell polyhedra !
									io_cell_topology.push_back(nvertices_cur);
									nvertices_cur++;
								}
								nfacets_cur++;
								io_cell_evapid.push_back(evapid_i); //duplicating of labels for each polygon required for XDMF because for mixed xdmf topologies the cell is the polygon not the polyhedron !
								j += fvert[j] + 1;
							}

							io_cellgeom[clpid_idx].n_vertices = nvertices_cur - nvertices_old;
							io_cellgeom[clpid_idx].n_facets = nfacets_cur - nfacets_old;
						}
						else {
							//report geometry of the surface which delineates the blob of cells matching a certain threshold distance
							//##MK:: !!!!! currently implemented and working only when using a single thread !!!
							//##MK::because only then io_cellifo contains information about all cells
							if ( omp_get_num_threads() == SINGLETHREADED ) {
								apt_real threshold = 3.5; //8.; //nm
								//if( io_cellifo[clpid_idx].edgedist < threshold ) { //d_i < threshold, render only facets of cells not less than threshold
								if ( io_cellifo[clpid_idx].edgedist >= threshold ) { //d_i < threshold, render only facets of cells not less than threshold
								//render boundary between affected and unaffected cells, is not possible as this would require that we know already the neighbors of very cell but therefore we would need to compute the tessellation twice!
								//if( io_cellifo[clpid_idx].wallcontact.local_touch == false && io_cellifo[clpid_idx].wallcontact.global_touch == false ) {  //a cell without a geometry affected
									apt_uint nvertices_old = nvertices_cur; //##MK::reduce to unique vertices to save storage
									apt_uint nfacets_old = nfacets_cur;
									int j = 0;
									for( size_t i = 0; i < first_neigh.size(); i++ ) {
										int clpid_j = first_neigh[i];
										if ( clpid_j >= 0 ) { //do not render what flags that this is a collision with the domain wall !
											size_t clpid_idx_j = (size_t) clpid_j;
											apt_uint evapid_j = io_cellifo[clpid_idx_j].evapid;
											//if ( evapid_i > evapid_j ) { //render a facet between i,j and j,i respectively only once!
												//if ( io_cellifo[clpid_idx_j].edgedist >= threshold ) {
												if ( io_cellifo[clpid_idx_j].edgedist < threshold ) {
													//render facet if the neighbor is below threshold
													io_cell_topology.push_back((apt_uint) 3); //XDMF keyword to visualize an n-polygon
													io_cell_topology.push_back((apt_uint) fvert[j]); //how many edges does the polygon have promotion uint32 to size_t no problem
													for( int k = 0; k < fvert[j]; k++) { //generating 3d points with implicit indices 0, 1, 2, ....
														//the structure of an fvert array is a contiguous set of blocks, neigh.size() blocks i.e. number of non-degenerate facets
														//each block has first the number of vertices per facet, followed by indices which double numbers to use in verts to compose a 3d coordinate triplet
														//the structure of an verts array is a collection of double triplets for vertex coordinates supporting the the facet polygons
														int l = 3*fvert[j+k+1];
														io_cell_vertices.push_back( p3d((apt_real) verts[l+0], (apt_real) verts[l+1], (apt_real) verts[l+2]) );
														//##MK::narrowing conversion from double to float suffices for visualization purposes but not necessarily for subsequent processing of the Voronoi cell polyhedra !
														io_cell_topology.push_back(nvertices_cur);
														nvertices_cur++;
													}
													nfacets_cur++;
													io_cell_evapid.push_back(evapid_i); //duplicating of labels for each polygon required for XDMF because for mixed xdmf topologies the cell is the polygon not the polyhedron !
												}
												j += fvert[j] + 1;
											//} //i > j
										} //no neighbor which is a domain wall (local or global)
									} //for every neighboring facets
									io_cellgeom[clpid_idx].n_vertices = nvertices_cur - nvertices_old;
									io_cellgeom[clpid_idx].n_facets = nfacets_cur - nfacets_old;
								} //d_i >= threshold
							} //
						}
						*/
					}

				} //analyzable cell
				else {
					io_cellifo[clpid_idx].success = false;
					#pragma omp critical
					{
						cerr << "Thread " << omp_get_thread_num() << " detected that cell/ion " << clpid_idx << " was not computable properly !" << "\n";
					}
				}
			} //not a ghost ion
		} while (cl.inc());
		//thread machines off all its cells
	}

	/*
	//mystats = stats;
	*/

	mytoc = omp_get_wtime();

	#pragma omp critical
	{
		/*
		cout << "Rank " << rr << " thread " << thr << " blocksxyz " << mystats.BlocksX << ";" << mystats.BlocksY << ";" << mystats.BlocksZ << ";" << " ion/con " << mystats.IonsPerRegion << ";" << mystats.IonsInContainer << "\n";
		cout << "Rank " << rr << " thread " << thr << " io_cellwall/io_cellifo/io_cellprof.size() " << io_cellwall.size() << ";" << io_cellifo.size() << ";" << io_cellprof.size() << "\n";
		cout << "Rank " << rr << " thread " << thr << " ghost/insideall/healthy/ill.size() " << mystats.CellsHalo << ";" << mystats.CellsInsideAll << ";" << mystats.CellsInsideHealthy << ";" << mystats.CellsInsideIll << "\n";
		*/
		cout << "Thread " << omp_get_thread_num() << " took " << (mytoc - mytic) << " s, nvertices_cur " << nvertices_cur << " nfacets_cur " << nfacets_cur << "\n";
	}

	return true;
}
