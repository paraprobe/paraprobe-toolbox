/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_TESSELLATOR_CORE_H__
#define __PARAPROBE_TESSELLATOR_CORE_H__

#include "PARAPROBE_TessellatorStructs.h"


class tess
{
	//implementing a thread-local object keeping the heavy- and metadata
	//of the thread-local part of the tessellation of the entire dataset
public:
	tess();
	~tess();

	bool tessellate( aabb3d const & globalbox,
			aabb1d const & halobox,
			aabb1d const & insidebox,
			vector<apt_uint> const & inside,
			vector<apt_uint> const & ghost,
			vector<p3d> const & pp3,
			vector<p3dinfo> const & pp3ifo,
			const bool has_zmi, const bool has_zmx,
			tess_task_info const & info );

	vector<cellinfo> io_cellifo;
	vector<cellgeom> io_cellgeom;
	vector<p3d> io_cell_vertices;
	vector<apt_uint> io_cell_topology; 	//XDMF topology
	vector<apt_uint> io_cell_evapid;

private:
	aabb3d globalworld;					//externally detailed bounding box typically entire APT tip
	aabb1d localhalo;					//thread memory region bounding box with halo
	aabb1d localinside;					//thread memory region bounding box without halo
	bool has_zmi_halo;
	bool has_zmx_halo;

	//MK::relevant identifier for wall contact have to be of int type
	//because of the fact that Voro++ internally uses int !
	p3i identify_blockpartitioning(
			const apt_uint p_total,
			const int p_perblock_target,
			aabb3d const & roi );
	wallinfo identify_cell_wallcontact( vector<int> const & nbors );
};

#endif
