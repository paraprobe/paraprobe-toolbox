/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_TessellatorHdl.h"


tessellatorHdl::tessellatorHdl()
{
	window = vector<unsigned char>();
	regions = vector<tess*>();
	tess_tictoc = profiler();
}


tessellatorHdl::~tessellatorHdl()
{
	for( size_t i = 0; i < regions.size(); i++ ) {
		if ( regions.at(i) != NULL ) {
			delete regions[i]; regions[i] = NULL;
		}
	}
	regions = vector<tess*>();
}


bool tessellatorHdl::read_relevant_input()
{
	double tic = MPI_Wtime();

	if ( read_xyz_from_h5( ConfigTessellator::InputfileDataset, ConfigTessellator::ReconstructionDatasetName ) == true &&
			read_ranging_from_h5( ConfigTessellator::InputfileIonTypes, ConfigTessellator::IonTypesGroupName ) == true &&
				read_ionlabels_from_h5( ConfigTessellator::InputfileIonTypes, ConfigTessellator::IonTypesGroupName ) == true ) {

		cout << "Rank " << "MASTER" << " all relevant input was loaded successfully" << "\n";

		//build also here a default array with pieces of information for all ions
		if ( ions.ionpp3.size() > 0 && ions.ionifo.size() != ions.ionpp3.size() ) {
			try {
				ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(),
						p3dinfo( RMX, UNKNOWNTYPE, UNKNOWNTYPE, 0x01, WINDOW_ION_EXCLUDE ) );
				//assume all ions are infinitely large from the edge, of unknown (default ion type), and multiplicity one (i.e. 0x01)
			}
			catch (bad_alloc &croak) {
				cerr << "Allocation of information array for the ions failed !" << "\n";
				return false;
			}
		}

		if ( ConfigTessellator::InputfileIonToEdgeDistances.h5fn != "" ) {
			if ( read_distance_dsetedge_from_h5( ConfigTessellator::InputfileIonToEdgeDistances.h5fn,
					ConfigTessellator::InputfileIonToEdgeDistances.dsnm ) == true ) {

				cout << "Rank " << "MASTER" << " all ion to edge distances were loaded successfully" << "\n";
			}
		}

		double toc = MPI_Wtime();
		memsnapshot mm = memsnapshot();
		tess_tictoc.prof_elpsdtime_and_mem( "ReadRelevantInput", APT_XX, APT_IS_SEQ, mm, tic, toc);
		return true;
	}

	return false;
}


bool tessellatorHdl::crop_reconstruction_to_analysis_window()
{
	double tic = MPI_Wtime();

	bool roi_has_accepted_ions = false;
	//assume first we work with the entire reconstruction, hence the ions.ionifo.mask1 is ANALYZE_YES for all
	//successively remove points from the analysis window that meet or not certain properties, like position, associated iontype, multiplicity, etc.
	if ( ConfigTessellator::WindowingMethod == ENTIRE_DATASET ) {
		apply_all_filter();
	}
	else if ( ConfigTessellator::WindowingMethod == UNION_OF_PRIMITIVES ) { //##MK::debug implementation
		apply_none_filter();

		//define which ions to include as a union of different geometric primitives
		apply_roi_ensemble_accept_filter( CG_SPHERE, ConfigTessellator::WindowingSpheres );
		apply_roi_ensemble_accept_filter( CG_CYLINDER, ConfigTessellator::WindowingCylinders );
		apply_roi_ensemble_accept_filter( CG_CUBOID, ConfigTessellator::WindowingCuboids );
	}
	else if ( ConfigTessellator::WindowingMethod == BITMASKED_POINTS ) {
		cerr << "Region-of-interest filtering mode BITMASKED_POINTS is not implemented yet!" << "\n";
		return roi_has_accepted_ions;
	}
	else {
		cerr << "Facing a yet unimplemented region-of-interest filtering mode !" << "\n";
		return roi_has_accepted_ions;
	}

	if ( ConfigTessellator::LinearSubSamplingRange.min > 0 ||
			ConfigTessellator::LinearSubSamplingRange.incr > 1 ||
				ConfigTessellator::LinearSubSamplingRange.max < ions.ionpp3.size() ) {
		//execute only when we can really restrict something
		apply_linear_subsampling_filter( ConfigTessellator::LinearSubSamplingRange );
	}
	else {
		//ignore this filter as it will have no effect
	}

	if ( ConfigTessellator::IontypeFilter.is_whitelist == true ) {
		apply_iontype_remove_filter( ConfigTessellator::IontypeFilter.candidates, false );
	}
	else if ( ConfigTessellator::IontypeFilter.is_blacklist == true ) {
		apply_iontype_remove_filter( ConfigTessellator::IontypeFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	if ( ConfigTessellator::HitMultiplicityFilter.is_whitelist == true ) {
		apply_multiplicity_remove_filter( ConfigTessellator::HitMultiplicityFilter.candidates, false );
	}
	else if ( ConfigTessellator::HitMultiplicityFilter.is_blacklist == true ) {
		apply_multiplicity_remove_filter( ConfigTessellator::HitMultiplicityFilter.candidates, true );
	}
	else {
		//ignore this filter
	}

	check_filter();

	//define at least two window classes, ions are assigned a class,
	//window class 0x00 ions are discarded, ions of all other classes are considered
	window = vector<unsigned char>( ions.ionpp3.size(), WINDOW_ION_EXCLUDE );
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			window[i] = ANALYZE_YES; //WINDOW_ION_INCLUDE_DEFAULT_CLASS;
			roi_has_accepted_ions = true;
		}
		//else, nothing to do, window[i] already WINDOW_ION_EXCLUDE
	}

	double toc = MPI_Wtime();
	memsnapshot mm = tess_tictoc.get_memoryconsumption();
	tess_tictoc.prof_elpsdtime_and_mem( "CropReconstructionToAnalysisWindow", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return roi_has_accepted_ions;
}


void tessellatorHdl::execute_local_workpackage( tess_task_info & info )
{
	//spatial decomposition
	double tic = MPI_Wtime();
	double toc = tic;

	cout << "Rank " << get_myrank() << " identifying which portion of the dataset should become tessellated ..." << "\n";

	//##MK::currently we use a z-position quantile-based work partitioning, for reconstructed datasets from specimens
	//with a larger shank angle this creates a situation where the bottom parts are much wider than the top part
	//so that for a given z-coordinate quantile the thread taking the bottom part has relatively a larger number of
	//ions to process than the thread at the top
	//##MK::a better strategy for better load balance for the future would be to split based on sorting first in z
	//but then let each thread collect ions from a contiguous slab until the processes on average the 1/number threads fraction
	//of the ion total, resulting in that the thread responsible for the top part of the specimen has a longer z slice
	//the process than the thread processing the bottom of the dataset to compensate for the widening of the point cloud

	//identify_ioncloud_dimensions() respecting the window and sort ascending order
	ions.aabb = aabb3d();
	vector<apt_real> zcoordinates;
	for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			ions.aabb.possibly_enlarge_me( ions.ionpp3[i] );
			zcoordinates.push_back( ions.ionpp3[i].z );
		}
	}
	ions.aabb.scale();
	aabb3d allbox_global = ions.aabb;

	toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	tess_tictoc.prof_elpsdtime_and_mem( "ComputeAABBofAnalysisWindow", APT_XX, APT_IS_SEQ, mm, tic, toc );
	tic = MPI_Wtime();

	if ( zcoordinates.size() > 0 ) {
		sort( zcoordinates.begin(), zcoordinates.end() ); //sorts by default in ascending order
	}
	else {
		cout << "WARNING::No ions to tessellate !" << "\n"; return;
	}

	toc = MPI_Wtime();
	tess_tictoc.prof_elpsdtime_and_mem( "SortZCoordinates", APT_XX, APT_IS_SEQ, mm, tic, toc );
	tic = MPI_Wtime();

	//release eventually allocated tessellator instances
	for( size_t i = 0; i < regions.size(); i++ ) {
		if ( regions.at(i) != NULL ) {
			delete regions[i]; regions[i] = NULL;
		}
	}

	//multithreaded tessellating
	bool healthy = true;
	#pragma omp parallel shared(zcoordinates, allbox_global, healthy)
	{
		int mt = omp_get_thread_num();
		int nt = omp_get_num_threads();
		aabb1d mybox_interior = aabb1d(); //z-region of ions.aabb thread mt takes care of
		aabb1d mybox_total = aabb1d(); //z-region of ions.aabb thread mt takes care of plus ConfigTessellator::GuardZone on the top and bottom for threadIDs > MASTER < nt-1

		#pragma omp master
		{
			regions = vector<tess*>( nt, NULL );
		}
		//necessary as otherwise write back conflict on workers from threads
		#pragma omp barrier

		//setup thread-local tessellator instance using preferentially thread-local memory
		tess* thrhdl = NULL;
		try {
			thrhdl = new tess;
		}
		catch (bad_alloc &mecroak) {
			#pragma omp critical
			{
				cerr << "Rank " << get_myrank() << " thread " << mt << " unable to allocate an tess instance!" << "\n";
				healthy = false;
			}
		}

		//necessary all threads should only execute if all tess instances were allocated
		#pragma omp barrier

		//if all are healthy we diverge and continue processing our own results
		if ( healthy == true ) {
			apt_real zmi = RMI;
			apt_real zmx = RMX;
			bool i_have_zmi_halo = true;
			bool i_have_zmx_halo = true;
			if ( nt == SINGLETHREADED ) { //single thread needs no guard zones
				zmi = zcoordinates[0];
				zmx = zcoordinates[zcoordinates.size()-1];
				i_have_zmi_halo = false;
				i_have_zmx_halo = false;
			}
			else { //multithreaded, nt >= 2
				//guard zones at split positions to assure that cells facets touch on correct neighbors instead of domain wall
				//touching on +-x +-y domain walls instead is okay but not along the split direction z !
				//apt_real zheight = zcoordinates[zcoordinates.size()-1] - zcoordinates[0];
				if ( mt == MASTER ) { //"first one", master needs only a "bottom" guard zone
					zmi = zcoordinates[0];
					zmx = quantile( zcoordinates, (apt_real) (mt+1) / (apt_real) nt );
					i_have_zmi_halo = false;
					i_have_zmx_halo = true;
				}
				else if ( mt == (nt-1)) { //"last one", needs only a "top" guard zone
					zmi = quantile( zcoordinates, (apt_real) mt / (apt_real) nt );
					zmx = zcoordinates[zcoordinates.size()-1];
					i_have_zmi_halo = true;
					i_have_zmx_halo = false;
				}
				else { //"middle regions" need a "bottom" and "top" guard zone
					zmi = quantile( zcoordinates, (apt_real) mt / (apt_real) nt );
					zmx = quantile( zcoordinates, (apt_real) (mt+1) / (apt_real) nt );
					i_have_zmi_halo = true;
					i_have_zmx_halo = true;
				}
			}
			mybox_interior = aabb1d( zmi, zmx );
			mybox_total = aabb1d( zmi - ConfigTessellator::GuardZoneFactor, zmx + ConfigTessellator::GuardZoneFactor );
			mybox_interior.scale();
			mybox_total.scale();
			#pragma omp critical
			{
				cout << "Thread " << mt << " processing zmi_halo, zmi, zmx, zmx_halo " <<
						mybox_total.zmi << ";" << mybox_interior.zmi <<
						";" << mybox_interior.zmx << ";" << mybox_total.zmx <<
						" has_zmi_halo " << i_have_zmi_halo << " has_zmx_halo " << i_have_zmx_halo << "\n";
			}

			//threads fish their portion from the dataset
			vector<apt_uint> myions_interiors; //ions in or on the boundary of interior
			vector<apt_uint> myions_ghosts; //ions in or on the boundary of the halo but not in the interior
			if ( mt != (nt-1)) {
				for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
					if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
						apt_real discriminator = ions.ionpp3[i].z;
						if ( discriminator < mybox_total.zmi || discriminator > mybox_total.zmx ) { //most are not inside, unless we work with only one thread, e.g. already for two threads approximately half out
							continue;
						}
						else { //inside mybox_total
							if ( discriminator >= mybox_interior.zmi && discriminator < mybox_interior.zmx ) { //MK:: < zmx not <= zmx,
								//otherwise points which are located floating-point-precise on the boundary will be processed twice !
								//halo is usually thinner than interior z slab, so inside interior is the more likely case
								myions_interiors.push_back( (apt_uint) i );
							}
							else { //a ghost
								myions_ghosts.push_back( (apt_uint) i );
							}
						}
					}
				}
			}
			else {
				for( size_t i = 0; i < ions.ionpp3.size(); i++ ) {
					if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
						apt_real discriminator = ions.ionpp3[i].z;
						if ( discriminator < mybox_total.zmi || discriminator > mybox_total.zmx ) { //most are not inside, unless we work with only one thread, e.g. already for two threads approximately half out
							continue;
						}
						else { //inside mybox_total
							if ( discriminator >= mybox_interior.zmi && discriminator <= mybox_interior.zmx ) {
								//the last thread gets by-virtue-of construction the ions with the largest z coordinate
								//values, as that last thread has no guard zone with even higher z coordinates
								//the single ion with the maximum z value would be excluded in case that one
								//were to test < mybox_interior.zmx!
								//halo is usually thinner than interior z slab, so inside interior is the more likely case
								myions_interiors.push_back( (apt_uint) i );
							}
							else { //a ghost
								myions_ghosts.push_back( (apt_uint) i );
							}
						}
					}
				}
			}
			//branch is pulled out and code duplicated in order to avoid many
			//branches inside the for loop

			thrhdl->tessellate( allbox_global, mybox_total, mybox_interior,
					myions_interiors, myions_ghosts, ions.ionpp3, ions.ionifo,
					i_have_zmi_halo, i_have_zmx_halo, info );

			//register point to solver instance with results with the master thread
			#pragma omp critical
			{
				if ( regions[mt] == NULL ) {
					regions[mt] = thrhdl;
				}
				else {
					cerr << "Thread " << omp_get_thread_num() << " was unable to register its solver instance with the master thread !" << "\n";
				}
			}
			//#pragma omp flush regions
		}
		//##MK::else error management
	} //end of parallel region

	toc = MPI_Wtime();
	mm = tess_tictoc.get_memoryconsumption();
	tess_tictoc.prof_elpsdtime_and_mem( "MultithreadedTessellating", APT_XX, APT_IS_PAR, mm, tic, toc);
	cout << "Rank " << get_myrank() << " hybrid tessellating took " << (toc-tic) << " seconds" << "\n";
}


bool tessellatorHdl::write_cell_info_h5( tess_task_info & info )
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( ConfigShared::OutputfileName );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	apt_uint entry_id = 1;
	//apt_uint proc_id = info.taskid + 1;
	grpnm = "/entry" + to_string(entry_id) + "/tessellation";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_results") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	//window
	grpnm = "/entry" + to_string(entry_id) + "/tessellation/window";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_boolean_filter_mask") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_ions";
	apt_uint n_ions = (apt_uint) ions.ionifo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/bitdepth";
	apt_uint bit_depth = sizeof(unsigned char) * 8;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, bit_depth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/mask";
	//use window in-place
	vector<unsigned char> u08_bitfield;
	BitPacker pack_bits;
	pack_bits.uint8_to_bitpacked_uint8( window, u08_bitfield );
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	u08_bitfield = vector<unsigned char>();

	//edge effects via wall contact or not
	grpnm = "/entry" + to_string(entry_id) + "/tessellation/wall_contact_global";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_filter_boolean_mask") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/number_of_objects";
	//apt_uint n_ions = (apt_uint) ions.ionifo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/bitdepth";
	//apt_uint bit_depth = sizeof(unsigned char) * 8;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, bit_depth, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/mask";
	vector<unsigned char> u08_wall = vector<unsigned char>( ions.ionpp3.size(), 0x00 );
	for ( size_t thr = 0; thr < regions.size(); thr++ ) {
		if ( regions.at(thr) != NULL ) {
			for( auto it = regions[thr]->io_cellifo.begin(); it != regions[thr]->io_cellifo.end(); it++ ) {
				if ( it->ghost == false ) {
					size_t ion_idx = it->evapid;
					//hitting an edge contact is seldom, especially a specific one
					if ( it->wallcontact.global_touch == false) { /*nop*/ }
					else { u08_wall[ion_idx] = 0x01; }
				}
			} //for all cells of thread
		} //for all threads
	}
	pack_bits.uint8_to_bitpacked_uint8( u08_wall, u08_bitfield );
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({u08_bitfield.size()}, {u08_bitfield.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u08_bitfield,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	u08_bitfield = vector<unsigned char>();


	vector<pair<int, string>> walls;
	walls.push_back( pair<int, string>(-1, "left") );
	walls.push_back( pair<int, string>(-2, "right") );
	walls.push_back( pair<int, string>(-3, "front") );
	walls.push_back( pair<int, string>(-4, "rear") );
	walls.push_back( pair<int, string>(-5, "bottom") );
	walls.push_back( pair<int, string>(-6, "top") );
	for( auto wall_it = walls.begin(); wall_it != walls.end(); wall_it++ ) {

		grpnm = "/entry" + to_string(entry_id) + "/tessellation/wall_contact_" + wall_it->second;
		anno = ioAttributes();
		anno.add( "NX_class", string("NXcs_filter_boolean_mask") );
		if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = grpnm + "/number_of_objects";
		//apt_uint n_ions = (apt_uint) ions.ionifo.size();
		anno = ioAttributes();
		if ( h5w.nexus_write( dsnm, n_ions, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = grpnm + "/bitdepth";
		//apt_uint bit_depth = sizeof(unsigned char) * 8;
		anno = ioAttributes();
		if ( h5w.nexus_write( dsnm, bit_depth, anno ) != MYHDF5_SUCCESS ) { return false; }

		dsnm = grpnm + "/mask";
		u08_wall = vector<unsigned char>( ions.ionpp3.size(), 0x00 );
		for ( size_t thr = 0; thr < regions.size(); thr++ ) {
			if ( regions.at(thr) != NULL ) {
				for( auto it = regions[thr]->io_cellifo.begin(); it != regions[thr]->io_cellifo.end(); it++ ) {
					if ( it->ghost == false ) {
						size_t ion_idx = it->evapid;
						switch(wall_it->first)
						{
							case -1:
							{
								//hitting an edge contact is seldom, especially a specific one
								if ( it->wallcontact.xmi_touch == false) { /*nop*/ }
								else { u08_wall[ion_idx] = 0x01; }
								break;
							}
							case -2:
							{
								if ( it->wallcontact.xmx_touch == false) { /*nop*/ }
								else { u08_wall[ion_idx] = 0x01; }
								break;
							}
							case -3:
							{
								if ( it->wallcontact.ymi_touch == false) { /*nop*/ }
								else { u08_wall[ion_idx] = 0x01; }
								break;
							}
							case -4:
							{
								if ( it->wallcontact.ymx_touch == false) { /*nop*/ }
								else { u08_wall[ion_idx] = 0x01; }
								break;
							}
							case -5:
							{
								if ( it->wallcontact.zmi_touch == false) { /*nop*/ }
								else { u08_wall[ion_idx] = 0x01; }
								break;
							}
							case -6:
							{
								if ( it->wallcontact.zmx_touch == false) { /*nop*/ }
								else { u08_wall[ion_idx] = 0x01; }
								break;
							}
							default:
							{
								break;
							}
						}
					}
				} //for all cells of thread
			} //for all threads
		}
		pack_bits.uint8_to_bitpacked_uint8( u08_wall, u08_bitfield );
		anno = ioAttributes();
		if ( h5w.nexus_write(
				dsnm,
				io_info({u08_bitfield.size()}, {u08_bitfield.size()},
						MYHDF5_COMPRESSION_GZIP, 0x01),
				u08_bitfield,
				anno ) != MYHDF5_SUCCESS ) { return false; }
		u08_bitfield = vector<unsigned char>();
	}


	grpnm = "/entry" + to_string(entry_id) + "/tessellation/voronoi_cells";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcg_polyhedron_set") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/dimensionality";
	apt_uint dim = 3;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, dim, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/cardinality";
	apt_uint card = (apt_uint) ions.ionifo.size();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, card, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/identifier_offset";
	int id_offset = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, id_offset, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/volume";
	vector<double> real = vector<double>( ions.ionifo.size(), MYZERO );
	for ( size_t thr = 0; thr < regions.size(); thr++ ) {
		if ( regions.at(thr) != NULL ) {
			for( auto it = regions[thr]->io_cellifo.begin(); it != regions[thr]->io_cellifo.end(); it++ ) {
				if ( it->ghost == false ) {
					size_t ion_idx = it->evapid;
					real[ion_idx] = it->volume;
				}
			}
		}
	}
	anno = ioAttributes();
	anno.add( "unit", string("nm^3") );
	if ( h5w.nexus_write(
			dsnm,
			io_info({real.size()}, {real.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			real,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	real = vector<double>();


	dsnm = grpnm + "/process_identifier";
	vector<unsigned short> u16 = vector<unsigned short>( ions.ionifo.size(), MASTER );
	//##MK::currently storing threadID as unsigned short maximum value to indicate that we do not know by which thread the cell/ion was processed
	/*
	for ( size_t thr = 0; thr < regions.size(); thr++ ) {
		if ( regions.at(thr) != NULL ) {
			for( auto it = regions[thr]->io_cellifo.begin(); it != regions[thr]->io_cellifo.end(); it++ ) {
				if ( it->ghost == false ) {
					size_t ion_idx = it->evapid;
					u16[ion_idx] = ( it->threadid < static_cast<unsigned short>(U16MX)) ? it->processid : static_cast<unsigned short>(U16MX);
				}
			}
		}
	}
	*/
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({u16.size()}, {u16.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u16,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	u16 = vector<unsigned short>();

	dsnm = grpnm + "/thread_identifier";
	u16 = vector<unsigned short>( ions.ionifo.size(), U16MX );
	//##MK::currently storing threadID as unsigned short maximum value to indicate that we do not know by which thread the cell/ion was processed
	for ( size_t thr = 0; thr < regions.size(); thr++ ) {
		if ( regions.at(thr) != NULL ) {
			for( auto it = regions[thr]->io_cellifo.begin(); it != regions[thr]->io_cellifo.end(); it++ ) {
				if ( it->ghost == false ) {
					size_t ion_idx = it->evapid;
					u16[ion_idx] = ( it->threadid < static_cast<unsigned short>(U16MX)) ? it->threadid : static_cast<unsigned short>(U16MX);
				}
			}
		}
	}
	anno = ioAttributes();
	if ( h5w.nexus_write(
			dsnm,
			io_info({u16.size()}, {u16.size()},
					MYHDF5_COMPRESSION_GZIP, 0x01),
			u16,
			anno ) != MYHDF5_SUCCESS ) { return false; }
	u16 = vector<unsigned short>();

	return MYHDF5_SUCCESS;
}


/*
bool tessellatorHdl::write_cell_geometry_h5( tess_task_info & info )
{
	//currently only implemented for sequential execution
	if ( info.hasGeometry == true ) {
		if ( regions.size() > 1 ) {
			cerr << "Debugging functionality for visualizing the cells currently implemented only in sequential version!" << "\n";
			for ( size_t thr = 0; thr < regions.size(); thr++ ) {
				if ( regions.at(thr) != NULL ) {
					regions[thr]->io_cellgeom = vector<cellgeom>();
					regions[thr]->io_cell_vertices = vector<p3d>();
					regions[thr]->io_cell_topology = vector<apt_uint>();
					regions[thr]->io_cell_evapid = vector<apt_uint>();
				}
			}
		}
		else {
			if ( regions.size() == 1 ) { //sequential export
				string fnm = ConfigTessellator::OutputfilePrefix + ".h5";
				string grpnm = "";
				string dsnm = "";
				string fwslash = "/";
				string str = "";
				int status = MYHDF5_SUCCESS;

				h5seqHdl h5w = h5seqHdl();
				status = h5w.use_file( fnm );

				string prefix = MYTESS + to_string(ConfigTessellator::DatasetID);
				grpnm = prefix + MYTESS_DATA_VORO_TSKS + fwslash + to_string(info.taskid);
				if ( info.hasVolume == false ) { //when group was not created before
					if ( h5w.add_group( grpnm, true, true ) != MYHDF5_SUCCESS ) {
						cerr << "Creating group " << grpnm << " failed !" << "\n"; return MYHDF5_FAILED;
					}
				}

				if ( regions.at(MASTER) != NULL ) {

					//cell geometry info
					dsnm = grpnm + fwslash + MYTESS_DATA_VORO_TSKS_CGEO_INFO;
					vector<size_t> u64;
					u64.reserve( regions[MASTER]->io_cellgeom.size()*5 );
					for( auto it = regions[MASTER]->io_cellgeom.begin(); it != regions[MASTER]->io_cellgeom.end(); it++ ) {
						u64.push_back((size_t) it->evapid);
						u64.push_back(it->offset_vertices);
						u64.push_back(it->offset_facets);
						u64.push_back((size_t) it->n_vertices);
						u64.push_back((size_t) it->n_facets);
					}
					if ( h5w.add_contiguous_array2d( dsnm, H5_U64, u64.size()/5, 5, u64 ) != MYHDF5_SUCCESS ) {
						cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
					}
					u64 = vector<size_t>();
					regions[MASTER]->io_cellgeom = vector<cellgeom>();

					//cell topology
					dsnm = grpnm + fwslash + MYTESS_DATA_VORO_TSKS_CGEO_TOPO;
					//use regions[MASTER]->io_cell_topology vector in-place
					if ( h5w.add_contiguous_array2d( dsnm, H5_U32, regions[MASTER]->io_cell_topology.size()/1, 1, regions[MASTER]->io_cell_topology ) != MYHDF5_SUCCESS ) {
						cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
					}
					regions[MASTER]->io_cell_topology = vector<apt_uint>();

					//cell vertices
					dsnm = grpnm + fwslash + MYTESS_DATA_VORO_TSKS_CGEO_VXYZ;
					vector<float> f32;
					f32.reserve( regions[MASTER]->io_cell_vertices.size()*3 );
					for( auto it = regions[MASTER]->io_cell_vertices.begin(); it != regions[MASTER]->io_cell_vertices.end(); it++ ) {
						f32.push_back(it->x);
						f32.push_back(it->y);
						f32.push_back(it->z);
					}
					if ( h5w.add_contiguous_array2d( dsnm, H5_F32, f32.size()/3, 3, f32 ) != MYHDF5_SUCCESS ) {
						cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
					}
					regions[MASTER]->io_cell_vertices = vector<p3d>();

					//cell evaporation IDs
					dsnm = grpnm + fwslash + MYTESS_DATA_VORO_TSKS_CGEO_EVAPID;
					//use regions[MASTER]->io_cell_evapid vector in-place
					if ( h5w.add_contiguous_array2d( dsnm, H5_U32, regions[MASTER]->io_cell_evapid.size()/1, 1, regions[MASTER]->io_cell_evapid ) != MYHDF5_SUCCESS ) {
						cerr << "Writing " << dsnm << " failed !" << "\n"; return MYHDF5_FAILED;
					}
					regions[MASTER]->io_cell_evapid = vector<apt_uint>();
				}
			}
		}
	}

	return MYHDF5_SUCCESS;
}
*/
