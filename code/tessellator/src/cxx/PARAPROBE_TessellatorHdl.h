/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_TESSELLATOR_HDL_H__
#define __PARAPROBE_TESSELLATOR_HDL_H__


#include "PARAPROBE_TessellatorXDMF.h"


class tessellatorHdl : public mpiHdl
{
	//process-level class which implements the worker instance

public:
	tessellatorHdl();
	~tessellatorHdl();

	bool read_relevant_input();

	bool crop_reconstruction_to_analysis_window();

	void execute_local_workpackage( tess_task_info & info );

	bool write_cell_info_h5( tess_task_info & info );
	/*
	bool write_cell_geometry_h5( tess_task_info & info );
	*/
	
	vector<unsigned char> window;					//the portion of the dataset that we want to analyze

	vector<tess*> regions;							//one region per OpenMP thread

	profiler tess_tictoc;
};

#endif
