/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_TessellatorStructs.h"


ostream& operator << (ostream& in, wallinfo const & val)
{
	if ( val.xmi_touch == true )
		in << "xmi_touch " << "yes" << "\n";
	else
		in << "xmi_touch " << "no" << "\n";
	if ( val.xmx_touch == true )
		in << "xmx_touch " << "yes" << "\n";
	else
		in << "xmx_touch " << "no" << "\n";
	if ( val.ymi_touch == true )
		in << "ymi_touch " << "yes" << "\n";
	else
		in << "ymi_touch " << "no" << "\n";
	if ( val.ymx_touch == true )
		in << "ymx_touch " << "yes" << "\n";
	else
		in << "ymx_touch " << "no" << "\n";
	if ( val.zmi_touch == true )
		in << "zmi_touch " << "yes" << "\n";
	else
		in << "zmi_touch " << "no" << "\n";
	if ( val.zmx_touch == true )
		in << "zmx_touch " << "yes" << "\n";
	else
		in << "zmx_touch " << "no" << "\n";
	if ( val.global_touch == true )
		in << "global_touch " << "yes" << "\n";
	else
		in << "global_touch " << "no" << "\n";
	if ( val.local_touch == true )
		in << "local_touch " << "yes" << "\n";
	else
		in << "local_touch " << "no" << "\n";
	return in;
}


ostream& operator << (ostream& in, cellinfo const & val)
{
	in << "volume " << val.volume << "\n";
	in << "evapid " << val.evapid << "\n";
	in << "edgedist " << val.edgedist << "\n";
	in << "rankid " << val.rankid << "\n";
	in << "threadid " << val.threadid << "\n";
	if ( val.ghost == true )
		in << "ghost " << "yes" << "\n";
	else
		in << "ghost " << "no" << "\n";
	if ( val.success == true )
		in << "success " << "yes" << "\n";
	else
		in << "success " << "no" << "\n";
	in << val.wallcontact << "\n";
	return in;
}


ostream& operator << (ostream& in, cellgeom const & val)
{
	in << "offset vertices " << val.offset_vertices << "\n";
	in << "offset facets " << val.offset_facets << "\n";
	in << "evapid " << val.evapid << "\n";
	in << "number of vertices " << val.n_vertices << "\n";
	in << "number of facets " << val.n_facets << "\n";
	return in;
}
