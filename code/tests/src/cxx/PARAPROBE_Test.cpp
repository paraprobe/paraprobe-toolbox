/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "../../../paraprobe-utils/src/cxx/PARAPROBE_ToolsBaseHdl.h"


/*
conventions:
- [x] decide on one encoding to be used consistently throughout the tools, e.g. variable, nullterm, utf8

c/cxx:
- [x] add write vector<string> to specifically formatted scalar or list of string dset or attr
- [x] refactor existent nexus_write to use new write functionalities cxx
- [x] refactor existent nexus_read to use new read functionalities cxx
- [x] error management and check for resource leakage
- [x] simplify existent hdf5core functions via removing string handling cxx
- [ ] read functions should report when value is not of the right type
- [x] tests
- [ ] make a package

python:
- [ ] refactor existent nexus_write to use new functionalities py
- [ ] refactor existent nexus_read to use new functionalities py
- [ ] add to package
- [ ] tests

matlab
- [ ] add read
- [ ] add write
- [ ] tests
- [ ] add to package
*/

void debug_vlen_string( const string file_name )
{
#define DATASET         "DS1"
#define ATTRIBUTE       "A1"
#define DIM0            5

    vector<string> val;
	val.push_back("Parting");
	val.push_back("");
	val.push_back("is such");
	val.push_back("sweet");
	val.push_back("sorrow");
	char** cstr = new char*[val.size()];
    for (unsigned long i=0; i<val.size(); i++) {
    	cstr[i] = new char[val[i].size()+1];
    	strcpy(cstr[i], val[i].c_str());
    }

	hid_t       file, filetype, memtype, space, dset, attr, amemtype_a, amemtype_b, aspace;
    herr_t      status;
    hsize_t     dims[1] = {DIM0};
    char        *wdata[DIM0] = {"Parting", "", "is such", "sweet", "sorrow"}, **rdata;
    int         ndims, i;

    file = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    memtype = H5Tcopy(H5T_C_S1);
    status = H5Tset_size (memtype, H5T_VARIABLE);
    status = H5Tset_strpad(memtype, H5T_STR_NULLTERM);
    space = H5Screate_simple(1, dims, NULL);
    dset = H5Dcreate(file, DATASET, memtype, space, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    status = H5Dwrite(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, cstr);
    for (unsigned long i=0; i<val.size(); i++)
    	delete[] cstr[i];
    delete[] cstr;

    val = vector<string>();
    val.push_back("Parting");
    amemtype_a = H5Tcopy(H5T_C_S1);
    size_t mxx = val[0].length()+1;
    status = H5Tset_size(amemtype_a, mxx); //H5T_VARIABLE);
    status = H5Tset_strpad(amemtype_a, H5T_STR_NULLTERM);
    status = H5Tset_cset(amemtype_a, H5T_CSET_UTF8);
    amemtype_b = H5Tcopy(H5T_C_S1);
    status = H5Tset_size(amemtype_b, H5T_VARIABLE);
    status = H5Tset_strpad(amemtype_b, H5T_STR_NULLTERM);
    status = H5Tset_cset(amemtype_b, H5T_CSET_UTF8);
    dims[0] = 1;
    aspace = H5Screate(H5S_SCALAR);
    //aspace = H5Screate_simple(1, dims, NULL); //list_single
    attr = H5Acreate2(dset, ATTRIBUTE, amemtype_a, aspace, H5P_DEFAULT, H5P_DEFAULT);
    char* adata = new char[mxx];
    if ( adata != NULL ) {
    	strcpy(adata, val.at(0).c_str());
    	status = H5Awrite(attr, amemtype_a, val.data()); //adata); //val.data()); //adata);
    	//status = H5Awrite(attr, amemtype_b, val.data()); //variable, single, nullterm, utf8
    }
    delete[] adata;
    status = H5Aclose(attr);
    status = H5Sclose(aspace);
    status = H5Tclose(amemtype_a);
    status = H5Tclose(amemtype_b);

    status = H5Dclose (dset);
    status = H5Sclose (space);
    status = H5Tclose (memtype);
    status = H5Fclose (file);
}


void test_hdf_five_basic_writing_cxx( const string fnm )
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( fnm );
	if( h5w.nexus_create() != MYHDF5_SUCCESS ) { return; }

	io_info ifo = io_info();
	ioAttributes anno = ioAttributes();
	vector<unsigned char> dimensionality = {MYHDF5_SCALAR, MYHDF5_LIST_SINGLE, MYHDF5_LIST_LONG};
	vector<string> types = {"u8", "i8", "u16", "i16", "u32", "i32", "u64", "i64", "f32", "f64" };
	for ( size_t typid = 0; typid < types.size(); typid++ ) {
		for ( auto dim = dimensionality.begin(); dim != dimensionality.end(); dim++ ) {
			string suffix = types[typid] + "_";
			switch(*dim)
			{
				case MYHDF5_SCALAR: { suffix += "scalar"; break; }
				case MYHDF5_LIST_SINGLE: { suffix += "list_single"; break; }
				case MYHDF5_LIST_LONG: { suffix += "list_long"; break; }
				default: { break; }
			}
			string dsnm = "/" + suffix;
			cout << dsnm << "\n";
			switch(*dim)
			{
				case MYHDF5_SCALAR:
				{
					switch(typid)
					{
						case 0: { unsigned char value = 0x00;  anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 1: { char value = 0x00; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 2: { unsigned short value = 0; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 3: { short value = 0; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 4: { unsigned int value = 0; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 5: { int value = 0; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 6: { unsigned long value = 0; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 7: { long value = 0; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 8: { float value = 0.; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 9: { double value = 0.; anno = ioAttributes(); anno.add( suffix, value ); if ( h5w.nexus_write( dsnm, value, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						default: { break; }
					}
					break;
				}
				case MYHDF5_LIST_SINGLE:
				{
					switch(typid)
					{
						case 0: { vector<unsigned char> values = {0x01}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 1: { vector<char> values = {0x01}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 2: { vector<unsigned short> values = {1}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 3: { vector<short> values = {1}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 4: { vector<unsigned int> values = {1}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 5: { vector<int> values = {1}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 6: { vector<unsigned long> values = {1}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 7: { vector<long> values = {1}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 8: { vector<float> values = {1.}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 9: { vector<double> values = {1.}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						default: { break; }
					}
					break;
				}
				case MYHDF5_LIST_LONG:
				{
					switch(typid)
					{
						case 0: { vector<unsigned char> values = {0x01, 0x02, 0x03}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 1: { vector<char> values = {0x01, 0x02, 0x03}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 2: { vector<unsigned short> values = {1, 2, 3}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 3: { vector<short> values = {1, 2, 3}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 4: { vector<unsigned int> values = {1, 2, 3}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 5: { vector<int> values = {1, 2, 3}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 6: { vector<unsigned long> values = {1, 2, 3}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 7: { vector<long> values = {1, 2, 3}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 8: { vector<float> values = {1., 2., 3.}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						case 9: { vector<double> values = {1., 2., 3.}; ifo = io_info({values.size()}); anno = ioAttributes(); anno.add( suffix, values ); if ( h5w.nexus_write( dsnm, ifo, values, anno ) != MYHDF5_SUCCESS ) { return; } break; }
						default: { break; }
					}
					break;
				}
				default: {
					break;
				}
			}
		}
	}
}


void test_hdf_five_basic_reading_cxx( const string fnm )
{
	HdfFiveSeqHdl h5r = HdfFiveSeqHdl( fnm );

	io_info ifo = io_info();
	ioAttributes anno = ioAttributes();
	vector<unsigned char> dimensionality = {MYHDF5_SCALAR, MYHDF5_LIST_SINGLE, MYHDF5_LIST_LONG};
	vector<string> types = {"u8", "i8", "u16", "i16", "u32", "i32", "u64", "i64", "f32", "f64" };
	for ( size_t typid = 0; typid < types.size(); typid++ ) {
		for ( auto dim = dimensionality.begin(); dim != dimensionality.end(); dim++ ) {
			string suffix = types[typid] + "_";
			switch(*dim)
			{
				case MYHDF5_SCALAR: { suffix += "scalar"; break; }
				case MYHDF5_LIST_SINGLE: { suffix += "list_single"; break; }
				case MYHDF5_LIST_LONG: { suffix += "list_long"; break; }
				default: { break; }
			}
			string dsnm = "/" + suffix;
			cout << dsnm << "\n";
			switch(*dim)
			{
				case MYHDF5_SCALAR:
				{
					switch(typid)
					{
						case 0: { unsigned char value = 0x00; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << (int) value << "__" << "\n"; break; }
						case 1: { char value = 0x00; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << (int) value << "__" << "\n"; break; }
						case 2: { unsigned short value = 0; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						case 3: { short value = 0; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						case 4: { unsigned int value = 0; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						case 5: { int value = 0; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						case 6: { unsigned long value = 0; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						case 7: { long value = 0; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						case 8: { float value = 0.; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						case 9: { double value = 0.; if ( h5r.nexus_read( dsnm, value ) != MYHDF5_SUCCESS ) { return; } cout << "__" << value << "__" << "\n"; break; }
						default: { break; }
					}
					break;
				}
				case MYHDF5_LIST_SINGLE:
				{
					switch(typid)
					{
						case 0: { vector<unsigned char> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << (int) *it << "__" << "\n"; } break; }
						case 1: { vector<char> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << (int) *it << "__" << "\n"; } break; }
						case 2: { vector<unsigned short> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 3: { vector<short> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 4: { vector<unsigned int> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 5: { vector<int> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 6: { vector<unsigned long> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 7: { vector<long> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 8: { vector<float> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 9: { vector<double> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						default: { break; }
					}
					break;
				}
				case MYHDF5_LIST_LONG:
				{
					switch(typid)
					{
						case 0: { vector<unsigned char> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << (int) *it << "__" << "\n"; } break; }
						case 1: { vector<char> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << (int) *it << "__" << "\n"; } break; }
						case 2: { vector<unsigned short> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 3: { vector<short> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 4: { vector<unsigned int> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 5: { vector<int> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 6: { vector<unsigned long> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 7: { vector<long> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 8: { vector<float> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						case 9: { vector<double> values; if ( h5r.nexus_read( dsnm, values ) != MYHDF5_SUCCESS ) { return; }
							for( auto it = values.begin(); it != values.end(); it++ ) { cout << "__" << *it << "__" << "\n"; } break; }
						default: { break; }
					}
					break;
				}
				default: {
					break;
				}
			}
		}
	}
}


void test_hdf_five_string_writing_cxx( const string fnm )
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( fnm );
	if( h5w.nexus_create() != MYHDF5_SUCCESS ) { return; }

	io_info ifo = io_info();
	ioAttributes anno = ioAttributes();
	vector<unsigned char> dimensionality = {MYHDF5_SCALAR, MYHDF5_LIST_SINGLE, MYHDF5_LIST_LONG}; //"scalar", "list_single", "list_long" };
	vector<unsigned char> memory = {MYHDF5_IS_VARIABLE, MYHDF5_IS_FIXED}; //"variable", "fixed" };
	vector<unsigned char> terminator = {MYHDF5_NULLTERM, MYHDF5_NULLPAD, MYHDF5_SPACEPAD}; //"nullterm", "nullpad", "spacepad" };
	vector<unsigned char> encoding = {MYHDF5_ASCII, MYHDF5_UTF8}; //"ascii", "utf8" };
	for ( auto dim = dimensionality.begin(); dim != dimensionality.end(); dim++ ) {
		for ( auto mem = memory.begin(); mem != memory.end(); mem++ ) {
			for ( auto trm = terminator.begin(); trm != terminator.end(); trm++ ) {
				for( auto cset = encoding.begin(); cset != encoding.end(); cset++ ) {
					string suffix = "string_";
					switch(*dim)
					{
						case MYHDF5_SCALAR: { suffix += "scalar_"; break; }
						case MYHDF5_LIST_SINGLE: { suffix += "list_single_"; break; }
						case MYHDF5_LIST_LONG: { suffix += "list_long_"; break; }
						default: { break; }
					}
					switch(*mem)
					{
						case MYHDF5_IS_VARIABLE: { suffix += "variable_"; break; }
						case MYHDF5_IS_FIXED: { suffix += "fixed_"; break; }
						default: { break; }
					}
					switch(*trm)
					{
						case MYHDF5_NULLTERM: { suffix += "nullterm_"; break; }
						case MYHDF5_NULLPAD: { suffix += "nullpad_"; break; }
						case MYHDF5_SPACEPAD: { suffix += "spacepad_"; break; }
						default: { break; }
					}
					switch(*cset)
					{
						case MYHDF5_ASCII: { suffix += "ascii"; break; }
						case MYHDF5_UTF8: { suffix += "utf8"; break; }
						default: { break; }
					}
					string dsnm = "/" + suffix;
					cout << dsnm << "\n";
					switch(*dim)
					{
						case MYHDF5_SCALAR:
						{
							vector<string> values;
							values.push_back("Parting is such sweet sorrow °");
							anno = ioAttributes();
							//if ( *mem == MYHDF5_IS_VARIABLE && *trm == MYHDF5_NULLTERM && *cset == MYHDF5_UTF8 ) {
							cout << "---> adding attributes, scalar" << "\n";
							string scalar = "Parting is such sweet sorrow °";
							anno.add( suffix, scalar );
							//}
							if ( h5w.nexus_write( dsnm, values, *dim, *mem, *trm, *cset, anno ) != MYHDF5_SUCCESS ) { return; }
							break;
						}
						case MYHDF5_LIST_SINGLE:
						{
							vector<string> values;
							values.push_back("Parting is such sweet sorrow °");
							anno = ioAttributes();
							//if ( *mem == MYHDF5_IS_VARIABLE && *trm == MYHDF5_NULLTERM && *cset == MYHDF5_UTF8 ) {
							cout << "---> adding attributes, list_single" << "\n";
							vector<string> list_single;
							list_single.push_back( "Parting is such sweet sorrow °");
							anno.add( suffix, list_single );
							//}
							if ( h5w.nexus_write( dsnm, values, *dim, *mem, *trm, *cset, anno ) != MYHDF5_SUCCESS ) { return; }
							break;
						}
						case MYHDF5_LIST_LONG:
						{
							vector<string> values;
							values.push_back("Parting");
							values.push_back("");
							values.push_back("is such");
							values.push_back("sweet");
							values.push_back("sorrow");
							values.push_back("°");
							anno = ioAttributes();
							//if ( *mem == MYHDF5_IS_VARIABLE && *trm == MYHDF5_NULLTERM && *cset == MYHDF5_UTF8 ) {
							cout << "---> adding attributes, list_long" << "\n";
							vector<string> list_long;
							list_long.push_back("Parting");
							list_long.push_back("");
							list_long.push_back("is such");
							list_long.push_back("sweet");
							list_long.push_back("sorrow");
							list_long.push_back("°");
							anno.add( suffix, list_long );
							//}
							if ( h5w.nexus_write( dsnm, values, *dim, *mem, *trm, *cset, anno ) != MYHDF5_SUCCESS ) { return; }
							break;
						}
						default: {
							break;
						}
					}
				}
			}
		}
	}
}


void test_hdf_five_string_reading_cxx( const string fnm )
{
	HdfFiveSeqHdl h5r = HdfFiveSeqHdl( fnm );

	int status = MYHDF5_SUCCESS;
	io_info ifo = io_info();
	ioAttributes anno = ioAttributes();
	vector<unsigned char> dimensionality = {MYHDF5_SCALAR, MYHDF5_LIST_SINGLE, MYHDF5_LIST_LONG}; //"scalar", "list_single", "list_long" };
	vector<unsigned char> memory = {MYHDF5_IS_VARIABLE, MYHDF5_IS_FIXED}; //"variable", "fixed" };
	vector<unsigned char> terminator = {MYHDF5_NULLTERM, MYHDF5_NULLPAD, MYHDF5_SPACEPAD}; //"nullterm", "nullpad", "spacepad" };
	vector<unsigned char> encoding = {MYHDF5_ASCII, MYHDF5_UTF8}; //"ascii", "utf8" };
	for ( auto dim = dimensionality.begin(); dim != dimensionality.end(); dim++ ) {
		for ( auto mem = memory.begin(); mem != memory.end(); mem++ ) {
			for ( auto trm = terminator.begin(); trm != terminator.end(); trm++ ) {
				for( auto cset = encoding.begin(); cset != encoding.end(); cset++ ) {
					string suffix = "string_";
					switch(*dim)
					{
						case MYHDF5_SCALAR: { suffix += "scalar_"; break; }
						case MYHDF5_LIST_SINGLE: { suffix += "list_single_"; break; }
						case MYHDF5_LIST_LONG: { suffix += "list_long_"; break; }
						default: { break; }
					}
					switch(*mem)
					{
						case MYHDF5_IS_VARIABLE: { suffix += "variable_"; break; }
						case MYHDF5_IS_FIXED: { suffix += "fixed_"; break; }
						default: { break; }
					}
					switch(*trm)
					{
						case MYHDF5_NULLTERM: { suffix += "nullterm_"; break; }
						case MYHDF5_NULLPAD: { suffix += "nullpad_"; break; }
						case MYHDF5_SPACEPAD: { suffix += "spacepad_"; break; }
						default: { break; }
					}
					switch(*cset)
					{
						case MYHDF5_ASCII: { suffix += "ascii"; break; }
						case MYHDF5_UTF8: { suffix += "utf8"; break; }
						default: { break; }
					}
					string dsnm = "/" + suffix;
					cout << dsnm << "\n";
					if ( *dim == MYHDF5_SCALAR ) {
						string retval = "";
						if ( h5r.nexus_read(dsnm, retval) != MYHDF5_SUCCESS ) { return; }
						cout << "__" << retval << "__" << "\n";
					}
					else if ( *dim == MYHDF5_LIST_SINGLE ) {
						vector<string> retval;
						if ( h5r.nexus_read(dsnm, retval) != MYHDF5_SUCCESS ) { return; }
						for ( auto it = retval.begin(); it != retval.end(); it++ ) {
							cout << "__" << *it << "__" << "\n";
						}
					}
					else { //*dim == MYHDF5_LIST_LONG:
						vector<string> retvals;
						if ( h5r.nexus_read(dsnm, retvals) != MYHDF5_SUCCESS ) { return; }
						for ( auto it = retvals.begin(); it != retvals.end(); it++ ) {
							cout << "__" << *it << "__" << "\n";
						}
					}
				}
			}
		}
	}
}


void string_formatting_run_cxx_reading( const string fnm, const string dsnm, const string attrnm )
{
	//read with automated conversion of the differently encodeable scalar or list of string dset or attr node into a vector of string
	hid_t file = H5I_INVALID_HID;
    hid_t memtype = H5I_INVALID_HID;
    hid_t dtype = H5I_INVALID_HID;
    hid_t dspace = H5I_INVALID_HID;
    hid_t dset = H5I_INVALID_HID;
    herr_t status = -1;
    hsize_t dims[1] = {0};
    hsize_t maxdims[1] = {0};
    htri_t tribool = -1;
    H5S_class_t detype = H5S_NO_CLASS;
	H5T_str_t strpad = H5T_STR_NULLTERM;
	H5T_cset_t cset = H5T_CSET_ASCII;
	int ndims = 0;
	int dim = 0;

	bool verbose = false;
    bool report_consts = false;
    bool report_config = false;
    bool report_dset = true;
    bool report_attr = true;

    if ( report_consts == true ) {
		cout << "inform about HDF5 library constants >>>>" << "\n";
		cout << "detype no class " << detype << "\n";
		detype = H5S_SCALAR;
		cout << "detype scalar " << detype << "\n";
		detype = H5S_SIMPLE;
		cout << "detype simple " << detype << "\n";
		detype = H5S_NULL;
		cout << "detype null " << detype << "\n";

		strpad = H5T_STR_NULLTERM;
		cout << "strpad nullterm " << strpad << "\n";
		strpad = H5T_STR_NULLPAD;
		cout << "strpad nullpad " << strpad << "\n";
		strpad = H5T_STR_SPACEPAD;
		cout << "strpad spacepad " << strpad << "\n";

		cset = H5T_CSET_ASCII;
		cout << "cset ascii " << cset << "\n";
		cset = H5T_CSET_UTF8;
		cout << "cset utf8 " << cset << "\n";
    }

	//fnm_cmd_opt = argv[1];
	//string loc_cmd_opt = argv[2]; //the string_formatting.h5 example uses the same name for dataset and attribute
	//fnm = fnm_cmd_opt;
	//string dsnm = "/" + loc_cmd_opt;
	//string attrnm = loc_cmd_opt;

	if ( report_config == true ) {
		cout << "report file, dataset, and attribute to operate on >>>>" << "\n";
		cout << "fnm " << fnm << "\n";
		cout << "dsnm " << dsnm << "\n";
		cout << "attrnm " << attrnm << "\n";

		cout << "SHA256 checksum >>>>" << "\n";
		//string old_sha256 = SHA256( fnm.c_str() );
		//cout << "__" << old_sha256 << "__" << "\n";
		string new_sha256 = SHA256( fnm );  //EVP( fnm );
		cout << "__" << new_sha256 << "__" << "\n";
	}

	if ( report_dset == true ) {
		//if ( verbose == true )
		cout << "handle dataset >>>>" << "\n";
		file = H5Fopen(fnm.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
		dset = H5Dopen(file, dsnm.c_str(), H5P_DEFAULT);
		if ( dset >= 0 ) {
			dspace = H5Dget_space(dset);
			detype = H5Sget_simple_extent_type(dspace);
			if ( verbose == true )
				cout << "detype " << detype << "\n";
			ndims = H5Sget_simple_extent_dims(dspace, dims, maxdims);
			if ( verbose == true )
				cout << "ndims " << ndims << "\n";
			dim = 0;
			if ( verbose == true ) {
				cout << "dims[" << dim << "] " << dims[dim] << "\n";
				cout << "maxdims[" << dim << "] " << maxdims[dim] << "\n";
			}
			dtype = H5Dget_type(dset);
			tribool = H5Tis_variable_str(dtype);
			if ( verbose == true ) {
				if ( tribool > 0 ) { cout << "variable" << "\n"; }
				else if ( tribool == 0 ) { cout << "fixed" << "\n"; }
				else { cout << "is_variable_str error!" << "\n"; }
			}
			strpad = H5Tget_strpad(dtype);
			if ( verbose == true ) {
				switch(strpad)
				{
					case 0: { cout << "strpad nullterm" << "\n"; break; }
					case 1: { cout << "strpad nullpad" << "\n"; break; }
					case 2: { cout << "strpad spacepad" << "\n"; break; }
					default: { cout << "strpad error!" << "\n"; break; }
				}
			}
			cset = H5Tget_cset(dtype);
			if ( verbose == true ) {
				if ( cset == 0 ) { cout << "cset ascii" << "\n"; }
				else if ( cset == 1 ) { cout << "cset utf8" << "\n"; }
				else { cout << "cset error!" << "\n"; }
			}
			size_t max_size = H5Tget_size(dtype);
			if ( verbose == true )
				cout << "max_size " << max_size << "\n";

			//read
			if ( tribool > 0 ) { //MYHDF5_IS_VARIABLE
				//https://git.rwth-aachen.de/lukas.weber2/hdf5/-/blob/0d1b43c79c479b1f88eff1cc39baaf037efccfc8/test/tvlstr.c
				if ( ndims == 0 ) { //SCALAR
					memtype = H5Tcopy(H5T_C_S1);
					status = H5Tset_size(memtype, H5T_VARIABLE);
					if ( cset == 1 ) {
						status = H5Tset_cset(memtype, H5T_CSET_UTF8);
					}
					char** rbuf = (char **) malloc(1 * sizeof(char *));
					if ( rbuf != NULL ) {
						if ( H5Dread(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rbuf) >= 0 ) {
							string value = string(rbuf[0]);
							cout << "__" << value << "__" << "\n";
						}
						status = H5Treclaim(memtype, dspace, H5P_DEFAULT, rbuf);
						free(rbuf);
					}
					status = H5Tclose(memtype);
				}
				else { //LIST_SINGLE or LIST_LONG
					memtype = H5Tcopy(H5T_C_S1);
					status = H5Tset_size(memtype, H5T_VARIABLE);
					if ( cset == 1 ) {
						status = H5Tset_cset(memtype, H5T_CSET_UTF8);
					}
					char** rbuf = (char **) malloc(dims[0] * sizeof(char *));
					if (rbuf != NULL) {
						if ( H5Dread(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rbuf) >= 0 ) {
							for ( size_t i = 0; i < dims[0]; i++ ) {
								string value = string(rbuf[i]);
								cout << "__" << value << "__" << "\n";
							}
						}
						status = H5Treclaim(memtype, dspace, H5P_DEFAULT, rbuf);
						free(rbuf);
					}
					status = H5Tclose(memtype);
				}
			}
			else if ( tribool == 0 ) { //MYHDF5_IS_FIXED
				if ( ndims == 0 ) { //SCALAR
					memtype = H5Tcopy(H5T_C_S1);
					max_size++;
					status = H5Tset_size(memtype, max_size);
					if ( cset == 1 ) {
						status = H5Tset_cset(memtype, H5T_CSET_UTF8);
					}
					char* rbuf = (char *) malloc(max_size * sizeof(char));
					if (rbuf != NULL) {
						if ( H5Dread(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rbuf) >= 0 ) {
							string value = string(rbuf);
							cout << "__" << value << "__" << "\n";
						}
						free(rbuf);
					}
					status = H5Tclose(memtype);
				}
				else { //LIST_SINGLE or LIST_LONG
					memtype = H5Tcopy(H5T_C_S1);
					max_size++;
					status = H5Tset_size(memtype, max_size);
					if ( cset == 1 ) {
						status = H5Tset_cset(memtype, H5T_CSET_UTF8);
					}
					char** rbuf = (char **) malloc(dims[0] * sizeof(char *));
					if (rbuf != NULL) {
						rbuf[0] = (char *) malloc(dims[0] * max_size * sizeof(char));
						for( size_t i = 1; i < dims[0]; i++ )
							rbuf[i] = rbuf[0] + i * max_size;
						if ( rbuf[0] != NULL ) {
							if ( H5Dread(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rbuf[0]) >= 0 ) {
								for ( size_t i = 0; i < dims[0]; i++ ) {
									string value = string(rbuf[i]);
									cout << "__" << value << "__" << "\n";
								}
							}
							free(rbuf[0]);
						}
						free(rbuf);
					}
					status = H5Tclose(memtype);
				}
			}
			else {
			}
			status = H5Sclose(dspace);
		}
		status = H5Dclose(dset);
		status = H5Fclose(file);
	}

	if ( report_attr == true ) {
		//if ( verbose == true )
		cout << "handle attribute >>>>" << "\n";
		hid_t attrid;
		file = H5Fopen(fnm.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
		attrid = H5Aopen_by_name(file, dsnm.c_str(), attrnm.c_str(), H5P_DEFAULT, H5P_DEFAULT);
		if ( attrid >= 0 ) {
			hid_t attrtyp;
			attrtyp = H5Aget_type(attrid);
			if ( attrtyp != H5I_INVALID_HID ) {
				dspace = H5Aget_space(attrid);
				detype = H5Sget_simple_extent_type(dspace);
				if ( verbose == true )
					cout << "detype " << detype << "\n";
				ndims = H5Sget_simple_extent_dims(dspace, dims, maxdims);
				if ( verbose == true )
					cout << "ndims " << ndims << "\n";
				dim = 0;
				if ( verbose == true ) {
					cout << "dims[" << dim << "] " << dims[dim] << "\n";
					cout << "maxdims[" << dim << "] " << maxdims[dim] << "\n";
				}
				dtype = H5Aget_type(attrid);
				tribool = H5Tis_variable_str(dtype);
				if ( verbose == true ) {
					if ( tribool > 0 ) { cout << "variable" << "\n"; }
					else if ( tribool == 0 ) { cout << "fixed" << "\n"; } //this is an assumption
					else { cout << "is_variable_str error!" << "\n"; }
				}
				strpad = H5Tget_strpad(dtype);
				if ( verbose == true ) {
					switch(strpad)
					{
						case 0: { cout << "strpad nullterm" << "\n"; break; }
						case 1: { cout << "strpad nullpad" << "\n"; break; }
						case 2: { cout << "strpad spacepad" << "\n"; break; }
						default: { cout << "strpad error!" << "\n"; break; }
					}
				}
				cset = H5Tget_cset(dtype);
				if ( verbose == true ) {
					if ( cset == 0 ) { cout << "cset ascii" << "\n"; }
					else if ( cset == 1 ) { cout << "cset utf8" << "\n"; }
					else { cout << "cset error!" << "\n"; }
				}
				size_t max_size = H5Tget_size(dtype);
				if ( verbose == true )
					cout << "max_size " << max_size << "\n";

				//read
				if ( tribool > 0 ) { //MYHDF5_IS_VARIABLE
					//https://git.rwth-aachen.de/lukas.weber2/hdf5/-/blob/0d1b43c79c479b1f88eff1cc39baaf037efccfc8/test/tvlstr.c
					if ( ndims == 0 ) { //SCALAR
						memtype = H5Tcopy(H5T_C_S1);
						status = H5Tset_size(memtype, H5T_VARIABLE);
						if ( cset == 1 ) {
							status = H5Tset_cset(memtype, H5T_CSET_UTF8);
						}
						char** rbuf = (char **) malloc(1 * sizeof(char *));
						if ( rbuf != NULL ) {
							if ( H5Aread(attrid, memtype, rbuf) >= 0 ) {
								string value = string(rbuf[0]);
								cout << "__" << value << "__" << "\n";
							}
							status = H5Treclaim(memtype, dspace, H5P_DEFAULT, rbuf);
							free(rbuf);
						}
						status = H5Tclose(memtype);
					}
					else { //LIST_SINGLE or LIST_LONG
						memtype = H5Tcopy(H5T_C_S1);
						status = H5Tset_size(memtype, H5T_VARIABLE);
						if ( cset == 1 ) {
							status = H5Tset_cset(memtype, H5T_CSET_UTF8);
						}
						char** rbuf = (char **) malloc(dims[0] * sizeof(char *));
						if (rbuf != NULL) {
							if ( H5Aread(attrid, memtype, rbuf) >= 0 ) {
								for ( size_t i = 0; i < dims[0]; i++ ) {
									string value = string(rbuf[i]);
									cout << "__" << value << "__" << "\n";
								}
							}
							status = H5Treclaim(memtype, dspace, H5P_DEFAULT, rbuf);
							free(rbuf);
						}
						status = H5Tclose(memtype);
					}
				}
				else if ( tribool == 0 ) { //MYHDF5_IS_FIXED
					if ( ndims == 0 ) { //SCALAR
						memtype = H5Tcopy(H5T_C_S1);
						max_size++;
						status = H5Tset_size(memtype, max_size);
						if ( cset == 1 ) {
							status = H5Tset_cset(memtype, H5T_CSET_UTF8);
						}
						char* rbuf = (char *) malloc(max_size * sizeof(char));
						if (rbuf != NULL) {
							if ( H5Aread(attrid, memtype, rbuf) >= 0 ) {
								string value = string(rbuf);
								cout << "__" << value << "__" << "\n";
							}
							free(rbuf);
						}
						status = H5Tclose(memtype);
					}
					else { //LIST_SINGLE, LIST_LONG
						memtype = H5Tcopy(H5T_C_S1);
						max_size++;
						status = H5Tset_size(memtype, max_size);
						if ( cset == 1 ) {
							status = H5Tset_cset(memtype, H5T_CSET_UTF8);
						}
						char** rbuf = (char **) malloc(dims[0] * sizeof(char *));
						if (rbuf != NULL) {
							rbuf[0] = (char *) malloc(dims[0] * max_size * sizeof(char));
							for( size_t i = 1; i < dims[0]; i++ )
								rbuf[i] = rbuf[0] + i * max_size;
							if ( rbuf[0] != NULL ) {
								if ( H5Aread(attrid, memtype, rbuf[0]) >= 0 ) {
									for ( size_t i = 0; i < dims[0]; i++ ) {
										string value = string(rbuf[i]);
										cout << "__" << value << "__" << "\n";
									}
								}
								free(rbuf[0]);
							}
							free(rbuf);
						}
						status = H5Tclose(memtype);
					}
				}
				else {
				}
				status = H5Sclose(dspace);
			}
			status = H5Tclose(attrtyp);
		}
		status = H5Aclose(attrid);
		status = H5Fclose(file);
	}
	return;
}


int main(int argc, char** argv)
{
	string fnm_cmd_opt = argv[1];
	string fnm = fnm_cmd_opt;
	//debug_vlen_string(fnm);
	//return 0;

	test_hdf_five_basic_writing_cxx( fnm );
	return 0;

	test_hdf_five_basic_reading_cxx( fnm );
	return 0;

	//fnm = argv[1];
	//test_hdf_five_string_writing_cxx(fnm);
	//return 0;

	//test_hdf_five_string_reading_cxx(fnm);
	//return 0;

	fnm_cmd_opt = argv[1];
	string loc_cmd_opt = argv[2]; //the string_formatting.h5 example uses the same name for dataset and attribute
	fnm = fnm_cmd_opt;
	string dsnm = "/" + loc_cmd_opt;
	string attrnm = loc_cmd_opt;
	string_formatting_run_cxx_reading(fnm, dsnm, attrnm);
	return 0;
}



