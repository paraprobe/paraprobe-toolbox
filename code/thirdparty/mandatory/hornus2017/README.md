https://github.com/horasio/intersection-detection 2784e77
Sam Hornus code for testing intersections of three-dimensional primitives.
The only modification made is in convexes.h
where the tet struct is not using alignas(16).