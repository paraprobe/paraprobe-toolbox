v0.4 clean up old code stuff, modify utils to use only apt_* types

### paraprobe-utils source file inclusion dependency graph

The header files are included in the following chain:

 1. PARAPROBE_STLInterface.h
 2. PARAPROBE_Numerics.h
 3. PARAPROBE_PhysicalConstants.h
 4. PARAPROBE_UnitConversion.h
 5. PARAPROBE_CoordinateSystem.h
 6. PARAPROBE_ArgsAndGitSha.h./.cpp
 7. PARAPROBE_OpenSSLInterface.h/.cpp
 8. PARAPROBE_TimeAndDateHandling.h./.cpp
 9. PARAPROBE_CiteMe.h/.cpp
10. PARAPROBE_ConfigShared.h/.cpp
11. PARAPROBE_OSInterface.h
12. PARAPROBE_CPUParallelization.h
13. PARAPROBE_GPUParallelization.h
14. PARAPROBE_BoostInterface.h
15. PARAPROBE_FilePathHandling.h/.cpp
16. PARAPROBE_BitMangling.h/.cpp
17. PARAPROBE_HDF5Core.h/.cpp
18. PARAPROBE_FFTInterface.h
19. PARAPROBE_VoroXXInterface.h
    PARAPROBE_Verbose.h/.cpp --> removed!
20. PARAPROBE_Profiling.h/.cpp --> leave the profiler as it is, but it needs revision, given the new design, a feature for v0.4.1
21. PARAPROBE_Math.h/.cpp
22. PARAPROBE_PrimsContinuum.h/.cpp
23. PARAPROBE_PrimsDiscrete.h/.cpp
24. PARAPROBE_Geometry.h/.cpp
25. PARAPROBE_UserStructs.h/.cpp
    UserStructSorting.h/.cpp --> removed!
26. PARAPROBE_MPIStructs.h/.cpp --> remove commented out MPI structs and defines
27. PARAPROBE_PeriodicTable.h/.cpp
28. PARAPROBE_Crystallography.h/.cpp
29. PARAPROBE_OriMath.h/.cpp
30. PARAPROBE_CGALInterface.h/.cpp
31. PARAPROBE_Solutes.h/.cpp
32. PARAPROBE_ThermodynamicPhase.h/.cpp
33. PARAPROBE_Grains.h/.cpp
34. PARAPROBE_Precipitates.h/.cpp
35. PARAPROBE_Histogram2D.h/.cpp
36. PARAPROBE_TwoPointStats3D.h/.cpp
37. PARAPROBE_ROIs.h/.cpp
38. PARAPROBE_VoxelTree.h/.cpp --> renamed!, all calls to an instance of LinearVolQuerier need to specify <type> see http://users.cis.fiu.edu/~weiss/Deltoid/vcstl/templates

39. PARAPROBE_AABBTree.h/.cpp
40. PARAPROBE_KDTree.h/.cpp
41. PARAPROBE_IonCloudMemory.h/.cpp
    HDF5BaseHdl.h/.cpp --> removed!
42. PARAPROBE_XDMFBaseHdl.h/.cpp
43. PARAPROBE_TriangleSoupHdl.h/.cpp
44. PARAPROBE_ToolsBaseHdl.h/.cpp
    ConfigBaseHdl.h/.cpp --> removed!
