/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_AABBTREE_H__
#define __PARAPROBE_UTILS_AABBTREE_H__

#include "PARAPROBE_VoxelTree.h"

/*
  Copyright (c) 2009 Erin Catto http://www.box2d.org
  Copyright (c) 2016-2017 Lester Hedges <lester.hedges+aabbcc@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

  This code was adapted from parts of the Box2D Physics Engine,
  http://www.box2d.org
*/

/// Null node flag
#define NULL_NODE				(UMX)
#define TREE_ERROR				(UMX)

#ifdef EMPLOY_SINGLE_PRECISION
	#define ASSERTATION_ACCURACY	EPSILON
#else
	#define ASSERTATION_ACCURACY	EPSILON
#endif

//MK::modifications of Hester's code consider that the apt_real world APT tip is 3d and does not show periodicity
//so many checks internally can be avoided to increase performance
//an axis-aligned bounding box (AABB) stores information for the minimum orthorhombic bounding-box of a primitive e.g. a triangle


struct trpl
{
	apt_real a;
	apt_real b;
	apt_real c;

	trpl() : a(MYZERO), b(MYZERO), c(MYZERO) {}
	trpl(const apt_real _a, const apt_real _b, const apt_real _c ) : a(_a), b(_b), c(_c) {}
};


class hedgesAABB
{
public:
	hedgesAABB() : surfaceArea(MYZERO) {
		//trpl objects get values assigned via their default constructor
	};
	hedgesAABB(const trpl lowerBound_, const trpl upperBound_);
	~hedgesAABB(){};

	apt_real computeSurfaceArea() const;
	apt_real getSurfaceArea() const;
	void merge(const hedgesAABB&, const hedgesAABB&);
	bool contains(const hedgesAABB&) const;
	bool overlaps(const hedgesAABB&) const;
	trpl computeCentre();
	
	//variables
	trpl lowerBound;
	trpl upperBound;
	trpl centre;
	apt_real surfaceArea;
};

/*! \brief A node of the hedgesAABB tree.

    Each node of the tree contains an hedgesAABB object which corresponds to a
    particle, or a group of particles, in the simulation box. The AABB
    objects of individual particles are "fattened" before they are stored
    to avoid having to continually update and rebalance the tree when
    displacements are small.

    Nodes are aware of their position within in the tree. The isLeaf member
    function allows the tree to query whether the node is a leaf, i.e. to
    determine whether it holds a single particle.
*/
struct Node
{
	Node() : parent(NULL_NODE), next(NULL_NODE), left(NULL_NODE), right(NULL_NODE), height(0), particle(0) {};
	//##MK::add default constructor choices

	hedgesAABB aabb;
	//node dependencies, parent and next
	apt_uint parent;
	apt_uint next;

	//indices of left and right child nodes
	apt_uint left;
	apt_uint right;

	/// Height of the node. This is 0 for a leaf and -1 for a free node.
	int height;

	/// The index of the particle that the node contains (leaf nodes only).
	apt_uint particle;

	//! Test whether the node is a leaf.

	bool isLeaf() const;
};

/*! \brief The dynamic hedgesAABB tree.

    The dynamic hedgesAABB tree is a hierarchical data structure that can be used
    to efficiently query overlaps between objects of arbitrary shape and
    size that lie inside of a simulation box. Support is provided for
    periodic and non-periodic boxes, as well as boxes with partial
    periodicity, e.g. periodic along specific axes.
	//MK::periodicty was eliminated
    */
class Tree
{
public:
	Tree(apt_uint nParticles = ConfigShared::AABBTreeNodeCapacity );

	//! Insert a particle into the tree (point particle) with index, position and radius
	void insertParticle(apt_uint, const trpl, apt_real);

	//! Insert a particle into the tree (arbitrary shape with bounding box) with index, lower and upper
	void insertParticle(apt_uint, const trpl, const trpl);

	//! Remove a particle from the tree.
	void removeParticle(apt_uint);

	//MK::no updates of tree required

	//! Query the tree to find candidate interactions for a particle.
	vector<apt_uint> query(apt_uint);

	//! Query the tree to find candidate interactions for an hedgesAABB, in: particle index, out:: aabb, particles indices
	vector<apt_uint> query(apt_uint, const hedgesAABB&);

	//! Query the tree to find candidate interactions for an hedgesAABB, in an hedgesAABB, out particles indices
	vector<apt_uint> query(const hedgesAABB&);

	//! Get a particle hedgesAABB for particle index
	const hedgesAABB& getAABB(apt_uint);

	//! Get the height of the tree.
	apt_uint getHeight() const;

	//! Get the number of nodes in the tree
	apt_uint getNodeCount() const;

	//! Compute the maximum balance of the tree.
	apt_uint computeMaximumBalance() const;

	//! Compute the surface area ratio of the tree.
	apt_real computeSurfaceAreaRatio() const;

	//! Validate the tree.
	void validate() const;

	/*
	void report_tree( const string csv_fn );
	*/

private:
	/// The index of the root node.
	apt_uint root;

	/// The dynamic tree. //##MK::place better on heap...
	vector<Node> nodes;

	/// The current number of nodes in the tree.
	apt_uint nodeCount;

	/// The current node capacity.
	apt_uint nodeCapacity;

	/// The position of node at the top of the free list.
	apt_uint freeList;

	/// The size of the system in each dimension.
	//trpl boxSize;

	/// A map between particle and node indices.
	map<apt_uint, apt_uint> particleMap;

	//! Allocate a new node, returns index of the allocated node
	apt_uint allocateNode();

	//! Free an existing node, index of the node to be freed
	void freeNode(apt_uint);

	//! Insert a leaf into the tree, index of the leaf node
	apt_uint insertLeaf(apt_uint);

	//! Remove a leaf from the tree, index of the leaf node
	void removeLeaf(apt_uint);

	//! Balance the tree, index of the node
	apt_uint balance(apt_uint);

	//! Compute the height of the tree, height of the entire tree.
	apt_uint computeHeight() const;

	//! Compute the height of a sub-tree, in: index of rootNode, out: height of sub-tree
	apt_uint computeHeight(apt_uint) const;

	//! Assert that the sub-tree has a valid structure.
	void validateStructure(apt_uint) const;

	//! Assert that the sub-tree has valid metrics.
	void validateMetrics(apt_uint) const;
};


#endif
