/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_ARGSANDGITSHA_H__
#define __PARAPROBE_ARGSANDGITSHA_H__

#include "PARAPROBE_STLInterface.h"
#include "PARAPROBE_Numerics.h"
#include "PARAPROBE_PhysicalConstants.h"
#include "PARAPROBE_UnitConversions.h"
#include "PARAPROBE_CoordinateSystem.h"
#include "PARAPROBE_NeXusInterface.h"

//define what is passed upon execution
//each paraprobe-toolbox run has the following two input arguments
#define SIMID									1
#define CONTROLFILE								2

//low level stuff to parse the GitSHA of the parent commit into the executable upon build using cmake
//GITSHA gets defined from cmake -DGITSHA="
#ifndef GITSHA									//otherwise we could not compile without adding -DGITSHA="d123" a dummy GitSha
	#define GITSHA								"v0.5.1"
#endif

//stringification of strings passed from cmake at compile time
//https://gcc.gnu.org/onlinedocs/gcc-4.8.5/cpp/Stringification.html
#define xstr(s)									mystr(s)
#define mystr(s) 								#s


void command_line_help( const string cmd_option, const string toolname );

#endif
