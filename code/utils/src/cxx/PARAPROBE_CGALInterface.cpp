/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_CGALInterface.h"

pair<double, int> check_if_closed_polyhedron( vector<tri3u> const & fcts, vector<p3d> const & vrts )
{
	double tic = omp_get_wtime();
	double toc = 0.0;

	if ( fcts.size() < 1 || vrts.size() < 1 ) {
		return pair<double, int>( MYZERO, TRIANGLE_SOUP_ANALYSIS_NO_SUPPORT );
	}
	if ( fcts.size() < 4 || vrts.size() < 4 ) { //smallest closed polyhedron is a tetrahedron needs to have four nondegenerated disjoint facets and four disjoint vertices
		return pair<double, int>( MYZERO, TRIANGLE_SOUP_ANALYSIS_INSUFFICIENT_SUPPORT );
	}

	//MK::not every triangle mesh esepcially not arbitrary triangle soups describes a watertight polyhedron, there can be problems
	//MK::inconsistently oriented triangles, holes, missing triangles, connectivity issues, so sophisticated mesh processing is needed to check for watertightness
	//MK::here we use the CGAL library
	//https://doc.cgal.org/latest/Polygon_mesh_processing/Polygon_mesh_processing_2repair_polygon_soup_example_8cpp-example.html
	vector<array<KK::FT, 3>> points;
	vector<vector<size_t>> polygons;
	size_t vertexID = 0;	
	for ( auto it = fcts.begin(); it != fcts.end(); it++ ) {
		points.push_back( CGAL::make_array<KK::FT>(vrts[it->v1].x, vrts[it->v1].y, vrts[it->v1].z) );
		points.push_back( CGAL::make_array<KK::FT>(vrts[it->v2].x, vrts[it->v2].y, vrts[it->v2].z) );
		points.push_back( CGAL::make_array<KK::FT>(vrts[it->v3].x, vrts[it->v3].y, vrts[it->v3].z) );
		polygons.push_back( { vertexID+0, vertexID+1, vertexID+2 } );
		vertexID += 3;
	}

	//MK::combinatorial repairing eventually modifies points and polygon arrays
	CGAL::Polygon_mesh_processing::repair_polygon_soup( points, polygons, CGAL::parameters::geom_traits(Array_traits()) );

	if ( points.size() < 4 || polygons.size() < 4 ) { //smallest closed polyhedron is a tetrahedron needs to have four nondegenerated disjoint facets and four disjoint vertices
		return pair<double, int>( MYZERO, TRIANGLE_SOUP_ANALYSIS_ERROR_REPAIRING );
	}
	
	//not returned, so repairing was possible which means the triangle soup can at least represent a surface patch
	//but can it represent more, we know now that this mesh is a surface_mesh
	//but not every surface mesh is also a (closed) polyhedron
	//e.g. take a triangle strip: that strip can be surface_mesh but that strip is not a necessarily a polyhedron,
	//yes, maybe a polyhedron cut open and unfolded but definately not required a closed polyhedron...
	//a surface_mesh is also not necessarily built only from triangles could also be build from prims like quads or other type of polygons
	//but by virtue of construction we can rule the last case out

	//check if the triangle soup is orientable, a Moebius band e.g. is not a consistently orientable manifold !

	if ( CGAL::Polygon_mesh_processing::orient_polygon_soup( points, polygons ) == false ) {
		return pair<double, int>( MYZERO, TRIANGLE_SOUP_ANALYSIS_SELFINTERSECTING );
	}

	//now we know the triangle soup is an orientable manifold

	CGAL::Surface_mesh<KK::Point_3> mesh;
	CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh( points, polygons, mesh );

	//points and polygons from now on no longer needed to release memory
	points = vector<array<KK::FT, 3>>();
	polygons = vector<vector<size_t>>();

	//##MK::in a seemingly ackward move to eventually check if the volume is bounded, I go from a Surface_mesh to polyhedron (3D polyhedral mesh processing)
	//##MK::here eventually copies can be improved but that seems to me needs more expert knowledge of CGAL
	//##MK::i.e. an optimization for the future

	vector<KK::Point_3> ply_points;
	vector<vector<size_t>> ply_polygons;
	//convert the Surface mesh into a polyhedron with which to test the closure of the mesh
	CGAL::Surface_mesh<KK::Point_3>::Vertex_range vrng = mesh.vertices();
	CGAL::Surface_mesh<KK::Point_3>::Vertex_range::iterator vb = vrng.begin();
	CGAL::Surface_mesh<KK::Point_3>::Vertex_range::iterator ve = vrng.end();
	for(   ; vb != ve; ++vb ) {
		ply_points.push_back( KK::Point_3( mesh.point(*vb).x(), mesh.point(*vb).y(), mesh.point(*vb).z() ) );
	}
	CGAL::Surface_mesh<KK::Point_3>::Face_range frng = mesh.faces();
	CGAL::Surface_mesh<KK::Point_3>::Face_range::iterator fb = frng.begin();
	CGAL::Surface_mesh<KK::Point_3>::Face_range::iterator fe = frng.end();
	for(   ; fb != fe; ++fb ) {
		//https://doc.cgal.org/latest/Surface_mesh/index.html#circulators_example
		vector<size_t> p;
		CGAL::Vertex_around_face_iterator<CGAL::Surface_mesh<KK::Point_3>> vbegin;
		CGAL::Vertex_around_face_iterator<CGAL::Surface_mesh<KK::Point_3>> vend;
		for( boost::tie(vbegin, vend) = vertices_around_face(mesh.halfedge(*fb), mesh); vbegin != vend; ++vbegin ) {
			p.push_back( (size_t) *vbegin );
	    	}
	    	ply_polygons.push_back( p );
	}

	CGAL::Polyhedron_3<KK, CGAL::Polyhedron_items_with_id_3> ply_mesh;
	CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh( ply_points, ply_polygons, ply_mesh );

	// Number the faces because 'orient_to_bound_a_volume' needs a face <--> index map
	int index = 0;
	for( CGAL::Polyhedron_3<KK, CGAL::Polyhedron_items_with_id_3>::Face_iterator ply_fb = ply_mesh.facets_begin(), ply_fe = ply_mesh.facets_end(); ply_fb != ply_fe; ++ply_fb ) {
		ply_fb->id() = index++;
	}

	//check if the polygon is closed
	if ( CGAL::is_closed(ply_mesh) == false ) {
		toc = omp_get_wtime();
		cout << "Watertight model: no, volume is undefined, checking watertightness took " << (toc-tic) << " seconds" << "\n";
		return pair<double,int>( MYZERO, TRIANGLE_SOUP_ANALYSIS_SURFACEPATCH );
	}

	//it is okay then orient to bound a volume a try to compute the interior volume
	CGAL::Polygon_mesh_processing::orient_to_bound_a_volume(ply_mesh);

	//http://cgal-discuss.949826.n4.nabble.com/normal-vector-of-a-facet-td1580004.html
	//the so computed normals result not in the same order as the facets so correct normals
	//will be assigned to the wrong facets, better follow the last hint on above with the parity of the facet
	//CGAL::Polygon_mesh_processing::compute_face_normals( ply_mesh, boost::make_assoc_property_map(ply_mesh_fcts_ounrm) );

	double volume = CGAL::Polygon_mesh_processing::volume( ply_mesh );
	
	toc = omp_get_wtime();
	cout << "Watertight model: yes, volume is " << volume << " nm^3, checking watertightness took " << (toc-tic) << " seconds" << "\n";

	return pair<double,int>( volume, TRIANGLE_SOUP_ANALYSIS_CLOSEDPOLYHEDRON );
}
