/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_CiteMe.h"

vector<bibtex> CiteParaprobe::Citations = vector<bibtex>();


ostream& operator<<(ostream& in, bibtex const & val)
{
	if ( val.entrytype != "" ) {
		in << val.entrytype << "\n";
	}
	if ( val.author != "" ) {
		in << val.author << "\n";
	}
	if ( val.title != "" ) {
		in << val.title << "\n";
	}
	if ( val.journal != "" ) {
		in << val.journal;
	}
	if ( val.year != "" ) {
		in << ", " << val.year;
	}
	if ( val.vol != "" ) {
		in << ", " << val.vol;
	}
	if ( val.pages != "" ) {
		in << ", pp" << val.pages;
	}
	//if ( val.year != "" ) {
	in << "\n";
	//}
	if ( val.doi != "" ) {
		in << val.doi << "\n";
	}
	in << "\n";
	return in;
}


size_t CiteParaprobe::cite()
{
	//add additional citations
	Citations.push_back( bibtex( 	"Article",
									"M. K\"uhbach and P. Bajaj and A. Breen and E. A. J\"agle and B. Gault",
									"On Strong Scaling Open Source Tools for Mining Atom Probe Tomography Data",
									"Microscopy and Microanalysis",
									"2019",
									"Volume 25, Supplement S2",
									"298-299",
									"https://doi.org/10.1017/S1431927619002228" ) );

	Citations.push_back( bibtex( 	"Article",
									"M. K\"uhbach and P. Bajaj and H. Zhao and M. H. C\"{c}elik E. A. J\"agle and B. Gault",
									"On strong-scaling and open-source tools for analyzing atom probe tomography data",
									"npj Computational Materials",
									"2021",
									"Volume 7",
									"Article number 21",
									"https://doi.org/10.1038/s41524-020-00486-1") );

	Citations.push_back( bibtex( 	"Article",
									"M. K\"uhbach and A. J. London  and J. Wang and D. K. Schreiber and F. Mendez-Martin and I. Ghamarian and H. Bilal and A. V.Ceguerra",
									"Community-Driven Methods for Open and Reproducible Software Tools for Analyzing Datasets from Atom Probe Microscopy",
									"Microscopy and Microanalysis",
									"2021",
									"",
									"1-16",
									"https://doi.org/10.1017/S1431927621012241" ) );

	Citations.push_back( bibtex( 	"Article",
									"M. K\"uhbach and M. Kasemer and A. Breen and B. Gault",
									"Open and strong-scaling tools for atom-probe crystallography: high-throughput methods for indexing crystal structure and orientation",
									"Journal of Applied Crystallography",
									"2021",
									"Volume 54",
									"1490-1508",
									"https://doi.org/10.1107/S1600576721008578" ) );

	Citations.push_back( bibtex( 	"Preprint",
									"M. K\"uhbach and V. V. Rielli and S. Primig and A. Saxena and B. Jenkins and D. Mayweg and A. Reichmann and S. Kardos and L. Romaner and S. Brockhauser",
									"CGM paper",
									"arXiv",
									"2022",
									"",
									"",
									"https://arxiv.org/abs/2205.13510" ) );

	return Citations.size();
}
