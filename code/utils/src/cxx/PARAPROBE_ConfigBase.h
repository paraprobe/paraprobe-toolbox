/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_CONFIGBASE_H__
#define __PARAPROBE_UTILS_CONFIGBASE_H__

//##MK::include ######################


class ConfigBase
{
public:
	//spatial filtering, add ROI filtering and sub-sampling functionalities
	static WINDOWING_METHOD WindowingMethod;
	static vector<roi_sphere> WindowingSpheres;
	static vector<roi_rotated_cylinder> WindowingCylinders;
	static vector<roi_rotated_cuboid> WindowingCuboids;
	static vector<roi_polyhedron> WindowingPolyhedra;
	//sub-sampling
	static lival<apt_uint> LinearSubSamplingRange;
	//ion property match filters
	static match_filter<unsigned char> IontypeFilter;
	static match_filter<unsigned char> HitMultiplicityFilter;

	void load_spatial_filter( const string nx5fn, const string prefix );
	void load_sub_sampling_filter( const string nx5fn, const string prefix );
	void load_ion_type_filter( const string nx5fn, const string prefix );
	void load_hit_multiplicity_filter( const string nx5fn, const string prefix );
};


#endif
