/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ConfigShared.h"


template<typename T>
lival<T>::lival()
{
	min = MYZERO;
	incr = MYONE;
	max = MYZERO;
}


template lival<unsigned int>::lival();
template lival<int>::lival();
template lival<float>::lival();
template lival<double>::lival();


template<typename T>
lival<T>::lival( const T _mi, const T _incr, const T _mx )
{
	min = _mi;
	incr = _incr;
	max = _mx;
}

template lival<unsigned int>::lival( const unsigned int _mi, const unsigned int _incr, const unsigned int _mx );
template lival<int>::lival( const int _mi, const int _incr, const int _mx );
template lival<float>::lival( const float _mi, const float _incr, const float _mx );
template lival<double>::lival( const double _mi, const double _incr, const double _mx );



template<typename T>
lival<T>::~lival()
{
}

template lival<unsigned int>::~lival();
template lival<int>::~lival();
template lival<float>::~lival();
template lival<double>::~lival();


template<typename T>
bool lival<T>::is_valid_first_quadrant()
{
	if ( min >= MYZERO && max >= min ) {
		return true;
	}
	return false;
}

template bool lival<unsigned int>::is_valid_first_quadrant();
template bool lival<int>::is_valid_first_quadrant();
template bool lival<float>::is_valid_first_quadrant();
template bool lival<double>::is_valid_first_quadrant();


template <typename T>
ostream& operator<< (ostream& in, lival<T> const & val)
{
	in << "LinearSubSamplingRange" << "\n";
	in << "[ " << val.min << ", " << val.incr << ", " << val.max << " ]" << "\n";
	return in;
}

template ostream& operator<< (ostream& in, lival<unsigned int> const & val);
template ostream& operator<< (ostream& in, lival<int> const & val);
template ostream& operator<< (ostream& in, lival<float> const & val);
template ostream& operator<< (ostream& in, lival<double> const & val);


template<typename T>
match_filter<T>::match_filter()
{
	candidates = vector<T>();
	is_whitelist = false;
	is_blacklist = false;
}

template match_filter<unsigned char>::match_filter();
template match_filter<unsigned int>::match_filter();


template<typename T>
match_filter<T>::match_filter( vector<T> const & _cand, const string _type )
{
	candidates = vector<T>();
	if ( _type.compare("whitelist") == 0 ) {
		is_whitelist = true;
		is_blacklist = false;
		candidates = _cand;
	}
	else if ( _type.compare("blacklist") == 0 ) {
		is_whitelist = false;
		is_blacklist = true;
		candidates = _cand;
	}
	else {
		is_whitelist = false;
		is_blacklist = false;
	}
}

template match_filter<unsigned char>::match_filter( vector<unsigned char> const & _cand, const string _type );
template match_filter<unsigned int>::match_filter( vector<unsigned int> const & _cand, const string _type );


template <typename T>
ostream& operator<< (ostream& in, match_filter<T> const & val)
{
	in << "MatchFilter" << "\n";
	if ( val.is_whitelist == false && val.is_blacklist == false ) {
		in << "match_filter is switched off" << "\n";
	}
	if ( val.is_whitelist == true && val.is_blacklist == false ) {
		in << "match_filter is switched on as a whitelist" << "\n";
	}
	if ( val.is_whitelist == false && val.is_blacklist == true ) {
		in << "match_filter is switched on as a blacklist" << "\n";
	}
	if ( val.is_whitelist == true && val.is_blacklist == true ) {
		in << "match_filter is switched off" << "\n";
	}
	in << "candidates" << "\n";
	if constexpr(std::is_same<T, unsigned char>::value) {
		for ( auto it = val.candidates.begin(); it != val.candidates.end(); it++ ) {
			in << (int) *it << "\n";
		}
	}
	else {
		for ( auto it = val.candidates.begin(); it != val.candidates.end(); it++ ) {
			in << *it << "\n";
		}
	}
	return in;
}

template ostream& operator<< (ostream& in, match_filter<unsigned char> const & val);
template ostream& operator<< (ostream& in, match_filter<unsigned int> const & val);



//predefined values
apt_uint ConfigShared::SimID = 0;
string ConfigShared::ConfigurationFile = "";
string ConfigShared::OutputfilePrefix = ""; //pattern is "PARAPROBE.<toolname>.Results.SimID.<simid>"
string ConfigShared::OutputfileName = ""; //pattern is "PARAPROBE.<toolname>.Results.SimID.<simid>.nxs"

size_t ConfigShared::RndDescrStatsPRNGSeed = -1;
size_t ConfigShared::RndDescrStatsPRNGDiscard = 700000;

apt_real ConfigShared::MinPDFResolution = 0.1; //nm
apt_real ConfigShared::AABBGuardZone = 0.1; //nm
apt_real ConfigShared::VolumeBinningEdgeLength = 2.; //nm

apt_int ConfigShared::MaxVoxelsTwoPointStats = 503;
apt_uint ConfigShared::AABBTreeNodeCapacity = 16;
//apt_uint ConfigShared::MaxLeafsAABBTree = 16;
apt_uint ConfigShared::MaxLeafsKDTree = 256;
size_t ConfigShared::KDTreeStackSize = 32;


void tool_startup( const string toolname )
{
	cout << toolname << "\n";
	cout << "A tool of the paraprobe-toolbox supporting FAIR materials science research" << "\n";
	cout << "Supporting the community with strong-scaling and open tools for robust and automated uncertainty quantification..." << "\n";
	cout << "\n";
	cout << "The compiled code of this tool uses the source code with the following GitSha:" << "\n";
	cout << "v0.4" << "\n";
	string githash = string(xstr(GITSHA));
	cout << githash << "\n";
	cout << "Preprocessor run" << "\n";
	string preprocessor_date = string(__DATE__);
	string preprocessor_time = string(__TIME__);
	cout << preprocessor_date << "\n";
	cout << preprocessor_time << "\n";
	cout << "Paraprobe can be cited via the following papers..." << "\n";
	cout << "Collecting " << CiteParaprobe::cite() << " publications for the tool to cite:" << "\n";
	for( auto it = CiteParaprobe::Citations.begin(); it != CiteParaprobe::Citations.end(); it++ ) {
		cout << *it << "\n";
	}
}
