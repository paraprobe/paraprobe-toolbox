/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_CONFIGSHARED_H__
#define __PARAPROBE_UTILS_CONFIGSHARED_H__

#include "PARAPROBE_CiteMe.h"

template<typename T>
struct lival
{
	T min;
	T incr;
	T max;
	lival();
	lival( const T _mi, const T _incr, const T _mx );
	~lival();
	bool is_valid_first_quadrant();
};

template<typename T>
ostream& operator<<(ostream& in, lival<T> const & val);


template<typename T>
struct match_filter
{
	vector<T> candidates;
	bool is_whitelist;
	bool is_blacklist;

	match_filter();
	match_filter( vector<T> const & _cand, const string _type );
};

template<typename T>
ostream& operator<<(ostream& in, match_filter<T> const & val);


void tool_startup( const string toolname );


class ConfigShared
{
public:
	static apt_uint SimID;
	static string ConfigurationFile;
	static string OutputfilePrefix;
	static string OutputfileName;

	static size_t RndDescrStatsPRNGSeed;
	static size_t RndDescrStatsPRNGDiscard;

	static apt_real MinPDFResolution;
	static apt_real AABBGuardZone;
	static apt_real VolumeBinningEdgeLength;

	static apt_int MaxVoxelsTwoPointStats;
	static apt_uint AABBTreeNodeCapacity;
	static apt_uint MaxLeafsKDTree;
	static size_t KDTreeStackSize;
};


#endif
