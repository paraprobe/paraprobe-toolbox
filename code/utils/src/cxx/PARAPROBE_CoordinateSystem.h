/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_COORDINATESYSTEM_H__
#define __PARAPROBE_COORDINATESYSTEM_H__

//paraprobe-toolbox uses a Cartesian coordinate system which is right-handed x,y,z
//objects are represented in 3D Euclidean space and time
#define PARAPROBE_XAXIS					0
#define PARAPROBE_YAXIS					1
#define PARAPROBE_ZAXIS					2


#endif
