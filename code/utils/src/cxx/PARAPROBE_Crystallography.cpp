/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_Crystallography.h"


ostream& operator<<(ostream& in, motifunit const & val)
{
	in << "u1 " << val.u1 << "\n";
	in << "u2 " << val.u2 << "\n";
	in << "u3 " << val.u3 << "\n";
	in << "ivec " << val.ivec << "\n";
	pair<unsigned char, unsigned char> pro_neu = isotope_unhash( val.ivec );
	in << "protons " << (int) pro_neu.first << "\n";
	in << "neutrons " << (int) pro_neu.second << "\n";
	return in;
}


crystalstructure::crystalstructure()
{
	MySpacegroups = { 62, 141, 194, 221, 225, 229 };

	a = MYZERO;
	b = MYZERO;
	c = MYZERO;
	alpha = MYZERO;
	beta = MYZERO;
	gamma = MYZERO;
	mtrx = t3x3();
	a1 = p3d(MYONE, MYZERO, MYZERO);
	a2 = p3d(MYZERO, MYONE, MYZERO);
	a3 = p3d(MYZERO, MYZERO, MYONE);
	spacegroup = 0;
	pad = 0;
	aabb = aabb3d();
	box_len = MYZERO;
	motif = vector<motifunit>();
}


crystalstructure::crystalstructure(
		const apt_real _a, const apt_real _b, const apt_real _c,
			const apt_real _alpha, const apt_real _beta, const apt_real _gamma,
				const unsigned short _spacegrp, vector<motifunit> const & in )
{
	MySpacegroups = { 62, 141, 194, 221, 225, 229 };

	a = _a;
	b = _b;
	c = _c;
	alpha = DEGREE2RADIANT(_alpha);
	beta = DEGREE2RADIANT(_beta);
	gamma = DEGREE2RADIANT(_gamma);
	mtrx = t3x3();
	a1 = p3d(MYONE, MYZERO, MYZERO);
	a2 = p3d(MYZERO, MYONE, MYZERO);
	a3 = p3d(MYZERO, MYZERO, MYONE);
	spacegroup = _spacegrp;
	pad = 0;

	//make spacegroup-specific definitions, we use primitive cells
	switch(spacegroup)
	{
		//##MK::list further space group members
		//first, the constraints on the lattice base vectors
		//second, the transformation matrix mtrx
		//case XX:
		//	constraints on a,b,c
		//	mtrx

		case 62: //Pnma, e.g. CaTiO3
			mtrx = t3x3( 	a,				MYZERO,			MYZERO,
							MYZERO,			b, 				MYZERO,
							MYZERO,			MYZERO,			c );
			break;
		case 141: //I4_1/amd, e.g. Zircon, body-centered tetragonal primitive
			b = _a;
			mtrx = t3x3(	-0.5*a,		+0.5*a,		+0.5*c,
							+0.5+a,		-0.5*a,		+0.5*c,
							+0.5*a,		+0.5*a,		-0.5*c  );
			break;
		case 194: //hcp, e.g. Mg
			b = _a;
			mtrx = t3x3( 	+0.5*a,		-0.5*sqrt(3.0)*a,		MYZERO,
							+0.5*a,		+0.5*sqrt(3.0)*a,		MYZERO,
							MYZERO,		MYZERO,					c );
			break;
		case 221: //L12, e.g. Cu3Al
			//set specific specialization of the tricline case
			b = _a;
			c = _a;
			//alpha = DEGREE2RADIANT(90.0);
			//beta = DEGREE2RADIANT(90.0);
			//gamma = DEGREE2RADIANT(90.0);
			mtrx = t3x3( 	a,			MYZERO,		MYZERO,
							MYZERO,		a, 			MYZERO,
							MYZERO,		MYZERO,		a );
			break;
		case 225: //fcc, e.g. Cu
			b = _a;
			c = _a;
			//alpha = DEGREE2RADIANT(60.0);
			//beta = DEGREE2RADIANT(60.0);
			//gamma = DEGREE2RADIANT(60.0);
			mtrx = t3x3( 	MYZERO,			+0.5*a,		+0.5*a,
							+0.5*a,			MYZERO,		+0.5*a,
							+0.5*a,			+0.5*a,		MYZERO );
			break;
		case 229: //bcc, e.g. W
			b = _a;
			c = _a;
			/*alpha = DEGREE2RADIANT(60.0);
			beta = DEGREE2RADIANT(60.0);
			gamma = DEGREE2RADIANT(60.0);*/
			mtrx = t3x3( 	-0.5*a,		+0.5*a,		+0.5*a,
							+0.5*a,		-0.5*a,		+0.5*a,
							+0.5*a,		+0.5*a,		-0.5*a );
			break;
		default:
			cerr << "The space group is not supported, implement it use e.g. the aflow.org/CrystalDatabase !" << "\n"; break;
	}
	//compute lattice vectors in Cartesian space mtrx leftmultiplied on Cartesian coordinate system base vectors
	p3d xbar = p3d( MYONE, MYZERO, MYZERO );
	p3d ybar = p3d( MYZERO, MYONE, MYZERO );
	p3d zbar = p3d( MYZERO, MYZERO, MYONE );
	a1 = p3d( 	mtrx.a11 * xbar.x + mtrx.a12 * ybar.x + mtrx.a13 * zbar.x,
				mtrx.a11 * xbar.y + mtrx.a12 * ybar.y + mtrx.a13 * zbar.y,
				mtrx.a11 * xbar.z + mtrx.a12 * ybar.z + mtrx.a13 * zbar.z   );

	a2 = p3d( 	mtrx.a21 * xbar.x + mtrx.a22 * ybar.x + mtrx.a23 * zbar.x,
				mtrx.a21 * xbar.y + mtrx.a22 * ybar.y + mtrx.a23 * zbar.y,
				mtrx.a21 * xbar.z + mtrx.a22 * ybar.z + mtrx.a23 * zbar.z   );

	a3 = p3d( 	mtrx.a31 * xbar.x + mtrx.a32 * ybar.x + mtrx.a33 * zbar.x,
				mtrx.a31 * xbar.y + mtrx.a32 * ybar.y + mtrx.a33 * zbar.y,
				mtrx.a31 * xbar.z + mtrx.a32 * ybar.z + mtrx.a33 * zbar.z   );
	//copy in the motif, ##MK::no copy-in check here
	motif = vector<motifunit>();
	for( auto it = in.begin(); it != in.end(); it++ ) {
		motif.push_back( *it );
	}

	//utility tasks
	//get uvw lattice plane limits to fully enclose tip AABB, ##MK::exemplarily fcc crystal lattice only here
	//the unit cell is a parallelepiped so we get the bounding box by checking the extremal positions of its 8 vertices
	vector<p3d> verts;
	verts.push_back( p3d(	MYZERO, MYZERO, MYZERO) ); //origin, or rebase
	verts.push_back( p3d( 	MYZERO + a1.x,
							MYZERO + a1.y,
							MYZERO + a1.z) ); //a0a1
	verts.push_back( p3d( 	MYZERO + a2.x,
							MYZERO + a2.y,
							MYZERO + a2.z) ); //a0a2
	verts.push_back( p3d( 	MYZERO + a3.x,
							MYZERO + a3.y,
							MYZERO + a3.z) ); //a0a3
	verts.push_back( p3d( 	MYZERO + a1.x + a2.x,
							MYZERO + a1.y + a2.y,
							MYZERO + a1.z + a2.z) ); //a0a1a2
	verts.push_back( p3d( 	MYZERO + a3.x + a2.x,
							MYZERO + a3.y + a2.y,
							MYZERO + a3.z + a2.z) ); //a0a3a2
	verts.push_back( p3d(	MYZERO + a1.x + a3.x,
							MYZERO + a1.y + a3.y,
							MYZERO + a1.z + a3.z) ); //a0a1a3
	verts.push_back( p3d( 	MYZERO + a1.x + a2.x + a3.x,
							MYZERO + a1.y + a2.y + a3.y,
							MYZERO + a1.z + a2.z + a3.z ) ); //a0a1a2a3

	for ( auto it = verts.begin(); it != verts.end(); it++ ) {
		aabb.possibly_enlarge_me( *it );
	}

	//use shortest unit cell base vector to scale sphere ##MK::find a more efficient way
	apt_real a1len = SQR(a1.x - MYZERO) + SQR(a1.y - MYZERO) + SQR(a1.z - MYZERO);
	apt_real a2len = SQR(a2.x - MYZERO) + SQR(a2.y - MYZERO) + SQR(a2.z - MYZERO);
	apt_real a3len = SQR(a3.x - MYZERO) + SQR(a3.y - MYZERO) + SQR(a3.z - MYZERO);
	apt_real minlen = min(min(a1len, a2len), a3len);
	if ( minlen > EPSILON ) {
		box_len = sqrt(minlen);
	}
	else {
		cerr << "Computation of minimum unit cell length results in a too low value, unsafe to compute sqrt from !" << "\n";
		box_len = MYZERO;
	}
}


crystalstructure::~crystalstructure()
{
}


apt_real crystalstructure::unitcell_volume()
{
	p3d a2a3 = cross( a2, a3 );
	return fabs( dot( a1, a2a3 ) );
}


p3d crystalstructure::get_atom_pos( const size_t _b, const int _u, const int _v, const int _w )
{
	motifunit atom = motif.at(_b);
	return p3d(
			(atom.u1 + _u)*a1.x + (atom.u2 + _v)*a2.x + (atom.u3 + _w)*a3.x,
			(atom.u1 + _u)*a1.y + (atom.u2 + _v)*a2.y + (atom.u3 + _w)*a3.y,
			(atom.u1 + _u)*a1.z + (atom.u2 + _v)*a2.z + (atom.u3 + _w)*a3.z  );
}


unsigned short crystalstructure::get_atom_type( const size_t b )
{
	return motif.at(b).ivec;
}


unsigned char crystalstructure::get_element_Z( const size_t b )
{
	return motif.at(b).nprotons;
}


unsigned char crystalstructure::get_chargestate( const size_t b )
{
	return motif.at(b).chargestate;
}


ostream& operator<<(ostream& in, crystalstructure const & val)
{
	in << "Crystal structure candidate" << "\n";
	in << "a " << val.a << "\n";
	in << "b " << val.b << "\n";
	in << "c " << val.c << "\n";
	in << "alpha " << RADIANT2DEGREE(val.alpha) << "\n";
	in << "beta " << RADIANT2DEGREE(val.beta) << "\n";
	in << "gamma " << RADIANT2DEGREE(val.gamma) << "\n";
	in << "mtrx ( " << val.mtrx.a11 << ", " << val.mtrx.a12 << ", " << val.mtrx.a13 << ", " << "\n";
	in << "         " << val.mtrx.a21 << ", " << val.mtrx.a22 << ", " << val.mtrx.a23 << ", " << "\n";
	in << "         " << val.mtrx.a31 << ", " << val.mtrx.a32 << ", " << val.mtrx.a33 << " ) " << "\n";
	in << "a1 " << val.a1.x << ", " << val.a1.y << ", " << val.a1.z << "\n";
	in << "a2 " << val.a2.x << ", " << val.a2.y << ", " << val.a2.z << "\n";
	in << "a3 " << val.a3.x << ", " << val.a3.y << ", " << val.a3.z << "\n";
	in << "spacegroup " << val.spacegroup << "\n";
	return in;
}
