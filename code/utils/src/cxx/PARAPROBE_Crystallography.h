/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_CRYSTALLOGRAPHY_H__
#define __PARAPROBE_UTILS_CRYSTALLOGRAPHY_H__

#include "PARAPROBE_PeriodicTable.h"


struct motifunit
{
	apt_real u1;					//fractional coordinates
	apt_real u2;
	apt_real u3;
	unsigned short ivec;
	unsigned char nprotons;			//element number
	unsigned char chargestate;		//assumed charge state, ##MK::here used to decode mass-to-charge
	motifunit() : u1(MYZERO), u2(MYZERO), u3(MYZERO), ivec(isotope_hash(0,0)), nprotons(0x00), chargestate(0x00) {}
	motifunit( const apt_real _u1, const apt_real _u2, const apt_real _u3, const unsigned short _ivec ):
			u1(_u1), u2(_u2), u3(_u3), ivec(_ivec), nprotons(0x00), chargestate(0x00) {}
};

ostream& operator<<(ostream& in, motifunit const & val);


class crystalstructure
{
public:
	crystalstructure();
	crystalstructure( const apt_real _a, const apt_real _b, const apt_real _c,
			const apt_real _alpha, const apt_real _beta, const apt_real _gamma,
				const unsigned short _spacegrp, vector<motifunit> const & in );
	~crystalstructure();

	apt_real unitcell_volume();
	p3d get_atom_pos( const size_t _b, const int _u, const int _v, const int _w );
	unsigned short get_atom_type( const size_t b );
	unsigned char get_element_Z( const size_t b );
	unsigned char get_chargestate( const size_t b );

	apt_real a;
	apt_real b;
	apt_real c;
	apt_real alpha;
	apt_real beta;
	apt_real gamma;
	t3x3 mtrx;
	p3d a1;
	p3d a2;
	p3d a3;
	unsigned short spacegroup;
	unsigned short pad;
	//utility, an axis-aligned bounding box about the given primitive cell in lab coordinate system
	aabb3d aabb;
	apt_real box_len;

	vector<motifunit> motif;
	vector<unsigned short> MySpacegroups;
};

ostream& operator<<(ostream& in, crystalstructure const & val);


#endif
