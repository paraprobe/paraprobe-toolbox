/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_FFTINTERFACE_H__
#define __PARAPROBE_UTILS_FFTINTERFACE_H__

#include "PARAPROBE_HDF5Core.h"

//MK::here use to switch between different FFT libraries
#define EMPLOY_FFTW
//#define EMPLOY_IMKL
//#define EMPLOY_CUFFT

#ifdef EMPLOY_FFTW
	//##MK::add includes for fftw
#endif

#ifdef EMPLOY_IMKL
	//##MK::add includes for Intel Math Kernel FFT library
#endif

#ifdef EMPLOY_CUFFT
	//##MK::add includes for Nvidia CUDA cuFFT library
#endif

#endif

