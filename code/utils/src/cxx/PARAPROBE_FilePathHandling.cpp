/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_FilePathHandling.h"


/*
string cwd = "/home/mkuehbach/tools/src"
		print(f"cwd: {cwd}")
		abs_fnm = squeeze("/", "/home/mkuehbach//////////////../../tools/../src/c.cpp")
		print(f"abs_fnm: {abs_fnm}")
		rel_fnm = "../home/mkuehbach/../../c.cpp"
		rel_fnm = "mkuehbach/tools/src/c.cpp"
		rel_fnm = "../c.cpp"
		print(f"rel_fnm: {rel_fnm}")
*/


string win_to_linux_path( const string & str )
{
	//replaces Windows backward slashes to Unix forward slashes
	string tmp = str;
	replace(tmp.begin(), tmp.end(), '\\', '/');
	return tmp;
}


int is_absolute_path( const string & path )
{
	//checks if path is an absolute path (1 if yes, 0 if not, -1 if empty)
	if ( path.length() > 0 ) {
		if ( path.at(0) == '/' ) {
			return 1;
		}
		return 0;
	}
	return -1;
}


string get_current_working_directory()
{
	char buf[FILENAME_MAX];
	if ( getcwd(buf, FILENAME_MAX) != NULL ) {
		string cwd = string(buf);
		//cout << "__" << cwd << "__" << "\n";
		return cwd;
	}
	return "";
}


string replace_repeating_forward_slashes( const string & str )
{
	//shortens sequences of consecutive forward slashes to one forward slash
	string tmp = "";
	for( size_t i = 0; i < str.length() - 1; i++ ) {
		if ( str.at(i) != '/' ) {
			tmp += str.at(i);
		}
		else {
			if ( str.at(i+1) != '/') {
				tmp += str.at(i);
			}
		}
	}
	tmp += str.back();
	return tmp;
}


size_t count_sub_string( const string & str, const string & sub )
{
    // returns count of non-overlapping occurrences of 'sub' in 'str'
	if (sub.length() == 0) {
    	return 0;
    }
    size_t count = 0;
    for (size_t offset = str.find(sub); offset != std::string::npos;
    		offset = str.find(sub, offset + sub.length())) {
        ++count;
    }
    return count;
}


string strip_path_prefix( const string path )
{
	if ( path.length() > 0 ) {
		size_t idx_s = path.find_last_of('/');
		if ( idx_s != string::npos ) {
			if ( idx_s + 1 < path.length() ) {
				return path.substr(idx_s + 1, path.length() - idx_s);
			}
			else {
				return "";
			}
		}
		else {
			return path;
		}
	}
	return "";
}


vector<string> split_on_forward_slash( const string path )
{
	vector<string> retval = vector<string>();
	char delimiter = '/';
	string fwslash = "/";

	if ( path.length() > 0 ) {
		size_t idx_s = 0;
		size_t idx_e = path.length();
		//remove eventually preceeding and/or trailing fwslash
		if (path.rfind(delimiter, 0) != string::npos) {
			idx_s++;
		}
		if (path.compare(path.length()-1, 1, fwslash) == 0) {
		  idx_e--;
		}
		stringstream sstream("");
		for ( size_t i = idx_s; i < idx_e; ++i) {
			sstream << path.at(i);
		}
		string tmp = "";
		while( getline(sstream, tmp, delimiter) ) {
			retval.push_back(tmp);
		}
	}
	return retval;
}


string remove_deindents( const string & path )
{
	//clean a path of intermediate jump backs to parent directories
	//cout << "---------------------------->" << "\n";
	//cout << "path: " << path << "\n";
	vector<string> path_split = split_on_forward_slash(path);
	//boost::split(path_split, path, boost::is_any_of("/"));
	//for( auto it = path_split.begin(); it != path_split.end(); it++ ) {
	//	cout << "__" << *it << "__" << "\n";
	//}
	//cout << ">>>>>>>>>>" << "\n";
	vector<string> clean_path;
	for( size_t i = 0; i < path_split.size(); i++ ) {
		//cout << "__" << path_split.at(i) << "__" << "\n";
		if ( path_split.at(i).size() > 0 ) {
			if ( path_split[i].compare("..") == 0 ) {
				//cout << "is .." << "\n";
				//cout << clean_path.size() << "\n";
				if ( clean_path.size() > 0 ) {
					clean_path.pop_back();
				}
				//cout << clean_path.size() << "\n";
			}
			else {
				//cout << "is ok" << "\n";
				clean_path.push_back( path_split[i] );
			}
		}
	}
	string retval = "";
	if ( clean_path.size() > 0 ) {
		bool absolute = false;
		if ( is_absolute_path( path ) == 1 ) {
			absolute = true;
		}
		for( size_t i = 0; i < clean_path.size(); i++ ) {
			if ( i != 0 ) {
				retval += "/" + clean_path[i];
			}
			else {
				if ( absolute == true ) {
					retval += "/";
				}
				retval += clean_path[i];
			}
		}
		//cout << "__" << retval << "__" << "\n";
	}
	return retval;
}


string relative_to_absolute_path(const string cwd, const string rel_path)
{
	if ( rel_path.compare("") != 0 ) {
		if ( rel_path.at(0) != '/' ) {
			string sub = "../";
			size_t n_deindent = count_sub_string( rel_path, sub );
			cout << n_deindent << "\n";
			vector<string> rel_path_split = split_on_forward_slash(rel_path);
			//boost::split(rel_path_split, rel_path, boost::is_any_of("/"));
			vector<string> cwd_split = split_on_forward_slash(cwd);
			//cout << cwd_split.size() << "\n";
			//boost::split(cwd_split, cwd, boost::is_any_of("/"));
			size_t n_levels = cwd_split.size();
			string absolute_path = "";
			if ( (n_levels - n_deindent) >= 1 ) {
				for( size_t i = 1; i < (n_levels - n_deindent); i++ ) {
					absolute_path += "/" + cwd_split.at(i);
				}
				for( size_t j = n_deindent; j < rel_path_split.size(); j++ ) {
					absolute_path += "/" + rel_path_split.at(j);
				}
				return absolute_path;
			}
			cerr << "Argument rel_path is too strongly intended over argument cwd !" << "\n";
			return "";
		}
		return rel_path;
	}
	return cwd;
}


string absolute_to_relative_path(const string cwd_path, const string abs_path)
{
	//cwd has to be an absolute path!
	if ( is_absolute_path( cwd_path ) != 1 ) {
		cerr << "Argument " << cwd_path << " is not an absolute path!" << "\n";
		return "";
	}
	if ( is_absolute_path( abs_path ) != 1 ) {
		cerr << "Argument " << abs_path << " is not an absolute path!" << "\n";
		return "";
	}
	string clean_cwd_path = remove_deindents(cwd_path);
	string clean_abs_path = remove_deindents(abs_path);
	//cout << "cwd_path: " << cwd_path << "\n";
	//cout << "clean_cwd_path: " << "\n";
	vector<string> cwd_path_split = split_on_forward_slash(clean_cwd_path);
	//boost::split(cwd_path_split, clean_cwd_path, boost::is_any_of("/"));
	//for ( auto it = cwd_path_split.begin(); it != cwd_path_split.end(); it++ ) {
	//	cout << "__" << *it << "__" << "\n";
	//}
	//cout << "abs_path: " << abs_path << "\n";
	//cout << "clean_abs_path: " << clean_abs_path << "\n";
 	vector<string> abs_path_split = split_on_forward_slash(clean_abs_path);
	//boost::split(abs_path_split, clean_abs_path, boost::is_any_of("/"));
	//for ( auto it = abs_path_split.begin(); it != abs_path_split.end(); it++ ) {
	//	cout << "__" << *it << "__" << "\n";
	//}

	//given that both paths are absolute three cases, walk along cwd
	string retval = "";
	if ( cwd_path_split.size() < abs_path_split.size() ) {
		size_t idx = 0;
		for(  ; idx < cwd_path_split.size(); idx++ ) {
			if ( cwd_path_split.at(idx).compare(abs_path_split.at(idx)) == 0 ) {
				continue;
			}
			else {
				return "";
			}
		}
		for( size_t i = idx; i < abs_path_split.size(); i++ ) {
			if ( i == idx) {
				retval += abs_path_split.at(i);
			}
			else {
				retval += "/" + abs_path_split.at(i);
			}
		}
		return retval;
	}
	else { //cwd_path_split.size() >= abs_path_split.size()
		size_t idx = 0;
		for(  ; idx < cwd_path_split.size(); idx++ ) {
			if ( cwd_path_split.at(idx).compare(abs_path_split.at(idx)) == 0 ) {
				continue;
			}
			else {
				return "";
			}
		}
		size_t n_deintend = 1 + cwd_path_split.size() - abs_path_split.size();
		for( size_t j = 0; j < n_deintend; j++ ) {
			retval += "../";
		}
		retval += abs_path_split.back();
		return retval;
	}
	return "";
}


string path_handling( const string str )
{
	//removes preceeding components of a file path as all files in paraprobe-toolbox are always expected in the current working directory
	/*
	size_t idx = str.find_last_of("/");
	if ( idx != string::npos )
		return str.substr(idx+1);
	return str;
	*/
	string cwd = get_current_working_directory();
	cout << "cwd: __" << cwd << "__" << "\n";
	string tmp_path = cwd + "/" + str;
	cout << tmp_path << "\n";
	tmp_path = replace_repeating_forward_slashes( tmp_path );
	cout << tmp_path << "\n";
	tmp_path = remove_deindents( tmp_path );
	cout << "abs_path: __" << tmp_path << "__" << "\n";
	return tmp_path;
}


void paraprobe_debug_file_path_handling()
{
	string win_path = "\\home\\markus\\";
	cout << "win_path: " << win_path << "\n";
	string linux_path = win_to_linux_path(win_path);
	cout << "linux_path: " << linux_path << "\n";
	string test_cwd = "/home/mkuehbach/tools/src";
	cout << "cwd: " << test_cwd << "\n";
	string test_junk = "/home/mkuehbach//////////////../../tools/../src/c.cpp";
	cout << "test_junk: " << test_junk << "\n";
	cout << replace_repeating_forward_slashes(test_junk) << "\n";
	test_junk = replace_repeating_forward_slashes(test_junk);
	size_t cnt = count_sub_string( test_junk, "../" );
	cout << "cnt: " << cnt << "\n";
	string rel_path = "../home/mkuehbach/../../c.cpp";
	rel_path = "../c.cpp";
	cout << "rel_path: " << rel_path << "\n";
	string abs_path = relative_to_absolute_path(test_cwd, rel_path);
	cout << "abs_path: " << abs_path << "\n";
	//rel_path = "mkuehbach/tools/src/c.cpp"
	//rel_path = "../c.cpp"
	string clean = remove_deindents( test_junk );
	cout << "clean: " << clean << "\n";
	string abs_to_rel = "";
	test_junk = "/a/b/c";
	test_cwd = "/a/b/c/c.cpp";
	abs_to_rel = absolute_to_relative_path(test_junk, test_cwd);
	cout << "abs_to_rel: " << abs_to_rel << "\n";
	test_junk = "/a/b/c";
	test_cwd = "/a/b/c.cpp";
	abs_to_rel = absolute_to_relative_path(test_junk, test_cwd);
	cout << "abs_to_rel: " << abs_to_rel << "\n";
	test_junk = "/a/b/c";
	test_cwd = "/a/c.cpp";
	abs_to_rel = absolute_to_relative_path(test_junk, test_cwd);
	cout << "abs_to_rel: " << abs_to_rel << "\n";
}



