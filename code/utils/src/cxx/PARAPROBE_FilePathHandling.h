/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_FILEPATHHANDLING_H__
#define __PARAPROBE_UTILS_FILEPATHHANDLING_H__

#include "PARAPROBE_BoostInterface.h"


/*
string cwd = "/home/mkuehbach/tools/src"
		print(f"cwd: {cwd}")
		abs_fnm = squeeze("/", "/home/mkuehbach//////////////../../tools/../src/c.cpp")
		print(f"abs_fnm: {abs_fnm}")
		rel_fnm = "../home/mkuehbach/../../c.cpp"
		rel_fnm = "mkuehbach/tools/src/c.cpp"
		rel_fnm = "../c.cpp"
		print(f"rel_fnm: {rel_fnm}")
*/


string win_to_linux_path( const string & str );


int is_absolute_path( const string & path );


string get_current_working_directory();


string replace_repeating_forward_slashes( const string & str );


size_t count_sub_string( const string & str, const string & sub );


string strip_path_prefix( const string path );


vector<string> split_on_forward_slash( const string path );


string remove_deindents( const string & path );


string relative_to_absolute_path( const string cwd, const string rel_path );


string absolute_to_relative_path( const string cwd_path, const string abs_path );


string path_handling( const string str );


void paraprobe_debug_file_path_handling();

#endif
