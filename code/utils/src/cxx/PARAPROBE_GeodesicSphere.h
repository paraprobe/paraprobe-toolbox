//##MK::GPLV3

#ifndef __PARAPROBE_COORDINATE_SYSTEM_H__
#define __PARAPROBE_COORDINATE_SYSTEM_H__

#include "PARAPROBE_CiteMe.h"

//MK::coordinate system is a Cartesian right-handed x,y,z in 3D Euclidean space
#define PARAPROBE_XAXIS					0
#define PARAPROBE_YAXIS					1
#define PARAPROBE_ZAXIS					2


//##MK::geodesic spheres

struct elev_azim
{
	apt_real elevation;
	apt_real azimuth;
	elev_azim() : elevation(MYZERO), azimuth(MYZERO) {}
	elev_azim( const apt_real _elev, const apt_real _azim ) :
		elevation(_elev), azimuth(_azim) {}
};

ostream& operator<<(ostream& in, elev_azim const & val);


class GeodesicSpheres
{
public:
	static vector<elev_azim> Kuehbach40962;
};


#endif
