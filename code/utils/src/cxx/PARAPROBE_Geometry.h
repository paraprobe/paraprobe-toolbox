/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_GEOMETRY_H__
#define __PARAPROBE_UTILS_GEOMETRY_H__

#include "PARAPROBE_PrimsDiscrete.h"


//paraprobe-toolbox can assign triangles from a mesh or soup a patch_identifier/patch_id to
//specify to which patch the triangle is associated
//patch identifier have to start at 1, identifier 0 is reserved for triangles without an associated patch

#define UNDEFINED_PATCHID		0

apt_real closestPointOnTriangle( const tri3d face, const p3d src );


apt_sphere circumsphere_about_triangle( const tri3d tri );


apt_real Normalize( p3d & v, bool robust = false);


apt_real Orthonormalize( vector<p3d> & v, bool robust = false);

//##MK::https://github.com/davideberly/GeometricTools/blob/master/GTE/Mathematics/Vector3.h
apt_real ComputeOrthogonalComplement( apt_int numInputs, vector<p3d> & v, bool robust = false);

//##MK::https://www.geometrictools.com/GTE/Mathematics/IntrTriangle3Cylinder3.h
bool DiskOverlapsPoint( p2d const & Q, apt_real const & radius);


bool DiskOverlapsSegment( p2d const & Q0, p2d const & Q1, apt_real const & radius);


bool DiskOverlapsPolygon( size_t numVertices, vector<p2d> const & Q, apt_real const & radius);


bool intersectTriangleCylinder( const tri3d face, const apt_cylinder cyl );


#endif
