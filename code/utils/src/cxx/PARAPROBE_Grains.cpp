/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_Grains.h"


ostream& operator<<(ostream& in, grain const & val)
{
	in << "grainid " << val.grainid << "\n";
	in << "natoms " << val.natoms << "\n";
	in << "crystalstructure" << "\n";
	in << val.structure << "\n";
	in << "orientation " << val.ori.q0 << ";" << val.ori.q1 << ";" << val.ori.q2 << ";" << val.ori.q3 << "\n";
	return in;
}


grainAggregate::grainAggregate()
{
}


grainAggregate::~grainAggregate()
{
}
