/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_Histogram2D.h"


histogram::histogram()
{
	cnts_lowest = 0.;
	cnts_highest = 0.;
	cnts_unexpected = 0.;
	bounds = lival<double>();
	tmp = 0.;
	nbins = 0;
}


histogram::histogram( lival<double> const & iv )
{
	cnts_lowest = 0.;
	cnts_highest = 0.;
	cnts_unexpected = 0.;
	bounds = lival<double>();
	tmp = 0.;
	nbins = 0;
	
	if ( iv.incr > EPSILON ) {
		size_t _nbins = (iv.max - iv.min) / iv.incr;
//cout << "Histogram construction [" << _start << ":" << _width << ":" << _end << "] " << _nbins << " bins" << endl;
		if ( _nbins > 0 && _nbins < UMX ) {
			bounds = iv;
			nbins = static_cast<apt_uint>(_nbins); //type cast from size_t to apt_uint safe
			try {
				cnts = vector<double>( nbins, 0. );
			}
			catch (bad_alloc &croak) {
				cerr << "Unable to allocate memory in histogram class object" << "\n";
				return;
			}
			tmp = 1. / (bounds.max - bounds.min) * static_cast<double>(nbins);
		}
		//else histogram cnts is empty
	}
}


histogram::~histogram()
{
	cnts = vector<double>();
}


void histogram::add_inside( const apt_real x, const apt_uint cnt )
{
	//increment cnts in specific bin at correct bin
	double xx = x;
	double mltply = cnt;
	if ( xx >= bounds.min && xx <= bounds.max ) {
		double where = (xx - bounds.min) * tmp;
		apt_uint b = (apt_uint) floor(where);
		if ( b < nbins )
			cnts[b] += mltply;
		else
			cnts_highest += mltply;
	}
	else {
		cnts_unexpected += mltply;
	}
}


void histogram::add_upper( const apt_uint cnt )
{
	double mltply = cnt;
	cnts_highest += mltply;
}


void histogram::add_dump_yes( const apt_real x )
{
	//increment cnts in specific bin at correct bin
	double xx = x;
	if ( xx >= bounds.min && xx <= bounds.max ) {
		double where = (xx - bounds.min) * tmp;
		apt_uint b = (apt_uint) floor(where);
		if ( b < nbins )
			cnts[b] += 1.;
		else
			cnts_highest += 1.;
	}
	else {
		if ( xx < bounds.min ) {
			cnts_lowest += 1.;
		}
		else if ( xx > bounds.max ) {
			cnts_highest += 1.;
		}
		else {
			cnts_unexpected += 1.;
		}
	}
}


void histogram::add_counts( histogram const & hst )
{
	//copy all counts from another histogram into an existent histogram
	//but only if the histograms cover the same range
	if ( this->cnts.size() == hst.cnts.size() && this->nbins == hst.nbins &&
		fabs(this->bounds.min - hst.bounds.min) < EPSILON &&
		fabs(this->bounds.incr - hst.bounds.incr) < EPSILON &&
		fabs(this->bounds.incr - hst.bounds.incr) < EPSILON &&
		fabs(this->tmp - hst.tmp) < EPSILON ) {

		this->cnts_lowest += hst.cnts_lowest;
		for( size_t b = 0; b < this->cnts.size(); b++ ) {
			this->cnts[b] += hst.cnts[b];
		}
		this->cnts_highest += hst.cnts_highest;
		this->cnts_unexpected += hst.cnts_unexpected;
	}
}


void histogram::normalize()
{
	double sum = 0.;
	for ( auto it = cnts.begin(); it != cnts.end(); it++ ) {
		sum = sum + *it;
	}
	
	if ( sum >= (1.0-EPSILON) ) { //at least one count
		for ( apt_uint b = 0; b < nbins; b++ ) {
			cnts[b] /= sum;
		}
	}
}


double histogram::report( const apt_uint b )
{
	if ( b < nbins ) {
		return cnts[b];
	}
	else {
		return 0.;
	}
}


double histogram::left( const apt_uint b )
{
	if ( b < nbins ) {
		return bounds.min + bounds.incr * static_cast<double>(b);
	}
	else {
		return F64MI;
	}
}
	
	
double histogram::center( const apt_uint b )
{
	if ( b < nbins ) {
		return bounds.min + bounds.incr * (static_cast<double>(b) + 0.5);
	}
	else {
		return 0.;
	}
}


double histogram::right( const apt_uint b )
{
	if ( b < nbins ) {
		return bounds.min + bounds.incr * static_cast<double>(b+1);
	}
	else {
		return F64MX;
	}
}


void histogram::report()
{
	cout << "Reporting status of histogram " << this << "\n";
	double total = MYZERO;
	cout << "low" << "\t\t" << cnts_lowest << "\n";
	total += cnts_lowest;
	for( size_t b = 0; b < cnts.size(); b++ ) {
		cout << this->right( b ) << "\t\t" << cnts[b] << "\n";
		total += cnts[b];
	}
	cout << "high" << "\t\t" << cnts_highest << "\n";
	total += cnts_highest;
	cout << "unexpected" << "\t\t" << cnts_unexpected << "\n";
	total += cnts_unexpected;
	cout << "total" << "\t\t" << setprecision(32) << total << "\n";
}
