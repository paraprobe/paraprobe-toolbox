/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_IonCloudMemory.h"


ionCloud::ionCloud()
{
	ionpp3 = vector<p3d>();
	ionmq = vector<apt_real>();
	ionifo = vector<p3dinfo>();
	aabb = aabb3d();
}


ionCloud::~ionCloud()
{
	ionpp3 = vector<p3d>();
	ionmq = vector<apt_real>();
	ionifo = vector<p3dinfo>();
	aabb = aabb3d();
}


void ionCloud::get_aabb()
{
	aabb = aabb3d();
	#pragma omp parallel shared(aabb)
	{
		aabb3d myaabb = aabb3d();
		#pragma omp for schedule(dynamic,1)
		for( size_t i = 0; i < ionpp3.size(); i++ ) {
			myaabb.possibly_enlarge_me( ionpp3[i] );
		}
		#pragma omp barrier
		#pragma omp critical
		{
			aabb.possibly_enlarge_me( myaabb );
		}
	}
}
