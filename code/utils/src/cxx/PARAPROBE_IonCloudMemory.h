/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_IONCLOUDMEMORY_H__
#define __PARAPROBE_UTILS_IONCLOUDMEMORY_H__

#include "PARAPROBE_KDTree.h"


class ionCloud
{
public:
	ionCloud();
	~ionCloud();

	void get_aabb();

	//the 3D point cloud of all ions in the dataset, global ion IDs are implicit
	vector<p3d> ionpp3;

	//the mass-to-charge-state ratios of all ions, stored separately because after map on iontypes no frequent interaction with this array
	vector<apt_real> ionmq;

	//implicitly resolved properties of the ions
	vector<p3dinfo> ionifo;

	aabb3d aabb;
};


#endif
