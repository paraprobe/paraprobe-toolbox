/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_KDTree.h"


template <typename T, typename U>
inline apt_real euclidean_sqrd( const T & a, const U & b )
{
	return SQR(a.x - b.x) + SQR(a.y - b.y) + SQR(a.z - b.z);
}

template apt_real euclidean_sqrd<p3d, p3d>( const p3d & a, const p3d & b );
template apt_real euclidean_sqrd<p3d, p3dm1>( const p3d & a, const p3dm1 & b );
template apt_real euclidean_sqrd<p3d, p3dmidx>( const p3d & a, const p3dmidx & b );
template apt_real euclidean_sqrd<p3d, p3dm5>( const p3d & a, const p3dm5 & b );

template apt_real euclidean_sqrd<p3dm1, p3d>( const p3dm1 & a, const p3d & b );
template apt_real euclidean_sqrd<p3dm1, p3dm1>( const p3dm1 & a, const p3dm1 & b );
template apt_real euclidean_sqrd<p3dm1, p3dmidx>( const p3dm1 & a, const p3dmidx & b );
template apt_real euclidean_sqrd<p3dm1, p3dm5>( const p3dm1 & a, const p3dm5 & b );

template apt_real euclidean_sqrd<p3dmidx, p3d>( const p3dmidx & a, const p3d & b );
template apt_real euclidean_sqrd<p3dmidx, p3dm1>( const p3dmidx & a, const p3dm1 & b );
template apt_real euclidean_sqrd<p3dmidx, p3dmidx>( const p3dmidx & a, const p3dmidx & b );
template apt_real euclidean_sqrd<p3dmidx, p3dm5>( const p3dmidx & a, const p3dm5 & b );

template apt_real euclidean_sqrd<p3dm5, p3d>( const p3dm5 & a, const p3d & b );
template apt_real euclidean_sqrd<p3dm5, p3dm1>( const p3dm5 & a, const p3dm1 & b );
template apt_real euclidean_sqrd<p3dm5, p3dmidx>( const p3dm5 & a, const p3dmidx & b );
template apt_real euclidean_sqrd<p3dm5, p3dm5>( const p3dm5 & a, const p3dm5 & b );


/*template<typename T> <T>*/
kd_cuboid::kd_cuboid()
{
	min = p3d(RMX, RMX, RMX);
	max = p3d(RMI, RMI, RMI);
};

/*
template kd_cuboid<p3d>::kd_cuboid();
template kd_cuboid<p3dm1>::kd_cuboid();
template kd_cuboid<p3dmidx>::kd_cuboid();
template kd_cuboid<p3dm5>::kd_cuboid();
*/

/*template<typename T> <T>*/
kd_cuboid::kd_cuboid( const p3d & p1, const p3d & p2 )
{
	min = p1;
	max = p2;
};

/*
template kd_cuboid<p3d>::kd_cuboid( const p3d & p1, const p3d & p2 );
template kd_cuboid<p3dm1>::kd_cuboid( const p3d & p1, const p3d & p2 );
template kd_cuboid<p3dmidx>::kd_cuboid( const p3d & p1, const p3d & p2 );
template kd_cuboid<p3dm5>::kd_cuboid( const p3d & p1, const p3d & p2 );
*/

template<typename T>
apt_real kd_cuboid::outside_proximity( const T & p )
{
	apt_real res = MYZERO; //assuming we are inside
	if ( p.x < this->min.x )
		res += SQR(this->min.x - p.x);
	else if (p.x > this->max.x )
		res += SQR(p.x - this->max.x);
	if ( p.y < this->min.y )
		res += SQR(this->min.y - p.y);
	else if (p.y > this->max.y )
		res += SQR(p.y - this->max.y);
	if ( p.z < this->min.z )
		res += SQR(this->min.z - p.z);
	else if (p.z > this->max.z )
		res += SQR(p.z - this->max.z);
	return res;
};

template apt_real kd_cuboid::outside_proximity<p3d>( const p3d & p );
template apt_real kd_cuboid::outside_proximity<p3dm1>( const p3dm1 & p );
template apt_real kd_cuboid::outside_proximity<p3dmidx>( const p3dmidx & p );
template apt_real kd_cuboid::outside_proximity<p3dm5>( const p3dm5 & p );


/*template <typename T> <T>*/
kd_traverse_task::kd_traverse_task()
{
	bx = kd_cuboid();
	node = UMX;
	dim = 0;
	d = MYZERO;
};

/*
template kd_traverse_task<p3d>::kd_traverse_task();
template kd_traverse_task<p3dm1>::kd_traverse_task();
template kd_traverse_task<p3dmidx>::kd_traverse_task();
template kd_traverse_task<p3dm5>::kd_traverse_task();
*/

/*template <typename T> <T>*/
kd_traverse_task::kd_traverse_task( kd_cuboid _bx, apt_uint _node, apt_uint _dim, apt_real _d )
{
	bx = _bx;
	node = _node;
	dim = _dim;
	d = _d;
}

/*
template kd_traverse_task<p3d>::kd_traverse_task( kd_cuboid<p3d> _bx, apt_uint _node, apt_uint _dim, apt_real _d );
template kd_traverse_task<p3dm1>::kd_traverse_task( kd_cuboid<p3dm1> _bx, apt_uint _node, apt_uint _dim, apt_real _d );
template kd_traverse_task<p3dmidx>::kd_traverse_task( kd_cuboid<p3dmidx> _bx, apt_uint _node, apt_uint _dim, apt_real _d );
template kd_traverse_task<p3dm5>::kd_traverse_task( kd_cuboid<p3dm5> _bx, apt_uint _node, apt_uint _dim, apt_real _d );
*/

/*template<typename T> <T>*/
kd_traverse_task::~kd_traverse_task(){};

/*
template kd_traverse_task<p3d>::~kd_traverse_task();
template kd_traverse_task<p3dm1>::~kd_traverse_task();
template kd_traverse_task<p3dmidx>::~kd_traverse_task();
template kd_traverse_task<p3dm5>::~kd_traverse_task();
*/


/*
template<typename T>
kd_scan_task<T>::kd_scan_task()
{
	bx = kd_cuboid<T>();
	node = UMX;
	dim = 0;
};

template kd_scan_task<p3d>::kd_scan_task();
template kd_scan_task<p3dm1>::kd_scan_task();
template kd_scan_task<p3dmidx>::kd_scan_task();
template kd_scan_task<p3dm5>::kd_scan_task();


template<typename T>
kd_scan_task<T>::~kd_scan_task(){};

template kd_scan_task<p3d>::~kd_scan_task();
template kd_scan_task<p3dm1>::~kd_scan_task();
template kd_scan_task<p3dmidx>::~kd_scan_task();
template kd_scan_task<p3dm5>::~kd_scan_task();
*/


template<typename T>
inline void kd_expand( p3d & min, p3d & max, const T & p )
{
	if (p.x < min.x)
		min.x = p.x;
	if (p.x > max.x)
		max.x = p.x;
	if (p.y	< min.y)
		min.y = p.y;
	if (p.y > max.y)
		max.y = p.y;
	if (p.z	< min.z)
		min.z = p.z;
	if (p.z > max.z)
		max.z = p.z;
}

template void kd_expand<p3d>( p3d & min, p3d & max, const p3d & p );
template void kd_expand<p3dm1>( p3d & min, p3d & max, const p3dm1 & p );
template void kd_expand<p3dmidx>( p3d & min, p3d & max, const p3dmidx & p );
template void kd_expand<p3dm5>( p3d & min, p3d & max, const p3dm5 & p );

/*
template<typename T>
inline void kd_expand( p3d & min, p3d & max, const T & p );
*/

template <typename T>
kd_lower_x<T>::kd_lower_x( const vector<T> & p ) : points(p) {};

template kd_lower_x<p3d>::kd_lower_x( const vector<p3d> & p );
template kd_lower_x<p3dm1>::kd_lower_x( const vector<p3dm1> & p );
template kd_lower_x<p3dmidx>::kd_lower_x( const vector<p3dmidx> & p );
template kd_lower_x<p3dm5>::kd_lower_x( const vector<p3dm5> & p );


template <typename T>
bool kd_lower_x<T>::operator()( size_t i1, size_t i2 ) const
{
	return points[i1].x < points[i2].x;
};

template bool kd_lower_x<p3d>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_x<p3dm1>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_x<p3dmidx>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_x<p3dm5>::operator()( size_t i1, size_t i2 ) const;


template <typename T>
kd_lower_y<T>::kd_lower_y( const vector<T> & p ) : points(p) {};

template kd_lower_y<p3d>::kd_lower_y( const vector<p3d> & p );
template kd_lower_y<p3dm1>::kd_lower_y( const vector<p3dm1> & p );
template kd_lower_y<p3dmidx>::kd_lower_y( const vector<p3dmidx> & p );
template kd_lower_y<p3dm5>::kd_lower_y( const vector<p3dm5> & p );


template <typename T>
bool kd_lower_y<T>::operator()( size_t i1, size_t i2 ) const
{
	return points[i1].x < points[i2].x;
};

template bool kd_lower_y<p3d>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_y<p3dm1>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_y<p3dmidx>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_y<p3dm5>::operator()( size_t i1, size_t i2 ) const;


template <typename T>
kd_lower_z<T>::kd_lower_z( const vector<T> & p ) : points(p) {};

template kd_lower_z<p3d>::kd_lower_z( const vector<p3d> & p );
template kd_lower_z<p3dm1>::kd_lower_z( const vector<p3dm1> & p );
template kd_lower_z<p3dmidx>::kd_lower_z( const vector<p3dmidx> & p );
template kd_lower_z<p3dm5>::kd_lower_z( const vector<p3dm5> & p );


template <typename T>
bool kd_lower_z<T>::operator()( size_t i1, size_t i2 ) const
{
	return points[i1].x < points[i2].x;
};

template bool kd_lower_z<p3d>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_z<p3dm1>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_z<p3dmidx>::operator()( size_t i1, size_t i2 ) const;
template bool kd_lower_z<p3dm5>::operator()( size_t i1, size_t i2 ) const;


template <typename T>
kdTree<T>::kdTree()
{
	domain = aabb3d();
	nodes = vector<kd_node>();
	points = vector<T>();
	m1 = UMX;
};

template kdTree<p3d>::kdTree();
template kdTree<p3dm1>::kdTree();
template kdTree<p3dmidx>::kdTree();
template kdTree<p3dm5>::kdTree();


template <typename T>
kdTree<T>::~kdTree()
{
	domain = aabb3d();
	nodes = vector<kd_node>();
	points = vector<T>();
	m1 = UMX;
};

template kdTree<p3d>::~kdTree();
template kdTree<p3dm1>::~kdTree();
template kdTree<p3dmidx>::~kdTree();
template kdTree<p3dm5>::~kdTree();


template<typename T>
void kdTree<T>::kd_build(
		vector<T> const & pts,
		vector<apt_uint> & prms,
		const apt_uint leaf_size,
		const apt_uint mark )
{
	if ( pts.size() < static_cast<size_t>(UMX) ) {
		const apt_uint stack_size = (apt_uint) ConfigShared::KDTreeStackSize;
		const apt_uint count = (apt_uint) pts.size();
		if ( count > 0 ) {
			prms = vector<apt_uint>();
			prms.reserve(count);
			for ( apt_uint i = 0; i < count; i++ ) {
				prms.push_back(i);
			}

			domain.xmi = RMX; domain.ymi = RMX; domain.zmi = RMX;
			domain.xmx = RMI; domain.ymx = RMI; domain.zmx = RMI;
			nodes.push_back( kd_node() );
			vector<kd_build_task> tasks = vector<kd_build_task>(
					stack_size, kd_build_task(0, count-1, 0, 0) );
			//accessing on i0, i1, [first, last], node id, split dim
			int current_task = 0;
			m1 = mark;

			do
			{
				kd_build_task task = tasks[current_task];
				kd_node &n = nodes[task.node];
				if ( (task.last - task.first) < leaf_size ) {
					//leaf_threshold, a window of the permutate field that is sorted [task.first <= task.last]
					//n.split MUST NOT BE SET! as it indicates that this node specifies now a leaf, and therefore i0 and i1 reference positions on permutation field rather on this->nodes !
					//##MK::DEBUG likewise we leave n.dimdebug -1
					//likewise n.splitpos needs to be numeric_limits<apt_real>::max()
					assert ( n.splitpos == numeric_limits<apt_real>::max() );

					//i.e. a sorted contiguous region on the permutation index field whose indices specify the points in this leaf
					//in case that a node is a leaf ONLY, i0 and i1 specify task.first and task.last as markers where we read indices from the permutation field index inclusively
					n.i0 = task.first; //MK::now transforming ID index i0 from indexing nodes to points
					n.i1 = task.last;
					//n.i0 and n.i1 can be the same namely if only single in the leaf
					for( apt_uint k = task.first; k <= task.last; ++k ) {
						domain.possibly_enlarge_me( pts[prms[k]] );
					}

					//##MK::assert(is_leaf(n));
					--current_task;
					continue;
				}

				//no leaf yet so split
				const apt_uint k = (task.first + task.last) / 2;

				if ( task.dim == PARAPROBE_XAXIS) {
					//cout << "xaxis, points.size() " << pts.size() << " permutate.size() " << prms.size() << " task.first/tassk.last " << task.first << ", " << task.last << " k " << k << "\n";
					nth_element( prms.begin() + task.first, prms.begin() + k, prms.begin() + task.last + 1, kd_lower_x<T>(pts) );
					n.splitpos = pts[prms[k]].x;
				}
				else if ( task.dim == PARAPROBE_YAXIS ) {
					//cout << "yaxis, points.size() " << pts.size() << " permutate.size() " << prms.size() << " task.first/tassk.last " << task.first << ", " << task.last << " k " << k << "\n";
					nth_element( prms.begin() + task.first, prms.begin() + k, prms.begin() + task.last + 1, kd_lower_y<T>(pts) );
					n.splitpos = pts[prms[k]].y;
				}
				else { //task.dim == PARAPROBE_ZAXIS
					//cout << "zaxis, points.size() " << pts.size() << " permutate.size() " << prms.size() << " task.first/tassk.last " << task.first << ", " << task.last << " k " << k << "\n";
					nth_element( prms.begin() + task.first, prms.begin() + k, prms.begin() + task.last + 1, kd_lower_z<T>(pts) );
					n.splitpos = pts[prms[k]].z;
				}

				const apt_uint i0 = (apt_uint) nodes.size(); //MK::i0 and i1 here indexing nodes, not points!, cannot have more nodes than points
				const apt_uint i1 = i0 + 1;

				n.split = k;
				n.i0 = i0;
				n.i1 = i1;

				apt_uint next_dim = ((task.dim+1) > PARAPROBE_ZAXIS) ? PARAPROBE_XAXIS : (task.dim + 1); //task.dim++; //cyclic X,Y,Z, X, Y, ...

				const kd_build_task taskleft = kd_build_task( task.first, k, i0, next_dim );
				const kd_build_task taskright = kd_build_task( k + 1, task.last, i1, next_dim );
				tasks[current_task] = taskleft;
				nodes.push_back( kd_node() );
				++current_task;

				assert( (apt_uint) current_task < stack_size );

				tasks[current_task] = taskright;

				//do not add ++current_task because we replace the existent on the stack on which we still work "so two new for an old"
				nodes.push_back( kd_node() );
			}
			while ( current_task != -1 );
		}
	}
	else {
		cerr << "pts.size() is longer than the maximum length !" << "\n";
	}
}

template void kdTree<p3d>::kd_build(
	vector<p3d> const & pts, vector<apt_uint> & prms, const apt_uint leaf_size, const apt_uint mark );
template void kdTree<p3dm1>::kd_build(
	vector<p3dm1> const & pts, vector<apt_uint> & prms, const apt_uint leaf_size, const apt_uint mark );
template void kdTree<p3dmidx>::kd_build(
	vector<p3dmidx> const & pts, vector<apt_uint> & prms, const apt_uint leaf_size, const apt_uint mark );
template void kdTree<p3dm5>::kd_build(
	vector<p3dm5> const & pts, vector<apt_uint> & prms, const apt_uint leaf_size, const apt_uint mark );


template <typename T>
void kdTree<T>::kd_pack(
	vector<T> const & pts, vector<apt_uint> const & prms )
{
	const size_t count = prms.size();
	if ( count > 0 ) {
		points = vector<T>();
		points.reserve( count );
		for( size_t i = 0; i < count; i++ ) {
			//MK::concentrate the usually random memory accesses here, instead of when the tree is used
			points.push_back( pts[prms[i]] );
		}
	}
}

template void kdTree<p3d>::kd_pack(
	vector<p3d> const & pts, vector<apt_uint> const & prms );
template void kdTree<p3dm1>::kd_pack(
	vector<p3dm1> const & pts, vector<apt_uint> const & prms );
template void kdTree<p3dmidx>::kd_pack(
	vector<p3dmidx> const & pts, vector<apt_uint> const & prms );
template void kdTree<p3dm5>::kd_pack(
	vector<p3dm5> const & pts, vector<apt_uint> const & prms );


template <typename T>
bool kdTree<T>::kd_verify()
{
	if ( points.size() > 0 ) {
		size_t nvisited = 0;
		vector<unsigned char> visited = vector<unsigned char>( points.size(), 0x00 );
		for( size_t i = 0; i < nodes.size(); ++i) { //check that point references for each node do not overlap!
			kd_node &n = nodes[i];
			if( is_leaf(n) == true ) {
				for( size_t k = n.i0; k <= n.i1; ++k ) { //n.i0 and n.i1 are inclusive!
					if( visited[k] == 0x00 ) {
						visited[k] = 0x01;
						++nvisited;
					}
					else {
						return false;
					}
				}
			}
		}
		if ( nvisited != points.size() ) {
			cerr << "kdTree was verified, it turned out that not every point was assigned to a node !" << "\n";
			return false;
		}
	}
	cout << "kdTree was verified, it turned out that every point was assigned to a node !" << "\n";
	return true;
}

template bool kdTree<p3d>::kd_verify();
template bool kdTree<p3dm1>::kd_verify();
template bool kdTree<p3dmidx>::kd_verify();
template bool kdTree<p3dm5>::kd_verify();


template <typename T>
void kdTree<T>::kd_query_exclude_target(
	const T target, const apt_real sqrR, vector<apt_real> & cand )
{
	//assume that cand is already properly prepared
	if ( points.size() > 0 ) {
		//MK::performs a spatial range query with a KDTree for which the target point is not in the sortedpoints
		kd_cuboid aabb = kd_cuboid();
		aabb.min = p3d( domain.xmi, domain.ymi, domain.zmi );
		aabb.max = p3d( domain.xmx, domain.ymx, domain.zmx );

		const apt_uint stack_size = (apt_uint) ConfigShared::KDTreeStackSize;
		vector<kd_traverse_task> tasks = vector<kd_traverse_task>(
				stack_size, kd_traverse_task( aabb, 0, 0, aabb.outside_proximity(target) ) );
		//first box, first node, dim, distance to first
		int current_task = 0; //##MK::for a very deep stack int may overflow

		do
		{
			const kd_traverse_task &task = tasks[current_task];
			if ( task.d > sqrR ) {
				--current_task;
				continue;
			}

			const kd_node &n = nodes[task.node];
			if ( is_leaf(n) == true ) {
				//if n is a node n.i0 and n.i1 specify contiguous range of points on SORTED!! sortedpoints
				for( apt_uint i = n.i0; i <= n.i1; ++i ) {
					apt_real sqrD = SQR(target.x - points[i].x) + SQR(target.y - points[i].y) + SQR(target.z - points[i].z);
					if ( sqrD > sqrR ) {
						continue;
					}
					else { //can not be myself
						if constexpr(std::is_same<T, p3d>::value) {
							if ( sqrD >= SQR(EPSILON) ) { //myself is numerically almost zero
								cand.push_back( sqrD );
							}
						}
						else if constexpr(std::is_same<T, p3dm1>::value) {
							if ( sqrD >= SQR(EPSILON) ) {
								cand.push_back( sqrD );
							}
						}
						else if constexpr(std::is_same<T, p3dmidx>::value) {
							if ( target.idx != points[i].idx ) {
								cand.push_back( sqrD );
							}
						}
						else if constexpr(std::is_same<T, p3dm5>::value) {
							if ( target.idx != points[i].idx ) {
								cand.push_back( sqrD );
							}
						}
						else {
							//nop
						}
					}
				}
				--current_task;
				continue;
			}

			kd_traverse_task near = kd_traverse_task();
			kd_traverse_task far = kd_traverse_task();
			//assert( n.dimdebug == task.dim );
			if ( task.dim == PARAPROBE_XAXIS ) {
				apt_real splitposx = n.splitpos; //MK::using node-local piece of information rather than probing random memory position
				kd_cuboid left = kd_cuboid( task.bx.min, p3d(splitposx, task.bx.max.y, task.bx.max.z) );
				kd_cuboid right = kd_cuboid( p3d(splitposx, task.bx.min.y, task.bx.min.z), task.bx.max );
				if ( target.x <= splitposx ) {
					near.bx = left;
					near.node = n.i0;
					far.bx = right;
					far.node = n.i1;
				}
				else {
					near.bx = right;
					near.node = n.i1;
					far.bx = left;
					far.node = n.i0;
				}
			}
			else if ( task.dim == PARAPROBE_YAXIS ) {
				apt_real splitposy = n.splitpos;
				kd_cuboid lower = kd_cuboid( task.bx.min, p3d(task.bx.max.x, splitposy, task.bx.max.z) );
				kd_cuboid upper = kd_cuboid( p3d(task.bx.min.x, splitposy, task.bx.min.z), task.bx.max );
				if ( target.y <= splitposy ) {
					near.bx = lower;
					near.node = n.i0;
					far.bx = upper;
					far.node = n.i1;
				}
				else {
					near.bx = upper;
					near.node = n.i1;
					far.bx = lower;
					far.node = n.i0;
				}
			}
			else {
				apt_real splitposz = n.splitpos;
				kd_cuboid back = kd_cuboid( task.bx.min, p3d(task.bx.max.x, task.bx.max.y, splitposz) );
				kd_cuboid front = kd_cuboid( p3d(task.bx.min.x, task.bx.min.y, splitposz), task.bx.max );
				if ( target.z <= splitposz ) {
					near.bx = back;
					near.node = n.i0;
					far.bx = front;
					far.node = n.i1;
				}
				else {
					near.bx = front;
					near.node = n.i1;
					far.bx = back;
					far.node = n.i0;
				}
			}

			apt_uint next_dim = ((task.dim + 1) > PARAPROBE_ZAXIS) ? PARAPROBE_XAXIS : (task.dim + 1); //task.dim++;

			//assert( near.bx.outside_proximity(target) == MYZERO ); //##MK:::DEBUG
			near.d = 0.; //##MK:: should be not significant so set to ... ##### 0.;
			near.dim = next_dim;

			far.d = far.bx.outside_proximity(target);
			far.dim = next_dim;

			tasks[current_task] = far; //MK::near must be processed before far!
			++current_task;

			assert( (apt_uint) current_task < stack_size );
			tasks[current_task] = near;
		}
		while ( current_task != -1 );
	}
	//return retval;
}

template void kdTree<p3d>::kd_query_exclude_target(
	const p3d target, const apt_real sqrR, vector<apt_real> & cand );
template void kdTree<p3dm1>::kd_query_exclude_target(
	const p3dm1 target, const apt_real sqrR, vector<apt_real> & cand );
template void kdTree<p3dmidx>::kd_query_exclude_target(
	const p3dmidx target, const apt_real sqrR, vector<apt_real> & cand );
template void kdTree<p3dm5>::kd_query_exclude_target(
	const p3dm5 target, const apt_real sqrR, vector<apt_real> & cand );


template <typename T>
void kdTree<T>::kd_query_exclude_target(
	const T target, const apt_real sqrR, vector<sqrd_mltply> & cand )
{
	//assume that cand is already properly prepared
	if ( points.size() > 0 ) {
		//MK::performs a spatial range query with a KDTree for which the target point is not in the sortedpoints
		kd_cuboid aabb = kd_cuboid();
		aabb.min = p3d( domain.xmi, domain.ymi, domain.zmi );
		aabb.max = p3d( domain.xmx, domain.ymx, domain.zmx );

		const apt_uint stack_size = (apt_uint) ConfigShared::KDTreeStackSize;
		vector<kd_traverse_task> tasks = vector<kd_traverse_task>(
				stack_size, kd_traverse_task( aabb, 0, 0, aabb.outside_proximity(target) ) );
		//first box, first node, dim, distance to first
		int current_task = 0; //##MK::for a very deep stack int may overflow

		do
		{
			const kd_traverse_task &task = tasks[current_task];
			if ( task.d > sqrR ) {
				--current_task;
				continue;
			}

			const kd_node &n = nodes[task.node];
			if ( is_leaf(n) == true ) {
				//if n is a node n.i0 and n.i1 specify contiguous range of points on SORTED!! sortedpoints
				for( apt_uint i = n.i0; i <= n.i1; ++i ) {
					apt_real sqrD = SQR(target.x - points[i].x) + SQR(target.y - points[i].y) + SQR(target.z - points[i].z);
					if ( sqrD > sqrR ) {
						continue;
					}
					else { //can not be myself
						if constexpr(std::is_same<T, p3d>::value) {
							if ( sqrD >= SQR(EPSILON) ) { //myself is numerically almost zero
								cand.push_back( sqrd_mltply( sqrD, 0 ) );
							}
						}
						else if constexpr(std::is_same<T, p3dm1>::value) {
							if ( sqrD >= SQR(EPSILON) ) {
								cand.push_back( sqrd_mltply( sqrD, points[i].m ) );
							}
						}
						else if constexpr(std::is_same<T, p3dmidx>::value) {
							if ( target.idx != points[i].idx ) {
								cand.push_back( sqrd_mltply( sqrD, points[i].idx ) );
							}
						}
						else if constexpr(std::is_same<T, p3dm5>::value ) {
							if ( target.idx != points[i].idx ) {
								cand.push_back( sqrd_mltply( sqrD, (apt_uint) points[i].m2 ) );
							}
						}
						else {
							//nop
						}
					}
				}
				--current_task;
				continue;
			}

			kd_traverse_task near = kd_traverse_task();
			kd_traverse_task far = kd_traverse_task();
			//assert( n.dimdebug == task.dim );
			if ( task.dim == PARAPROBE_XAXIS ) {
				apt_real splitposx = n.splitpos; //MK::using node-local piece of information rather than probing random memory position
				kd_cuboid left = kd_cuboid( task.bx.min, p3d(splitposx, task.bx.max.y, task.bx.max.z) );
				kd_cuboid right = kd_cuboid( p3d(splitposx, task.bx.min.y, task.bx.min.z), task.bx.max );
				if ( target.x <= splitposx ) {
					near.bx = left;
					near.node = n.i0;
					far.bx = right;
					far.node = n.i1;
				}
				else {
					near.bx = right;
					near.node = n.i1;
					far.bx = left;
					far.node = n.i0;
				}
			}
			else if ( task.dim == PARAPROBE_YAXIS ) {
				apt_real splitposy = n.splitpos;
				kd_cuboid lower = kd_cuboid( task.bx.min, p3d(task.bx.max.x, splitposy, task.bx.max.z) );
				kd_cuboid upper = kd_cuboid( p3d(task.bx.min.x, splitposy, task.bx.min.z), task.bx.max );
				if ( target.y <= splitposy ) {
					near.bx = lower;
					near.node = n.i0;
					far.bx = upper;
					far.node = n.i1;
				}
				else {
					near.bx = upper;
					near.node = n.i1;
					far.bx = lower;
					far.node = n.i0;
				}
			}
			else {
				apt_real splitposz = n.splitpos;
				kd_cuboid back = kd_cuboid( task.bx.min, p3d(task.bx.max.x, task.bx.max.y, splitposz) );
				kd_cuboid front = kd_cuboid( p3d(task.bx.min.x, task.bx.min.y, splitposz), task.bx.max );
				if ( target.z <= splitposz ) {
					near.bx = back;
					near.node = n.i0;
					far.bx = front;
					far.node = n.i1;
				}
				else {
					near.bx = front;
					near.node = n.i1;
					far.bx = back;
					far.node = n.i0;
				}
			}

			apt_uint next_dim = ((task.dim + 1) > PARAPROBE_ZAXIS) ? PARAPROBE_XAXIS : (task.dim + 1); //task.dim++;

			//assert( near.bx.outside_proximity(target) == MYZERO ); //##MK:::DEBUG
			near.d = 0.; //##MK:: should be not significant so set to ... ##### 0.;
			near.dim = next_dim;

			far.d = far.bx.outside_proximity(target);
			far.dim = next_dim;

			tasks[current_task] = far; //MK::near must be processed before far!
			++current_task;

			assert( (apt_uint) current_task < stack_size );
			tasks[current_task] = near;
		}
		while ( current_task != -1 );
	}
	//return retval;
}

template void kdTree<p3d>::kd_query_exclude_target(
	const p3d target, const apt_real sqrR, vector<sqrd_mltply> & cand );
template void kdTree<p3dm1>::kd_query_exclude_target(
	const p3dm1 target, const apt_real sqrR, vector<sqrd_mltply> & cand );
template void kdTree<p3dmidx>::kd_query_exclude_target(
	const p3dmidx target, const apt_real sqrR, vector<sqrd_mltply> & cand );
template void kdTree<p3dm5>::kd_query_exclude_target(
	const p3dm5 target, const apt_real sqrR, vector<sqrd_mltply> & cand );


template <typename T>
void kdTree<T>::kd_query_exclude_target(
	const T target, const apt_real sqrR, vector<p3d> & cand )
{
	//assume that cand is already properly prepared
	if ( points.size() > 0 ) {
		//MK::performs a spatial range query with a KDTree for which the target point is not in the sortedpoints
		kd_cuboid aabb = kd_cuboid();
		aabb.min = p3d( domain.xmi, domain.ymi, domain.zmi );
		aabb.max = p3d( domain.xmx, domain.ymx, domain.zmx );

		const apt_uint stack_size = (apt_uint) ConfigShared::KDTreeStackSize;
		vector<kd_traverse_task> tasks = vector<kd_traverse_task>(
				stack_size, kd_traverse_task( aabb, 0, 0, aabb.outside_proximity(target) ) );
		//first box, first node, dim, distance to first
		int current_task = 0; //##MK::for a very deep stack int may overflow

		do
		{
			const kd_traverse_task &task = tasks[current_task];
			if ( task.d > sqrR ) {
				--current_task;
				continue;
			}

			const kd_node &n = nodes[task.node];
			if ( is_leaf(n) == true ) {
				//if n is a node n.i0 and n.i1 specify contiguous range of points on SORTED!! sortedpoints
				for( apt_uint i = n.i0; i <= n.i1; ++i ) {
					apt_real sqrD = SQR(target.x - points[i].x) + SQR(target.y - points[i].y) + SQR(target.z - points[i].z);
					if ( sqrD > sqrR ) {
						continue;
					}
					else { //can not be myself
						if constexpr(std::is_same<T, p3d>::value) {
							if ( sqrD >= SQR(EPSILON) ) { //myself is numerically almost zero
								cand.push_back( p3d( points[i].x, points[i].y, points[i].z ) );
							}
						}
						else if constexpr(std::is_same<T, p3dm1>::value) {
							if ( sqrD >= SQR(EPSILON) ) {
								cand.push_back( p3d( points[i].x, points[i].y, points[i].z ) );
							}
						}
						else if constexpr(std::is_same<T, p3dmidx>::value) {
							if ( target.idx != points[i].idx ) {
								cand.push_back( p3d( points[i].x, points[i].y, points[i].z ) );
							}
						}
						else if constexpr(std::is_same<T, p3dm5>::value ) {
							if ( target.idx != points[i].idx ) {
								cand.push_back( p3d( points[i].x, points[i].y, points[i].z ) );
							}
						}
						else {
							//nop
						}
					}
				}
				--current_task;
				continue;
			}

			kd_traverse_task near = kd_traverse_task();
			kd_traverse_task far = kd_traverse_task();
			//assert( n.dimdebug == task.dim );
			if ( task.dim == PARAPROBE_XAXIS ) {
				apt_real splitposx = n.splitpos; //MK::using node-local piece of information rather than probing random memory position
				kd_cuboid left = kd_cuboid( task.bx.min, p3d(splitposx, task.bx.max.y, task.bx.max.z) );
				kd_cuboid right = kd_cuboid( p3d(splitposx, task.bx.min.y, task.bx.min.z), task.bx.max );
				if ( target.x <= splitposx ) {
					near.bx = left;
					near.node = n.i0;
					far.bx = right;
					far.node = n.i1;
				}
				else {
					near.bx = right;
					near.node = n.i1;
					far.bx = left;
					far.node = n.i0;
				}
			}
			else if ( task.dim == PARAPROBE_YAXIS ) {
				apt_real splitposy = n.splitpos;
				kd_cuboid lower = kd_cuboid( task.bx.min, p3d(task.bx.max.x, splitposy, task.bx.max.z) );
				kd_cuboid upper = kd_cuboid( p3d(task.bx.min.x, splitposy, task.bx.min.z), task.bx.max );
				if ( target.y <= splitposy ) {
					near.bx = lower;
					near.node = n.i0;
					far.bx = upper;
					far.node = n.i1;
				}
				else {
					near.bx = upper;
					near.node = n.i1;
					far.bx = lower;
					far.node = n.i0;
				}
			}
			else {
				apt_real splitposz = n.splitpos;
				kd_cuboid back = kd_cuboid( task.bx.min, p3d(task.bx.max.x, task.bx.max.y, splitposz) );
				kd_cuboid front = kd_cuboid( p3d(task.bx.min.x, task.bx.min.y, splitposz), task.bx.max );
				if ( target.z <= splitposz ) {
					near.bx = back;
					near.node = n.i0;
					far.bx = front;
					far.node = n.i1;
				}
				else {
					near.bx = front;
					near.node = n.i1;
					far.bx = back;
					far.node = n.i0;
				}
			}

			apt_uint next_dim = ((task.dim + 1) > PARAPROBE_ZAXIS) ? PARAPROBE_XAXIS : (task.dim + 1); //task.dim++;

			//assert( near.bx.outside_proximity(target) == MYZERO ); //##MK:::DEBUG
			near.d = 0.; //##MK:: should be not significant so set to ... ##### 0.;
			near.dim = next_dim;

			far.d = far.bx.outside_proximity(target);
			far.dim = next_dim;

			tasks[current_task] = far; //MK::near must be processed before far!
			++current_task;

			assert( (apt_uint) current_task < stack_size );
			tasks[current_task] = near;
		}
		while ( current_task != -1 );
	}
	//return retval;
}

template void kdTree<p3d>::kd_query_exclude_target(
	const p3d target, const apt_real sqrR, vector<p3d> & cand );
template void kdTree<p3dm1>::kd_query_exclude_target(
	const p3dm1 target, const apt_real sqrR, vector<p3d> & cand );
template void kdTree<p3dmidx>::kd_query_exclude_target(
	const p3dmidx target, const apt_real sqrR, vector<p3d> & cand );
template void kdTree<p3dm5>::kd_query_exclude_target(
	const p3dm5 target, const apt_real sqrR, vector<p3d> & cand );


/*
void kd_tree::get_allboundingboxes( vector<scan_task> & out )
{
	//returns all bounding boxes of the tree
	const size_t stack_size = ConfigShared::KDTreeStackSize;
	cuboid aabb(min, max);
	scan_task tasks[stack_size] = {aabb, 0, 0}; //first box, first node, dim, distance to first
	int current_task = 0;
	out.push_back( tasks[current_task] );

	do
	{
		const scan_task &task = tasks[current_task];
		const node &n = nodes[task.node];

		if ( is_leaf(n) == true ) {
			//if arrived at leaf we just have to jump back, the corresponding box pair was written out already at one level higher before
			--current_task;
			continue;
		}

		scan_task aa; //##MK::irrelevant whether aa or bb are qualitatively near or far we want to visit all nodes...
		scan_task bb;
		if ( task.dim == PARAPROBE_XAXIS ) {
			const apt_real splitposx = n.splitpos;
			const cuboid a = cuboid( task.bx.min, p3d(splitposx, task.bx.max.y, task.bx.max.z) );
			const cuboid b = cuboid( p3d(splitposx, task.bx.min.y, task.bx.min.z), task.bx.max );
			aa.bx = a;
			aa.node = n.i0;
			bb.bx = b;
			bb.node = n.i1;
		}
		else if ( task.dim == PARAPROBE_YAXIS ) {
			const apt_real splitposy = n.splitpos;
			const cuboid a = cuboid( task.bx.min, p3d(task.bx.max.x, splitposy, task.bx.max.z) );
			const cuboid b = cuboid( p3d(task.bx.min.x, splitposy, task.bx.min.z), task.bx.max );
			aa.bx = a;
			aa.node = n.i0;
			bb.bx = b;
			bb.node = n.i1;
		}
		else {
			const apt_real splitposz = n.splitpos;
			const cuboid a = cuboid( task.bx.min, p3d(task.bx.max.x, task.bx.max.y, splitposz) );
			const cuboid b = cuboid( p3d(task.bx.min.x, task.bx.min.y, splitposz), task.bx.max );
			aa.bx = a;
			aa.node = n.i0;
			bb.bx = b;
			bb.node = n.i1;
		}

		size_t next_dim = ((task.dim + 1) > PARAPROBE_ZAXIS) ? PARAPROBE_XAXIS : (task.dim + 1); //circular looping
		aa.dim = next_dim;
		bb.dim = next_dim;

		tasks[current_task] = aa; //MK::near must be processed before far!
		out.push_back( aa );
		++current_task;

		assert( static_cast<size_t>(current_task) < stack_size);
		out.push_back( bb );
		tasks[current_task] = bb;

	} while ( current_task != -1 );
}


void kd_tree::display_nodes()
{
	for(size_t i = 0; i < nodes.size(); ++i) {
		node &n = nodes[i];
		if(is_leaf(n)==true) {
			cout << n.i0 << "\t\t" << n.i1 << "\t\t" << n.split << "\t\t" << (nodes[i].i1 - nodes[i].i0 + 1) << "\n";
		}
	}
}


size_t kd_tree::get_treememory_consumption()
{
	size_t bytes = 2*sizeof(p3d) + nodes.size()*sizeof(node);
	return bytes;
}
*/


