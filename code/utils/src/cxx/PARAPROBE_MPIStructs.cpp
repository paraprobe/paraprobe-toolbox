/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_MPIStructs.h"


MPI_Iontype::MPI_Iontype()
{
	mqival_start = 0;
	mqival_end = 0;
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		ivec[i] = 0;
	}
	charge_sgn = MYNEUTRAL;
	charge_state = 0x00;
	itypid = UNKNOWNTYPE;
}


MPI_Iontype::MPI_Iontype( const apt_uint _start, const apt_uint _end, vector<unsigned short> const & _ivec,
		const unsigned char _sgn, const unsigned char _chrg, const apt_uint _id )
{
	mqival_start = _start;
	mqival_end = _end;
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		ivec[i] = _ivec.at(i);
	}
	charge_sgn = _sgn;
	charge_state = _chrg;
	itypid = _id;
}


ostream& operator<<(ostream& in, MPI_Iontype const & val)
{
	in << "id " << (int) val.itypid << "\n";
	if ( val.charge_sgn != MYNEUTRAL ) {
		if ( val.charge_sgn == MYPOSITIVE )
			in << "charge " << (int) val.charge_state << "+" << "\n";
		else
			in << "charge " << (int) val.charge_state << "-" << "\n";
	}
	else {
		in << "charge " << "neutral" << "\n";
	}
	in << "isotope vector " << "\n";
	for( int i = 0; i < MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION; i++ ) {
		in << val.ivec[i] << ", ";
	}
	in << "\n";
	in << "mqival_start " << val.mqival_start << "\n";
	in << "mqival_end " << val.mqival_end << "\n";
	return in;
}


ostream& operator<<(ostream& in, MPI_MQIval const & val)
{
	in << "low/high " << val.low << ", " << val.high << "\n";
	return in;
}
