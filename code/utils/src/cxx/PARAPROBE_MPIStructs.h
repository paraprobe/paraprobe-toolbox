/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_MPISTRUCTS_H__
#define __PARAPROBE_UTILS_MPISTRUCTS_H__

#include "PARAPROBE_UserStructs.h"


#define MPIIO_OFFSET_INCR_F64			8		//##MK::improve portability with MPI_Aint and sizeof type queries
#define MPIIO_OFFSET_INCR_F32			4
#define MPIIO_OFFSET_INCR_INT32			4
#define MPIIO_OFFSET_INCR_UINT32		4


struct MPI_DistAndID
{
	apt_real d;
	apt_uint id;
	MPI_DistAndID() : d(0.), id(UMX) {}
	MPI_DistAndID( const apt_real _d, const apt_uint _id ) : d(_d), id(_id) {}
};


struct MPI_WorkAndID
{
	apt_uint ntriangles;
	apt_uint id;
	MPI_WorkAndID() : ntriangles(0), id(UMX) {}
	MPI_WorkAndID( const apt_uint _ntri, const apt_uint _id) : ntriangles(_ntri), id(_id) {}
};


struct MPI_Point3D
{
	apt_real x;
	apt_real y;
	apt_real z;
	MPI_Point3D() : x(MYZERO), y(MYZERO), z(MYZERO) {}
	MPI_Point3D( const apt_real _x, const apt_real _y, const apt_real _z ) :
		x(_x), y(_y), z(_z) {}
	MPI_Point3D( p3d const & _p ) :
		x(_p.x), y(_p.y), z(_p.z) {}
};


struct MPI_Triangle3D
{
	apt_real x1;
	apt_real y1;
	apt_real z1;
	apt_real x2;
	apt_real y2;
	apt_real z2;
	apt_real x3;
	apt_real y3;
	apt_real z3;
	MPI_Triangle3D() : x1(MYZERO), y1(MYZERO), z1(MYZERO), x2(MYZERO), y2(MYZERO), z2(MYZERO), x3(MYZERO), y3(MYZERO), z3(MYZERO) {}
	MPI_Triangle3D( tri3d const & _tri ) :
		x1(_tri.x1), y1(_tri.y1), z1(_tri.z1),
		x2(_tri.x2), y2(_tri.y2), z2(_tri.z2),
		x3(_tri.x3), y3(_tri.y3), z3(_tri.z3) {}
};


struct MPI_Iontype
{
	apt_uint mqival_start;		//index on linearized array of mqival pairs indicating which contiguous block of pairs belong to which ion
	apt_uint mqival_end;		//index on linearized array one past last element
	unsigned short ivec[MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION];
	unsigned char charge_sgn;
	unsigned char charge_state;
	unsigned char itypid;
	MPI_Iontype();
	MPI_Iontype( const apt_uint _start, const apt_uint _end, vector<unsigned short> const & _ivec, const unsigned char _sgn,
			const unsigned char _chrg, const apt_uint _id );
};

ostream& operator<<(ostream& in, MPI_Iontype const & val);


struct MPI_MQIval
{
	apt_real low;
	apt_real high;
	MPI_MQIval() : low(MYZERO), high(MYZERO) {}
	MPI_MQIval( const apt_real _low, const apt_real _high ) :
		low(_low), high(_high) {}
};

ostream& operator<<(ostream& in, MPI_MQIval const & val);



/*
struct MPI_Ranger_Iontype
{
	unsigned char id;
	unsigned char Z1;
	unsigned char N1;
	unsigned char Z2;
	unsigned char N2;
	unsigned char Z3;
	unsigned char N3;
	unsigned char sign;
	unsigned char charge;
	MPI_Ranger_Iontype() : id(0x00), Z1(0x00), N1(0x00),
			Z2(0x00), N2(0x00), Z3(0x00), N3(0x00), sign(0x00), charge(0x00) {}
	MPI_Ranger_Iontype( const unsigned int _id, const unsigned int _z1, const unsigned int _n1,
			const unsigned int _z2, const unsigned int _n2,
			const unsigned int _z3, const unsigned int _n3,
			const unsigned int _sgn, const unsigned int _chg ) :
				id(_id), Z1(_z1), N1(_n1), Z2(_z2), N2(_n2),
				Z3(_z3), N3(_n3), sign(_sgn), charge(_chg) {}
};


struct MPI_Ranger_DictKeyVal
{
	//https://stackoverflow.com/questions/15637228/what-is-the-downside-of-replacing-size-t-with-unsigned-long
	unsigned long long key;
	unsigned long long val;
	MPI_Ranger_DictKeyVal() : key(0), val(0) {}
	MPI_Ranger_DictKeyVal( const size_t _key, const size_t _val ) : key(_key), val(_val) {}
};


struct MPI_Synth_XYZ
{
	apt_real x;
	apt_real y;
	apt_real z;
	MPI_Synth_XYZ() : x(MYZERO), y(MYZERO), z(MYZERO) {}
	MPI_Synth_XYZ( p3d const & p ) : x(p.x), y(p.y), z(p.z) {}
};


struct MPI_VoroCellInfo
{
	double V;					//cell volume
	apt_uint IonID;			//the global with according to the implicit order of the input data!
	apt_uint pad;
	MPI_VoroCellInfo() : V(0.0), IonID(UINT32MX), pad(0)  {}
	MPI_VoroCellInfo(  const double _vol, const apt_uint _ionid ) : V(_vol), IonID(_ionid), pad(0) {}
};
*/

#endif
