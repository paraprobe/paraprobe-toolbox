/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_Math.h"


ostream& operator<<(ostream& in, t3x1 const & val)
{
	in << "a11 " << val.a11 << "\n";
	in << "a21 " << val.a21 << "\n";
	in << "a31 " << val.a31 << "\n";
	return in;
}



void t3x3::add( const t3x3 & increase, const apt_real weight )
{
	this->a11 += weight * increase.a11;
	this->a12 += weight * increase.a12;
	this->a13 += weight * increase.a13;

	this->a21 += weight * increase.a21;
	this->a22 += weight * increase.a22;
	this->a23 += weight * increase.a23;

	this->a31 += weight * increase.a31;
	this->a32 += weight * increase.a32;
	this->a33 += weight * increase.a33;
}


void t3x3::div( const apt_real divisor )
{
	if (abs(divisor) > EPSILON) {
		this->a11 /= divisor;
		this->a12 /= divisor;
		this->a13 /= divisor;

		this->a21 /= divisor;
		this->a22 /= divisor;
		this->a23 /= divisor;

		this->a31 /= divisor;
		this->a32 /= divisor;
		this->a33 /= divisor;
	}
}


apt_real t3x3::det()
{
	apt_real row1 = +MYONE*this->a11 * ((this->a22*this->a33) - (this->a32*this->a23));
	apt_real row2 = +MYONE*this->a21 * ((this->a12*this->a33) - (this->a32*this->a13));
	apt_real row3 = +MYONE*this->a31 * ((this->a12*this->a23) - (this->a22*this->a13));
	return row1 - row2 + row3;
}


t3x3 t3x3::leftmultiply( t3x3 const & T )
{
	return t3x3(
			this->a11*T.a11 + this->a12*T.a21 + this->a13 * T.a31,
			this->a11*T.a12 + this->a12*T.a22 + this->a13 * T.a32,
			this->a11*T.a13 + this->a12*T.a23 + this->a13 * T.a33,

			this->a21*T.a11 + this->a22*T.a21 + this->a23 * T.a31,
			this->a21*T.a12 + this->a22*T.a22 + this->a23 * T.a32,
			this->a21*T.a13 + this->a22*T.a23 + this->a23 * T.a33,

			this->a31*T.a11 + this->a32*T.a21 + this->a33 * T.a31,
			this->a31*T.a12 + this->a32*T.a22 + this->a33 * T.a32,
			this->a31*T.a13 + this->a32*T.a23 + this->a33 * T.a33  );
}


ostream& operator<<(ostream& in, t3x3 const & val)
{
	in << "a11/a12/a13 " << val.a11 << ", " << val.a12 << ", " << val.a13 << "\n";
	in << "a21/a22/a23 " << val.a21 << ", " << val.a22 << ", " << val.a23  << "\n";
	in << "a31/a32/a33 " << val.a31 << ", " << val.a32 << ", " << val.a33  << "\n";
	return in;
}


template<typename T>
T lerp(const T v0, const T v1, const T t)
{
	return (MYONE - t)*v0 + t*v1;
}

template float lerp<float>(const float v0, const float v1, const float t);
template double lerp<double>(const double v0, const double v1, const double t);


template<typename T>
vector<T> quantiles_nosort( vector<T> const & in, vector<T> const & q )
{
	if ( in.size() > 1 ) { //most likely more than one or nothing
		//##MK::if desired use random sub-sampling here
		//vector<apt_real> data = in;

		//##MK::can be improved for instance if only quantiles << 1 are sought via using n_th element
		//sort(data.begin(), data.end());

		vector<T> quants;
		for ( size_t i = 0; i < q.size(); ++i ) {
			T poi = lerp<T>(-0.5, static_cast<T>(in.size()) - 0.5, q.at(i));

			size_t left = max( static_cast<int64_t>(floor(poi)), static_cast<int64_t>(0) );
			size_t right = min( static_cast<int64_t>(ceil(poi)), static_cast<int64_t>(in.size() - 1) );

			T datLeft = in.at(left);
			T datRight = in.at(right);

			T quantile = lerp<T>(datLeft, datRight, poi - static_cast<T>(left) );

			quants.push_back(quantile);
		}

		return quants;
	}
	else if ( in.size() == 1 ) {
		return vector<T>(1, in[0]);
	}
	else {
		return vector<T>();
	}
}

template vector<float> quantiles_nosort<float>( vector<float> const & in, vector<float> const & q );
template vector<double> quantiles_nosort<double>( vector<double> const & in, vector<double> const & q );


template<typename T>
T quantile( vector<T> const & in, const T q )
{
	if ( in.size() > 2 ) { //most likely more than one or nothing
		T poi = lerp<T>(-0.5, static_cast<T>(in.size()) - 0.5, q );

		size_t left = max( static_cast<int64_t>(floor(poi)), static_cast<int64_t>(0) );
		size_t right = min( static_cast<int64_t>(ceil(poi)), static_cast<int64_t>(in.size() - 1) );

		T datLeft = in.at(left);
		T datRight = in.at(right);

		return lerp<T>(datLeft, datRight, poi - static_cast<T>(left) );
	}
	else if ( in.size() == 1 ){
		return in[0];
	}
	else {
		cerr << "Trying to compute a quantile from an empty vector !" << "\n";
		return MYZERO;
	}
}

template float quantile<float>( vector<float> const & in, const float q);
template double quantile<double>( vector<double> const & in, const double q );

