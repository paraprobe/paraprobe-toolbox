/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_OSINTERFACE_H__
#define __PARAPROBE_UTILS_OSINTERFACE_H__

#include "PARAPROBE_ConfigShared.h"

//low level POSIX stuff to Posixs pagesize to identify memory consumption
//##MK::some systems' POSIX library implements _SC_PAGESIZE
#define SYSTEMSPECIFIC_POSIX_PAGESIZE				(_SC_PAGE_SIZE)
#define SYSTEMSPECIFIC_POSIX_NPAGES					(_SC_PHYS_PAGES)
#define SYSTEMSPECIFIC_POSIX_HOSTNAMEMAX			(64)
//https://stackoverflow.com/questions/30084116/host-name-max-undefined-after-include-limits-h
//https://stackoverflow.com/questions/27914311/get-computer-name-and-logged-user-name/44152790


//low level compile time check to assure that the byte order on the target system is little endian
#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
	#error paraprobe-toolbox requires a LittleEndian architecture
#endif
//https://stackoverflow.com/questions/4239993/determining-endianness-at-compile-time


#endif
