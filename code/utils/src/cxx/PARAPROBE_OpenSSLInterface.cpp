/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_OpenSSLInterface.h"


string SHA256( const string path )
{
	ifstream fp(path, ios::in | ios::binary);

	if (not fp.good()) {
		cerr << "Can not open path " << path << "\n";
		return "";
	}

	constexpr const size_t buffer_size { 1 << 12 };
	char buffer[buffer_size];

	ostringstream os;

	EVP_MD_CTX* context = EVP_MD_CTX_new();
	if(context != NULL) {
		if(EVP_DigestInit_ex(context, EVP_sha256(), NULL)) {

			while (fp.good()) {
				fp.read(buffer, buffer_size);

				if( EVP_DigestUpdate(context, buffer, fp.gcount()) ) {
					continue;
				}
				else {
					cerr << "EVP_DigestUpdate failed on path " << path << "\n";
				}
			}

			unsigned char hash[EVP_MAX_MD_SIZE];
			unsigned int lengthOfHash = 0;

			if(EVP_DigestFinal_ex(context, hash, &lengthOfHash)) {
				for( unsigned int i = 0; i < lengthOfHash; ++i ) {
					os << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
				}
			}
		}
		EVP_MD_CTX_free(context);
	}

	fp.close();
	return os.str();
}
