/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_ORIMATH_H__
#define __PARAPROBE_UTILS_ORIMATH_H__


#include "PARAPROBE_Crystallography.h"

#define P_IJK			-1

struct bunge;

struct squat
{
	//w,v,x,z
	apt_real q0;
	apt_real q1;
	apt_real q2;
	apt_real q3;
	squat() : q0(MYONE), q1(MYZERO), q2(MYZERO), q3(MYZERO) {}
	squat( const apt_real _q0, const apt_real _q1, const apt_real _q2, const apt_real _q3 );
	squat( const apt_real X0, const apt_real X1, const apt_real X2 );

	void normalize();
	squat invert(); //to switch active <=> passive
	squat conjugate();
	squat multiply_quaternion( squat const & q);

	p3d Lqr( v3d const & r );
	apt_real disorientation_angle_fcc( squat & q ); //me being p
	
	//conversion routine D. Rowenhorst et al. MSMSE 23 2015
	t3x3 qu2om();
	bunge qu2eu();
};

ostream& operator<<(ostream& in, squat const & val);

squat multiply_quaternion( squat const & p, squat const & q);
apt_real disorientation_angle_fcc_grimmer( squat const & qcand );

void cubic_cubic_operators( vector<squat> & out );

pair<bool,squat> disoriquaternion_cubic_cubic( squat const & ori_p, squat const & ori_q );


struct bunge
{
	//Bunge Texture Orientations book
	//phi1, Phi, phi2
	apt_real phi1;
	apt_real Phi;
	apt_real phi2;
	bunge() : phi1(MYZERO), Phi(MYZERO), phi2(MYZERO) {}
	bunge( const apt_real _e1, const apt_real _e2, const apt_real _e3 ) :
		phi1(_e1), Phi(_e2), phi2(_e3) {}
	//bunge( const apt_real _q0, const apt_real _q1, const apt_real _q2, const apt_real _q3 ); //qu2eu
	bunge( const string parseme );

	squat eu2qu();
	t3x3 eu2om();
};

ostream& operator<<(ostream& in, bunge const & val);


struct orimatrix
{
	apt_real a11;
	apt_real a12;
	apt_real a13;
	apt_real a21;
	apt_real a22;
	apt_real a23;
	apt_real a31;
	apt_real a32;
	apt_real a33;
	orimatrix() :	a11(MYONE), a12(MYZERO), a13(MYZERO),
					a21(MYZERO), a22(MYONE), a23(MYZERO),
					a31(MYZERO), a32(MYZERO), a33(MYONE) {}	//initialize to identity orientation
	orimatrix(	const apt_real _a11, const apt_real _a12, const apt_real _a13,
				const apt_real _a21, const apt_real _a22, const apt_real _a23,
				const apt_real _a31, const apt_real _a32, const apt_real _a33 ) :
					a11(_a11), a12(_a12), a13(_a13),
					a21(_a21), a22(_a22), a23(_a23),
					a31(_a31), a32(_a32), a33(_a33) {}
	apt_real det();
	orimatrix inv();
	squat om2qu();
};

ostream& operator << (ostream& in, orimatrix const & val);


//cubic crystal symmetry operators
//##MK::http://pajarito.materials.cmu.edu/rollett/27750/L13-Grain_Bndries_RFspace-15Mar16.pdf
//see here also https://github.com/BlueQuartzSoftware/DREAM3D/commit/0446b45616257e421c0e6b4550d412b1af171020

//mind that dream3d has VectorScale and non vector scale i.e. x,y,z,w and w, x,y,z quaternions!
//PARAPROBE uses w, x, y, z !

//identity
//
//DREAM3D 1
#define CUBIC_SYMM_L01_Q0		(+MYONE)
#define CUBIC_SYMM_L01_Q1		(+MYZERO)
#define CUBIC_SYMM_L01_Q2		(+MYZERO)
#define CUBIC_SYMM_L01_Q3		(+MYZERO)

//2fold
//DREAM3D

//corresponding in DREAM3D assuming DREAM3D encodes x,y,z,w
//2,3,4
#define CUBIC_SYMM_L03_Q0		(+MYZERO)
#define CUBIC_SYMM_L03_Q1		(+MYONE)
#define CUBIC_SYMM_L03_Q2		(+MYZERO)
#define CUBIC_SYMM_L03_Q3		(+MYZERO)

#define CUBIC_SYMM_L05_Q0		(+MYZERO)
#define CUBIC_SYMM_L05_Q1		(+MYZERO)
#define CUBIC_SYMM_L05_Q2		(+MYONE)
#define CUBIC_SYMM_L05_Q3		(+MYZERO)

#define CUBIC_SYMM_L06_Q0		(+MYZERO)
#define CUBIC_SYMM_L06_Q1		(+MYZERO)
#define CUBIC_SYMM_L06_Q2		(+MYZERO)
#define CUBIC_SYMM_L06_Q3		(+MYONE)


#define ONE_OVER_SQRT2			(MYONE/sqrt(2.))
//4fold <100>
//DREAM3D 19, 26,7,21,27,9,23,25,15,17,11,13
#define CUBIC_SYMM_L07_Q0		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L07_Q1		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L07_Q2		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L07_Q3		(+ONE_OVER_SQRT2 * +MYONE)

#define CUBIC_SYMM_L09_Q0		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L09_Q1		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L09_Q2		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L09_Q3		(+ONE_OVER_SQRT2 * -MYONE)

#define CUBIC_SYMM_L11_Q0		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L11_Q1		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L11_Q2		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L11_Q3		(+ONE_OVER_SQRT2 * +MYONE)

#define CUBIC_SYMM_L13_Q0		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L13_Q1		(+ONE_OVER_SQRT2 * -MYONE)
#define CUBIC_SYMM_L13_Q2		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L13_Q3		(+ONE_OVER_SQRT2 * +MYONE)

#define CUBIC_SYMM_L15_Q0		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L15_Q1		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L15_Q2		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L15_Q3		(+ONE_OVER_SQRT2 * +MYONE)

#define CUBIC_SYMM_L17_Q0		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L17_Q1		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L17_Q2		(+ONE_OVER_SQRT2 * -MYONE)
#define CUBIC_SYMM_L17_Q3		(+ONE_OVER_SQRT2 * +MYONE)

//2fold <110>
#define CUBIC_SYMM_L19_Q0		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L19_Q1		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L19_Q2		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L19_Q3		(+ONE_OVER_SQRT2 * +MYZERO)

#define CUBIC_SYMM_L21_Q0		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L21_Q1		(+ONE_OVER_SQRT2 * -MYONE)
#define CUBIC_SYMM_L21_Q2		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L21_Q3		(+ONE_OVER_SQRT2 * +MYZERO)

#define CUBIC_SYMM_L23_Q0		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L23_Q1		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L23_Q2		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L23_Q3		(+ONE_OVER_SQRT2 * +MYZERO)

#define CUBIC_SYMM_L25_Q0		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L25_Q1		(+ONE_OVER_SQRT2 * -MYONE)
#define CUBIC_SYMM_L25_Q2		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L25_Q3		(+ONE_OVER_SQRT2 * +MYZERO)

#define CUBIC_SYMM_L26_Q0		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L26_Q1		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L26_Q2		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L26_Q3		(+ONE_OVER_SQRT2 * +MYZERO)

#define CUBIC_SYMM_L27_Q0		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L27_Q1		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L27_Q2		(+ONE_OVER_SQRT2 * -MYONE)
#define CUBIC_SYMM_L27_Q3		(+ONE_OVER_SQRT2 * +MYZERO)

#define CUBIC_SYMM_L29_Q0		(+ONE_OVER_SQRT2 * -MYONE)
#define CUBIC_SYMM_L29_Q1		(+ONE_OVER_SQRT2 * +MYZERO)
#define CUBIC_SYMM_L29_Q2		(+ONE_OVER_SQRT2 * +MYONE)
#define CUBIC_SYMM_L29_Q3		(+ONE_OVER_SQRT2 * +MYZERO)

#define ONE_HALF				(0.5)
//3-fold <111>
//DREAM3D assuming x, y, z, w in DREAM3D 31,45,37,41,35,39,43,33
#define CUBIC_SYMM_L31_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L31_Q1		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L31_Q2		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L31_Q3		(+ONE_HALF * +MYONE)

#define CUBIC_SYMM_L33_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L33_Q1		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L33_Q2		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L33_Q3		(+ONE_HALF * -MYONE)

#define CUBIC_SYMM_L35_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L35_Q1		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L35_Q2		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L35_Q3		(+ONE_HALF * +MYONE)

#define CUBIC_SYMM_L37_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L37_Q1		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L37_Q2		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L37_Q3		(+ONE_HALF * +MYONE)

#define CUBIC_SYMM_L39_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L39_Q1		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L39_Q2		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L39_Q3		(+ONE_HALF * -MYONE)

#define CUBIC_SYMM_L41_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L41_Q1		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L41_Q2		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L41_Q3		(+ONE_HALF * -MYONE)

#define CUBIC_SYMM_L43_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L43_Q1		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L43_Q2		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L43_Q3		(+ONE_HALF * +MYONE)

#define CUBIC_SYMM_L45_Q0		(+ONE_HALF * +MYONE)
#define CUBIC_SYMM_L45_Q1		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L45_Q2		(+ONE_HALF * -MYONE)
#define CUBIC_SYMM_L45_Q3		(+ONE_HALF * -MYONE)

#endif
