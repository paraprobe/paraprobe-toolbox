/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_PERIODICTABLE_H__
#define __PARAPROBE_UTILS_PERIODICTABLE_H__


#include "PARAPROBE_MPIStructs.h"


enum ION_DECOMPOSITION_METHOD {
	ION_DECOMPOSITION_NONE,
	ION_DECOMPOSITION_ATOM,
	ION_DECOMPOSTIION_ION
};
//##MK::maybe more are required


unsigned short isotope_hash( const unsigned char nprotons, const unsigned char nneutrons );
pair<unsigned char, unsigned char> isotope_unhash( const unsigned short hashval );


struct element
{
	apt_real mass; //u
	unsigned char nprotons;
	unsigned char pad1;
	unsigned char pad2;
	unsigned char pad3;
	element() : mass(MYZERO), nprotons(0x00), pad1(0x00), pad2(0x00), pad3(0x00) {}
	element( const apt_real _mass, const unsigned char _npro ) :
		mass(_mass), nprotons(_npro), pad1(0x00), pad2(0x00), pad3(0x00) {}
};

ostream& operator<<(ostream& in, element const & val);


struct isotope
{
	apt_real mass;
	apt_real abundance;
	unsigned char nprotons;
	unsigned char nneutrons;
	unsigned char observationally_stable; //radioactive;
	unsigned char unclear_half_life;
	isotope() : mass(MYZERO), abundance(MYZERO), nprotons(0x00), nneutrons(0x00),
			observationally_stable(false), unclear_half_life(0x00) {}
	isotope( const apt_real _mass, const apt_real _abun,
			const unsigned char _npro, const unsigned char _nneu, const bool _stable, const bool _unclear ) :
		mass(_mass), abundance(_abun), nprotons(_npro), nneutrons(_nneu),
		observationally_stable(_stable), unclear_half_life(0x00) {}

	unsigned short get_isotope_hash();
};

ostream& operator<<(ostream& in, isotope const & val);


vector<unsigned short> get_nuclids( unsigned short hash );


struct mqival
{
	apt_real lo;
	apt_real hi;
	mqival() : lo(MYZERO), hi(MYZERO) {}
	mqival( const apt_real _lo, const apt_real _hi) : lo(_lo), hi(_hi) {}

	apt_real width();
	bool inrange(const apt_real val);
};

ostream& operator<<(ostream& in, mqival const & val);


inline bool SortMQRangeAscending( const mqival &aa1, const mqival &aa2)
{
	//promoting sorting in descending order
	return aa1.hi < aa2.lo;
}


struct ion
{
	unsigned short ivec[MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION];
	unsigned char id;
	unsigned char charge_sgn;
	unsigned char charge_state;
	unsigned char pad;
	vector<mqival> ranges;
	ion();
	ion( vector<unsigned short> & ivec, const unsigned char id,
			const unsigned char charge_sgn, const unsigned char charge_state, vector<mqival> const & irng );
	vector<unsigned short> get_ivec() const;
	char get_charge() const;
	bool is_the_same_ivec_as( ion const & b ) const;
	bool is_the_same_as( ion const & b ) const;
	bool is_the_same_ivec_as( vector<unsigned short> const & iv ) const;
	bool is_the_unknown_iontype() const;
	apt_uint get_multiplicity( const string method, vector<unsigned short> const & nuclide_wlst, vector<int> const & charge_state_wlst ) const;
};

ostream& operator<<(ostream& in, ion const & val);


class rangeTable
{
public:
	rangeTable();
	~rangeTable();
	
	bool read_ionspecies_from_h5( const string h5fn, const string grpnm );
	bool init_ionspecies_custom( map<unsigned char, unsigned short> const & ityp2ivec );
	
	bool validity_check_rangemax();
	bool validity_check_noemptyness();
	bool validity_check_nonoverlapping();
	bool validity_check();

	pair<bool, unsigned char> get_ityp_if_exists( vector<unsigned short> const & ivec );

	//generalized iontype decomposition with respect to search patterns
	vector<pair<unsigned char, unsigned char>> generalized_ion_decomposition_using_search_pattern(
			unsigned int method, vector<unsigned short> ivec_matrix );

	map<unsigned char, ion> iontypes;		//taken as is from classical RNG and/or RRNG range files
	set<unsigned char> disjoint_elements; //e.g. 0, 6 always ordered ! for UNKNOWN_ELEMENT (0) and carbon (6) for instance, stores the number of protons to distinguish elements
	map<unsigned char, vector<apt_uint>> ityp2mltply; //for each iontype id how many element counts do we contribute to element types
	pair<apt_uint, apt_uint> mltplytable_dim;	//first is the number of row, second is the number of columns, columns change fastest get answer quickly for a given iontype the counts for each disjoint element
	vector<apt_uint> mltplytable_val;	//ityp2mltply vectorized
};


class periodicTable
{
public:
	//periodicTable();
	//~periodicTable();

	static map<string, element> MyElementsSymbol;
	static map<unsigned char, element> MyElementsZ;
	static vector<isotope> MyIsotopes;
};


#endif

