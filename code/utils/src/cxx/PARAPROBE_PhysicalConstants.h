/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_PHYSICALCONSTANTS_H__
#define __PARAPROBE_UTILS_PHYSICALCONSTANTS_H__

//natural constants
#define MYPI									(3.141592653589793238462643383279502884197169399375105820974)

//logical
#define NO										0x00
#define YES										0x01

//natural beauty
#define SQR(a)									((a)*(a))
#define CUBE(a)									((a)*(a)*(a))
#define MIN(X, Y)								(((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y)								(((X) < (Y)) ? (Y) : (X))
#define CLAMP(x, lo, hi)							(MIN(MAX((x), (lo)), (hi)))


#endif
