/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_PrimsContinuum.h"


ostream& operator<<(ostream& in, p1d const & val)
{
	in << "xm " << val.x << ", " << val.m << "\n";
	return in;
}


ostream& operator<<(ostream& in, p2d const & val)
{
	in << "xy " << val.x << ", " << val.y << "\n";
	return in;
}


ostream& operator<<(ostream& in, p2dm1 const & val)
{
	in << "xym " << val.x << ", " << val.y << ", " << val.m << "\n";
	return in;
}


p3d p3d::active_rotation_relocate( t3x3 const & R )
{
	return p3d( 	R.a11 * this->x + R.a12 * this->y + R.a13 * this->z,
					R.a21 * this->x + R.a22 * this->y + R.a23 * this->z,
					R.a31 * this->x + R.a32 * this->y + R.a33 * this->z   );
}


void p3d::active_shift_relocate( p3d const & dR )
{
	this->x += dR.x;
	this->y += dR.y;
	this->z += dR.z;
}


apt_real dotperp( p2d const & a, p2d const & b )
{
	return a.x*b.y - a.y*b.x;
}


apt_real dot( p2d const & a, p2d const & b )
{
	return a.x*b.x + a.y*b.y;
}


apt_real dot( p3d const & a, p3d const & b )
{
	return a.x*b.x + a.y*b.y + a.z*b.z;
}


p3d cross( p3d const & a, p3d const & b )
{
	return p3d( a.y*b.z - a.z*b.y,
				a.z*b.x - a.x*b.z,
				a.x*b.y - a.y*b.x );
}


apt_real vnorm( p3d const & a )
{
	return sqrt(SQR(a.x)+SQR(a.y)+SQR(a.z));
}


ostream& operator<<(ostream& in, p3d const & val)
{
	in << "xyz " << val.x << ", " << val.y << ", " << val.z << "\n";
	return in;
}



ostream& operator<<(ostream& in, p3dm1 const & val)
{
	in << "xyzm " << val.x << ", " << val.y << ", " << val.z << ", " << val.m << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dmidx const & val)
{
	in << "xyzm1m2 " << val.x << ", " << val.y << ", " << val.z << ", " << val.idx << ", " << (int) val.m1 << ", " << val.m2 << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dm5 const & val)
{
	in << "xyzidxm1m2m3m4 " << val.x << ", " << val.y << ", " << val.z << ", " << val.idx << ", " << (int) val.m1 << ", " << (int) val.m2 << ", " << val.m3 << ", " << val.m4 << "\n";
	return in;
}


void v3d::normalize()
{
	apt_real SQRlen = SQR(this->u)+SQR(this->v)+SQR(this->w);
	if ( SQRlen > EPSILON ) {
		apt_real norm = 1. / sqrt(SQRlen);
		this->u *= norm;
		this->v *= norm;
		this->w *= norm;
	}
	else {
		cerr << "Vector too short, setting to null vector !" << "\n";
		this->u = 0.;
		this->v = 0.;
		this->w = 0.;
	}
}


inline apt_real v3d::len() const
{
	apt_real SQRlen = SQR(this->u)+SQR(this->v)+SQR(this->w);
	if ( SQRlen > EPSILON ) {
		return sqrt(SQRlen);
	}
	else {
		cerr << "Vector too short, computation of sqrt unsafe !" << "\n";
		return 0.;
	}
}


ostream& operator<<(ostream& in, v3d const & val)
{
	in << "uvw " << val.u << ", " << val.v << ", " << val.w << "\n";
	return in;
}


ostream& operator<<(ostream& in, apt_sphere const & val)
{
	in << "xyzR " << val.x << ", " << val.y << ", " << val.z << ", " << val.R << "\n";
	return in;
}


ostream& operator<<(ostream& in, apt_cylinder const & val)
{
	in << "p1 " << val.p1.x << ";" << val.p1.y << ";" << val.p1.z << "\n";
	in << "p2 " << val.p2.x << ";" << val.p2.y << ";" << val.p2.z << "\n";
	in << "R " << val.R << "\n";
	return in;
}


p3d tri3d::barycenter()
{
	return p3d( (this->x1 + this->x2 + this->x3 )/3.,
				(this->y1 + this->y2 + this->y3 )/3.,
				(this->z1 + this->z2 + this->z3 )/3. );
}


apt_real tri3d::area()
{
	apt_real detA =
			+ (this->x1 * (this->y2*MYONE - MYONE*this->y3))
			- (this->y1 * (this->x2*MYONE - MYONE*this->x3))
			+ (MYONE * (this->x2*this->y3 - this->y2*this->x3));
	apt_real detB =
			+ (this->y1 * (this->z2*MYONE - MYONE*this->z3))
			- (this->z1 * (this->y2*MYONE - MYONE*this->y3))
			+ (MYONE * (this->y2*this->z3 - this->z2*this->y3));
	apt_real detC =
			+ (this->z1 * (this->x2*MYONE - MYONE*this->x3))
			- (this->x1 * (this->z2*MYONE - MYONE*this->z3))
			+ (MYONE * (this->z2*this->x3 - this->x2*this->z3));
	return 0.5 * sqrt( SQR(detA) + SQR(detB) + SQR(detC) );
}


apt_real tri3d::edge_length_ab()
{
	return sqrt( SQR(this->x2 - this->x1) + SQR(this->y2 - this->y1) + SQR(this->z2 - this->z1));
}


apt_real tri3d::edge_length_bc()
{
	return sqrt( SQR(this->x3 - this->x2) + SQR(this->y3 - this->y2) + SQR(this->z3 -  this->z2));
}


apt_real tri3d::edge_length_ca()
{
	return sqrt( SQR(this->x1 - this->x3) + SQR(this->y1 - this->y3) + SQR(this->z1 - this->z3));
}


apt_real tri3d::angle_ba_ca()
{
	v3d ba = v3d( this->x2 - this->x1, this->y2 - this->y1, this->z2 - this->z1 );
	v3d ca = v3d( this->x3 - this->x1, this->y3 - this->y1, this->z3 - this->z1 );
	return acos( ( ba.u * ca.u + ba.v * ca.v + ba.w * ca.w ) /
			 ( sqrt(SQR(ba.u) + SQR(ba.v) + SQR(ba.w)) * sqrt(SQR(ca.u) + SQR(ca.v) + SQR(ca.w)) ) );
}


apt_real tri3d::angle_cb_ab()
{
	v3d cb = v3d( this->x3 - this->x2, this->y3 - this->y2, this->z3 - this->z2 );
	v3d ab = v3d( this->x1 - this->x2, this->y1 - this->y2, this->z1 - this->z2 );
	return acos( ( cb.u * ab.u + cb.v * ab.v + cb.w * ab.w ) /
			 ( sqrt(SQR(cb.u) + SQR(cb.v) + SQR(cb.w)) * sqrt(SQR(ab.u) + SQR(ab.v) + SQR(ab.w)) ) );
}

apt_real tri3d::angle_ac_bc()
{
	v3d ac = v3d( this->x1 - this->x3, this->y1 - this->y3, this->z1 - this->z3 );
	v3d bc = v3d( this->x2 - this->x3, this->y2 - this->y3, this->z2 - this->z3 );
	return acos( ( ac.u * bc.u + ac.v * bc.v + ac.w * bc.w ) /
			 ( sqrt(SQR(ac.u) + SQR(ac.v) + SQR(ac.w)) * sqrt(SQR(bc.u) + SQR(bc.v) + SQR(bc.w)) ) );
}


ostream& operator<<(ostream& in, tri3d const & val)
{
	in << "x1y1z1 " << val.x1 << ";" << val.y1 << ";" << val.z1 << "\n";
	in << "x2y2z2 " << val.x2 << ";" << val.y2 << ";" << val.z2 << "\n";
	in << "x3y3z3 " << val.x3 << ";" << val.y3 << ";" << val.z3 << "\n";
	return in;
}


void aabb1d::add_epsilon_guard()
{
	this->zmi -= EPSILON;
	this->zmx += EPSILON;
}


void aabb1d::scale()
{
	this->zsz = this->zmx - this->zmi;
}


ostream& operator<<(ostream& in, aabb1d const & val)
{
	in << "zmi/zmx " << val.zmi << ", " << val.zmx << "\n";
	in << "zsz " << val.zsz << "\n";
	return in;
}


void aabb2d::add_epsilon_guard()
{
	this->xmi -= EPSILON;
	this->xmx += EPSILON;
	this->ymi -= EPSILON;
	this->ymx += EPSILON;
}


void aabb2d::scale()
{
	this->xsz = this->xmx - this->xmi;
	this->ysz = this->ymx - this->ymi;
}


void aabb2d::blowup( const apt_real f )
{
	this->xmi -= f;
	this->xmx += f;
	this->ymi -= f;
	this->ymx += f;
	this->scale();
}


apt_real aabb2d::diag()
{
	return sqrt(SQR(this->xmx-this->xmi)+SQR(this->ymx-this->ymi));
}


void aabb2d::possibly_enlarge_me( p2dm1 const & p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;
}


ostream& operator<<(ostream& in, aabb2d const & val)
{
	in << "xmi/xmx " << val.xmi << ", " << val.xmx << "\n";
	in << "ymi/ymx " << val.ymi << ", " << val.ymx << "\n";
	in << "xsz/ysz " << val.xsz << ", " << val.ysz << "\n";
	return in;
}


void aabb3d::add_guard( const apt_real di )
{
	this->xmi -= di;
	this->xmx += di;
	this->ymi -= di;
	this->ymx += di;
	this->zmi -= di;
	this->zmx += di;
}


void aabb3d::add_epsilon_guard()
{
	this->xmi -= EPSILON;
	this->xmx += EPSILON;
	this->ymi -= EPSILON;
	this->ymx += EPSILON;
	this->zmi -= EPSILON;
	this->zmx += EPSILON;
}


void aabb3d::scale()
{
	this->xsz = this->xmx - this->xmi;
	this->ysz = this->ymx - this->ymi;
	this->zsz = this->zmx - this->zmi;
}

void aabb3d::blowup( const apt_real f )
{
	this->xmi -= f;
	this->xmx += f;
	this->ymi -= f;
	this->ymx += f;
	this->zmi -= f;
	this->zmx += f;
	this->scale();
}


apt_real aabb3d::diag()
{
	return sqrt(SQR(this->xmx-this->xmi)+SQR(this->ymx-this->ymi)+SQR(this->zmx-this->zmi));
}


bool aabb3d::is_inside( p3d const & test ) {
	if ( test.x < this->xmi )
		return false;
	if ( test.x > this->xmx )
		return false;
	if ( test.y < this->ymi )
		return false;
	if ( test.y > this->ymx )
		return false;
	if ( test.z < this->zmi )
		return false;
	if ( test.z > this->zmx )
		return false;
	//not returned so test either on box walls or inside
	return true;
}


bool aabb3d::is_inside( p3dm1 const & test )
{
	if ( test.x < this->xmi )
		return false;
	if ( test.x > this->xmx )
		return false;
	if ( test.y < this->ymi )
		return false;
	if ( test.y > this->ymx )
		return false;
	if ( test.z < this->zmi )
		return false;
	if ( test.z > this->zmx )
		return false;
	//not returned so test either on box walls or inside
	return true;
}


bool aabb3d::is_inside( p3dmidx const & test )
{
	if ( test.x < this->xmi )
		return false;
	if ( test.x > this->xmx )
		return false;
	if ( test.y < this->ymi )
		return false;
	if ( test.y > this->ymx )
		return false;
	if ( test.z < this->zmi )
		return false;
	if ( test.z > this->zmx )
		return false;
	//not returned so test either on box walls or inside
	return true;
}


p3d aabb3d::center()
{
	return p3d( 0.5*(this->xmi + this->xmx),
				0.5*(this->ymi + this->ymx),
				0.5*(this->zmi + this->zmx) );
}


apt_real aabb3d::circumscribed_sphere()
{
	p3d boxcenter = this->center();
	apt_real RSQR = 0.;
	apt_real DistSQR = 0.;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmi-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymi-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmi-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;
	DistSQR = SQR(this->xmx-boxcenter.x) + SQR(this->ymx-boxcenter.y) + SQR(this->zmx-boxcenter.z);
	if ( DistSQR >= RSQR )
		RSQR = DistSQR;

	if ( RSQR > EPSILON )
		return sqrt(RSQR);
	else
		return RMX;
}


apt_real aabb3d::volume()
{
	return 	(this->xmx - this->xmi) *
			(this->ymx - this->ymi) *
			(this->zmx - this->zmi);
}


vector<apt_real> aabb3d::nexus_get_vertices()
{
	vector<apt_real> retval;
	retval.push_back( this->xmi ); //0
	retval.push_back( this->ymi );
	retval.push_back( this->zmi );

	retval.push_back( this->xmx ); //1
	retval.push_back( this->ymi );
	retval.push_back( this->zmi );

	retval.push_back( this->xmx ); //2
	retval.push_back( this->ymx );
	retval.push_back( this->zmi );

	retval.push_back( this->xmi ); //3
	retval.push_back( this->ymx );
	retval.push_back( this->zmi );

	retval.push_back( this->xmi ); //4
	retval.push_back( this->ymi );
	retval.push_back( this->zmx );

	retval.push_back( this->xmx ); //5
	retval.push_back( this->ymi );
	retval.push_back( this->zmx );

	retval.push_back( this->xmx ); //6
	retval.push_back( this->ymx );
	retval.push_back( this->zmx );

	retval.push_back( this->xmi ); //7
	retval.push_back( this->ymx );
	retval.push_back( this->zmx );
	return retval;
}


vector<apt_uint> aabb3d::nexus_get_faces_as_quads( const bool xdmf )
{
	vector<apt_uint> retval;
	if ( xdmf == true ) {
		retval.push_back( 9 ); //xdmf hexahedron type key
		retval.push_back( 6 ); //xdmf number of faces of hexahedron as it is a polyhedron
		retval.push_back( 4 ); //xdmf number of nodes each face is made of quad
	}
	retval.push_back( 1 ); //+x point at the viewer, right
	retval.push_back( 2 );
	retval.push_back( 6 );
	retval.push_back( 5 );
	if ( xdmf == true ) {
		retval.push_back( 4 );
	}
	retval.push_back( 3 ); //-x point at the viewer, left
	retval.push_back( 0 );
	retval.push_back( 4 );
	retval.push_back( 7 );
	if ( xdmf == true ) {
		retval.push_back( 4 );
	}
	retval.push_back( 2 ); //+y point at the viewer, rear
	retval.push_back( 3 );
	retval.push_back( 7 );
	retval.push_back( 6 );
	if ( xdmf == true ) {
		retval.push_back( 4 );
	}
	retval.push_back( 0 ); //-y point at the viewer, front
	retval.push_back( 1 );
	retval.push_back( 5 );
	retval.push_back( 4 );
	if ( xdmf == true ) {
		retval.push_back( 4 );
	}
	retval.push_back( 4 ); //+z point at the viewer, top
	retval.push_back( 5 );
	retval.push_back( 6 );
	retval.push_back( 7 );
	if ( xdmf == true ) {
		retval.push_back( 4 );
	}
	retval.push_back( 3 ); //-z point at the viewer, bottom
	retval.push_back( 2 );
	retval.push_back( 1 );
	retval.push_back( 0 );
	return retval;
}


void aabb3d::possibly_enlarge_me( p3d const & p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;

	if ( p.z <= this->zmi )
		this->zmi = p.z;
	if ( p.z >= this->zmx )
		this->zmx = p.z;
}


void aabb3d::possibly_enlarge_me( p3dm1 const & p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;

	if ( p.z <= this->zmi )
		this->zmi = p.z;
	if ( p.z >= this->zmx )
		this->zmx = p.z;
}


void aabb3d::possibly_enlarge_me( p3dmidx const & p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;

	if ( p.z <= this->zmi )
		this->zmi = p.z;
	if ( p.z >= this->zmx )
		this->zmx = p.z;
}


void aabb3d::possibly_enlarge_me( p3dm5 const & p )
{
	if ( p.x <= this->xmi )
		this->xmi = p.x;
	if ( p.x >= this->xmx )
		this->xmx = p.x;

	if ( p.y <= this->ymi )
		this->ymi = p.y;
	if ( p.y >= this->ymx )
		this->ymx = p.y;

	if ( p.z <= this->zmi )
		this->zmi = p.z;
	if ( p.z >= this->zmx )
		this->zmx = p.z;
}


void aabb3d::possibly_enlarge_me( tri3d const & trgl )
{
	apt_real xmin = fmin( fmin(trgl.x1, trgl.x2), trgl.x3 );
	apt_real xmax = fmax( fmax(trgl.x1, trgl.x2), trgl.x3 );
	apt_real ymin = fmin( fmin(trgl.y1, trgl.y2), trgl.y3 );
	apt_real ymax = fmax( fmax(trgl.y1, trgl.y2), trgl.y3 );
	apt_real zmin = fmin( fmin(trgl.z1, trgl.z2), trgl.z3 );
	apt_real zmax = fmax( fmax(trgl.z1, trgl.z2), trgl.z3 );
	if ( xmin <= this->xmi )
		this->xmi = xmin;
	if ( xmax >= this->xmx )
		this->xmx = xmax;
	if ( ymin <= this->ymi )
		this->ymi = ymin;
	if ( ymax >= this->ymx )
		this->ymx = ymax;
	if ( zmin <= this->zmi )
		this->zmi = zmin;
	if ( zmax >= this->zmx )
		this->zmx = zmax;
}


void aabb3d::possibly_enlarge_me( aabb3d const & refbx )
{
	if ( refbx.xmi <= this->xmi )
		this->xmi = refbx.xmi;
	if ( refbx.xmx >= this->xmx )
		this->xmx = refbx.xmx;
	if ( refbx.ymi <= this->ymi )
		this->ymi = refbx.ymi;
	if ( refbx.ymx >= this->ymx )
		this->ymx = refbx.ymx;
	if ( refbx.zmi <= this->zmi )
		this->zmi = refbx.zmi;
	if ( refbx.zmx >= this->zmx )
		this->zmx = refbx.zmx;
}


ostream& operator<<(ostream& in, aabb3d const & val)
{
	in << "xmi/xmx " << val.xmi << ", " << val.xmx << "\n";
	in << "ymi/ymx " << val.ymi << ", " << val.ymx << "\n";
	in << "zmi/zmx " << val.zmi << ", " << val.zmx << "\n";
	in << "xsz/ysz/zsz " << val.xsz << ";" << val.ysz << ";" << val.zsz << "\n";
	return in;
}
