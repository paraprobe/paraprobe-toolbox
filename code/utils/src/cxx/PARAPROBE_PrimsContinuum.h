/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_PRIMSCONTINUUM_H__
#define __PARAPROBE_UTILS_PRIMSCONTINUUM_H__

#include "PARAPROBE_Math.h"

struct p1d
{
	apt_real x;
	apt_uint m;

	p1d() : x(RMX), m(UNKNOWNTYPE) {}
	p1d(const apt_real _x, const apt_uint _m) : x(_x), m(_m) {}
};

ostream& operator<<(ostream& in, p1d const & val);


struct p2d
{
	apt_real x;
	apt_real y;
	p2d() : x(MYZERO), y(MYZERO) {}
	p2d(const apt_real _x, const apt_real _y) : x(_x), y(_y) {}
};

ostream& operator<<(ostream& in, p2d const & val);


struct p2dm1
{
	apt_real x;
	apt_real y;
	apt_uint m;

	p2dm1() : x(MYZERO), y(MYZERO), m(UNKNOWNTYPE) {}
	p2dm1(const apt_real _x, const apt_real _y, const apt_uint _m ) :
		x(_x), y(_y), m(_m) {}
};

ostream& operator<<(ostream& in, p2dm1 const & val);


struct p3d
{
	apt_real x;
	apt_real y;
	apt_real z;

	p3d() : x(MYZERO), y(MYZERO), z(MYZERO) {}
	p3d(const apt_real _x, const apt_real _y, const apt_real _z) :
		x(_x), y(_y), z(_z) {}
	p3d active_rotation_relocate( t3x3 const & R );
	void active_shift_relocate( p3d const & dR );
};

ostream& operator<<(ostream& in, p3d const & val);


apt_real dotperp( p2d const & a, p2d const & b );
apt_real dot( p2d const & a, p2d const & b );
apt_real dot( p3d const & a, p3d const & b );
p3d cross( p3d const & a, p3d const & b );
apt_real vnorm( p3d const & a );


struct p3dm1
{
	apt_real x;
	apt_real y;
	apt_real z;
	apt_uint m;

	p3dm1() : x(MYZERO), y(MYZERO), z(MYZERO), m(UNKNOWNTYPE) {}
	p3dm1( const apt_real _x, const apt_real _y, const apt_real _z ) :
		x(_x), y(_y), z(_z), m(UNKNOWNTYPE) {}
	p3dm1(const apt_real _x, const apt_real _y, const apt_real _z, const apt_uint _m ) :
		x(_x), y(_y), z(_z), m(_m) {}
};

ostream& operator<<(ostream& in, p3dm1 const & val);


struct p3dmidx
{
	//3d point with index and two marks
	apt_real x;
	apt_real y;
	apt_real z;
	apt_uint idx;
	unsigned char m1; //i_org
	bool m2; //within threshold to edge yes or no?
	bool pad1;
	bool pad2;

	p3dmidx() : x(MYZERO), y(MYZERO), z(MYZERO), idx(UNKNOWNTYPE),
			m1(UNKNOWNTYPE), m2(false), pad1(false), pad2(false) {}
	p3dmidx(const apt_real _x, const apt_real _y, const apt_real _z ) :
		x(_x), y(_y), z(_z), idx(UNKNOWNTYPE),
		m1(UNKNOWNTYPE), m2(UNKNOWNTYPE), pad1(false), pad2(false) {}
	p3dmidx(const apt_real _x, const apt_real _y, const apt_real _z,
			const apt_uint _idx, const unsigned char _m1,
			const bool _m2 ) :
		x(_x), y(_y), z(_z), idx(_idx), m1(_m1), m2(_m2), pad1(false), pad2(false) {}
};

ostream& operator<<(ostream& in, p3dmidx const & val);


struct p3dm5
{
	//3d point with index, and two marks
	apt_real x;
	apt_real y;
	apt_real z;
	apt_uint idx; //evaporation ID
	unsigned char m1; //src_mult
	unsigned char m2; //trg_mult
	bool m3; //if true far_from_(d)edge
	bool m4; //if true close_to_(d)feat(ure)

	p3dm5() : x(MYZERO), y(MYZERO), z(MYZERO),
			idx(UMX), m1(0x00), m2(0x00), m3(false), m4(false) {}
	p3dm5(const apt_real _x, const apt_real _y, const apt_real _z ) :
			x(_x), y(_y), z(_z), idx(UMX), m1(0x00), m2(0x00), m3(false), m4(false) {}
	p3dm5(const apt_real _x, const apt_real _y, const apt_real _z, const apt_uint _idx,
			const unsigned char _m1, const unsigned char _m2, const bool _m3, const bool _m4 ) :
		x(_x), y(_y), z(_z), idx(_idx), m1(_m1), m2(_m2), m3(_m3), m4(_m4) {}
};

ostream& operator<<(ostream& in, p3dm5 const & val);


struct v3d
{
	//3d vector
	apt_real u;
	apt_real v;
	apt_real w;
	v3d() : u(MYZERO), v(MYZERO), w(MYZERO) {}
	v3d( const apt_real _u, const apt_real _v, const apt_real _w ) :
		u(_u), v(_v), w(_w) {}
	void normalize();
	inline apt_real len() const;
};

ostream& operator<<(ostream& in, v3d const & val);


struct apt_sphere
{
	apt_real x;
	apt_real y;
	apt_real z;
	apt_real R;
	apt_sphere() : x(MYZERO), y(MYZERO), z(MYZERO), R(MYZERO) {}
	apt_sphere(const apt_real _x, const apt_real _y, const apt_real _z, const apt_real _R) :
		x(_x), y(_y), z(_z), R(_R) {}
};

ostream& operator<<(ostream& in, apt_sphere const & val);


struct apt_cylinder
{
	p3d p1; //bottom cap center of mass
	p3d p2; //top cap center of mass
	apt_real R; //radius
	apt_cylinder() :
		p1(p3d()), p2(p3d()), R(MYZERO) {}
	apt_cylinder( const p3d _p1, const p3d _p2, const apt_real _R ) :
		p1(p3d(_p1.x,_p1.y,_p1.z)), p2(p3d(_p2.x,_p2.y,_p2.z)), R(_R) {}
};

ostream& operator<<(ostream& in, apt_cylinder const & val);


struct tri3d
{
	//triangle in 3d space defined by vertex positions
	apt_real x1;
	apt_real y1;
	apt_real z1;

	apt_real x2;
	apt_real y2;
	apt_real z2;

	apt_real x3;
	apt_real y3;
	apt_real z3;
	tri3d() : x1(MYZERO), y1(MYZERO), z1(MYZERO), x2(MYZERO), y2(MYZERO), z2(MYZERO), x3(MYZERO), y3(MYZERO), z3(MYZERO) {}
	tri3d( const apt_real _x1, const apt_real _y1, const apt_real _z1,
		const apt_real _x2, const apt_real _y2, const apt_real _z2,
		const apt_real _x3, const apt_real _y3, const apt_real _z3 ) :
		x1(_x1), y1(_y1), z1(_z1),
		x2(_x2), y2(_y2), z2(_z2),
		x3(_x3), y3(_y3), z3(_z3) {}
	p3d barycenter();
	apt_real area();
	apt_real edge_length_ab();
	apt_real edge_length_bc();
	apt_real edge_length_ca();

	//a, b, c, counter-clockwise opposite side
	apt_real angle_ba_ca();
	apt_real angle_cb_ab();
	apt_real angle_ac_bc();
};

ostream& operator<<(ostream& in, tri3d const & val);


struct aabb1d
{
	apt_real zmi;
	apt_real zmx;
	apt_real zsz;
	aabb1d() :
		zmi(RMX), zmx(RMI), zsz(MYZERO)  {}
	aabb1d(const apt_real _zmi, const apt_real _zmx ) :
		zmi(_zmi), zmx(_zmx), zsz(_zmx-_zmi) {}
	void add_epsilon_guard();
	void scale();
};

ostream& operator<<(ostream& in, aabb1d const & val);


struct aabb2d
{
	apt_real xmi;
	apt_real xmx;
	apt_real ymi;
	apt_real ymx;
	apt_real xsz;
	apt_real ysz;
	aabb2d() :
		xmi(RMX), xmx(RMI), ymi(RMX), ymx(RMI), xsz(MYZERO), ysz(MYZERO)  {}
	aabb2d(const apt_real _xmi, const apt_real _xmx, const apt_real _ymi, const apt_real _ymx) :
		xmi(_xmi), xmx(_xmx), ymi(_ymi), ymx(_ymx), xsz(_xmx-_xmi), ysz(_ymx-_ymi) {}
	void add_epsilon_guard();
	void scale();
	void blowup( const apt_real f );
	apt_real diag();
	void possibly_enlarge_me( p2dm1 const & p ); //##MK::candidate for template method
};

ostream& operator<<(ostream& in, aabb2d const & val);


struct aabb3d
{
	apt_real xmi;
	apt_real xmx;
	apt_real ymi;
	apt_real ymx;
	apt_real zmi;
	apt_real zmx;
	apt_real xsz;
	apt_real ysz;
	apt_real zsz;
	aabb3d() :
		xmi(RMX), xmx(RMI), ymi(RMX), ymx(RMI), zmi(RMX), zmx(RMI), xsz(MYZERO), ysz(MYZERO), zsz(MYZERO)  {}
	aabb3d(const apt_real _xmi, const apt_real _xmx, const apt_real _ymi, const apt_real _ymx, const apt_real _zmi, const apt_real _zmx) :
		xmi(_xmi), xmx(_xmx), ymi(_ymi), ymx(_ymx), zmi(_zmi), zmx(_zmx), xsz(_xmx-_xmi), ysz(_ymx-_ymi), zsz(_zmx-_zmi) {}

	void add_guard( const apt_real di );
	void add_epsilon_guard();
	void scale();
	void blowup( const apt_real f );
	apt_real diag();

	bool is_inside( p3d const & test );
	bool is_inside( p3dm1 const & test );
	bool is_inside( p3dmidx const & test );
	p3d center();
	apt_real circumscribed_sphere();
	apt_real volume();
	vector<apt_real> nexus_get_vertices();
	vector<apt_uint> nexus_get_faces_as_quads( const bool xdmf );

	//##MK::candidate for template member
	void possibly_enlarge_me( p3d const & p );
	void possibly_enlarge_me( p3dm1 const & p );
	void possibly_enlarge_me( p3dmidx const & p );
	void possibly_enlarge_me( p3dm5 const & p );
	void possibly_enlarge_me( tri3d const & trgl );
	void possibly_enlarge_me( aabb3d const & refbx );
};

ostream& operator<<(ostream& in, aabb3d const & val);


#endif
