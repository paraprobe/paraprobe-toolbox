/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_PrimsDiscrete.h"


/*
ostream& operator<<(ostream& in, p2i const & val)
{
	in << "xy " << val.x << ", " << val.y << "\n";
	return in;
}
*/


ostream& operator<<(ostream& in, p3i const & val)
{
	in << "xyz " << val.x << ", " << val.y << ", " << val.z << "\n";
	return in;
}


/*
ostream& operator<<(ostream& in, aabb2i const & val)
{
	in << "xmi/xmx " << val.xmi << ", " << val.xmx << "\n";
	in << "ymi/ymx " << val.ymi << ", " << val.ymx << "\n";
	return in;
}
*/


ostream& operator<<(ostream& in, aabb3i const & val)
{
	in << "xmi/xmx " << val.xmi << ", " << val.xmx << "\n";
	in << "ymi/ymx " << val.ymi << ", " << val.ymx << "\n";
	in << "zmi/zmx " << val.zmi << ", " << val.zmx << "\n";
	return in;
}


/*
ostream& operator<<(ostream& in, tri3i const & val)
{
	in << "v1v2v3 " << val.v1 << ", " << val.v2 << ", " << val.v3 << "\n";
	return in;
}
*/


ostream& operator<<(ostream& in, tri3u const & val)
{
	in << "v1v2v3 " << val.v1 << ", " << val.v2 << ", " << val.v3 << "\n";
	return in;
}


ostream& operator<<(ostream& in, quad3u const & val)
{
	in << "v1v2v3v4 " << val.v1 << ", " << val.v2 << ", " << val.v3 << ", " << val.v4 << "\n";
	return in;
}


voxelgrid::voxelgrid( p3d const & _org, const apt_real _halfx, const apt_real _halfy, const apt_real _halfz,
		const apt_int _nx, const apt_int _ny, const apt_int _nz )
{
	nx = 0;
	ny = 0;
	nz = 0;
	nxy = 0;
	nxyz = 0;
	dx = MYZERO;
	dy = MYZERO;
	dz = MYZERO;
	aabb = aabb3d();
	origin = p3d();
	SQRRmax = MYZERO;
	origin_offset = p3i();
	if ( _nx > 0 && _nx < ConfigShared::MaxVoxelsTwoPointStats &&
			_ny > 0 && _ny < ConfigShared::MaxVoxelsTwoPointStats &&
				_nz > 0 && _nz < ConfigShared::MaxVoxelsTwoPointStats ) {
		nx = (_nx % 2 != 0) ? _nx : _nx+1;
		ny = (_ny % 2 != 0) ? _ny : _ny+1;
		nz = (_nz % 2 != 0) ? _nz : _nz+1;
		nxy = nx*ny;
		nxyz = nx*ny*nz;
		origin = _org;
		//##MK::check _halfi large enough
		aabb = aabb3d( 	origin.x - 1.*_halfx, origin.x + 1.*_halfx,
						origin.y - 1.*_halfy, origin.y + 1.*_halfy,
						origin.z - 1.*_halfz, origin.z + 1.*_halfz  );
		aabb.scale();
		dx = aabb.xsz / nx;
		dy = aabb.ysz / ny;
		dz = aabb.zsz / nz;
		SQRRmax = SQR(min(min(_halfx, _halfy), _halfz));
		origin_offset = p3i( (_nx-1)/2, (_ny-1)/2, (_nz-1)/2 );
	}
	else {
		cerr << "Given grid dimensions either too small or individually larger " <<
				"than what the implementation currently supports (" << ConfigShared::MaxVoxelsTwoPointStats << ") !" << "\n";
	}
}


voxelgrid::voxelgrid( p3d const & _org, apt_int _ni, apt_real _di )
{
	nx = 0;
	ny = 0;
	nz = 0;
	nxy = 0;
	nxyz = 0;
	dx = MYZERO;
	dy = MYZERO;
	dz = MYZERO;
	aabb = aabb3d();
	origin = p3d();
	SQRRmax = MYZERO;
	origin_offset = p3i();
	if ( _ni > 0 && _ni < (ConfigShared::MaxVoxelsTwoPointStats-1)/2 && _di > EPSILON) {
		origin = _org;
		dx = _di;
		dy = _di;
		dz = _di;
		nx = 2*_ni + 1;
		ny = 2*_ni + 1;
		nz = 2*_ni + 1;
		nxy = nx*ny;
		nxyz = nx*ny*nz;
		apt_real halfi = ((apt_real) _ni)*_di;
		aabb = aabb3d( 	origin.x - 0.5*_di - halfi,
						origin.x + 0.5*_di + halfi,
						origin.y - 0.5*_di - halfi,
						origin.y + 0.5*_di + halfi,
						origin.z - 0.5*_di - halfi,
						origin.z + 0.5*_di + halfi  );
		aabb.scale();
		SQRRmax = SQR(0.5*_di + halfi);
		origin_offset = p3i( _ni, _ni, _ni );
	}
	else {
		cerr << "Given grid dimensions either too small or individually larger " <<
					"than what the implementation currently supports (" << ConfigShared::MaxVoxelsTwoPointStats << ") !" << "\n";
	}
}


voxelgrid::voxelgrid( p3d const & _org, apt_real _r, apt_real _di )
{
	nx = 0;
	ny = 0;
	nz = 0;
	nxy = 0;
	nxyz = 0;
	dx = MYZERO;
	dy = MYZERO;
	dz = MYZERO;
	aabb = aabb3d();
	origin = p3d();
	SQRRmax = MYZERO;
	origin_offset = p3i();
	if ( _di > EPSILON ) {
		origin = _org;
		dx = _di;
		dy = _di;
		dz = _di;
		apt_real halflen = ceil(_r / _di) + 1.0 + EPSILON;
		apt_int nhalf = (apt_int) halflen;
		if ( 2*nhalf + 1 < ConfigShared::MaxVoxelsTwoPointStats ) {
			nx = 2*nhalf + 1;
			ny = 2*nhalf + 1;
			nz = 2*nhalf + 1;
			nxy = nx*ny;
			nxyz = nx*ny*nz;
			apt_real halfi = ((apt_real) nhalf)*_di;
			aabb = aabb3d( 	origin.x - 0.5*_di - halfi,
							origin.x + 0.5*_di + halfi,
							origin.y - 0.5*_di - halfi,
							origin.y + 0.5*_di + halfi,
							origin.z - 0.5*_di - halfi,
							origin.z + 0.5*_di + halfi  );
			aabb.scale();
			SQRRmax = SQR(0.5*_di + halfi);
			origin_offset = p3i( nhalf, nhalf, nhalf );
		}
		else {
			cerr << "Total size of the computing grid is with " << 2*nhalf + 1 << " larger than what the implementation currently supports (" << ConfigShared::MaxVoxelsTwoPointStats << ") !" << "\n";
			nx = 0;
			ny = 0;
			nz = 0;
			nxy = 0;
			nxyz = 0;
			dx = MYZERO;
			dy = MYZERO;
			dz = MYZERO;
			aabb = aabb3d();
			origin = p3d();
			SQRRmax = MYZERO;
			origin_offset = p3i();
			return;
		}
	}
	else {
		cerr << "Voxel edge length is with " << _di << " too small than what the implementation requires !" << "\n";
		return;
	}
}


voxelgrid::voxelgrid( aabb3d const & refbx, const apt_real _di )
{
	nx = 0;
	ny = 0;
	nz = 0;
	nxy = 0;
	nxyz = 0;
	dx = MYZERO;
	dy = MYZERO;
	dz = MYZERO;
	aabb = aabb3d();
	origin = p3d();
	SQRRmax = MYZERO;
	origin_offset = p3i();
	//discretize volume axis-aligned bounding box refbx
	//into cubic voxel of edgelength di, make resulting volume larger eventually
	if ( _di > EPSILON ) {
		apt_real lenx = ceil((2.*EPSILON + (refbx.xmx - refbx.xmi)) / _di);
		apt_real leny = ceil((2.*EPSILON + (refbx.ymx - refbx.ymi)) / _di);
		apt_real lenz = ceil((2.*EPSILON + (refbx.zmx - refbx.zmi)) / _di);
		apt_int ix = (apt_int) lenx;
		apt_int iy = (apt_int) leny;
		apt_int iz = (apt_int) lenz;
		if ( ix < IMX && iy < IMX && iz < IMX && ix*iy < IMX && ix*iy*iz < IMX ) { //##MK::can overflow?
			dx = _di;
			dy = _di;
			dz = _di;
			nx = ix;
			ny = iy;
			nz = iz;
			nxy = nx*ny;
			nxyz = nx*ny*nz;
			//definition based on the barycenter of the box
			aabb = aabb3d( 	refbx.xmi + 0.5*(refbx.xmx-refbx.xmi) - (0.5*lenx*_di),
							refbx.xmi + 0.5*(refbx.xmx-refbx.xmi) + (0.5*lenx*_di),
							refbx.ymi + 0.5*(refbx.ymx-refbx.ymi) - (0.5*leny*_di),
							refbx.ymi + 0.5*(refbx.ymx-refbx.ymi) + (0.5*leny*_di),
							refbx.zmi + 0.5*(refbx.zmx-refbx.zmi) - (0.5*lenz*_di),
							refbx.zmi + 0.5*(refbx.zmx-refbx.zmi) + (0.5*lenz*_di) );
			aabb.scale();
			origin = p3d(aabb.xmi, aabb.ymi, aabb.zmi);
			origin_offset = p3i(0, 0, 0);
			SQRRmax = MYZERO;
		}
		else {
			cerr << "Resulting voxelgrid would be larger than what is currently supported by the implementation !" << "\n";
			return;
		}
	}
	else {
		cerr << "Voxel edge length is with " << _di << " too small than what the implementation requires !" << "\n";
		return;
	}
}


voxelgrid::voxelgrid( aabb3d const & _refbx, const apt_real _di, const apt_int _iguard )
{
	nx = 0;
	ny = 0;
	nz = 0;
	nxy = 0;
	nxyz = 0;
	dx = MYZERO;
	dy = MYZERO;
	dz = MYZERO;
	aabb = aabb3d();
	origin = p3d();
	SQRRmax = MYZERO;
	origin_offset = p3i();
	//discretize volume axis-aligned bounding box refbx
	//into cubic voxel of edgelength di, make resulting volume larger eventually
	if ( _di > EPSILON ) {
		apt_real lenx = ceil(( ((apt_real) _iguard)*_di + (_refbx.xmx - _refbx.xmi) + ((apt_real) _iguard)*_di ) / _di);
		apt_real leny = ceil(( ((apt_real) _iguard)*_di + (_refbx.ymx - _refbx.ymi) + ((apt_real) _iguard)*_di ) / _di);
		apt_real lenz = ceil(( ((apt_real) _iguard)*_di + (_refbx.zmx - _refbx.zmi) + ((apt_real) _iguard)*_di ) / _di);
		apt_int ix = (apt_int) lenx;
		apt_int iy = (apt_int) leny;
		apt_int iz = (apt_int) lenz;
		if ( ix < IMX && iy < IMX && iz < IMX && ix*iy < IMX && ix*iy*iz < IMX ) { //##MK::can overflow?
			dx = _di;
			dy = _di;
			dz = _di;
			nx = ix;
			ny = iy;
			nz = iz;
			nxy = nx*ny;
			nxyz = nx*ny*nz;
			origin = p3d( 	_refbx.xmi - ((apt_real) _iguard * _di),
							_refbx.ymi - ((apt_real) _iguard * _di),
							_refbx.zmi - ((apt_real) _iguard * _di)  );
			origin_offset = p3i(0, 0, 0);
			aabb = aabb3d( 	_refbx.xmi - ((apt_real) _iguard * _di),
							_refbx.xmi - ((apt_real) _iguard * _di) + ((apt_real) ix * _di),
							_refbx.ymi - ((apt_real) _iguard * _di),
							_refbx.ymi - ((apt_real) _iguard * _di) + ((apt_real) iy * _di),
							_refbx.zmi - ((apt_real) _iguard * _di),
							_refbx.zmi - ((apt_real) _iguard * _di) + ((apt_real) iz * _di)   );
			aabb.scale();
			SQRRmax = MYZERO;
		}
		else {
			cerr << "Resulting voxelgrid would be larger than what is currently supported by the implementation !" << "\n";
			return;
		}
	}
	else {
		cerr << "Voxel edge length is with " << _di << " too small than what the implementation requires !" << "\n";
		return;
	}
}


apt_int voxelgrid::where_x( const apt_real x )
{
	if ( x >= this->aabb.xmi ) {
		if ( x <= this->aabb.xmx ) {
			apt_real lenx = ((x - this->aabb.xmi)/this->aabb.xsz) * (apt_real) this->nx;
			if ( lenx >= MYZERO ) {
				/*
				apt_int ilenx = (apt_int) (lenx + 0.5);
				*/
				apt_int ilenx = static_cast<apt_int>(lenx);
				if ( ilenx < this->nx )
					return ilenx;
				else
					return IMX;
			}
			else {
				return IMI;
				/*
				apt_int ilenx = (apt_int) (lenx - 0.5);
				if ( ilenx >= 0 )
					return ilenx;
				else
					return IMI;
				*/
			}
			/*
			apt_real lenx = floor((x - this->aabb.xmi)/this->aabb.xsz * (apt_real) this->nx);
			apt_int ilenx = (apt_int) lenx;
			if ( ilenx >= 0) {
				if ( ilenx < this->nx ) {
					return ilenx;
				}
				else {
					return IMX;
				}
			}
			else {
				return IMI;
			}
			*/
			//return (apt_int) lenx;
		}
		else {
			return IMX;
		}
	}
	else {
		return IMI;
	}
}


apt_int voxelgrid::where_y( const apt_real y )
{
	if ( y >= this->aabb.ymi ) {
		if ( y <= this->aabb.ymx ) {
			apt_real leny = ((y - this->aabb.ymi)/this->aabb.ysz) * (apt_real) this->ny;
			if ( leny >= MYZERO ) {
				/*
				apt_int ileny = (apt_int) (leny + 0.5);
				*/
				apt_int ileny = static_cast<apt_int>(leny);
				if ( ileny < this->ny )
					return ileny;
				else
					return IMX;
			}
			else {
				return IMI;
				/*
				apt_int ileny = (apt_int) (leny - 0.5);
				if ( ileny >= 0 )
					return ileny;
				else
					return IMI;
				*/
			}
			/*
			apt_real leny = floor((y - this->aabb.ymi)/this->aabb.ysz * (apt_real) this->ny);
			apt_int ileny = (apt_int) leny;
			if ( ileny >= 0) {
				if ( ileny < this->ny ) {
					return ileny;
				}
				else {
					return IMX;
				}
			}
			else {
				return IMI;
			}
			*/
			//return (apt_int) leny;
		}
		else {
			return IMX;
		}
	}
	else {
		return IMI;
	}
}


apt_int voxelgrid::where_z( const apt_real z )
{
	if ( z >= this->aabb.zmi ) {
		if ( z <= this->aabb.zmx ) {
			apt_real lenz = ((z - this->aabb.zmi)/this->aabb.zsz) * (apt_real) this->nz;
			if ( lenz >= MYZERO ) {
				/*
				apt_int ilenz = (apt_int) (lenz + 0.5);
				*/
				apt_int ilenz = static_cast<apt_int>(lenz);
				if ( ilenz < this->nz )
					return ilenz;
				else
					return IMX;
			}
			else {
				return IMI;
				/*
				apt_int ilenz = (apt_int) (lenz - 0.5);
				if ( ilenz >= 0 )
					return ilenz;
				else
					return IMI;
				*/
			}
			/*
			apt_real lenz = floor((z - this->aabb.zmi)/this->aabb.zsz * (apt_real) this->nz);
			apt_int ilenz = (apt_int) lenz;
			if ( ilenz >= 0) {
				if ( ilenz < this->nz ) {
					return ilenz;
				}
				else {
					return IMX;
				}
			}
			else {
				return IMI;
			}
			*/
			//return (apt_int) lenz;
		}
		else {
			return IMX;
		}
	}
	else {
		return IMI;
	}
}


apt_int voxelgrid::where_x_no_checks_vxl( const apt_real x )
{
	//assume x >= this->aabb.xmi and x <= this->aabb.xmx
	apt_real lenx = ((x - this->aabb.xmi)/this->aabb.xsz) * (apt_real) this->nx;
	if ( lenx >= MYZERO ) {
		return ((apt_int) (lenx + 0.5));
	}
	return ((apt_int) (lenx - 0.5));
}


apt_int voxelgrid::where_y_no_checks_vxl( const apt_real y )
{
	//assume y >= this->aabb.ymi and y <= this->aabb.ymx
	apt_real leny = ((y - this->aabb.ymi)/this->aabb.ysz) * (apt_real) this->ny;
	if ( leny >= MYZERO ) {
		return ((apt_int) (leny + 0.5));
	}
	return ((apt_int) (leny - 0.5));
}


apt_int voxelgrid::where_z_no_checks_vxl( const apt_real z )
{
	//assume z >= this->aabb.zmi and z <= this->aabb.zmx
	apt_real lenz = ((z - this->aabb.zmi)/this->aabb.zsz) * (apt_real) this->nz;
	if ( lenz >= MYZERO ) {
		return ((apt_int) (lenz + 0.5));
	}
	return ((apt_int) (lenz - 0.5));
}


apt_real voxelgrid::where_x_no_checks_pos( const apt_real x )
{
	//assume x >= this->aabb.xmi == true and x <= this->aabb.xmx == true
	apt_real lenx = ((x - this->aabb.xmi)/this->aabb.xsz) * (apt_real) this->nx;
	return (lenx - floor(lenx));
}


apt_real voxelgrid::where_y_no_checks_pos( const apt_real y )
{
	//assume y >= this->aabb.ymi == true and y <= this->aabb.ymx == true
	apt_real leny = ((y - this->aabb.ymi)/this->aabb.ysz) * (apt_real) this->nx;
	return (leny - floor(leny));
}


apt_real voxelgrid::where_z_no_checks_pos( const apt_real z )
{
	//assume z >= this->aabb.zmi == true and z <= this->aabb.zmx == true
	apt_real lenz = ((z - this->aabb.zmi)/this->aabb.zsz) * (apt_real) this->nz;
	return (lenz - floor(lenz));
}


pair<apt_int, double> voxelgrid::where_x_no_checks( const apt_real x )
{
	//assume x >= this->aabb.xmi == true and x <= this->aabb.xmx == true
	apt_real lenx = ((x - this->aabb.xmi)/this->aabb.xsz) * (apt_real) this->nx;
	return pair<apt_int, double>(
			(lenx >= MYZERO ) ? (apt_int) (lenx + 0.5) : (apt_int) (lenx - 0.5),
			(double) (lenx - trunc(lenx)) ); //(lenx - floor(lenx)) );
}


pair<apt_int, double> voxelgrid::where_y_no_checks( const apt_real y )
{
	//assume y >= this->aabb.ymi == true and y <= this->aabb.ymx == true
	apt_real leny = ((y - this->aabb.ymi)/this->aabb.ysz) * (apt_real) this->ny;
	return pair<apt_int, double>(
			(leny >= MYZERO ) ? (apt_int) (leny + 0.5) : (apt_int) (leny - 0.5),
			(double) (leny - trunc(leny)) ); //(leny - floor(leny)) );
}


pair<apt_int, double> voxelgrid::where_z_no_checks( const apt_real z )
{
	//assume z >= this->aabb.zmi == true and z <= this->aabb.zmx == true
	apt_real lenz = ((z - this->aabb.zmi)/this->aabb.zsz) * (apt_real) this->nz;
	return pair<apt_int, double>(
			(lenz >= MYZERO ) ? (apt_int) (lenz + 0.5) : (apt_int) (lenz - 0.5),
			(double) (lenz - trunc(lenz)) ); //(lenz - floor(lenz)) );
}


double voxelgrid::where_x_no_checks_f64( const apt_real x )
{
	//assume x >= this->aabb.xmi == true and x <= this->aabb.xmx == true
	apt_real lenx = ((x - this->aabb.xmi)/this->aabb.xsz) * (apt_real) this->nx;
	return (lenx - trunc(lenx) ); //(lenx - floor(lenx)) );
}


double voxelgrid::where_y_no_checks_f64( const apt_real y )
{
	//assume y >= this->aabb.ymi == true and y <= this->aabb.ymx == true
	apt_real leny = ((y - this->aabb.ymi)/this->aabb.ysz) * (apt_real) this->ny;
	return (leny - trunc(leny) ); //(leny - floor(leny)) );
}


double voxelgrid::where_z_no_checks_f64( const apt_real z )
{
	//assume z >= this->aabb.zmi == true and z <= this->aabb.zmx == true
	apt_real lenz = ((z - this->aabb.zmi)/this->aabb.zsz) * (apt_real) this->nz;
	return (double) (lenz - trunc(lenz) ); //(lenz - floor(lenz)) );
}


apt_real voxelgrid::where_x( const apt_int ix )
{
	//voxel coordinates to barycenter point continuum coordinate
	if ( ix >= 0 ) {
		if ( ix < this->nx ) {
			return this->aabb.xmi + (0.5 + (apt_real) ix) * this->dx;
		}
		else {
			return RMX;
		}
	}
	else {
		return RMI;
	}
}


apt_real voxelgrid::where_y( const apt_int iy ) {
	if ( iy >= 0 ) {
		if ( iy < this->ny ) {
			return this->aabb.ymi + (0.5 + (apt_real) iy) * this->dy;
		}
		else {
			return RMX;
		}
	}
	else {
		return RMI;
	}
}


apt_real voxelgrid::where_z( const apt_int iz ) {
	if ( iz >= 0 ) {
		if ( iz < this->nz ) {
			return this->aabb.zmi + (0.5 + (apt_real) iz) * this->dz;
		}
		else {
			return RMX;
		}
	}
	else {
		return RMI;
	}
}


size_t voxelgrid::where_xyz( p3d const & p )
{
	//continuum point position to rectangular transfer function voxel containing that point
	apt_int ix = this->where_x( p.x );
	if ( ix != IMI && ix != IMX ) {
		apt_int iy = this->where_y( p.y );
		if ( iy != IMI && iy != IMX ) {
			apt_int iz = this->where_z( p.z );
			if ( iz != IMI && iz != IMX) {
				return (size_t) ix + (size_t) (iy*this->nx) + (size_t) (iz*this->nxy);
			}
		}
	}
	return -1;
}


vector<apt_int> voxelgrid::where_xyz_no_checks( const size_t xyz )
{
	vector<apt_int> retval;

	size_t nnxy = (size_t) this->nxy;
	size_t nnx = (size_t) this->nx;
	size_t z = xyz / nnxy;
	size_t rem = xyz - z*nnxy;
	size_t y = rem / nnx;
	size_t x = rem - y*nnx;

	if ( x < IMX && y < IMX && z < IMX ) {
		retval = { (apt_int) x, (apt_int) y, (apt_int) z };
	}
	else {
		cerr << "where_xyz_no_checks overflows" << "\n";
	}

	return retval;
}


apt_int voxelgrid::where_z_no_checks( const size_t xyz )
{
	size_t nnxy = (size_t) this->nxy;
	size_t z = xyz / nnxy;

	if ( z < IMX ) {
		return (apt_int) z;
	}
	//else {
	cerr << "where_xyz_no_checks overflows" << "\n";
	return -1;
	//}
}


voxelgrid::voxelgrid( voxelgrid const & vxlgrd )
{
	this->nx = vxlgrd.nx;
	this->ny = vxlgrd.ny;
	this->nz = vxlgrd.nz;
	this->nxy = vxlgrd.nxy;
	this->nxyz = vxlgrd.nxyz;
	this->dx = vxlgrd.dx;
	this->dy = vxlgrd.dy;
	this->dz = vxlgrd.dz;
	this->aabb = vxlgrd.aabb;
	this->origin = vxlgrd.origin;
	this->SQRRmax = vxlgrd.SQRRmax;
	this->origin_offset = vxlgrd.origin_offset;
}


ostream& operator<<(ostream& in, voxelgrid const & val)
{
	in << "nx/ny/nz/nxy/nxyz " << val.nx << ", " << val.ny << ", " << val.nz << ", " << val.nxy << ", " << val.nxyz << "\n";
	in << "origin xyz " << val.origin << "\n";
	in << "origin offset " << val.origin_offset << "\n";
	in << "aabb " << val.aabb << "\n";
	in << "SQRRmax " << val.SQRRmax << "\n";
	return in;
}

