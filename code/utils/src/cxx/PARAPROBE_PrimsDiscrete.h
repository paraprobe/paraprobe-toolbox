/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_PRIMSDISCRETE_H__
#define __PARAPROBE_UTILS_PRIMSDISCRETE_H__

#include "PARAPROBE_PrimsContinuum.h"

/*
struct p2i
{
	//a pixel in 2d space
	apt_int x;
	apt_int y;
	p2i() : x(0), y(0) {}
	p2i( const apt_int _x, const apt_int _y ) : x(_x), y(_y) {}
};

ostream& operator<<(ostream& in, p2i const & val);
*/


struct p3i
{
	//a voxel in 3d space
	apt_int x;
	apt_int y;
	apt_int z;
	p3i() : x(0), y(0), z(0) {}
	p3i( const apt_int _x, const apt_int _y, const apt_int _z) :
		x(_x), y(_y), z(_z) {}
};

ostream& operator<<(ostream& in, p3i const & val);


/*
struct aabb2i
{
	//a pixelated axis-aligned rectangle
	apt_int xmi;
	apt_int xmx;
	apt_int ymi;
	apt_int ymx;
	aabb2i() : xmi(IMX), xmx(IMI), ymi(IMX), ymx(IMI) {}
	aabb2i( const apt_int _xmi, const apt_int _xmx, const apt_int _ymi, const apt_int _ymx ) :
		xmi(_xmi), xmx(_xmx), ymi(_ymi), ymx(_ymx) {}
	//##MK::inclusion functions
};

ostream& operator<<(ostream& in, aabb2i const & val);
*/


struct aabb3i
{
	//a voxelized axis-aligned rectangle
	apt_int xmi;
	apt_int xmx;
	apt_int ymi;
	apt_int ymx;
	apt_int zmi;
	apt_int zmx;
	aabb3i() : xmi(IMX), xmx(IMI), ymi(IMX), ymx(IMI), zmi(IMX), zmx(IMI) {}
	aabb3i( const apt_int _xmi, const apt_int _xmx, const apt_int _ymi, const apt_int _ymx,
			const apt_int _zmi, const apt_int _zmx) : xmi(_xmi), xmx(_xmx),
					ymi(_ymi), ymx(_ymx), zmi(_zmi), zmx(_zmx) {}
	//##MK::inclusion functions
};

ostream& operator<<(ostream& in, aabb3i const & val);


/*
struct tri3i
{
	//a pixelated triangle
	apt_int v1;
	apt_int v2;
	apt_int v3;
	tri3i() : v1(0), v2(0), v3(0) {}
	tri3i( const apt_int _v1, const apt_int _v2, const apt_int _v3 ) :
		v1(_v1), v2(_v2), v3(_v3) {}
};

ostream& operator<<(ostream& in, tri3i const & val);
*/


struct tri3u
{
	apt_uint v1;
	apt_uint v2;
	apt_uint v3;
	tri3u() : v1(0), v2(0), v3(0) {}
	tri3u( const apt_uint _v1, const apt_uint _v2, const apt_uint _v3 ) :
		v1(_v1), v2(_v2), v3(_v3) {}
};

ostream& operator<<(ostream& in, tri3u const & val);


struct quad3u
{
	apt_uint v1;
	apt_uint v2;
	apt_uint v3;
	apt_uint v4;
	quad3u() : v1(0), v2(0), v3(0), v4(0) {}
	quad3u( const apt_uint _v1, const apt_uint _v2, const apt_uint _v3 , const apt_uint _v4 ) :
		v1(_v1), v2(_v2), v3(_v3), v4(_v4) {}
};

ostream& operator<<(ostream& in, quad3u const & val);


struct voxelgrid
{
	apt_int nx;
	apt_int ny;
	apt_int nz;
	apt_int nxy;
	apt_int nxyz;
	apt_real dx;
	apt_real dy;
	apt_real dz;
	aabb3d aabb;
	p3d origin;

	apt_real SQRRmax;
	p3i origin_offset; //min edge of bin cnts[0] is at support.box.imi which is offset by (nx-1)/2

	voxelgrid() : nx(0), ny(0), nz(0), nxy(0*0), nxyz(0*0*0),
			dx(MYZERO), dy(MYZERO), dz(MYZERO), aabb(aabb3d()), origin(p3d()),
			SQRRmax(MYZERO), origin_offset(p3i()) {}
	//build a box about _org with at least halfi in each direction build of ni cuboidal voxels, _org not necessarily in the center
	voxelgrid( p3d const & _org, const apt_real _halfx, const apt_real _halfy,
			const apt_real halfz, const apt_int _nx, const apt_int _ny, const apt_int _nz );
	//build a box so that _org is the barycenter of a cube of cubic voxels (di edge length) with at least ni voxel along each half
	voxelgrid( p3d const & _org, apt_int _ni, apt_real _di );
	//build a box so that _org is the barycenter of a cube of cubic voxels (di edge length) with at least r+di covered in each direction
	voxelgrid( p3d const & _org, apt_real _r, apt_real _di );
	//build a discretization of a volume bounded by an axis-aligned bounding box and discretize this volume using cubic voxel of edgelength di
	voxelgrid( aabb3d const & refbx, const apt_real _di );
	//build a box with iguard in halo on either side!,  discretized into cubic voxels (di edge length)
	voxelgrid( aabb3d const & _refbx, const apt_real _di, const apt_int _iguard );
	apt_int where_x( const apt_real x );
	apt_int where_y( const apt_real y );
	apt_int where_z( const apt_real z );
	apt_int where_x_no_checks_vxl( const apt_real x );
	apt_int where_y_no_checks_vxl( const apt_real y );
	apt_int where_z_no_checks_vxl( const apt_real z );
	apt_real where_x_no_checks_pos( const apt_real x );
	apt_real where_y_no_checks_pos( const apt_real y );
	apt_real where_z_no_checks_pos( const apt_real z );
	pair<apt_int, double> where_x_no_checks( const apt_real x );
	pair<apt_int, double> where_y_no_checks( const apt_real y );
	pair<apt_int, double> where_z_no_checks( const apt_real z );
	double where_x_no_checks_f64( const apt_real x );
	double where_y_no_checks_f64( const apt_real y );
	double where_z_no_checks_f64( const apt_real z );
	apt_real where_x( const apt_int ix );
	apt_real where_y( const apt_int iy );
	apt_real where_z( const apt_int iz );
	size_t where_xyz( p3d const & p );
	vector<apt_int> where_xyz_no_checks( const size_t xyz );
	apt_int where_z_no_checks( const size_t xyz );
	voxelgrid( voxelgrid const & vxlgrd );
};

ostream& operator<<(ostream& in, voxelgrid const & val);


#endif
