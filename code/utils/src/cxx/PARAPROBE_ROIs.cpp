/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ROIs.h"


void roi_rotated::init_proxigrams()
{
	for( auto it = this->itypes_whitelist.begin(); it != this->itypes_whitelist.end(); it++ ) {
		vector<apt_real>* cdf = NULL;
		cdf = new vector<apt_real>();
		this->ityp_proxigrams.insert( pair<unsigned char, vector<apt_real>*>( *it, cdf ) );
	}
	for( auto jt = this->elemtypes_whitelist.begin(); jt != this->elemtypes_whitelist.end(); jt++ ) {
		vector<apt_real>* cdf = NULL;
		cdf = new vector<apt_real>();
		this->elemtyp_proxigrams.insert( pair<unsigned char, vector<apt_real>*>( *jt, cdf ) );
	}

	this->ityp_global_proxigram = vector<proxigram_support>();
	this->elemtyp_global_proxigram = vector<proxigram_support>();
	this->isotope_global_proxigram = vector<proxigram_support>();
}


void roi_rotated::update_proxigrams()
{
	for( auto it = this->ityp_proxigrams.begin(); it != this->ityp_proxigrams.end(); it++ ) {
		if ( it->second != NULL ) {
			std::stable_sort( it->second->begin(), it->second->end() ); //stable sorting in ascending order, distances are signed !
		}
	}
	for( auto jt = this->elemtyp_proxigrams.begin(); jt != this->elemtyp_proxigrams.end(); jt++ ) {
		if ( jt->second != NULL ) {
			std::stable_sort( jt->second->begin(), jt->second->end() ); //stable sorting in ascending order, distances are signed !
		}
	}
}


void roi_rotated::compute_joint_proxigrams()
{
	for( auto jt = this->elemtyp_proxigrams.begin(); jt != this->elemtyp_proxigrams.end(); jt++ ) {
		if ( jt->second != NULL ) {
			unsigned short element_without_isotope_info = isotope_hash( jt->first, 0x00 );
			for ( auto kt = jt->second->begin(); kt != jt->second->end(); kt++ ) {
				this->elemtyp_global_proxigram.push_back( proxigram_support(*kt, element_without_isotope_info) );
			}
		}
	}

	std::stable_sort( this->elemtyp_global_proxigram.begin(), this->elemtyp_global_proxigram.end(), SortAscProxigramSupport );
	//stable sorting in ascending order, distances are signed !
}


void roi_rotated::clear_proxigrams()
{
	for( auto it = this->ityp_proxigrams.begin(); it != this->ityp_proxigrams.end(); it++  ) {
		if ( it->second != NULL ) {
			delete it->second; it->second = NULL;
		}
	}
	for( auto jt = this->elemtyp_proxigrams.begin(); jt != this->elemtyp_proxigrams.end(); jt++  ) {
		if ( jt->second != NULL ) {
			delete jt->second; jt->second = NULL;
		}
	}

	this->ityp_global_proxigram = vector<proxigram_support>();
	this->elemtyp_global_proxigram = vector<proxigram_support>();
	this->isotope_global_proxigram = vector<proxigram_support>();
}


roi_rotated_cuboid::roi_rotated_cuboid( p3d const & _low, p3d const & _high )
{
	this->barycenter = p3d( _low.x + 0.5*(_high.x - _low.x),
							_low.y + 0.5*(_high.y - _low.y),
							_low.z + 0.5*(_high.z - _low.z)  );
	this->Lx = _high.x - _low.x;
	this->Ly = _high.y - _low.y;
	this->Lz = _high.z - _low.z;
	p3d v1 = p3d( -0.5*this->Lx, -0.5*this->Ly, -0.5*this->Lz );
	p3d v2 = p3d( -0.5*this->Lx, +0.5*this->Ly, -0.5*this->Lz );
	p3d v3 = p3d( +0.5*this->Lx, +0.5*this->Ly, -0.5*this->Lz );
	p3d v4 = p3d( +0.5*this->Lx, -0.5*this->Ly, -0.5*this->Lz );

	p3d v5 = p3d( -0.5*this->Lx, -0.5*this->Ly, +0.5*this->Lz );
	p3d v6 = p3d( -0.5*this->Lx, +0.5*this->Ly, +0.5*this->Lz );
	p3d v7 = p3d( +0.5*this->Lx, +0.5*this->Ly, +0.5*this->Lz );
	p3d v8 = p3d( +0.5*this->Lx, -0.5*this->Ly, +0.5*this->Lz );

	this->Ractive = t3x3( 	MYONE, 	MYZERO, MYZERO,
							MYZERO, MYONE, 	MYZERO,
							MYZERO, MYZERO, MYONE );

	this->ori = p3d(MYZERO, MYZERO, MYZERO); //##MK::NOT DEFINED

	//identify rotation does not require appyling a rotation
	this->p1 = v1.active_rotation_relocate( this->Ractive ); //this->Ractive.left_multiply( v1 );
	this->p2 = v2.active_rotation_relocate( this->Ractive );
	this->p3 = v3.active_rotation_relocate( this->Ractive );
	this->p4 = v4.active_rotation_relocate( this->Ractive );
	this->p5 = v5.active_rotation_relocate( this->Ractive );
	this->p6 = v6.active_rotation_relocate( this->Ractive );
	this->p7 = v7.active_rotation_relocate( this->Ractive );
	this->p8 = v8.active_rotation_relocate( this->Ractive );

	//shift rotated cuboid to final position
	this->p1.active_shift_relocate( this->barycenter );
	this->p2.active_shift_relocate( this->barycenter );
	this->p3.active_shift_relocate( this->barycenter );
	this->p4.active_shift_relocate( this->barycenter );
	this->p5.active_shift_relocate( this->barycenter );
	this->p6.active_shift_relocate( this->barycenter );
	this->p7.active_shift_relocate( this->barycenter );
	this->p8.active_shift_relocate( this->barycenter );
}


roi_rotated_cuboid::roi_rotated_cuboid( p3d const & _barycenter, const apt_real _lx, const apt_real _ly, const apt_real _lz, t3x3 const & _rmtrx )
{
	this->barycenter = _barycenter;
	//apt_real Lshort = min(min(_lx, _ly), _lz);
	//apt_real Llong =  max(max(_lx, _ly), _lz);
	vector<apt_real> tmp = { _lx, _ly, _lz };
	//by definition longest axis along PARAPROBE_ZAXIS when unrotated
	this->Lx = tmp[0]; //Lshort
	this->Ly = tmp[1]; //Lshort
	this->Lz = tmp[2]; //Llong, makes special case of quadratic prism

	//##MK::https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
	p3d v1 = p3d( -0.5*this->Lx, -0.5*this->Ly, -0.5*this->Lz );
	p3d v2 = p3d( -0.5*this->Lx, +0.5*this->Ly, -0.5*this->Lz );
	p3d v3 = p3d( +0.5*this->Lx, +0.5*this->Ly, -0.5*this->Lz );
	p3d v4 = p3d( +0.5*this->Lx, -0.5*this->Ly, -0.5*this->Lz );

	p3d v5 = p3d( -0.5*this->Lx, -0.5*this->Ly, +0.5*this->Lz );
	p3d v6 = p3d( -0.5*this->Lx, +0.5*this->Ly, +0.5*this->Lz );
	p3d v7 = p3d( +0.5*this->Lx, +0.5*this->Ly, +0.5*this->Lz );
	p3d v8 = p3d( +0.5*this->Lx, -0.5*this->Ly, +0.5*this->Lz );

	this->Ractive = _rmtrx;

	this->ori = p3d(MYZERO, MYZERO, MYZERO); //##MK::NOT DEFINED

	this->p1 = v1.active_rotation_relocate( this->Ractive ); //this->Ractive.left_multiply( v1 );
	this->p2 = v2.active_rotation_relocate( this->Ractive );
	this->p3 = v3.active_rotation_relocate( this->Ractive );
	this->p4 = v4.active_rotation_relocate( this->Ractive );
	this->p5 = v5.active_rotation_relocate( this->Ractive );
	this->p6 = v6.active_rotation_relocate( this->Ractive );
	this->p7 = v7.active_rotation_relocate( this->Ractive );
	this->p8 = v8.active_rotation_relocate( this->Ractive );

	//shift rotated cuboid to final position
	this->p1.active_shift_relocate( this->barycenter );
	this->p2.active_shift_relocate( this->barycenter );
	this->p3.active_shift_relocate( this->barycenter );
	this->p4.active_shift_relocate( this->barycenter );
	this->p5.active_shift_relocate( this->barycenter );
	this->p6.active_shift_relocate( this->barycenter );
	this->p7.active_shift_relocate( this->barycenter );
	this->p8.active_shift_relocate( this->barycenter );
}


roi_rotated_cuboid::roi_rotated_cuboid( p3d const & _barycenter, const apt_real _lx, const apt_real _ly, const apt_real _lz, p3d const & _ounrm )
{
	this->barycenter = _barycenter;
	vector<apt_real> tmp = { _lx, _ly, _lz };
	//by definition longest axis parallel to _ounrm
	this->Lx = tmp[0]; //Lshort
	this->Ly = tmp[1]; //Lshort
	this->Lz = tmp[2]; //Llong, makes special case of quadratic prism

	//##MK::https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
	//start building an origin-centered cuboid
	p3d v1 = p3d( -0.5*this->Lx, -0.5*this->Ly, -0.5*this->Lz );
	p3d v2 = p3d( +0.5*this->Lx, -0.5*this->Ly, -0.5*this->Lz );
	p3d v3 = p3d( -0.5*this->Lx, +0.5*this->Ly, -0.5*this->Lz );
	p3d v4 = p3d( +0.5*this->Lx, +0.5*this->Ly, -0.5*this->Lz );

	p3d v5 = p3d( -0.5*this->Lx, -0.5*this->Ly, +0.5*this->Lz );
	p3d v6 = p3d( +0.5*this->Lx, -0.5*this->Ly, +0.5*this->Lz );
	p3d v7 = p3d( -0.5*this->Lx, +0.5*this->Ly, +0.5*this->Lz );
	p3d v8 = p3d( +0.5*this->Lx, +0.5*this->Ly, +0.5*this->Lz );

	//do not rotate, ##MK::but if antipodal and at some point using gradients from underlying gradient field
	//need to swop bottom and upper part, i.e. swop antipodal
	this->p1 = v1;
	this->p2 = v2;
	this->p3 = v3;
	this->p4 = v4;
	this->p5 = v5;
	this->p6 = v6;
	this->p7 = v7;
	this->p8 = v8;

	//eventually we need to rotate
	//find a matrix to rotate vector [001] on ounrm
	p3d f = p3d( MYZERO, MYZERO, MYONE );

	//better normalize ounrm
	v3d tt = v3d(_ounrm.x,_ounrm.y,_ounrm.z);
	tt.normalize();
	p3d t = p3d(tt.u, tt.v, tt.w); //_ounrm
	apt_real lenf = sqrt(SQR(f.x)+SQR(f.y)+SQR(f.z));
	apt_real lent = sqrt(SQR(t.x)+SQR(t.y)+SQR(t.z));
	if ( (lenf * lent) >= EPSILON ) {
		apt_real arg = dot(f,t) / (lenf*lent);
		if ( fabs(arg) < (MYONE - EPSILON) ) { //angle between is f and t is large that f,t are not (anti)parallel to one another
			p3d v = cross( f, t );
			//p3d u = v; //|v| by construction is MYONE, cross product of unit vectors
			apt_real c = dot( f, t);
			apt_real h = (MYONE - c) / (MYONE - SQR(c));
			this->Ractive = t3x3( 	c+h*SQR(v.x),
									h*v.x*v.y - v.z,
									h*v.x*v.z + v.y,
									h*v.x*v.y + v.z,
									c+h*SQR(v.y),
									h*v.y*v.z - v.x,
									h*v.x*v.z - v.y,
									h*v.y*v.z + v.x,
									c + h*SQR(v.z) );
			this->ori = t; //_ounrm;
			this->p1 = v1.active_rotation_relocate( this->Ractive ); //this->Ractive.left_multiply( v1 );
			this->p2 = v2.active_rotation_relocate( this->Ractive );
			this->p3 = v3.active_rotation_relocate( this->Ractive );
			this->p4 = v4.active_rotation_relocate( this->Ractive );
			this->p5 = v5.active_rotation_relocate( this->Ractive );
			this->p6 = v6.active_rotation_relocate( this->Ractive );
			this->p7 = v7.active_rotation_relocate( this->Ractive );
			this->p8 = v8.active_rotation_relocate( this->Ractive );
		}
	}

	//shift rotated cuboid to final position
	this->p1.active_shift_relocate( this->barycenter );
	this->p2.active_shift_relocate( this->barycenter );
	this->p3.active_shift_relocate( this->barycenter );
	this->p4.active_shift_relocate( this->barycenter );
	this->p5.active_shift_relocate( this->barycenter );
	this->p6.active_shift_relocate( this->barycenter );
	this->p7.active_shift_relocate( this->barycenter );
	this->p8.active_shift_relocate( this->barycenter );
}


roi_rotated_cuboid::roi_rotated_cuboid( vector<p3d> const & _cornerpoints )
{
	if ( _cornerpoints.size() == 8 ) {
		p3d bary = p3d( 0., 0., 0. );
		for( size_t i = 0; i < 8; i++ ) {
			bary.x += _cornerpoints[i].x;
			bary.y += _cornerpoints[i].y;
			bary.z += _cornerpoints[i].z;
		}
		this->barycenter = p3d( bary.x/8., bary.y/8., bary.z/8. );
		this->Lx = MYZERO; //##MK::not defined
		this->Ly = MYZERO;
		this->Lz = MYZERO;
		this->Ractive = t3x3(); //##MK::not defined
		this->ori = p3d(); //##MK::not defined
		this->p1 = _cornerpoints[0];
		this->p2 = _cornerpoints[1];
		this->p3 = _cornerpoints[2];
		this->p4 = _cornerpoints[3];
		this->p5 = _cornerpoints[4];
		this->p6 = _cornerpoints[5];
		this->p7 = _cornerpoints[6];
		this->p8 = _cornerpoints[7];
	}
	else {
		cerr << "Initialization error for roi_rotated_cuboid, _cornerpoints.size() != 8 !" << "\n";
	}
}


aabb3d roi_rotated_cuboid::get_aabb() const
{
	aabb3d box = aabb3d(
			fmin(fmin(fmin(fmin(fmin(fmin(fmin(this->p1.x, this->p2.x), this->p3.x), this->p4.x), this->p5.x), this->p6.x), this->p7.x ), this->p8.x),
			fmax(fmax(fmax(fmax(fmax(fmax(fmax(this->p1.x, this->p2.x), this->p3.x), this->p4.x), this->p5.x), this->p6.x), this->p7.x ), this->p8.x),
			fmin(fmin(fmin(fmin(fmin(fmin(fmin(this->p1.y, this->p2.y), this->p3.y), this->p4.y), this->p5.y), this->p6.y), this->p7.y ), this->p8.y),
			fmax(fmax(fmax(fmax(fmax(fmax(fmax(this->p1.y, this->p2.y), this->p3.y), this->p4.y), this->p5.y), this->p6.y), this->p7.y ), this->p8.y),
			fmin(fmin(fmin(fmin(fmin(fmin(fmin(this->p1.z, this->p2.z), this->p3.z), this->p4.z), this->p5.z), this->p6.z), this->p7.z ), this->p8.z),
			fmax(fmax(fmax(fmax(fmax(fmax(fmax(this->p1.z, this->p2.z), this->p3.z), this->p4.z), this->p5.z), this->p6.z), this->p7.z ), this->p8.z)  );
	box.add_epsilon_guard();
	box.scale();
	return box;
}


bool roi_rotated_cuboid::is_inside( p3d const & p ) const
{
	//based on https://math.stackexchange.com/questions/1472049/check-if-a-point-is-inside-a-rectangular-shaped-area-3d
	//if a point p is inside a 3D rotated the point has to lay behind each or on the surface of at least one of the six facets
	//this is a special case for the general convex polyhedra inclusion
	apt_real ix = this->p2.x - this->p1.x; //vector normal on plane P1P4P8P5
	apt_real iy = this->p2.y - this->p1.y;
	apt_real iz = this->p2.z - this->p1.z;

	apt_real jx = this->p4.x - this->p1.x; //vector normal on plane P1P2P6P5
	apt_real jy = this->p4.y - this->p1.y;
	apt_real jz = this->p4.z - this->p1.z;

	apt_real kx = this->p5.x - this->p1.x; //vector normal on plane P1P4P3P2
	apt_real ky = this->p5.y - this->p1.y;
	apt_real kz = this->p5.z - this->p1.z;

	apt_real vx = p.x - this->p1.x;
	apt_real vy = p.y - this->p1.y;
	apt_real vz = p.z - this->p1.z;

	apt_real vi = vx*ix + vy*iy + vz*iz;
	apt_real ii = ix*ix + iy*iy + iz*iz;
	apt_real vj = vx*jx + vy*jy + vz*jz;
	apt_real jj = jx*jx + jy*jy + jz*jz;
	apt_real vk = vx*kx + vy*ky + vz*kz;
	apt_real kk = kx*kx + ky*ky + kz*kz;

	////##MK::there is the possibility to make an early reject and return false by checking the individual conditions
	////in case we expect that most points are in the box the following is more efficient
	if ( ii > vi && vi > MYZERO &&
			jj > vj && vj > MYZERO &&
				kk > vk && vk > MYZERO ) {
		return true;
	}
	return false;

	//however, if we expect that the ROI is much smaller than the point cloud, most points are not expected not inside so the following could be more efficient
	//given that in the first case all conditions have to be met to confirm inclusion
	/*
	if ( ii <= vi )
		return false;
	if ( vi <= MYZERO )
		return false;
	if ( jj <= vj )
		return false;
	if ( vj <= MYZERO )
		return false;
	if ( kk <= vk )
		return false;
	if ( vk <= MYZERO )
		return false;
	*/

	//not returned? so inside
	return true;
}


void roi_rotated_cuboid::proxigram_add( p3d const & p, unsigned char const & ityp )
{
	//by construction we have a cuboid where the points p5 and p1 form an edge of the cuboid of the rotated p1p2 plane
	apt_real p1dx = p.x - this->barycenter.x; // vector pd from point 1 to barycenter
	apt_real p1dy = p.y - this->barycenter.y;
	apt_real p1dz = p.z - this->barycenter.z;
	apt_real signed_distance = p1dx * this->ori.x + p1dy * this->ori.y + p1dz * this->ori.z; //ori is by construction an ounrm pointing from rotated "up"
	//along the longest axis of the cuboid

	map<unsigned char, vector<apt_real>*>::iterator thisone = this->ityp_proxigrams.find( ityp );
	if ( thisone != this->ityp_proxigrams.end() ) {
		if ( thisone->second != NULL ) {
			thisone->second->push_back( signed_distance );
		}
		else {
			cerr << "roi_rotated_cuboid::proxigram_add leaking because not ityp array allocated !" << "\n";
		}
	}
	else {
		cerr << "roi_rotated_cuboid::proxigram_add trying to add an unknown ityp for which no results vector was allocated !" << "\n";
	}
}


ostream& operator << (ostream& in, roi_rotated_cuboid const & val) {
	in << "roi_cuboid " << "\n";
	in << "barycenter x/y/z " << val.barycenter.x << ", " << val.barycenter.y << ", " << val.barycenter.z << "\n";
	in << "Lx/y/z " << val.Lx << ", " << val.Ly << ", " << val.Lz << "\n";
	in << "Ractive " << val.Ractive << "\n";
	in << "p1 x/y/z " << val.p1.x << ", " << val.p1.y << ", " << val.p1.z << "\n";
	in << "p2 x/y/z " << val.p2.x << ", " << val.p2.y << ", " << val.p2.z << "\n";
	in << "p3 x/y/z " << val.p3.x << ", " << val.p3.y << ", " << val.p3.z << "\n";
	in << "p4 x/y/z " << val.p4.x << ", " << val.p4.y << ", " << val.p4.z << "\n";
	in << "p5 x/y/z " << val.p5.x << ", " << val.p5.y << ", " << val.p5.z << "\n";
	in << "p6 x/y/z " << val.p6.x << ", " << val.p6.y << ", " << val.p6.z << "\n";
	in << "p7 x/y/z " << val.p7.x << ", " << val.p7.y << ", " << val.p7.z << "\n";
	in << "p8 x/y/z " << val.p8.x << ", " << val.p8.y << ", " << val.p8.z << "\n";
	if ( val.close_to_the_edge == true )
		in << "close_to_the_edge " << "yes" << "\n";
	else
		in << "close_to_the_edge " << "no" << "\n";
	return in;
}


roi_rotated_cylinder::roi_rotated_cylinder( p3d const & _p1, p3d const & _p2, const apt_real _r )
{
	this->p1 = p3d( _p1.x, _p1.y, _p1.z );
	this->p2 = p3d( _p2.x, _p2.y, _p2.z );
	this->ori = p3d( _p2.x - _p1.x, _p2.y - _p1.y, _p2.z - _p1.z );
	this->r = _r;
}


roi_rotated_cylinder::roi_rotated_cylinder( p3d const & _bary, p3d const & _direction, const apt_real _h, const apt_real _r )
{
	apt_real SQRlen = SQR(_direction.x)+SQR(_direction.y)+SQR(_direction.z);
	p3d nrmdir = p3d(_direction.x, _direction.y, _direction.z);
	if ( SQRlen >= EPSILON ) {
		apt_real norm = MYONE / sqrt(SQRlen);
		nrmdir.x *= norm;
		nrmdir.y *= norm;
		nrmdir.z *= norm;

		this->p1 = p3d( _bary.x - 0.5*_h*nrmdir.x, _bary.y - 0.5*_h*nrmdir.y, _bary.z - 0.5*_h*nrmdir.z );
		this->p2 = p3d( _bary.x + 0.5*_h*nrmdir.x, _bary.y + 0.5*_h*nrmdir.y, _bary.z + 0.5*_h*nrmdir.z );
		this->ori = p3d( this->p2.x - this->p1.x, this->p2.y - this->p1.y, this->p2.z - this->p1.z );
		this->r = _r;
		return;
	}
	cerr << "Direction vector too short to instantiate roi_rotated_cylinder !" << "\n";
	this->p1 = p3d();
	this->p2 = p3d();
	this->ori = p3d();
	this->r = MYZERO;
	//##MK::ori,x,y,z
}


aabb3d roi_rotated_cylinder::get_aabb() const
{
	//this is not the tightest fitting AABB !, also there is some redundant computing because if it always holds that p2 >= p1 for all their x,y,z
	//we do not need to evaluate the pair
	aabb3d box = aabb3d(
			fmin(fmin((this->p1.x-this->r), (this->p1.x+this->r)), fmin((this->p2.x-this->r),(this->p2.x+this->r))),
			fmax(fmax((this->p1.x+this->r), (this->p1.x-this->r)), fmax((this->p2.x+this->r),(this->p2.x-this->r))),
			fmin(fmin((this->p1.y-this->r), (this->p1.y+this->r)), fmin((this->p2.y-this->r),(this->p2.y+this->r))),
			fmax(fmax((this->p1.y+this->r), (this->p1.y-this->r)), fmax((this->p2.y+this->r),(this->p2.y-this->r))),
			fmin(fmin((this->p1.z-this->r), (this->p1.z+this->r)), fmin((this->p2.z-this->r),(this->p2.z+this->r))),
			fmax(fmax((this->p1.z+this->r), (this->p1.z-this->r)), fmax((this->p2.z+this->r),(this->p2.z-this->r)))  );
	box.add_epsilon_guard();
	box.scale();
	return box;
}


bool roi_rotated_cylinder::is_inside( p3d const & p ) const
{
/*
	//essentially https://www.flipcode.com/archives/Fast_Point-In-Cylinder_Test.shtml
	//-----------------------------------------------------------------------------
	// Name: CylTest_CapsFirst
	// Orig: Greg James - gjames@NVIDIA.com
	// Lisc: Free code - no warranty & no money back.  Use it all you want
	// Desc:
	//    This function tests if the 3D point 'testpt' lies within an arbitrarily
	// oriented cylinder.  The cylinder is defined by an axis from 'pt1' to 'pt2',
	// the axis having a length squared of 'lengthsq' (pre-compute for each cylinder
	// to avoid repeated work!), and radius squared of 'radius_sq'.
	//    The function tests against the end caps first, which is cheap -> only
	// a single dot product to test against the parallel cylinder caps.  If the
	// point is within these, more work is done to find the distance of the point
	// from the cylinder axis.
	//    Fancy Math (TM) makes the whole test possible with only two dot-products
	// a subtract, and two multiplies.  For clarity, the 2nd mult is kept as a
	// divide.  It might be faster to change this to a mult by also passing in
	// 1/lengthsq and using that instead.
	//    Elminiate the first 3 subtracts by specifying the cylinder as a base
	// point on one end cap and a vector to the other end cap (pass in {dx,dy,dz}
	// instead of 'pt2' ).
	//
	//    The dot product is constant along a plane perpendicular to a vector.
	//    The magnitude of the cross product divided by one vector length is
	// constant along a cylinder surface defined by the other vector as axis.
	//
	// Return:  -MYONE if point is outside the cylinder
	// Return:  distance squared from cylinder axis if point is inside.

	//float CylTest_CapsFirst( const Vec3 & pt1, const Vec3 & pt2, float lengthsq, float radius_sq, const Vec3 & testpt )

	apt_real dx = this->ori_x; //vector d  from line segment point 1 to point 2
	apt_real dy = this->ori_y;
	apt_real dz = this->ori_z;

	//pt1 is at 0.5H*\bar{ori_n}
	apt_real pdx = p.x - this->p1.x; // vector pd from point 1 to test point
	apt_real pdy = p.y - this->p1.y;
	apt_real pdz = p.z - this->p1.z;

	// Dot the d and pd vectors to see if point lies behind the
	// cylinder cap at pt1.x, pt1.y, pt1.z

	apt_real dot = pdx * dx + pdy * dy + pdz * dz;

	// If dot is less than zero the point is behind the pt1 cap.
	// If greater than the cylinder axis line segment length squared
	// then the point is outside the other end cap at pt2.

	apt_real lengthsq = SQR(this->ori_x)+SQR(this->ori_y)+SQR(this->ori_z);
	//##MK::I am not convinced that it should be dot > lengthsq
	//https://stackoverflow.com/questions/47932955/how-to-check-if-a-3d-point-is-inside-a-cylinder
	if( dot < MYZERO || dot > lengthsq ) {
		return false;
		//return( -ONEf );
	}
	else
	{
		// Point lies within the parallel caps, so find
		// distance squared from point to line, using the fact that sin^2 + cos^2 = 1
		// the dot = cos() * |d||pd|, and cross*cross = sin^2 * |d|^2 * |pd|^2
		// Carefull: '*' means mult for scalars and dotproduct for vectors
		// In short, where dist is pt distance to cyl axis:
		// dist = sin( pd to d ) * |pd|
		// distsq = dsq = (1 - cos^2( pd to d)) * |pd|^2
		// dsq = ( 1 - (pd * d)^2 / (|pd|^2 * |d|^2) ) * |pd|^2
		// dsq = pd * pd - dot * dot / lengthsq
		//  where lengthsq is d*d or |d|^2 that is passed into this function

		// distance squared to the cylinder axis:

		apt_real dsq = (pdx*pdx + pdy*pdy + pdz*pdz) - dot*dot/lengthsq;

		apt_real radiussq = SQR(this->r);
		if( dsq > radiussq ) {
			return false;
			//return( -ONEf );
		}
		else {
			return true;
			//return( dsq );		// return distance squared to axis
		}
	}
*/
	apt_real p1dx = p.x - this->p1.x; // vector pd from point 1 to test point
	apt_real p1dy = p.y - this->p1.y;
	apt_real p1dz = p.z - this->p1.z;
	apt_real dot1 = p1dx * +this->ori.x + p1dy * +this->ori.y + p1dz * +this->ori.z;
	if ( dot1 < MYZERO ) { //behind cap of p1
		return false;
	}

	apt_real p2dx = p.x - this->p2.x; // vector pd from point 1 to test point
	apt_real p2dy = p.y - this->p2.y;
	apt_real p2dz = p.z - this->p2.z;
	apt_real dot2 = p2dx * -this->ori.x + p2dy * -this->ori.y + p2dz * -this->ori.z;
	if ( dot2 < MYZERO ) { //in front of cap of p2
		return false;
	}

	//|(p-p1)x(p2-p1)| / |(p2-p1)| <= r is inside
	apt_real crosssq = SQR(p1dy*this->ori.z - p1dz*this->ori.y)
							+ SQR(p1dz*this->ori.x - p1dx*this->ori.z)
								+ SQR(p1dx*this->ori.y - p1dy*this->ori.x);
	if ( crosssq > SQR(this->r)*(SQR(this->ori.x)+SQR(this->ori.y)+SQR(this->ori.z)) ) {
		return false;
	}
	return true;
}


apt_real roi_rotated_cylinder::projected_height( p3d const & p )
{
	apt_real p1dx = p.x - this->p1.x; // vector pd from point 1 to test point
	apt_real p1dy = p.y - this->p1.y;
	apt_real p1dz = p.z - this->p1.z;
	apt_real SQRlen = SQR(this->ori.x)+SQR(this->ori.y)+SQR(this->ori.z);
	apt_real len = MYONE;
	if ( SQRlen >= EPSILON ) {
		len /= sqrt(SQRlen);
	}
	else {
		len = MYZERO;
		cerr << "roi_rotated_cylinder::trying to normalize a vector with too small coordinates !" << "\n";
	}
	apt_real dot = p1dx * this->ori.x * len + p1dy * this->ori.y * len + p1dz * this->ori.z * len;
	return dot;
}


void roi_rotated_cylinder::proxigram_add( p3d const & p, const unsigned char elemtyp,
	apt_uint mltply, const unsigned char cmp_sgn_dist_method )
{
	//by construction of the cylinder its barycenter is halfway between p1 and p2
	apt_real signed_distance = RMX;
	if ( cmp_sgn_dist_method == SIGNED_DISTANCE_BASED_ON_BARYCENTER ) {
		p3d barycenter = p3d(
			0.5*(this->p1.x + this->p2.x),
			0.5*(this->p1.y + this->p2.y),
			0.5*(this->p1.z + this->p2.z) );
		apt_real p1dx = p.x - barycenter.x;
		apt_real p1dy = p.y - barycenter.y;
		apt_real p1dz = p.z - barycenter.z;
		apt_real SQRlen = SQR(this->ori.x)+SQR(this->ori.y)+SQR(this->ori.z);
		apt_real len = MYONE;
		if ( SQRlen >= EPSILON ) {
			len /= sqrt(SQRlen);
		}
		else {
			len = MYZERO;
			cerr << "roi_rotated_cylinder::trying to normalize a vector with too small coordinates !" << "\n";
		}
		signed_distance = p1dx * this->ori.x * len + p1dy * this->ori.y * len + p1dz * this->ori.z * len;
	}
	else { //SIGNED_DISTANCE_BASED_ON_LOWEST_CAP
		signed_distance = this->projected_height( p );
	}
	if ( signed_distance < (RMX - EPSILON) ) {
		map<unsigned char, vector<apt_real>*>::iterator thisone = this->elemtyp_proxigrams.find( elemtyp );
		if ( thisone != this->elemtyp_proxigrams.end() ) {
			if ( thisone->second != NULL ) {
				for( apt_uint m = 0; m < mltply; m++ ) {
					thisone->second->push_back( signed_distance );
				}
			}
			else {
				cerr << "roi_rotated_cylinder::proxigram_add leaking because not elemtyp array allocated !" << "\n";
			}
		}
		else {
			cerr << "roi_rotated_cylinder::proxigram_add trying to add an unknown elemtyp for which no results vector was allocated !" << "\n";
		}
	}
	else {
		cerr << "roi_rotated_cylinder::signed_distance >= (RMX - EPSILON) !" << "\n";
	}
}


ostream& operator << (ostream& in, roi_rotated_cylinder const & val)
{
	in << "roi_rotated_cylinder = " << "\n";
	in << "p1 x/y/z " << val.p1.x << ", " << val.p1.y << ", " << val.p1.z << "\n";
	in << "p2 x/y/z " << val.p2.x << ", " << val.p2.y << ", " << val.p2.z << "\n";
	in << "r " << val.r << "\n";
	in << "ori x/y/z " << val.ori.x << ", " << val.ori.y << ", " << val.ori.z << "\n";
	if ( val.close_to_the_edge == true )
		in << "close_to_the_edge " << "yes" << "\n";
	else
		in << "close_to_the_edge " << "no" << "\n";
	return in;
}


bool roi_sphere::is_inside( p3d const & p ) const
{
	//assume sphere is small so most volume is outside
	if ( (SQR(this->barycenter.x - p.x) + SQR(this->barycenter.y - p.y) + SQR(this->barycenter.z - p.z)) > SQR(this->r) ) {
		return false;
	}
	return true;
}


aabb3d roi_sphere::get_aabb()
{
	return aabb3d( 	this->barycenter.x - this->r - EPSILON,
					this->barycenter.x + this->r + EPSILON,
					this->barycenter.y - this->r - EPSILON,
					this->barycenter.y + this->r + EPSILON,
					this->barycenter.z - this->r - EPSILON,
					this->barycenter.z + this->r + EPSILON   );
}


ostream& operator << (ostream& in, roi_sphere const & val)
{
	in << "roi_sphere " << "\n";
	in << "barycenter x/y/z " << val.barycenter.x << ", " << val.barycenter.y << ", " << val.barycenter.z << "\n";
	in << "r " << val.r << "\n";
	return in;
}


aabb3d roi_tetrahedron::get_aabb()
{
	return aabb3d( 	fmin(fmin(fmin(v1.x, v2.x), v3.x), v4.x) - EPSILON,
					fmax(fmax(fmax(v1.x, v2.x), v3.x), v4.x) + EPSILON,
					fmin(fmin(fmin(v1.y, v2.y), v3.y), v4.y) - EPSILON,
					fmax(fmax(fmax(v1.y, v2.y), v3.y), v4.y) + EPSILON,
					fmin(fmin(fmin(v1.z, v2.z), v3.z), v4.z) - EPSILON,
					fmax(fmax(fmax(v1.z, v2.z), v3.z), v4.z) + EPSILON   );
}


apt_real roi_tetrahedron::get_volume()
{
	p3d a = p3d( v2.x-v1.x, v2.y-v1.y, v2.z-v1.z );
	p3d b = p3d( v3.x-v1.x, v3.y-v1.y, v3.z-v1.z );
	p3d c = p3d( v4.x-v1.x, v4.y-v1.y, v4.z-v1.z );
	p3d b_x_c = cross(b, c);
	return 1./6. * fabs( dot(a, b_x_c) );
}


ostream& operator << (ostream& in, roi_tetrahedron const & val)
{
	//######
	return in;
}


aabb3d roi_polyhedron::get_aabb( size_t const cellidx )
{
	aabb3d retval = aabb3d();
	if ( cellidx < this->cells_volume.size() ) {
		quad3u thisone = this->cells_volume[cellidx];
		retval.xmi = fmin(fmin(fmin( this->vrts_volume[thisone.v1].x, this->vrts_volume[thisone.v2].x), this->vrts_volume[thisone.v3].x), this->vrts_volume[thisone.v4].x );
		retval.xmx = fmax(fmax(fmax(this->vrts_volume[thisone.v1].x, this->vrts_volume[thisone.v2].x), this->vrts_volume[thisone.v3].x), this->vrts_volume[thisone.v4].x );
		retval.ymi = fmin(fmin(fmin(this->vrts_volume[thisone.v1].y, this->vrts_volume[thisone.v2].y), this->vrts_volume[thisone.v3].y), this->vrts_volume[thisone.v4].y );
		retval.ymx = fmax(fmax(fmax(this->vrts_volume[thisone.v1].y, this->vrts_volume[thisone.v2].y), this->vrts_volume[thisone.v3].y), this->vrts_volume[thisone.v4].y );
		retval.zmi = fmin(fmin(fmin(this->vrts_volume[thisone.v1].z, this->vrts_volume[thisone.v2].z), this->vrts_volume[thisone.v3].z), this->vrts_volume[thisone.v4].z );
		retval.zmx = fmax(fmax(fmax(this->vrts_volume[thisone.v1].z, this->vrts_volume[thisone.v2].z), this->vrts_volume[thisone.v3].z), this->vrts_volume[thisone.v4].z );
	}
	return retval;
}


roi_tetrahedron roi_polyhedron::get_tetrahedron( const size_t cellidx )
{
	roi_tetrahedron retval;
	if ( cellidx < this->cells_volume.size() ) {
		quad3u thisone = this->cells_volume[cellidx];
		retval.v1 = this->vrts_volume[thisone.v1];
		retval.v2 = this->vrts_volume[thisone.v2];
		retval.v3 = this->vrts_volume[thisone.v3];
		retval.v4 = this->vrts_volume[thisone.v4];
	}
	return retval;
}
