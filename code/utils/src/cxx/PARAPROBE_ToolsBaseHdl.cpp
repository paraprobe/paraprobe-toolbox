/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_ToolsBaseHdl.h"


mpiHdl::mpiHdl()
{
	edge = triangleSoup();
	ions = ionCloud();
	rng = rangeTable();
	base_tictoc = profiler();

	myrank = MASTER;
	nranks = 1;	
}


mpiHdl::~mpiHdl()
{
}


int mpiHdl::get_myrank()
{
	return myrank;
}


int mpiHdl::get_nranks()
{
	return nranks;
}


void mpiHdl::set_myrank( const int rr )
{
	myrank = rr;
}


void mpiHdl::set_nranks( const int nn )
{
	nranks = nn;
}


bool mpiHdl::all_healthy_still( const int myhealth )
{
	int localhealth = 0;
	if ( myhealth == 1 || myhealth == 0 ) {
		localhealth = myhealth;
	}
	int globalhealth = 0;
	MPI_Allreduce(&localhealth, &globalhealth, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	if ( globalhealth == get_nranks() ) {
		return true;
	}
	return false;
}


void mpiHdl::commit_mpidatatypes()
{
/*
#ifdef EMPLOY_SINGLE_PRECISION
	MPI_Type_contiguous(3, MPI_FLOAT, &MPI_Point3D_Type);
	MPI_Type_commit(&MPI_Point3D_Type);

	MPI_Type_contiguous(9, MPI_FLOAT, &MPI_Triangle3D_Type);
	MPI_Type_commit(&MPI_Triangle3D_Type);

	MPI_Type_contiguous(2, MPI_FLOAT, &MPI_MQIval_Type);
	MPI_Type_commit(&MPI_MQIval_Type);
#elif
	MPI_Type_contiguous(3, MPI_DOUBLE, &MPI_Point3D_Type);
	MPI_Type_commit(&MPI_Point3D_Type);

	MPI_Type_contiguous(9, MPI_DOUBLE, &MPI_Triangle3D_Type);
	MPI_Type_commit(&MPI_Triangle3D_Type);

	MPI_Type_contiguous(2, MPI_DOUBLE, &MPI_MQIval_Type);
	MPI_Type_commit(&MPI_MQIval_Type);
#endif

	//isotope vector, itypid, sgn, charge
	struct MPI_Iontype dummy[1];
	MPI_Datatype types[3] = { MPI_UNSIGNED, MPI_UNSIGNED_SHORT, MPI_UNSIGNED_CHAR };
	int blcklen[3] = {2, MAX_NUMBER_OF_ATOMS_PER_MOLECULAR_ION, 3};
	MPI_Aint displ[3];
	displ[0] = (MPI_Iontype*) &dummy[0].mqival_start - &dummy[0];
	displ[1] = (MPI_Iontype*) &dummy[0].ivec[0] - &dummy[0];
	displ[2] = (MPI_Iontype*) &dummy[0].charge_sgn - &dummy[0];
	//we need to test this
	MPI_Type_create_struct(3, blcklen, displ, types, &MPI_Iontype_Type);
	MPI_Type_commit(&MPI_Iontype_Type);
*/
}


void mpiHdl::free_mpidatatypes()
{
/*
	MPI_Type_free(&MPI_Point3D_Type);
	MPI_Type_free(&MPI_Triangle3D_Type);
	MPI_Type_free(&MPI_MQIval_Type);
	MPI_Type_free(&MPI_Iontype_Type);
*/
}


bool mpiHdl::read_xyz_from_h5( const string h5fn, const string dsnm )
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {
		//load xyz positions
		ions.ionpp3 = vector<p3d>();

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( h5fn );
		vector<apt_real> real;
		if ( h5r.nexus_read( dsnm, real ) == MYHDF5_SUCCESS ) {
			try {
				ions.ionpp3 = vector<p3d>( real.size()/3, p3d() );
			}
			catch(bad_alloc &croak) {
				cerr << "Allocating p3d array for ion positions failed !" << "\n";
				return false;
			}
			for( size_t i = 0; i < real.size()/3; i++ ) {
				ions.ionpp3[i].x = real[3*i+0]; //if EMPLOY_SINGLE_PRECISION is undefined, implicit promotion to double
				ions.ionpp3[i].y = real[3*i+1];
				ions.ionpp3[i].z = real[3*i+2];
			}
		}
		else {
			cerr << h5fn << " reading reconstructed ion positions failed !" << "\n";
			return false;
		}
	}
	//slaves get their values from the broadcast operations, thereby reducing traffic on the file I/O system

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ReadXYZFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


bool mpiHdl::read_mq_from_h5( const string h5fn, const string dsnm )
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {
		//load mass-to-charge ratio
		ions.ionmq = vector<apt_real>();

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( h5fn );
		vector<apt_real> real;
	 	if ( h5r.nexus_read( dsnm, real ) == MYHDF5_SUCCESS ) {
			try {
				ions.ionmq = vector<apt_real>( real.size(), MYZERO );
			}
			catch(bad_alloc &croak) {
				cerr << "Allocating p3d array for mass-to-charge state ratio values failed !" << "\n";
				return false;
			}
			for( size_t i = 0; i < real.size(); i++ ) {
				ions.ionmq[i] = real[i];
			}
	 	}
	 	else {
			cerr << h5fn << " reading mass-to-charge state ratio values failed !" << "\n";
			return false;
		}
	}
	//slaves get their values from the broadcast operations, thereby reducing traffic on the file I/O system

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ReadMQFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


bool mpiHdl::read_ranging_from_h5( const string h5fn, const string grpnm )
{
	double tic = MPI_Wtime();
	string fwslash = "/";

	if ( get_myrank() == MASTER ) {
		rng = rangeTable();
		// ##MK::sprint9 string subgrpnm = grpnm + fwslash + "IonSpecies";

		if ( rng.read_ionspecies_from_h5( h5fn, grpnm ) == true ) {
			cout << h5fn << " read " << grpnm << " successfully" << "\n";
		}
		else {
			cerr << h5fn << " read " << grpnm << " failed!" << "\n";
			return false;
		}
		cout << "Rank " << get_myrank() << " loaded rng.nuclides.isotopes.size() " << rng.iontypes.size() << " iontypes" << "\n";
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ReadIontypesFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool mpiHdl::read_ionlabels_from_h5( const string h5fn, const string grpnm )
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {
		//load ion labels aka iontypes assigned to each ion/point
		try {
			ions.ionifo = vector<p3dinfo>( ions.ionpp3.size(), p3dinfo() );
		}
		catch(bad_alloc &croak) {
			cerr << "Allocating information array for all ions failed !" << "\n";
			return false;
		}

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( h5fn );
		vector<unsigned char> u08;
		string dsnm = grpnm + "/iontypes";
		if ( h5r.nexus_read( dsnm, u08 ) == MYHDF5_SUCCESS ) {
			if ( u08.size() == ions.ionifo.size() ) {
				for( size_t i = 0; i < u08.size(); i++ ) {
					ions.ionifo[i].i_org = u08[i];
					ions.ionifo[i].i_rnd = u08[i];
				}
			}
			else {
				cerr << "Length of ion labels array is different to mass-to-charge-state ratio array !" << "\n";
				return false;
			}
		}
		else {
			cerr << h5fn << " reading ion labels failed !" << "\n"; return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ReadIonLabelsFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


bool mpiHdl::read_distance_dsetedge_from_h5( const string h5fn, const string dsnm )
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {
		if ( ions.ionpp3.size() != ions.ionifo.size() ) {
			cerr << "Inconsistence between length of ionpp3 and ionifo arrays !" << "\n";
			return false;
		}

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( h5fn );
		vector<apt_real> real;
		//out was already allocated and we need to read an array of the same length, because we read for all ions in the dataset by definition
		if ( h5r.nexus_read( dsnm, real ) == MYHDF5_SUCCESS ) {
			if ( real.size() == ions.ionifo.size() ) {
				for( size_t i = 0; i < real.size(); i++ ) {
					ions.ionifo[i].dist = real[i];
				}
				return true;
			}
			else {
				cerr << "Length of ion-to-edge distance array is different to mass-to-charge-state ratio array !" << "\n";
				return false;
			}
		}
		else {
			cerr << h5fn << " reading distance of ion to edge failed !" << "\n";
			return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ReadIonToEdgeFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);

	return true;
}


bool mpiHdl::read_mesh_dsetedge_from_h5( const string h5fn, const string dsnm_vrts, const string dsnm_fcts )
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {

		edge.triangles = vector<tri3d>();

		HdfFiveSeqHdl h5r = HdfFiveSeqHdl( h5fn );
		vector<apt_real> real;
		if ( h5r.nexus_read( dsnm_vrts, real ) == MYHDF5_SUCCESS ) {
			vector<apt_uint> uint;
			if ( h5r.nexus_read( dsnm_fcts, uint ) == MYHDF5_SUCCESS ) {
				if ( (real.size() % 3 == 0) && (uint.size() % 3 == 0) ) {
					size_t n_triangles = uint.size() / 3;
					for( size_t triidx = 0; triidx < n_triangles; triidx++ ) {
						size_t v1 = (size_t) uint[3*triidx+0];
						size_t v2 = (size_t) uint[3*triidx+1];
						size_t v3 = (size_t) uint[3*triidx+2];

						edge.triangles.push_back( tri3d(
								real[3*v1+0], real[3*v1+1], real[3*v1+2],
								real[3*v2+0], real[3*v2+1], real[3*v2+2],
								real[3*v3+0], real[3*v3+1], real[3*v3+2] ) );
					}
					cout << h5fn << " read edge mesh successfully" << "\n";
				}
				else {
					cerr << "real.size() % 3 != 0 or uint.size() % 3 != 0 !" << "\n";
					return false;
				}
			}
			else {
				cerr << h5fn << " reading mesh facet indices failed !" << "\n";
				return false;
			}
		}
		else {
			cerr << h5fn << " reading mesh vertices failed !" << "\n";
			return false;
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ReadEdgeMeshFromH5", APT_XX, APT_IS_SEQ, mm, tic, toc);
	return true;
}


void mpiHdl::itype_sensitive_spatial_decomposition()
{
	double tic = MPI_Wtime();

	ions.get_aabb();

	double toc = MPI_Wtime();
	memsnapshot mm = base_tictoc.get_memoryconsumption();
	base_tictoc.prof_elpsdtime_and_mem( "IonCloudComputeAABB", APT_XX, APT_IS_PAR, mm, tic, toc);

	/*
	tic = MPI_Wtime();

	sp.loadpartitioning_subkdtree_per_ityp( xyz, dist2edge, ityp_org, ityp_org, maximum_iontype_uc ); //random labels will not be used!

	toc = MPI_Wtime();
	mm = base_tictoc.get_memoryconsumption();
	base_tictoc.prof_elpsdtime_and_mem( "EntireDatasetOpenMPLoadPartitioning", APT_XX, APT_IS_PAR, mm, tic, toc);
	*/
}


void mpiHdl::apply_none_filter()
{
	double tic = MPI_Wtime();

	//by default no ion is selected for analysis, we need to explicitly say which ions to analyze
	apt_uint nions = (apt_uint) ions.ionpp3.size();
	//apt_uint accepted = 0;
	apt_uint omitted = 0;
	for( apt_uint i = 0;  i < nions; i++ ) {
		ions.ionifo[i].mask1 = ANALYZE_NO;
	}

	//accepted = 0;
	omitted = nions;
	cout << "Applied none filter omitted " << omitted << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ApplyNoneFilter", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


void mpiHdl::apply_all_filter()
{
	double tic = MPI_Wtime();

	//by default no ion is selected for analysis, the all filter explicitly says that all ions are analyzed
	apt_uint nions = (apt_uint) ions.ionpp3.size();
	apt_uint accepted = 0;
	apt_uint omitted = 0;
	for( apt_uint i = 0;  i < nions; i++ ) {
		ions.ionifo[i].mask1 = ANALYZE_YES;
	}

	accepted = nions;
	omitted = 0;
	cout << "Applied all filter accepted " << accepted << " omitted " << omitted << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ApplyAllFilter", APT_XX, APT_IS_SEQ, mm, tic, toc);
}


void mpiHdl::apply_linear_subsampling_filter( lival<apt_uint> const & ionids )
{
	double tic = MPI_Wtime();

	//apt_uint accepted = 0;
	apt_uint omitted = 0;

	apt_uint idmin = ionids.min;
	apt_uint idincr = ionids.incr;
	apt_uint idmax = ionids.max;
	apt_uint nions = (apt_uint) ions.ionpp3.size();
	if ( idmax >= nions ) {
		idmax = nions - 1;
	}

	//MK::the filter switches those ions off (sets mask1 to ANALYZE_NO) which
	//should not be analyzed, because for sub-sampling very often the number of
	//ions chosen is smaller than those filtered off

	for( apt_uint i = 0; i < idmin; i++ ) { //clip evaporation IDs too small
		ions.ionifo[i].mask1 = ANALYZE_NO;
	}
	omitted += (idmin - 0);
	for( apt_uint j = idmin; j <= idmax; j++ ) {
		if ( ((j - idmin) + 1) % idincr != 0 ) {
			ions.ionifo[j].mask1 = ANALYZE_NO;
			omitted++; //##MK::make a more elegant solution
		}
	}
	for( apt_uint k = idmax+1; k < nions; k++ ) { //clip evaporation IDs too large
		ions.ionifo[k].mask1 = ANALYZE_NO;
	}
	omitted += (nions - (idmax+1));

	cout << "Applied linear sub sample filter omitted " << omitted << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ApplyLinearSubSamplingRangeFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
}


void mpiHdl::apply_iontype_remove_filter( vector<unsigned char> const & ityp_list, const bool is_blacklist )
{
	double tic = MPI_Wtime();

	//the ion filter pipeline works either from all or none and has applied already eventually a spatial and sub-sampling
	//filter telling which ions should be analyzed
	//this filter will mark those ions with iontype in the ityp_list
	//ityp_list is interpreted according to is_blacklist
	apt_uint omitted = 0;
	#pragma omp parallel shared(omitted)
	{
		//thread-local cache
		vector<apt_uint> my_omit_res;
		apt_uint nions = (apt_uint) ions.ionpp3.size();
		#pragma omp for
		for( apt_uint i = 0; i < nions; i++ ) {
			unsigned char ityp = ions.ionifo[i].i_org;
			if ( is_blacklist == true ) { //filtering for ityp_list acting as a blacklist
				for( auto it = ityp_list.begin(); it != ityp_list.end(); it++ ) {
					if ( *it == ityp ) {
						my_omit_res.push_back(i);
						break;
					}
				}
			}
			else { //filtering for ityp_list acting as a whitelist
				bool found = false;
				for( auto it = ityp_list.begin(); it != ityp_list.end(); it++ ) {
					if ( *it == ityp ) {
						found = true;
						break;
					}
				}
				if ( found == false ) {
					my_omit_res.push_back(i);
				}
			}
		}
		#pragma omp barrier
		#pragma omp critical
		{
			//for( auto it = myres.begin(); it != myres.end(); it++ ) {
			for ( auto it = my_omit_res.begin(); it != my_omit_res.end(); it++ ) {
				//ions.ionifo.at(it->first).mask1 = it->second;
				//do not check whether ions have already set to ANALYZE_NO
				ions.ionifo.at(*it).mask1 = ANALYZE_NO;
			}
			omitted += (apt_uint) my_omit_res.size();
			//myres = map<apt_uint, unsigned char>();
			my_omit_res = vector<apt_uint>();
		}
	}

	cout << "Applied iontype remove filter omitted " << omitted << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ApplyIonTypeRemoveFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
}


/*
void mpiHdl::apply_iontype_accept_filter( vector<unsigned char> const & ityp_whitelist )
{
	double tic = MPI_Wtime();

	//the ion filter pipeline works either from all or none and has applied already eventually a spatial and sub-sampling
	//filter telling which ions should be analyzed
	//this filter will mark those ions with iontype not in the whitelist as that they should not be analyzed irrespective
	//whether the previous filters flagged these ions to be analyzed, this is because the filters in the chain are additive
	apt_uint omitted = 0;
	#pragma omp parallel shared(accepted, omitted)
	{
		//thread-local cache which ions should we accept
		vector<apt_uint> my_omit_res;
		apt_uint nions = (apt_uint) ions.ionpp3.size();
		#pragma omp for
		for( apt_uint i = 0; i < nions; i++ ) {
			unsigned char ityp = ions.ionifo[i].i_org;
			bool accept = false;
			for( auto it = ityp_whitelist.begin(); it != ityp_whitelist.end(); it++ ) {
				if ( *it == ityp ) {
					accept = true;
					break;
				}
			}
			if ( accept == false ) {
				my_omit_res.push_back(i);
			}
		}
		#pragma omp barrier
		#pragma omp critical
		{
			for ( auto it = my_omit_res.begin(); it != my_omit_res.end(); it++ ) {
				//ions.ionifo.at(it->first).mask1 = it->second;
				if ( ions.ionifo.at(*it).mask1 == ANALYZE_YES )
				ions.ionifo.at(*it).mask1 = ANALYZE_YES;
			}
			accepted += (apt_uint) my_accept_res.size();
			my_accept_res = vector<apt_uint>();
		}
	}

	cout << "Applied iontype accept filter accepted " << accepted << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ApplyIonTypeAcceptFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
}
*/


void mpiHdl::apply_multiplicity_remove_filter( vector<unsigned char> const & mltply_list, const bool is_blacklist )
{
	double tic = MPI_Wtime();

	//the ion filter pipeline works either from all or none and has applied already eventually a spatial and sub-sampling
	//filter telling which ions should be analyzed
	//this filter will mark those ions with hit multiplicity in the mltply_list
	//ityp_list is interpreted according to is_blacklist
	apt_uint omitted = 0;
	#pragma omp parallel shared(omitted)
	{
		//thread-local cache
		vector<apt_uint> my_omit_res;
		apt_uint nions = (apt_uint) ions.ionpp3.size();
		#pragma omp for
		for( apt_uint i = 0; i < nions; i++ ) {
			unsigned char mltply = ions.ionifo[i].mltply;
			if ( is_blacklist == true ) { //filtering for ityp_list acting as a blacklist
				for( auto it = mltply_list.begin(); it != mltply_list.end(); it++ ) {
					if ( *it == mltply ) {
						my_omit_res.push_back(i);
						break;
					}
				}
			}
			else { //filtering for ityp_list acting as a whitelist
				bool found = false;
				for( auto it = mltply_list.begin(); it != mltply_list.end(); it++ ) {
					if ( *it == mltply ) {
						found = true;
						break;
					}
				}
				if ( found == false ) {
					my_omit_res.push_back(i);
				}
			}
		}
		#pragma omp barrier
		#pragma omp critical
		{
			//for( auto it = myres.begin(); it != myres.end(); it++ ) {
			for ( auto it = my_omit_res.begin(); it != my_omit_res.end(); it++ ) {
				//ions.ionifo.at(it->first).mask1 = it->second;
				//do not check whether ions have already set to ANALYZE_NO
				ions.ionifo.at(*it).mask1 = ANALYZE_NO;
			}
			omitted += (apt_uint) my_omit_res.size();
			//myres = map<apt_uint, unsigned char>();
			my_omit_res = vector<apt_uint>();
		}
	}

	cout << "Applied multiplicity remove filter omitted " << omitted << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "ApplyMultiplicityRemoveFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
}


template<typename T>
void mpiHdl::apply_roi_ensemble_accept_filter( const enum MYPRIMITIVE_TYPES primtyp, vector<T> & union_of_prims )
{
	double tic = MPI_Wtime();

	apt_uint accepted = 0;
	//apt_uint omitted = 0;
	//we assume the filter is applied after the none_filter was used
	//so this filter marks which ions are inside or on the boundary of
	//any of the primitives, assuming before the none filter was applied
	#pragma omp parallel shared(accepted)
	{
		apt_uint my_accepted = 0;
		vector<apt_uint> my_accept_res;
		apt_uint nions = (apt_uint) ions.ionpp3.size();
		#pragma omp for
		for( apt_uint i = 0; i < nions; i++ ) {
			p3d thisone = ions.ionpp3[i];
			for( auto it = union_of_prims.begin(); it != union_of_prims.end(); it++ ) {
				if ( it->is_inside(thisone) == true ) {
					my_accept_res.push_back( i );
					break; //inside one member of the union
				}
			} //test eventually all members of the union for a given type of primitives
		}
		my_accepted += ((apt_uint) my_accept_res.size());
		#pragma omp barrier
		#pragma omp critical
		{
			for( auto it = my_accept_res.begin(); it != my_accept_res.end(); it++ ) {
				ions.ionifo[*it].mask1 = ANALYZE_YES;
			}
			accepted += my_accepted;
			my_accept_res = vector<apt_uint>();
		}
	}

	cout << "Applied ROI ensemble filter accepted " << accepted << "\n";

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();

	switch(primtyp)
	{
		case CG_SPHERE:
		{
			cout << "Applied union of cylinder accept filter accepted " << accepted << "\n";
			base_tictoc.prof_elpsdtime_and_mem( "ApplyROIUnionOfSpheresFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
			break;
		}
		case CG_CYLINDER:
		{
			cout << "Applied union of cylinder accept filter accepted " << accepted << "\n";
			base_tictoc.prof_elpsdtime_and_mem( "ApplyROIUnionOfCylindersFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
			break;
		}
		case CG_CUBOID:
		{
			cout << "Applied union of cuboids accept filter accepted " << accepted << "\n";
			base_tictoc.prof_elpsdtime_and_mem( "ApplyROIUnionOfPrimsFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
			break;
		}
		/*
		case CG_POLYHEDRON:
		{
			cout << "Applied union of polyhedron accept filter accepted " << accepted << "\n";
			base_tictoc.prof_elpsdtime_and_mem( "ApplyROIUnionOfPolyhedraFilter", APT_XX, APT_IS_PAR, mm, tic, toc);
			break;
		}
		*/
		default:
			break;
	}
}

template void mpiHdl::apply_roi_ensemble_accept_filter<roi_sphere>(
		const enum MYPRIMITIVE_TYPES primtyp, vector<roi_sphere> & union_of_prims );
template void mpiHdl::apply_roi_ensemble_accept_filter<roi_rotated_cylinder>(
		const enum MYPRIMITIVE_TYPES primtyp, vector<roi_rotated_cylinder> & union_of_prims );
template void mpiHdl::apply_roi_ensemble_accept_filter<roi_rotated_cuboid>(
		const enum MYPRIMITIVE_TYPES primtyp, vector<roi_rotated_cuboid> & union_of_prims );


void mpiHdl::check_filter()
{
	apt_uint accepted = 0;
	apt_uint omitted = 0;
	for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
		if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
			continue;
		}
		else {
			omitted++;
		}
	}

	accepted = ((apt_uint) ions.ionifo.size()) - omitted;
	cout << "Filter state accepted " << accepted << " omitted " << omitted <<
			" total ion fraction accepted " << ((double) accepted) / ((double) (accepted + omitted)) << "\n";
	//return pair<apt_uint, apt_uint>( accepted, omitted );
}


bool mpiHdl::write_filter_state( const string h5fn, const string dsnm )
{
	double tic = MPI_Wtime();

	if ( get_myrank() == MASTER ) {
		//##MK::store a bitmask which details which ions were taken

		HdfFiveSeqHdl h5w = HdfFiveSeqHdl( h5fn );
		vector<unsigned char> u08 = vector<unsigned char>( ions.ionifo.size(), ANALYZE_NO );
		for( size_t i = 0; i < ions.ionifo.size(); i++ ) {
			if ( ions.ionifo[i].mask1 == ANALYZE_YES ) {
				u08[i] = ANALYZE_YES;
			}
		}

		//##MK::recursively create groups
		if ( h5w.nexus_write(
				dsnm,
				io_info({u08.size()}, {u08.size()},
				MYHDF5_COMPRESSION_GZIP, 0x01),
				u08, ioAttributes() ) == MYHDF5_SUCCESS ) {
			cout << dsnm << " written successfully" << "\n";
		}
		else {
			cerr << dsnm << " writing failed !" << "\n";
		}
	}

	double toc = MPI_Wtime();
	memsnapshot mm = memsnapshot();
	base_tictoc.prof_elpsdtime_and_mem( "WriteFilterState", APT_XX, APT_IS_SEQ, mm, tic, toc);
    return true;
}


bool init_results_nxs( const string nx5fn, const string toolname, const string start_time, const apt_uint entry_id )
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( nx5fn );
	if( h5w.nexus_create() != MYHDF5_SUCCESS ) { return false; }

	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string sub_grpnm = "";
	string dsnm = "";

	grpnm = "/entry" + to_string(entry_id);
	anno = ioAttributes();
	anno.add( "NX_class", string("NXentry") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/definition";
	string appdef_name = "NXapm_paraprobe_" + toolname + "_results";
	anno = ioAttributes();
	anno.add( "version", string(NEXUS_VERSION) );
	if ( h5w.nexus_write( dsnm, appdef_name, anno ) != MYHDF5_SUCCESS ) { return false; }

	grpnm = "/entry" + to_string(entry_id) + "/common";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXapm_paraprobe_tool_common") );
	if ( h5w.nexus_write_group( grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/analysis_identifier";
	apt_uint identifier = ConfigShared::SimID;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, identifier, anno ) != MYHDF5_SUCCESS ) { return false; }

	sub_grpnm = grpnm + "/config";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXserialized") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	dsnm = sub_grpnm + "/type";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, string("file"), anno ) != MYHDF5_SUCCESS ) { return false; }
	string config_file_sha256 = SHA256( ConfigShared::ConfigurationFile );
	string config_file_name = ConfigShared::ConfigurationFile;
	dsnm = sub_grpnm + "/path";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, config_file_name, anno ) != MYHDF5_SUCCESS ) { return false; }
	dsnm = sub_grpnm + "/checksum";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, config_file_sha256, anno ) != MYHDF5_SUCCESS ) { return false; }
	dsnm = sub_grpnm + "/algorithm";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, string("sha256"), anno ) != MYHDF5_SUCCESS ) { return false; }

	sub_grpnm = grpnm + "/program1";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXprogram") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	string program_name = "paraprobe-toolbox-" + toolname;
	dsnm = sub_grpnm + "/program";
	anno = ioAttributes();
	//anno.add( "version", string(xstr(GITSHA)) );
	string auto_gitsha = string(xstr(GITSHA));
	string version = "https://gitlab.com/paraprobe/paraprobe-toolbox/-/releases/" + auto_gitsha;
	if ( auto_gitsha.size() > 0 ) {
		if ( auto_gitsha[0] != 'v' ) {
			version = "https://gitlab.com/paraprobe/paraprobe-toolbox/-/commit/" + auto_gitsha;
		}
	}
	anno.add( "version", string(version) );
	anno.add( "preprocessor_date", string(__DATE__) );
	anno.add( "preprocessor_time", string(__TIME__) );
	if ( h5w.nexus_write( dsnm, program_name, anno ) != MYHDF5_SUCCESS ) { return false; }

	sub_grpnm = grpnm + "/profiling";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcs_profiling") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	
	dsnm = sub_grpnm + "/start_time";
	string start_time_stamp = start_time;
	anno = ioAttributes();
	//MK::IF THE VALUE to WRITE IS A STRING IT MUST NOT BE A CONST STRING
	if ( h5w.nexus_write( dsnm, start_time_stamp, anno ) != MYHDF5_SUCCESS ) { return false; }

	sub_grpnm = grpnm + "/coordinate_system_set";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcoordinate_system_set") );
	if ( h5w.nexus_write_group( sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }

	string sub_sub_grpnm = sub_grpnm + "/paraprobe";
	anno = ioAttributes();
	anno.add( "NX_class", string("NXcoordinate_set") );
	if ( h5w.nexus_write_group( sub_sub_grpnm, anno ) != MYHDF5_SUCCESS ) { return false; }
	dsnm = sub_sub_grpnm + "/type";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, string("cartesian"), anno ) != MYHDF5_SUCCESS ) { return false; }
	dsnm = sub_sub_grpnm + "/handedness";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, string("right_handed"), anno ) != MYHDF5_SUCCESS ) { return false; }
	vector<string> axis_name = { "x", "y", "z" };
	for( size_t i = 0; i < 3; i++ ) {
		dsnm = sub_sub_grpnm + "/" + axis_name[i];
		vector<apt_real> real = vector<apt_real>( 3, MYZERO );
		real[i] = MYONE;
		anno = ioAttributes();
		// anno.add( "depends_on", string(".") );
		// anno.add( "offset", string("{MYZERO, MYZERO, MYZERO}, storing 1d array as an attribute value not yet implemented") ); //##MK::not yet implemented
		// anno.add( "offset_units", string("nm") );
		if ( h5w.nexus_write(
				dsnm,
				io_info({3, 1}, {3, 1}),
				real,
				anno) != MYHDF5_SUCCESS ) { return false; }
	}

	return true;
}


bool finish_results_nxs( const string nx5fn, const string start_time, const double tic, const apt_uint entry_id )
{
	HdfFiveSeqHdl h5w = HdfFiveSeqHdl( nx5fn );
	ioAttributes anno = ioAttributes();
	string grpnm = "";
	string dsnm = "";

	grpnm = "/entry" + to_string(entry_id) + "/common";
	string sub_grpnm = grpnm + "/profiling";
	dsnm = sub_grpnm + "/current_working_directory";
	string cwd = get_current_working_directory();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, cwd, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/number_of_processes";
	apt_uint uint = 1;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, uint, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/number_of_threads";
	uint = (apt_uint) omp_get_max_threads();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, uint, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/number_of_gpus";
	uint = 0;
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, uint, anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = grpnm + "/status";
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, string("success"), anno ) != MYHDF5_SUCCESS ) { return false; }

	dsnm = sub_grpnm + "/end_time";
	string end_time = timestamp_now_iso8601();
	anno = ioAttributes();
	if ( h5w.nexus_write( dsnm, end_time, anno ) != MYHDF5_SUCCESS ) { return false; }

	double wall_clock = omp_get_wtime() - tic;
	dsnm = sub_grpnm + "/total_elapsed_time";
	anno = ioAttributes();
	anno.add( "unit", string("s") );
	if ( h5w.nexus_write( dsnm, wall_clock, anno ) != MYHDF5_SUCCESS ) { return false; }
	return true;
}
