/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_TOOLSBASEHDL_H__
#define __PARAPROBE_UTILS_TOOLSBASEHDL_H__

#include "PARAPROBE_TriangleSoupHdl.h"

class mpiHdl
{
	//process-level class which implements the worker instance
	//specimens result are written to a specifically-formatted HDF5 file

public:
	mpiHdl();
	~mpiHdl();
	
	void commit_mpidatatypes();
	void free_mpidatatypes();
	int get_myrank();
	int get_nranks();
	void set_myrank( const int rr );
	void set_nranks( const int nn );
	bool all_healthy_still( const int myhealth );

	//read function which do not test for the length of linked arrays
	//linked arrays are e.g. xyz and mq because for every ion position we also need to have a mass-to-charge
	bool read_xyz_from_h5( const string h5fn, const string dsnm );
	bool read_mq_from_h5( const string h5fn, const string dsnm );
	bool read_ranging_from_h5( const string h5fn, const string grpnm );
	bool read_ionlabels_from_h5( const string h5fn, const string grpnm );
	bool read_distance_dsetedge_from_h5( const string h5fn, const string dsnm );
	bool read_mesh_dsetedge_from_h5( const string h5fn, const string dsnm_vrts, const string dsnm_fcts );

	void itype_sensitive_spatial_decomposition();

	void apply_none_filter();
	void apply_all_filter();
	void apply_linear_subsampling_filter( lival<apt_uint> const & ionids );
	void apply_iontype_remove_filter( vector<unsigned char> const & ityp_list, const bool is_blacklist );
	//void apply_iontype_accept_filter( vector<unsigned char> const & ityp_whitelist );
	void apply_multiplicity_remove_filter( vector<unsigned char> const & mltply_list, const bool is_blacklist );
	template<typename T>
	void apply_roi_ensemble_accept_filter( const enum MYPRIMITIVE_TYPES primtyp, vector<T> & union_of_prims );
	void check_filter();

	bool write_filter_state( const string h5fn, const string dsnm );

	triangleSoup edge;
	ionCloud ions;
	rangeTable rng;
	
	profiler base_tictoc;
	
	/*
	//common paraprobe-specific datatypes
	MPI_Datatype MPI_Point3D_Type;
	MPI_Datatype MPI_Triangle3D_Type;
	MPI_Datatype MPI_MQIval_Type;
	MPI_Datatype MPI_Iontype_Type;
	*/

private:
	//MPI related
	int myrank;										//my MPI process ID in MPI_COMM_WORLD communicator group
	int nranks;										//total number of MPI processes in MPI_COMM_WORLD
};


bool init_results_nxs( const string nx5fn, const string toolname, const string start_time, const apt_uint entry_id );


bool finish_results_nxs( const string nx5fn, const string start_time, const double tic, const apt_uint entry_id );


#endif

