//##MK::GPLV3

#include "PARAPROBE_TwoPointStats3D.h"

//implements functionality to compute two-point correlation functions in spherical environment about ion location supporting
//by 3d binning with binwidth _width to identify directionality dependent features and perform methods as developed by Kalidindi et al.


ostream& operator<<(ostream& in, d3dd const & val)
{
	in << "dx/dy/dz/dSQR " << val.dx << ", " << val.dy << ", " << val.dz << ", " << val.dSQR << "\n";
	return in;
}


sdm3d::sdm3d()
{
	cnts = vector<apt_uint>();
	cnts_unexpected = 0;
	box = voxelgrid();
}


sdm3d::sdm3d( const apt_real rmax, const apt_real width, const apt_int nmax )
{
	//probe first of all expected number of bins, bin aggregate is a cube with odd number of bins along edge, cubic bins of width _width
	//MK::uses right-handed coordinate system with origin centered at ((support.ni - 1) + 0.5)*support.width	
	p3d org = p3d(MYZERO, MYZERO, MYZERO);
	box = voxelgrid( org, rmax, width );
	if ( box.nxyz > 0 ) {
		cnts_unexpected = 0;
		try {
			cnts = vector<apt_uint>( box.nxyz, 0 );
		}
		catch (bad_alloc &croak) {
			cerr << "TwoPointStatistics initialization allocation error for cnts!" << "\n";
			return;
		}
	}
}


sdm3d::~sdm3d()
{
	cnts = vector<apt_uint>();
}


void sdm3d::add( d3dd const & where )
{
	//MK::add only if we are within the length of the maximum size feature vector
	//MK::where uvw coordinates can be negative translation into local bin coordinate system necessary
	if ( where.dSQR <= box.SQRRmax ) {
		//MK::cast issues http://jkorpela.fi/round.html, https://www.cs.cmu.edu/~rbd/papers/cmj-float-to-int.html
		//##MK::binning like so int discretelyhere = there, we get concentration at the 0 component values symmetrical to coordinates undesired
		apt_int targetbin = 0;
		apt_real there = where.dx / box.dx;
		apt_int here = (there >= MYZERO) ? (apt_int) (there+0.5) : (apt_int) (there-0.5);
		targetbin = targetbin + (here + box.origin_offset.x)*1;
		//cout << where.dx << "\t\t" << there << "\t\t" << here << "\t\t" << targetbin << "\n";

		there = where.dy / box.dy;
		here = (there >= MYZERO) ? (apt_int) (there+0.5) : (apt_int) (there-0.5);
		targetbin = targetbin + (here + box.origin_offset.y)*box.nx;
		//cout << where.dy << "\t\t" << there << "\t\t" << here << "\t\t" << targetbin << "\n";

		there = where.dz / box.dz;
		here = (there >= MYZERO) ? (apt_int) (there+0.5) : (apt_int) (there-0.5);
		targetbin = targetbin + (here + box.origin_offset.z)*box.nxy;
		//cout << where.dz << "\t\t" << there << "\t\t" << here << "\t\t" << targetbin << "\n";

		//cout << where.w << "\t\t" << there << "\t\t" << targetbin << endl;
		cnts[targetbin]++; //.at(targetbin)++;
	}
	else {
		cnts_unexpected++;
	}
}
