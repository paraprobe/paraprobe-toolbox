/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_UserStructs.h"


ostream& operator<<(ostream& in, p3dinfo const & val)
{
	in << "dist " << val.dist << "\n";
	in << "iorg/irnd " << (int) val.i_org << ", " << (int) val.i_rnd << "\n";
	in << "mltply/mask1 " << (int) val.mltply << ", " << (int) val.mask1 << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dinfo_idx const & val) {
	in << "dist/ionid " << val.dist << ", " << val.IonID << ", " << "\n";
	in << "iorg/irnd " << (int) val.i_org << ", " << (int) val.i_rnd << "\n";
	in << "mask1/mask2 " << (int) val.mask1 << ", " << (int) val.mask2 << "\n";
	return in;
}


ostream& operator<<(ostream& in, p3dm2 const & val) {
	in << "ityp_org/ityp_rnd\t\t" << val.i_org << ";" << val.i_rnd << ";";
	in << val.x << ";" << val.y << ";" << val.z << "\n";
	return in;
}


ostream& operator<<(ostream& in, peak const & val)
{
	//##MK::for FFT peaks
	in << "Peak position " << val.position << " strength " << val.intensity << "\n";
	return in;
}
