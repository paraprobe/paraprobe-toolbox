/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __PARAPROBE_UTILS_USERSTRUCTS_H__
#define __PARAPROBE_UTILS_USERSTRUCTS_H__

#include "PARAPROBE_Geometry.h"


struct p3dinfo
{
	apt_real dist;				//placeholder to store case-dependent distance of point to a geometric primitive or the shortest distance to an ensemble of primitives (e.g. for edge detection)
	unsigned char i_org;		//iontype originally assigned
	unsigned char i_rnd;		//iontype randomized from the originally assigned
	unsigned char mltply;		//hit multiplicity group of the ion
	unsigned char mask1;		//analysis mask group of the ion (assigned case-specific to allow fast filtering of ions according to e.g. inclusion in primitives or otherwise filtered analyses)
	p3dinfo() : dist(RMX), i_org(UNKNOWNTYPE), i_rnd(UNKNOWNTYPE), mltply(0x00), mask1(0x00) {}
	p3dinfo( const apt_real _d, const unsigned char _org, const unsigned char _rnd, const unsigned char _mltply, const unsigned char _mask1 ) :
			dist(_d), i_org(_org), i_rnd(_rnd), mltply(_mltply), mask1(_mask1) {}
};

ostream& operator<<(ostream& in, p3dinfo const & val);


struct p3dinfo_idx
{
	//properties of an ion which should be kept together to save cache space
	apt_real dist;				//a distance value
	apt_uint IonID;				//the global, ion_id
	unsigned char i_org;		//a mark, interpreted as the unrandomized ranged iontype
	unsigned char i_rnd;		//a mark, interpreted as the randomized ranged iontype
	unsigned char mask1;		//further masks that could be used, here to fill the struct (padding)
	unsigned char mask2;
	p3dinfo_idx() : dist(RMX), IonID(UMX), i_org(0x00), i_rnd(0x00), mask1(0x00), mask2(0x00) {}
	p3dinfo_idx( const apt_real _d, const apt_uint _id, const unsigned char _org, const unsigned char _rnd ) :
			dist(_d), IonID(_id), i_org(_org), i_rnd(_rnd), mask1(0x00), mask2(0x00) {}
	p3dinfo_idx( const apt_real _d, const apt_uint _id, const unsigned char _org, const unsigned char _real, const unsigned char _ghost ) :
				dist(_d), IonID(_id), i_org(_org), i_rnd(_org), mask1(_real), mask2(_ghost) {}
};

ostream& operator<<(ostream& in, p3dinfo_idx const & val);


struct p3dm2
{
	//3d point with
	apt_real x;
	apt_real y;
	apt_real z;
	unsigned char i_org;   		//for storing original and randomized iontype label
	unsigned char i_rnd;
	unsigned char mask1; 		//two filter masks
	unsigned char mask2;
	p3dm2() : x(MYZERO), y(MYZERO), z(MYZERO), i_org(0x00), i_rnd(0x00), mask1(0x00), mask2(0x00)  {}
	p3dm2( const apt_real _x, const apt_real _y, const apt_real _z,
			const unsigned char _org, const unsigned char _rnd ) :
			x(_x), y(_y), z(_z), i_org(_org), i_rnd(_rnd), mask1(0x00), mask2(0x00) {}
	p3dm2( const apt_real _x, const apt_real _y, const apt_real _z,
			const unsigned char _org, const unsigned char _rnd,
				const unsigned char _msk1, const unsigned char _msk2 ) :
				x(_x), y(_y), z(_z), i_org(_org), i_rnd(_rnd), mask1(_msk1), mask2(_msk2) {}
};

ostream& operator<<(ostream& in, p3dm2 const & val);


struct peak
{
	apt_real position;
	apt_real intensity;
	peak() : position(MYZERO), intensity(MYZERO) {}
	peak( const apt_real _position, const apt_real _intensity ) :
		position(_position), intensity(_intensity) {}
};

ostream& operator<<(ostream& in, peak const & val);


inline bool SortPeakForIntensityAsc(const peak & a, const peak & b)
{
	return a.intensity < b.intensity;
}


inline bool SortSQRLenAscending( const v3d & aa1, const v3d & aa2)
{
	apt_real len1 = SQR(aa1.u)+SQR(aa1.v)+SQR(aa1.w);
	apt_real len2 = SQR(aa2.u)+SQR(aa2.v)+SQR(aa2.w);
	return len1 < len2;
}


inline bool SortNeighborsForAscDistance(const p1d & a, const p1d & b)
{
	return a.x < b.x;
}

#endif
