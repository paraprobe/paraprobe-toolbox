/*
* This file is part of paraprobe-toolbox.
*
* paraprobe-toolbox is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License,
*  or (at your option) any later version.
*
* paraprobe-toolbox is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
*/

#include "PARAPROBE_XDMFBaseHdl.h"


xdmf_geom::xdmf_geom( vector<size_t> const & _dims,
		const enum MYHDF5_DATATYPES _dtyp, const string _lnk )
{
	geometrytype = "XYZ";
	dimensions = "";
	for( size_t i = 0; i < _dims.size()-1; i++ ) {
		dimensions += to_string(_dims[i]);
		dimensions += " ";
	}
	dimensions += to_string(_dims[_dims.size()-1]);
	switch(_dtyp)
	{
		case H5_U32:
		{
			numbertype = "UInt";
			precision = "4";
			break;
		}
		case H5_F32:
		{
			numbertype = "Float";
			precision = "4";
			break;
		}
		case H5_F64:
		{
			numbertype = "Double";
			precision = "8";
			break;
		}
		default:
			return;
	}
	format = "HDF";
	link = _lnk;
};



xdmfBaseHdl::xdmfBaseHdl()
{
	grid = xdmf_grid();
	topo = xdmf_topo();
	geom = xdmf_geom();
	attrs = vector<xdmf_attr>();
}


xdmfBaseHdl::~xdmfBaseHdl()
{
}


void xdmfBaseHdl::add_grid_uniform( const string nm )
{
	grid.type = "Uniform";
	grid.name = nm;
}


/*
template<typename T>
void xdmfBaseHdl::add_topology_triangle<T>( size_t nelem, const string h5link )
{
	topo.type = "Triangle";
	topo.number_of_elements = to_string(nelem);
	topo.dimensions = "";
	topo.dimensions += to_string(nelem) + " " + to_string(4);
	if constexpr(std::is_same<T, unsigned int>::value) {
		topo.numbertype = "UInt";
		topo.precision = "4";
	}
	else if constexpr(std::is_same<T, int>::value) {
		topo.numbertype = "Int";
		topo.precision = "4";
	}
	else if constexpr(std::is_same<T, float>::value) {
		topo.numbertype = "Float";
		topo.precision = "4";
	}
	else if constexpr(std::is_same<T, double>::value) {
		topo.numbertype = "Double";
		topo.precision = "8";
	}
	else {
		//nop
	}
	topo.format = "HDF";
	topo.link = h5link;
}

template void xdmfBaseHdl::add_topology_triangle<unsigned int>( size_t nelem, const string h5link );
template void xdmfBaseHdl::add_topology_triangle<int>( size_t nelem, const string h5link );
template void xdmfBaseHdl::add_topology_triangle<float>( size_t nelem, const string h5link );
template void xdmfBaseHdl::add_topology_triangle<double>( size_t nelem, const string h5link );
*/


void xdmfBaseHdl::add_topology_triangle( size_t nelem, enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	topo.type = "Triangle";
	topo.number_of_elements = to_string(nelem);
	topo.dimensions = "";
	topo.dimensions += to_string(nelem) + " " + to_string(4);
	switch(dtyp)
	{
		case H5_U32:
		{
			topo.numbertype = "UInt";
			topo.precision = "4";
			break;
		}
		case H5_I32:
		{
			topo.numbertype = "Int";
			topo.precision = "4";
			break;
		}
		case H5_F32:
		{
			topo.numbertype = "Float";
			topo.precision = "4";
			break;
		}
		case H5_F64:
		{
			topo.numbertype = "Double";
			topo.precision = "8";
			break;
		}
		default:
			return;
	}
	topo.format = "HDF";
	topo.link = h5link;
}


void xdmfBaseHdl::add_topology_mixed( size_t nelem, vector<size_t> const & dims,
		enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	topo.type = "Mixed";
	topo.number_of_elements = to_string(nelem);
	topo.dimensions = "";
	for( size_t i = 0; i < dims.size()-1; i++ ) {
		topo.dimensions += to_string(dims[i]);
		topo.dimensions += " ";
	}
	topo.dimensions += to_string(dims[dims.size()-1]);
	switch(dtyp)
	{
		case H5_U32:
		{
			topo.numbertype = "UInt";
			topo.precision = "4";
			break;
		}
		case H5_F32:
		{
			topo.numbertype = "Float";
			topo.precision = "4";
			break;
		}
		case H5_F64:
		{
			topo.numbertype = "Double";
			topo.precision = "8";
			break;
		}
		default:
			return;
	}
	topo.format = "HDF";
	topo.link = h5link;
}


void xdmfBaseHdl::add_geometry_xyz( vector<size_t> const & dims,
		enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	geom.geometrytype = "XYZ";
	geom.dimensions = "";
	for( size_t i = 0; i < dims.size()-1; i++ ) {
		geom.dimensions += to_string(dims[i]);
		geom.dimensions += " ";
	}
	geom.dimensions += to_string(dims[dims.size()-1]);
	switch(dtyp)
	{
		case H5_U32:
		{
			geom.numbertype = "UInt";
			geom.precision = "4";
			break;
		}
		case H5_F32:
		{
			geom.numbertype = "Float";
			geom.precision = "4";
			break;
		}
		case H5_F64:
		{
			geom.numbertype = "Double";
			geom.precision = "8";
			break;
		}
		default:
			return;
	}
	geom.format = "HDF";
	geom.link = h5link;
}


void xdmfBaseHdl::add_attribute_scalar( const string nm,
		const string center, vector<size_t> const & dims,
		enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	attrs.push_back( xdmf_attr() );
	attrs.back().attrtype = "Scalar";
	attrs.back().center = center;
	attrs.back().name = nm;
	attrs.back().dimensions = "";
	for( size_t i = 0; i < dims.size()-1; i++ ) {
		attrs.back().dimensions += to_string(dims[i]);
		attrs.back().dimensions += " ";
	}
	attrs.back().dimensions += to_string(dims[dims.size()-1]);
	switch(dtyp)
	{
		case H5_U8:
		{
			attrs.back().datatype = "UChar";
			attrs.back().precision = "1";
			break;
		}
		case H5_U32:
		{
			attrs.back().datatype = "UInt";
			attrs.back().precision = "4";
			break;
		}
		case H5_F32:
		{
			attrs.back().datatype = "Float";
			attrs.back().precision = "4";
			break;
		}
		case H5_F64:
		{
			attrs.back().datatype = "Double";
			attrs.back().precision = "8";
			break;
		}
		default:
			return;
	}
	attrs.back().format = "HDF";
	attrs.back().link = h5link;
}


void xdmfBaseHdl::add_attribute_vector( const string nm,
		const string center, vector<size_t> const & dims,
		enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	attrs.push_back( xdmf_attr() );
	attrs.back().attrtype = "Vector";
	attrs.back().center = center;
	attrs.back().name = nm;
	attrs.back().dimensions = "";
	for( size_t i = 0; i < dims.size()-1; i++ ) {
		attrs.back().dimensions += to_string(dims[i]);
		attrs.back().dimensions += " ";
	}
	attrs.back().dimensions += to_string(dims[dims.size()-1]);
	switch(dtyp)
	{
		case H5_U32:
		{
			attrs.back().datatype = "UInt";
			attrs.back().precision = "4";
			break;
		}
		case H5_F32:
		{
			attrs.back().datatype = "Float";
			attrs.back().precision = "4";
			break;
		}
		case H5_F64:
		{
			attrs.back().datatype = "Double";
			attrs.back().precision = "8";
			break;
		}
		default:
			return;
	}
	attrs.back().format = "HDF";
	attrs.back().link = h5link;
}


void xdmfBaseHdl::write( const string xmlfn )
{
	xdmfout.open( xmlfn.c_str() );
	if ( xdmfout.is_open() == true ) {
		xdmfout << XDMF_HEADER_LINE1 << "\n";
		xdmfout << XDMF_HEADER_LINE2 << "\n";
		xdmfout << XDMF_HEADER_LINE3 << "\n";

		xdmfout << "  <Domain>" << "\n";
		xdmfout << "    <Grid Name=\"" << grid.name << "\" GridType=\"" << grid.type << "\">" << "\n";
		xdmfout << "      <Topology TopologyType=\"" << topo.type << "\" NumberOfElements=\"" << topo.number_of_elements << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << topo.dimensions << "\" NumberType=\"" << topo.numbertype << "\" Precision=\"" <<
									topo.precision << "\" Format=\"" << topo.format << "\">" << "\n";
		xdmfout << "          " << topo.link << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Topology>" << "\n";
		xdmfout << "      <Geometry GeometryType=\"" << geom.geometrytype << "\">" << "\n";
		xdmfout << "        <DataItem Dimensions=\"" << geom.dimensions << "\" NumberType=\"" << geom.numbertype << "\" Precision=\"" <<
									geom.precision << "\" Format=\"" << geom.format << "\">" << "\n";
		xdmfout << "          " << geom.link << "\n";
		xdmfout << "        </DataItem>" << "\n";
		xdmfout << "      </Geometry>" << "\n";

		for( auto it = attrs.begin(); it != attrs.end(); it++ ) {
			xdmfout << "      <Attribute AttributeType=\"" << it->attrtype << "\" Center=\"" << it->center << "\" Name=\"" << it->name << "\">" << "\n";
			xdmfout << "        <DataItem Dimensions=\"" << it->dimensions << "\" DataType=\"" << it->datatype << "\" Precision=\"" <<
									it->precision << "\" Format=\"" << it->format << "\">" << "\n";
			xdmfout << "           " << it->link << "\n";
			xdmfout << "        </DataItem>" << "\n";
			xdmfout << "      </Attribute>" << "\n";
		}
 		xdmfout << "    </Grid>" << "\n";
		xdmfout << "  </Domain>" << "\n";
		xdmfout << "</Xdmf>" << "\n";

		xdmfout.flush();
		xdmfout.close();
		//return WRAPPED_XDMF_SUCCESS;
	}
	//else {
	//	return WRAPPED_XDMF_IOFAILED;
	//}
}


void xdmfBaseHdl::init_collection()
{
	collection = vector<xdmf_coll_entry>();
}


void xdmfBaseHdl::add_collection_entry()
{
	collection.push_back( xdmf_coll_entry() );
}


void xdmfBaseHdl::add_collection_grid_uniform( const string nm )
{
	if ( collection.size() >= 1 ) {
		collection.back().grid.type = "Uniform";
		collection.back().grid.name = nm;
	}
}


void xdmfBaseHdl::add_collection_topology_mixed( size_t nelem, vector<size_t> const & dims,
		enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	if ( collection.size() >= 1 ) {
		collection.back().topo.type = "Mixed";
		collection.back().topo.number_of_elements = to_string(nelem);
		collection.back().topo.dimensions = "";
		for( size_t i = 0; i < dims.size()-1; i++ ) {
			collection.back().topo.dimensions += to_string(dims[i]);
			collection.back().topo.dimensions += " ";
		}
		collection.back().topo.dimensions += to_string(dims[dims.size()-1]);
		switch(dtyp)
		{
			case H5_U32:
			{
				collection.back().topo.numbertype = "UInt";
				collection.back().topo.precision = "4";
				break;
			}
			case H5_F32:
			{
				collection.back().topo.numbertype = "Float";
				collection.back().topo.precision = "4";
				break;
			}
			case H5_F64:
			{
				collection.back().topo.numbertype = "Double";
				collection.back().topo.precision = "8";
				break;
			}
			default:
				return;
		}
		collection.back().topo.format = "HDF";
		collection.back().topo.link = h5link;
	}
}


void xdmfBaseHdl::add_collection_geometry_xyz( vector<size_t> const & dims, enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	if ( collection.size() >= 1 ) {
		collection.back().geom.geometrytype = "XYZ";
		collection.back().geom.dimensions = "";
		for( size_t i = 0; i < dims.size()-1; i++ ) {
			collection.back().geom.dimensions += to_string(dims[i]);
			collection.back().geom.dimensions += " ";
		}
		collection.back().geom.dimensions += to_string(dims[dims.size()-1]);
		switch(dtyp)
		{
			case H5_U32:
			{
				collection.back().geom.numbertype = "UInt";
				collection.back().geom.precision = "4";
				break;
			}
			case H5_F32:
			{
				collection.back().geom.numbertype = "Float";
				collection.back().geom.precision = "4";
				break;
			}
			case H5_F64:
			{
				collection.back().geom.numbertype = "Double";
				collection.back().geom.precision = "8";
				break;
			}
			default:
				return;
		}
		collection.back().geom.format = "HDF";
		collection.back().geom.link = h5link;
	}
}


void xdmfBaseHdl::add_collection_attribute_scalar( const string nm, const string center, vector<size_t> const & dims,
				enum MYHDF5_DATATYPES dtyp, const string h5link )
{
	if ( collection.size() >= 1 ) {
		collection.back().attr =xdmf_attr();
		collection.back().attr.attrtype = "Scalar";
		collection.back().attr.center = center;
		collection.back().attr.name = nm;
		collection.back().attr.dimensions = "";
		for( size_t i = 0; i < dims.size()-1; i++ ) {
			collection.back().attr.dimensions += to_string(dims[i]);
			collection.back().attr.dimensions += " ";
		}
		collection.back().attr.dimensions += to_string(dims[dims.size()-1]);
		switch(dtyp)
		{
			case H5_U32:
			{
				collection.back().attr.datatype = "UInt";
				collection.back().attr.precision = "4";
				break;
			}
			case H5_F32:
			{
				collection.back().attr.datatype = "Float";
				collection.back().attr.precision = "4";
				break;
			}
			case H5_F64:
			{
				collection.back().attr.datatype = "Double";
				collection.back().attr.precision = "8";
				break;
			}
			default:
				return;
		}
		collection.back().attr.format = "HDF";
		collection.back().attr.link = h5link;
	}
}


void xdmfBaseHdl::write_collection( const string xmlfn )
{
	if ( collection.size() >= 1 ) {
		xdmfout.open( xmlfn.c_str() );
		if ( xdmfout.is_open() == true ) {
			xdmfout << XDMF_HEADER_LINE1 << "\n";
			xdmfout << XDMF_HEADER_LINE2 << "\n";
			xdmfout << XDMF_HEADER_LINE3 << "\n";

			xdmfout << "  <Domain>" << "\n";
			for( auto it = collection.begin(); it != collection.end(); it++ ) {
				xdmfout << "    <Grid Name=\"" << it->grid.name << "\" GridType=\"" << it->grid.type << "\">" << "\n";
				xdmfout << "      <Topology TopologyType=\"" << it->topo.type << "\" NumberOfElements=\"" << it->topo.number_of_elements << "\">" << "\n";
				xdmfout << "        <DataItem Dimensions=\"" << it->topo.dimensions << "\" NumberType=\"" << it->topo.numbertype << "\" Precision=\"" <<
										it->topo.precision << "\" Format=\"" << it->topo.format << "\">" << "\n";
				xdmfout << "          " << it->topo.link << "\n";
				xdmfout << "        </DataItem>" << "\n";
				xdmfout << "      </Topology>" << "\n";
				xdmfout << "      <Geometry GeometryType=\"" << it->geom.geometrytype << "\">" << "\n";
				xdmfout << "        <DataItem Dimensions=\"" << it->geom.dimensions << "\" NumberType=\"" << it->geom.numbertype << "\" Precision=\"" <<
										it->geom.precision << "\" Format=\"" << it->geom.format << "\">" << "\n";
				xdmfout << "          " << it->geom.link << "\n";
				xdmfout << "        </DataItem>" << "\n";
				xdmfout << "      </Geometry>" << "\n";
				xdmfout << "      <Attribute AttributeType=\"" << it->attr.attrtype << "\" Center=\"" << it->attr.center << "\" Name=\"" << it->attr.name << "\">" << "\n";
				xdmfout << "        <DataItem Dimensions=\"" << it->attr.dimensions << "\" DataType=\"" << it->attr.datatype << "\" Precision=\"" <<
											it->attr.precision << "\" Format=\"" << it->attr.format << "\">" << "\n";
				xdmfout << "           " << it->attr.link << "\n";
				xdmfout << "        </DataItem>" << "\n";
				xdmfout << "      </Attribute>" << "\n";
				xdmfout << "    </Grid>" << "\n";
			}
			xdmfout << "  </Domain>" << "\n";
			xdmfout << "</Xdmf>" << "\n";

			xdmfout.flush();
			xdmfout.close();
			//return WRAPPED_XDMF_SUCCESS;
		}
	}
}


p3d leftmultiply( t3x3 const & a, p3d const & b )
{
	return p3d(	(a.a11 + a.a21 + a.a31) * b.x,
				(a.a12 + a.a22 + a.a32) * b.y,
				(a.a13 + a.a23 + a.a33) * b.z );
}


//MK::http://paulbourke.net/geometry/rotate/source.c
/*
   Rotate a point p by angle theta around an arbitrary axis r
   Return the rotated point.
   Positive angles are anticlockwise looking down the axis
   towards the origin.
   Assume right hand coordinate system.
*/

p3d ArbitraryRotate( p3d const & p, p3d const & axis, const apt_real theta )
{
	p3d q = p3d( MYZERO, MYZERO, MYZERO );

	/*
	//rotation axis has to pass through the origin to confirm step1
	apt_real a = axis.x;
	apt_real b = axis.y;
	apt_real c = axis.z;
	apt_real discriminator = SQR(b)+SQR(c);
	if ( discriminator >= EPSILON ) { //it is necessary to rotate the axis into xz plane
		apt_real d = sqrt(discriminator);
		apt_real c_d = c / d;
		apt_real b_d = b / d;
		t3x3 Rx = t3x3();
		Rx.a11 = MYONE;
		Rx.a22 = c_d;
		Rx.a23 = -b_d;
		Rx.a32 = b_d;
		Rx.a33 = c_d;
		t3x3 Rxi = t3x3();
		Rxi.a11 = MYONE;
		Rxi.a22 = c_d;
		Rxi.a23 = b_d;
		Rxi.a32 = -b_d;
		Rxi.a33 = c_d;

		t3x3 Ry = t3x3();
		Ry.a11 = d;
		Ry.a13 = -a;
		Ry.a22 = MYONE;
		Ry.a31 = a;
		Ry.a33 = d;
		t3x3 Ryi = t3x3();
		Ryi.a11 = d;
		Ryi.a13 = a;
		Ryi.a22 = MYONE;
		Ryi.a31 = -a;
		Ryi.a33 = d;

		t3x3 Rz = t3x3();
		Rz.a11 = cos(theta);
		Rz.a12 = -sin(theta);
		Rz.a21 = sin(theta);
		Rz.a22 = cos(theta);
		Rz.a33 = MYONE;

		t3x3 Ryx = Ry.leftmultiply( Rx );
		t3x3 Rzyx = Rz.leftmultiply( Ryx );
		t3x3 Ryizyx = Ryi.leftmultiply( Rzyx );
		t3x3 Rres = Ryizyx.leftmultiply( Ryizyx );

		q = leftmultiply( Rres, p );
		return q;
	}
	*/

	//axis is normalized already
	apt_real costheta = cos(theta);
	apt_real sintheta = sin(theta);

	q.x += (costheta + (MYONE - costheta) * axis.x * axis.x) * p.x;
	q.x += ((MYONE - costheta) * axis.x * axis.y - axis.z * sintheta) * p.y;
	q.x += ((MYONE - costheta) * axis.x * axis.z + axis.y * sintheta) * p.z;

	q.y += ((MYONE - costheta) * axis.x * axis.y + axis.z * sintheta) * p.x;
	q.y += (costheta + (MYONE - costheta) * axis.y * axis.y) * p.y;
	q.y += ((MYONE - costheta) * axis.y * axis.z - axis.x * sintheta) * p.z;

	q.z += ((MYONE - costheta) * axis.x * axis.z - axis.y * sintheta) * p.x;
	q.z += ((MYONE - costheta) * axis.y * axis.z + axis.x * sintheta) * p.y;
	q.z += (costheta + (MYONE - costheta) * axis.z * axis.z) * p.z;

	return q;
}


void create_ngonal_prism( p3d const & p1, p3d const & p2, apt_real const & radius,
		vector<apt_real> & vertices_out, vector<apt_uint> & topology_out, const apt_uint _nfacets )
{
	//render the facet geometry of an approximated cylinder by computing the inscribed n-gon with _nfacets facets and add the bottom and top part
	//p1 is the bottom, p2 is the top

	apt_uint facets = _nfacets;
	if ( facets < 3 )
		facets = 3;
	if ( facets > 360 )
		facets = 360;

	//compute supplementing cylinder geometry quantities
	p3d p2p1 = p3d( p2.x-p1.x, p2.y-p1.y, p2.z-p1.z );
	apt_real SQRlen = SQR(p2p1.x)+SQR(p2p1.y)+SQR(p2p1.z); //p1 is behind the prism barycenter plane, p2 in front
	apt_real nrmlen = MYZERO;
	if ( SQRlen >= EPSILON ) {
		nrmlen = MYONE / sqrt(SQRlen);
		p2p1.x *= nrmlen;
		p2p1.y *= nrmlen;
		p2p1.z *= nrmlen;
		//find minimum coordinate value to construct a vector with which to compute a vector perpendicular to the axis p2p1
		int imi_idx = 0; apt_real imi_val = RMX;
		if ( p2p1.x <= imi_val ) { imi_idx = 0; imi_val = p2p1.x; }
		if ( p2p1.y <= imi_val ) { imi_idx = 1; imi_val = p2p1.y; }
		if ( p2p1.z <= imi_val ) { imi_idx = 2; imi_val = p2p1.z; }
		apt_real tmp[3] = { MYZERO, MYZERO, MYZERO }; tmp[imi_idx] = MYONE;

		p3d crss = cross( p3d(tmp[0],tmp[1],tmp[2]), p2p1 );
		SQRlen = SQR(crss.x)+SQR(crss.y)+SQR(crss.z);
		if ( SQRlen >= EPSILON ) {
			nrmlen = MYONE / sqrt(SQRlen);
			crss.x *= nrmlen;
			crss.y *= nrmlen;
			crss.z *= nrmlen;

			//crss is unit length vector in the bottom or top plane by construction
			size_t voffset = vertices_out.size()/3; //offset to translate local vertexID namespace to global vertexID namespace
			apt_real omega = 2.*MYPI / ((apt_real) facets);
			//add bottom
			vector<p3d> bottom;
			p3d p_prev_bot = p3d(radius*crss.x, radius*crss.y, radius*crss.z);
			bottom.push_back( p3d(p_prev_bot.x + p1.x, p_prev_bot.y + p1.y, p_prev_bot.z + p1.z) );
			for( apt_uint i = 1; i < facets; i++ ) {
				p3d p_next_bot = ArbitraryRotate( p_prev_bot, p2p1, omega );
				bottom.push_back( p3d(p_next_bot.x + p1.x, p_next_bot.y + p1.y, p_next_bot.z + p1.z) );
				p_prev_bot = p_next_bot;
			} //indices [0, facets-1]
			//add top
			vector<p3d> top;
			p3d p_prev_top = p3d(radius*crss.x, radius*crss.y, radius*crss.z);
			top.push_back( p3d(p_prev_top.x + p2.x, p_prev_top.y + p2.y, p_prev_top.z + p2.z) );
			for( apt_uint i = 1; i < facets; i++ ) {
				p3d p_next_top = ArbitraryRotate( p_prev_top, p2p1, omega );
				top.push_back( p3d(p_next_top.x + p2.x, p_next_top.y + p2.y, p_next_top.z + p2.z) );
				p_prev_top = p_next_top;
			} //indices [facets+0, facet+facets-1]

			//now all supporting vertices exists now write down the coordinates
			for( auto it = bottom.begin(); it != bottom.end(); it++ ) {
				vertices_out.push_back( it->x );
				vertices_out.push_back( it->y );
				vertices_out.push_back( it->z );
			}
			for( auto jt = top.begin(); jt != top.end(); jt++ ) {
				vertices_out.push_back( jt->x );
				vertices_out.push_back( jt->y );
				vertices_out.push_back( jt->z );
			}
			//at bottom facet
			topology_out.push_back( 3 ); //XDMF type key for an n-gon
			topology_out.push_back( facets ); //a facets-gon
			for( apt_uint i = 0; i < facets; i++ ) {
				topology_out.push_back( voffset + 0 + i ); //0 only for clarity bottom vertices become before top vertices, the additions will be optimized during compilation
			}
			//add side facets of the facets-gonal prism
			/* see for educational purpose that this one twists the faces into one another creating a self intersection
			for( apt_uint k = 0; k < facets-1; k++ ) { //counter clock wise when looking inside, stop one before the last to avoid checks for wraparound
				//each facet is a quad
				topology_out.push_back( 3 ); //XDMF type key for an n-gon
				topology_out.push_back( 4 );
				topology_out.push_back( voffset + 0 + k ); //bottom "left"
				topology_out.push_back( voffset + 0 + k + 1 ); //bottom "right"
				topology_out.push_back( voffset + facets + k ); //top "left"
				topology_out.push_back( voffset + facets + k + 1 ); //top "right"
			}
			//add "last" facet wrapping around
			topology_out.push_back( 3 ); //XDMF type key for an n-gon
			topology_out.push_back( 4 ); //a quad-
			topology_out.push_back( voffset + 0 + facets-1 ); //bottom "left"
			topology_out.push_back( voffset + 0 + 0 ); //bottom "right", wrap around
			topology_out.push_back( voffset + facets + facets-1 ); //top "left"
			topology_out.push_back( voffset + facets + 0 ); //top "right", wrap around
			*/
			for( apt_uint k = 0; k < facets-1; k++ ) {
				//each facet is a quad
				topology_out.push_back( 3 ); //XDMF type key for an n-gon
				topology_out.push_back( 4 );
				topology_out.push_back( voffset + 0 + k ); //bottom "left"
				topology_out.push_back( voffset + 0 + k + 1 ); //bottom "right"
				topology_out.push_back( voffset + facets + k + 1); //top "right", see the difference here !
				topology_out.push_back( voffset + facets + k ); //top "left"
			}
			//add "last" facet wrapping around
			topology_out.push_back( 3 ); //XDMF type key for an n-gon
			topology_out.push_back( 4 ); //a quad-
			topology_out.push_back( voffset + 0 + facets-1 ); //bottom "left"
			topology_out.push_back( voffset + 0 + 0 ); //bottom "right", wrap around
			topology_out.push_back( voffset + facets + 0 ); //top "right"
			topology_out.push_back( voffset + facets + facets-1 ); //top "left", "double" wrap around
			//add top facet
			topology_out.push_back( 3 ); //XDMF type key for an n-gon
			topology_out.push_back( facets ); //again a facets-gon
			for( apt_uint j = 0; j < facets; j++ ) {
				topology_out.push_back( voffset + facets + j );
			}
			return; //(2 + facets);
		}
		cerr << "crete_ngonal_prism::trying to compute the length of a very short crss vector, this is not stable !" << "\n";
		return; // 0;
	}
	cerr << "crete_ngonal_prism::trying to compute the length of a very short cylinder axis vector, this is not stable !" << "\n";
	//return 0;
}
