#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Utility function to compute specific secure hash values for a file.
#

import hashlib
import numpy as np


def get_file_hashvalue(file_name: str) -> str:
    """Compute a hashvalue of given file, here SHA256."""
    if file_name != "":
        print(f"Computing SHA256 hash for file named {file_name}")
        sha256_hash = hashlib.sha256()
        try:
            with open(file_name, "rb") as file_handle:
                # Read and update hash string value in blocks of 4K
                for byte_block in iter(lambda: file_handle.read(4096), b""):
                    sha256_hash.update(byte_block)
        except IOError:
            print(f"File {file_name} not accessible !")
        return str(sha256_hash.hexdigest())
    return str(0)


def get_simulation_id(file_name: str = "") -> int:
    """Extract SimID from file_name."""
    if not isinstance(file_name, str):
        raise TypeError("Argument file_name needs to be a string!")
    tmp = file_name.split(".")
    if len(tmp) <= 3:  # implicitly handles "" str
        raise ValueError("Argument file_name needs to be a string with formatted *.SimID.<uint32>.nxs !")
    allowed = ["nxs", "hdf", "h5"]
    if tmp[-1] not in allowed:
        raise ValueError("Argument file_name file ending needs to be in {allowed}!")
    simid = np.uint32(tmp[-2])
    if str(simid) != tmp[-2]:
        raise ValueError("Argument file_name needs to have a valid <uint32> part!")
    if simid <= 0 or int(simid) > int(np.iinfo(np.uint32).max):
        raise ValueError(f"SimID needs to be on [1, {np.iinfo(np.uint32).max}]. 0 is for development !")
    return simid
