#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#

import h5py
import numpy as np


def write_strings_to_dataset(
        fid, dsnm, values, length="variable", terminator="nullterm", encoding="utf8"):
    # append to an existent file
    if (isinstance(values, list) and len(values) > 0 and all(isinstance(val, str) for val in values)) or isinstance(values, str):
        if length == "variable":
            # if terminator != "spacepad":
            #     mtypid = h5py.h5t.C_S1.copy()
            # else:
            #     mtypid = h5py.h5t.FORTRAN_S1.copy()
            # only with fortran we get a spacepad otherwise we get nullterm?
            mtypid = h5py.h5t.C_S1.copy()
            mtypid.set_size(h5py.h5t.VARIABLE)

            if terminator == "nullterm":
                mtypid.set_strpad(0)
            elif terminator == "nullpad":
                mtypid.set_strpad(1)
            else:  # spacepad
                mtypid.set_strpad(2)

            if encoding == "utf8":
                mtypid.set_cset(h5py.h5t.CSET_UTF8)
            else:
                mtypid.set_cset(h5py.h5t.CSET_ASCII)

            if isinstance(values, str):
                wdata = np.asarray(np.str_(values), dtype="|O")
                # space = h5py.h5s.create(h5py.h5s.SCALAR)
            else:
                wdata = np.empty((np.shape(values)[0],), "|O")
                for i in np.arange(0, np.shape(values)[0]):
                    wdata[i] = values[i]
                # space = h5py.h5s.create_simple((np.shape(values)[0],))

            fid.create_dataset(dsnm.encode(), dtype=mtypid, data=wdata)
            # dset = h5py.h5d.create(fid.ref, dsnm.encode(), mtypid, space)
            # dset.write(h5py.h5s.ALL, h5py.h5s.ALL, wdata)

            del mtypid
            # del space
            # del dset
        else:  # length == "fixed"
            mtypid = h5py.h5t.C_S1.copy()
            max_size = 0
            if isinstance(values, list):
                for entry in values:
                    if len(entry) >= max_size:
                        max_size = len(entry)
            else:
                max_size = len(values)
            mtypid.set_size(max_size)

            if terminator == "nullterm":
                mtypid.set_strpad(0)
                max_size += 1
            # without this fixed_nullterm will end up with the longest character missing
            elif terminator == "nullpad":
                mtypid.set_strpad(1)
            else:
                mtypid.set_strpad(2)

            if encoding == "utf8":
                mtypid.set_cset(1)
            else:
                mtypid.set_cset(0)

            if isinstance(values, str):
                wdata = np.asarray(np.str_(values), dtype="|S")
                # space = h5py.h5s.create(h5py.h5s.SCALAR)
            else:
                wdata = np.empty((np.shape(values)[0],), "|S")
                for i in np.arange(0, np.shape(values)[0]):
                    wdata[i] = np.string_(values[i])
                # space = h5py.h5s.create_simple((np.shape(wdata)[0],))
            fid.create_dataset(dsnm.encode(), dtype=mtypid, data=wdata)
            # dset = h5py.h5d.create(fid, dsnm.encode(), mtypid, wdata)
            # dset.write(h5py.h5s.ALL, h5py.h5s.ALL, wdata)

            del mtypid
            # del space
            # del dset
    else:
        raise ValueError("values should either be a list of Python strings or a single Python string!")


def write_strings_to_attribute(
        loc, attrnm, values, length="variable", terminator="nullterm", encoding="utf8"):
    # add a Python string or list of Python strings as an attribute
    # to an existent location loc identified for an open file
    if h5py.h5a.exists(loc.id, attrnm.encode()) is True:
        return

    if isinstance(values, str) or (isinstance(values, list) and len(values) > 0 and all(isinstance(val, str) for val in values)):
        if length == "variable":
            mtypid = h5py.h5t.C_S1.copy()
            mtypid.set_size(h5py.h5t.VARIABLE)

            if terminator == "nullterm":
                mtypid.set_strpad(0)
            elif terminator == "nullpad":
                mtypid.set_strpad(1)
            else:  # == "spacepad"
                mtypid.set_strpad(2)

            if encoding == "utf8":
                mtypid.set_cset(h5py.h5t.CSET_UTF8)
            else:
                mtypid.set_cset(h5py.h5t.CSET_ASCII)

            if isinstance(values, str):
                wdata = np.asarray(np.str_(values), dtype="|O")
                space = h5py.h5s.create(h5py.h5s.SCALAR)
            else:
                wdata = np.empty((np.shape(values)[0],), "|O")
                for i in np.arange(0, np.shape(values)[0]):
                    wdata[i] = values[i]
                space = h5py.h5s.create_simple((np.shape(wdata)[0],))

            attr = h5py.h5a.create(loc.id, attrnm.encode(), mtypid, space)
            attr.write(wdata)

            del attr
            del mtypid
            del space
        else:  # length == "fixed"
            max_size = 0
            if isinstance(values, list):
                for entry in values:
                    if len(entry) >= max_size:
                        max_size = len(entry)
            else:
                max_size = len(values)
            mtypid = h5py.h5t.C_S1.copy()
            mtypid.set_size(max_size)

            if terminator == "nullterm":
                mtypid.set_strpad(0)
                max_size += 1
            # without this fixed_nullterm will end up with the longest character missing
            elif terminator == "nullpad":
                mtypid.set_strpad(1)
            else:  # == "spacepad"
                mtypid.set_strpad(2)

            if encoding == "utf8":
                mtypid.set_cset(1)
            else:
                mtypid.set_cset(0)

            if isinstance(values, str):
                wdata = np.asarray(np.str_(values), dtype="|S")
                space = h5py.h5s.create(h5py.h5s.SCALAR)
            else:
                wdata = np.empty((np.shape(values)[0],), "|S")
                for i in np.arange(0, np.shape(values)[0]):
                    wdata[i] = np.string_(values[i])
                space = h5py.h5s.create_simple((np.shape(wdata)[0],))

            attr = h5py.h5a.create(loc.id, attrnm.encode(), h5py.Datatype(mtypid), space)
            attr.write(wdata)

            del attr
            del mtypid
            del space
    else:
        raise ValueError("values has to be either Python string or list of Python strings!")


def read_strings_from_dataset(obj):
    # print(f"type {type(obj)}, np.shape {np.shape(obj)}, obj {obj}")
    # if hasattr(obj, "dtype"):
    #     print(obj.dtype)
    if isinstance(obj, np.ndarray):
        retval = []
        for entry in obj:
            if isinstance(entry, bytes):
                retval.append(entry.decode("utf-8"))
            elif isinstance(entry, str):
                retval.append(entry)
            else:
                continue
                # raise ValueError("Neither bytes nor str inside np.ndarray!")
        # specific implementation rule that all lists with a single string
        # will be returned in paraprobe as a scalar string
        if len(retval) > 1:
            return retval
        if len(retval) == 1:
            return retval[0]
        return None
    if isinstance(obj, bytes):
        return obj.decode("utf8")
    if isinstance(obj, str):
        return obj
    return None
    # raise ValueError("Neither np.ndarray, nor bytes, nor str !")
