#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
"""Python-equivalent of C/C++ iontype and ioncloud classes."""


class IonType():
    """Paraprobe-toolbox representation of an ion."""
    def __init__(self):
        self.charge = None
        self.ivec = None
        self.mqrange = None


class IonCloud():
    """Position and properties of a set of ions as point features in R^3"""
    def __init__(self):
        self.ionpp3 = None
        # reconstructed positions (n_ions, 3) np.float32
        self.ionmq = None
        # mass-to-charge calibrated (n_ions,), np.float32
        self.ionifo_i_org = None
        # label for each ion
        self.ionifo_dist = None
        # optional distance to modelled edge of the dataset (n_ions,) np.float32
