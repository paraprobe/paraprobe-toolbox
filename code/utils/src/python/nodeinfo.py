#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Class to be used for creating NeXus/HDF5 configuration files.


class NodeInfo:
    """Container for data/metadata of an entry used for automated HDF5 I/O."""

    def __init__(self, value=None, unit=None, dtype=str, **kwargs):
        """Initialize the container."""
        # optional shape, dims, and occurs as kwargs arguments
        self.value = None
        self.unit = None
        self.dtype = None
        self.shape = None
        # self.dimensionality = 1
        self.occurs = "required"

        if value is None:
            raise ValueError("Argument value must not be None !")
        if isinstance(value, list):
            if value == []:
                raise ValueError("Argument value list must not be an empty list!")
        self.value = value

        if unit is not None:
            if not isinstance(unit, str):
                raise TypeError("Argument unit needs to be a str !")
            self.unit = unit

        if dtype is None:
            raise ValueError("Argument dtype must not be None !")
        if not isinstance(dtype, type):
            raise ValueError(f"Argument {dtype} needs to be a datatype !")
        self.dtype = dtype

        if "shape" in kwargs:
            if kwargs["shape"] is None:
                raise ValueError("Keyword argument shape must not be None !")
            if not isinstance(kwargs["shape"], tuple):
                raise TypeError("Keyword argument shape needs to be a tuple !")
            self.shape = kwargs["shape"]

        if "occurs" in kwargs:
            if kwargs["occurs"] is None:
                raise ValueError("Keyword argument occurs must not be None !")
            if not isinstance(kwargs["occurs"], str):
                raise TypeError("Keyword argument occurs needs to be a string !")
            allowed = ["required", "optional"]
            if kwargs["occurs"] not in allowed:
                raise ValueError(f"Keyword argument occurs needs to be in {allowed} !")
