#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Utility classes which define geometric primitives to use as regions-of-interest.

import numpy as np

from utils.src.python.numerics import EPSILON


class RoiRotatedCuboid:
    """Represent a rotated bounding box in 3D space."""

    def __init__(self, center=None, boxdims=None, ori=np.asarray([0., 0., 1.])):
        # ori=np.asarray([[1., 0., 0.], [0., 1., 0.], [0., 0., 1.]])):
        """Create a rotated bounding box in 3D space.

        By default the cuboid edges are axis-aligned, i.e. parallel to the
        x, y, z axes of the paraprobe coordinate system.
        Consequently, boxdims represent the size of the cuboid along the
        x (boxdims[0]), y (boxdims[1]), and the z direction (boxdims[2])
        Specification of a normalized direction vector can be used though
        to rotate the box actively.
        """
        if not isinstance(center, (list, np.ndarray)):
            raise TypeError("Argument center needs to be a list or np.ndarray !")
        if np.shape(center) != (3,):
            raise ValueError("Argument center needs to be of shape (3,) !")
        if not all(isinstance(val, float) for val in center):
            raise TypeError("Argument center needs to be an array of float !")
        self.barycenter = center

        if not isinstance(boxdims, (list, np.ndarray)):
            raise TypeError("Argument boxdims needs to be a list or np.ndarray !")
        if np.shape(boxdims) != (3,):
            raise ValueError("Argument boxdims needs to be of shape (3,) !")
        if not all(isinstance(val, float) for val in boxdims):
            raise TypeError("Argument boxdims needs to be an array of float !")
        self.boxdims = boxdims

        if not isinstance(ori, (list, np.ndarray)):
            raise TypeError("Argument ori needs to be a list or np.ndarray !")
        if np.shape(np.asarray(ori)) != (3,):
            raise ValueError("Argument ori needs to be of shape (3,) !")
        if not all(isinstance(val, float) for val in ori):
            raise TypeError("Argument ori needs to be an arrayy of float !")
        if np.linalg.norm(np.asarray(ori)) > (1. + EPSILON) or np.linalg.norm(np.asarray(ori)) < (1. - EPSILON):
            raise ValueError("Argument ori needs to have a norm of 1. !")
        self.orientation = np.asarray(ori)

        # compute the location of the edge vertices based on center,
        # boxdims, and orientation
        pts = np.zeros([8, 3], np.float32)
        pts[0, :] = [-0.5 * boxdims[0], -0.5 * boxdims[1], -0.5 * boxdims[2]]
        pts[1, :] = [+0.5 * boxdims[0], -0.5 * boxdims[1], -0.5 * boxdims[2]]
        pts[2, :] = [-0.5 * boxdims[0], +0.5 * boxdims[1], -0.5 * boxdims[2]]
        pts[3, :] = [+0.5 * boxdims[0], +0.5 * boxdims[1], -0.5 * boxdims[2]]

        pts[4, :] = [-0.5 * boxdims[0], -0.5 * boxdims[1], +0.5 * boxdims[2]]
        pts[5, :] = [+0.5 * boxdims[0], -0.5 * boxdims[1], +0.5 * boxdims[2]]
        pts[6, :] = [-0.5 * boxdims[0], +0.5 * boxdims[1], +0.5 * boxdims[2]]
        pts[7, :] = [+0.5 * boxdims[0], +0.5 * boxdims[1], +0.5 * boxdims[2]]

        f = np.asarray([0., 0., 1.], np.float32)
        tt = np.asarray(ori, np.float32)
        nrm = np.linalg.norm(tt)
        t = tt / nrm
        lenf = np.linalg.norm(f)
        lent = np.linalg.norm(t)
        if (lenf * lent) >= EPSILON:
            arg = np.dot(f, t) / (lenf * lent)
            if np.fabs(arg) < (1. - EPSILON):
                v = np.cross(f, t)
                c = np.dot(f, t)
                h = (1. - c) / (1. - np.sqrt(c))
                rot = np.zeros([3, 3], np.float32)
                rot[0, 0] = c + h * v[0]**2
                rot[0, 1] = h * v[0] * v[1] - v[2]
                rot[0, 2] = h * v[0] * v[2] + v[1]
                rot[1, 0] = h * v[0] * v[1] + v[2]
                rot[1, 1] = c + h * v[1]**2
                rot[1, 2] = h * v[1] * v[2] - v[0]
                rot[2, 0] = h * v[0] * v[2] - v[1]
                rot[2, 1] = h * v[1] * v[2] + v[0]
                rot[2, 2] = c + h * v[2]**2
                self.orientation = t
                for i in np.arange(0, 8):
                    xyz = pts[i, :]
                    pts[i, 0] \
                        = rot[0, 0] * xyz[0] + rot[0, 1] * xyz[1] + rot[0, 2] * xyz[2]
                    pts[i, 1] \
                        = rot[1, 0] * xyz[0] + rot[1, 1] * xyz[1] + rot[1, 2] * xyz[2]
                    pts[i, 2] \
                        = rot[2, 0] * xyz[0] + rot[2, 1] * xyz[1] + rot[2, 2] * xyz[2]

        self.v = pts


class RoiRotatedCylinder:
    """Represent a rotated cylinder in 3D space."""

    def __init__(self, center_bottom_cap=None, center_top_cap=None, radius=None):
        if not isinstance(center_bottom_cap, (list, np.ndarray)):
            raise TypeError("Argument center_bottom_cap needs to be a list or np.ndarray !")
        if np.shape(center_bottom_cap) != (3,):
            raise ValueError("Argument center_bottom_cap needs to of shape (3,) !")
        if not all(isinstance(val, float) for val in center_bottom_cap):
            raise TypeError("Argument center_bottom_cap needs to be an array of float !")
        self.center_bottom_cap = center_bottom_cap

        if not isinstance(center_top_cap, (list, np.ndarray)):
            raise TypeError("Argument center_top_cap needs to be a list or np.ndarray !")
        if np.shape(center_top_cap) != (3,):
            raise ValueError("Argument center_top_cap needs to of shape (3,) !")
        if not all(isinstance(val, float) for val in center_top_cap):
            raise TypeError("Argument center_top_cap needs to be an array of float !")
        self.center_top_cap = center_top_cap

        if not isinstance(radius, float):
            raise TypeError("Argument radius needs to be a float !")
        if radius < EPSILON:
            raise ValueError(f"Argument radius needs to be at least as large as {EPSILON} !")
        self.radius = radius


class RoiSphere:
    """Represent a sphere in 3D space."""

    def __init__(self, center=None, radius=None):
        """Create a sphere in 3D space."""
        if not isinstance(center, (list, np.ndarray)):
            raise TypeError("Argument center needs to be a list or np.ndarray !")
        if np.shape(center) != (3,):
            raise ValueError("Argument center needs to be of shape (3,) !")
        if not all(isinstance(val, float) for val in center):
            raise TypeError("Argument boxdims needs to be an array of float !")
        self.barycenter = center

        if not isinstance(radius, float):
            raise TypeError("Argument radius needs to be a float !")
        if radius < EPSILON:
            raise ValueError(f"Argument radius needs to be at least as large as {EPSILON} !")
        self.radius = radius


# test examples
# obb = RoiRotatedCuboid(center=[0., 0., 0.], boxdims=[10., 2., 5.])
# cyl = RoiRotatedCylinder(center_bottom_cap=[0., 0., 0.],
#                          center_top_cap=[0., 0., 10.], radius=4.)
# ell = RoiSphere(center=[0., 0., 0.], radius=1.5)
