#
# This file is part of paraprobe-toolbox.
#
# paraprobe-toolbox is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License,
#  or (at your option) any later version.
#
# paraprobe-toolbox is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with paraprobe-toolbox. If not, see <https://www.gnu.org/licenses/>.
#
# Base class to be used with jupyter notebooks for creating HDF5 config files
# for generic paraprobe-toolbox tool runs.

import os
import time
import datetime as dt
import numpy as np
import h5py

from utils.src.python.numerics import SUPPORTED_DATATYPES, TOOLS_IN_TOOLBOX
from utils.src.python.primscontinuum \
    import RoiRotatedCuboid, RoiRotatedCylinder, RoiSphere
from utils.src.python.nodeinfo import NodeInfo
from utils.src.python.nexus import specific_nxdl_path, \
    nxdl_path_resolve_parent_of_attribute, \
    nxdl_path_resolve_name_of_attribute
from utils.src.python.hashing import get_file_hashvalue
from utils.src.python.gitinfo import PARAPROBE_VERSION, NEXUS_VERSION
from utils.src.python.hdf_five_string_io \
    import write_strings_to_attribute, write_strings_to_dataset


class ParmsetupFilter:
    """Offer shared functionalities for setting filters for a task."""

    def __init__(self, entry_id: int = 1):
        self.cfg = {}
        self.entry_id = entry_id
        self.add_spatial_filter()
        # having a spatial filter is always required
        # other filters are optional

    def add_evaporation_id_filter(self, lival):
        """Specify linear interval evaporation ID filter."""
        if not isinstance(lival, tuple):
            raise TypeError("Argument lival has to be a tuple !")
        if np.shape(lival) != (3,) \
            or not all(isinstance(val, int) for val in lival) \
            or not all(0 <= val <= np.iinfo(np.uint32).max for val in lival):
            raise ValueError(
                f"Argument lival needs to be an array of float all on interval [0, {np.iinfo(np.uint32).max}] !")
        if lival[0] > lival[2]:
            raise ValueError(f"Arguments lival[0] and lival[2] have to be <= but are {lival[0]} > {lival[2]} !")
        trg = "evaporation_id_filter/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXsubsampling_filter")
        self.cfg[f"{trg}min_incr_max"] = NodeInfo(lival, "", np.uint32)

    def add_iontyp_filter(self,
                          method="whitelist",
                          iontypes=[]):
        """Specify which ions to consider (white) or exclude (black)."""
        allowed = ["whitelist", "blacklist"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed} !")
        if not isinstance(iontypes, (list, np.ndarray)):
            raise TypeError("Argument iontypes needs to be a list or np.ndarray!")
        if not all(isinstance(val, int) for val in iontypes) \
            or not all((0 <= val < 256) for val in iontypes):
            raise ValueError("Argument iontypes needs to be an array of integer on [0, 256)")
        trg = "iontype_filter/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXmatch_filter")
        self.cfg[f"{trg}method"] = NodeInfo(method)
        self.cfg[f"{trg}match"] = NodeInfo(iontypes, "", np.uint32)

    def add_hit_multiplicity_filter(self,
                                    method="whitelist",
                                    multiplicities=[]):
        """Specify which ions to consider based on their hit multiplicity."""
        allowed = ["whitelist", "blacklist"]
        if method not in allowed:
            raise ValueError(f"Argument method needs to be in {allowed} !")
        if not isinstance(multiplicities, (list, np.ndarray)):
            raise TypeError("Argument multiplicities needs to be a list or np.ndarray!")
        if not all(isinstance(val, int) for val in multiplicities) \
            or not all((0 <= val < 256) for val in multiplicities):
            raise ValueError("Argument multiplicities needs to be an array of integer on [0, 256)")
        trg = "hit_multiplicity_filter/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXmatch_filter")
        self.cfg[f"{trg}method"] = NodeInfo(method)
        self.cfg[f"{trg}match"] = NodeInfo(multiplicities, "", np.uint32)

    def add_spatial_filter(self,
                           primitive_list=None,
                           bitmask=None):
        """Define geometric primitives to specify analysis volume."""
        trg = "spatial_filter/"
        if (primitive_list is None) and (bitmask is None):
            self.cfg[f"{trg}@NX_class"] = NodeInfo("NXspatial_filter")
            self.cfg[f"{trg}windowing_method"] = NodeInfo("entire_dataset")
            return

        if primitive_list is not None:
            if isinstance(primitive_list, list):
                n_ell = 0
                n_ell_center = []
                n_ell_ha_radii = []
                n_ell_ori = []
                n_cyl = 0
                n_cyl_center = []
                n_cyl_height = []
                n_cyl_radii = []
                n_obb = 0
                n_obb_vrts = []
                # n_obb_edges = []

                for prim in primitive_list:
                    if isinstance(prim, RoiSphere) is True:
                        n_ell_center.append(prim.barycenter)
                        n_ell_ha_radii.append(
                            [prim.radius, prim.radius, prim.radius])  # sphere
                        n_ell_ori.append([0., 0., 1.])  # sphere
                        n_ell += 1
                    if isinstance(prim, RoiRotatedCylinder) is True:
                        xyz = [0., 0., 0.]
                        uvw = [0., 0., 0.]
                        for i in np.arange(0, 3):
                            xyz[i] = 0.5 * (prim.center_bottom_cap[i] + prim.center_top_cap[i])
                            uvw[i] = prim.center_top_cap[i] - prim.center_bottom_cap[i]
                        n_cyl_center.append(xyz)
                        n_cyl_height.append(uvw)
                        n_cyl_radii.append(prim.radius)
                        n_cyl += 1
                    if isinstance(prim, RoiRotatedCuboid) is True:
                        for i in np.arange(0, 8):
                            n_obb_vrts.append(prim.v[i, :])
                            # ##MK::add edges
                        n_obb += 1

                # prepare the accepted primitives for HDF5 output
                if (n_ell + n_cyl + n_obb) == 0:
                    raise ValueError("The primitive list needs to contain at least one shaped primitive!")
                self.cfg[f"{trg}@NX_class"] = NodeInfo("NXspatial_filter")
                self.cfg[f"{trg}windowing_method"] = NodeInfo("union_of_primitives")
                if n_ell > 0:
                    trg = "spatial_filter/ellipsoid_set/"
                    self.cfg[f"{trg}@NX_class"] = NodeInfo("NXcg_ellipsoid_set")
                    self.cfg[f"{trg}dimensionality"] = NodeInfo(3, "", np.uint32)
                    self.cfg[f"{trg}cardinality"] = NodeInfo(n_ell, "", np.uint32)
                    self.cfg[f"{trg}identifier_offset"] = NodeInfo(0, "", np.uint32)
                    self.cfg[f"{trg}center"] = NodeInfo(
                        np.asarray(n_ell_center, np.float32), "nm", np.float32)
                    self.cfg[f"{trg}half_axes_radius"] = NodeInfo(
                        np.asarray(n_ell_ha_radii, np.float32), "nm", np.float32)
                    self.cfg[f"{trg}orientation"] = NodeInfo(
                        np.asarray(n_ell_ori, np.float32), "", np.float32)
                if n_cyl > 0:
                    trg = "spatial_filter/cylinder_set/"
                    self.cfg[f"{trg}@NX_class"] = NodeInfo("NXcg_cylinder_set")
                    self.cfg[f"{trg}dimensionality"] = NodeInfo(3, "", np.uint32)
                    self.cfg[f"{trg}cardinality"] = NodeInfo(n_cyl, "", np.uint32)
                    self.cfg[f"{trg}identifier_offset"] = NodeInfo(0, "", np.uint32)
                    self.cfg[f"{trg}center"] = NodeInfo(
                        np.asarray(n_cyl_center, np.float32), "nm", np.float32)
                    self.cfg[f"{trg}height"] = NodeInfo(
                        np.asarray(n_cyl_height, np.float32), "nm", np.float32)
                    self.cfg[f"{trg}radii"] = NodeInfo(
                        np.asarray(n_cyl_radii, np.float32), "nm", np.float32)
                if n_obb > 0:
                    trg = "spatial_filter/hexahedron_set/"
                    self.cfg[f"{trg}@NX_class"] = NodeInfo("NXcg_hexahedron_set")
                    self.cfg[f"{trg}dimensionality"] = NodeInfo(3, "", np.uint32)
                    self.cfg[f"{trg}cardinality"] = NodeInfo(n_obb, "", np.uint32)
                    self.cfg[f"{trg}identifier_offset"] = NodeInfo(0, "", np.uint32)
                    self.cfg[f"{trg}hexahedra/@NX_class"] = NodeInfo("NXcg_face_list_structure")
                    self.cfg[f"{trg}hexahedra/vertices"] \
                        = NodeInfo(np.asarray(n_obb_vrts, np.float32), "nm", np.float32)
                # ##MK::add polyhedron
            return

        if bitmask is not None:
            if not isinstance(bitmask, (list, np.ndarray)):
                raise TypeError("Argument bitmask should be a list or np.ndarray!")
            if not all(isinstance(val, bool) for val in bitmask):
                raise ValueError("Argument bitmask needs to be an array of bool !")
            trg = f"/entry{self.entry_id}/spatial_filter/"
            self.cfg[f"{trg}@NX_class"] = NodeInfo("NXspatial_filter")
            self.cfg[f"{trg}windowing_method"] = NodeInfo("bitmask")
            self.cfg[f"{trg}/bitmask/@NX_class"] \
                = NodeInfo("NXcs_filter_boolean_mask")
            self.cfg[f"{trg}/bitmask/number_of_objects"] \
                = NodeInfo(len(bitmask), "", np.uint32)
            self.cfg[f"{trg}/bitmask/bitdepth"] = NodeInfo(8, "bit", np.uint32)
            self.cfg[f"{trg}/bitmask/mask"] \
                = NodeInfo(np.asarray(bitmask, bool), "", bool)


class ParmsetupTaskBase:
    """Offer shared functionalities for setting filters for a task."""

    def __init__(self, toolname: str = "", taskname: str = ""):
        self.entry_id = 1
        self.cfg = {}
        self.toolname = toolname
        if taskname != "":
            self.taskname = taskname
            self.cfg["@NX_class"] = NodeInfo("NXapm_paraprobe_tool_config")
        self.flt = ParmsetupFilter()
        # by default no sub-sampling, and iontype or hit multiply. match filters
        # self.cfg_filter.add_evaporation_id_filter(lival=(0, 1))
        # self.cfg_filter.add_spatial_filter()

    def report_named_option(self,
                            opt_name: str = "",
                            boolean: bool = False):
        """Set a named boolean option."""
        if not isinstance(opt_name, str) or not isinstance(boolean, bool):
            raise TypeError("Argument opt_name needs to be a str, boolean a bool!")
        self.cfg[opt_name] = NodeInfo(np.uint8(boolean), "", np.uint8)

    def load_ranging_relevant_input(self, recon_fpath: str = ""):
        """Identify from where to load the reconstruction and ranging data."""
        if not isinstance(recon_fpath, str):
            raise TypeError("Argument recon_fpath needs to be a string!")
        if recon_fpath == "":
            raise ValueError("Argument recon_fpath needs to be a non-empty string!")
        hashval = get_file_hashvalue(recon_fpath)
        trg = "reconstruction/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}path"] = NodeInfo(recon_fpath)
        self.cfg[f"{trg}checksum"] = NodeInfo(hashval)
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}position"] \
            = NodeInfo("/entry1/atom_probe/reconstruction/reconstructed_positions")
        self.cfg[f"{trg}mass_to_charge"] \
            = NodeInfo("/entry1/atom_probe/mass_to_charge_conversion/mass_to_charge")

        trg = "ranging/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}path"] = NodeInfo(recon_fpath)
        self.cfg[f"{trg}checksum"] = NodeInfo(hashval)
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}ranging_definitions"] \
            = NodeInfo("/entry1/atom_probe/ranging/peak_identification")

    def load_reconstruction(self, recon_fpath: str = ""):
        """Identify from where to load the reconstruction and iontype data."""
        if not isinstance(recon_fpath, str):
            raise TypeError("Argument recon_fpath needs to be a string!")
        if recon_fpath == "":
            raise ValueError("Argument recon_fpath needs to be a non-empty string!")
        trg = "reconstruction/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}path"] = NodeInfo(recon_fpath)
        self.cfg[f"{trg}checksum"] \
            = NodeInfo(get_file_hashvalue(recon_fpath))
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}position"] \
            = NodeInfo("/entry1/atom_probe/reconstruction/reconstructed_positions")
        self.cfg[f"{trg}mass_to_charge"] \
            = NodeInfo("/entry1/atom_probe/mass_to_charge_conversion/mass_to_charge")

    def load_ranging(self, iontypes_fpath: str = ""):
        if not isinstance(iontypes_fpath, str):
            raise TypeError("Argument iontypes_fpath needs to be a string!")
        if iontypes_fpath == "":
            raise ValueError("Argument iontypes_fpath needs to be a non-empty string!")
        trg = "ranging/"
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}path"] = NodeInfo(iontypes_fpath)
        self.cfg[f"{trg}checksum"] \
            = NodeInfo(get_file_hashvalue(iontypes_fpath))
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}ranging_definitions"] \
            = NodeInfo("/entry1/iontypes")  # /iontypes")

    def load_edge_model(self,
                        fpath: str = "",
                        trg: str = "",
                        vertices_dset_name: str = "",
                        facet_indices_dset_name: str = ""):
        """Specify HDF5 file where triangle data should be loaded from."""
        if not isinstance(fpath, str):
            raise TypeError("Argument fpath needs to be a string!")
        if fpath == "":
            raise ValueError("Argument fpath needs to be a non-empty string!")
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] \
            = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}vertices"] \
            = NodeInfo(vertices_dset_name)
        self.cfg[f"{trg}indices"] \
            = NodeInfo(facet_indices_dset_name)

    def load_ion_to_edge_distances(self,
                                   fpath: str = "",
                                   trg: str = "",
                                   dset_name: str = ""):
        """Specify HDF5 file with precomputed ion-to-edge distances."""
        if not isinstance(fpath, str):
            raise TypeError("Argument fpath needs to be a string!")
        if fpath == "":
            raise ValueError("Argument fpath needs to be a non-empty string!")
        self.cfg[f"{trg}@NX_class"] = NodeInfo("NXserialized")
        self.cfg[f"{trg}path"] = NodeInfo(fpath)
        self.cfg[f"{trg}checksum"] = NodeInfo(get_file_hashvalue(fpath))
        self.cfg[f"{trg}type"] = NodeInfo("file")
        self.cfg[f"{trg}algorithm"] = NodeInfo("sha256")
        self.cfg[f"{trg}distance"] = NodeInfo(dset_name)


class ParmsetupBase:
    """Offer frequently used convenience functions."""

    def __init__(self, toolname: str = "", taskname: str = ""):
        """Construct instance."""
        if toolname not in TOOLS_IN_TOOLBOX:
            raise ValueError(f"{toolname} needs to be in {TOOLS_IN_TOOLBOX} !")
        self.toolname = toolname
        self.taskname = taskname
        self.entry_id = 1
        self.nodes = {}
        self.tictoc = {"tic": time.perf_counter(),
                       "start": dt.datetime.now(dt.timezone.utc).isoformat().replace("+00:00", "Z")}

    def create_h5_entry(self, h5_handle, path, obj, verbose=False):
        """Handle all low-level instructing of h5py."""
        if verbose is True:
            print(f"Create {path} {obj.dtype}")
        if type(obj.value) not in SUPPORTED_DATATYPES:
            if hasattr(obj.value, "dtype") is False:
                raise ValueError(f"Value for {path} is of an unsupported type !")

        if hasattr(obj, "shape") is True:
            if obj.dtype in [np.uint8, np.int8, np.uint16, np.int16,
                             np.uint32, np.int32, np.uint64, np.int64,
                             np.float32, np.float64]:
                dst = h5_handle.create_dataset(
                    path, shape=obj.shape, dtype=obj.dtype,
                    data=np.reshape(np.asarray(obj.value), obj.shape))
                if obj.dtype is not str:
                    if obj.unit is not None and obj.unit != "":
                        write_strings_to_attribute(dst, "unit", obj.unit)
            elif obj.dtype == str:
                if obj.value != "":
                    write_strings_to_dataset(h5_handle, path, obj.value)
                else:
                    write_strings_to_dataset(h5_handle, path, "None")
            else:
                raise TypeError(f"Dtype {obj.dtype} for {path} is not one of the supported ones !")
                # dst = h5_handle.create_dataset(path, data=obj.value)
                # if obj.dtype is not str:
                #     if obj.unit is not None and obj.unit != "":
                #         write_strings_to_attribute(dst, "unit", obj.unit)
        else:
            if obj.dtype in [np.uint8, np.int8, np.uint16, np.int16,
                             np.uint32, np.int32, np.uint64,
                             np.int64, np.float32, np.float64]:
                if np.size(np.asarray(obj.value)) != 1:
                    raise ValueError(f"Value for {path} needs to be a scalar !")
                dst = h5_handle.create_dataset(
                    path, shape=(1,), dtype=obj.dtype,
                    data=np.asarray(obj.value).flatten())
                if obj.dtype is not str:
                    if obj.unit is not None and obj.unit != "":
                        write_strings_to_attribute(dst, "unit", obj.unit)
            elif obj.dtype == str:
                # https://forum.hdfgroup.org/t/
                # error-reading-variable-length-utf8-string-using-h5lt-c-api/
                # 4392/5
                if obj.value != "":
                    write_strings_to_dataset(h5_handle, path, obj.value)
                else:
                    write_strings_to_dataset(h5_handle, path, "None")
            else:
                raise TypeError(f"Dtype {obj.dtype} for {path} is not one of the supported ones !")

    def write_config_to_nexus(self, toolname, simid, nodes, verbose=False):
        """Create NeXus/HDF5 file."""
        if toolname not in TOOLS_IN_TOOLBOX:
            raise ValueError(f"Argument toolname needs to be in {TOOLS_IN_TOOLBOX} !")
        if not isinstance(simid, int) or simid <= 0 \
            or int(simid) > int(np.iinfo(np.uint32).max):
            raise ValueError(f"Argument simid needs to be an integer on [1, {np.iinfo(np.uint32).max}]")

        h5fn = f"PARAPROBE.{toolname.capitalize()}.Config.SimID.{simid}.nxs"
        with h5py.File(h5fn, "w") as h5w:
            grp = h5w.create_group(f"/entry{self.entry_id}")
            grp.attrs["NX_class"] = "NXentry"
            dst = h5w.create_dataset(
                f"/entry{self.entry_id}/definition",
                data=f"NXapm_paraprobe_{toolname}_config")
            dst.attrs["version"] = PARAPROBE_VERSION
            grp = h5w.create_group(f"/entry{self.entry_id}/common")
            grp.attrs["NX_class"] = "NXapm_paraprobe_tool_common"
            if self.taskname != "":
                grp = h5w.create_group(f"/entry{self.entry_id}/{self.taskname}")
            grp.attrs["NX_class"] = "NXapm_paraprobe_tool_config"
            trg = f"/entry{self.entry_id}/common/program1"
            grp = h5w.create_group(f"{trg}")
            grp.attrs["NX_class"] = "NXprogram"
            dst = h5w.create_dataset(f"{trg}/program",
                                     data=f"paraprobe-parmsetup-{self.toolname}")
            dst.attrs["version"] = PARAPROBE_VERSION
            # https://www.bloomberg.com/company/stories/work-dates-time-python/
            trg = f"/entry{self.entry_id}/common/profiling"
            grp = h5w.create_group(f"{trg}")
            grp.attrs["NX_class"] = "NXcs_profiling"
            dst = h5w.create_dataset(f"{trg}/number_of_processes",
                                     data=np.uint32(1))
            dst = h5w.create_dataset(f"{trg}/number_of_threads",
                                     data=np.uint16(1))
            dst = h5w.create_dataset(f"{trg}/number_of_gpus",
                                     data=np.uint16(0))
            dst = h5w.create_dataset(f"{trg}/current_working_directory",
                                     data=os.getcwd())

            postponed_attributes = {}

            for keyword, node_info in nodes.items():
                if node_info.value is None:
                    raise ValueError("Value of node_info must not be None !")
                full_nxdl_path = specific_nxdl_path(keyword)
                if verbose is True:
                    print(f"key: {keyword}, val: {full_nxdl_path}")

                if full_nxdl_path.count("/@") == 1:
                    # this is an attribute, postpone
                    if verbose is True:
                        print(f"Postponed {keyword}")
                    postponed_attributes[full_nxdl_path] = node_info
                else:
                    if hasattr(node_info.value, "shape"):
                        # attempt to write node.value reshaped
                        # explicitly based on node.shape
                        if verbose is True:
                            print("Write with explicit shape")
                        self.create_h5_entry(h5w, full_nxdl_path, node_info, False)
                    else:
                        if verbose is True:
                            print("Write with implicit shape")
                        self.create_h5_entry(h5w, full_nxdl_path, node_info, False)

            # write attributes now in the hope that respective groups and datasets exist
            for keyword, node_info in postponed_attributes.items():
                # if verbose is True:
                # print(f"WARNING: Writing a postponed attribute {keyword}, inspect code for string formatting")
                # strip attribute
                path_attr = nxdl_path_resolve_parent_of_attribute(keyword)
                if path_attr == "":
                    raise ValueError(f"Path of attribute {keyword} must not be empty string !")
                attr_attr = nxdl_path_resolve_name_of_attribute(keyword)
                if attr_attr == "":
                    raise ValueError(f"Name of attribute {keyword} must not be empty string !")
                if verbose is True:
                    print(f"path_attr: {path_attr}, attr_attr: {attr_attr}")
                obj = h5w.get(path_attr, default=None)
                if obj is None:
                    raise KeyError(f"Object for attribute {keyword} not found !")

                if type(node_info.value) not in [str, bool, int, float]:
                    raise TypeError(f"Value of attribute {keyword} is an unsupported type !")
                # also a list of only str members should also be allowed as an attribute for the future
                obj.attrs[attr_attr.replace("@", "").replace("units", "unit")] = node_info.value

            dst = h5w.create_dataset(f"/entry{self.entry_id}/common/status", data="success")
            trg = f"/entry{self.entry_id}/common/profiling"
            dst = h5w.create_dataset(f"{trg}/start_time",
                                     data=self.tictoc["start"])
            dst = h5w.create_dataset(f"{trg}/end_time",
                                     data=dt.datetime.now(dt.timezone.utc).isoformat().replace("+00:00", "Z"))
            dst = h5w.create_dataset(f"{trg}/total_elapsed_time",
                                     data=time.perf_counter() - self.tictoc["tic"])
            dst.attrs["unit"] = "s"
            print(f"{h5fn} was written successfully")
        return h5fn
