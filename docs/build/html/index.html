<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>paraprobe-toolbox &#8212; paraprobe-toolbox v0.4 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=4f649999" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=039e1c02" />
    <script src="_static/documentation_options.js?v=6cc4e9c0"></script>
    <script src="_static/doctools.js?v=888ff710"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
          

          <div class="body" role="main">
            
  <figure class="align-center">
<a class="reference external image-reference" href="http://gitlab.com/paraprobe/paraprobe-toolbox"><img alt="_images/FigSydneyGraphExample.05.png" src="_images/FigSydneyGraphExample.05.png" /></a>
</figure>
<section id="paraprobe-toolbox">
<h1>paraprobe-toolbox<a class="headerlink" href="#paraprobe-toolbox" title="Link to this heading">¶</a></h1>
<p>The <a class="reference external" href="http://gitlab.com/paraprobe/paraprobe-toolbox">paraprobe-toolbox</a> is a collection of open-source tools for efficient analyses of point cloud data where each point can represent atoms or molecular ions. A key application of the toolbox has been for research in the field of <a class="reference external" href="https://www.nature.com/articles/s43586-021-00054-x">Atom Probe Tomography (APT)</a> and related <a class="reference external" href="https://doi.org/10.1088/1367-2630/ab5cc4">Field Ion Microscopy (FIM)</a>. The toolbox does not replace but complements existent software tools in this research field. Given its capabilities of handling points as objects with properties and enabling analyses of the spatial arrangement of and intersections between geometric primitives, the software can equally be used for analyzing data in materials science and engineering.</p>
<section id="capabilities">
<h2>Capabilities<a class="headerlink" href="#capabilities" title="Link to this heading">¶</a></h2>
<p>Each tool of the paraprobe-toolbox is specialized for specific tasks. Examples are the loading of point cloud data from formats of commercial atom probe software, the computing of triangulated surface meshes using convex hulls, alpha shapes, or alpha wrappings, the building of tessellations, the computing of spatial statistics, the computing of iso-surfaces and subsequent identifying of microstructural features, and the correlating of geometric primitives and objects via graph-based analyses.</p>
</section>
<section id="efficient-and-fair-embracing">
<h2>Efficient and FAIR-embracing<a class="headerlink" href="#efficient-and-fair-embracing" title="Link to this heading">¶</a></h2>
<p>The paraprobe-toolbox is designed for users which like to take advantage of parallelization. To this end the project <a class="reference external" href="https://www.nature.com/articles/s41524-020-00486-1">has delivered how open-source software developments within the field of computer science and computational geometry</a> are successfully made accessible <a class="reference external" href="https://onlinelibrary.wiley.com/iucr/doi/10.1107/S1600576721008578">to the atom probe research community</a> enabling users to take advantage of transparent, fast, and robust algorithms. Originally developed in C/C++, the project has recently embraced Python to offer scientists an easier way how they can use the tools in combination with own Python-based workflows. The paraprobe-toolbox uses state-of-the-art libraries like the <a class="reference external" href="https://www.cgal.org">Computational Geometry Algorithms Library</a>. Supporting users with <a class="reference external" href="https://www.nature.com/articles/s41586-022-04501-x">FAIR research</a> and implementing <a class="reference external" href="https://www.nature.com/articles/sdata201618">FAIR data stewardship principles</a> is another recent design priority of paraprobe-toolbox.</p>
</section>
<section id="nexus-data-schemas">
<h2>NeXus data schemas<a class="headerlink" href="#nexus-data-schemas" title="Link to this heading">¶</a></h2>
<p>With using <a class="reference external" href="https://fairmat-experimental.github.io/nexus-fairmat-proposal/9636feecb79bb32b828b1a9804269573256d7696/north-structure.html">NeXus</a> as the data format and description tool, a large set of defined data schemas have been formulated which pioneer how common computational workflows in the field of atom probe can be described with clearly defined data schemas. Using these for each tool, makes computations with the paraprobe-toolbox numerically repeatable and the respective input and output files understandable and machine-actionable.</p>
<p>Feel free to utilize the tool. In doing so, feel free to suggest improvements or analysis features which you think would be great to have or help improve the toolbox. This documentation should serve as a guide for using the paraprobe-toolbox.</p>
</section>
<section id="how-to-start">
<h2>How to start?<a class="headerlink" href="#how-to-start" title="Link to this heading">¶</a></h2>
<p>The paraprobe-toolbox is a combination of software tools. Some are written in Python, some are written in C/C++. This requires an installation of the software and its dependencies.
We tested the tool successfully on Linux (Ubuntu &gt;=v18) and Windows using the <a class="reference external" href="https://learn.microsoft.com/en-us/windows/wsl/">Windows Subsystem for Linux (WSL2)</a>. Compiling for a Macintosh computer should be possible. In absence of such computer, we had unfortunately not a chance yet to test this. Let us know if you would like to use paraprobe-toolbox with a Mac.</p>
<p>The latest version of paraprobe-toolbox is v0.4. Using this version is recommended as it enables to profit from clearly defined data schemas, clean HDF5 files with provenance tracking, and data compression by default to make working with large studies more disk-space efficient.</p>
<p>So far the paraprobe-toolbox has to be installed in what is effectively a developer version. <a class="reference external" href="https://gitlab.com/paraprobe/paraprobe-toolbox">A sequence of steps is required. Users should inspect these in the following scripts</a>:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">PARAPROBE</span><span class="o">.</span><span class="n">Step01</span><span class="o">.</span><span class="n">InstallOSDeps</span><span class="o">.</span><span class="n">sh</span>
<span class="n">PARAPROBE</span><span class="o">.</span><span class="n">Step02</span><span class="o">.</span><span class="n">InstallCondaJupyterLab</span><span class="o">.</span><span class="n">sh</span>
<span class="n">PARAPROBE</span><span class="o">.</span><span class="n">Step03</span><span class="o">.</span><span class="n">InstallThirdPartyDeps</span><span class="o">.</span><span class="n">sh</span>
<span class="n">PARAPROBE</span><span class="o">.</span><span class="n">Step04</span><span class="o">.</span><span class="n">BuildTools</span><span class="o">.</span><span class="n">sh</span>
<span class="n">PARAPROBE</span><span class="o">.</span><span class="n">Step05</span><span class="o">.</span><span class="n">Install</span><span class="o">.</span><span class="n">Tools</span><span class="o">.</span><span class="n">sh</span>
<span class="n">PARAPROBE</span><span class="o">.</span><span class="n">Step06</span><span class="o">.</span><span class="n">GetExamples</span><span class="o">.</span><span class="n">sh</span>
</pre></div>
</div>
<p>Thanks to <a class="reference external" href="https://www.mpie.de/person/122342/2656491">Sarath Menon</a> and members of the NFDI-MatWerk consortium there is an <strong>experimental</strong> conda-forge channel available for the paraprobe-toolbox.
This channel still uses an older version of the toolbox (v0.3.1) which we are currently porting to v0.4. Therefore, <strong>we</strong> strongly <strong>advise not to
use</strong> the conda-forge channel <strong>for now</strong>. After having everything ported, though, installing the paraprobe-toolbox via conda-forge is the preferred (and much easier way) how to get an installation of the paraprobe-toolbox.</p>
<p>Another approach how users can take advantage of latest research data management software with native support and inclusion of a containerized version of the paraprobe-toolbox (amongst other open-source software tools) is via installing a local instance of the <a class="reference external" href="https://nomad-lab.eu/prod/v1/staging/docs/developers.html">NOMAD Oasis</a> research data management system.</p>
</section>
<section id="how-to-use">
<h2>How to use?<a class="headerlink" href="#how-to-use" title="Link to this heading">¶</a></h2>
<p>The toolbox comes with a collection of examples how the tools can be used for various types of analyses. <a class="reference external" href="https://gitlab.com/paraprobe/paraprobe-toolbox/-/tree/main/teaching">These examples are available as jupyter-notebooks</a>.</p>
<p>We recommend to <strong>start with the beginners examples</strong> and specifically <strong>explore</strong> first the <strong>usa_portland_wang.ipynb tutorial</strong> before starting to customize existent or build own workflows with the paraprobe-toolbox. These tutorials show that every analysis with a tool has three steps:</p>
<ol class="arabic simple">
<li><p>Creation of a configuration file using convenience function in Python. The respective tool is called paraprobe-parmsetup. The result will be a NeXus/HDF5 config file. This file includes all settings, time stamps, and hashes of input files.</p></li>
<li><p>Run the analysis. This will execute either a Python script or a compiled C/C++ application. Results will be stored in a NeXus/HDF5 results file. This file will include all results, time stamps, hashes, and profiling data. Depending on which tool and tasks are performed, additional <a class="reference external" href="https://www.xdmf.org/index.php/XDMF_Model_and_Format">XDMF</a> files are generated whereby results can be visualized using <a class="reference external" href="https://www.paraview.org/">ParaView</a>.</p></li>
<li><p>Post-process the results using Python for example. For these steps, the paraprobe-autoreporter Python tool of the toolbox offers many convenience functions for generating frequently shown plots.</p></li>
</ol>
</section>
<section id="how-to-cite">
<h2>How to cite?<a class="headerlink" href="#how-to-cite" title="Link to this heading">¶</a></h2>
<p>Users of tools, data schemas, or implementation ideas of the paraprobe-toolbox should cite the
<a class="reference external" href="https://gitlab.com/paraprobe/paraprobe-toolbox">GitLab repository of the software</a> and at least one of the
scientific paper’s for the toolbox:</p>
<ul class="simple">
<li><p><a class="reference external" href="https://arxiv.org/abs/2205.13510">https://arxiv.org/abs/2205.13510</a></p></li>
<li><p><a class="reference external" href="https://doi.org/10.1107/S1600576721008578">https://doi.org/10.1107/S1600576721008578</a></p></li>
<li><p><a class="reference external" href="https://doi.org/10.1038/s41524-020-00486-1">https://doi.org/10.1038/s41524-020-00486-1</a></p></li>
<li><p><a class="reference external" href="https://doi.org/10.1017/S1431927621012241">https://doi.org/10.1017/S1431927621012241</a></p></li>
</ul>
</section>
<section id="license">
<h2>License<a class="headerlink" href="#license" title="Link to this heading">¶</a></h2>
<p>The paraprobe-toolbox is <a class="reference external" href="https://gitlab.com/paraprobe/paraprobe-toolbox/-/blob/main/LICENSE.GPL">GPLv3 licensed</a>.</p>
</section>
<section id="funding">
<h2>Funding<a class="headerlink" href="#funding" title="Link to this heading">¶</a></h2>
<p>Markus Kühbach gratefully acknowledges the support from several partners over the years: The support from the Deutsche Forschungsgemeinschaft (<a class="reference external" href="https://www.dfg.de/">DFG</a>) through project BA 4253/2-1. The provisioning of computing resources by the Max-Planck Gesellschaft, and the funding received through BiGmax, the Max-Planck-Research Network on Big-Data-Driven Materials Science. The support from the FAIRmat consortium. FAIRmat is funded by the Deutsche Forschungsgemeinschaft (<a class="reference external" href="https://www.dfg.de/">DFG</a>, German Research Foundation) – project 460197019.</p>
</section>
<section id="history">
<h2>History<a class="headerlink" href="#history" title="Link to this heading">¶</a></h2>
<p>The toolbox is developed by <a class="reference external" href="https://orcid.org/0000-0002-7117-5196">Markus Kühbach</a> who is supported by members of the international atom probe community. The project started in autumn 2017 with Andrew Breen from the University at New South Wales. The early development of the project was implemented during a PostDoc stay in Dierk Raabe’s and Baptiste Gault’s group for atom probe tomography at the <a class="reference external" href="https://www.mpie.de">Max-Planck Institut für Eisenforschung GmbH in Düsseldorf</a>. Later the project was supported by the <a class="reference external" href="https://www.bigmax.mpg.de">BiGmax</a> research network of the Max-Planck Society. With moving to the Humboldt-Universität zu Berlin (Department of Physics), the project got more and more support from members of the international atom probe community. With the formation of the <a class="reference external" href="https://www.nfdi.de/?lang=en">German National Research Data Infrastructure (NFDI)</a> and the main developer of the paraprobe-toolbox now supporting the international electron microscopy and atom probe community to build and work towards the implementation and usage of software for FAIR research data management, the paraprobe-toolbox project is developed further. Now the software is used as an open-source plugin amongst other tools in software tools of the <a class="reference external" href="https://www.fairmat-nfdi.eu/fairmat">FAIRmat</a> and the <a class="reference external" href="https://nfdi-matwerk.de/infrastructure-use-cases/iuc09-infrastructure-interfaces-with-condensed-matter-physics-collaboration-with-fairmat">NFDI-MatWerk</a> consortia of the NFDI.</p>
</section>
</section>


          </div>
          
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2018-2023, Markus Kühbach.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 7.2.5</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.13</a>
      
      |
      <a href="_sources/index.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>