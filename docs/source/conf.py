# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'paraprobe-toolbox'
copyright = '2018-2023, Markus Kühbach'
author = '2018-2023, Markus Kühbach'
release = 'v0.4'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'  # 'python_docs_theme'  # 'alabaster'
html_theme_options = {
        # disable showing the sidebar
        'nosidebar': True
}
# html_static_path = ['_static']
