# Example workflows
The paraprobe-toolbox comes with a set of jupyter-notebook based tutorials.
These are meant as templates to show how the tools work. The jupyter notebooks
are designed for interactive execution to explore how each step of a workflow
proceeds and which input and output is used or generated.
The jupyter-notebooks are also designed to support users and developers who wish
to write, based on the available code snippets, Python scripts with which they
can automate the workflow via looping over a collection of datasets. We hope that
this will also support developers who wish to implement these workflows
into workflow automation tools like [pyiron](https://www.sciencedirect.com/science/article/pii/S0927025618304786).


# Getting started
The examples/tutorials/teaching material is structured as follows: The *example_data* directory
stores the measured (reconstructed and ranged) datasets for example in one sub-directory for each case.
The *example_analyses* directory stores the jupyter notebook and respective results of a workflow;
again inside a sub-directory for each case.

## tutorial_molecular_ion_beginner
Learn how atoms, elements, and ions are represented in the paraprobe-toolbox.

## tutorial_molecular_ion_search
Learn how classical combinatorial searches for all (theoretically) possible ions
can be achieved with paraprobe-toolbox.

## tutorial_nexus
Learn what NeXus is and how the specific NeXus data schema is used by the tools
of the paraprobe-toolbox in NOMAD (Oasis) to read from and write to
specifically-formatted HDF5 files whose data hierarchy conforms to tool-specific
NeXus data schemas.

## usa_portland_wang
Learn how to use the variety of specific analyses which the tools of the paraprobe-
toolbox offer. The training dataset was kindly made open-source and available by
colleagues Jing Wang and Daniel Schreiber from the Pacific Northwest National Laboratory
(PNNL). Specifically, it is a small dataset describing an ODS steel with Y-rich
oxides/precipitates. The dataset can be used as a benchmark for cluster analyses
and iso-surfaces, as it was discussed in the [literature](https://doi.org/10.1017/S1431927621012241).
Here we use the dataset as a generic example for testing all tools of the paraprobe-toolbox.


# Advanced topics

## aut_leoben_leitner
Learn how to use the paraprobe-toolbox to analyze solute segregation at interfaces.
The training dataset was kindly made available by Katharina Leitner neé Babinski
and colleagues at the Montanuniversität Leoben. This is the dataset analyzed in the
use case by Markus Kühbach, Alexander Reichmann, Stefan Kardos, and Lorenz Romaner
in the [following work](https://arxiv.org/abs/2205.13510). Specifically, the example shows
how to instruct an interface reconstruction and perform subsequence composition and
interfacial excess analyses using a revised implementation of the DCOM algorithm
(which was originally proposed by [P. Felfer et al.](https://doi.org/10.1016/j.ultramic.2013.03.004).
The implementation in the paraprobe-toolbox was successfully used to segment
several interfaces completely automatically provided that only a single interface
is present. In cases where the interface was not fully automatically detected it
was in all investigated cases possible to restrict the analyses to an oriented bounding
box to recover the capability of the algorithm to reconstruct a triangulated surface
mesh without further supervision. We expect that there will be cases where a completely
automated analysis may not be possible though. Therefore, we would like to mention
two [complementary approaches tools](https://www.youtube.com/watch?v=rep9d8Opk9c) using
Blender or [artificial intelligence](https://doi.org/10.1016/j.actamat.2022.117633).

## tutorial_compare_edge_models
Learn how the variety of tools of the paraprobe-toolbox can help you with creating
triangulated surface meshes which can be used to represent the edge of a dataset
to segment an atom probe dataset for distances of ions to the edge of the dataset.
Specifically, the example walks you through a detailed computational geometry
analyses which exemplifies how using different meshing algorithms affect your
results quantitatively. The training dataset is in the open-source domain.
It was measured and characterized by [Samara Levine and team](https://dx.doi.org/10.17632/375c38sf2h.1).
The tutorial covers classical convex hulls, mesh refinements, alpha-shapes
and alpha wrappings as well as complementary iso-surface and tessellation-based
approaches.

## aut_leoben_mendez_martin
Learn how classical iso-surface-based analyses can be performed using a fully-automated
and scriptable workflow which automatically reconstructs volumetric microstructural
features. The training dataset was measured by Francisca Mendez-Martin at her time
at the Montanuniversität Leoben. The dataset contains an interface with a large
carbide-rich precipitate. The dataset lends itself perfectly for feature development
because the precipitate is truncated by the edge of the dataset which enables to
check how the tools of the paraprobe-toolbox deal with microstructural features
at the edge of the dataset.

## aus_sydney_rielli
Learn how the variety of tools of the paraprobe-toolbox can be used to perform
fully-automated precipitate analyses including not only classically explored
descriptors like number density and local composition but also the spatial
correlation of groups of nearby precipitates. This tutorial exemplifies most
of the work which was reported in [Markus Kühbach's et al.](https://arxiv.org/abs/2205.13510)
work. The specific dataset probes a nickel-base super alloy specimen which was measured
by Vitor Rielli supervised by Sophie Primig (both with the University of New South Wales, UNSW).
Te original dataset is not in the public space. Users interested in repeating the analyses
are kindly asked to contact Vitor Rielli directly. Alternatively, the jupyter notebook
can be used to instruct very similar type of analyses by just editing the notebook.
This enables to explore a substantially larger number of dataset for which one expects
or already confirmed that precipitates are included and one wishes to segment and analyze
these using high-throughput iso-composition analyses and coprecipitation analyses.
