You can use this notebook as a template and customize it to create your own analyses.

Like stated in the paper, please contact Sophie Primig and Vitor V. Rielli 
if you would like to run this notebook on exactly the same datasets that 
were used in the paper. 

SHA256 checksums have been published with the supplementary material
to the paper to confirm whether you have the same binary reconstructed
dataset and range file respectively.

R04_22071.pos
8040adffed5c1f9761cdc2da6b881625c9f9335f3e96c557374f8bae0dc89078

R04_22071.RRNG
4cb6dc8da15412e33104fc64c6b2e6480484d08f8ca8641895fd00ea848e9f8f
