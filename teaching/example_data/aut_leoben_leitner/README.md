You can use this notebook as a template and customize it to create your own analyses.

Like stated in the paper, please contact Alexander Reichmann, Stefan Kardos, and 
Lorenz Romaner directly if you would like to run this notebook on exactly the 
same datasets that were used in the paper. 

SHA256 checksums have been published with the supplementary material 
to the paper to confirm whether you have the same binary reconstructed 
dataset and range file respectively.

R21_08680-v02.pos
be6287c45233de97f0376fd8e1e078f7cfa029bac8b1a4087936076a2ea0de51

R21_08680.rrng
c75aead0e82d7e4149cf07192e734ee9765259259b9e7cba96f284d4a7ec7cea
