The dataset is a small one for testing purposes that comes as
a test case with the repository. The dataset is also accessible here:

Ceguerra, AV (2021) Supplementary material: APT test cases.
Available at http://dx.doi.org/10.25833/3ge0-y420
(retrieved February 23, 2021).

The SHA256 checksum of the reconstructed dataset and range files are:
R31_06365-v02.pos
9ef781cb004a966d05484c8704688e8d19bd47e17ce8135b742348cda85439ab

R31_06365-v02.rrng
82833c87d92d4ec99ffd1541fd15d9911fdca1c823ae929d292f1d56ee176d27

Once you have the two files you can either use this notebook as to try your
installation and familiarize with the tools or use the notebook as a template
and customize it so that you can create your own analyses.
